/*******************************************************************************
* Source File : WorkflowAttachmentShowViewController.h
* Date Created: 12/14/2010
* Copyright   : 2000 - 2010, Sybase, Inc.
* Description : Handles workflow attachment displaying
* Notes       :
*
*******************************************************************************/
#import <UIKit/UIKit.h>
@class WidgetScreenControllerPrivate;
/*!
 @class WorkflowAttachment
 @abstract Workflow attachment class
 */
@interface WorkflowAttachment : NSObject
{
   NSString* fileName;
   NSString* tmpFileName;
   NSString* mimeType;
   NSData* data;
   NSString* url;
   WidgetScreenControllerPrivate* parentScreenController;
}
/*!
 @property
 @abstract data of attachment
 */
@property (nonatomic, retain) NSData * data;
/*!
 @property
 @abstract file name of attachment
 */
@property (nonatomic, retain) NSString * fileName;
/*!
 @property
 @abstract MIME type of attachment
 */
@property (nonatomic, retain) NSString * mimeType;
/*!
 @property
 @abstract temp file name of attachment
 */
@property (nonatomic, retain) NSString * tmpFileName;
/*!
 @property
 @abstract url of attachment
 */
@property (nonatomic, retain) NSString * url;
/*!
 @property
 @abstract parent screen view controller
 */
@property (nonatomic, retain) WidgetScreenControllerPrivate * parentScreenController;
/*!
 @method
 @abstract initialization of attachment
 
 @param aData     data of attachment
 @param aFileName file name of attachment
 @param aMimeType Mime type
 
 @return WorkflowAttachment
 */
- (id)initWithData:(NSData*) aData withFileName:(NSString*)aFileName withMimeType:(NSString*)aMimeType;
/*!
 @method
 @abstract initialization of attachment
 
 @param aUrl url of attachment
 
 @return WorkflowAttachment
 */
- (id)initWithUrl:(NSString*)aUrl;

@end
/*!
 @class WorkflowAttachmentShowViewController
 @abstract Workflow attachment view controller
 */
@interface WorkflowAttachmentShowViewController : UIViewController <UIWebViewDelegate> {
   UIWebView* content;
   UIActivityIndicatorView* spinner;
   UILabel* loading;
   WorkflowAttachment* attachment;   
   CGRect viewFrame;
}
/*!
 @property
 @abstract WorkflowAttachment
 */
@property (nonatomic, retain) WorkflowAttachment* attachment;
/*!
 @method
 @abstract WorkflowAttachmentShowViewController initialization
 
 @param aFrame frame of WorkflowAttachmentShowViewController
 @param att    WorkflowAttachment
 
 @return WorkflowAttachmentShowViewController
 */
- (id)initWithFrame:(CGRect)aFrame withAtt:(WorkflowAttachment *)att;

@end
