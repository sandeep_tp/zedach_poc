/*
Copyright (c) Sybase, Inc. 2012 All rights reserved. 

In addition to the license terms set out in the Sybase License Agreement for 
the Sybase Unwired Platform ("Program"), the following additional or different 
rights and accompanying obligations and restrictions shall apply to the source 
code in this file ("Code"). Sybase grants you a limited, non-exclusive, 
non-transferable, revocable license to use, reproduce, and modify the Code 
solely for purposes of (i) maintaining the Code as reference material to better 
understand the operation of the Program, and (ii) development and testing of 
applications created in connection with your licensed use of the Program. 
The Code may not be transferred, sold, assigned, sublicensed or otherwise 
conveyed (whether by operation of law or otherwise) to another party without 
Sybase's prior written consent. The following provisions shall apply to any 
modifications you make to the Code: (i) Sybase will not provide any maintenance 
or support for modified Code or problems that result from use of modified Code; 
(ii) Sybase expressly disclaims any warranties and conditions, express or 
implied, relating to modified Code or any problems that result from use of the 
modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING 
TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN 
IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
to indemnify, hold harmless, and defend Sybase from and against any claims or 
lawsuits, including attorney's fees, that arise from or are related to the 
modified Code or from use of the modified Code.
*/

/******************************************************************************
*    Copyright 2012 Sybase, Inc
*    Source File            : CmoError.h
*    Platform Dependencies  :
*    Notes                  :
******************************************************************************/

#ifndef CmoError_H_INCLUDED
#define CmoError_H_INCLUDED

class CmoError;

#include "moOS.h"
#include "moString.h"
#include "moTypes.h"

using namespace mo;

typedef CmoError CmoErr;

#ifdef MOCLIENT_IPHONE
#define SOURCE_LOCATION             ((strrchr(__FILE__, '/') ?: __FILE__ - 1) + 1), __LINE__
#else
#define SOURCE_LOCATION             _T(__FILE__), __LINE__
#endif

#define NO_ERROR_LINE_NUM        -1

#define OBJECT_BEGIN_TAG        _T("<object>")
#define OBJECT_END_TAG          _T("</object>")
#define METHOD_BEGIN_TAG        _T("<method>")
#define METHOD_END_TAG          _T("</method>")
#define DESC_BEGIN_TAG          _T("<desc>")
#define DESC_END_TAG            _T("</desc>")

CmoErr* GetLastErr();
void FreeLastErr();
TCHAR* GetErrMsg( I32 lErr );
const TCHAR* GetDefaultDetail( I32 lErrorCode );

void SetLastErr( I32          lm_ErrorCode,
                 const TCHAR* pcFileName,
                 I32          lLineNo,
                 const TCHAR* pcDetail = 0,
                 I32          lNativem_ErrorCode = 0,
                 bool         bThrowError = true,
                 const TCHAR* pcMessage= 0);

#if defined( MO_SERVER )
void CheckCOMErr( HRESULT hr,
                  TCHAR* pcFile,
                  I32 lLineNo,
                  I32 lErrorCode,
                  TCHAR *pcObjectName,
                  TCHAR *pcMethodName=0,
                  IUnknown* pThis=0);

#include "\Pioneer\Platform\WIN32\ETrace.h"
void SetLastErrTrace( TraceLevel level, 
                   I32          lm_ErrorCode,
                   const TCHAR* pcFileName,
                   I32          lLineNo,
                   const TCHAR* pcDetail = 0,
                   I32          lNativem_ErrorCode = 0,
                   bool         bThrowError = true,
                   const TCHAR* pcMessage= 0);
#endif /* MO_SERVER */

class CmoError
{
public:
   CmoError();
   CmoError(CmoError& rhs);
   CmoError(I32 nCode);
   ~CmoError();

   void Clear();
   long Init(bool bNoAlloc = false);
   bool Display( const TCHAR* pcTitle = 0 );

   void setSource( const TCHAR* pcFileName,
                   long lLineNo );
   void Raise( bool bShowLastOSErr = false );
   void PopulateMessage( bool bShowLastOSErr );

   void GetString( CmoString* pstrResult );
   TCHAR* GetString( TCHAR*& pstrResult );
   TCHAR* SetString(TCHAR*& szDest, const TCHAR* szSrc);
   TCHAR* MakeDisplayString(TCHAR*& pszDisplay, bool bIsForDisplay = false);

   CmoError& operator =( CmoError& RHValue );
   void SetErrorCode(I32 nCode){m_ErrorCode = nCode;}
   void SetNativeErrorCode(I32 nCode){m_NativeErrorCode = nCode;}
   I32 ErrorCode(){return m_ErrorCode;}
   I32 NativeErrorCode(){return m_NativeErrorCode;}

   TCHAR* SetSource(const TCHAR* szVal){return SetString(m_szSource, szVal);}
   TCHAR* SetMessage(const TCHAR* szVal){return SetString(m_szMessage, szVal);}
   TCHAR* SetDetail(const TCHAR* szVal){return SetString(m_szDetail, szVal);}
   TCHAR* SetDetail(I32 lVal);
   TCHAR* Message(){return m_szMessage;}
   TCHAR* Source(){return m_szSource;}
   TCHAR* Detail(){return m_szDetail;}
#if defined ( MO_SERVER)
   void SetTraceAndNotLog(){ m_bTraceOnly = true; }
   void ClearTrace(){ m_bTraceOnly = false; }
   void SetTraceLevel( TraceLevel level ){ m_eTraceLevel = level; }
   bool m_bTraceOnly;
   TraceLevel m_eTraceLevel;
#endif

   TCHAR* m_szMessage;
   TCHAR* m_szSource;
   TCHAR* m_szDetail;
   I32 m_ErrorCode;
   I32 m_NativeErrorCode;
};


#endif// CmoError_H_INCLUDED
