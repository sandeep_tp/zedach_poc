/*
 
 Copyright (c) Sybase, Inc. 2009-2013   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */

// Error message defines
#define SUPOBJ_NULLMETHODMESSAGE "SUPDeliverMessage: null method, no message to deliver"
#define SUPOBJ_NOQUEUECONNECTIONFOUND "No queue connection found"
#define SUPOBJ_GOTSUPINITIALIZE "SUPObj Got SUPInitialize()"
#define SUPOBJ_GOTSUPCALLDISPOSE "SUPObj Got SUPDispose()"
#define SUPOBJ_GOTSUPASYNCMETHODCALLFORSERVER "SUPObj Got SUPAsyncMethodCallFromServer(): %s"
#define SUPOBJ_GOTSUPPIMCHANGENOTIFICATION "SUPObj Got SUPPIMChangeNotification()"
#define SUPOBJ_GOTSUPREFRESHALLDATA "SUPObj Got SUPRefreshAllData()"
#define SUPOBJ_GOTSUPDATASTORECLEARED "SUPObj Got SUPDatastoreCleared()"
#define SUPOBJ_GOTSUPCONNECTIONSTATECHANGED "SUPObj Got SUPConnectionStateChanged()"
#define SUPOBJ_INVALIDJSONHEADER "Header received from server is not a valid JsonObject."
#define SUPOBJ_CIDCANNOTBEZERO "cid should not be 0 in the message header from server."
#define SUPOBJ_CIDISMISSING "No cid in the message headers from server."
#define SUPOBJ_CLIENTCIDISINVALID "SUPObj: cid value sent by application is invalid"
#define SUPOBJ_INVALIDCLIENTMESSAGEFORMAT "The format of request from SUP application is incorrect"
#define SUPOBJ_INVALIDCONNECTIONSTATE "SUPObj: connection state received is not a valid value: %d"
#define SUPOBJ_INVALIDCONNECTIONTYPE "SUPObj: connection type received is not a valid value: %d"
#define SUPOBJ_CALLABLECOMPONENTTAG "__componentName__"






