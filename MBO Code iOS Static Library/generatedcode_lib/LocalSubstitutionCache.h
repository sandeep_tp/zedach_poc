/*******************************************************************************
* Source File : LocalSubstitutionCache.h
* Date Created: 09/17/2010
* Copyright   : 2000 - 2010, Sybase, Inc.
* Description : NSURLConnection cache where the data can be loaded from an 
*             : encrypted store.
* Notes       :
*
*******************************************************************************/

#import <Foundation/Foundation.h>
#import "WidgetScreenControllerPrivate.h"

@interface LocalSubstitutionCache : NSURLCache
{
   NSString* baseFolder;   
   WidgetScreenControllerPrivate* widget;
   NSMutableDictionary* bodyDict;
}

@property (nonatomic, retain) NSString* baseFolder;
@property (nonatomic, readonly) WidgetScreenControllerPrivate* widget;
@property (nonatomic, retain) NSMutableDictionary* bodyDict;

- (id) initWithWidget:(WidgetScreenControllerPrivate*)aWidget;
- (void) logError:(int)iErrorCode withMsg:(NSString*)nativeMsg;
- (void) logErrorToMocaLog:(int)iErrorCode withMsg:(NSString*)nativeMsg;
- (void) setWidget:(WidgetScreenControllerPrivate *)aWidget;
@end
