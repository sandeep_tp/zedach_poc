/*
 Copyright (c) Sybase, Inc. 2011 All rights reserved. 
 In addition to the license terms set out in the Sybase License Agreement for 
 the Sybase Unwired Platform ("Program"), the following additional or different 
 rights and accompanying obligations and restrictions shall apply to the source 
 code in this file ("Code"). Sybase grants you a limited, non-exclusive, 
 non-transferable, revocable license to use, reproduce, and modify the Code 
 solely for purposes of (i) maintaining the Code as reference material to better 
 understand the operation of the Program, and (ii) development and testing of 
 applications created in connection with your licensed use of the Program. 
 The Code may not be transferred, sold, assigned, sublicensed or otherwise 
 conveyed (whether by operation of law or otherwise) to another party without 
 Sybase's prior written consent. The following provisions shall apply to any 
 modifications you make to the Code: (i) Sybase will not provide any maintenance 
 or support for modified Code or problems that result from use of modified Code; 
 (ii) Sybase expressly disclaims any warranties and conditions, express or 
 implied, relating to modified Code or any problems that result from use of the 
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING 
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN 
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
 to indemnify, hold harmless, and defend Sybase from and against any claims or 
 lawsuits, including attorney's fees, that arise from or are related to the 
 modified Code or from use of the modified Code.
 */
#import <UIKit/UIKit.h>
#import "SUPJsonReader.h"
#import "SUPJsonArray.h"

@interface WorkflowInstaller : NSObject <NSXMLParserDelegate> {
   BOOL _isIphone;
   NSXMLParser *_parser;
   NSMutableString *_currentString;
   BOOL _storingCharacters;
   NSString *_path;
   NSString *_workflowFileName;
   NSString *_moduleName;
   NSString *_moduleVersion;
   NSString *_moduleDisplayName;
   NSString *_clientIconIndex;
   NSString *_invokeOnClient;
   NSString *_markProcessedMessages;
   NSString *_deleteProcessedMessages;
   NSString *_credentialsCache;
   NSString *_credentialsCacheKey;
   NSString *_requiresActivation;
   NSString *_sharedStorage;
   NSString *_sharedStorageKey;
   NSString *_activationKey;
   NSString *_credentialsScreen;
   NSString *_activationScreen;
   SUPJsonArray* _customIcon;
   SUPJsonObject* _metadata;
   BOOL _firstFileParsed;
   BOOL parserError;
   BOOL _bInsideMetadataElement;
   BOOL _bInsideMetadataItemElement;
   NSString * _metadataItemKey;
   NSString * _metadataItemValue;
   NSString * _designerVersion;
}

@property (nonatomic, assign) NSXMLParser *parser;
@property (nonatomic, assign) NSMutableString *currentString;
@property (nonatomic, copy) NSString *path;
@property (nonatomic, copy) NSString *workflowFileName;
@property (nonatomic, copy) NSString *moduleName;
@property (nonatomic, copy) NSString *moduleVersion;
@property (nonatomic, copy) NSString *moduleDisplayName;
@property (nonatomic, copy) NSString *clientIconIndex;
@property (nonatomic, copy) NSString *invokeOnClient;
@property (nonatomic, copy) NSString *markProcessedMessages;
@property (nonatomic, copy) NSString *deleteProcessedMessages;
@property (nonatomic, copy) NSString *credentialsCache;
@property (nonatomic, copy) NSString *credentialsCacheKey;
@property (nonatomic, copy) NSString *requiresActivation;
@property (nonatomic, copy) NSString *sharedStorage;
@property (nonatomic, copy) NSString *sharedStorageKey;
@property (nonatomic, copy) NSString *activationKey;
@property (nonatomic, copy) NSString *credentialsScreen;
@property (nonatomic, copy) NSString *activationScreen;
@property (nonatomic, retain) SUPJsonArray *customIcon;
@property (nonatomic, retain) SUPJsonObject *metadata;
@property (nonatomic, copy) NSString *metadataItemKey;
@property (nonatomic, copy) NSString *metadataItemValue;
@property (nonatomic, copy) NSString *designerVersion;

- (id)initWithWorkflowPath:(NSString *)relativePath;
- (BOOL)loadWorkflow;
- (BOOL)isAutomatedTestingWF;

@end
