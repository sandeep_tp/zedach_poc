//
//  WidgetMessage.h
//  Widgets
//
//  Created by Alexey Piterkin on 2/9/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WidgetXmlParser.h"
#import "WidgetScreenController.h"
#import "ScreenDefinition.h"

// Version 1 is original format of workflow messages
#define kWFMsgVer1 1
// Version 2 is the compact format introduced with 2.1.1
#define kWFMsgVer2 2

// These match the long and compact strings below
enum WFTags
{
   kWidgetMessageTag = 0,
   kWorklowMessageTag,
   kHeaderTag, 
   kScreenTag, 
   kValuesTag, 
   kValueTag, 
   kActionTag, 
   kTypeTag, 
   kKeyTag, 
   kNullTag,
   kStateTag, 
   kTextTag, 
   kListTag, 
   kFileTag    
};

@class WidgetDefinition, MessageParser;

@interface WorkflowMsgDict : NSObject

+ (int) getVersion:(NSString*)msg;
+ (NSString*) getTag:(int)version withTag:(int)tag;

@end

@interface RowData : NSObject
{
   NSMutableDictionary* record;
   NSMutableDictionary* types;
   NSMutableDictionary* optionLists;
}

@property (nonatomic, retain) NSMutableDictionary* record;
@property (nonatomic, retain) NSMutableDictionary* types;
@property (nonatomic, retain) NSMutableDictionary* optionLists;

@end

@interface ListRows : NSObject
{
   NSMutableDictionary* rows;
   NSMutableDictionary* states;
   NSMutableArray* indexedKeys; // Contains keyname of each row at specific index. This key can be used to access RowData and state of the row.
}

@property (nonatomic, readonly, retain) NSMutableDictionary* rows;
@property (nonatomic, readonly, retain) NSMutableDictionary* states;
@property (nonatomic, readonly, retain) NSMutableArray* indexedKeys;

@end

@interface WidgetMessage : NSObject <Parseable> {
   WidgetDefinition* widget;
   
   NSString* header;
   NSString* widgetScreen;
   NSString* requestAction;
   
   RowData* data;
   NSMutableDictionary* isNull;
   
   RowData* curData;               // Points to current data while parsing
   NSMutableDictionary* curIsNull; // Points to current isNull while parsing
   
   NSString* key;
   NSMutableString* value;
   
   // Holds pointers to hierarchy of keys/xmltags/data to traverse through the list during message parsing
   NSMutableArray* listKeys;
   NSMutableArray* xmlTags;
   NSMutableArray* listData;
   
   WidgetMessage* original;
   
   WidgetMessageMode mode;
   
   int version;
}

@property (nonatomic, readonly, retain) RowData* data;
@property (nonatomic, assign) NSMutableDictionary* isNull;
@property (nonatomic, retain) NSString* widgetScreen;
@property (nonatomic) WidgetMessageMode mode;
@property (nonatomic, retain) NSString* requestAction;
@property (nonatomic, assign) int version;
@property (nonatomic, retain) NSString* key;
@property (nonatomic, retain) NSMutableString* value;

// This property should be set to associate processing state version of the message with the message
// that arrived from the server. Note that this property should only be set once, and should always
// be set internally by the widgets engine
@property (nonatomic, retain) WidgetMessage* original;

// These properties are currently only for unit test purposes
@property (nonatomic, readonly, retain) NSString* header;

// PUBLIC API
+ (WidgetMessage*) widgetMessageFromString:(NSString*)body forWidget:(WidgetDefinition*)widget inMode:(WidgetMessageMode)mode error:(NSString**)error;
- (id) initWithWidget:(WidgetDefinition*)aWidget inMode:(WidgetMessageMode)aMode;
- (void) removeRecordDataForKey:(NSString*)aKey;
- (NSString*) xmlForHybrid;

@end
