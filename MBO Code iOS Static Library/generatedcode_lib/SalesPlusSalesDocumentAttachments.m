
/* Copyright (c) Sybase, Inc. 2010-2013   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code;OT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES;
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */

/*
 Generated by Sybase Unwired Platform 
 Compiler version - 3.0.13.40
*/ 

#import "SalesPlusSalesDocumentAttachments.h"
#import "SalesPlusSalesDocumentAttachments+Internal.h"
#import "SalesPlusSalesDocumentAttachmentsMetaData.h"
#import "SUPJsonObject.h"
#import "SUPJsonReader.h"
#import "SalesPlusERPSalesPlus_SuperUserDB.h"
#import "SalesPlusERPSalesPlus_SuperUserDB+Internal.h"
#import "SUPEntityDelegate.h"
#import "SUPEntityMetaDataRBS.h"
#import "SUPQuery.h"
#import "SalesPlusKeyGenerator.h"
#import "SalesPlusLocalKeyGenerator.h"
#import "SalesPlusKeyGenerator+Internal.h"
#import "SalesPlusLocalKeyGenerator+Internal.h"
#import "SalesPlusLogRecordImpl.h"

#import "SalesPlusSalesDocument.h"
#import "SalesPlusSalesDocument+Internal.h"
#import "SUPRelationshipMetaData.h"

@implementation SalesPlusSalesDocumentAttachments


@synthesize VBELN = _VBELN;
@synthesize DESCRIPTION = _DESCRIPTION;
@synthesize LINK = _LINK;
@synthesize SPRAS = _SPRAS;
@synthesize cvpOperation = _cvpOperation;
@synthesize cvpOperationLobs = _cvpOperationLobs;
@synthesize salesDocumentFK = _salesDocumentFK;
@synthesize salesDocumentValid = _salesDocumentValid;
@synthesize surrogateKey = _surrogateKey;
@synthesize cvpOperationLength = _cvpOperationLength;
@synthesize cvpOperationLobsLength = _cvpOperationLobsLength;


#pragma mark -
#pragma mark Init, dealloc, getInstance
- (id) init
{
    if ((self = [super init]))
    {
        self.classMetaData = [SalesPlusSalesDocumentAttachments metaData];
        [self setEntityDelegate:(SUPEntityDelegate*)[SalesPlusSalesDocumentAttachments delegate]];
    }
    return self;    
}
- (void)dealloc
{
    self.isDirty = YES; // So that not to trigger side effects in setDirty
    self.VBELN = nil;
    self.DESCRIPTION = nil;
    self.LINK = nil;
    self.SPRAS = nil;
    self.cvpOperation = nil;
    self.cvpOperationLobs = nil;
    self.salesDocumentFK = nil;
	[super dealloc];
}
+ (SalesPlusSalesDocumentAttachments*)getInstance
{
     SalesPlusSalesDocumentAttachments* me = [[SalesPlusSalesDocumentAttachments alloc] init];
    [me autorelease];
    return me;
}



#pragma mark -
#pragma mark Property getters and setters

- (SalesPlusSalesDocument*)salesDocument
{
    if (_salesDocumentValid == NO)
    {
        if (_salesDocumentFK != nil)
        {
            _salesDocument = [SalesPlusSalesDocument find:(int64_t)[_salesDocumentFK longLongValue]];
        }
        _salesDocumentValid = YES;
    }
    return _salesDocument;
}

- (SalesPlusSalesDocument*)salesDocumentEntity
{
    return [self salesDocument];
}

- (SUPBigString*)cvpOperation
{
	if(self->_cvpOperation == nil)
	{
		self->_cvpOperation = [SalesPlusERPSalesPlus_SuperUserDB createBigString];
		[self->_cvpOperation retain];
		[self->_cvpOperation setString:@"table":@"erpsalesplus_superuser_1_0_salesdocumentattachments"];
		[self->_cvpOperation setString:@"column":@"cvp_operation_header"];
		[self->_cvpOperation setDbClass:[SalesPlusERPSalesPlus_SuperUserDB class]];
		[self->_cvpOperation setMboClass:[SalesPlusSalesDocumentAttachments class]];
		[self->_cvpOperation setRbsMbo:self];
		[self->_cvpOperation setAttributeName:@"cvpOperation"];
		[self->_cvpOperation setBoolean:@"pending":YES];
		[self->_cvpOperation setBoolean:@"allow_pending_state":YES];
	}

    return _cvpOperation;
}

- (SUPBigString*)cvpOperationLobs
{
	if(self->_cvpOperationLobs == nil)
	{
		self->_cvpOperationLobs = [SalesPlusERPSalesPlus_SuperUserDB createBigString];
		[self->_cvpOperationLobs retain];
		[self->_cvpOperationLobs setString:@"table":@"erpsalesplus_superuser_1_0_salesdocumentattachments"];
		[self->_cvpOperationLobs setString:@"column":@"cvp_operation_lobs"];
		[self->_cvpOperationLobs setDbClass:[SalesPlusERPSalesPlus_SuperUserDB class]];
		[self->_cvpOperationLobs setMboClass:[SalesPlusSalesDocumentAttachments class]];
		[self->_cvpOperationLobs setRbsMbo:self];
		[self->_cvpOperationLobs setAttributeName:@"cvpOperationLobs"];
		[self->_cvpOperationLobs setBoolean:@"pending":YES];
		[self->_cvpOperationLobs setBoolean:@"allow_pending_state":YES];
	}

    return _cvpOperationLobs;
}

- (int64_t)surrogateKey
{
    return _surrogateKey;
}

- (void)setVBELN:(NSString*)newVBELN
{
    if (newVBELN != self->_VBELN)
    {
        [self setDirty];
        [self->_VBELN release];
        self->_VBELN = [newVBELN retain];
    }
}

- (void)setDESCRIPTION:(NSString*)newDESCRIPTION
{
    if (newDESCRIPTION != self->_DESCRIPTION)
    {
        [self setDirty];
        [self->_DESCRIPTION release];
        self->_DESCRIPTION = [newDESCRIPTION retain];
    }
}

- (void)setLINK:(NSString*)newLINK
{
    if (newLINK != self->_LINK)
    {
        [self setDirty];
        [self->_LINK release];
        self->_LINK = [newLINK retain];
    }
}

- (void)setSPRAS:(NSString*)newSPRAS
{
    if (newSPRAS != self->_SPRAS)
    {
        [self setDirty];
        [self->_SPRAS release];
        self->_SPRAS = [newSPRAS retain];
    }
}

- (void)setSalesDocument:(SalesPlusSalesDocument*)newSalesDocument
{
    [self setSalesDocument:(SalesPlusSalesDocument*)newSalesDocument :NO];
}

- (void)setSalesDocumentEntity:(SalesPlusSalesDocument*)newSalesDocument
{
    [self setSalesDocument:(SalesPlusSalesDocument*)newSalesDocument :YES];
}

- (void)setSalesDocument:(SalesPlusSalesDocument*)newSalesDocument :(BOOL)fromLoad
{
    if (_salesDocument != newSalesDocument)
    {
        if (_salesDocument != nil )
        {
        	self.isDirty = YES;
        }       
    }
    if (newSalesDocument == nil)
    {
		[_salesDocumentFK release];
        _salesDocumentFK = nil;
    }
    else
    {
        if (!fromLoad)
        {
            // set dirty only if not from loading
            [newSalesDocument setDirty];
        }
        [_salesDocumentFK release];
        _salesDocumentFK = [[NSNumber alloc] initWithLongLong:[newSalesDocument _pk]];
		[_VBELN release];
        _VBELN = [newSalesDocument DOC_NUMBER];
	    [_VBELN retain];
    }
    if (_salesDocument != newSalesDocument)
    {
        
        _salesDocument = newSalesDocument;
        
    }
    _salesDocumentValid = YES;
}

- (void)setCvpOperation:(SUPBigString*)newCvpOperation
{
    if (newCvpOperation != self->_cvpOperation)
    {
        [self setDirty];
        [self->_cvpOperation release];
        self->_cvpOperation = [newCvpOperation retain];
    }
}

- (void)setCvpOperationLobs:(SUPBigString*)newCvpOperationLobs
{
    if (newCvpOperationLobs != self->_cvpOperationLobs)
    {
        [self setDirty];
        [self->_cvpOperationLobs release];
        self->_cvpOperationLobs = [newCvpOperationLobs retain];
    }
}

- (void)setSalesDocumentFK:(NSNumber*)newSalesDocumentFK
{
    if (newSalesDocumentFK != self->_salesDocumentFK)
    {
        [self setDirty];
        [self->_salesDocumentFK release];
        self->_salesDocumentFK = [newSalesDocumentFK retain];
    }
}

- (void)setSalesDocumentValid:(BOOL)newSalesDocumentValid
{
    if (newSalesDocumentValid != self->_salesDocumentValid)
    {
        [self setDirty];
        self->_salesDocumentValid = newSalesDocumentValid;
    }
}

- (void)setSurrogateKey:(int64_t)newSurrogateKey
{
    if (newSurrogateKey != self->_surrogateKey)
    {
        self.isNew = YES;
        self->_surrogateKey = newSurrogateKey;
    }
}

- (void)setCvpOperationLength:(int64_t)newCvpOperationLength
{
    if (newCvpOperationLength != self->_cvpOperationLength)
    {
        [self setDirty];
        self->_cvpOperationLength = newCvpOperationLength;
    }
}

- (void)setCvpOperationLobsLength:(int64_t)newCvpOperationLobsLength
{
    if (newCvpOperationLobsLength != self->_cvpOperationLobsLength)
    {
        [self setDirty];
        self->_cvpOperationLobsLength = newCvpOperationLobsLength;
    }
}

#pragma mark -
#pragma mark Metadata methods

static SUPEntityMetaDataRBS* SalesPlusSalesDocumentAttachments_META_DATA;

+ (SUPEntityMetaDataRBS*)metaData
{
    if (SalesPlusSalesDocumentAttachments_META_DATA == nil) {
		SalesPlusSalesDocumentAttachments_META_DATA = [[SalesPlusSalesDocumentAttachmentsMetaData alloc] init];
	}
	
	return SalesPlusSalesDocumentAttachments_META_DATA;
}

- (SUPClassMetaDataRBS*)getClassMetaData
{
    return [[self class] metaData];
}

#pragma mark -
#pragma mark Clear relationship objects

- (void)clearRelationshipObjects
{
	if(_salesDocument)
	{
		_salesDocument = nil;
	}
	_salesDocumentValid = NO;
}

#pragma mark -
#pragma mark Callback handler getter/setter

+ (NSObject<SUPCallbackHandler>*)callbackHandler
{
	return [[self delegate] callbackHandler];
}

+ (void)registerCallbackHandler:(NSObject<SUPCallbackHandler>*)newCallbackHandler
{
	[[self delegate] registerCallbackHandler:newCallbackHandler];
}
#pragma mark -
#pragma mark find() and related methods
+ (id)allocInitialTraceRequest:(NSString*)method withSkip:(int32_t)skip withTake:(int32_t)take withSql:(NSString*)sql withValues:(SUPObjectList*)values
{
    id request = nil;
    
    CFSCTraceLevel level = [SUPCFSCTrace getTraceLevel];
    NSMutableString *params = [[[NSMutableString alloc] init] autorelease];

    for (id val in values)
    {
        [params appendFormat:@"%@;", [SUPStringUtil toString_object:val]];
    }
    if (skip >=0)
    	[params appendFormat:@"skip=%d;", skip];
    if (take >= 0)
        [params appendFormat:@"take=%d", take];
    if (level == CFSCTraceLevel_DETAIL)
    {
        
        request = [[SUPCFSCTraceDetailRequest alloc] initWithParams:@"SalesPlusSalesDocumentAttachments" :method :nil:nil:[NSDate date] :nil :0
                                                                   :nil :NO :NO :NO :NO :NO :sql :params];
    } else if (level == CFSCTraceLevel_NORMAL)
    {
        request = [[SUPCFSCTraceRequest alloc] initWithParams:@"SalesPlusSalesDocumentAttachments"  :method :nil :nil  :[NSDate date] :nil :0 :params];
    }
    return request;
}
+ (void)finishTraceRequest:(id)request :(int)rows
{
    if (!request)
    {
        return;
    }
    
    SUPCFSCTraceRequest *traceReq = (SUPCFSCTraceRequest *)request;
    
    traceReq.endTime = [NSDate date];
  
    traceReq.count = rows;
    
    [SUPCFSCTrace log:(id)request];
}

+ (SalesPlusSalesDocumentAttachments*)find:(int64_t)id_
{
    SUPCFSCTraceRequest* request = nil;
    NSString *msg = @"success";
    if ([SUPCFSCTrace isEnabled])
    {
        request = [[SUPCFSCTraceRequest alloc] initWithParams:@"SalesPlusSalesDocumentAttachments" :@"find" 
            :nil :[self metaData].synchronizationGroup  :[NSDate date] :nil :0 :[SUPStringUtil toString_object:[NSNumber numberWithLongLong:id_]]];
    } 
    SalesPlusSalesDocumentAttachments* result = nil;
    @try {
         SUPObjectList *keys = [SUPObjectList getInstance];
         [keys add:[NSNumber numberWithLongLong:id_]];
         result = (SalesPlusSalesDocumentAttachments*)[(SUPEntityDelegate*)([[self class] delegate]) findEntityWithKeys:keys];
         return result;
     }
     @catch (NSException *e) {
        msg = [NSString stringWithFormat:@"fail in SalesPlusSalesDocumentAttachments--find: %@:%@", [e name], [e reason]];        
        CFSCTraceLogError(msg);
        @throw e;
    }
    @finally
    {
        if (request)
        {
            request.endTime = [NSDate date];
            if ([msg isEqualToString:@"success"])
                request.count = 1;

            [SUPCFSCTrace log:(id)request];
            [request release];
        }
    }
}

+ (SUPObjectList*)findWithQuery:(SUPQuery*)query
{
    return (SUPObjectList*)[(SUPEntityDelegate*)([[self class] delegate])  findWithQuery:query:[SalesPlusSalesDocumentAttachments class]];
}

- (int64_t)_pk
{
    return (int64_t)[[self i_pk] longLongValue];
}

+ (SalesPlusSalesDocumentAttachments*)load:(int64_t)id_
{
    return (SalesPlusSalesDocumentAttachments*)[(SUPEntityDelegate*)([[self class] delegate]) load:[NSNumber numberWithLongLong:id_]];
}
#pragma mark -
#pragma mark submitPending(), getLastOperation(), state methods

- (SUPString)getLastOperation
{
    if (self.pendingChange == 'C')
    {
        return @"create";
    }
    else if (self.pendingChange == 'D')
    {
        return @"delete";
    }
    else if (self.pendingChange == 'U')
    {
        return @"update";
    }
    return @"";

}
+ (void)submitPendingOperations
{
    [[[self class] delegate] submitPendingOperations];
}

+ (void)cancelPendingOperations
{
    [[[self class] delegate] cancelPendingOperations];
}
- (SalesPlusSalesDocumentAttachments*)getDownloadState
{
    return (SalesPlusSalesDocumentAttachments*)[self i_getDownloadState];
}

- (SalesPlusSalesDocumentAttachments*) getOriginalState
{
    return (SalesPlusSalesDocumentAttachments*)[self i_getOriginalState];
}
+ (SUPObjectList*)getPendingObjects
{
    return (SUPObjectList*)[(SUPEntityDelegate*)[[self class] delegate] getPendingObjects];
}

+ (SUPObjectList*)getPendingObjects:(int32_t)skip take:(int32_t)take
{
    return (SUPObjectList*)[(SUPEntityDelegate*)[[self class] delegate] getPendingObjects:skip:take];
}
#pragma mark -
#pragma mark Log record methods

- (SUPObjectList*)getLogRecords
{
   return [SalesPlusLogRecordImpl findByEntity:@"SalesDocumentAttachments":[self keyToString]];
}




#pragma mark -
#pragma mark Description implemetation

- (NSString*)toString
{
	NSString* str = [NSString stringWithFormat:@"\
	SalesDocumentAttachments = \n\
	    VBELN = %@,\n\
	    DESCRIPTION = %@,\n\
	    LINK = %@,\n\
	    SPRAS = %@,\n\
	    pending = %i,\n\
	    pendingChange = %c,\n\
	    replayPending = %qi,\n\
	    replayFailure = %qi,\n\
	    cvpOperation = %@,\n\
	    cvpOperationLobs = %@,\n\
	    salesDocumentFK = %@,\n\
	    salesDocumentValid = %i,\n\
	    surrogateKey = %qi,\n\
	    replayCounter = %qi,\n\
	    disableSubmit = %i,\n\
	    cvpOperationLength = %qi,\n\
	    cvpOperationLobsLength = %qi,\n\
	    isNew = %i,\n\
        isDirty = %i,\n\
        isDeleted = %i,\n\
	\n"
    	,self.VBELN
    	,self.DESCRIPTION
    	,self.LINK
    	,self.SPRAS
    	,self.pending
    	,self.pendingChange
    	,self.replayPending
    	,self.replayFailure
    	,self.cvpOperation
    	,self.cvpOperationLobs
    	,self.salesDocumentFK
    	,self.salesDocumentValid
    	,self.surrogateKey
    	,self.replayCounter
    	,self.disableSubmit
    	,self.cvpOperationLength
    	,self.cvpOperationLobsLength
		,self.isNew
		,self.isDirty
		,self.isDeleted
	];
	return str;

}

- (NSString*)description
{
	return [self toString];
}




#pragma mark - 
#pragma mark Delegate method (internal)

static SUPEntityDelegate *g_SalesPlusSalesDocumentAttachments_delegate = nil;

+ (SUPEntityDelegate *) delegate
{
	@synchronized(self) {
		if (g_SalesPlusSalesDocumentAttachments_delegate == nil) {
			g_SalesPlusSalesDocumentAttachments_delegate = [[SUPEntityDelegate alloc] initWithName:@"SalesPlusSalesDocumentAttachments" clazz:[self class]
				metaData:[self metaData] dbDelegate:[SalesPlusERPSalesPlus_SuperUserDB delegate] database:[SalesPlusERPSalesPlus_SuperUserDB instance]];
		}
	}
	
	return [[g_SalesPlusSalesDocumentAttachments_delegate retain] autorelease];
}

#pragma mark -
#pragma mark JSON methods (internal)


- (SUPJsonObject*)getAttributeJson:(int)id_
{
    switch(id_)
    {
        case 7739:
             return nil;
        break;
        case 7740:
             return nil;
        break;
        default:
        return [super getAttributeJson:id_];
    }

}
- (void)setAttributeJson:(int)id_ :(SUPJsonObject*)value
{
    switch(id_)
    { 
        case 7739:
             self.cvpOperation = nil;
        break;
        case 7740:
             self.cvpOperationLobs = nil;
        break;
        default:
            [super setAttributeJson:id_:value];
            break;
    }

}

+ (SUPObjectList*)fromJSONList:(SUPJsonArray*)jsonArray
{
    if(jsonArray == nil)
        return nil;
    
    SUPObjectList* instanceList = [[SUPObjectList alloc] initWithCapacity:1];
    [instanceList autorelease];
    if(instanceList == nil)
        return nil;

    for (SUPJsonObject* jsonObject in jsonArray)
    {
        SalesPlusSalesDocumentAttachments* inst = [[SalesPlusSalesDocumentAttachments alloc] init];
        [inst readJson:jsonObject];
        [instanceList add:inst];
        [inst release];
    }
    return instanceList;
}

+ (SUPJsonArray*)toJSONList:(SUPObjectList*)instanceList
{
    SUPJsonArray* jsonArray = [[SUPJsonArray alloc] init];
    [jsonArray autorelease];
    for (SalesPlusSalesDocumentAttachments* inst in instanceList)
    {
        SUPJsonObject *o = [[SUPJsonObject alloc] init];
        [inst writeJson:o];
        [jsonArray add:o];
        [o release];
    }
    return jsonArray;
}

#pragma mark -
#pragma mark Internal attribute get/set methods
-(SUPLong) getAttributeLong:(int)id_
{
    switch(id_)
    {
    case 3802:
        return self.surrogateKey;
    case 10277:
        return self.cvpOperationLength;
    case 10278:
        return self.cvpOperationLobsLength;
    default:
         return [super getAttributeLong:id_];
    }
}

-(void) setAttributeLong:(int)id_ :(SUPLong)v
{
    switch(id_)
    {
    case 3802:
        self.surrogateKey = v;
        break;;
    case 10277:
        self.cvpOperationLength = v;
        break;;
    case 10278:
        self.cvpOperationLobsLength = v;
        break;;
    default:
        [super setAttributeLong:id_:v];
        break;;
    }
}
-(SUPString) getAttributeString:(int)id_
{
    switch(id_)
    {
    case 3798:
        return self.VBELN;
    case 3799:
        return self.DESCRIPTION;
    case 3800:
        return self.LINK;
    case 3801:
        return self.SPRAS;
    default:
         return [super getAttributeString:id_];
    }
}

-(void) setAttributeString:(int)id_ :(SUPString)v
{
    switch(id_)
    {
    case 3798:
        self.VBELN = v;
        break;;
    case 3799:
        self.DESCRIPTION = v;
        break;;
    case 3800:
        self.LINK = v;
        break;;
    case 3801:
        self.SPRAS = v;
        break;;
    default:
        [super setAttributeString:id_:v];
        break;;
    }
}
-(SUPNullableLong) getAttributeNullableLong:(int)id_
{
    switch(id_)
    {
    case 7764:
        return self.salesDocumentFK;
    default:
         return [super getAttributeNullableLong:id_];
    }
}

-(void) setAttributeNullableLong:(int)id_ :(SUPNullableLong)v
{
    switch(id_)
    {
    case 7764:
        self.salesDocumentFK = v;
        break;;
    default:
        [super setAttributeNullableLong:id_:v];
        break;;
    }
}
-(SUPBoolean) getAttributeBoolean:(int)id_
{
    switch(id_)
    {
    case 7768:
        return self.salesDocumentValid;
    default:
         return [super getAttributeBoolean:id_];
    }
}

-(void) setAttributeBoolean:(int)id_ :(SUPBoolean)v
{
    switch(id_)
    {
    case 7768:
        self.salesDocumentValid = v;
        break;;
    default:
        [super setAttributeBoolean:id_:v];
        break;;
    }
}
-(id) getAttributeObject:(int)id_ :(BOOL)loadFromDBIfInvalid
{
    switch(id_)
    {
    case 3803:
        if (loadFromDBIfInvalid)
        {
            return self.salesDocument;
        } else
        {
            return _salesDocument;
        }
    default:
         return [super getAttributeObject:id_];
    }
}

-(void) setAttributeObject:(int)id_ :(id)v
{
    switch(id_)
    {
    case 3803:
        self.salesDocument = v;
        break;;
    default:
        [super setAttributeObject:id_:v];
        break;;
    }
}
- (id)getAttributeLargeObject:(int)id_ loadFromDB:(BOOL)loadFromDB
{
    switch(id_)
    {
    	case 7739:
            if(loadFromDB)
            {
                return self.cvpOperation;
            }
            else
                return nil;
        break;
    	case 7740:
            if(loadFromDB)
            {
                return self.cvpOperationLobs;
            }
            else
                return nil;
        break;
        default:
        return [super getAttributeJson:id_];
    }
}
- (void)setAttributeLargeObject:(int)id_ :(id)value
{
    switch(id_)
    {
    	case 7739:
             self.cvpOperation = nil;
        break;
    	case 7740:
             self.cvpOperationLobs = nil;
        break;
        default:
            [super setAttributeJson:id_:value];
            break;
    }

}



#pragma mark -
#pragma mark Object queries and operation implementations



+ (SUPObjectList*)findAll
{
	return [self findAll:0 take:INT_MAX]; 
}


	

+ (SUPObjectList*)findAll:(int32_t)skip take:(int32_t)take
{
	NSMutableString *sql = nil;
	NSMutableString *_selectSQL = nil;
	_selectSQL = [[[NSMutableString alloc] initWithCapacity:416] autorelease];
	[_selectSQL appendString:@" x.\"a\",x.\"b\",x.\"c\",x.\"d\",x.\"pending\",x.\"_pc\",x.\"_rp\",x.\"_rf\",x.\"salesDocumentFK\",x.\"e\",x.\"_rc\",x.\"_ds\",x.\"cvpOperation_length\",x.\"cvpOperationLobs_length\" FROM \"erpsalesplus_superuser_1_0_salesdocumentattachments\" x where (((x.\"pending\" = 1 or not exists (select x_os.\"e"
	                               "\" from \"erpsalesplus_superuser_1_0_salesdocumentattachments_os\" x_os where x_os.\"e\" = x.\"e\"))))"];
	SUPObjectList* result = nil;
	sql = [[NSMutableString alloc] initWithFormat:@"select %@", _selectSQL];
	[sql autorelease];
	sql = [[NSMutableString alloc] initWithFormat:@"select %@", _selectSQL];
	[sql autorelease];
	SUPStringList *ids = [SUPStringList listWithCapacity:0];
	SUPObjectList *dts = [SUPObjectList getInstance];
	SUPObjectList* values = [SUPObjectList getInstance];
	id request = [[self class] allocInitialTraceRequest:@"findAll" withSkip:skip withTake:take withSql:sql withValues:values ];
	result = (SUPObjectList*)[[[self class] delegate] findWithSQL:sql withDataTypes:dts withValues:values withIDs:ids withSkip:skip withTake:take withClass:[SalesPlusSalesDocumentAttachments class]];
	[[self class] finishTraceRequest:request :[result size]];
	[request release];
	return result;
}



+ (SUPObjectList*)findByVBELN:(NSString*)vBELN
{
	return [self findByVBELN:vBELN skip:0 take:INT_MAX]; 
}


	

+ (SUPObjectList*)findByVBELN:(NSString*)vBELN skip:(int32_t)skip take:(int32_t)take
{
	NSMutableString *sql = nil;
	NSMutableString *_selectSQL = nil;
	_selectSQL = [[[NSMutableString alloc] initWithCapacity:435] autorelease];
	[_selectSQL appendString:@" x.\"a\",x.\"b\",x.\"c\",x.\"d\",x.\"pending\",x.\"_pc\",x.\"_rp\",x.\"_rf\",x.\"salesDocumentFK\",x.\"e\",x.\"_rc\",x.\"_ds\",x.\"cvpOperation_length\",x.\"cvpOperationLobs_length\" FROM \"erpsalesplus_superuser_1_0_salesdocumentattachments\" x WHERE (((x.\"pending\" = 1 or not exists (select x_os.\"e"
	                               "\" from \"erpsalesplus_superuser_1_0_salesdocumentattachments_os\" x_os where x_os.\"e\" = x.\"e\")))) and ( x.\"a\" = ?)"];
	SUPObjectList* result = nil;
	sql = [[NSMutableString alloc] initWithFormat:@"select %@", _selectSQL];
	[sql autorelease];
	SUPStringList *ids = [SUPStringList listWithCapacity:0];
	SUPObjectList *dts = [SUPObjectList getInstance];
	[dts addObject:[SUPDataType forName:@"string"]];
	SUPObjectList* values = [SUPObjectList getInstance];
	[values addObject:vBELN];
	id request = [[self class] allocInitialTraceRequest:@"findByVBELN" withSkip:skip withTake:take withSql:sql withValues:values ];
	result = (SUPObjectList*)[[[self class] delegate] findWithSQL:sql withDataTypes:dts withValues:values withIDs:ids withSkip:skip withTake:take withClass:[SalesPlusSalesDocumentAttachments class]];
	[[self class] finishTraceRequest:request :[result size]];
	[request release];
	return result;
}



+ (SUPObjectList*)findByLINK:(NSString*)lINK
{
	return [self findByLINK:lINK skip:0 take:INT_MAX]; 
}


	

+ (SUPObjectList*)findByLINK:(NSString*)lINK skip:(int32_t)skip take:(int32_t)take
{
	NSMutableString *sql = nil;
	NSMutableString *_selectSQL = nil;
	_selectSQL = [[[NSMutableString alloc] initWithCapacity:435] autorelease];
	[_selectSQL appendString:@" x.\"a\",x.\"b\",x.\"c\",x.\"d\",x.\"pending\",x.\"_pc\",x.\"_rp\",x.\"_rf\",x.\"salesDocumentFK\",x.\"e\",x.\"_rc\",x.\"_ds\",x.\"cvpOperation_length\",x.\"cvpOperationLobs_length\" FROM \"erpsalesplus_superuser_1_0_salesdocumentattachments\" x WHERE (((x.\"pending\" = 1 or not exists (select x_os.\"e"
	                               "\" from \"erpsalesplus_superuser_1_0_salesdocumentattachments_os\" x_os where x_os.\"e\" = x.\"e\")))) and ( x.\"c\" = ?)"];
	SUPObjectList* result = nil;
	sql = [[NSMutableString alloc] initWithFormat:@"select %@", _selectSQL];
	[sql autorelease];
	SUPStringList *ids = [SUPStringList listWithCapacity:0];
	SUPObjectList *dts = [SUPObjectList getInstance];
	[dts addObject:[SUPDataType forName:@"string"]];
	SUPObjectList* values = [SUPObjectList getInstance];
	[values addObject:lINK];
	id request = [[self class] allocInitialTraceRequest:@"findByLINK" withSkip:skip withTake:take withSql:sql withValues:values ];
	result = (SUPObjectList*)[[[self class] delegate] findWithSQL:sql withDataTypes:dts withValues:values withIDs:ids withSkip:skip withTake:take withClass:[SalesPlusSalesDocumentAttachments class]];
	[[self class] finishTraceRequest:request :[result size]];
	[request release];
	return result;
}



+ (SUPObjectList*)findBySPRAS:(NSString*)sPRAS
{
	return [self findBySPRAS:sPRAS skip:0 take:INT_MAX]; 
}


	

+ (SUPObjectList*)findBySPRAS:(NSString*)sPRAS skip:(int32_t)skip take:(int32_t)take
{
	NSMutableString *sql = nil;
	NSMutableString *_selectSQL = nil;
	_selectSQL = [[[NSMutableString alloc] initWithCapacity:435] autorelease];
	[_selectSQL appendString:@" x.\"a\",x.\"b\",x.\"c\",x.\"d\",x.\"pending\",x.\"_pc\",x.\"_rp\",x.\"_rf\",x.\"salesDocumentFK\",x.\"e\",x.\"_rc\",x.\"_ds\",x.\"cvpOperation_length\",x.\"cvpOperationLobs_length\" FROM \"erpsalesplus_superuser_1_0_salesdocumentattachments\" x WHERE (((x.\"pending\" = 1 or not exists (select x_os.\"e"
	                               "\" from \"erpsalesplus_superuser_1_0_salesdocumentattachments_os\" x_os where x_os.\"e\" = x.\"e\")))) and ( x.\"d\" = ?)"];
	SUPObjectList* result = nil;
	sql = [[NSMutableString alloc] initWithFormat:@"select %@", _selectSQL];
	[sql autorelease];
	SUPStringList *ids = [SUPStringList listWithCapacity:0];
	SUPObjectList *dts = [SUPObjectList getInstance];
	[dts addObject:[SUPDataType forName:@"string"]];
	SUPObjectList* values = [SUPObjectList getInstance];
	[values addObject:sPRAS];
	id request = [[self class] allocInitialTraceRequest:@"findBySPRAS" withSkip:skip withTake:take withSql:sql withValues:values ];
	result = (SUPObjectList*)[[[self class] delegate] findWithSQL:sql withDataTypes:dts withValues:values withIDs:ids withSkip:skip withTake:take withClass:[SalesPlusSalesDocumentAttachments class]];
	[[self class] finishTraceRequest:request :[result size]];
	[request release];
	return result;
}



+ (SalesPlusSalesDocumentAttachments*)findByPrimaryKey:(NSString*)vBELN withLINK:(NSString*)lINK withSPRAS:(NSString*)sPRAS
{
	NSMutableString *sql = nil;
	NSMutableString *_selectSQL = nil;
	_selectSQL = [[[NSMutableString alloc] initWithCapacity:473] autorelease];
	[_selectSQL appendString:@"SELECT x.\"a\",x.\"b\",x.\"c\",x.\"d\",x.\"pending\",x.\"_pc\",x.\"_rp\",x.\"_rf\",x.\"salesDocumentFK\",x.\"e\",x.\"_rc\",x.\"_ds\",x.\"cvpOperation_length\",x.\"cvpOperationLobs_length\" FROM \"erpsalesplus_superuser_1_0_salesdocumentattachments\" x WHERE (((x.\"pending\" = 1 or not exists (select x"
	                               "_os.\"e\" from \"erpsalesplus_superuser_1_0_salesdocumentattachments_os\" x_os where x_os.\"e\" = x.\"e\")))) and ( x.\"a\" = ? AND x.\"c\" = ? AND x.\"d\" = ?)"];
	sql = [[NSMutableString alloc] initWithFormat:@"%@", _selectSQL];
	[sql autorelease];
	SUPStringList *ids = [SUPStringList listWithCapacity:0];
	SUPObjectList *dts = [SUPObjectList getInstance];
	[dts addObject:[SUPDataType forName:@"string"]];
	[dts addObject:[SUPDataType forName:@"string"]];
	[dts addObject:[SUPDataType forName:@"string"]];
	SUPObjectList* values = [SUPObjectList getInstance];
	[values addObject:vBELN];
	[values addObject:lINK];
	[values addObject:sPRAS];
	
	id request = [[self class] allocInitialTraceRequest:@"findByPrimaryKey" withSkip:-1 withTake:-1 withSql:sql withValues:values ];	
	SUPObjectList *result = (SUPObjectList*)[[[self class] delegate] findWithSQL:sql withDataTypes:dts withValues:values withIDs:ids withClass:[SalesPlusSalesDocumentAttachments class]];
	[[self class] finishTraceRequest:request :[result size]];
	[request release];
	if(result && ([result size] > 0))
	{   
		SalesPlusSalesDocumentAttachments* cus = (SalesPlusSalesDocumentAttachments*)[result item:0];
	    return cus;
	}
	else
	    return nil;
}



+ (SUPObjectList*)findByVbelnSpras:(NSString*)vBELN withSPRAS:(NSString*)sPRAS
{
	return [self findByVbelnSpras:vBELN withSPRAS:sPRAS skip:0 take:INT_MAX]; 
}


	

+ (SUPObjectList*)findByVbelnSpras:(NSString*)vBELN withSPRAS:(NSString*)sPRAS skip:(int32_t)skip take:(int32_t)take
{
	NSMutableString *sql = nil;
	NSMutableString *_selectSQL = nil;
	_selectSQL = [[[NSMutableString alloc] initWithCapacity:451] autorelease];
	[_selectSQL appendString:@" x.\"a\",x.\"b\",x.\"c\",x.\"d\",x.\"pending\",x.\"_pc\",x.\"_rp\",x.\"_rf\",x.\"salesDocumentFK\",x.\"e\",x.\"_rc\",x.\"_ds\",x.\"cvpOperation_length\",x.\"cvpOperationLobs_length\" FROM \"erpsalesplus_superuser_1_0_salesdocumentattachments\" x WHERE (((x.\"pending\" = 1 or not exists (select x_os.\"e"
	                               "\" from \"erpsalesplus_superuser_1_0_salesdocumentattachments_os\" x_os where x_os.\"e\" = x.\"e\")))) and ( x.\"a\" = ? AND x.\"d\" = ?)"];
	SUPObjectList* result = nil;
	sql = [[NSMutableString alloc] initWithFormat:@"select %@", _selectSQL];
	[sql autorelease];
	SUPStringList *ids = [SUPStringList listWithCapacity:0];
	SUPObjectList *dts = [SUPObjectList getInstance];
	[dts addObject:[SUPDataType forName:@"string"]];
	[dts addObject:[SUPDataType forName:@"string"]];
	SUPObjectList* values = [SUPObjectList getInstance];
	[values addObject:vBELN];
	[values addObject:sPRAS];
	id request = [[self class] allocInitialTraceRequest:@"findByVbelnSpras" withSkip:skip withTake:take withSql:sql withValues:values ];
	result = (SUPObjectList*)[[[self class] delegate] findWithSQL:sql withDataTypes:dts withValues:values withIDs:ids withSkip:skip withTake:take withClass:[SalesPlusSalesDocumentAttachments class]];
	[[self class] finishTraceRequest:request :[result size]];
	[request release];
	return result;
}



+ (SUPObjectList*)getSalesDocumentAttachmentss_for_SalesDocument:(NSNumber*)surrogateKey
{
	return [self getSalesDocumentAttachmentss_for_SalesDocument:surrogateKey skip:0 take:INT_MAX]; 
}


	

+ (SUPObjectList*)getSalesDocumentAttachmentss_for_SalesDocument:(NSNumber*)surrogateKey skip:(int32_t)skip take:(int32_t)take
{
	NSMutableString *sql = nil;
	NSMutableString *_selectSQL = nil;
	_selectSQL = [[[NSMutableString alloc] initWithCapacity:447] autorelease];
	[_selectSQL appendString:@" x.\"a\",x.\"b\",x.\"c\",x.\"d\",x.\"pending\",x.\"_pc\",x.\"_rp\",x.\"_rf\",x.\"salesDocumentFK\",x.\"e\",x.\"_rc\",x.\"_ds\",x.\"cvpOperation_length\",x.\"cvpOperationLobs_length\" from \"erpsalesplus_superuser_1_0_salesdocumentattachments\" x where (((x.\"pending\" = 1 or not exists (select x_os.\"e"
	                               "\" from \"erpsalesplus_superuser_1_0_salesdocumentattachments_os\" x_os where x_os.\"e\" = x.\"e\")))) and ( x.\"salesDocumentFK\"=?)"];
	SUPObjectList* result = nil;
	sql = [[NSMutableString alloc] initWithFormat:@"select %@", _selectSQL];
	[sql autorelease];
	SUPStringList *ids = [SUPStringList listWithCapacity:0];
	SUPObjectList *dts = [SUPObjectList getInstance];
	[dts addObject:[SUPDataType forName:@"long?"]];
	SUPObjectList* values = [SUPObjectList getInstance];
	[values addObject:surrogateKey];
	id request = [[self class] allocInitialTraceRequest:@"getSalesDocumentAttachmentss_for_SalesDocument" withSkip:skip withTake:take withSql:sql withValues:values ];
	result = (SUPObjectList*)[[[self class] delegate] findWithSQL:sql withDataTypes:dts withValues:values withIDs:ids withSkip:skip withTake:take withClass:[SalesPlusSalesDocumentAttachments class]];
	[[self class] finishTraceRequest:request :[result size]];
	[request release];
	return result;
}

/*!
  @method
  @abstract Generated class method 
  @param query
  @throws SUPPersistenceException
 */
+ (int32_t)getSize:(SUPQuery*)query
{
    return [(SUPEntityDelegate*)([[self class] delegate]) getSize:query];
}

@end