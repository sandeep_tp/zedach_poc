/*
Copyright (c) Sybase, Inc. 2012 All rights reserved. 

In addition to the license terms set out in the Sybase License Agreement for 
the Sybase Unwired Platform ("Program"), the following additional or different 
rights and accompanying obligations and restrictions shall apply to the source 
code in this file ("Code"). Sybase grants you a limited, non-exclusive, 
non-transferable, revocable license to use, reproduce, and modify the Code 
solely for purposes of (i) maintaining the Code as reference material to better 
understand the operation of the Program, and (ii) development and testing of 
applications created in connection with your licensed use of the Program. 
The Code may not be transferred, sold, assigned, sublicensed or otherwise 
conveyed (whether by operation of law or otherwise) to another party without 
Sybase's prior written consent. The following provisions shall apply to any 
modifications you make to the Code: (i) Sybase will not provide any maintenance 
or support for modified Code or problems that result from use of modified Code; 
(ii) Sybase expressly disclaims any warranties and conditions, express or 
implied, relating to modified Code or any problems that result from use of the 
modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING 
TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN 
IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
to indemnify, hold harmless, and defend Sybase from and against any claims or 
lawsuits, including attorney's fees, that arise from or are related to the 
modified Code or from use of the modified Code.
*/

/******************************************************************************
*    Copyright 2012 Sybase, Inc
*    Source File            : moDBCommon.h
*    Platform Dependencies  :
*    Description            : Header file db definitions.
*    Notes                  :
******************************************************************************/

#ifndef mo_db_common_H_INCLUDED
#define mo_db_common_H_INCLUDED


#ifdef ESI_PALM
   #define MO_CREATOR_ID       'mo'
#endif

// This enum cannot exceed 256 items.
// The tranport casts the item to a UI8.
typedef enum MOFieldType
{
   ftAutoInc=0,
   ftCurrency=1,
   ftString=2,
   ftBinary=3,
   ftLong=4,
   ftULong=5,
   ftShort=6,
   ftUShort=7,
   ftSingle=8,
   ftDouble=9,
   ftDateTime=10,
   ftBoolean=11,
   ftDecimal=12, //dtDecimal is only used for .net client, 
			  //C++ clients shall never use this type in their apps
   
   ftUnknown=13,
   ftBytes=14,
   ftByte=15,
   ftSByte=16,
   ftInt64=17,
   ftUInt64=18,
   ftGuid =19
}MOFieldType;


typedef enum  MOAppendFromOption
{
   afAll,
   afCurrentRecordOnly

}MOAppendFromOption;


typedef enum MOSaveAsOption
{
   soAppendIfExists,
   soDeleteIfExists
}MOSaveAsOption;

typedef enum MOCursorLocation
{
   clUseLocalStore,
   clUseInMemory
}MOCursorLocation;

typedef enum MOTableLifetime
{
   rlPermanent,
   rlTemporary
} MOTableLifetime;

typedef enum MOState
{
   dsInactive,
   dsBrowse,
   dsEdit,
   dsAppend
}MOState;

typedef enum MOCopyOption
{
   coStructureOnly,
   coStructureAndData
} MOCopyOption;

#endif // mo_db_common_H_INCLUDED
