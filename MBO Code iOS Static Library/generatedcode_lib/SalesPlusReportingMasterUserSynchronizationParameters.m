
/* Copyright (c) Sybase, Inc. 2010-2013   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code;OT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES;
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */

/*
 Generated by Sybase Unwired Platform 
 Compiler version - 3.0.13.40
*/ 

#import "SalesPlusReportingMasterUserSynchronizationParameters.h"
#import "SalesPlusReportingMasterUserSynchronizationParameters+Internal.h"
#import "SalesPlusReportingMasterUserSynchronizationParametersMetaData.h"
#import "SUPJsonObject.h"
#import "SUPJsonReader.h"
#import "SalesPlusERPSalesPlus_SuperUserDB.h"
#import "SalesPlusERPSalesPlus_SuperUserDB+Internal.h"
#import "SUPSyncParamEntityDelegate.h"
#import "SUPEntityMetaDataRBS.h"
#import "SUPQuery.h"
#import "SalesPlusKeyGenerator.h"
#import "SalesPlusLocalKeyGenerator.h"
#import "SalesPlusKeyGenerator+Internal.h"
#import "SalesPlusLocalKeyGenerator+Internal.h"
#import "SalesPlusLogRecordImpl.h"
#import "SalesPlusReportingMasterUser_pull_pq.h"
#import "SalesPlusReportingMasterUser_pull_pq+Internal.h"
#import "SalesPlusPersonalizationParameters.h"

#import "SalesPlusPersonalizationParameters.h"

@implementation SalesPlusReportingMasterUserSynchronizationParameters


@synthesize UserID = _UserID;
@synthesize UserIDUserDefined = _UserIDUserDefined;


#pragma mark -
#pragma mark Init, dealloc, getInstance
- (id) init
{
    if ((self = [super init]))
    {
        self.classMetaData = [SalesPlusReportingMasterUserSynchronizationParameters metaData];
        [self setEntityDelegate:(SUPEntityDelegate*)[SalesPlusReportingMasterUserSynchronizationParameters delegate]];
		self.persistentQueryDelegate = [SalesPlusReportingMasterUser_pull_pq getEntityDelegateForPQ];
        self.UserIDUserDefined = NO;
    }
    return self;    
}
- (void)dealloc
{
    self.isDirty = YES; // So that not to trigger side effects in setDirty
    self.UserID = nil;
	[super dealloc];
}
+ (SalesPlusReportingMasterUserSynchronizationParameters*)getInstance
{
     SalesPlusReportingMasterUserSynchronizationParameters* me = [[SalesPlusReportingMasterUserSynchronizationParameters alloc] init];
    [me autorelease];
    return me;
}



#pragma mark -
#pragma mark Property getters and setters

- (void)setUserID:(NSString*)newUserID
{
    if (newUserID != self->_UserID)
    {
        [self setDirty];
        [self->_UserID release];
        self->_UserID = [newUserID retain];
		_UserIDUserDefined = YES;
    }
    else {
        if ((newUserID == nil) == (self->_UserID == nil))
        {
            if (_UserIDUserDefined == NO)
            {
                [self setDirty];
                _UserIDUserDefined = YES;
            }
        }
    }
}

- (void)setUserIDUserDefined:(BOOL)newUserIDUserDefined
{
    if (newUserIDUserDefined != self->_UserIDUserDefined)
    {
        [self setDirty];
        self->_UserIDUserDefined = newUserIDUserDefined;
    }
}

#pragma mark -
#pragma mark Metadata methods

static SUPEntityMetaDataRBS* SalesPlusReportingMasterUserSynchronizationParameters_META_DATA;

+ (SUPEntityMetaDataRBS*)metaData
{
    if (SalesPlusReportingMasterUserSynchronizationParameters_META_DATA == nil) {
		SalesPlusReportingMasterUserSynchronizationParameters_META_DATA = [[SalesPlusReportingMasterUserSynchronizationParametersMetaData alloc] init];
	}
	
	return SalesPlusReportingMasterUserSynchronizationParameters_META_DATA;
}

- (SUPClassMetaDataRBS*)getClassMetaData
{
    return [[self class] metaData];
}

#pragma mark -
#pragma mark Clear relationship objects

- (void)clearRelationshipObjects
{
}

#pragma mark -
#pragma mark find() and related methods
+ (id)allocInitialTraceRequest:(NSString*)method withSkip:(int32_t)skip withTake:(int32_t)take withSql:(NSString*)sql withValues:(SUPObjectList*)values
{
    id request = nil;
    
    CFSCTraceLevel level = [SUPCFSCTrace getTraceLevel];
    NSMutableString *params = [[[NSMutableString alloc] init] autorelease];

    for (id val in values)
    {
        [params appendFormat:@"%@;", [SUPStringUtil toString_object:val]];
    }
    if (skip >=0)
    	[params appendFormat:@"skip=%d;", skip];
    if (take >= 0)
        [params appendFormat:@"take=%d", take];
    if (level == CFSCTraceLevel_DETAIL)
    {
        
        request = [[SUPCFSCTraceDetailRequest alloc] initWithParams:@"SalesPlusReportingMasterUserSynchronizationParameters" :method :nil:nil:[NSDate date] :nil :0
                                                                   :nil :NO :NO :NO :NO :NO :sql :params];
    } else if (level == CFSCTraceLevel_NORMAL)
    {
        request = [[SUPCFSCTraceRequest alloc] initWithParams:@"SalesPlusReportingMasterUserSynchronizationParameters"  :method :nil :nil  :[NSDate date] :nil :0 :params];
    }
    return request;
}
+ (void)finishTraceRequest:(id)request :(int)rows
{
    if (!request)
    {
        return;
    }
    
    SUPCFSCTraceRequest *traceReq = (SUPCFSCTraceRequest *)request;
    
    traceReq.endTime = [NSDate date];
  
    traceReq.count = rows;
    
    [SUPCFSCTrace log:(id)request];
}

+ (SalesPlusReportingMasterUserSynchronizationParameters*)find:(NSString*)id_
{
    SUPCFSCTraceRequest* request = nil;
    NSString *msg = @"success";
    if ([SUPCFSCTrace isEnabled])
    {
        request = [[SUPCFSCTraceRequest alloc] initWithParams:@"SalesPlusReportingMasterUserSynchronizationParameters" :@"find" 
            :nil :[self metaData].synchronizationGroup  :[NSDate date] :nil :0 :[SUPStringUtil toString_object:id_]];
    } 
    SalesPlusReportingMasterUserSynchronizationParameters* result = nil;
    @try {
         SUPObjectList *keys = [SUPObjectList getInstance];
         [keys add:id_];
         result = (SalesPlusReportingMasterUserSynchronizationParameters*)[(SUPEntityDelegate*)([[self class] delegate]) findEntityWithKeys:keys];
         return result;
     }
     @catch (NSException *e) {
        msg = [NSString stringWithFormat:@"fail in SalesPlusReportingMasterUserSynchronizationParameters--find: %@:%@", [e name], [e reason]];        
        CFSCTraceLogError(msg);
        @throw e;
    }
    @finally
    {
        if (request)
        {
            request.endTime = [NSDate date];
            if ([msg isEqualToString:@"success"])
                request.count = 1;

            [SUPCFSCTrace log:(id)request];
            [request release];
        }
    }
}

- (NSString*)_pk
{
    return (NSString*)[self i_pk];
}

+ (SalesPlusReportingMasterUserSynchronizationParameters*)load:(NSString*)id_
{
    return (SalesPlusReportingMasterUserSynchronizationParameters*)[(SUPEntityDelegate*)([[self class] delegate]) load:id_];
}




#pragma mark -
#pragma mark Description implemetation

- (NSString*)toString
{
	NSString* str = [NSString stringWithFormat:@"\
	ReportingMasterUserSynchronizationParameters = \n\
	    UserID = %@,\n\
	    UserIDUserDefined = %i,\n\
	    size_sp = %i,\n\
	    user_sp = %@,\n\
	    isNew = %i,\n\
        isDirty = %i,\n\
        isDeleted = %i,\n\
	\n"
    	,self.UserID
    	,self.UserIDUserDefined
    	,self.size_sp
    	,self.user_sp
		,self.isNew
		,self.isDirty
		,self.isDeleted
	];
	return str;

}

- (NSString*)description
{
	return [self toString];
}




#pragma mark - 
#pragma mark Delegate method (internal)

static SUPSyncParamEntityDelegate *g_SalesPlusReportingMasterUserSynchronizationParameters_delegate = nil;

+ (SUPSyncParamEntityDelegate *) delegate
{
	@synchronized(self) {
		if (g_SalesPlusReportingMasterUserSynchronizationParameters_delegate == nil) {
			g_SalesPlusReportingMasterUserSynchronizationParameters_delegate = [[SUPSyncParamEntityDelegate alloc] initWithName:@"SalesPlusReportingMasterUserSynchronizationParameters" clazz:[self class]
				metaData:[self metaData] dbDelegate:[SalesPlusERPSalesPlus_SuperUserDB delegate] database:[SalesPlusERPSalesPlus_SuperUserDB instance]];
		}
	}
	
	return [[g_SalesPlusReportingMasterUserSynchronizationParameters_delegate retain] autorelease];
}

#pragma mark -
#pragma mark JSON methods (internal)


- (SUPJsonObject*)getAttributeJson:(int)id_
{
    switch(id_)
    {
        default:
        return [super getAttributeJson:id_];
    }

}
- (void)setAttributeJson:(int)id_ :(SUPJsonObject*)value
{
    switch(id_)
    { 
        default:
            [super setAttributeJson:id_:value];
            break;
    }

}

+ (SUPObjectList*)fromJSONList:(SUPJsonArray*)jsonArray
{
    if(jsonArray == nil)
        return nil;
    
    SUPObjectList* instanceList = [[SUPObjectList alloc] initWithCapacity:1];
    [instanceList autorelease];
    if(instanceList == nil)
        return nil;

    for (SUPJsonObject* jsonObject in jsonArray)
    {
        SalesPlusReportingMasterUserSynchronizationParameters* inst = [[SalesPlusReportingMasterUserSynchronizationParameters alloc] init];
        [inst readJson:jsonObject];
        [instanceList add:inst];
        [inst release];
    }
    return instanceList;
}

+ (SUPJsonArray*)toJSONList:(SUPObjectList*)instanceList
{
    SUPJsonArray* jsonArray = [[SUPJsonArray alloc] init];
    [jsonArray autorelease];
    for (SalesPlusReportingMasterUserSynchronizationParameters* inst in instanceList)
    {
        SUPJsonObject *o = [[SUPJsonObject alloc] init];
        [inst writeJson:o];
        [jsonArray add:o];
        [o release];
    }
    return jsonArray;
}

#pragma mark -
#pragma mark Internal attribute get/set methods
-(SUPString) getAttributeNullableString:(int)id_
{
    switch(id_)
    {
    case 5361:
        return self.UserID;
    default:
         return [super getAttributeNullableString:id_];
    }
}

-(void) setAttributeNullableString:(int)id_ :(SUPString)v
{
    switch(id_)
    {
    case 5361:
        self.UserID = v;
        break;;
    default:
        [super setAttributeNullableString:id_:v];
        break;;
    }
}
-(SUPString) getAttributeString:(int)id_
{
    switch(id_)
    {
    case 5363:
        return self.user_sp;
    default:
         return [super getAttributeString:id_];
    }
}

-(void) setAttributeString:(int)id_ :(SUPString)v
{
    switch(id_)
    {
    case 5363:
        self.user_sp = v;
        break;;
    default:
        [super setAttributeString:id_:v];
        break;;
    }
}
-(SUPInt) getAttributeInt:(int)id_
{
    switch(id_)
    {
    case 5364:
        return self.size_sp;
    default:
         return [super getAttributeInt:id_];
    }
}

-(void) setAttributeInt:(int)id_ :(SUPInt)v
{
    switch(id_)
    {
    case 5364:
        self.size_sp = v;
        break;;
    default:
        [super setAttributeInt:id_:v];
        break;;
    }
}
-(SUPBoolean) getAttributeBoolean:(int)id_
{
    switch(id_)
    {
    case 5362:
        return self.UserIDUserDefined;
    default:
         return [super getAttributeBoolean:id_];
    }
}

-(void) setAttributeBoolean:(int)id_ :(SUPBoolean)v
{
    switch(id_)
    {
    case 5362:
        self.UserIDUserDefined = v;
        break;;
    default:
        [super setAttributeBoolean:id_:v];
        break;;
    }
}
- (id)getAttributeLargeObject:(int)id_ loadFromDB:(BOOL)loadFromDB
{
    switch(id_)
    {
        default:
        return [super getAttributeJson:id_];
    }
}
- (void)setAttributeLargeObject:(int)id_ :(id)value
{
    switch(id_)
    {
        default:
            [super setAttributeJson:id_:value];
            break;
    }

}



#pragma mark -
#pragma mark Object queries and operation implementations

@end