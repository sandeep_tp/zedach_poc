
/* Copyright (c) Sybase, Inc. 2010-2013   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code;OT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES;
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */

/*
 Generated by Sybase Unwired Platform 
 Compiler version - 3.0.13.40
*/ 

#import "SalesPlusContactHeaderCreateMetaData.h"

#import "SUPParameterMetaData.h"
#import "SUPAttributeMap.h"
#import "SUPObjectList.h"
#import "SUPClassMap.h"
#import "SUPEntityMap.h"
#import "SalesPlusContactHeaderCreate.h"
#import "SUPDataType.h"
#import "SUPAttributeMetaDataRBS.h"

@implementation SalesPlusContactHeaderCreateMetaData

+ (SalesPlusContactHeaderCreateMetaData*)getInstance
{
    return [[[SalesPlusContactHeaderCreateMetaData alloc] init] autorelease];
}

- (id)init
{
	if (self = [super init]) {
		self.name = @"ContactHeaderCreate";
		self.klass = [SalesPlusContactHeaderCreate class];
 
		SUPObjectList *attributes = [SUPObjectList listWithCapacity:7];
		SUPAttributeMetaDataRBS* a_PARNR = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			43:
			[SUPDataType forName:@"string"]:@"varchar(10)":@"PARNR":@"":@"":
			@"":10:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_PARNR];
		SUPAttributeMetaDataRBS* a_KUNNR = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			44:
			[SUPDataType forName:@"string"]:@"varchar(10)":@"KUNNR":@"":@"":
			@"":10:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_KUNNR];
		SUPAttributeMetaDataRBS* a_NAMEV = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			45:
			[SUPDataType forName:@"string"]:@"varchar(35)":@"NAMEV":@"":@"":
			@"":35:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_NAMEV];
		SUPAttributeMetaDataRBS* a_NAME1 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			46:
			[SUPDataType forName:@"string"]:@"varchar(35)":@"NAME1":@"":@"":
			@"":35:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_NAME1];
		SUPAttributeMetaDataRBS* a_ORT01 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			47:
			[SUPDataType forName:@"string"]:@"varchar(35)":@"ORT01":@"":@"":
			@"":35:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_ORT01];
		SUPAttributeMetaDataRBS* a_ADRND = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			48:
			[SUPDataType forName:@"string"]:@"varchar(10)":@"ADRND":@"":@"":
			@"":10:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_ADRND];
		SUPAttributeMetaDataRBS* a_ADRNP = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			49:
			[SUPDataType forName:@"string"]:@"varchar(10)":@"ADRNP":@"":@"":
			@"":10:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_ADRNP];
		SUPAttributeMetaDataRBS* a_ABTPA = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			50:
			[SUPDataType forName:@"string"]:@"varchar(12)":@"ABTPA":@"":@"":
			@"":12:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_ABTPA];
		SUPAttributeMetaDataRBS* a_ABTNR = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			51:
			[SUPDataType forName:@"string"]:@"varchar(4)":@"ABTNR":@"":@"":
			@"":4:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_ABTNR];
		SUPAttributeMetaDataRBS* a_UEPAR = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			52:
			[SUPDataType forName:@"string"]:@"varchar(10)":@"UEPAR":@"":@"":
			@"":10:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_UEPAR];
		SUPAttributeMetaDataRBS* a_TELF1 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			53:
			[SUPDataType forName:@"string"]:@"varchar(16)":@"TELF1":@"":@"":
			@"":16:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_TELF1];
		SUPAttributeMetaDataRBS* a_ANRED = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			54:
			[SUPDataType forName:@"string"]:@"varchar(30)":@"ANRED":@"":@"":
			@"":30:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_ANRED];
		SUPAttributeMetaDataRBS* a_PAFKT = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			55:
			[SUPDataType forName:@"string"]:@"varchar(2)":@"PAFKT":@"":@"":
			@"":2:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_PAFKT];
		SUPAttributeMetaDataRBS* a_PARVO = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			56:
			[SUPDataType forName:@"string"]:@"varchar(1)":@"PARVO":@"":@"":
			@"":1:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_PARVO];
		SUPAttributeMetaDataRBS* a_PAVIP = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			57:
			[SUPDataType forName:@"string"]:@"varchar(1)":@"PAVIP":@"":@"":
			@"":1:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_PAVIP];
		SUPAttributeMetaDataRBS* a_PARGE = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			58:
			[SUPDataType forName:@"string"]:@"varchar(1)":@"PARGE":@"":@"":
			@"":1:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_PARGE];
		SUPAttributeMetaDataRBS* a_PARLA = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			59:
			[SUPDataType forName:@"string"]:@"varchar(1)":@"PARLA":@"":@"":
			@"":1:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_PARLA];
		SUPAttributeMetaDataRBS* a_GBDAT = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			60:
			[SUPDataType forName:@"date?"]:@"date":@"GBDAT":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_GBDAT];
		SUPAttributeMetaDataRBS* a_VRTNR = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			61:
			[SUPDataType forName:@"string"]:@"varchar(10)":@"VRTNR":@"":@"":
			@"":10:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_VRTNR];
		SUPAttributeMetaDataRBS* a_BRYTH = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			62:
			[SUPDataType forName:@"string"]:@"varchar(4)":@"BRYTH":@"":@"":
			@"":4:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_BRYTH];
		SUPAttributeMetaDataRBS* a_AKVER = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			63:
			[SUPDataType forName:@"string"]:@"varchar(2)":@"AKVER":@"":@"":
			@"":2:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_AKVER];
		SUPAttributeMetaDataRBS* a_NMAIL = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			64:
			[SUPDataType forName:@"string"]:@"varchar(1)":@"NMAIL":@"":@"":
			@"":1:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_NMAIL];
		SUPAttributeMetaDataRBS* a_PARAU = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			65:
			[SUPDataType forName:@"string"]:@"varchar(40)":@"PARAU":@"":@"":
			@"":40:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_PARAU];
		SUPAttributeMetaDataRBS* a_PARH1 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			66:
			[SUPDataType forName:@"string"]:@"varchar(2)":@"PARH1":@"":@"":
			@"":2:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_PARH1];
		SUPAttributeMetaDataRBS* a_PARH2 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			67:
			[SUPDataType forName:@"string"]:@"varchar(2)":@"PARH2":@"":@"":
			@"":2:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_PARH2];
		SUPAttributeMetaDataRBS* a_PARH3 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			68:
			[SUPDataType forName:@"string"]:@"varchar(2)":@"PARH3":@"":@"":
			@"":2:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_PARH3];
		SUPAttributeMetaDataRBS* a_PARH4 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			69:
			[SUPDataType forName:@"string"]:@"varchar(2)":@"PARH4":@"":@"":
			@"":2:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_PARH4];
		SUPAttributeMetaDataRBS* a_PARH5 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			70:
			[SUPDataType forName:@"string"]:@"varchar(2)":@"PARH5":@"":@"":
			@"":2:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_PARH5];
		SUPAttributeMetaDataRBS* a_MOAB1 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			71:
			[SUPDataType forName:@"time?"]:@"time":@"MOAB1":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_MOAB1];
		SUPAttributeMetaDataRBS* a_MOBI1 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			72:
			[SUPDataType forName:@"time?"]:@"time":@"MOBI1":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_MOBI1];
		SUPAttributeMetaDataRBS* a_MOAB2 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			73:
			[SUPDataType forName:@"time?"]:@"time":@"MOAB2":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_MOAB2];
		SUPAttributeMetaDataRBS* a_MOBI2 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			74:
			[SUPDataType forName:@"time?"]:@"time":@"MOBI2":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_MOBI2];
		SUPAttributeMetaDataRBS* a_DIAB1 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			75:
			[SUPDataType forName:@"time?"]:@"time":@"DIAB1":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_DIAB1];
		SUPAttributeMetaDataRBS* a_DIBI1 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			76:
			[SUPDataType forName:@"time?"]:@"time":@"DIBI1":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_DIBI1];
		SUPAttributeMetaDataRBS* a_DIAB2 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			77:
			[SUPDataType forName:@"time?"]:@"time":@"DIAB2":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_DIAB2];
		SUPAttributeMetaDataRBS* a_DIBI2 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			78:
			[SUPDataType forName:@"time?"]:@"time":@"DIBI2":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_DIBI2];
		SUPAttributeMetaDataRBS* a_MIAB1 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			79:
			[SUPDataType forName:@"time?"]:@"time":@"MIAB1":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_MIAB1];
		SUPAttributeMetaDataRBS* a_MIBI1 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			80:
			[SUPDataType forName:@"time?"]:@"time":@"MIBI1":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_MIBI1];
		SUPAttributeMetaDataRBS* a_MIAB2 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			81:
			[SUPDataType forName:@"time?"]:@"time":@"MIAB2":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_MIAB2];
		SUPAttributeMetaDataRBS* a_MIBI2 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			82:
			[SUPDataType forName:@"time?"]:@"time":@"MIBI2":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_MIBI2];
		SUPAttributeMetaDataRBS* a_DOAB1 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			83:
			[SUPDataType forName:@"time?"]:@"time":@"DOAB1":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_DOAB1];
		SUPAttributeMetaDataRBS* a_DOBI1 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			84:
			[SUPDataType forName:@"time?"]:@"time":@"DOBI1":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_DOBI1];
		SUPAttributeMetaDataRBS* a_DOAB2 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			85:
			[SUPDataType forName:@"time?"]:@"time":@"DOAB2":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_DOAB2];
		SUPAttributeMetaDataRBS* a_DOBI2 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			86:
			[SUPDataType forName:@"time?"]:@"time":@"DOBI2":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_DOBI2];
		SUPAttributeMetaDataRBS* a_FRAB1 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			87:
			[SUPDataType forName:@"time?"]:@"time":@"FRAB1":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_FRAB1];
		SUPAttributeMetaDataRBS* a_FRBI1 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			88:
			[SUPDataType forName:@"time?"]:@"time":@"FRBI1":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_FRBI1];
		SUPAttributeMetaDataRBS* a_FRAB2 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			89:
			[SUPDataType forName:@"time?"]:@"time":@"FRAB2":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_FRAB2];
		SUPAttributeMetaDataRBS* a_FRBI2 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			90:
			[SUPDataType forName:@"time?"]:@"time":@"FRBI2":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_FRBI2];
		SUPAttributeMetaDataRBS* a_SAAB1 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			91:
			[SUPDataType forName:@"time?"]:@"time":@"SAAB1":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_SAAB1];
		SUPAttributeMetaDataRBS* a_SABI1 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			92:
			[SUPDataType forName:@"time?"]:@"time":@"SABI1":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_SABI1];
		SUPAttributeMetaDataRBS* a_SAAB2 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			93:
			[SUPDataType forName:@"time?"]:@"time":@"SAAB2":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_SAAB2];
		SUPAttributeMetaDataRBS* a_SABI2 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			94:
			[SUPDataType forName:@"time?"]:@"time":@"SABI2":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_SABI2];
		SUPAttributeMetaDataRBS* a_SOAB1 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			95:
			[SUPDataType forName:@"time?"]:@"time":@"SOAB1":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_SOAB1];
		SUPAttributeMetaDataRBS* a_SOBI1 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			96:
			[SUPDataType forName:@"time?"]:@"time":@"SOBI1":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_SOBI1];
		SUPAttributeMetaDataRBS* a_SOAB2 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			97:
			[SUPDataType forName:@"time?"]:@"time":@"SOAB2":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_SOAB2];
		SUPAttributeMetaDataRBS* a_SOBI2 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			98:
			[SUPDataType forName:@"time?"]:@"time":@"SOBI2":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_SOBI2];
		SUPAttributeMetaDataRBS* a_PAKN1 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			99:
			[SUPDataType forName:@"string"]:@"varchar(3)":@"PAKN1":@"":@"":
			@"":3:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_PAKN1];
		SUPAttributeMetaDataRBS* a_PAKN2 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			100:
			[SUPDataType forName:@"string"]:@"varchar(3)":@"PAKN2":@"":@"":
			@"":3:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_PAKN2];
		SUPAttributeMetaDataRBS* a_PAKN3 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			101:
			[SUPDataType forName:@"string"]:@"varchar(3)":@"PAKN3":@"":@"":
			@"":3:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_PAKN3];
		SUPAttributeMetaDataRBS* a_PAKN4 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			102:
			[SUPDataType forName:@"string"]:@"varchar(3)":@"PAKN4":@"":@"":
			@"":3:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_PAKN4];
		SUPAttributeMetaDataRBS* a_PAKN5 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			103:
			[SUPDataType forName:@"string"]:@"varchar(3)":@"PAKN5":@"":@"":
			@"":3:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_PAKN5];
		SUPAttributeMetaDataRBS* a_SORTL = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			104:
			[SUPDataType forName:@"string"]:@"varchar(10)":@"SORTL":@"":@"":
			@"":10:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_SORTL];
		SUPAttributeMetaDataRBS* a_FAMST = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			105:
			[SUPDataType forName:@"string"]:@"varchar(1)":@"FAMST":@"":@"":
			@"":1:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_FAMST];
		SUPAttributeMetaDataRBS* a_SPNAM = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			106:
			[SUPDataType forName:@"string"]:@"varchar(10)":@"SPNAM":@"":@"":
			@"":10:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_SPNAM];
		SUPAttributeMetaDataRBS* a_TITEL_AP = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			107:
			[SUPDataType forName:@"string"]:@"varchar(35)":@"TITEL_AP":@"":@"":
			@"":35:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_TITEL_AP];
		SUPAttributeMetaDataRBS* a_ERDAT = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			108:
			[SUPDataType forName:@"date?"]:@"date":@"ERDAT":@"":@"":
			@"":-1:0:0:
			@"null":NO:@"":
			NO:NO:YES:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_ERDAT];
		SUPAttributeMetaDataRBS* a_ERNAM = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			109:
			[SUPDataType forName:@"string"]:@"varchar(12)":@"ERNAM":@"":@"":
			@"":12:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_ERNAM];
		SUPAttributeMetaDataRBS* a_DUEFL = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			110:
			[SUPDataType forName:@"string"]:@"varchar(1)":@"DUEFL":@"":@"":
			@"":1:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_DUEFL];
		SUPAttributeMetaDataRBS* a_LIFNR = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			111:
			[SUPDataType forName:@"string"]:@"varchar(10)":@"LIFNR":@"":@"":
			@"":10:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_LIFNR];
		SUPAttributeMetaDataRBS* a_LOEVM = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			112:
			[SUPDataType forName:@"string"]:@"varchar(1)":@"LOEVM":@"":@"":
			@"":1:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_LOEVM];
		SUPAttributeMetaDataRBS* a_KZHERK = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			113:
			[SUPDataType forName:@"string"]:@"varchar(1)":@"KZHERK":@"":@"":
			@"":1:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_KZHERK];
		SUPAttributeMetaDataRBS* a_ADRNP_2 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			114:
			[SUPDataType forName:@"string"]:@"varchar(10)":@"ADRNP_2":@"":@"":
			@"":10:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_ADRNP_2];
		SUPAttributeMetaDataRBS* a_PRSNR = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			115:
			[SUPDataType forName:@"string"]:@"varchar(10)":@"PRSNR":@"":@"":
			@"":10:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
  		[attributes addThis:a_PRSNR];
 		self.attributes = attributes;
 		
 		SUPAttributeMap * attributeMap = [SUPAttributeMap getInstance];
    	[attributeMap setAttributes:attributes];
    	self.attributeMap = attributeMap;

	}
    return self;
}
@end