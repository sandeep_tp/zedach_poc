
/* Copyright (c) Sybase, Inc. 2010-2013   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code;OT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES;
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */

/*
 Generated by Sybase Unwired Platform 
 Compiler version - 3.0.13.40
*/ 

#import "SalesPlusCustPARH3MetaData.h"

#import "SUPRelationshipMetaData.h"
#import "SUPOperationMetaData.h"
#import "SUPParameterMetaData.h"
#import "SUPIndexMetaData.h"
#import "SUPAttributeMap.h"
#import "SUPOperationMap.h"
#import "SUPClassMap.h"
#import "SUPEntityMap.h"
#import "SUPDataType.h"
#import "SalesPlusCustPARH3.h"
#import "SUPObjectList.h"
#import "SalesPlusERPSalesPlus_SuperUserDBMetaData.h"

@implementation SalesPlusCustPARH3MetaData

+ (SalesPlusCustPARH3MetaData*)getInstance
{
    return [[[SalesPlusCustPARH3MetaData alloc] init] autorelease];
}

- (id)init
{
    if (self = [super init]) {
		self.id = 44;
		self.name = @"CustPARH3";
		self.klass = [SalesPlusCustPARH3 class];
 		self.allowPending = NO;;

		self.isClientOnly = YES;

		SUPObjectList *attributes = [SUPObjectList getInstance];
		SUPAttributeMetaDataRBS* a_SPRAS = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			2461:
			[SUPDataType forName:@"string"]:@"varchar(4)":@"SPRAS":@"":@"a":
			@"":1:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
		[a_SPRAS setColumn:@"a"];
		SUPAttributeMetaDataRBS* a_PARH3 = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			2462:
			[SUPDataType forName:@"string"]:@"varchar(8)":@"PARH3":@"":@"b":
			@"":2:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
		[a_PARH3 setColumn:@"b"];
		SUPAttributeMetaDataRBS* a_VTEXT = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			2463:
			[SUPDataType forName:@"string"]:@"varchar(80)":@"VTEXT":@"":@"c":
			@"":20:0:0:
			@"null":NO:@"":
			NO:NO:NO:NO:NO:NO:
			GeneratedScheme_NONE:
			NO:SUPPersonalizationType_None:NO];
		[a_VTEXT setColumn:@"c"];
		SUPAttributeMetaDataRBS* a_surrogateKey = [SUPAttributeMetaDataRBS attributeMetaDataWith:
			2464:
			[SUPDataType forName:@"long"]:@"decimal(20,0)":@"surrogateKey":@"":@"d":
			@"":-1:0:0:
			@"null":NO:@"":
			YES:NO:NO:NO:NO:NO:
			GeneratedScheme_GLOBAL:
			NO:SUPPersonalizationType_None:NO];
		[a_surrogateKey setColumn:@"d"];
 	
		[attributes addThis:a_SPRAS];
		[attributes addThis:a_PARH3];
		[attributes addThis:a_VTEXT];
		[attributes addThis:a_surrogateKey];
		self.attributes = attributes;
		
		SUPAttributeMap * attributeMap = [SUPAttributeMap getInstance];
   		[attributeMap setAttributes:attributes];
	   	self.attributeMap = attributeMap;

 		SUPOperationMetaData* o_findAll_0 = [SUPOperationMetaData createOperationMetaData:1:(SUPString)@"findAll":[SUPDataType forName:@"CustPARH3*"]:true];
		[o_findAll_0 setIsStatic:YES];
		[o_findAll_0 setIsCreate:NO];
		[o_findAll_0 setIsUpdate:NO];
		[o_findAll_0 setIsDelete:NO]; 		
 		SUPOperationMetaData* o_findBySPRAS_1 = [SUPOperationMetaData createOperationMetaData:2:(SUPString)@"findBySPRAS":[SUPDataType forName:@"CustPARH3*"]:true];
	  	{
			SUPObjectList *parameters_list = nil;
	 		SUPParameterMetaData* p_findBySPRAS_SPRAS = [SUPParameterMetaData createParameterMetaData:1:(SUPString)@"SPRAS":[SUPDataType forName:@"string"]];
 			parameters_list = [SUPObjectList listWithCapacity:1];
			[parameters_list addThis:p_findBySPRAS_SPRAS];
			o_findBySPRAS_1.parameters = parameters_list;
	 	}
		[o_findBySPRAS_1 setIsStatic:YES];
		[o_findBySPRAS_1 setIsCreate:NO];
		[o_findBySPRAS_1 setIsUpdate:NO];
		[o_findBySPRAS_1 setIsDelete:NO]; 		
 		SUPOperationMetaData* o_findByPARH3_1 = [SUPOperationMetaData createOperationMetaData:3:(SUPString)@"findByPARH3":[SUPDataType forName:@"CustPARH3*"]:true];
	  	{
			SUPObjectList *parameters_list = nil;
	 		SUPParameterMetaData* p_findByPARH3_PARH3 = [SUPParameterMetaData createParameterMetaData:1:(SUPString)@"PARH3":[SUPDataType forName:@"string"]];
 			parameters_list = [SUPObjectList listWithCapacity:1];
			[parameters_list addThis:p_findByPARH3_PARH3];
			o_findByPARH3_1.parameters = parameters_list;
	 	}
		[o_findByPARH3_1 setIsStatic:YES];
		[o_findByPARH3_1 setIsCreate:NO];
		[o_findByPARH3_1 setIsUpdate:NO];
		[o_findByPARH3_1 setIsDelete:NO]; 		
 		SUPOperationMetaData* o_findByPrimaryKey_2 = [SUPOperationMetaData createOperationMetaData:4:(SUPString)@"findByPrimaryKey":[SUPDataType forName:@"CustPARH3"]:true];
	  	{
			SUPObjectList *parameters_list = nil;
	 		SUPParameterMetaData* p_findByPrimaryKey_SPRAS = [SUPParameterMetaData createParameterMetaData:1:(SUPString)@"SPRAS":[SUPDataType forName:@"string"]];
	 		SUPParameterMetaData* p_findByPrimaryKey_PARH3 = [SUPParameterMetaData createParameterMetaData:2:(SUPString)@"PARH3":[SUPDataType forName:@"string"]];
 			parameters_list = [SUPObjectList listWithCapacity:2];
			[parameters_list addThis:p_findByPrimaryKey_SPRAS];
			[parameters_list addThis:p_findByPrimaryKey_PARH3];
			o_findByPrimaryKey_2.parameters = parameters_list;
	 	}
		[o_findByPrimaryKey_2 setIsStatic:YES];
		[o_findByPrimaryKey_2 setIsCreate:NO];
		[o_findByPrimaryKey_2 setIsUpdate:NO];
		[o_findByPrimaryKey_2 setIsDelete:NO]; 		
 		SUPOperationMetaData* o_findAllSorted_0 = [SUPOperationMetaData createOperationMetaData:5:(SUPString)@"findAllSorted":[SUPDataType forName:@"CustPARH3*"]:true];
		[o_findAllSorted_0 setIsStatic:YES];
		[o_findAllSorted_0 setIsCreate:NO];
		[o_findAllSorted_0 setIsUpdate:NO];
		[o_findAllSorted_0 setIsDelete:NO]; 		
 		SUPOperationMetaData* o_refresh_0 = [SUPOperationMetaData createOperationMetaData:6:(SUPString)@"refresh":[SUPDataType forName:@"void"]:true];
		[o_refresh_0 setIsStatic:NO];
		[o_refresh_0 setIsCreate:NO];
		[o_refresh_0 setIsUpdate:NO];
		[o_refresh_0 setIsDelete:NO]; 		
 		SUPOperationMetaData* o__pk_0 = [SUPOperationMetaData createOperationMetaData:7:(SUPString)@"_pk":[SUPDataType forName:@"long?"]:true];
		[o__pk_0 setIsStatic:NO];
		[o__pk_0 setIsCreate:NO];
		[o__pk_0 setIsUpdate:NO];
		[o__pk_0 setIsDelete:NO]; 		
 		SUPOperationMetaData* o_findWithQuery_1 = [SUPOperationMetaData createOperationMetaData:8:(SUPString)@"findWithQuery":[SUPDataType forName:@"CustPARH3*"]:true];
	  	{
			SUPObjectList *parameters_list = nil;
	 		SUPParameterMetaData* p_findWithQuery_query = [SUPParameterMetaData createParameterMetaData:1:(SUPString)@"query":[SUPDataType forName:@"com.sybase.persistence.Query"]];
 			parameters_list = [SUPObjectList listWithCapacity:1];
			[parameters_list addThis:p_findWithQuery_query];
			o_findWithQuery_1.parameters = parameters_list;
	 	}
		[o_findWithQuery_1 setIsStatic:YES];
		[o_findWithQuery_1 setIsCreate:NO];
		[o_findWithQuery_1 setIsUpdate:NO];
		[o_findWithQuery_1 setIsDelete:NO]; 		
 		SUPOperationMetaData* o_getSize_1 = [SUPOperationMetaData createOperationMetaData:9:(SUPString)@"getSize":[SUPDataType forName:@"int"]:true];
	  	{
			SUPObjectList *parameters_list = nil;
	 		SUPParameterMetaData* p_getSize_query = [SUPParameterMetaData createParameterMetaData:1:(SUPString)@"query":[SUPDataType forName:@"com.sybase.persistence.Query"]];
 			parameters_list = [SUPObjectList listWithCapacity:1];
			[parameters_list addThis:p_getSize_query];
			o_getSize_1.parameters = parameters_list;
	 	}
		[o_getSize_1 setIsStatic:YES];
		[o_getSize_1 setIsCreate:NO];
		[o_getSize_1 setIsUpdate:NO];
		[o_getSize_1 setIsDelete:NO]; 		
 
  		SUPObjectList *operations = [SUPObjectList listWithCapacity:9];
 		[operations addThis:o_findAll_0];
 		[operations addThis:o_findBySPRAS_1];
 		[operations addThis:o_findByPARH3_1];
 		[operations addThis:o_findByPrimaryKey_2];
 		[operations addThis:o_findAllSorted_0];
 		[operations addThis:o_refresh_0];
 		[operations addThis:o__pk_0];
 		[operations addThis:o_findWithQuery_1];
 		[operations addThis:o_getSize_1];
	 	self.operations = operations;
 	
		SUPOperationMap *operationMap = [SUPOperationMap getInstance];
		[operationMap setOperations:operations];
		self.operationMap = operationMap;		
		self.table = @"erpsalesplus_superuser_1_0_custparh3";
		self.synchronizationGroup = @"default";

    
		SUPIndexMetaData *i_findByPrimaryKeyIndex = [[[SUPIndexMetaData alloc] init] autorelease];
		i_findByPrimaryKeyIndex.name = @"findByPrimaryKeyIndex";
		[i_findByPrimaryKeyIndex.attributes add:a_SPRAS];
		[i_findByPrimaryKeyIndex.attributes add:a_PARH3];
	
		[self.indexList add:i_findByPrimaryKeyIndex];
			
		[self.keyList add:a_surrogateKey];


  self.superClassDefined = YES;
    }
    return self;
}

@end