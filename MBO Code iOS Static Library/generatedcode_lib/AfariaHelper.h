/*
 Copyright (c) Sybase, Inc. 2012 All rights reserved. 
 
 In addition to the license terms set out in the Sybase License Agreement for 
 the Sybase Unwired Platform ("Program"), the following additional or different 
 rights and accompanying obligations and restrictions shall apply to the source 
 code in this file ("Code"). Sybase grants you a limited, non-exclusive, 
 non-transferable, revocable license to use, reproduce, and modify the Code 
 solely for purposes of (i) maintaining the Code as reference material to better 
 understand the operation of the Program, and (ii) development and testing of 
 applications created in connection with your licensed use of the Program. 
 The Code may not be transferred, sold, assigned, sublicensed or otherwise 
 conveyed (whether by operation of law or otherwise) to another party without 
 Sybase's prior written consent. The following provisions shall apply to any 
 modifications you make to the Code: (i) Sybase will not provide any maintenance 
 or support for modified Code or problems that result from use of modified Code; 
 (ii) Sybase expressly disclaims any warranties and conditions, express or 
 implied, relating to modified Code or any problems that result from use of the 
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING 
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN 
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
 to indemnify, hold harmless, and defend Sybase from and against any claims or 
 lawsuits, including attorney's fees, that arise from or are related to the 
 modified Code or from use of the modified Code.
 */

#import <UIKit/UIKit.h>
#import <Security/Security.h>
#import "SSOCertManager.h"

//Return codes from certificate provisioning APIs
#define kCertAvailable                 0
#define kCertNotAvailable              1
#define kCertAfariaClientNotInstalled  2
#define kCertAfariaSettingsRequested   3
#define kCertUrlNotFromAfaria          4
#define kCertNotImplemented            5
#define kCertFormatError               6
#define kCertUnexpectedError           7
#define kCertInvalidArg                8

#define kCertGenerateKeyFailed         20
#define kCertSeedDataUnavailable       21
#define kCertAuthenticationRequired    22
#define kCertUserNotFound              23
#define kCertFailToContactAfariaServer 24
#define kCertFailToStoreItemToKeyChain 25


//Return codes from Afaria provisioning API
#define kAfariaProvisionComplete                0
#define kAfariaProvisionedPartially             1
#define kAfariaProvisionDataNotAvailable        2
#define kAfariaProvisionRequested               3
#define kAfariaProvisionNotInstalled            4
#define kAfariaProvisionUrlNotFromAfaria        5
#define kAfariaProvisionDataVaultLocked         6
#define kAfariaProvisionUnexpectedError         7
#define kAfariaProvisionFormatError             8
#define kAfariaProvisioningNotImplemented       9

#define AFARIA_CLIENT_BUNDLE_ID @"com.sybase.afariaclient"

@interface AfariaHelper : NSObject {

}

/*!
 @method
 @abstract Use this API to set the Afaria server settings in the URL to Afaria static library
 If the caller received kAfariaSettingsRequested response code while using above getCertBlob API
 then calling application should use this API to pass on afaria server settings in the URL from Afaria client application.
 Caller should again call getCertBlob API to get the certBlob from Afaria
 Note: This API is typically called from application:didFinishLaunchingWithOptions: delegate method
 @param url url
 
 @return BOOL
 */
+ (BOOL)setAfariaServerSettings:(NSURL *)url;

/*!
 @method
 @abstract Check whether Afaria server settings is available for retrieving client certificate
 @return BOOL
 */
+ (BOOL)areAfariaSetingsConfigured;

/*!
 @method
 @abstract Clean afaria settings
 */
+ (void)clearAfariaSettings;

/*!
 @method
 @abstract Use this API to retrieve seed data from Afaria server when seed info is passed in an url from Afaria client
 * If seed data file is successfully retrieved from server it will be written to a file in application sandbox and file name is returned in seedFile parameter
 * Return values:
 * kSeedDataAvailable - Seed data available in seedFile
 * kSeedDataUnavailable - Seed data is unable to load either due to network problems or it isn't available on the server
 * kUrlNotFromAfaria - The URL parameters did not match what the library was expecting
 @return NSInteger
 */
+ (NSInteger)retrieveSeedDataWithUrl:(NSURL *)url InFile:(NSMutableString *)seedFile withCredentials:(NSURLCredential *)credentials;


/*!
 @method
 @abstract Retrieve seed data from Afaria server
 *If Afaria settings are available it will used to connect to server
 *If not, the settings will be requested from Afaria client and kAfariaSettingsRequested will be returned
 *If seed data file is successfully retrieved from server it will be written to a file in application sandbox and file name is returned in seedFile parameter
 *Return values:
 *kSeedDataAvailable - Seed data is available in seedFile
 *kSeedDataUnavailable - Seed data is unable to load either due to network problems or it isn't available on the server
 *kAfariaClientNotInstalled - Install Afaria client and try again
 *kAfariaSettingsRequested - Afaria information is requested from Afaria client. Use retrieveSeedDataWithUrl:InFile:withCredentials API when the URL is passed by Afaria client
 @return NSInteger
 */
+ (NSInteger)retrieveSeedData:(NSString *)urlScheme InFile:(NSMutableString *)seedFile withCredentials:(NSURLCredential *)credentials;

/*!
 @method
 @abstract Generate key pair, use it to generate CSR, send it to CA through Afaria, get the X.509 certificate,
 *construct a certBlob using certificate and private key and return the certBlob along with certificate and private key.
 *Note: This API implementation uses Afaria API retrieveCertificateWithPrivateKey which would communicate with Afaria server if it has all the necessary settings to do so.
 *If not it would request further information from Afaria client which would be passed back to Afaria library through the API getCertBlob:withUrl:forUser:withChallengeCode:
 *Return values:
 *kCertAvailable - Certificate is available in certData
 *kCertNotAvailable - Certificate is not available from server
 *kCertAfariaClientNotInstalled - Install Afaria client and try again
 *kCertAfariaSettingsRequested - Afaria information is requested from Afaria client. Use getCertBlob:withUrl API when Afaria client communicates back through URL scheme
 *kCertNotImplemented - This feature is implemented only on devices. So simulator builds will return this error
 *kCertFormatError - Format of the certificate is not valid. Unable to make certBlob out of it
 *kCertUnexpectedError - Unexpected error
 @param certData      Signed certificate (see details on CertData above)
 @param urlScheme     URL scheme of the calling application so that Afaria library can use it to pass it to on Afaria client if necessary
 @param commonName    Common name used to generate the CSR
 @param challengeCode Challenge code for CA that should associate with CSR
 @return NSInteger code
 */
+ (NSInteger)getCertBlob:(CertData *)certData forUrlScheme:(NSString *)urlScheme forUser:(NSString *)commonName withChallengeCode:(NSString *)challengeCode  certificateLabel:(NSString*)label;


/*!
 @method
 @abstract Use this method to get a client identity containing certificate and associated private key
 *stored during the getCertBlob call.
 @return NSInteger
 */
+ (NSInteger)getClientIdentity:(SecIdentityRef *)identityRef;

/*!
 @method
 @abstract Get identity based on kSecAttrLabel's value
 @return OSStatus
 */
+ (OSStatus) getIdentityBasedOnLabel:(NSString *)label identity:(SecIdentityRef*) pidentityRef;

/*!
 @method
 @abstract delete identities, certificate and keys whose kSecAttrLabel matches the name parameter.
 @return OSStatus
 */
+ (OSStatus)deleteIdentities:(NSString*)name;

/*!
 @method
 @abstract
 @return OSStatus
 */
+ (OSStatus)deleteCertificates:(NSString*)name;

/*!
 @method
 @abstract
 @return OSStatus
 */
+ (OSStatus)deleteKeys:(NSString*)name;

/*!
 @method
 @abstract
 @return BOOL
 */
+ (BOOL)isCertificateCached:(NSString*)name;

/*!
 @method
 @abstract helper method to dump credential information
 */
+ (void)_dumpCredentials:(NSString*)description;

/*!
 @method
 @abstract helper method to dump credential with matched kSecAttrLabel value
 */
+ (void)_dumpCredentials:(NSString*)description labelAttribute:(NSString*)name;

/*!
 @method
 @abstract
 @return NSInteger
 */
+ (NSInteger)initializeAfariaLibrary:(NSURL *)url inUrlScheme:(NSString*)inUrlScheme withBundleId:(NSString*)bundleID;
@end