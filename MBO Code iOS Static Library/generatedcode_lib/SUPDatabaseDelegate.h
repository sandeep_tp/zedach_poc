/*
 
 Copyright (c) Sybase, Inc. 2011-2012   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */


#import <Foundation/Foundation.h>
#import "SUPDatabaseMetaDataRBS.h"
#import "SUPConnectionProfile.h"
#import "SUPCallbackHandler.h"
#import "SUPConnectionWrapper.h"
#import "SUPLogger.h"
#import "SUPQueryResultSet.h"
#import "SUPOnlineLoginStatus.h"
#import "SUPAbstractDBRBS.h"
#import "SUPJsonObject.h"
#import "SUPEntityDelegate.h"
#import "SUPLocalStorage.h"

@class SUPAbstractPersonalizationParameters;
@class SUPServerPersonalizationDelegate;
@class SUPSynchronizationRequestQueue;
@class SUPJsonMessage;
@class SUPIndexMetaData; 
@class SUPEntityMetaDataRBS;

@interface SUPDatabaseDelegate : NSObject {
    @private
    SUPAbstractPersonalizationParameters *personalizationParameters;
    BOOL    mSubscribed;
    BOOL    serviceStarted;
    SUPStringList    *syncSet;
    SUPStringList    *contextSet;
    int syncMode;
    SUPSynchronizationRequestQueue *srq;
    SUPSynchronizationRequestQueue *imq;
    BOOL isInsideCB;
    SUPLocalStorage* localStorage;
    
}

@property (readwrite, retain, nonatomic) SUPAbstractDBRBS *database;
@property (readwrite, retain, nonatomic) SUPObjectList *classDelegates;
@property (readwrite, retain, nonatomic) NSMutableDictionary *delegateMap;
@property (readwrite, retain, nonatomic) SUPAbstractPersonalizationParameters *personalizationParameters;
@property (readwrite, retain, nonatomic) SUPServerPersonalizationDelegate *spDelegate;
@property (readwrite, retain, nonatomic) SUPSynchronizationRequestQueue *srq;
@property (readwrite, retain, nonatomic) SUPSynchronizationRequestQueue *imq;
@property (readwrite, retain, nonatomic) SUPString remoteId;
@property (readwrite, retain, nonatomic) SUPLocalStorage *localStorage;

- (id)initWithDatabase:(SUPAbstractDBRBS *)inDatabase;

- (void)addDelegate:(SUPClassDelegate *)inDelegate forName:(NSString *)entityName;
- (SUPEntityDelegate *)getDelegate:(NSString*) entityName;

- (SUPLocalTransaction*)beginTransaction;
- (NSObject<SUPCallbackHandler>*)callbackHandler;
- (SUPConnectionProfile*)connectionProfile;
- (SUPConnectionProfile*)getConnectionProfile;
- (SUPConnectionProfile*)getSynchronizationProfile;
- (void)createDatabase;
- (void)createPublication:(SUPStringList *)statements withName:(SUPString)publicationName forTables:(SUPObjectList *)emdList;
- (SUPStringList *)sqlCreateStatements;
- (void)cleanAllData:(BOOL) keepClientOnly;
- (void)cleanAllData;
- (void)deleteDatabase;
- (BOOL)databaseExists;
- (id<SUPConnectionWrapper>)getConnectionWrapper;
- (void)openConnection;
- (void)closeConnection;
- (NSString*)getSyncUsername;
- (NSString*)getDomainName;
- (int32_t)getSchemaVersion;
- (int32_t)getProtocolVersion;
- (id<SUPLogger>)getLogger;
- (SUPOnlineLoginStatus*)getOnlineLoginStatus;
//- (NSObject<SUPSynchronizationGroup> *)getSyncGroupForName:(NSString*)_name;
- (void)registerCallbackHandler:(NSObject<SUPCallbackHandler>*)handler;

- (void)loginToSync:(NSString *)user password:(NSString *)pass;
//- (void)beginOnlineLogin:(NSString *)user password:(NSString *)pass;
//- (void)beginOnlineLogin;
- (BOOL)offlineLogin:(NSString *)user password:(NSString *)pass;
//- (BOOL)offlineLogin;
- (void)subscribe;
- (id<SUPSynchronizationGroup>) getSynchronizationGroup:(NSString*)name;
- (void)beginSynchronize;
- (void)beginSynchronizeForGroups:(SUPObjectList*)synchronizationGroups withContext:(id)context;
- (void)beginSynchronizeForGroups:(SUPObjectList*)inSynchronizationGroups withContext:(id)context withUploadOnly:(BOOL)uploadOnly;
- (void)beginSynchronizeForGroups:(SUPObjectList*)synchronizationGroups withContext:(NSString*)context withUploadOnly:(BOOL)uploadOnly withSyncStatusListener:(id<SUPSyncStatusListener>) listener;
- (BOOL)resumePendingSynchronization;
- (BOOL)cancelPendingSynchronization;
- (void)submitPendingOperations;
- (void)submitPendingOperations:(NSString*)synchronizationGroup;
- (void)cancelPendingOperations;
- (void)cancelPendingOperations:(NSString*)synchronizationGroup;
//- (BOOL)hasPendingOperations;
- (void)unsubscribe;
- (int64_t)generateId;
- (int64_t)generateLocalId;
- (BOOL)isSynchronized:(NSString*)synchronizationGroup;
- (NSDate*)getLastSynchronizationTime:(NSString*)synchronizationGroup;

- (void)changeEncryptionKey:(NSString *)newKey;
//- (SUPObjectList*)getLogRecords:(SUPQuery*)query;
//- (void)submitLogRecords;
- (BOOL) initialSync;
- (BOOL)onlineLogin:(NSString *)user password:(NSString *)pass;
- (void)synchronize:(NSString*)syncGroups_param;

- (void)synchronize;
- (void)synchronizeWithListener:(id<SUPSyncStatusListener>) listener;

- (void)synchronize:(NSString *)synchronizationGroup withListener:(id<SUPSyncStatusListener>)listener;
-(NSString*)scriptVersion;
- (void)generateEncryptionKey;
- (void)handleRbsPushMessage:(SUPJsonMessage*)message;
- (BOOL) isReplayQueueEmpty;
- (SUPObjectList*) getBackgroundSyncRequests;
- (SUPObjectList*)getChangeLogs:(SUPQuery*)query;
+ (void) findAndCheckPendingObjects : (SUPObjectList*)reverseList :(SUPObjectList*)mboList;
- (NSString*) getRemoteId;
- (void)traceSynchronize:(SUPConnectionProfile*)profile synchronizationGroup:(SUPString)synchronizationGroup userContext:(SUPString)userContext remoteId:(SUPString)remoteId;
- (NSString*) getDbPath;
- (void) createIndex:(SUPIndexMetaData *)index withEntity:(SUPEntityMetaDataRBS *)entity;
- (void) dropIndex:(NSString*)name withEntity:(SUPEntityMetaDataRBS *)entity;
- (SUPObjectList*) getIndexes:(SUPEntityMetaDataRBS *)entity;
@end
