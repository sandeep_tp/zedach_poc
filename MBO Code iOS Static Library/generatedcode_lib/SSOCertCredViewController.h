/*******************************************************************************
* Source File : SSOCertCredViewController.h
* Date Created: 12/16/2010
* Copyright   : 2000 - 2010, Sybase, Inc.
* Description : Handles SSO certificate password entry
* Notes       :
*
*******************************************************************************/
#import <UIKit/UIKit.h>
#import "WidgetScreenControllerPrivate.h"
/*!
 @class SSOCertCredViewController
 @abstract Certificate credential view controller
 */
@interface SSOCertCredViewController : UIViewController <UITextFieldDelegate>
{
   CGRect viewFrame;
   UILabel* certPwLabel;
   UITextField* certPw;
   UILabel* winNameLabel;
   UITextField* winName;
   UILabel* winPwLabel;
   UITextField* winPw;
   UIBarButtonItem* doneButton;
   WidgetScreenControllerPrivate* widget;
}
/*!
 @method
 @abstract Certificate credential view controller initialization
 
 @param aFrame  frame of view controller
 @param aWidget WidgetScreenControllerPrivate
 
 @return SSOCertCredViewController
 */
- (id)initWithFrame:(CGRect)aFrame withWidget:(WidgetScreenControllerPrivate*)aWidget;
/*!
 @method
 @abstract setViewRotateToInterfaceOrientation
 
 @param interfaceOrientation UIInterfaceOrientation
 */
- (void)setViewRotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;

@end
