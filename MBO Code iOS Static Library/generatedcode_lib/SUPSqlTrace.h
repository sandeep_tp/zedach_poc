/*
 
 Copyright (c) Sybase, Inc. 2011-2012   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */


#import <Foundation/Foundation.h>
#import "SUPConnectionProfile.h"
#import "SUPJsonObject.h"
@class SUPConnectionProfile;
// Log levels:
// DEBUG > INFO > WARN > ERROR > FATAL > OFF
/*!
 @defined
 @abstract   No logging.
 @discussion Set for no logging in the system.
 */
#define TRACE_OFF		0

/*!
 @defined
 @abstract   Fatal logging.
 @discussion Set for Fatal logging only.
 */
#define TRACE_FATAL	1

/*!
 @defined
 @abstract   Error logging.
 @discussion Set for Error or Fatal logging in the system.
 */
#define TRACE_ERROR	2

/*!
 @defined
 @abstract   Warning logging.
 @discussion Set for Warning, Error or Fatal logging in the system.
 */
#define TRACE_WARN	3

/*!
 @defined
 @abstract   Informational logging.
 @discussion Set for info, warning, error or fatal logging in the system.
 */
#define TRACE_INFO	4

/*!
 @defined
 @abstract   Debug logging.
 @discussion Set for All logging in the system.
 */
#define TRACE_DEBUG	5

#define SQLTraceLogDebug(pf,s,...) \
[SUPSqlTrace log:pf :(s),##__VA_ARGS__]

#define SQLTraceLogDetail(pf,s,...) \
[SUPSqlTrace logWithDetail:pf :(s),##__VA_ARGS__]

#define SQLTraceLogThrowableError(pf,s,...) \
[SUPSqlTrace throwableError:pf :(s),##__VA_ARGS__]

#define MBSLog(pf,s,...) \
 [SUPSqlTrace mbsLog:pf :(s),##__VA_ARGS__]

#define MBSLogHeader(pf,j,t) \
 [SUPSqlTrace mbsLogJsonHeader:pf : j:t]

#define MBSLogContent(pf,j,t) \
[SUPSqlTrace mbsLogJsonContent:pf : j:t]

void SQLTraceLogPersistenceExceptionWithThrow(SUPConnectionProfile *pf,  NSException *orignalException, NSString* format, ...);

@interface SUPSqlTrace : NSObject {
    
}
+ (void) log:(SUPConnectionProfile*)profile :(NSString*)format, ...;
+ (void) error:(SUPConnectionProfile*)profile withThrow:(BOOL)throwable withMessage:(NSString*)message;
+ (void) logWithDetail:(SUPConnectionProfile*)profile :(NSString*)format, ...;
+ (BOOL) getEnabled:(SUPConnectionProfile*)profile;
+ (void) throwableError:(SUPConnectionProfile*)profile :(NSString*)format, ...;
+ (void) mbsLog:(SUPConnectionProfile*)profile :(NSString*)format, ...;
+ (void) mbsLogJsonHeader:(SUPConnectionProfile*)profile :(SUPJsonObject*)header :(NSString*)title;
+ (void) mbsLogJsonContent:(SUPConnectionProfile*)profile :(SUPJsonObject*)content :(NSString*)title;
+ (void) mbsLogJsonStreamedContent:(SUPConnectionProfile*)profile :(NSString*)content :(NSString*)title;
@end


