/*
 Copyright (c) Sybase, Inc. 2012 All rights reserved. 
 
 In addition to the license terms set out in the Sybase License Agreement for 
 the Sybase Unwired Platform ("Program"), the following additional or different 
 rights and accompanying obligations and restrictions shall apply to the source 
 code in this file ("Code"). Sybase grants you a limited, non-exclusive, 
 non-transferable, revocable license to use, reproduce, and modify the Code 
 solely for purposes of (i) maintaining the Code as reference material to better 
 understand the operation of the Program, and (ii) development and testing of 
 applications created in connection with your licensed use of the Program. 
 The Code may not be transferred, sold, assigned, sublicensed or otherwise 
 conveyed (whether by operation of law or otherwise) to another party without 
 Sybase's prior written consent. The following provisions shall apply to any 
 modifications you make to the Code: (i) Sybase will not provide any maintenance 
 or support for modified Code or problems that result from use of modified Code; 
 (ii) Sybase expressly disclaims any warranties and conditions, express or 
 implied, relating to modified Code or any problems that result from use of the 
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING 
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN 
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
 to indemnify, hold harmless, and defend Sybase from and against any claims or 
 lawsuits, including attorney's fees, that arise from or are related to the 
 modified Code or from use of the modified Code.
 */
 
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "HybridAppViewController.h"

// Workflow priorities
// Constants moved to HybridAppViewController under HybridApp priorities
// import HybridAppViewController.h in your code 

#define WORKFLOW_CHANGED_NOTIFICATION  @"WorkflowChanged"

// CustomIcon class definition moved to HybriAppViewController.h
// import HybridAppViewController.h in your code 


// The class describing a Workflow that can be opened
// Note: This class is deprecated. Instead use HybridApp defined in HybridAppViewController.h
@interface Workflow : NSObject {
   I32 moduleId;       // A unique number assigned to the Workflow by the server
   I32 moduleVersion;  // A number identifying the version of the module as specified in the manifest
   I32 msgId;          // For Workflows accessed from the inbox, a unique ID associated with the message
                             // For Workflows accessed from the Workflows folder this will be -1
   NSString* moduleName;     // The Workflow name from the manifest
   NSMutableArray* customIconList;     // custom icon list. 

   BOOL isProcessed;         // Indicates whether Workflow has been processed or not.  Needed internally by the class.
   I32 iconIndex;      // Used to determine icon to return from getImage.  Needed internally by the class.
   NSString* from;           // Who workflow is from, only applies to workflow messages
   NSString* subject;        // Subject of workflow, only applies to workflow messages
   NSDate* date;             // Received date, only applies to workflow messages
   BOOL readFlag;            // Indicates whether or not workflow has been read, only applies to workflow messages
   I32 priority;       // Indicates workflow priorities (see above),  only applies to workflow messages
   BOOL isDefault;           // Indicates whether this workflow is the single default one that can be opened
   BOOL isPrepackaged;       // Is this workflow compiled into the client rather than received from the server?
}

@property(nonatomic, assign) I32 moduleId;
@property(nonatomic, assign) I32 moduleVersion;
@property(nonatomic, assign) I32 msgId;
@property(nonatomic, assign) NSString* moduleName;
@property(nonatomic, assign) BOOL isProcessed;
@property(nonatomic, assign) I32 iconIndex;
@property(nonatomic, assign) NSString* from;
@property(nonatomic, assign) NSString* subject;
@property(nonatomic, assign) NSDate* date;
@property(nonatomic, assign) BOOL readFlag;
@property(nonatomic, assign) I32 priority;
@property(nonatomic, assign) BOOL isDefault;
@property(nonatomic, assign) BOOL isPrepackaged;
@property(nonatomic, assign) NSMutableArray* customIconList;

// Used to sort Workflows in the Workflows folder       
- (NSComparisonResult)compareByName:(Workflow*)workflow;

// Returns an image to display that is associated with the Workflow
// The image may change after the Workflow is processed
// The files contained within WorkflowImages.zip must be added to the applications project in order for this
// routine to operate properly, otherwise nil will be returned.
- (UIImage*)getImage;

// The image that indicates the read flag and priority of the message
// The files contained within WorkflowImages.zip must be added to the applications project in order for this
// routine to operate properly, otherwise nil will be returned.
- (UIImage*)getReadFlagImage;

// Internal class level function used internally
+ (Workflow*)workflowWithModuleId:(UI32)moduleId withModuleVersion:(UI32)moduleVersion scope:(bool)bFromInstalledWorkflows;

// Call this to install a prepackaged workflow that is included as a resource
// path is the absolute path to the folder
+ (BOOL)installPackageByClient:(NSString*)path;

+ (UIImage*) getBuiltInImageByIndex:(I32)index isProcessed:(BOOL)bProcessed;
- (UIImage*) getCustomIconImage:(CustomIcon*)customIcon;
- (UIImage*) getCustomIconProcessedImage:(CustomIcon*)customIcon;
  //return the custom icon selected with default logic, returning null if no suitable custom icon is avaialble 
- (CustomIcon*) getDefaultCustomIcon;
@end

// Note: This enum is deprecated. Instead use HybridAppFolderType defined in HybridAppViewController.h
typedef enum {
   UnknownFolder,
   WorkflowFolder,
   MessageFolder
} WorkflowFolderType;

// Note: This enum is deprecated. Instead use HybridAppOperation defined in HybridAppViewController.h
typedef enum {
   WORKFLOW_OPERATION_NEW = 1,
   WORKFLOW_OPERATION_MOD = 2,
   WORKFLOW_OPERATION_DELETE = 3,
   WORKFLOW_OPERATION_DELETE_ALL = 4
} WorkflowOperation;

// Each folder of Workflows should maintain a collection
// This collection can be populated via loadFromWorkflowFolder or loadFromInbox depending upon the folder in use
// For Workflow folders the collection should be accessed via workflowAtIndexFromWorkflowFolder
// For Inbox folders the collection should be accessed via workflowFromMsgId
// Note: This class is deprecated. Instead use HybridAppCollection defined in HybridAppViewController.h
DEPRECATED_ATTRIBUTE @interface WorkflowCollection : NSObject {
   NSMutableArray* workflows;
   WorkflowFolderType folderType;
}

@property (nonatomic, readonly) NSInteger count;
@property (nonatomic, readonly, copy) NSMutableArray* workflows;

- (void)loadForFolder:(WorkflowFolderType)folderTypeToLoad;
- (WorkflowFolderType)getFolderType;
- (Workflow*)workflowAtIndex:(UI32)index;
- (Workflow*)workflowFromModuleId:(UI32)moduleId fromModuleVersion:(UI32)moduleVersion;
- (Workflow*)workflowFromMsgId:(UI32)msgId;
+ (UI32)unreadCount;
- (void)deleteWorkflowAtIndex:(UI32)index;
- (void)deleteWorkflowWithModuleId:(UI32)moduleId withModuleVersion:(UI32)moduleVersion;

@end


@class WorkflowViewController;

// Note: This protocol is deprecated. Instead use HybridAppViewControllerDelegate defined in HybridAppViewController.h
DEPRECATED_ATTRIBUTE @protocol WorkflowViewControllerDelegate

// A delegate should be implemented for each folder
// It will be caled after a Workflow submission occurs
// Depending upon the Workflow definition, some Workflows need to be immediately reopened after a
// submission occurs.  If the workflowToReopen parameter is not nil this workflow should be opened by
// the calling view and the it is up to the caller to release this parameter.
// Whether the workflowToReopen is nil or not, typically the delegate should reload its collection
// when called.  
- (void)workflowViewController:(WorkflowViewController*)controller workflowToReopen:(Workflow*)workflow;

- (void)workflowDidStartLoad:(I32)msgId withModuleId:(I32)moduleId withModuleVersion:(I32)moduleVersion;
- (void)workflowDidFinishLoad:(I32)msgId withModuleId:(I32)moduleId withModuleVersion:(I32)moduleVersion;
            
- (void)workflowDidClose:(I32)msgId withModuleId:(I32)moduleId withModuleVersion:(I32)moduleVersion;

@end

// The controller that provides the ability to open a Workflow
// Note: This class is deprecated. Instead use HybridAppViewController defined in HybridAppViewController.h
DEPRECATED_ATTRIBUTE @interface WorkflowViewController : UIViewController 
+ (I32)openWithWorkflow:(Workflow*)workflow
               fromController:(UIViewController*)viewController
                     delegate:(id <WorkflowViewControllerDelegate>)delegate;

+ (void)deleteWorkFlowMsg:(UI32)msgId;

+ (void)closeCurrentWorkFlowMsg;

// application calls refreshView to let workflow redrew itself. For example, application should call this method when in-call status bar 
// height gets changed in iphone portrait mode,
+ (void)refreshView;

@end

// Used whenever the collection changes
// folderType specifies which folder was changed
// operation indicates new, modifify, or delete
// msgId only applies to MessageFolder
// Note: This class is deprecated. Instead use HybridAppChangeNotification defined in HybridAppViewController.h
DEPRECATED_ATTRIBUTE @interface WorkflowChangeNotification: NSObject
{
   WorkflowFolderType folderType;
   WorkflowOperation operation;
   I32 moduleId;
   I32 moduleVersion;
   I32 msgId;
}

@property (nonatomic, readonly) WorkflowFolderType folderType;
@property (nonatomic, readonly) I32 moduleId;
@property (nonatomic, readonly) I32 moduleVersion;
@property (nonatomic, readonly) I32 msgId;
@property (nonatomic, readonly) WorkflowOperation operation;

// Internal API used for object creation by internal code
- (id)initWithType:(WorkflowFolderType)type withOperation:(WorkflowOperation)op withModuleId:(I32)moduleID withModuleVersion:(I32)version withMsgId:(I32)msgID;

@end
