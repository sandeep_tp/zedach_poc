/*
 
 Copyright (c) Sybase, Inc. 2011-2012   All rights reserved.                                    
 
 In addition to the license terms set out in the Sybase License Agreement for 
 the Sybase Unwired Platform ("Program"), the following additional or different 
 rights and accompanying obligations and restrictions shall apply to the source 
 code in this file ("Code").  Sybase grants you a limited, non-exclusive, 
 non-transferable, revocable license to use, reproduce, and modify the Code 
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of 
 applications created in connection with your licensed use of the Program.  
 The Code may not be transferred, sold, assigned, sublicensed or otherwise 
 conveyed (whether by operation of law or otherwise) to another party without 
 Sybase's prior written consent.  The following provisions shall apply to any 
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or 
 implied, relating to modified Code or any problems that result from use of the 
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
 to indemnify, hold harmless, and defend Sybase from and against any claims or 
 lawsuits, including attorney's fees, that arise from or are related to the 
 modified Code or from use of the modified Code. 
 
 */
#import "sybase_core.h"

#import "SUPAbstractOperationException.h"
#import "SUPJsonObject.h"

#import "SUPJsonInputStream.h"

/*!
 @class
 @abstract Abstract class representing an input stream.  Used by @link //apple_ref/occ/cl/SUPJsonStreamParser SUPJsonStreamParser @/link.
 @discussion
 */
@interface SUPJsonInputStream : NSObject
{
    @protected
}

/*!
 @method
 @abstract Closes the stream.
 @discussion
 */
- (void)close;

/*!
 @method
 @abstract Mark the stream at the current input position, if mark and reset are supported.
 @discussion
 */
- (void)mark;

/*!
 @method
 @abstract Returns true if the mark and reset operations are supported for this stream object.
 @discussion
 */
- (SUPBoolean)markSupported;

/*!
 @method
 @abstract Read a character from the stream, and move to the next character in the stream.
 @discussion
 @result The character that was read.
 */
- (SUPChar)read;

/*!
 @method
 @abstract Reset the stream to the marked position, if mark and reset are supported.
 @discussion
 */
- (void)reset;

/*!
 @method
 @abstract Skip over and discard characters in the stream, returning the new stream position.
 @discussion
 @param skipLength The number of characters to skip.
 @result The new stream position.
 */
- (SUPLong)skip:(SUPInt)skipLength;

- (NSString*)toString;
- (NSString*)description;

@end
