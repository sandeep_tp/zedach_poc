/*
 
 Copyright (c) Sybase, Inc. 2013   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */


#import <Foundation/Foundation.h>
#import "SUPCFSCTraceDetailRequest.h"
#import "SUPCFSCTraceSync.h"

@class SUPCFSCTraceRequestQueue;
@class SUPConnectionProfile;
@class SUPCFSCTrace;

typedef enum
{
	CFSCTraceLevel_ERROR          = 0,
	CFSCTraceLevel_NORMAL            = 1,
	CFSCTraceLevel_DETAIL         = 2
} CFSCTraceLevel;

#define CFSCLogNormal(mbo, f, k, sg, log) \
[SUPCFSCTrace log:mbo withFunction:f withKey:k withSyncGroup:sg withMessage:log]

#define CFSCLogDetail(mbo, f, k, sg, log, p, n, u, d, c, sql, pa) \
[SUPCFSCTrace logDetail:mbo withFunction:f withKey:k withSyncGroup:sg withMessage:log \
withPending:p withIsNew:n withIsUpdated:u withIsDeleted:d withIsCreated:c withSql:sql withParameters:pa]

#define CFSCLogQueryStatement(sql) \
[SUPCFSCTrace logDetail:nil withFunction:nil withKey:nil withSyncGroup:nil WithMessage:nil withPending:NO withIsNew:NO \
withIsUpdated:NO withIsDeleted:NO withIsCreated:NO withSql:sql withParameters:nil]

#define CFSCLogParameter(pa) \
[SUPCFSCTrace logDetail:nil withFunction:nil withKey:nil withSyncGroup:nil WithMessage:nil withPending:NO withIsNew:NO \
withIsUpdated:NO withIsDeleted:NO withIsCreated:NO withSql:nil withParameters:pa]

#define CFSCTraceLogMsg(m) \
[SUPCFSCTrace log:nil withFunction:nil withKey:nil withSyncGroup:nil WithMessage:m]

#define CFSCTraceLogError(e) \
[SUPCFSCTrace logError:e]

@interface SUPCFSCTrace : NSObject {
    @private
    BOOL    traceStarted;

}
@property(readwrite, assign, nonatomic)CFSCTraceLevel traceLevel;
+ (SUPCFSCTrace*)getInstance;
+ (BOOL)isEnabled;
+ (void)log:(NSString*)mbo withFunction:(NSString*)function withKey:(NSString*)key withSyncGroup:(NSString*)syncGroup WithMessage:(NSString*)logContext;
+ (void)logDetail:(NSString*)mbo withFunction:(NSString*)function withKey:(NSString*)key withSyncGroup:(NSString*)syncGroup WithMessage:(NSString*)logContext withPending:(BOOL)pending withIsNew:(BOOL)isNew withIsUpdated:(BOOL)isUpdated withIsDeleted:(BOOL)isDeleted withIsCreated:(BOOL)isCreated withSql:(NSString*)sql withParameters:(NSString*)params;

+ (void)logAll:(NSString*)mbo withFunction:(NSString*)function withKey:(NSString*)key withSyncGroup:(NSString*)syncGroup WithMessage:(NSString*)logContext withPending:(BOOL)pending withIsNew:(BOOL)isNew withIsUpdated:(BOOL)isUpdated withIsDeleted:(BOOL)isDeleted withIsCreated:(BOOL)isCreated withSql:(NSString*)sql withParameters:(NSString*)params;
+ (void)logError:(NSString*)message;

+ (void)startTrace;
+ (void)setTraceLevel:(CFSCTraceLevel)level;
+ (CFSCTraceLevel)getTraceLevel;
+ (void)log:(id)request;
/*
+ (void)logQuery:(SUPConnectionProfile*)profile :(NSString*)sql;
+ (void)logParameters:(SUPConnectionProfile*)profile :(NSString*)params;
+ (void)logMessage:(SUPConnectionProfile*)profile :(NSString*)message;
 */
@end


