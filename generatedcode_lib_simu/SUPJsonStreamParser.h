/*
 
 Copyright (c) Sybase, Inc. 2011-2012   All rights reserved.                                    
 
 In addition to the license terms set out in the Sybase License Agreement for 
 the Sybase Unwired Platform ("Program"), the following additional or different 
 rights and accompanying obligations and restrictions shall apply to the source 
 code in this file ("Code").  Sybase grants you a limited, non-exclusive, 
 non-transferable, revocable license to use, reproduce, and modify the Code 
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of 
 applications created in connection with your licensed use of the Program.  
 The Code may not be transferred, sold, assigned, sublicensed or otherwise 
 conveyed (whether by operation of law or otherwise) to another party without 
 Sybase's prior written consent.  The following provisions shall apply to any 
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or 
 implied, relating to modified Code or any problems that result from use of the 
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
 to indemnify, hold harmless, and defend Sybase from and against any claims or 
 lawsuits, including attorney's fees, that arise from or are related to the 
 modified Code or from use of the modified Code. 
 
 */
#import "sybase_core.h"

#import "SUPBooleanUtil.h"
#import "SUPDateTimeUtil.h"
#import "SUPDateUtil.h"
#import "SUPJsonException.h"
#import "SUPJsonInputStream.h"
#import "SUPJsonNumber.h"
#import "SUPNumberUtil.h"
#import "SUPStringUtil.h"
#import "SUPCharList.h"
#import "SUPTimeUtil.h"
#import "SUPIntList.h"
#import "SUPStringList.h"

#import "SUPJsonStreamParserState.h"

/*!
 @class
 @abstract Implements a stream parser for parsing an input stream of JSON-encoded text.
 @discussion
 @unsorted
 */
@interface SUPJsonStreamParser : NSObject

/*!
 @property
 @abstract The current state of the parser.
 @discussion
 */
@property(readonly,assign) SUPJsonStreamParserStateType state;

/*!
 @property
 @abstract The current token from the input stream.
 @discussion
 */
@property(readonly,retain) NSString *token;

/*!
 @property
 @abstract The maximum size of a string token.
 */
@property(readwrite,assign,nonatomic) int32_t maxStringSize;

- (SUPJsonStreamParser*)init;
- (void)dealloc;
+ (SUPJsonStreamParser*)getInstance;

/*!
 @method
 @abstract Creates a new parser instance from an input stream.
 @discussion
 @param stream The input stream.
 @result A new instance of this class.
 */
+ (SUPJsonStreamParser*)parserFromInputStream:(SUPJsonInputStream*)stream;

/*!
 @method
 @abstract Reads text from the input stream until it constructs a token corresponding to a JSON object beginning/end, JSON array beginning/end, the name of a name/value pair, or a JSON value (boolean, string, numeric, or null).  If invalid JSON text is encountered, an exception will be thrown.
 @discussion If a string value is longer than the maxStringSize property, it will be broken up into substring tokens, and the state returned will indicate the beginning, continuation, or end of the string value.
 @result The new state of the parser.  The token property will be filled with the new token constructed by the parser.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPJsonStreamParserStateType)nextState;

/*!
 @method
 @abstract Convenience method that reads text from the input stream until it constructs a complete SUPJsonObject.  If invalid JSON text is encountered, an exception will be thrown.
 @discussion If the stream is not positioned at the beginning of a JSON object ("{"), the parser will skip characters until a JSON object is found.
 @result An SUPJsonObject instance.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPJsonObject*)getNextObject;

/*!
 @method
 @abstract Convenience method that reads text from the input stream until it constructs a complete SUPJsonArray.  If invalid JSON text is encountered, an exception will be thrown.
 @discussion If the stream is not positioned at the beginning of a JSON array ("["), the parser will skip characters until a JSON array is found.
 @result An SUPJsonArray instance.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPJsonArray*)getNextArray;

/*!
 @method
 @abstract Convenience method to skip characters until the end of the current JSON object is reached.  If invalid JSON text is encountered, an exception will be thrown.
 @discussion
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (void)skipObject;
/*!
 @method
 @abstract Convenience method to skip characters until the end of the current JSON array is reached.  If invalid JSON text is encountered, an exception will be thrown.
 @discussion
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (void)skipArray;

/*!
 @method
 @abstract If the parser is currently positioned at the name of a JSON name/value pair, returns the name.  Otherwise, throws an exception.
 @discussion
 @result The name.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPString)getFieldName;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON numeric value, returns the value.  Otherwise, throws an exception.
 @discussion
 @result The value, as a SUPJsonNumber object.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPJsonNumber*)getNumberValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPBoolean)getBooleanValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPString)getStringValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPBinary)getBinaryValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPChar)getCharValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPByte)getByteValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPShort)getShortValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPInt)getIntValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPLong)getLongValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPInteger)getIntegerValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPDecimal)getDecimalValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPFloat)getFloatValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPDouble)getDoubleValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPDate)getDateValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPTime)getTimeValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPDateTime)getDateTimeValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPNullableBoolean)getNullableBooleanValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPNullableString)getNullableStringValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPNullableBinary)getNullableBinaryValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPNullableChar)getNullableCharValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPNullableByte)getNullableByteValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPNullableShort)getNullableShortValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPNullableInt)getNullableIntValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPNullableLong)getNullableLongValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPNullableInteger)getNullableIntegerValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPNullableDecimal)getNullableDecimalValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPNullableFloat)getNullableFloatValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPNullableDouble)getNullableDoubleValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPNullableDate)getNullableDateValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPNullableTime)getNullableTimeValue;

/*!
 @method
 @abstract If the parser is currently positioned at a JSON value, returns the value as this type.  If the value is not of this type, an exception will be thrown.
 @discussion
 @result The value.
 @throws @link //apple_ref/occ/cl/SUPJsonException SUPJsonException @/link
 */
- (SUPNullableDateTime)getNullableDateTimeValue;

@end
