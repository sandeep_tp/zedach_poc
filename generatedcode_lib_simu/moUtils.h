/*
Copyright (c) Sybase, Inc. 2012 All rights reserved. 

In addition to the license terms set out in the Sybase License Agreement for 
the Sybase Unwired Platform ("Program"), the following additional or different 
rights and accompanying obligations and restrictions shall apply to the source 
code in this file ("Code"). Sybase grants you a limited, non-exclusive, 
non-transferable, revocable license to use, reproduce, and modify the Code 
solely for purposes of (i) maintaining the Code as reference material to better 
understand the operation of the Program, and (ii) development and testing of 
applications created in connection with your licensed use of the Program. 
The Code may not be transferred, sold, assigned, sublicensed or otherwise 
conveyed (whether by operation of law or otherwise) to another party without 
Sybase's prior written consent. The following provisions shall apply to any 
modifications you make to the Code: (i) Sybase will not provide any maintenance 
or support for modified Code or problems that result from use of modified Code; 
(ii) Sybase expressly disclaims any warranties and conditions, express or 
implied, relating to modified Code or any problems that result from use of the 
modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING 
TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN 
IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
to indemnify, hold harmless, and defend Sybase from and against any claims or 
lawsuits, including attorney's fees, that arise from or are related to the 
modified Code or from use of the modified Code.
*/

/******************************************************************************
*    Copyright 2012 Sybase, Inc
*    Source File            : moUtils.h
*    Platform Dependencies  :
*    Description            : Header file for misc. util functions.
*
*    Notes                  :
******************************************************************************/

#ifndef mo_utils_H_INCLUDED
#define mo_utils_H_INCLUDED


#include "moString.h"
#include "mo_bin_protocol.h"

using namespace mo;


#define MAX_LONG_VALUE     2147483647


UI32 GetCRC( UI8 *pucData,
             UI32 ulDataLength );

void CombinedLCG( I32* plS1,
                  I32* plS2,
                  double* pdResult );

I32 moGetRandNum();

void MsgDlg( const TCHAR* pcTitle,
             const TCHAR* pcMessage );

void MoURLEncode( const TCHAR *pszURLIn, TCHAR **ppszURLOut );
int MoIsvalidurlchar( TCHAR ch );

void MOAssertFunc( const TCHAR* pcMsg,
                   const TCHAR* pcMsg2,
                   const TCHAR* pcFileName,
                   int lLineNumber );

void MOAssertFunc( TCHAR* pcMsg,
                   TCHAR* pcFileName,
                   int lLineNumber );


#ifdef ESI_PALM
   #include "moOS.h"
   #include "moTypes.h"

UI32 GetAvailableHeap( UI32* pMaxChunk );

UI32 GetCreatorID();

void Sleep( UI32 ulMSec );

#endif //ESI_PALM



#ifdef _WIN32

#include <winbase.h>

bool Launch( const TCHAR* pcFileName,
             const TCHAR* pcCmdLine );


class CmoApp
{
public:
   CmoString ExeName();
};// CmoApp


class CmoMath
{
public:
   static I32 Round( R8 dValue );
   static I32 Trunc( R8 dValue );
};// CmoMath

#endif


void SetBit( I8    *pcBits,
             I32   lBitNum,
             bool  bSetBit );

bool IsBitSet( I8    *pcBits,
               I32   lBitNum );

void InitTempFile();
void GetTempFile( CmoString *pcsTempFileName );
const TCHAR *GetTempFolder();
const TCHAR *GetDataFolder();
const TCHAR *GetMocaQFolder();

void CreateDirectoryStructure( const TCHAR *pcDirectory );

inline UI16 GetCharacterEncoding()
{
#if defined( MOCLIENT_IPHONE ) || defined( USE_MOBILE_OBJECTS )
   return 65001; // the value for CP_UTF8
#else
   return CHAR_ENCODING_UNICODE;
#endif
}

#endif // mo_utils_H_INCLUDED
