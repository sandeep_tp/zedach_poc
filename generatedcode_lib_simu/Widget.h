//
//  Widget.h
//  iota
//
//  Created by Alexey Piterkin on 3/16/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Foundation/NSObjCRuntime.h>
#import "LocalSubstitutionCache.h"

@interface Widget : NSObject {
	I32 moduleId;                        // Module ID for the widget
	I32 moduleVersion;                   // Widget version

	I32 iconIndex;                       // Index of the icon used
   NSString * status;
	NSString * widgetXML;                 // XML Definition of the widget

   NSString * customIcon;
   NSString * clientVariables;
   
   NSString * moduleName;
   NSString * displayName;
	BOOL deleteMsgAfterProcessing;        // Should delete message after processing it
	BOOL supportCredentialsCache;         // Does this widget support credentials cache
   BOOL supportSharedStorage;            // Does this widget support shared storage
	NSString * credentialsScreen;         // Key name of the credentials screen that is used to prompt for credentials
	NSString * credentialsKey;      
   NSString * sharedStorageKey;
   I32 credentialsId;                   // record id for the key
   BOOL requiresActivation;
   BOOL isActivated;
   NSString * activationKey;
   NSString * activationScreen;          
   BOOL clientInvocable;
   BOOL markMsgAsProcessed;
   NSString * processedScreen;           // Not used
   BOOL saveResponseInSentItems;         // Not used

   BOOL credentialsExpired;
   NSString * username;
   NSData * password;
   BOOL isDefault;
   BOOL isPrepackaged;
   NSString* prepackagedPath;
}

@property (nonatomic, readonly) NSString * widgetXML;
@property (nonatomic, readonly) NSString * customIcon;
@property (nonatomic, readonly) NSString * clientVariables;
@property (nonatomic, readonly) NSString * moduleName;
@property (nonatomic, readonly) NSString * displayName;
@property (nonatomic, readonly) I32 moduleVersion;
@property (nonatomic, readonly) I32 moduleId;
@property (nonatomic, readonly) BOOL supportCredentialsCache;
@property (nonatomic, readonly) BOOL supportSharedStorage;
@property (nonatomic, readonly) NSString * credentialsScreen;
@property (nonatomic, readonly) NSString * credentialsKey;
@property (nonatomic, readonly) NSString * sharedStorageKey;
@property (nonatomic, readonly) NSString * username;
@property (nonatomic, readonly) NSData * password;
@property (nonatomic, readonly) BOOL requiresActivation;
@property (nonatomic, readonly) BOOL isActivated;
@property (nonatomic, readonly) NSString * activationKey;
@property (nonatomic, readonly) NSString * activationScreen;
@property (nonatomic, readonly) BOOL clientInvocable;
@property (nonatomic, readonly) I32 credentialsId;
@property (nonatomic, readonly) NSString * status;
@property (nonatomic, readonly) BOOL deleteMsgAfterProcessing;
@property (nonatomic, readonly) NSString * processedScreen;
@property (nonatomic, readonly) BOOL markMsgAsProcessed;
@property (nonatomic, readonly) BOOL saveResponseInSentItems;
@property (nonatomic, readonly) I32 iconIndex;
@property (nonatomic, readonly) BOOL requiresNewCredentials;
@property (nonatomic, readonly) BOOL isDefault;
@property (nonatomic, readonly) BOOL isPrepackaged;
@property (nonatomic, readonly) NSString* prepackagedPath;

#ifndef TESTAPP
+ (Widget*)widgetWithModuleId:(I32)moduleId version:(I32)moduleVersion;
#endif

#ifdef TESTAPP
+ (Widget*)widgetWIthXML:(NSString*)xml;
#endif

- (void)setUsername:(NSString*)aUsername password:(NSData*)aPassword;
- (void)markActivated;
- (void)unableToOpenWidget;
- (NSString *)getSharedStorageKey;

- (NSComparisonResult)compareByName:(Widget*)widget;

@end
