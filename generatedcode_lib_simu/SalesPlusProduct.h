
/* Copyright (c) Sybase, Inc. 2010-2013   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code;OT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES;
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */

#import "sybase_sup.h"


#import "SUPClassWithMetaData.h"
#import "SUPAbstractEntityRBS.h"
#import "SUPMobileBusinessObject.h"
#import "SUPEntityDelegate.h"


@class SUPObjectList;
@class SUPEntityMetaDataRBS;
@class SUPEntityDelegate;
@class SUPClassMetaDataRBS;
@class SUPQuery;

// public interface declaration, can be used by application. 
/*!
 @class SalesPlusProduct
 @abstract This class is part of package "ERPSalesPlus_SuperUser:1.0"
 @discussion Generated by Sybase Unwired Platform, compiler version 3.0.13.40
*/

@interface SalesPlusProduct : SUPAbstractEntityRBS<SUPMobileBusinessObject, SUPClassWithMetaData>
{
@private
    NSString* _MATNR;
    NSString* _BASE_UOM;
    NSNumber* _GROSS_WT;
    NSString* _UNIT_OF_WT;
    SUPObjectList* _productSalesRels;
    SUPObjectList* _productAttachmentss;
    SUPObjectList* _productCategoryRelationships;
    SUPObjectList* _productConditionItems;
    SUPObjectList* _productTexts;
    SUPObjectList* _productUnitss;
    SUPObjectList* _reportingMasterProducts;
    SUPBigString* _cvpOperation;
    SUPBigString* _cvpOperationLobs;
    int64_t _surrogateKey;
    int64_t _cvpOperationLength;
    int64_t _cvpOperationLobsLength;
}


@property(retain,nonatomic) NSString* MATNR;
@property(retain,nonatomic) NSString* BASE_UOM;
@property(retain,nonatomic) NSNumber* GROSS_WT;
@property(retain,nonatomic) NSString* UNIT_OF_WT;
@property(retain,nonatomic) SUPObjectList* productSalesRels;
@property(retain,nonatomic) SUPObjectList* productAttachmentss;
@property(retain,nonatomic) SUPObjectList* productCategoryRelationships;
@property(retain,nonatomic) SUPObjectList* productConditionItems;
@property(retain,nonatomic) SUPObjectList* productTexts;
@property(retain,nonatomic) SUPObjectList* productUnitss;
@property(retain,nonatomic) SUPObjectList* reportingMasterProducts;
@property(retain,nonatomic) SUPBigString* cvpOperation;
@property(retain,nonatomic) SUPBigString* cvpOperationLobs;
@property(assign,nonatomic) int64_t surrogateKey;
@property(assign,nonatomic) int64_t cvpOperationLength;
@property(assign,nonatomic) int64_t cvpOperationLobsLength;


- (id) init;
- (void)dealloc;
/*!
  @method 
  @abstract Creates a new autoreleased instance of this class
  @discussion
 */
+ (SalesPlusProduct*)getInstance;


+ (SUPEntityMetaDataRBS*)metaData;
+ (void) registerCallbackHandler:(NSObject<SUPCallbackHandler>*)handler;
+ (NSObject<SUPCallbackHandler>*)callbackHandler;
- (SUPClassMetaDataRBS*)getClassMetaData;
/*!
  @method 
  @abstract Sets relationship attributes to null to save memory (they will be retrieved from the DB on the next getter call or property reference)
  @discussion
  @throws SUPPersistenceException
 */
- (void)clearRelationshipObjects;
/*!
  @method 
  @abstract Returns the entity for the primary key value passed in, or null if the entity is not found.
  @discussion
  @throws SUPPersistenceException
 */
+ (SalesPlusProduct*)find:(int64_t)id_;
/*!
  @method 
  @abstract Returns an SUPObjectList of entity rows satisfying this query
  @discussion
  @throws SUPPersistenceException
 */
+ (SUPObjectList*)findWithQuery:(SUPQuery*)query;
/*!
  @method 
  @abstract Returns the primary key for this entity.
  @discussion
 */
- (int64_t)_pk;
/*!
  @method 
  @abstract Returns the entity for the primary key value passed in; throws an exception if the entity is not found.
  @discussion
  @throws SUPPersistenceException
 */
+ (SalesPlusProduct*)load:(int64_t)id;
/*!
  @method 
  @abstract Returns an SUPObjectList of log records for this entity.
  @discussion
  @throws SUPPersistenceException
 */
- (SUPObjectList*)getLogRecords;
/*!
  @method 
  @abstract Return a string description of this entity.
  @discussion
 */
- (NSString*)toString;
/*!
  @method 
  @abstract Return a string description of this entity.
  @discussion
 */
- (NSString*)description;
/*!
  @method 
  @abstract Submit all the pending operations (ready for sending to server) for objects of this type.
  @discussion
  @throws SUPPersistenceException
 */
+ (void) submitPendingOperations;
/*!
  @method 
  @abstract Cancel any pending changes for objects of this type.
  @discussion
  @throws SUPPersistenceException
 */
+ (void) cancelPendingOperations;
/*!
  @method 
  @abstract If this object has been modified on the client, returns the original state of the object.
  @discussion
  @throws SUPPersistenceException
 */
- (SalesPlusProduct*)getOriginalState;
/*!
  @method 
  @abstract If this object has been modified on the client, returns the downloaded state of the object.
  @discussion
  @throws SUPPersistenceException
 */
- (SalesPlusProduct*)getDownloadState;
/*!
  @method 
  @abstract Return the name of the last operation executed on this object.
  @discussion
 */
- (SUPString)getLastOperation;
/*!
  @method 
  @abstract Return a list of SalesPlusProduct objects
  @discussion
  @throws SUPPersistenceException
 */
+ (SUPObjectList*)getPendingObjects;
/*!
  @method 
  @abstract Return a list of SalesPlusProduct objects
  @discussion
  @throws SUPPersistenceException
 */
+ (SUPObjectList*)getPendingObjects:(int32_t)skip take:(int32_t)take;
/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @throws SUPPersistenceException
 */

+ (SUPObjectList*)findAll;

/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param skip
  @param take
  @throws SUPPersistenceException
 */

+ (SUPObjectList*)findAll:(int32_t)skip take:(int32_t)take;
/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param mATNR
  @throws SUPPersistenceException
 */

+ (SalesPlusProduct*)findByPrimaryKey:(NSString*)mATNR;
/*!
  @method
  @abstract Generated instance method of type CREATE
  @throws SUPPersistenceException
 */
- (void)create_dcn;
/*!
  @method
  @abstract Generated class method 
  @param query
  @throws SUPPersistenceException
 */
+ (int32_t)getSize:(SUPQuery*)query;
/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param query
  @throws SUPPersistenceException
 */

- (SUPObjectList*)getProductSalesRelsFilterBy:(SUPQuery*)query;

/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param query
  @param skip
  @param take
  @throws SUPPersistenceException
 */

- (SUPObjectList*)getProductSalesRelsFilterBy:(SUPQuery*)query skip:(int32_t)skip take:(int32_t)take;
/*!
  @method
  @abstract Generated instance method 
  @param query
  @throws SUPPersistenceException
 */
- (int32_t)getProductSalesRelsSize:(SUPQuery*)query;
/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param query
  @throws SUPPersistenceException
 */

- (SUPObjectList*)getProductAttachmentssFilterBy:(SUPQuery*)query;

/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param query
  @param skip
  @param take
  @throws SUPPersistenceException
 */

- (SUPObjectList*)getProductAttachmentssFilterBy:(SUPQuery*)query skip:(int32_t)skip take:(int32_t)take;
/*!
  @method
  @abstract Generated instance method 
  @param query
  @throws SUPPersistenceException
 */
- (int32_t)getProductAttachmentssSize:(SUPQuery*)query;
/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param query
  @throws SUPPersistenceException
 */

- (SUPObjectList*)getProductCategoryRelationshipsFilterBy:(SUPQuery*)query;

/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param query
  @param skip
  @param take
  @throws SUPPersistenceException
 */

- (SUPObjectList*)getProductCategoryRelationshipsFilterBy:(SUPQuery*)query skip:(int32_t)skip take:(int32_t)take;
/*!
  @method
  @abstract Generated instance method 
  @param query
  @throws SUPPersistenceException
 */
- (int32_t)getProductCategoryRelationshipsSize:(SUPQuery*)query;
/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param query
  @throws SUPPersistenceException
 */

- (SUPObjectList*)getProductConditionItemsFilterBy:(SUPQuery*)query;

/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param query
  @param skip
  @param take
  @throws SUPPersistenceException
 */

- (SUPObjectList*)getProductConditionItemsFilterBy:(SUPQuery*)query skip:(int32_t)skip take:(int32_t)take;
/*!
  @method
  @abstract Generated instance method 
  @param query
  @throws SUPPersistenceException
 */
- (int32_t)getProductConditionItemsSize:(SUPQuery*)query;
/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param query
  @throws SUPPersistenceException
 */

- (SUPObjectList*)getProductTextsFilterBy:(SUPQuery*)query;

/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param query
  @param skip
  @param take
  @throws SUPPersistenceException
 */

- (SUPObjectList*)getProductTextsFilterBy:(SUPQuery*)query skip:(int32_t)skip take:(int32_t)take;
/*!
  @method
  @abstract Generated instance method 
  @param query
  @throws SUPPersistenceException
 */
- (int32_t)getProductTextsSize:(SUPQuery*)query;
/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param query
  @throws SUPPersistenceException
 */

- (SUPObjectList*)getProductUnitssFilterBy:(SUPQuery*)query;

/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param query
  @param skip
  @param take
  @throws SUPPersistenceException
 */

- (SUPObjectList*)getProductUnitssFilterBy:(SUPQuery*)query skip:(int32_t)skip take:(int32_t)take;
/*!
  @method
  @abstract Generated instance method 
  @param query
  @throws SUPPersistenceException
 */
- (int32_t)getProductUnitssSize:(SUPQuery*)query;
/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param query
  @throws SUPPersistenceException
 */

- (SUPObjectList*)getReportingMasterProductsFilterBy:(SUPQuery*)query;

/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param query
  @param skip
  @param take
  @throws SUPPersistenceException
 */

- (SUPObjectList*)getReportingMasterProductsFilterBy:(SUPQuery*)query skip:(int32_t)skip take:(int32_t)take;
/*!
  @method
  @abstract Generated instance method 
  @param query
  @throws SUPPersistenceException
 */
- (int32_t)getReportingMasterProductsSize:(SUPQuery*)query;


@end
typedef SUPObjectList SalesPlusProductList;