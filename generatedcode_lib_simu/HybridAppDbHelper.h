//  Created by Brad Wilder on 10/31/12.
//  Copyright 2012 Sybase. All rights reserved.

#import "HybridAppViewController.h"
#import "pimdb.h"

@interface HybridAppDbHelper : NSObject

+ (HybridApp*) hybridAppFromWidget:(Widget*)widget;
+ (HybridApp*) hybridAppFromEmailItem:(EmailItem*)emailItem withWigdetItem:(WidgetItem*)widgetItem;

@end
