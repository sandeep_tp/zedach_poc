/*
 
 Copyright (c) Sybase, Inc. 2010-2013   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */


#import "sybase_core.h"

#import "SUPStringUtil.h"
#import "SUPDataType.h"
#import "SUPObjectList.h"
#import "SUPStringList.h"

#import "SUPBasicTypes.h"

@interface SUPBasicTypes : NSObject

+ (SUPBasicTypes*)instance;
- (SUPBasicTypes*)init;
+ (SUPDataType*)t_void;
+ (void)setT_void:(SUPDataType*)value;
+ (SUPDataType*)n_void;
+ (void)setN_void:(SUPDataType*)value;
+ (SUPDataType*)t_object;
+ (void)setT_object:(SUPDataType*)value;
+ (SUPDataType*)n_object;
+ (void)setN_object:(SUPDataType*)value;
+ (SUPDataType*)t_boolean;
+ (void)setT_boolean:(SUPDataType*)value;
+ (SUPDataType*)n_boolean;
+ (void)setN_boolean:(SUPDataType*)value;
+ (SUPDataType*)t_string;
+ (void)setT_string:(SUPDataType*)value;
+ (SUPDataType*)n_string;
+ (void)setN_string:(SUPDataType*)value;
+ (SUPDataType*)t_binary;
+ (void)setT_binary:(SUPDataType*)value;
+ (SUPDataType*)n_binary;
+ (void)setN_binary:(SUPDataType*)value;
+ (SUPDataType*)t_char;
+ (void)setT_char:(SUPDataType*)value;
+ (SUPDataType*)n_char;
+ (void)setN_char:(SUPDataType*)value;
+ (SUPDataType*)t_byte;
+ (void)setT_byte:(SUPDataType*)value;
+ (SUPDataType*)n_byte;
+ (void)setN_byte:(SUPDataType*)value;
+ (SUPDataType*)t_short;
+ (void)setT_short:(SUPDataType*)value;
+ (SUPDataType*)n_short;
+ (void)setN_short:(SUPDataType*)value;
+ (SUPDataType*)t_int;
+ (void)setT_int:(SUPDataType*)value;
+ (SUPDataType*)n_int;
+ (void)setN_int:(SUPDataType*)value;
+ (SUPDataType*)t_long;
+ (void)setT_long:(SUPDataType*)value;
+ (SUPDataType*)n_long;
+ (void)setN_long:(SUPDataType*)value;
+ (SUPDataType*)t_integer;
+ (void)setT_integer:(SUPDataType*)value;
+ (SUPDataType*)n_integer;
+ (void)setN_integer:(SUPDataType*)value;
+ (SUPDataType*)t_decimal;
+ (void)setT_decimal:(SUPDataType*)value;
+ (SUPDataType*)n_decimal;
+ (void)setN_decimal:(SUPDataType*)value;
+ (SUPDataType*)t_float;
+ (void)setT_float:(SUPDataType*)value;
+ (SUPDataType*)n_float;
+ (void)setN_float:(SUPDataType*)value;
+ (SUPDataType*)t_double;
+ (void)setT_double:(SUPDataType*)value;
+ (SUPDataType*)n_double;
+ (void)setN_double:(SUPDataType*)value;
+ (SUPDataType*)t_date;
+ (void)setT_date:(SUPDataType*)value;
+ (SUPDataType*)n_date;
+ (void)setN_date:(SUPDataType*)value;
+ (SUPDataType*)t_time;
+ (void)setT_time:(SUPDataType*)value;
+ (SUPDataType*)n_time;
+ (void)setN_time:(SUPDataType*)value;
+ (SUPDataType*)t_dateTime;
+ (void)setT_dateTime:(SUPDataType*)value;
+ (SUPDataType*)n_dateTime;
+ (void)setN_dateTime:(SUPDataType*)value;
+ (SUPDataType*)n_bigBinary;
+ (void)setN_bigBinary:(SUPDataType*)value;
+ (SUPDataType*)n_bigString;
+ (void)setN_bigString:(SUPDataType*)value;
- (void)add:(SUPString)key :(SUPDataType*)value;
- (void)remove:(SUPString)key;
- (void)clear;
- (SUPBoolean)containsKey:(SUPString)key;
- (SUPDataType*)item:(SUPString)key;
- (SUPStringList*)keys;
- (SUPObjectList*)values;
- (SUPInt)size;
- (NSMutableDictionary*)internalMap;
- (void)dealloc;


@end
