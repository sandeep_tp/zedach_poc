/*******************************************************************************
* Source File : WorkflowErrorConsts.h
* Date Created: 11/17/2011
* Copyright   : 2000 - 2011, Sybase, Inc.
* Description : Workflow constants
* Notes       :
*******************************************************************************/

#define WF_UNKNOWN_ERROR                      1 // "unknown error" 
#define WF_ATTACHMENT_NOT_DOWNLOADED        100 //"Attachment has not been downloaded" 
#define WF_UNKNOWN_MIME_TYPE                101 //"Unknow MIME type" 
#define WF_FILENAME_NO_EXTENSION            102 //"File name without extension" 
#define WF_REQUIRED_PARAMETER_NOT_AVAILABLE 103 //"Required parameter is not available" 
#define WF_CERTIFICATE_NOT_SELECTED         104 //"No certificate is selected by user" 
#define WF_UNSUPPORTED_ATTACHMENT_TYPE      105 //attachment type is not supported 
#define WF_SSOCERT_EXCEPTION                106 //SSO Certificate manager exception 
#define WF_FAIL_TO_SAVE_CREDENTIAL          107 //Fail to save credential 
#define WF_FAIL_TO_SAVE_CERTIFICATE         108 //Fail to save certificate 
#define WF_DEVICE_NOT_CONNECTED             109 //Device is not connected
#define WF_RESPONSE_TOO_LARGE               110 //Javascript var will not be able to handle response
#define WF_NAVIGATION_ERROR                 111 //Invalid url for showUrlInBrowser request (CR 689659)
#define WF_INVALID_COMMON_NAME              112 //Invalid Common Name passed to get certificate from Afaria
#define WF_SHARED_STORAGE_DISABLED          120 //shared storage disabled for the currrent sharestorage query
#define WF_REQUIRED_INITIALIZE_HWA_WITH_SERVER 113 //The HWA was needed initialized with server

// Result codes for camera API
#define WF_RESULT_CODE_PICTURE_NO_ERROR         0
#define WF_RESULT_CODE_PICTURE_NOT_SUPPORTED    -1
#define WF_RESULT_CODE_PICTURE_IN_PROGRESS      -2
#define WF_RESULT_CODE_PICTURE_USER_REJECT      -3
#define WF_RESULT_CODE_PICTURE_BAD_OPTIONS      -4
#define WF_RESULT_CODE_PICTURE_TOO_LARGE        -5
#define WF_RESULT_CODE_PICTURE_UNKNOWN          -6

// WorkflowClient.dll com exception
#define WF_CLIENT_COMM_EXCEPTION              1053