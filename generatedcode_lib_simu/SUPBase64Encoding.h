/*
 
 Copyright (c) Sybase, Inc. 2009-2013   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */

#import "sybase_core.h"

@class SUPAbstractOperationException;
@class SUPBase64EncodingException;
@class SUPByteList;
@class SUPJsonObject;
@class SUPNumberUtil;
@class SUPStringList;
@class SUPStringUtil;

@class SUPBase64Encoding;


/*!
 @class
 @abstract Contains class methods for encoding and decoding Base64-encoded strings.
 @discussion
 */
@interface SUPBase64Encoding : NSObject
{
}

/*!
 @method
 @abstract Decodes a Base64-encoded string.
 @discussion If the string contains invalid Base64 characters, an exception will be thrown.
 @param text The Base64-encoded string.
 @result An NSData object containing the decoded binary data.
 @throws @link //apple_ref/occ/cl/SUPBase64EncodingException SUPBase64EncodingException @/link
 */
+ (SUPBinary)decode:(SUPString)text;

/*!
 @method
 @abstract Encodes data as a Base64-encoded string.
 @discussion
 @param data An NSData object containing the bytes to be encoded.
 @result An NSString with the Base64-encoded version of the input.
 */
+ (SUPString)encode:(SUPBinary)data;

@end
