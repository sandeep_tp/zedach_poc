//
//  WidgetParser.h
//  iota
//
//  Created by Alexey Piterkin on 2/6/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WidgetXmlParser.h"

@class WidgetDefinition;


@interface WidgetParser : WidgetXmlParser {
}

@property (nonatomic, readonly) WidgetDefinition * widget;

@end
