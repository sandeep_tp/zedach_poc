/*
 Copyright (c) Sybase, Inc. 2012   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 */

#import "SUPE2ETraceService.h"
#import "SUPE2ETraceMessage.h"
#import "SUPE2ETraceLevel.h"
#import "SUPBTXUploadHandler.h"

@class SUPE2ETraceTransaction;
@class SUPE2ETraceController;
@class SUPE2ETraceStep;

/**
 *  header key to be used for sending passport
 */
extern NSString * const SUP_E2E_PASSPORT_HEADER_KEY;

/*!
 @class
 @abstract Class that implements the SUPE2ETraceService protocol.
 @discussion
 */
@interface SUPE2ETraceServiceImpl : NSObject<SUPE2ETraceService>
{
    @private
    SUPE2ETraceController *_traceController;
    SUPE2ETraceLevel _traceLevel;
    SUPE2ETraceTransaction *_transaction;
    SUPE2ETraceStep *_step; 
}

+ (SUPE2ETraceServiceImpl *)getInstance;

- (void)setTraceLevel:(SUPE2ETraceLevel)traceLevel;


- (void)startTrace;


- (NSData *)stopTrace;
    
- (BOOL)isTraceEnabled;

- (BOOL)isTraceStopped;

- (BOOL)uploadTrace:(NSData *)btx;

/*!
 @method 
 @abstract Creates and returns an autorelease instance of SUPE2ETraceMessage which can be used to add the request/response details.
 @return new SUPE2ETraceMessage object
 */
-(SUPE2ETraceMessage *)createNewTraceMessage;

/*!
 @method 
 @abstract Adds the request trace (content, headers, data statistics) to the BTX.  Needs to be called by the application code dealing with request-response handling after filling required details into the SUPE2ETraceMessage instance obtained using createNewTraceMessage call before sending the request.
 @param traceRequest request details/statistics
 */
- (void)addMessage:(SUPE2ETraceMessage *)traceMessage;   

@end
