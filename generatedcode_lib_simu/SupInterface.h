/*
Copyright (c) Sybase, Inc. 2010 All rights reserved. 

In addition to the license terms set out in the Sybase License Agreement for 
the Sybase Unwired Platform ("Program"), the following additional or different 
rights and accompanying obligations and restrictions shall apply to the source 
code in this file ("Code"). Sybase grants you a limited, non-exclusive, 
non-transferable, revocable license to use, reproduce, and modify the Code 
solely for purposes of (i) maintaining the Code as reference material to better 
understand the operation of the Program, and (ii) development and testing of 
applications created in connection with your licensed use of the Program. 
The Code may not be transferred, sold, assigned, sublicensed or otherwise 
conveyed (whether by operation of law or otherwise) to another party without 
Sybase's prior written consent. The following provisions shall apply to any 
modifications you make to the Code: (i) Sybase will not provide any maintenance 
or support for modified Code or problems that result from use of modified Code; 
(ii) Sybase expressly disclaims any warranties and conditions, express or 
implied, relating to modified Code or any problems that result from use of the 
modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING 
TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN 
IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
to indemnify, hold harmless, and defend Sybase from and against any claims or 
lawsuits, including attorney's fees, that arise from or are related to the 
modified Code or from use of the modified Code.
*/

#pragma once

//////////////////////////////////////
// definitions of interface functions

//////////////////////////////////////
// function pointers passed from handler to SUPObj DLL in Initialize()
typedef void (*PFNLogText)( int iLevel,               // logging level 1-5
                            const char* pcLogText ); // text to log

typedef int (*PFNQueueAsyncMethodCallForServer)( 
                             const char* pcHeader,   // header argument
                             const unsigned char* pbData,      // data argument
                             unsigned int uiByteCount // number of bytes in data argument
                             );

//////////////////////////////////////
// functions implemented by SUPObj DLL

// Initialize()
typedef  void (*PFNInitialize)(  
                                  PFNQueueAsyncMethodCallForServer fnQueueAsyncMethodCallForServer, 
                                  PFNLogText fnLogText, 
                                  char *pcApplicationName // a pointer to a buffer of 16 characters that may be set to a value that
                                  // is to be added to the DeviceID in order to allow more than one application 
                                  // to run on a single iPhone device
                               );
// Dispose()
typedef void (*PFNDispose)();

// AsyncMethodCallFromServer()
typedef void (*PFNAsyncMethodCallFromServer)( 
   const char* pcHeader,     // header argument received
   const unsigned char* pbData,        // data argument received
   unsigned int uiByteCount   // length of data argument
   );

// RefreshAllData()
typedef void (*PFNRefreshAllData)();

// ConnectionStateChanged
typedef void (*PFNConnectionStateChanged)( int iConnectionStatus, int iConnectionType, int iError, const char *pcErrorMessage );


//////////////////////////////////////
// Constants for MOCA communication
const char* const SUP_CLIENT_OBJ      = "SUP";
const char* const SUP_CLIENT_ASYNC_RESPONSE = "SUPAsyncResponse";
const char* const SUP_CLIENT_METHOD   = "Message";
const char* const SUP_SERVER_OBJ      = "monet:SUPBridge.dll:SUPBridge";
const char* const SUP_SERVER_METHOD   = "MessageFromClient" ;
const char* const SUP_HEADER_ARG      = "Header";
const char* const SUP_DATA_ARG        = "Data";

// the maximum delay time when retrying a request that failed (seconds)
static const int MAX_SUP_RETRY_DELAY = 60*30; // 30 minutes
 

static const int SUPDLL_CONNECTED                = 1;   // device connected
static const int SUPDLL_DISCONNECTED             = 2;   // device not connected
static const int SUPDLL_DEVICEINFLIGHTMODE       = 3;   // device not connected because of flight mode
static const int SUPDLL_DEVICEOUTOFNETWORKCOVERAGE = 4; // device not connected because no network coverage
static const int SUPDLL_WAITINGTOCONNECT         = 5;   // device not connected and waiting to retry a connection      
static const int SUPDLL_DEVICEROAMING            = 6;   // device not connected because roaming was set to false and device is roaming
static const int SUPDLL_DEVICELOWSTORAGE         = 7;   // device not connected because of low storage.  The state may change soon to something 
                                                        // else like SUPDLL_DISCONNECTED, but the device will not connect again until storage is
                                                        // cleared

static const int SUPDLL_CONNECTION_TYPE_ALWAYS_ON      = 1; // iPhone has only one connection type


