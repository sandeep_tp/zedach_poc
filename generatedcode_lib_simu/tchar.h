/*
Copyright (c) Sybase, Inc. 2012 All rights reserved. 

In addition to the license terms set out in the Sybase License Agreement for 
the Sybase Unwired Platform ("Program"), the following additional or different 
rights and accompanying obligations and restrictions shall apply to the source 
code in this file ("Code"). Sybase grants you a limited, non-exclusive, 
non-transferable, revocable license to use, reproduce, and modify the Code 
solely for purposes of (i) maintaining the Code as reference material to better 
understand the operation of the Program, and (ii) development and testing of 
applications created in connection with your licensed use of the Program. 
The Code may not be transferred, sold, assigned, sublicensed or otherwise 
conveyed (whether by operation of law or otherwise) to another party without 
Sybase's prior written consent. The following provisions shall apply to any 
modifications you make to the Code: (i) Sybase will not provide any maintenance 
or support for modified Code or problems that result from use of modified Code; 
(ii) Sybase expressly disclaims any warranties and conditions, express or 
implied, relating to modified Code or any problems that result from use of the 
modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING 
TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN 
IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
to indemnify, hold harmless, and defend Sybase from and against any claims or 
lawsuits, including attorney's fees, that arise from or are related to the 
modified Code or from use of the modified Code.
*/

/******************************************************************************
*    Copyright 2012 Sybase, Inc
*    Source File            :  tchar.h
*    Created By             :  Eric Nelson
*    Date Created           :  4 June 2008
*    Platform Dependencies  :  iPhone Platform (OS)
*    Description            :  This is the TCHAR header file for
*                              iPhone Platform
******************************************************/
#ifndef _TCHAR_H_
#define _TCHAR_H_

#define _T(x) x

#define LPCTSTR const TCHAR*


#if defined( MOCLIENT_IPHONE)  // do not define for projects other than iMO
#include "ETypes.h"
#include "platmacro.h"
#include "EStringHelper.h"

// CPP_Brett_Unresolved   #ifdef __cplusplus
// CPP_Brett_Unresolved   extern "C" {
// CPP_Brett_Unresolved   #endif

#define _T(x) x

inline size_t _tcslen(const TCHAR *p)
{ return(EStringHelper::EStrlen(p)); }
inline TCHAR *_tcscpy(TCHAR *pnew, const TCHAR *p)
{ return(EStringHelper::EStrcpy(pnew, p)); }
inline TCHAR *_tcsncpy(TCHAR *pnew, const TCHAR *p, size_t n)
{ return(EStringHelper::EStrncpy(pnew, p, n)); }
inline TCHAR *_tcscat(TCHAR *pnew, const TCHAR *p)
{ return(EStringHelper::EStrcat(pnew, p)); };
inline int _tcscmp(const TCHAR *pstr, const TCHAR *rhs)
{ return(EStringHelper::EStrcmp(pstr, rhs)); }
inline int _tcsicmp(const TCHAR *pstr, const TCHAR *rhs)
{ return(EStringHelper::EStricmp(pstr, rhs)); }
inline TCHAR *_tcsstr(const TCHAR *pstr, const TCHAR *s)
{ return(TCHAR*)(EStringHelper::EStrstr((TCHAR*)pstr, (TCHAR*)s)); }
inline TCHAR *_tcschr(const TCHAR *p, unsigned c)
{ return(TCHAR*)(EStringHelper::EStrchr(p, (TCHAR) c)); }
inline int _istspace(TCHAR cnew)
{ return(EStringHelper::EStrisspace(cnew)); }
inline char* _itot(int value, char *string, int radix)
{   sprintf(string, _T("%d"), value);
   return string;
}
#endif



#define _stprintf sprintf
#define _tcstol strtol
#define _tcstoul strtoul
#define _tcstod strtod
#define _tcslwr strlwr
#define _tcsupr strupr
#define _tcsncmp strncmp
#define _tcsrchr strchr
#define _istdigit isdigit
#define _ttoi atoi

#endif

