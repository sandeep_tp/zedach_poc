#import "sybase_core.h"
#import "SUPBaseException.h"


@class SUPStringList;
@class SUPJsonRpcException;

@interface SUPJsonRpcException : SUPBaseException
{
    SUPString _errorMessage;
    id _errorContent;
}

/*!
 @method jsonRpcExceptionWithErrorCode
 @abstract  Instantiates an SUPJsonRpcException object
 @param  errorCode The error code value
 @result The SUPJsonRpcException object.
 @discussion
 */
+ (SUPJsonRpcException*)jsonRpcExceptionWithErrorCode:(int)errorCode;

/*!
 @method jsonRpcExceptionWithErrorCode
 @abstract  Instantiates an SUPJsonRpcException object
 @param  errorCode The error code value
 @param  message the error message
 @result The SUPJsonRpcException object.
 @discussion
 */
+ (SUPJsonRpcException *)jsonRpcExceptionWithErrorCode:(int)errorCode message:(NSString *)message;


/*!
 @method jsonRpcExceptionWithErrorCode
 @abstract  Instantiates an SUPJsonRpcException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @result The SUPJsonRpcException object.
 @discussion
 */
+ (SUPJsonRpcException*)jsonRpcExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments;

/*!
 @method jsonRpcExceptionWithErrorCode
 @abstract  Instantiates an SUPJsonRpcException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @param  cause The cause of the exception
 @result The SUPJsonRpcException object.
 @discussion
 */
+ (SUPJsonRpcException*)jsonRpcExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments cause:(NSException *)cause;


+ (SUPJsonRpcException*)getInstance;

- (SUPJsonRpcException*)init;
@property(readwrite, copy,nonatomic) SUPString errorMessage;
@property(readwrite, retain,nonatomic) id errorContent;
- (SUPBoolean)isError;

@end
