/*
 
 Copyright (c) Sybase, Inc. 2010-2012   All rights reserved.                                    
 
 In addition to the license terms set out in the Sybase License Agreement for 
 the Sybase Unwired Platform ("Program"), the following additional or different 
 rights and accompanying obligations and restrictions shall apply to the source 
 code in this file ("Code").  Sybase grants you a limited, non-exclusive, 
 non-transferable, revocable license to use, reproduce, and modify the Code 
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of 
 applications created in connection with your licensed use of the Program.  
 The Code may not be transferred, sold, assigned, sublicensed or otherwise 
 conveyed (whether by operation of law or otherwise) to another party without 
 Sybase's prior written consent.  The following provisions shall apply to any 
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or 
 implied, relating to modified Code or any problems that result from use of the 
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
 to indemnify, hold harmless, and defend Sybase from and against any claims or 
 lawsuits, including attorney's fees, that arise from or are related to the 
 modified Code or from use of the modified Code. 
 
 */

#import <UIKit/UIKit.h>
#import "SUPShortList.h"
#import "SUPLoginCertificate.h"
#import "SUPLoginCredentials.h"
#import "SUPStringProperties.h"
#import "SUPConnectionStatus.h"
#import "SUPRegistrationStatus.h"
#import "SUPDeviceCondition.h"

#define SUP_NOTIFICATION_CONTINUE 0
#define SUP_NOTIFICATION_CANCEL 1

/*!
 @protocol
 @abstract   SUPApplicationCallback protocol
 @discussion A callback handler for events of interest to a mobile application. These callback methods are considered part of the triggered SUP operations. Custom implementations of these callback methods should not do complicated operations that could affect the completion of these operations. For example, when onRegistrationStatusChanged is invoked, it is considered part of the device registration operation. Device registration is not done until onRegistrationStatusChanged is returned. If it is required to do complex operations, it is recommended to create another thread to do them.
 */
@protocol SUPApplicationCallback

/*!
 @method     
 @abstract   Invoked when one or more applicationSettings have been changed by the server administrator.
 @discussion 
 */
- (void)onApplicationSettingsChanged:(SUPStringList*)names;

/*!
 @method     
 @abstract   Invoked when the connectionStatus changes.
 @discussion 
 */
- (void)onConnectionStatusChanged:(SUPConnectionStatusType)connectionStatus :(int32_t)errorCode :(NSString*)errorMessage;

/*!
 @method     
 @abstract   Invoked when the registrationStatus changes.
 @discussion 
 */
- (void)onRegistrationStatusChanged:(SUPRegistrationStatusType)registrationStatus :(int32_t)errorCode :(NSString*)errorMessage;

/*!
 @method     
 @abstract   Invoked when a condition is detected on the mobile device that may be of interest to the application or the application user.
 @discussion 
 */
- (void)onDeviceConditionChanged :(SUPDeviceConditionType)condition;

/*!
 @method
 @abstract Invoked when an HTTP communication server rejects HTTP communication with an error code.
 @discussion 
 If error code is 401, it might be an authentication challenge that the httpCredential in the ConnectionProperties should be provided.
 If the httpCredentials are not valid and have not been changed since last challenge, then the connection attempt will be canceled.
 @param errorCode Error code returned by the HTTP server. For example: code 401 for authentication failure, code 403 for authorization failure.
 @param errorMessage Error message returned by the HTTP server.
 @param responseHeaders Response headers returned by the HTTP server.
 */
- (void)onHttpCommunicationError :(int32_t)errorCode :(NSString*)errorMessage :(SUPStringProperties*)responseHeaders;


/*!
 @method
 @abstract Invoked when download resource bundle complete
 @discussion 
 @param customizationBundleID  The customization resource name. If null, application default bundled resource is downloaded
 @param errorCode If download fail, return error code. If download success, return 0
 @param errorMessage If download fail, return error message. If download success, return ""
 */
- (void)onCustomizationBundleDownloadComplete : (NSString*) customizationBundleID :  (int32_t) errorCode : (NSString*) errorMessage;

/*!
 @method
 @abstract Invoked when a notification arrives.
 @discussion
 @param notifications 
 @return An int to indicate if the notification has been handled.
     SUP_NOTIFICATION_CONTINUE if the notification was not handled by the callback method.
     SUP_NOTIFICATION_CANCEL if the notification has already been handled by the callback method.
 */
- (int)onPushNotification :(NSDictionary*)notification;

@end
