/*
 
 Copyright (c) Sybase, Inc. 2010-2012   All rights reserved.                                    
 
 In addition to the license terms set out in the Sybase License Agreement for 
 the Sybase Unwired Platform ("Program"), the following additional or different 
 rights and accompanying obligations and restrictions shall apply to the source 
 code in this file ("Code").  Sybase grants you a limited, non-exclusive, 
 non-transferable, revocable license to use, reproduce, and modify the Code 
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of 
 applications created in connection with your licensed use of the Program.  
 The Code may not be transferred, sold, assigned, sublicensed or otherwise 
 conveyed (whether by operation of law or otherwise) to another party without 
 Sybase's prior written consent.  The following provisions shall apply to any 
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the 
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
 to indemnify, hold harmless, and defend Sybase from and against any claims or 
 lawsuits, including attorney's fees, that arise from or are related to the 
 modified Code or from use of the modified Code. 
 
 */

#import <Foundation/Foundation.h>
#import "SUPErrorCodes.h"

/*!
 @class SUPBaseException
 @abstract  This class contains information about exception error code and error messages. 
 @discussion
 */

@interface SUPBaseException : NSException
{
    NSArray*        _arguments;
    int             _errorCode;
    NSException*    _cause;  
}   


@property(readwrite, assign, nonatomic) int errorCode;
@property(readwrite, retain, nonatomic) NSArray* arguments;
@property(readwrite, retain, nonatomic) NSException* cause;
@property(readwrite, copy, nonatomic) NSString* message;


/*!
 @method 
 @abstract  Instantiates an SUPBaseException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @param  cause The cause of the exception
 @result The SUPBaseException object.
 @discussion
 */
- (id) initWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments cause:(NSException *)cause;
 
/*!
@method 
@abstract  Instantiates an SUPBaseException object
@param  errorCode The error code value
@result The SUPBaseException object.
@discussion
*/
+ (SUPBaseException *)baseExceptionWithErrorCode:(int)errorCode;

/*!
 @method 
 @abstract  Instantiates an SUPBaseException object
 @param  errorCode The error code value
 @param  message the error message
 @result The SUPBaseException object.
 @discussion
 */
+ (SUPBaseException *)baseExceptionWithErrorCode:(int)errorCode message:(NSString *)message;


/*!
 @method 
 @abstract  Instantiates an SUPBaseException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @result The SUPBaseException object.
 @discussion
 */
+ (SUPBaseException *)baseExceptionWithErrorCode:(int)errorCode message:(NSString *)message arguments:(NSArray *)arguments;

/*!
 @method 
 @abstract  Instantiates an SUPBaseException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @param  cause The cause of the exception
 @result The SUPBaseException object.
 @discussion
 */
+ (SUPBaseException *)baseExceptionWithErrorCode:(int)errorCode message:(NSString *)message arguments:(NSArray* )arguments cause:(NSException *)cause;


/*!
 @method 
 @abstract get the error message using the locale specified
 @param locale locale identifier
 @result the localized message
 @discussion The locale identifier is the language-specific project (.lproj) directory name for loading resource bunlde, ErrorMessages.strings.  It could be also the value passed to NSString's initWithFormat method for string formatting the arguments.
 <p>The locale value can be in one of the following two forms:
 <ul>
 <li>"language":  language specific value.  eg: @"en"</li>
 <li>"language"_"region":  language and region specific value.  eg: @"en_US"</li>
 </ul>
 <p>If resource bundle is not found in the "language"_"region" form, The "language" part of the value would be use to load the resource bundle.  If resource bundle is not found, go by [[NSBundle mainBundle] preferredLocalizations].  If it is still not found, default to "en".  If the value is not one of the locale identifiers available in [NSLocale availableLocaleIdentifiers],  the locale in [[NSLocale currentLocale] localeIdentifier] would be used in string formatting the arguments.
 */
- (NSString *)messageWithLocale:(NSString *)locale;

@end

