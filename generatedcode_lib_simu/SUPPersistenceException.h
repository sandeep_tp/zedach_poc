/*
 
 Copyright (c) Sybase, Inc. 2010-2012   All rights reserved.                                    
 
 In addition to the license terms set out in the Sybase License Agreement for 
 the Sybase Unwired Platform ("Program"), the following additional or different 
 rights and accompanying obligations and restrictions shall apply to the source 
 code in this file ("Code").  Sybase grants you a limited, non-exclusive, 
 non-transferable, revocable license to use, reproduce, and modify the Code 
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of 
 applications created in connection with your licensed use of the Program.  
 The Code may not be transferred, sold, assigned, sublicensed or otherwise 
 conveyed (whether by operation of law or otherwise) to another party without 
 Sybase's prior written consent.  The following provisions shall apply to any 
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or 
 implied, relating to modified Code or any problems that result from use of the 
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
 to indemnify, hold harmless, and defend Sybase from and against any claims or 
 lawsuits, including attorney's fees, that arise from or are related to the 
 modified Code or from use of the modified Code. 
 
 */



#import "sybase_core.h"
#import "SUPPersistenceException.h"

@class SUPStringList;
@class SUPPersistenceException;

/*!
 @class SUPPersistenceException
 @abstract   SUPPersistenceException is thrown when an exception occurs during database manipulation.
 @discussion 
 */

@interface SUPPersistenceException : SUPBaseException
{
}


/*!
 @method persistenceExceptionWithErrorCode
 @abstract  Instantiates an SUPPersistenceException object
 @param  errorCode The error code value
 @result The SUPPersistenceException object.
 @discussion
 */
+ (SUPPersistenceException*)persistenceExceptionWithErrorCode:(int)errorCode;

/*!
 @method persistenceExceptionWithErrorCode
 @abstract  Instantiates an SUPPersistenceException object
 @param  errorCode The error code value
 @param  message the error message
 @result The SUPPersistenceException object.
 @discussion
 */
+ (SUPPersistenceException *)persistenceExceptionWithErrorCode:(int)errorCode message:(NSString *)message;


/*!
 @method persistenceExceptionWithErrorCode
 @abstract  Instantiates an SUPPersistenceException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @result The SUPPersistenceException object.
 @discussion
 */
+ (SUPPersistenceException*)persistenceExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments;

/*!
 @method persistenceExceptionWithErrorCode
 @abstract  Instantiates an SUPPersistenceException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @param  cause The cause of the exception
 @result The SUPPersistenceException object.
 @discussion
 */
+ (SUPPersistenceException*)persistenceExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments cause:(NSException *)cause;

/*!
 @method     
 @abstract   (Deprecated) Returns a new instance of SUPPersistenceException object.
 @result The SUPPersistenceException object.
 @discussion 
 */
+ (SUPPersistenceException*)getInstance DEPRECATED_ATTRIBUTE;

/*!
 @method     
 @abstract   (Deprecated) Returns a new instance of SUPPersistenceException object.
 @result The SUPPersistenceException object.
 @discussion This method is deprecated. use getInstance.
 */
+ (SUPPersistenceException*)newInstance DEPRECATED_ATTRIBUTE NS_RETURNS_NON_RETAINED;

- (SUPPersistenceException*)init;

/*!
 @method     
 @abstract   (Deprecated) Returns the SUPPersistenceException object with "theCause".
 @param theCause The cause.
 @result The SUPPersistenceException.
 @discussion 
 */
+ (SUPPersistenceException*)withCause:(NSException*)theCause DEPRECATED_ATTRIBUTE;

/*!
 @method     
 @abstract   (Deprecated) Returns the SUPPersistenceException object with "theMessage".
 @param theMessage The  message.
 @result The SUPPersistenceException.
 @discussion 
 */
+ (SUPPersistenceException*)withMessage:(SUPString)theMessage DEPRECATED_ATTRIBUTE;

/*!
 @method     
 @abstract   (Deprecated) Returns the SUPPersistenceException object with "theMessage" and "theCause".
 @param theMessage The  message.
 @param theCause The cause.
 @result The SUPPersistenceException.
 @discussion 
 */
+ (SUPPersistenceException*)withCauseAndMessage:(NSException*)theCause :(SUPString)theMessage DEPRECATED_ATTRIBUTE;

/*!
 @method     
 @abstract (Deprecated) This method is used to create an exception if code tries to write a null into a database column marked "not null"
   or reads a JSON null for a non-nullable attribute.
 @param theMessage The  message.
 @result The SUPPersistenceException.
 @discussion 
 */
+ (SUPPersistenceException*)forUnexpectedNull:(SUPString)name DEPRECATED_ATTRIBUTE;


@end
