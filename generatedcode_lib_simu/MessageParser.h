//
//  MessageParser.h
//  Widgets
//
//  Created by Alexey Piterkin on 2/27/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WidgetXmlParser.h"
#import "WidgetMessage.h"

@class WidgetDefinition;

@interface MessageParser : WidgetXmlParser {
   WidgetDefinition * widget;
   WidgetMessageMode mode;
}

@property (nonatomic, readonly) WidgetMessage * message;

- (id)initWithString:(NSString*)xmlText forWidget:(WidgetDefinition*)aWidget inMode:(WidgetMessageMode)aMode;

@end
