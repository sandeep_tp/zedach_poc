//
//  ScreenDefinition.h
//  iota
//
//  Created by Alexey Piterkin on 2/6/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WidgetParser.h"
#import "NSString+Widgets.h"

@class WidgetDefinition, WidgetScreenControllerPrivate;

@interface ScreenDefinition : NSObject <Parseable> {
   WidgetDefinition * widget;
   
   NSString * key;
   NSString * text;
   
   // XML parsing state
   NSString * skipTag;   
}

// General properties
@property (nonatomic, readonly) WidgetDefinition * widget;
@property (nonatomic, readonly) NSString * key;
@property (nonatomic, readonly) NSString * text;

- (id)initWithAttributes:(NSDictionary*)attributes widget:(WidgetDefinition*)aWidget parser:(WidgetXmlParser*)parser;

// XML-Parsing API
- (void)processAfterLoadingWithLanguage:(NSMutableDictionary*)language;

@end
