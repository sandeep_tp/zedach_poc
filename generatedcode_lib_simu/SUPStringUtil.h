/*
 
 Copyright (c) Sybase, Inc. 2009-2012   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */


#import "sybase_core.h"
@class SUPDataType;
@class SUPArrayList;

@interface SUPStringUtil : NSObject

// Comparison methods

+ (SUPBoolean)equal:(SUPString)a :(SUPString)b;
+ (SUPBoolean)notEqual:(SUPString)a :(SUPString)b;
+ (SUPBoolean)lessThan:(SUPString)a :(SUPString)b;
+ (SUPBoolean)lessEqual:(SUPString)a :(SUPString)b;
+ (SUPBoolean)greaterThan:(SUPString)a :(SUPString)b;
+ (SUPBoolean)greaterEqual:(SUPString)a :(SUPString)b;

// Conversion methods

+ (SUPChar)intToChar:(SUPInt)i;
+ (SUPInt)charToInt:(SUPChar)c;

+ (SUPChar)getChar:(SUPNullableChar)c;
+ (SUPNullableChar)getNullableChar:(SUPChar)c;

+ (SUPChar)getChar_s:(SUPString)s;
+ (SUPNullableChar)getNullableChar_s:(SUPString)s;

// String representation of primitive types

+ (SUPString)toString_object:(id)x;
+ (SUPString)toString_object:(id)x withType:(SUPDataType*)type;

+ (SUPString)toString_boolean:(SUPBoolean)b;
+ (SUPString)toString_string:(SUPString)s;
+ (SUPString)toString_binary:(SUPBinary)b;
+ (SUPString)toString_char:(SUPChar)c;
+ (SUPString)toString_byte:(SUPByte)n;
+ (SUPString)toString_short:(SUPShort)n;
+ (SUPString)toString_int:(SUPInt)n;
+ (SUPString)toString_long:(SUPLong)n;
+ (SUPString)toString_integer:(SUPInteger)n;
+ (SUPString)toString_decimal:(SUPDecimal)n;
+ (SUPString)toString_float:(SUPFloat)n;
+ (SUPString)toString_double:(SUPDouble)n;
+ (SUPString)toString_date:(SUPDate)x;
+ (SUPString)toString_time:(SUPTime)x;
+ (SUPString)toString_dateTime:(SUPDateTime)x;

// String representation of nullable primitive types

+ (SUPString)toString_nullableBoolean:(SUPNullableBoolean)b;
+ (SUPString)toString_nullableString:(SUPString)s;
+ (SUPString)toString_nullableBinary:(SUPBinary)b;
+ (SUPString)toString_nullableChar:(SUPNullableChar)c;
+ (SUPString)toString_nullableByte:(SUPNullableByte)n;
+ (SUPString)toString_nullableShort:(SUPNullableShort)n;
+ (SUPString)toString_nullableInt:(SUPNullableInt)n;
+ (SUPString)toString_nullableLong:(SUPNullableLong)n;
+ (SUPString)toString_nullableInteger:(SUPNullableInteger)n;
+ (SUPString)toString_nullableDecimal:(SUPNullableDecimal)n;
+ (SUPString)toString_nullableFloat:(SUPNullableFloat)n;
+ (SUPString)toString_nullableDouble:(SUPNullableDouble)n;
+ (SUPString)toString_nullableDate:(SUPNullableDate)x;
+ (SUPString)toString_nullableTime:(SUPNullableTime)x;
+ (SUPString)toString_nullableDateTime:(SUPNullableDateTime)x;

// Utility methods

+ (SUPChar)charAt:(SUPString)string :(SUPInt)index;
+ (SUPString)concat:(SUPArrayList*)strings;
+ (SUPBoolean)endsWith:(SUPString)s :(SUPString)x;
+ (SUPString)fromChars:(SUPArrayList*)chars;
+ (SUPInt)hashCode:(SUPString)s;
+ (SUPInt)indexOf:(SUPString)s :(SUPString)x;
+ (SUPInt)indexOf2:(SUPString)s :(SUPString)x :(SUPInt)start;
+ (SUPString)join:(SUPArrayList*)strings :(SUPString)separator;
+ (SUPInt)lastIndexOf:(SUPString)s :(SUPString)x;
+ (SUPInt)lastIndexOf2:(SUPString)s :(SUPString)x :(SUPInt)start;
+ (SUPBoolean)like:(SUPString)string :(SUPString)pattern;
+ (SUPBoolean)startsWith:(SUPString)s :(SUPString)x;
+ (SUPBoolean)startsWith2:(SUPString)s :(SUPString)x :(SUPInt)start;
+ (SUPString)substring:(SUPString)s :(SUPInt)start;
+ (SUPString)substring2:(SUPString)s :(SUPInt)start :(SUPInt)end;
+ (SUPString)toLowerCase:(SUPString)s;
+ (SUPString)toUpperCase:(SUPString)s;
+ (SUPString)trim:(SUPString)s;

+ (SUPString)fromUTF8:(SUPBinary)b;
+ (SUPBinary)toUTF8:(SUPString)s;

// GUID methods

+ (SUPString)guid32;
+ (SUPString)guid36;

// Methods for generating escaped strings used by JSON code

+ (SUPString)toJsonEscapedString:(SUPString)s;
+ (SUPString)toJsonEscapedString:(SUPString)s :(BOOL)withQuotes;


@end
