
/* Copyright (c) Sybase, Inc. 2010-2013   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code;OT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES;
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */

/*
 Generated by Sybase Unwired Platform 
 Compiler version - 3.0.13.40
*/ 

#import "SalesPlusChangeLogKey.h"
#import "SalesPlusChangeLogKey+Internal.h"
#import "SalesPlusChangeLogKeyMetaData.h"
#import "SUPJsonObject.h"
#import "SUPJsonReader.h"
#import "SalesPlusERPSalesPlus_SuperUserDB.h"
#import "SalesPlusERPSalesPlus_SuperUserDB+Internal.h"
#import "SUPClassDelegate.h"
#import "SUPEntityMetaDataRBS.h"

@implementation SalesPlusChangeLogKey


@synthesize entityType = _entityType;
@synthesize surrogateKey = _surrogateKey;


#pragma mark -
#pragma mark Init, dealloc, getInstance
- (id) init
{
    if ((self = [super init]))
    {
        self.classMetaData = (SUPEntityMetaDataRBS *)[SalesPlusChangeLogKey metaData];
        [self setClassDelegate:[[self class] delegate]];
        
    }
    return self;    
}
- (void)dealloc
{
	[super dealloc];
}
+ (SalesPlusChangeLogKey*)getInstance
{
     SalesPlusChangeLogKey* me = [[SalesPlusChangeLogKey alloc] init];
    [me autorelease];
    return me;
}



#pragma mark -
#pragma mark Property getters and setters

#pragma mark -
#pragma mark Metadata methods

static SUPClassMetaDataRBS* SalesPlusChangeLogKey_META_DATA;

+ (SUPClassMetaDataRBS*)metaData
{
    if (SalesPlusChangeLogKey_META_DATA == nil) {
 	   	SalesPlusChangeLogKey_META_DATA = [[SalesPlusChangeLogKeyMetaData alloc] init];
	}
	
	return SalesPlusChangeLogKey_META_DATA;
}

- (SUPClassMetaDataRBS*)getClassMetaData
{
    return [[self class] metaData];
}
#pragma mark -
#pragma mark Attribute get/set methods (internal)
-(SUPLong) getAttributeLong:(int)id_
{
    switch(id_)
    {
    case 4056:
        return self.surrogateKey;
    default:
        return [super getAttributeLong:id_];
    }
}

-(void) setAttributeLong:(int)id_ :(SUPLong)v
{
    switch(id_)
    {
    case 4056:
        self.surrogateKey = v;
        break;;
    default:
        [super setAttributeLong:id_:v];
        break;;
    }
}
-(SUPInt) getAttributeInt:(int)id_
{
    switch(id_)
    {
    case 4055:
        return self.entityType;
    default:
        return [super getAttributeInt:id_];
    }
}

-(void) setAttributeInt:(int)id_ :(SUPInt)v
{
    switch(id_)
    {
    case 4055:
        self.entityType = v;
        break;;
    default:
        [super setAttributeInt:id_:v];
        break;;
    }
}
- (id)getAttributeLargeObject:(int)id_ loadFromDB:(BOOL)loadFromDB
{
    switch(id_)
    {
        default:
        return [super getAttributeJson:id_];
    }
}
- (void)setAttributeLargeObject:(int)id_ :(id)value
{
    switch(id_)
    {
        default:
            [super setAttributeJson:id_:value];
            break;
    }

}

#pragma mark -
#pragma mark Delegate method (internal)

static SUPClassDelegate *g_SalesPlusChangeLogKey_delegate = nil;

+ (SUPClassDelegate *) delegate
{
	@synchronized(self) {
		if (g_SalesPlusChangeLogKey_delegate == nil) {
			g_SalesPlusChangeLogKey_delegate = [[SUPClassDelegate alloc] initWithName:@"SalesPlusChangeLogKey" clazz:[self class]
				metaData:[self metaData] dbDelegate:[SalesPlusERPSalesPlus_SuperUserDB delegate] database:[SalesPlusERPSalesPlus_SuperUserDB instance]];
		}
	}
	
	return [[g_SalesPlusChangeLogKey_delegate retain] autorelease];
}

#pragma mark -
#pragma mark toString method

- (NSString*)toString
{
	NSString* str = [NSString stringWithFormat:@"\
	ChangeLogKey = \n\
	    entityType = %i,\n\
	    surrogateKey = %qi,\n\
	    isNew = %i,\n\
        isDirty = %i,\n\
        isDeleted = %i,\n\
	\n"
    	,self.entityType
    	,self.surrogateKey
		,self.isNew
		,self.isDirty
		,self.isDeleted
	];
	return str;

}

- (NSString*)description
{
	return [self toString];
}

#pragma mark -
#pragma mark JSON methods (internal)



- (SUPJsonObject*)getAttributeJson:(int)id_
{
    switch(id_)
    {
        default:
        return [super getAttributeJson:id_];
    }

}
- (void)setAttributeJson:(int)id_ :(SUPJsonObject*)value
{
    switch(id_)
    { 
        default:
            [super setAttributeJson:id_:value];
            break;
    }

}

+ (SUPObjectList*)fromJSONList:(SUPJsonArray*)jsonArray
{
    if(jsonArray == nil)
        return nil;
    
    SUPObjectList* instanceList = [[SUPObjectList alloc] initWithCapacity:1];
    [instanceList autorelease];
    if(instanceList == nil)
        return nil;

    for (SUPJsonObject* jsonObject in jsonArray)
    {
        SalesPlusChangeLogKey* inst = [[SalesPlusChangeLogKey alloc] init];
        [inst readJson:jsonObject];
        [instanceList add:inst];
        [inst release];
    }
    return instanceList;
}

+ (SUPJsonArray*)toJSONList:(SUPObjectList*)instanceList
{
    SUPJsonArray* jsonArray = [[SUPJsonArray alloc] init];
    [jsonArray autorelease];
    for (SalesPlusChangeLogKey* inst in instanceList)
    {
        SUPJsonObject *o = [[SUPJsonObject alloc] init];
        [inst writeJson:o];
        [jsonArray add:o];
        [o release];
    }
    return jsonArray;
}

@end