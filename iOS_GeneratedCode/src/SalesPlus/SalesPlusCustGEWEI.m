
/* Copyright (c) Sybase, Inc. 2010-2013   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code;OT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES;
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */

/*
 Generated by Sybase Unwired Platform 
 Compiler version - 3.0.13.40
*/ 

#import "SalesPlusCustGEWEI.h"
#import "SalesPlusCustGEWEI+Internal.h"
#import "SalesPlusCustGEWEIMetaData.h"
#import "SUPJsonObject.h"
#import "SUPJsonReader.h"
#import "SalesPlusERPSalesPlus_SuperUserDB.h"
#import "SalesPlusERPSalesPlus_SuperUserDB+Internal.h"
#import "SUPLocalEntityDelegate.h"
#import "SUPEntityMetaDataRBS.h"
#import "SUPQuery.h"
#import "SalesPlusKeyGenerator.h"
#import "SalesPlusLocalKeyGenerator.h"
#import "SalesPlusKeyGenerator+Internal.h"
#import "SalesPlusLocalKeyGenerator+Internal.h"
#import "SalesPlusLogRecordImpl.h"
#import "SUPSyncParamEntityDelegate.h"
#import "SalesPlusCustGEWEI_pull_pq.h"
#import "SalesPlusCustGEWEI_pull_pq+Internal.h"
#import "SalesPlusCustGEWEISynchronizationParameters+Internal.h"
#import "SalesPlusCustGEWEISubscription+Internal.h"

@implementation SalesPlusCustGEWEI


@synthesize MSEHI = _MSEHI;
@synthesize MSEHT = _MSEHT;
@synthesize SPRAS = _SPRAS;
@synthesize surrogateKey = _surrogateKey;


#pragma mark -
#pragma mark Init, dealloc, getInstance
- (id) init
{
    if ((self = [super init]))
    {
        self.classMetaData = [SalesPlusCustGEWEI metaData];
        [self setEntityDelegate:(SUPEntityDelegate*)[SalesPlusCustGEWEI delegate]];
    }
    return self;    
}
- (void)dealloc
{
    self.isDirty = YES; // So that not to trigger side effects in setDirty
    self.MSEHI = nil;
    self.MSEHT = nil;
    self.SPRAS = nil;
	[super dealloc];
}
+ (SalesPlusCustGEWEI*)getInstance
{
     SalesPlusCustGEWEI* me = [[SalesPlusCustGEWEI alloc] init];
    [me autorelease];
    return me;
}



#pragma mark -
#pragma mark Property getters and setters

- (int64_t)surrogateKey
{
    return _surrogateKey;
}

- (void)setMSEHI:(NSString*)newMSEHI
{
    if (newMSEHI != self->_MSEHI)
    {
        [self setDirty];
        [self->_MSEHI release];
        self->_MSEHI = [newMSEHI retain];
    }
}

- (void)setMSEHT:(NSString*)newMSEHT
{
    if (newMSEHT != self->_MSEHT)
    {
        [self setDirty];
        [self->_MSEHT release];
        self->_MSEHT = [newMSEHT retain];
    }
}

- (void)setSPRAS:(NSString*)newSPRAS
{
    if (newSPRAS != self->_SPRAS)
    {
        [self setDirty];
        [self->_SPRAS release];
        self->_SPRAS = [newSPRAS retain];
    }
}

- (void)setSurrogateKey:(int64_t)newSurrogateKey
{
    if (newSurrogateKey != self->_surrogateKey)
    {
        self.isNew = YES;
        self->_surrogateKey = newSurrogateKey;
    }
}

#pragma mark -
#pragma mark Metadata methods

static SUPEntityMetaDataRBS* SalesPlusCustGEWEI_META_DATA;

+ (SUPEntityMetaDataRBS*)metaData
{
    if (SalesPlusCustGEWEI_META_DATA == nil) {
		SalesPlusCustGEWEI_META_DATA = [[SalesPlusCustGEWEIMetaData alloc] init];
	}
	
	return SalesPlusCustGEWEI_META_DATA;
}

- (SUPClassMetaDataRBS*)getClassMetaData
{
    return [[self class] metaData];
}

#pragma mark -
#pragma mark Clear relationship objects

- (void)clearRelationshipObjects
{
}

#pragma mark -
#pragma mark find() and related methods
+ (id)allocInitialTraceRequest:(NSString*)method withSkip:(int32_t)skip withTake:(int32_t)take withSql:(NSString*)sql withValues:(SUPObjectList*)values
{
    id request = nil;
    
    CFSCTraceLevel level = [SUPCFSCTrace getTraceLevel];
    NSMutableString *params = [[[NSMutableString alloc] init] autorelease];

    for (id val in values)
    {
        [params appendFormat:@"%@;", [SUPStringUtil toString_object:val]];
    }
    if (skip >=0)
    	[params appendFormat:@"skip=%d;", skip];
    if (take >= 0)
        [params appendFormat:@"take=%d", take];
    if (level == CFSCTraceLevel_DETAIL)
    {
        
        request = [[SUPCFSCTraceDetailRequest alloc] initWithParams:@"SalesPlusCustGEWEI" :method :nil:nil:[NSDate date] :nil :0
                                                                   :nil :NO :NO :NO :NO :NO :sql :params];
    } else if (level == CFSCTraceLevel_NORMAL)
    {
        request = [[SUPCFSCTraceRequest alloc] initWithParams:@"SalesPlusCustGEWEI"  :method :nil :nil  :[NSDate date] :nil :0 :params];
    }
    return request;
}
+ (void)finishTraceRequest:(id)request :(int)rows
{
    if (!request)
    {
        return;
    }
    
    SUPCFSCTraceRequest *traceReq = (SUPCFSCTraceRequest *)request;
    
    traceReq.endTime = [NSDate date];
  
    traceReq.count = rows;
    
    [SUPCFSCTrace log:(id)request];
}

+ (SalesPlusCustGEWEI*)find:(int64_t)id_
{
    SUPCFSCTraceRequest* request = nil;
    NSString *msg = @"success";
    if ([SUPCFSCTrace isEnabled])
    {
        request = [[SUPCFSCTraceRequest alloc] initWithParams:@"SalesPlusCustGEWEI" :@"find" 
            :nil :[self metaData].synchronizationGroup  :[NSDate date] :nil :0 :[SUPStringUtil toString_object:[NSNumber numberWithLongLong:id_]]];
    } 
    SalesPlusCustGEWEI* result = nil;
    @try {
         SUPObjectList *keys = [SUPObjectList getInstance];
         [keys add:[NSNumber numberWithLongLong:id_]];
         result = (SalesPlusCustGEWEI*)[(SUPEntityDelegate*)([[self class] delegate]) findEntityWithKeys:keys];
         return result;
     }
     @catch (NSException *e) {
        msg = [NSString stringWithFormat:@"fail in SalesPlusCustGEWEI--find: %@:%@", [e name], [e reason]];        
        CFSCTraceLogError(msg);
        @throw e;
    }
    @finally
    {
        if (request)
        {
            request.endTime = [NSDate date];
            if ([msg isEqualToString:@"success"])
                request.count = 1;

            [SUPCFSCTrace log:(id)request];
            [request release];
        }
    }
}

+ (SUPObjectList*)findWithQuery:(SUPQuery*)query
{
    return (SUPObjectList*)[(SUPEntityDelegate*)([[self class] delegate])  findWithQuery:query:[SalesPlusCustGEWEI class]];
}

- (int64_t)_pk
{
    return (int64_t)[[self i_pk] longLongValue];
}

+ (SalesPlusCustGEWEI*)load:(int64_t)id_
{
    return (SalesPlusCustGEWEI*)[(SUPEntityDelegate*)([[self class] delegate]) load:[NSNumber numberWithLongLong:id_]];
}
#pragma mark -
#pragma mark Log record methods

- (SUPObjectList*)getLogRecords
{
   return [SalesPlusLogRecordImpl findByEntity:@"CustGEWEI":[self keyToString]];
}




#pragma mark -
#pragma mark Description implemetation

- (NSString*)toString
{
	NSString* str = [NSString stringWithFormat:@"\
	CustGEWEI = \n\
	    MSEHI = %@,\n\
	    MSEHT = %@,\n\
	    SPRAS = %@,\n\
	    surrogateKey = %qi,\n\
	    isNew = %i,\n\
        isDirty = %i,\n\
        isDeleted = %i,\n\
	\n"
    	,self.MSEHI
    	,self.MSEHT
    	,self.SPRAS
    	,self.surrogateKey
		,self.isNew
		,self.isDirty
		,self.isDeleted
	];
	return str;

}

- (NSString*)description
{
	return [self toString];
}




+ (SUPObjectList *)getSubscriptions
{
    SUPObjectList *result = [SUPObjectList getInstance];
    
    for (SalesPlusCustGEWEI_pull_pq *pq in [SalesPlusCustGEWEI_pull_pq findAll])    
    {
        SalesPlusCustGEWEISubscription *subscription = [SalesPlusCustGEWEISubscription getInstance];
        subscription.Language = pq.LanguageParam;
        [result add:subscription];
    }
    return result;
}

+ (void)addSubscription:(SalesPlusCustGEWEISubscription *)subscription
{
    NSString *username = [SalesPlusERPSalesPlus_SuperUserDB getSyncUsername];
    SalesPlusCustGEWEI_pull_pq *pqEntity = [SalesPlusCustGEWEI_pull_pq findSub:username withLanguage:subscription.Language];  

    if (!pqEntity)
    {
        pqEntity = [SalesPlusCustGEWEI_pull_pq getInstance];
        pqEntity.username = username;
        pqEntity.LanguageParam = subscription.Language;  
        [pqEntity save];
    }
}

+ (void)removeSubscription:(SalesPlusCustGEWEISubscription *)subscription
{
    NSString *username = [SalesPlusERPSalesPlus_SuperUserDB getSyncUsername];   
    SalesPlusCustGEWEI_pull_pq *pqEntity = [SalesPlusCustGEWEI_pull_pq findSub:username withLanguage:subscription.Language];
    
    if (pqEntity)
    {   
        [pqEntity delete];
    }
}

+ (SalesPlusCustGEWEISynchronizationParameters*)getSynchronizationParameters
{
    return (SalesPlusCustGEWEISynchronizationParameters*)[(SUPSyncParamEntityDelegate*)([[SalesPlusERPSalesPlus_SuperUserDB delegate] getDelegate:@"CustGEWEISynchronizationParameters"]) getSynchronizationParameters];
}

#pragma mark - 
#pragma mark Delegate method (internal)

static SUPLocalEntityDelegate *g_SalesPlusCustGEWEI_delegate = nil;

+ (SUPLocalEntityDelegate *) delegate
{
	@synchronized(self) {
		if (g_SalesPlusCustGEWEI_delegate == nil) {
			g_SalesPlusCustGEWEI_delegate = [[SUPLocalEntityDelegate alloc] initWithName:@"SalesPlusCustGEWEI" clazz:[self class]
				metaData:[self metaData] dbDelegate:[SalesPlusERPSalesPlus_SuperUserDB delegate] database:[SalesPlusERPSalesPlus_SuperUserDB instance]];
		}
	}
	
	return [[g_SalesPlusCustGEWEI_delegate retain] autorelease];
}

#pragma mark -
#pragma mark JSON methods (internal)


- (SUPJsonObject*)getAttributeJson:(int)id_
{
    switch(id_)
    {
        default:
        return [super getAttributeJson:id_];
    }

}
- (void)setAttributeJson:(int)id_ :(SUPJsonObject*)value
{
    switch(id_)
    { 
        default:
            [super setAttributeJson:id_:value];
            break;
    }

}

+ (SUPObjectList*)fromJSONList:(SUPJsonArray*)jsonArray
{
    if(jsonArray == nil)
        return nil;
    
    SUPObjectList* instanceList = [[SUPObjectList alloc] initWithCapacity:1];
    [instanceList autorelease];
    if(instanceList == nil)
        return nil;

    for (SUPJsonObject* jsonObject in jsonArray)
    {
        SalesPlusCustGEWEI* inst = [[SalesPlusCustGEWEI alloc] init];
        [inst readJson:jsonObject];
        [instanceList add:inst];
        [inst release];
    }
    return instanceList;
}

+ (SUPJsonArray*)toJSONList:(SUPObjectList*)instanceList
{
    SUPJsonArray* jsonArray = [[SUPJsonArray alloc] init];
    [jsonArray autorelease];
    for (SalesPlusCustGEWEI* inst in instanceList)
    {
        SUPJsonObject *o = [[SUPJsonObject alloc] init];
        [inst writeJson:o];
        [jsonArray add:o];
        [o release];
    }
    return jsonArray;
}

#pragma mark -
#pragma mark Internal attribute get/set methods
-(SUPLong) getAttributeLong:(int)id_
{
    switch(id_)
    {
    case 2130:
        return self.surrogateKey;
    default:
         return [super getAttributeLong:id_];
    }
}

-(void) setAttributeLong:(int)id_ :(SUPLong)v
{
    switch(id_)
    {
    case 2130:
        self.surrogateKey = v;
        break;;
    default:
        [super setAttributeLong:id_:v];
        break;;
    }
}
-(SUPString) getAttributeString:(int)id_
{
    switch(id_)
    {
    case 2127:
        return self.MSEHI;
    case 2128:
        return self.MSEHT;
    case 2129:
        return self.SPRAS;
    default:
         return [super getAttributeString:id_];
    }
}

-(void) setAttributeString:(int)id_ :(SUPString)v
{
    switch(id_)
    {
    case 2127:
        self.MSEHI = v;
        break;;
    case 2128:
        self.MSEHT = v;
        break;;
    case 2129:
        self.SPRAS = v;
        break;;
    default:
        [super setAttributeString:id_:v];
        break;;
    }
}
- (id)getAttributeLargeObject:(int)id_ loadFromDB:(BOOL)loadFromDB
{
    switch(id_)
    {
        default:
        return [super getAttributeJson:id_];
    }
}
- (void)setAttributeLargeObject:(int)id_ :(id)value
{
    switch(id_)
    {
        default:
            [super setAttributeJson:id_:value];
            break;
    }

}



#pragma mark -
#pragma mark Object queries and operation implementations



+ (SUPObjectList*)findAll
{
	return [self findAll:0 take:INT_MAX]; 
}


	

+ (SUPObjectList*)findAll:(int32_t)skip take:(int32_t)take
{
	NSMutableString *sql = nil;
	NSMutableString *_selectSQL = nil;
	_selectSQL = [[[NSMutableString alloc] initWithCapacity:82] autorelease];
	[_selectSQL appendString:@" x.\"a\",x.\"b\",x.\"c\",x.\"d\" FROM \"erpsalesplus_superuser_1_0_custgewei\" x"];
	SUPObjectList* result = nil;
	sql = [[NSMutableString alloc] initWithFormat:@"select %@", _selectSQL];
	[sql autorelease];
	sql = [[NSMutableString alloc] initWithFormat:@"select %@", _selectSQL];
	[sql autorelease];
	SUPStringList *ids = [SUPStringList listWithCapacity:0];
	SUPObjectList *dts = [SUPObjectList getInstance];
	SUPObjectList* values = [SUPObjectList getInstance];
	id request = [[self class] allocInitialTraceRequest:@"findAll" withSkip:skip withTake:take withSql:sql withValues:values ];
	result = (SUPObjectList*)[[[self class] delegate] findWithSQL:sql withDataTypes:dts withValues:values withIDs:ids withSkip:skip withTake:take withClass:[SalesPlusCustGEWEI class]];
	[[self class] finishTraceRequest:request :[result size]];
	[request release];
	return result;
}



+ (SUPObjectList*)findByMSEHI:(NSString*)mSEHI
{
	return [self findByMSEHI:mSEHI skip:0 take:INT_MAX]; 
}


	

+ (SUPObjectList*)findByMSEHI:(NSString*)mSEHI skip:(int32_t)skip take:(int32_t)take
{
	NSMutableString *sql = nil;
	NSMutableString *_selectSQL = nil;
	_selectSQL = [[[NSMutableString alloc] initWithCapacity:100] autorelease];
	[_selectSQL appendString:@" x.\"a\",x.\"b\",x.\"c\",x.\"d\" FROM \"erpsalesplus_superuser_1_0_custgewei\" x WHERE x.\"a\" = ?"];
	SUPObjectList* result = nil;
	sql = [[NSMutableString alloc] initWithFormat:@"select %@", _selectSQL];
	[sql autorelease];
	SUPStringList *ids = [SUPStringList listWithCapacity:0];
	SUPObjectList *dts = [SUPObjectList getInstance];
	[dts addObject:[SUPDataType forName:@"string"]];
	SUPObjectList* values = [SUPObjectList getInstance];
	[values addObject:mSEHI];
	id request = [[self class] allocInitialTraceRequest:@"findByMSEHI" withSkip:skip withTake:take withSql:sql withValues:values ];
	result = (SUPObjectList*)[[[self class] delegate] findWithSQL:sql withDataTypes:dts withValues:values withIDs:ids withSkip:skip withTake:take withClass:[SalesPlusCustGEWEI class]];
	[[self class] finishTraceRequest:request :[result size]];
	[request release];
	return result;
}



+ (SUPObjectList*)findBySPRAS:(NSString*)sPRAS
{
	return [self findBySPRAS:sPRAS skip:0 take:INT_MAX]; 
}


	

+ (SUPObjectList*)findBySPRAS:(NSString*)sPRAS skip:(int32_t)skip take:(int32_t)take
{
	NSMutableString *sql = nil;
	NSMutableString *_selectSQL = nil;
	_selectSQL = [[[NSMutableString alloc] initWithCapacity:100] autorelease];
	[_selectSQL appendString:@" x.\"a\",x.\"b\",x.\"c\",x.\"d\" FROM \"erpsalesplus_superuser_1_0_custgewei\" x WHERE x.\"c\" = ?"];
	SUPObjectList* result = nil;
	sql = [[NSMutableString alloc] initWithFormat:@"select %@", _selectSQL];
	[sql autorelease];
	SUPStringList *ids = [SUPStringList listWithCapacity:0];
	SUPObjectList *dts = [SUPObjectList getInstance];
	[dts addObject:[SUPDataType forName:@"string"]];
	SUPObjectList* values = [SUPObjectList getInstance];
	[values addObject:sPRAS];
	id request = [[self class] allocInitialTraceRequest:@"findBySPRAS" withSkip:skip withTake:take withSql:sql withValues:values ];
	result = (SUPObjectList*)[[[self class] delegate] findWithSQL:sql withDataTypes:dts withValues:values withIDs:ids withSkip:skip withTake:take withClass:[SalesPlusCustGEWEI class]];
	[[self class] finishTraceRequest:request :[result size]];
	[request release];
	return result;
}



+ (SalesPlusCustGEWEI*)findByPrimaryKey:(NSString*)mSEHI withSPRAS:(NSString*)sPRAS
{
	NSMutableString *sql = nil;
	NSMutableString *_selectSQL = nil;
	_selectSQL = [[[NSMutableString alloc] initWithCapacity:122] autorelease];
	[_selectSQL appendString:@"SELECT x.\"a\",x.\"b\",x.\"c\",x.\"d\" FROM \"erpsalesplus_superuser_1_0_custgewei\" x WHERE x.\"a\" = ? AND x.\"c\" = ?"];
	sql = [[NSMutableString alloc] initWithFormat:@"%@", _selectSQL];
	[sql autorelease];
	SUPStringList *ids = [SUPStringList listWithCapacity:0];
	SUPObjectList *dts = [SUPObjectList getInstance];
	[dts addObject:[SUPDataType forName:@"string"]];
	[dts addObject:[SUPDataType forName:@"string"]];
	SUPObjectList* values = [SUPObjectList getInstance];
	[values addObject:mSEHI];
	[values addObject:sPRAS];
	
	id request = [[self class] allocInitialTraceRequest:@"findByPrimaryKey" withSkip:-1 withTake:-1 withSql:sql withValues:values ];	
	SUPObjectList *result = (SUPObjectList*)[[[self class] delegate] findWithSQL:sql withDataTypes:dts withValues:values withIDs:ids withClass:[SalesPlusCustGEWEI class]];
	[[self class] finishTraceRequest:request :[result size]];
	[request release];
	if(result && ([result size] > 0))
	{   
		SalesPlusCustGEWEI* cus = (SalesPlusCustGEWEI*)[result item:0];
	    return cus;
	}
	else
	    return nil;
}



/*!
  @method
  @abstract Generated class method 
  @param query
  @throws SUPPersistenceException
 */
+ (int32_t)getSize:(SUPQuery*)query
{
    return [(SUPEntityDelegate*)([[self class] delegate]) getSize:query];
}

@end