
/* Copyright (c) Sybase, Inc. 2010-2013   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code;OT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES;
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */

/*
 Generated by Sybase Unwired Platform 
 Compiler version - 3.0.13.40
*/ 

#import "SalesPlusUser.h"
#import "SalesPlusUser+Internal.h"
#import "SalesPlusUserMetaData.h"
#import "SUPJsonObject.h"
#import "SUPJsonReader.h"
#import "SalesPlusERPSalesPlus_SuperUserDB.h"
#import "SalesPlusERPSalesPlus_SuperUserDB+Internal.h"
#import "SUPLocalEntityDelegate.h"
#import "SUPEntityMetaDataRBS.h"
#import "SUPQuery.h"
#import "SalesPlusKeyGenerator.h"
#import "SalesPlusLocalKeyGenerator.h"
#import "SalesPlusKeyGenerator+Internal.h"
#import "SalesPlusLocalKeyGenerator+Internal.h"
#import "SalesPlusLogRecordImpl.h"

@implementation SalesPlusUser


@synthesize PASSWORD = _PASSWORD;
@synthesize BNAME = _BNAME;
@synthesize VKORG = _VKORG;
@synthesize VTWEG = _VTWEG;
@synthesize SPART = _SPART;
@synthesize VKBUR = _VKBUR;
@synthesize VKGRP = _VKGRP;
@synthesize WERK = _WERK;
@synthesize LGORT = _LGORT;
@synthesize KUNNR = _KUNNR;
@synthesize LANGU = _LANGU;
@synthesize surrogateKey = _surrogateKey;


#pragma mark -
#pragma mark Init, dealloc, getInstance
- (id) init
{
    if ((self = [super init]))
    {
        self.classMetaData = [SalesPlusUser metaData];
        [self setEntityDelegate:(SUPEntityDelegate*)[SalesPlusUser delegate]];
    }
    return self;    
}
- (void)dealloc
{
    self.isDirty = YES; // So that not to trigger side effects in setDirty
    self.PASSWORD = nil;
    self.BNAME = nil;
    self.VKORG = nil;
    self.VTWEG = nil;
    self.SPART = nil;
    self.VKBUR = nil;
    self.VKGRP = nil;
    self.WERK = nil;
    self.LGORT = nil;
    self.KUNNR = nil;
    self.LANGU = nil;
	[super dealloc];
}
+ (SalesPlusUser*)getInstance
{
     SalesPlusUser* me = [[SalesPlusUser alloc] init];
    [me autorelease];
    return me;
}



#pragma mark -
#pragma mark Property getters and setters

- (int64_t)surrogateKey
{
    return _surrogateKey;
}

- (void)setPASSWORD:(NSString*)newPASSWORD
{
    if (newPASSWORD != self->_PASSWORD)
    {
        [self setDirty];
        [self->_PASSWORD release];
        self->_PASSWORD = [newPASSWORD retain];
    }
}

- (void)setBNAME:(NSString*)newBNAME
{
    if (newBNAME != self->_BNAME)
    {
        [self setDirty];
        [self->_BNAME release];
        self->_BNAME = [newBNAME retain];
    }
}

- (void)setVKORG:(NSString*)newVKORG
{
    if (newVKORG != self->_VKORG)
    {
        [self setDirty];
        [self->_VKORG release];
        self->_VKORG = [newVKORG retain];
    }
}

- (void)setVTWEG:(NSString*)newVTWEG
{
    if (newVTWEG != self->_VTWEG)
    {
        [self setDirty];
        [self->_VTWEG release];
        self->_VTWEG = [newVTWEG retain];
    }
}

- (void)setSPART:(NSString*)newSPART
{
    if (newSPART != self->_SPART)
    {
        [self setDirty];
        [self->_SPART release];
        self->_SPART = [newSPART retain];
    }
}

- (void)setVKBUR:(NSString*)newVKBUR
{
    if (newVKBUR != self->_VKBUR)
    {
        [self setDirty];
        [self->_VKBUR release];
        self->_VKBUR = [newVKBUR retain];
    }
}

- (void)setVKGRP:(NSString*)newVKGRP
{
    if (newVKGRP != self->_VKGRP)
    {
        [self setDirty];
        [self->_VKGRP release];
        self->_VKGRP = [newVKGRP retain];
    }
}

- (void)setWERK:(NSString*)newWERK
{
    if (newWERK != self->_WERK)
    {
        [self setDirty];
        [self->_WERK release];
        self->_WERK = [newWERK retain];
    }
}

- (void)setLGORT:(NSString*)newLGORT
{
    if (newLGORT != self->_LGORT)
    {
        [self setDirty];
        [self->_LGORT release];
        self->_LGORT = [newLGORT retain];
    }
}

- (void)setKUNNR:(NSString*)newKUNNR
{
    if (newKUNNR != self->_KUNNR)
    {
        [self setDirty];
        [self->_KUNNR release];
        self->_KUNNR = [newKUNNR retain];
    }
}

- (void)setLANGU:(NSString*)newLANGU
{
    if (newLANGU != self->_LANGU)
    {
        [self setDirty];
        [self->_LANGU release];
        self->_LANGU = [newLANGU retain];
    }
}

- (void)setSurrogateKey:(int64_t)newSurrogateKey
{
    if (newSurrogateKey != self->_surrogateKey)
    {
        self.isNew = YES;
        self->_surrogateKey = newSurrogateKey;
    }
}

#pragma mark -
#pragma mark Metadata methods

static SUPEntityMetaDataRBS* SalesPlusUser_META_DATA;

+ (SUPEntityMetaDataRBS*)metaData
{
    if (SalesPlusUser_META_DATA == nil) {
		SalesPlusUser_META_DATA = [[SalesPlusUserMetaData alloc] init];
	}
	
	return SalesPlusUser_META_DATA;
}

- (SUPClassMetaDataRBS*)getClassMetaData
{
    return [[self class] metaData];
}

#pragma mark -
#pragma mark Clear relationship objects

- (void)clearRelationshipObjects
{
}

#pragma mark -
#pragma mark find() and related methods
+ (id)allocInitialTraceRequest:(NSString*)method withSkip:(int32_t)skip withTake:(int32_t)take withSql:(NSString*)sql withValues:(SUPObjectList*)values
{
    id request = nil;
    
    CFSCTraceLevel level = [SUPCFSCTrace getTraceLevel];
    NSMutableString *params = [[[NSMutableString alloc] init] autorelease];

    for (id val in values)
    {
        [params appendFormat:@"%@;", [SUPStringUtil toString_object:val]];
    }
    if (skip >=0)
    	[params appendFormat:@"skip=%d;", skip];
    if (take >= 0)
        [params appendFormat:@"take=%d", take];
    if (level == CFSCTraceLevel_DETAIL)
    {
        
        request = [[SUPCFSCTraceDetailRequest alloc] initWithParams:@"SalesPlusUser" :method :nil:nil:[NSDate date] :nil :0
                                                                   :nil :NO :NO :NO :NO :NO :sql :params];
    } else if (level == CFSCTraceLevel_NORMAL)
    {
        request = [[SUPCFSCTraceRequest alloc] initWithParams:@"SalesPlusUser"  :method :nil :nil  :[NSDate date] :nil :0 :params];
    }
    return request;
}
+ (void)finishTraceRequest:(id)request :(int)rows
{
    if (!request)
    {
        return;
    }
    
    SUPCFSCTraceRequest *traceReq = (SUPCFSCTraceRequest *)request;
    
    traceReq.endTime = [NSDate date];
  
    traceReq.count = rows;
    
    [SUPCFSCTrace log:(id)request];
}

+ (SalesPlusUser*)find:(int64_t)id_
{
    SUPCFSCTraceRequest* request = nil;
    NSString *msg = @"success";
    if ([SUPCFSCTrace isEnabled])
    {
        request = [[SUPCFSCTraceRequest alloc] initWithParams:@"SalesPlusUser" :@"find" 
            :nil :[self metaData].synchronizationGroup  :[NSDate date] :nil :0 :[SUPStringUtil toString_object:[NSNumber numberWithLongLong:id_]]];
    } 
    SalesPlusUser* result = nil;
    @try {
         SUPObjectList *keys = [SUPObjectList getInstance];
         [keys add:[NSNumber numberWithLongLong:id_]];
         result = (SalesPlusUser*)[(SUPEntityDelegate*)([[self class] delegate]) findEntityWithKeys:keys];
         return result;
     }
     @catch (NSException *e) {
        msg = [NSString stringWithFormat:@"fail in SalesPlusUser--find: %@:%@", [e name], [e reason]];        
        CFSCTraceLogError(msg);
        @throw e;
    }
    @finally
    {
        if (request)
        {
            request.endTime = [NSDate date];
            if ([msg isEqualToString:@"success"])
                request.count = 1;

            [SUPCFSCTrace log:(id)request];
            [request release];
        }
    }
}

+ (SUPObjectList*)findWithQuery:(SUPQuery*)query
{
    return (SUPObjectList*)[(SUPEntityDelegate*)([[self class] delegate])  findWithQuery:query:[SalesPlusUser class]];
}

- (int64_t)_pk
{
    return (int64_t)[[self i_pk] longLongValue];
}

+ (SalesPlusUser*)load:(int64_t)id_
{
    return (SalesPlusUser*)[(SUPEntityDelegate*)([[self class] delegate]) load:[NSNumber numberWithLongLong:id_]];
}
#pragma mark -
#pragma mark Log record methods

- (SUPObjectList*)getLogRecords
{
   return [SalesPlusLogRecordImpl findByEntity:@"User":[self keyToString]];
}




#pragma mark -
#pragma mark Description implemetation

- (NSString*)toString
{
	NSString* str = [NSString stringWithFormat:@"\
	User = \n\
	    PASSWORD = %@,\n\
	    BNAME = %@,\n\
	    VKORG = %@,\n\
	    VTWEG = %@,\n\
	    SPART = %@,\n\
	    VKBUR = %@,\n\
	    VKGRP = %@,\n\
	    WERK = %@,\n\
	    LGORT = %@,\n\
	    KUNNR = %@,\n\
	    LANGU = %@,\n\
	    surrogateKey = %qi,\n\
	    isNew = %i,\n\
        isDirty = %i,\n\
        isDeleted = %i,\n\
	\n"
    	,self.PASSWORD
    	,self.BNAME
    	,self.VKORG
    	,self.VTWEG
    	,self.SPART
    	,self.VKBUR
    	,self.VKGRP
    	,self.WERK
    	,self.LGORT
    	,self.KUNNR
    	,self.LANGU
    	,self.surrogateKey
		,self.isNew
		,self.isDirty
		,self.isDeleted
	];
	return str;

}

- (NSString*)description
{
	return [self toString];
}




#pragma mark - 
#pragma mark Delegate method (internal)

static SUPLocalEntityDelegate *g_SalesPlusUser_delegate = nil;

+ (SUPLocalEntityDelegate *) delegate
{
	@synchronized(self) {
		if (g_SalesPlusUser_delegate == nil) {
			g_SalesPlusUser_delegate = [[SUPLocalEntityDelegate alloc] initWithName:@"SalesPlusUser" clazz:[self class]
				metaData:[self metaData] dbDelegate:[SalesPlusERPSalesPlus_SuperUserDB delegate] database:[SalesPlusERPSalesPlus_SuperUserDB instance]];
		}
	}
	
	return [[g_SalesPlusUser_delegate retain] autorelease];
}

#pragma mark -
#pragma mark JSON methods (internal)


- (SUPJsonObject*)getAttributeJson:(int)id_
{
    switch(id_)
    {
        default:
        return [super getAttributeJson:id_];
    }

}
- (void)setAttributeJson:(int)id_ :(SUPJsonObject*)value
{
    switch(id_)
    { 
        default:
            [super setAttributeJson:id_:value];
            break;
    }

}

+ (SUPObjectList*)fromJSONList:(SUPJsonArray*)jsonArray
{
    if(jsonArray == nil)
        return nil;
    
    SUPObjectList* instanceList = [[SUPObjectList alloc] initWithCapacity:1];
    [instanceList autorelease];
    if(instanceList == nil)
        return nil;

    for (SUPJsonObject* jsonObject in jsonArray)
    {
        SalesPlusUser* inst = [[SalesPlusUser alloc] init];
        [inst readJson:jsonObject];
        [instanceList add:inst];
        [inst release];
    }
    return instanceList;
}

+ (SUPJsonArray*)toJSONList:(SUPObjectList*)instanceList
{
    SUPJsonArray* jsonArray = [[SUPJsonArray alloc] init];
    [jsonArray autorelease];
    for (SalesPlusUser* inst in instanceList)
    {
        SUPJsonObject *o = [[SUPJsonObject alloc] init];
        [inst writeJson:o];
        [jsonArray add:o];
        [o release];
    }
    return jsonArray;
}

#pragma mark -
#pragma mark Internal attribute get/set methods
-(SUPLong) getAttributeLong:(int)id_
{
    switch(id_)
    {
    case 3487:
        return self.surrogateKey;
    default:
         return [super getAttributeLong:id_];
    }
}

-(void) setAttributeLong:(int)id_ :(SUPLong)v
{
    switch(id_)
    {
    case 3487:
        self.surrogateKey = v;
        break;;
    default:
        [super setAttributeLong:id_:v];
        break;;
    }
}
-(SUPString) getAttributeString:(int)id_
{
    switch(id_)
    {
    case 3476:
        return self.PASSWORD;
    case 3477:
        return self.BNAME;
    case 3478:
        return self.VKORG;
    case 3479:
        return self.VTWEG;
    case 3480:
        return self.SPART;
    case 3481:
        return self.VKBUR;
    case 3482:
        return self.VKGRP;
    case 3483:
        return self.WERK;
    case 3484:
        return self.LGORT;
    case 3485:
        return self.KUNNR;
    case 3486:
        return self.LANGU;
    default:
         return [super getAttributeString:id_];
    }
}

-(void) setAttributeString:(int)id_ :(SUPString)v
{
    switch(id_)
    {
    case 3476:
        self.PASSWORD = v;
        break;;
    case 3477:
        self.BNAME = v;
        break;;
    case 3478:
        self.VKORG = v;
        break;;
    case 3479:
        self.VTWEG = v;
        break;;
    case 3480:
        self.SPART = v;
        break;;
    case 3481:
        self.VKBUR = v;
        break;;
    case 3482:
        self.VKGRP = v;
        break;;
    case 3483:
        self.WERK = v;
        break;;
    case 3484:
        self.LGORT = v;
        break;;
    case 3485:
        self.KUNNR = v;
        break;;
    case 3486:
        self.LANGU = v;
        break;;
    default:
        [super setAttributeString:id_:v];
        break;;
    }
}
- (id)getAttributeLargeObject:(int)id_ loadFromDB:(BOOL)loadFromDB
{
    switch(id_)
    {
        default:
        return [super getAttributeJson:id_];
    }
}
- (void)setAttributeLargeObject:(int)id_ :(id)value
{
    switch(id_)
    {
        default:
            [super setAttributeJson:id_:value];
            break;
    }

}



#pragma mark -
#pragma mark Object queries and operation implementations



+ (SUPObjectList*)findAll
{
	return [self findAll:0 take:INT_MAX]; 
}


	

+ (SUPObjectList*)findAll:(int32_t)skip take:(int32_t)take
{
	NSMutableString *sql = nil;
	NSMutableString *_selectSQL = nil;
	_selectSQL = [[[NSMutableString alloc] initWithCapacity:141] autorelease];
	[_selectSQL appendString:@" x.\"a\",x.\"b\",x.\"c\",x.\"d\",x.\"e\",x.\"f\",x.\"g\",x.\"h\",x.\"i\",x.\"j\",x.\"l\",x.\"m\" FROM \"erpsalesplus_superuser_1_0_user\" x"];
	SUPObjectList* result = nil;
	sql = [[NSMutableString alloc] initWithFormat:@"select %@", _selectSQL];
	[sql autorelease];
	sql = [[NSMutableString alloc] initWithFormat:@"select %@", _selectSQL];
	[sql autorelease];
	SUPStringList *ids = [SUPStringList listWithCapacity:0];
	SUPObjectList *dts = [SUPObjectList getInstance];
	SUPObjectList* values = [SUPObjectList getInstance];
	id request = [[self class] allocInitialTraceRequest:@"findAll" withSkip:skip withTake:take withSql:sql withValues:values ];
	result = (SUPObjectList*)[[[self class] delegate] findWithSQL:sql withDataTypes:dts withValues:values withIDs:ids withSkip:skip withTake:take withClass:[SalesPlusUser class]];
	[[self class] finishTraceRequest:request :[result size]];
	[request release];
	return result;
}



+ (SalesPlusUser*)findByPrimaryKey:(NSString*)bNAME
{
	NSMutableString *sql = nil;
	NSMutableString *_selectSQL = nil;
	_selectSQL = [[[NSMutableString alloc] initWithCapacity:165] autorelease];
	[_selectSQL appendString:@"SELECT x.\"a\",x.\"b\",x.\"c\",x.\"d\",x.\"e\",x.\"f\",x.\"g\",x.\"h\",x.\"i\",x.\"j\",x.\"l\",x.\"m\" FROM \"erpsalesplus_superuser_1_0_user\" x WHERE x.\"b\" = ?"];
	sql = [[NSMutableString alloc] initWithFormat:@"%@", _selectSQL];
	[sql autorelease];
	SUPStringList *ids = [SUPStringList listWithCapacity:0];
	SUPObjectList *dts = [SUPObjectList getInstance];
	[dts addObject:[SUPDataType forName:@"string"]];
	SUPObjectList* values = [SUPObjectList getInstance];
	[values addObject:bNAME];
	
	id request = [[self class] allocInitialTraceRequest:@"findByPrimaryKey" withSkip:-1 withTake:-1 withSql:sql withValues:values ];	
	SUPObjectList *result = (SUPObjectList*)[[[self class] delegate] findWithSQL:sql withDataTypes:dts withValues:values withIDs:ids withClass:[SalesPlusUser class]];
	[[self class] finishTraceRequest:request :[result size]];
	[request release];
	if(result && ([result size] > 0))
	{   
		SalesPlusUser* cus = (SalesPlusUser*)[result item:0];
	    return cus;
	}
	else
	    return nil;
}

/*!
  @method
  @abstract Generated class method 
  @param query
  @throws SUPPersistenceException
 */
+ (int32_t)getSize:(SUPQuery*)query
{
    return [(SUPEntityDelegate*)([[self class] delegate]) getSize:query];
}

@end