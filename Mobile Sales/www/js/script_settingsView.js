var serverProtocol_Key   = "serverProtocol";
var server_Key    = "serverURL";
var serverSyncPort_Key   = "syncPort";
var serverPort_Key   = "port";
var serverFarmID_Key = "farmID";
var serverSynchFarmID_Key = "synch_farmID";


document.addEventListener("deviceready", onDeviceReady, false);


function waitForNavigate(url) {
    setTimeout(function() {navigateToUrl(url);},1500);
    
}

function navigateToUrl(url) {
    document.location = url;
}

function readFromStorage(key)
{
    return window.localStorage.getItem(key);
}


$(function(){
    $('#cancel_but').on("click", function() {
        navigateToUrl("index.html");
    });
    $('#save_but').on("click", function() {

        var serverProtocol_Value   = document.getElementById('serverProtocol').value;
        var server_Value           = document.getElementById('serverURL').value;
        var serverSyncPort_Value   = document.getElementById('syncPort').value;
        var serverPort_Value       = document.getElementById('port').value;
        var serverFarmID_Value     = document.getElementById('farmID').value;
        var serverSynchFarmID_Value     = document.getElementById('synch_farmID').value;
        
        window.localStorage.setItem(serverProtocol_Key, serverProtocol_Value);
        window.localStorage.setItem(server_Key, server_Value);
        window.localStorage.setItem(serverSyncPort_Key, serverSyncPort_Value);
        window.localStorage.setItem(serverPort_Key, serverPort_Value);
        window.localStorage.setItem(serverFarmID_Key, serverFarmID_Value);
        window.localStorage.setItem(serverSynchFarmID_Key, serverSynchFarmID_Value);
                      
        navigateToUrl("index.html");
    });
    $('#info_but').on('click', function(){
        $('#used_frameworks').text(lang.used_frameworks);
        $( "#app_info" ).dialog({
            resizable: false,
            draggable: false,
            height: 570,
            width: 552,
            modal: true,
            show: 'fade',
            hide: 'fade'
        });
        $('.ui-widget-overlay').on('click', function(){
            $('.ui-widget-overlay').off('click');
            $( "#app_info" ).dialog( "close" );
            $("#pageAside").css("overflow", "auto");
            $("#customers").css("overflow", "auto");
        });
    });
  });


function onDeviceReady() {

    var protocol  = readFromStorage(serverProtocol_Key);
    var server    = readFromStorage(server_Key);
    var syncPort  = readFromStorage(serverSyncPort_Key);
    var port      = readFromStorage(serverPort_Key);
    var farmID    = readFromStorage(serverFarmID_Key);
    var synch_farmID    = readFromStorage(serverSynchFarmID_Key);

    if (protocol) {
        document.getElementById('serverProtocol').value = protocol;
    } else {
        window.localStorage.setItem(serverProtocol_Key, "https");
    }
    if (server) {
        document.getElementById('serverURL').value  = server;
    } else {
        window.localStorage.setItem(server_Key, "mdm.zedach.eu");
    }
    if (syncPort) {
        document.getElementById('syncPort').value = syncPort;
    } else {
        window.localStorage.setItem(serverSyncPort_Key, "443");
    }
    if (port) {
        document.getElementById('port').value = port;
    } else {
        window.localStorage.setItem(serverPort_Key, "443");
    }
    if (farmID) {
        document.getElementById('farmID').value = farmID;
    } else {
        window.localStorage.setItem(serverFarmID_Key, gv_farm_id);
         document.getElementById('farmID').value = gv_farm_id;
    }
    if (synch_farmID) {
        document.getElementById('synch_farmID').value = synch_farmID;
    } else {
        window.localStorage.setItem(serverSynchFarmID_Key, gv_synch_farm_id);
        document.getElementById('synch_farmID').value = gv_synch_farm_id;
    }

}
