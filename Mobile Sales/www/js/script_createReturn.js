(function ($) {
  $.fn.taphold = function (callback, timeout) {
   // bind to element's touchstart event to track the taphold's beginning
   $(this).bind("touchstart", function (event) {
    // save the initial event object
    var initialEvent = event;
    // set the delay after which the callback will be called
    var timer = window.setTimeout(function () { callback(initialEvent); }, timeout);
    // bind to global touchend and touchcancel events for clearance
    $(document).bind("touchend touchcancel", function () {
      // clear timer
      window.clearTimeout(timer);
      // unbind from touchend and touchcancel events
      $(document).unbind("touchend touchcancel");
      return false;
      // use 'return false;' if you need to prevent default handler and
      // stop event bubbling
    });
    return true;
    // use 'return false;' if you need to prevent default handler and
    // stop event bubbling
   });
  };
})(jQuery);

//bojo
function show_product_details(pr_id){
	cordova.exec(fireProductDetails, execFailed, 'Product', 'getProductDetails', [pr_id]);
	
	function fireProductDetails(param){
		var result = eval('(' + param + ')');
		var product = $(".prodets");
		product.find("#product_name_display").html(result["MATL_DESC"]);
		product.find("#product_id_display").html(result["MATNR"].replace(/^0+/, ''));
		product.find("#product_descr").html(result["TEXT"]);
		
			cordova.exec(fireUoM, execFailed, 'Customizing', 'getUnitOfMeasureList', ['']);

			function fireUoM(param2){
				var result_uom = eval('(' + param2 + ')');
				var one = "";
				var two = "";
				for(j=0;j<result_uom.length;j++){
					if(result["BASE_UOM"] == result_uom[j]['MSEHI']){
						one = result_uom[j]['MSEHT'];
					}
					if(result['SalesRels'][0]["VRKME"] == result_uom[j]['MSEHI']){
						two = result_uom[j]['MSEHT'];
					}
				}
				product.find("#type").html(one);
				product.find("#division").html(two);
			}

		
		product.find("#group").html(result["ANZPAK"]);//result["MATL_GROUP"]);
		ppb = parseInt(result["ANZGEB"])/parseInt(result["ANZPAK"]);
		product.find("#cat").html(ppb);//result["MATL_CAT"]);
		if(result["IMAGE"] != ""){
			product.find("#head_information").find('.product').html('<img src="'+result["IMAGE"]+'" />');
		}
		else{
			product.find("#head_information").find('.product').html('<img src="img/product_image.jpg" />');
		}
		$('#dialog_product_details').dialog({
			dialogClass: "with-close",
			title: lang.productPopUp_title,
			show: 'fade',
			hide: "fade",
			width: 550
		});
	}
}

var sold_car_total = 0;
var sold_car_totalQty = 0;
var fg_car_total = 0;
var fg_car_totalQty = 0;
var total_currenty = "";
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
if(dd<10){dd='0'+dd}
if(mm<10){mm='0'+mm}
var date_today =  yyyy.toString()+mm.toString()+dd.toString();
var hour = today.getHours();
if(hour<10){hour='0'+hour}
var minutes = today.getMinutes();
if(minutes<10){minutes='0'+minutes}
var secs = today.getSeconds();
if(secs<10){secs='0'+secs}
var new_doc_num = "*"+yyyy.toString()+mm.toString()+dd.toString()+hour.toString()+minutes.toString()+secs.toString();
var editing = 0;
var help_ed_val = 0;
var zero_elems_check = 0;

function set_unrel(){
	if(editing==1){
		editing=0;
		help_ed_val=1;
		$('.save').text('Save');
		$('.save').show();
	}
}

$(function(){
	$("#set_reqDelDate").on('focus', function(){
		set_unrel();
	});

	$('#prior').on('click', function(){
		set_unrel();
	});

	$("#cson").on('focus', function(){
		set_unrel();
	});

	$("#note1").on('focus', function(){
		set_unrel();
	});

	$("#note2").on('focus', function(){
		set_unrel();
	});
});

//Calculate Business Days between 2 dates:
function calcBusinessDays(startDate, endDate) {
  
    // Validate input
    if (endDate < startDate)
        return 0;
    
    // Calculate days between dates
    var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
    startDate.setHours(0,0,0,1);  // Start just after midnight
    endDate.setHours(23,59,59,999);  // End just before midnight
    var diff = endDate - startDate;  // Milliseconds between datetime objects    
    var days = Math.ceil(diff / millisecondsPerDay);
    
    // Subtract two weekend days for every week in between
    var weeks = Math.floor(days / 7);
    var days = days - (weeks * 2);

    // Handle special cases
    var startDay = startDate.getDay();
    var endDay = endDate.getDay();
    
    // Remove weekend not previously removed.   
    if (startDay - endDay > 1)         
        days = days - 2;      
    
    // Remove start day if span starts on Sunday but ends before Saturday
    if (startDay == 0 && endDay != 6)
        days = days - 1  
            
    // Remove end day if span ends on Saturday but starts after Sunday
    if (endDay == 6 && startDay != 0)
        days = days - 1  
    
    return days;
}

/* Format Dates START */
function successGetDatePattern(datePattern) {
	currentDatePattern = datePattern.pattern;

	//Check if it's edit mode:
	if (editMode != 1){
		var today = new Date();
		var today_year = today.getFullYear();
		var today_month = ("0" + (today.getMonth() + 1)).slice(-2);
		var today_day = today.getDate();
		var today_full = today_year + today_month + today_day;

		//Set Purch date for today:
		$("#purch_date").text(formatDate(today_full));
		$("#purch_date").data("fulldate", today_full);
	}

}
function errorGetDatePattern() {}
function getUsersGlobalization() {
	navigator.globalization.getDatePattern(successGetDatePattern, errorGetDatePattern, {formatLength:'short', selector:'date'});
}
/* Format Dates END */


/* Inital sets SATRT */
var cid = (location.search.match(new RegExp('uid' + "=(.*?)($|\&)", "i")) || [])[1];
var said = (location.search.match(new RegExp('said' + "=(.*?)($|\&)", "i")) || [])[1];
var editMode = (location.search.match(new RegExp('edit' + "=(.*?)($|\&)", "i")) || [])[1];
var currentDatePattern;

$(function(){
  $('#customer_id').val(cid);
  $('#sa_id').val(said);
  $('#cust_nr').text(cid.replace(/^0+/, ''));
  document.addEventListener("deviceready", getUsersGlobalization, false);
});
/* Initial sets END */
var incommingParams = getParams();

function checkEditMode(){
	cordova.exec(cust_dets, execFailed, 'Customer', 'getCustomerDetails', [cid]);
	  function cust_dets(paramc){
	  	var cust_detais = eval('(' + paramc + ')');
	  	$("#cust_nr").text(cust_detais['NAME1'] + " " + cust_detais['NAME2'] +  " (" + cid.replace(/^0+/, '') + "), " + cust_detais["STRAS"] + " " + cust_detais["HOUSENUM"] + " " + cust_detais["PSTLZ"] + " " + cust_detais["ORT01"]);
	  }
	if(incommingParams.edit == 1){
		ifSignature = true;
		
		//Display delete button:
		$("#customers_header .delete").show();
		$("#customers_header .delete").on("click", function(){
			cordova.exec(function(param){
				$("#customers_header #back_salesOrders").click();
			}, execFailed, 'Order', 'deleteSalesOrder', [new_doc_num]);
		});
		
		var doc_nr = incommingParams.suba_id.toString();

		cordova.exec(function(param){
			var result = eval('(' + param + ')');
			//alert(param);
			
			//Header Tab:
			var customersHeader = $("#customers_header");
			customersHeader.find("#cson").val(result["Header"]["BSTKD"]);
			customersHeader.find("#purch_date").text(formatDate(result["Header"]["DOC_DATE"]));
			customersHeader.find("#purch_date").data("fulldate", result["Header"]["DOC_DATE"]);
			new_doc_num_ws = result["Header"]["DOC_DATE"];
			new_doc_num = result["Header"]["DOC_NUMBER"];

			//Set the signature:
			if(result["Header"]["SIGNATURE"] != "" && result["Header"]["SIGNATURE"] != undefined){
				$("#clear_signature").show();

				var canvas = $("#signature").get(0);
				var ctx = canvas.getContext("2d");

				var image = new Image();
				image.src = "data:image/  png;base64," + result["Header"]["SIGNATURE"];
				image.onload = function() {
					ctx.drawImage(image, 0, 0);
				};
			}
			
			var req_date = result["Header"]["REQ_DATE_H"];
			var req_date_year = req_date.substr(0, 4);
			var req_date_month = req_date.substr(4, 2);
			var req_date_day = req_date.substr(6, 2);
			customersHeader.find("#set_reqDelDate").val(req_date_year + "-" + req_date_month + "-" + req_date_day); //20131020
			if(result["Items"][0]["DLV_PRIO"] == "20"){
				$('#prior').attr('checked', 'checked');
				$('#prior').val("20");
			}
			else{
				$('#prior').removeAttr('checked');
			}
			customersHeader.find("#note1").val(result["Header"]["KTEXT1"]);
			customersHeader.find("#note2").val(result["Header"]["KTEXT2"]);
			$('.save').show();
			
			//Get product info
			function get_product_info(product_id, result, i){
				cordova.exec(function(param_product){
						//alert(param_product);
						var product_detais = eval('(' + param_product + ')');
						//alert(JSON.stringify(result, null, 4));

						var product_name = product_detais["MATL_DESC"];
						var anzgeb = parseInt(product_detais["ANZGEB"])/parseInt(product_detais["ANZPAK"]);
						var batchTrHighest = product_detais["ConditionItems"][0]["KBETR"];
						var batchTrHighest_id = product_detais["ConditionItems"][0]["CHARG"];
						var cur_text = "";
						var shiban_real_val = 0;
						for(j=0;j<product_detais['ConditionItems'].length;j++){
							//alert('44');
							//alert(JSON.stringify(result["Items"][i]["BATCH"], null, 4));
							//alert(JSON.stringify(product_detais['ConditionItems'][j]['CHARG'], null, 4));
							if(result["Items"][i]["BATCH"] == product_detais['ConditionItems'][j]['CHARG']){
								cur_text = product_detais['ConditionItems'][j]['CHARG'] +" - "+ product_detais['ConditionItems'][j]['KBETR'] + " " + product_detais['ConditionItems'][j]['KONWA'];
								shiban_real_val = product_detais['ConditionItems'][j]['KBETR'];
							}
						}
						//alert(JSON.stringify(product_detais, null, 4));
						switch(result["Items"][i]["ITEM_CATEG"]){
							case 'RV01':
								//alert(1);
								//***Sold Car:
								var q_sold_car = result["Items"][i]["REQ_QTY"];
								var act_e = result["Items"][i]["DLVSCHEDUS"];
								//var q_sold_car = 0;
								var q_sold_car_realVal = shiban_real_val;
								var q_sold_car_IdBatchCur = cur_text;

								//FG Car:
								var q_fg_car = 0;
								var q_fg_car_realVal = 0;
								var q_fg_car_IdBatchCur = "";
								//Sold Stock:
								var q_sold_stock = 0;
								//FG Stock:
								var q_fg_stock = 0;
								break;
							case 'RV02':
								//alert(2);
								//Sold Car:
								var q_sold_car = 0;
								var act_e = result["Items"][i]["DLVSCHEDUS"];
								var q_sold_car_realVal = 0;
								var q_sold_car_IdBatchCur = "";

								//***FG Car:
								var q_fg_car = result["Items"][i]["REQ_QTY"];
								//var q_fg_car = 0;
								var q_fg_car_realVal = shiban_real_val;
								var q_fg_car_IdBatchCur = cur_text;

								//Sold Stock:
								var q_sold_stock = 0;
								//FG Stock:
								var q_fg_stock = 0;

								break;
						}

						putProductInBasket(product_id, product_name, batchTrHighest, batchTrHighest_id, q_sold_car, q_sold_car_realVal, q_sold_car_IdBatchCur, q_fg_car, q_fg_car_realVal, q_fg_car_IdBatchCur, q_sold_stock, q_fg_stock, anzgeb, act_e);

					}, execFailed, 'Product', 'getProductDetails', [product_id.toString()]);
				}

			//Article Tab:
			function putProductInBasket(product_id, product_name, batchTrHighest, batchTrHighest_id, q_sold_car, q_sold_car_realVal, q_sold_car_IdBatchCur, q_fg_car, q_fg_car_realVal, q_fg_car_IdBatchCur, q_sold_stock, q_fg_stock, anzgeb, act_e){
				var currentProduct = product_id;
				
				//TO DO:
				var bundle = anzgeb;
				if(bundle == undefined || bundle == ""){
					bundle = "-";
				}

				//Action Select:
				var abrvwList;
				var action_select = "";
				cordova.exec(function(abrvw_param){
					var abrvwList = eval('(' + abrvw_param + ')');
					//alert(abrvw_param);
					if(act_e != ""){
						action_select = '<select id="action_select">';
						for(i = 0; i < abrvwList.length; i++){
							if(act_e == abrvwList[i]["ABRVW"]){
								action_select += '<option selected value="' + abrvwList[i]["ABRVW"] + '">' + abrvwList[i]["BEZEI"]  + '</option>';
							} else {
								action_select += '<option value="' + abrvwList[i]["ABRVW"] + '">' + abrvwList[i]["BEZEI"]  + '</option>';
							}
						}
					} else {
						action_select = '<select id="action_select" style="display: none;">';
						for(i = 0; i < abrvwList.length; i++){
							action_select += '<option value="' + abrvwList[i]["ABRVW"] + '">' + abrvwList[i]["BEZEI"]  + '</option>'
						}
					}
					
					
					action_select += '</select>';
					

					//Attach the highest BATCH!!
					//TO DO:
					var batchTr = batchTrHighest;
					var batchTr_id = batchTrHighest_id;
					/*
					var batchTr = $(this_el).find(".number").data("highestbatch");
					var batchTr_id = $(this_el).find(".number").data("highestbatchid");
					*/
					//alert(q_sold_car);
					var roll_sold_car = parseFloat(q_sold_car_realVal) * parseFloat(q_sold_car);
					var roll_fg_car = parseFloat(q_fg_car_realVal) * parseFloat(q_fg_car);
					var roll_total = roll_sold_car + roll_fg_car;

					$("#customers_createReturns .ordersTable tbody").append("<tr class='line_height' data-highestbatch='" + batchTr + "' data-highestbatch_id='" + batchTr_id + "' data-productid='" + product_id + "'><td>" + product_name + '</td><td class="quantity_sold_car"><span class="data_number">' + q_sold_car + '</span><br /><span class="data_label" style="font-size: 10px;" data-realval="' + q_sold_car_realVal + '">' + q_sold_car_IdBatchCur + '</span></td><td class="quantity_fg_car"><span class="data_number">' + q_fg_car + '</span><br /><span class="data_label" style="font-size: 10px;" data-realval="' + q_fg_car_realVal + '">' + q_fg_car_IdBatchCur + '</span></td><td class="action">' + action_select + '</td><td>' + bundle + '</td><td class="kvp_result">'+roll_total.toFixed(2)+'</td><td class="deleteTd"><span class="deleteItem"></span></td></tr>');
					calculateArticleTotals();
					$('#but_header').click();
				}, execFailed, 'Customizing', 'getABRVWList', ['']);

		        // if(!$('#customers_createOrders').is(':visible')){
		        // 	$('#but_article').trigger('click');
		        // }
			}
			var customersArticle = $("#customers_createOrders");
			var articleItems = result["Items"];

			for(i = 0; i < articleItems.length; i++){
				var product_id = articleItems[i]["MATERIAL"];

				get_product_info(product_id, result, i);
			}

		}, execFailed, 'Order', 'getSalesOrderDetails', [doc_nr]);
	} else {
		//Fire create mode.
	}
}

function less_quantity(){
	var quan_var = $('#quantity').val();
	if(parseInt(quan_var) > 0){
		$('#quantity').val(parseInt(quan_var) - 1);
	}
}

function close_dialog(){
	var curent_el = $('#quantity').data("id_item");
	var current_td = $('#quantity').data("id_td");
	var result_el = $("#customers_createReturns .ordersTable tr[data-productid=" + current_td + "] ." + curent_el);

	result_el.find(".data_number").text($('#quantity').val());

	if($('#batch_select').css('display') != "none"){
		result_el.find(".data_label").css('font-size', '10px').text($('#batch_select option:selected').text());
		result_el.find(".data_label").data("realval", $('#batch_select option:selected').data("kvpval"));
	}


	if(curent_el == "quantity_fg_car"){
		result_el.parent().find("#action_select").show();
	}
	if(result_el.parent().find(".quantity_fg_car .data_number").text() == 0){
		result_el.parent().find("#action_select").hide();
	}
	
	calculateArticleTotals();

	//Calculate KVP:
	var currentRoll = $("#customers_createReturns .ordersTable tr[data-productid=" + current_td + "]");
	//alert(currentRoll.html());
	var roll_sold_car = parseFloat(currentRoll.find(".quantity_sold_car .data_label").data("realval")) * parseFloat(currentRoll.find(".quantity_sold_car .data_number").text());

	var roll_fg_car = parseFloat(currentRoll.find(".quantity_fg_car .data_label").data("realval")) * parseFloat(currentRoll.find(".quantity_fg_car .data_number").text());
	
	var roll_total = roll_sold_car + roll_fg_car;

	currentRoll.find(".kvp_result").text(roll_total.toFixed(2));


	$('.ui-dialog-titlebar-close').trigger('click');
}

function more_quantity(){
	$('#quantity').val(parseInt($('#quantity').val()) + 1);
}

//Get parameters from url
function getParams() {
	var prmstr = window.location.search.substr(1);
	var prmarr = prmstr.split ("&");
	var params = {};

	for ( var i = 0; i < prmarr.length; i++) {
	    var tmparr = prmarr[i].split("=");
	    params[tmparr[0]] = tmparr[1];
	}

	return params;
} 

var getParam = getParams();

function execFailed(param) {
    var execFailedDict = {};
	execFailedDict[param] = "error";
	customAlert(execFailedDict);
}


//Calculate Article Table Totals:
function calculateArticleTotals(){
	var table = $("#customers_createReturns .ordersTable tbody");
	var sold_car_total = 0;
	var sold_car_total_sum = 0;
	var fg_car_total = 0;
	var fg_car_total_sum = 0;

	//TO DO
	$("#customers_createReturns .ordersTable tbody td:nth-child(2)").each(function(index, el){
		sold_car_total += parseInt($(el).find(".data_number").text());
		sold_car_total_sum += parseFloat($(el).find(".data_number").text()) * parseFloat($(el).find(".data_label").data("realval"));
	});
	$("#customers_createReturns .ordersTable tbody td:nth-child(3)").each(function(index, el){
		fg_car_total += parseInt($(el).find(".data_number").text());
		fg_car_total_sum += parseFloat($(el).find(".data_number").text()) * parseFloat($(el).find(".data_label").data("realval"));
	});

	$(".total_of_sold_car").text(sold_car_total);
	$(".total_of_sold_car").data("totalsum", sold_car_total_sum.toFixed(2));
	$(".total_of_fg_car").text(fg_car_total);
	$(".total_of_fg_car").data("totalsum", fg_car_total_sum.toFixed(2));
}

function getProductsList(){
    cordova.exec(fireProductsList, execFailed, 'Product', 'getProductList', ['']);

    function fireProductsList(param) {
    	//alert(param);
		function record(product_name, product_id, product_anzgeb){
			var productRecord = "";

			//set active product
			if(product_id==getParam.matnr){
				$('#categories').removeClass();
				$('#all_products').addClass('active');
				$('#listed_products li.active, #listed_categories li.active').removeClass();
				$('.back_button').hide();
				$('#listed_categories').hide(function(){
					$('#listed_products').show();
					$('.search_holder').show();
				});
				productRecord += '<li class="active"><div class="arrow group"><div class="cart"><div class="products_count">cart_width</div><div class="cart_icon"></div></div><span class="name">';

			}
			else
			{
				productRecord += '<li><div class="arrow group"><div class="cart"><div class="products_count">cart_width</div><div class="cart_icon"></div></div><span class="name">';
			}
			productRecord += product_name;
			productRecord += '</span><span class="number" data-productid="' + product_id + '" data-anzgeb="' + product_anzgeb + '">'
			productRecord += product_id;
			productRecord += '</span></div></li>';
		
			return productRecord;
		};
		function gallery_record(product_name, product_id){
			var gallery_item = "";
			gallery_item += '<div class="product_holder"><div class="product_img"><img src="img/product_image.png" /></div><div class="product_title" data-productid="' + product_id + '"><span>';
			gallery_item += product_name;
			gallery_item += '</span></div></div>';

			return gallery_item;
		}
	
		var result = eval('(' + param + ')');
		var result_html = "";
		var gallery_html = "";
		for(i = 0; i < result.length; i++){
			var product_name = result[i]["MATL_DESC"];
			var product_id = result[i]["MATNR"];
			//var product_anzgeb = result[i]["ANZGEB"];
			var product_anzgeb = parseInt(result[i]["ANZGEB"])/parseInt(result[i]["ANZPAK"]);
			result_html += record(product_name, product_id, product_anzgeb);
			
			if(i%12 == 0){
				if(i%12 == 0 && i != 0){
					gallery_html += '</div>';
				}
				gallery_html += '<div class="swiper-slide" style="height: 100%; width: 100%;">';
			}
			gallery_html += gallery_record(product_name, product_id);
		}

		$("#pageAside ul#listed_products").html(result_html);

		//Remove Loading Animation:
		$('#loadingBar').fadeOut(500);
		$('#pageAside ul li').each(function(index, el){
		    $(this).taphold(function (e) {
	    		show_product_details($(el).find('.number').data('productid'));
	    	},
           1500);
		});
		
		//For Products.html only!
		//if we have active products which is set from home page.
		/*
		if(getParam.matnr){
			getProductDetails(getParam.matnr);
			//$(".swiper-wrapper").hide();
			var activeArrow = '<span class="active_arrow"></span>';
			$("#pageNav ul li:eq(2)").append(activeArrow);
			setTimeout(function() {
				$("#pageAside").scrollTop(($('#listed_products .active').position().top) - $('#listed_products .active').height() - 20);
				$('#pageDownNav li').removeClass('active');
				$('#pageDownNav li#general').addClass('active');

			}, 500);
		}
		*/
		//$(".swiper-wrapper").html(gallery_html);

	}
}


//Product Categories START:
function getCategories(){
	$('#goBack').hide();
	cordova.exec(fireProductCategories, execFailed, 'Product', 'getCategories', ['']);
	function fireProductCategories(param){
		var categories_result = eval('(' + param + ')');

		function record(product_name, product_id, has_children, product_name_real){
			var productRecord = "";
			productRecord += '<li><div class="arrow group"><div class="cart"><div class="products_count">cart_width</div><div class="cart_icon"></div></div><span class="name" data-realname="' + product_name_real + '">';
			productRecord += product_name;
			productRecord += '</span><span class="number" data-productid="' + product_id + '" data-haschildren="' + has_children + '">';
			productRecord += product_id.replace(/^0+/, '');
			productRecord += '</span></div></li>';
			return productRecord;
		};
	
		//var result = eval('(' + param + ')');
		var result_html = "";
		for(i = 0; i < categories_result.length; i++){
			var product_name = categories_result[i]["CATEGORYDESCRIPTION"];
			var product_id = categories_result[i]["CATEGORYID"];
            var has_children = categories_result[i]["HASCHILDREN"];
			var product_name_real = categories_result[i]["CATALOGUENAME"];
            result_html += record(product_name, product_id, has_children, product_name_real);
		}
		$("#pageAside ul#listed_categories").html(result_html);
	}
}

//Get Product SubCategories:
function getSubCategories(category_name, category_id){
	$('#goBack').show();
	cordova.exec(fireProductSubCategories, execFailed, 'Product', 'getCategories', [category_name, category_id]);
	function fireProductSubCategories(param){
		var sub_categories_result = eval('(' + param + ')');

		function record(product_name, product_id, has_children, product_name_real){
			var productRecord = "";
			productRecord += '<li><div class="arrow group"><div class="cart"><div class="products_count">cart_width</div><div class="cart_icon"></div></div><span class="name" data-realname="' + product_name_real + '">';
			productRecord += product_name;
			productRecord += '</span><span class="number" data-productid="' + product_id + '" data-haschildren="' + has_children + '">';
			productRecord += product_id.replace(/^0+/, '');
			productRecord += '</span></div></li>';
			//alert(product_name_real);
			return productRecord;
		};
	
		//var result = eval('(' + param + ')');
		var result_html = "";
		for(i = 0; i < sub_categories_result.length; i++){
			var product_name = sub_categories_result[i]["CATEGORYDESCRIPTION"];
			var product_id = sub_categories_result[i]["CATEGORYID"];
			var has_children = sub_categories_result[i]["HASCHILDREN"];
			var product_name_real = sub_categories_result[i]["CATALOGUENAME"];
			result_html += record(product_name, product_id, has_children, product_name_real);
		}
		$("#pageAside ul#listed_categories").html(result_html);

	}
}

function getProductListByCategory(category_id){
	$('#goBack').show();
	cordova.exec(fireProductListByCategory, execFailed, 'Product', 'getProductListByCategory', [category_id]);
	function fireProductListByCategory(param){
		//alert(param);
		var sub_categories_result = eval('(' + param + ')');
		
		function record(product_name, product_id, has_children, indica){
			var indicator = '<span class="listViewFlag"></span>';
			if (indica != "50"){
				var indicator = "";
			}
			var productRecord = "";
			productRecord += '<li><div class="arrow group"><div class="cart"><div class="products_count">cart_width</div><div class="cart_icon"></div></div><span class="name">';
			productRecord += indicator + product_name;
			productRecord += '</span><span class="number" id="so_' + product_id + '" data-productid="' + product_id + '" data-haschildren="' + has_children + '">';
			productRecord += product_id.replace(/^0+/, '');
			productRecord += '</span></div></li>';
		
			return productRecord;
		};
	
		//var result = eval('(' + param + ')');
		var result_html = "";

		//clear the list:
		$("#pageAside ul#listed_categories").html("");
		
		for(i = 0; i < sub_categories_result.length; i++){
			var product_name = sub_categories_result[i]["MATL_DESC"];
			var product_id = sub_categories_result[i]["MATNR"];
			var indicator = sub_categories_result[i]["VMSTA"];
			
			$("#pageAside ul#listed_categories").html($("#pageAside ul#listed_categories").html() + record(product_name, product_id, 0, indicator));

            var batches = sub_categories_result[i]['ConditionItems'];
            if (batches.length == 0) {

            	//FREE PRODUCT:

            	//Set All BATCHes:
	            $('#so_'+product_id).attr('data-batches', "000 - 000 ");
	            //Set Highest BATCH:
	            $('#so_'+product_id).attr('data-highestbatch', "0");
	            $('#so_'+product_id).attr('data-highestbatchid', "000");

            } else {
            	var html_string = "";
	            for(ii=0;ii<batches.length;ii++){
	                if(ii>0) html_string += '_';
	                html_string += batches[ii]['CHARG']+' - '+batches[ii]['KBETR']+' '+batches[ii]['KONWA'];
	            }
	            
	            //Set All BATCHes:
	            $('#so_'+product_id).attr('data-batches', html_string);

	            //Set Highest BATCH:
	            $('#so_'+product_id).attr('data-highestbatch', batches[0]['KBETR']);
	            $('#so_'+product_id).attr('data-highestbatchid', batches[0]['CHARG']);
            }
            
            //Set ANZGEB (Action):
            var product_anzgeb2 = parseInt(sub_categories_result[i]["ANZGEB"])/parseInt(sub_categories_result[i]["ANZPAK"]);
	        $('#so_'+product_id).attr('data-anzgeb', product_anzgeb2);
   
            
		}


		$("#pageAside ul#listed_categories").off("click", "li");
		$("#pageAside ul#listed_categories").on("click", "li", function(){
			$("#pageAside ul#listed_categories li").removeClass("active");
			$(this).addClass("active");
			// $("#pageDownNav li").removeClass("active");


			//Fire product in basket:
			$("#pageAside li").removeClass("active");
			$(this).parent().addClass("active");
			$("#pageDownNav #general").addClass("active");
			$('.save').show();
			fireProductInBasket(this);

			return false;
		});
		$('#pageAside ul#listed_categories li').each(function(index, el){
		    $(this).taphold(function (e) {
		       		//alert($(e.target).find('.number').data('productid'));
			    	show_product_details($(el).find('.number').data('productid'));
           		 },
           1500);
		});
	}
}
function getParentCategory(){
	cordova.exec(fireParentCategory, execFailed, 'Product', 'getParentCategory', ['']);
	function fireParentCategory(param){
		var categories_result = eval('(' + param + ')');

		//Detect if main category is reached!
		if(categories_result[0]["PARENTCATEGORYID"] == "0000000000"){
			$('#goBack').hide();
		}

		function record(product_name, product_id, has_children, product_name_real){
			var productRecord = "";
			productRecord += '<li><div class="arrow group"><div class="cart"><div class="products_count">cart_width</div><div class="cart_icon"></div></div><span class="name" data-realname="' + product_name_real + '">';
			productRecord += product_name;
			productRecord += '</span><span class="number" data-productid="' + product_id + '" data-haschildren="' + has_children + '">';
			productRecord += product_id.replace(/^0+/, '');
			productRecord += '</span></div></li>';
			return productRecord;
		};
	
		//var result = eval('(' + param + ')');
		var result_html = "";
		for(i = 0; i < categories_result.length; i++){
			var product_name = categories_result[i]["CATEGORYDESCRIPTION"];
			var product_id = categories_result[i]["CATEGORYID"];
            var has_children = categories_result[i]["HASCHILDREN"];
			var product_name_real = categories_result[i]["CATALOGUENAME"];
			result_html += record(product_name, product_id, has_children, product_name_real);
		}
		$("#pageAside ul#listed_categories").html(result_html);
	}
}
//Product Categories END:




//Get Products from last three Sales Orders:
function getProductsFromLastOrders(){
	cordova.exec(fireProductsFromLastOrders, execFailed, 'Order', 'getRecentSalesOrderProductsForCustomer', [$("#customer_id").val(), "3"]);
	function fireProductsFromLastOrders(param_latest){
		//alert(param_latest);
		$("#pageAside ul#listed_lastproducts").html("");

		/* THE OLD WAY of getting ANZGEB:
		function set_anzgeb(product_id){
			cordova.exec(fireProductDetails, execFailed, 'Product', 'getProductDetails', [product_id]);
	
			function fireProductDetails(param){
				var result = eval('(' + param + ')');
				if(result['ConditionItems'] != undefined){
					//alert("NOT EXIST");
				}
					//alert(JSON.stringify(result['ConditionItems'], null, 4));
					var batches = result['ConditionItems'];
					var html_string = "";
					var highestB = [];
					for(ii=0;ii<batches.length;ii++){
						if(ii>0) html_string += '_';
						html_string += batches[ii]['CHARG']+' - '+batches[ii]['KBETR']+' '+batches[ii]['KONWA'];
						highestB.push(parseInt(batches[ii]['CHARG']));
					}
					
					$('#so_'+product_id).attr('data-batches', html_string);

					var highestBatchItem = highestB.sort(function(a,b){return b-a;})[0];
					
					for(ii=0;ii<batches.length;ii++){
						if(parseInt(batches[ii]['CHARG']) == highestBatchItem) {
							$('#so_'+product_id).attr('data-highestbatch', batches[ii]['KBETR']);
							break;
						}
					}

					$('#so_'+product_id).attr('data-anzgeb', result["ANZGEB"]);
				//}
				
			}	
		}
		*/


		function record(product_name, product_id){
			var productRecord = "";
			productRecord += '<li><div class="arrow group"><div class="cart"><div class="products_count">cart_width</div><div class="cart_icon"></div></div><span class="name">';
			productRecord += product_name;
			productRecord += '</span><span class="number" id="so_' + product_id + '" data-productid="' + product_id + '">'
			productRecord += product_id;
			productRecord += '</span></div></li>';
			return productRecord;
		};

		var result = eval('(' + param_latest + ')');
		for(i = 0; i < result.length; i++){
            var product_name = result[i]["MATL_DESC"];
            var product_id = result[i]["MATNR"];
            $("#pageAside ul#listed_lastproducts").html($("#pageAside ul#listed_lastproducts").html() + record(product_name, product_id/*, product_anzgeb*/));

            //alert(JSON.stringify(result['ConditionItems'], null, 4));
            var batches = result[i]['ConditionItems'];
            var html_string = "";
            for(ii=0;ii<batches.length;ii++){
                if(ii>0) html_string += '_';
                html_string += batches[ii]['CHARG']+' - '+batches[ii]['KBETR']+' '+batches[ii]['KONWA'];
            }
            
            //Set All BATCHes:
            $('#so_'+product_id).attr('data-batches', html_string);
            
            //Set Highest BATCH:
            $('#so_'+product_id).attr('data-highestbatch', batches[0]['KBETR']);
            $('#so_'+product_id).attr('data-highestbatchid', batches[0]['CHARG']);
            
            //Set ANZGEB (Action):
            var anzgeb2 = parseInt(result[i]["ANZGEB"])/parseInt(result[i]["ANZPAK"]);
            $('#so_'+product_id).attr('data-anzgeb', anzgeb2);
		}
		
	}
}


/*
function getProductListByCategory(category_id){
	cordova.exec(fireProductListByCategory, execFailed, 'Product', 'getProductListByCategory', [category_id]);
	function fireProductListByCategory(param){
		var sub_categories_result = eval('(' + param + ')');
		
		function record(product_name, product_id, has_children){
			var productRecord = "";
			productRecord += '<li><div class="arrow group"><div class="cart"><div class="products_count">cart_width</div><div class="cart_icon"></div></div><span class="name">';
			productRecord += product_name;
			productRecord += '</span><span class="number" data-haschildren="' + has_children + '">';
			productRecord += product_id;
			productRecord += '</span></div></li>';
		
			return productRecord;
		};
	
		//var result = eval('(' + param + ')');
		var result_html = "";
		for(i = 0; i < sub_categories_result.length; i++){
			var product_name = sub_categories_result[i]["MATL_DESC"];
			var product_id = sub_categories_result[i]["MATNR"];
			result_html += record(product_name, product_id);
		}
		$("#pageAside ul#listed_categories").html(result_html);

		$("#pageAside ul#listed_categories").off("click", "li");
		$("#pageAside ul#listed_categories").on("click", "li", function(){
			$("#pageAside ul#listed_categories li").removeClass("active");
			$(this).addClass("active");
			$("#pageDownNav li").removeClass("active");
			$("#pageDownNav #general").addClass("active");

			//var productId = $(this).find(".number").text();
			//getProductDetails(productId);

			var ifExist = false;
			var currentProduct = $(this).find(".number").data("productid");
			var all_tr = $("#customers_createOrders .ordersTable tbody tr");

			all_tr.each(function(index, domEle){
				if($(domEle).data("productid") == currentProduct) {
					ifExist = true;
				}
			});
			
			if(!ifExist){
				$("#customers_createOrders .ordersTable tbody").append("<tr class='line_height' data-productid='" + $(this).find(".number").data("productid") + "'><td>" + $(this).find(".name").text()  + '</td><td class="quantity_sold_car"><span class="data_number">0</span><br /><span class="data_label"></span></td><td class="quantity_fg_car"><span class="data_number">0</span><br /><span class="data_label"></span></td><td class="quantity_sold_stock"><span class="data_number">0</span><br /><span class="data_label"></span></td><td class="quantity_fg_stock"><span class="data_number">0</span><br /><span class="data_label"></span></td><td class="action"><select><option val="1">1</option><option val="2">2</option></select></td></tr>');

		        if(!$('#customers_createOrders').is(':visible')){
		        	$('#but_article').trigger('click');
		        }
			}

			return false;
		});

		// $("#goBack").data("parentCategoryName", sub_categories_result[0]["CATALOGUENAME"]);
		// $("#goBack").data("parentCategoryId", sub_categories_result[0]["PARENTCATEGORYID"]);
	}
}
*/

function fireProductInBasket(this_el){
	var currentProduct = $(this_el).find(".number").data("productid");
	var all_tr = $("#customers_createReturns .ordersTable tbody tr");

	
	var ifExist = false;
	all_tr.each(function(index, domEle){
		if($(domEle).data("productid") == currentProduct) {
			ifExist = true;
		}
	});
	
	
	if(!ifExist){
		var bundle = $(this_el).find(".number").data("anzgeb");
		if(bundle == undefined || isNaN(bundle)){
			bundle = "-";
		}

		//TO DO: Action
		var abrvwList;
		var action_select = "";
		cordova.exec(function(abrvw_param){
			//alert(abrvw_param);
			var abrvwList = eval('(' + abrvw_param + ')');
			//alert(abrvw_param);
			action_select = '<select id="action_select" style="display: none;">';
			for(i = 0; i < abrvwList.length; i++){
				action_select += '<option value="' + abrvwList[i]["ABRVW"] + '">' + abrvwList[i]["BEZEI"]  + '</option>'
			}
			action_select += '</select>';

			//Attach the highest BATCH!!
			var batchTr = $(this_el).find(".number").data("highestbatch");
			var batchTr_id = $(this_el).find(".number").data("highestbatchid");
			//alert(batchTr);

			$("#customers_createReturns .ordersTable tbody").append("<tr class='line_height' data-highestbatch='" + batchTr + "' data-highestbatch_id='" + batchTr_id + "' data-productid='" + $(this_el).find(".number").data("productid") + "'><td>" + $(this_el).find(".name").text() + '</td><td class="quantity_sold_car"><span class="data_number">0</span><br /><span class="data_label" data-realval="0"></span></td><td class="quantity_fg_car"><span class="data_number">0</span><br /><span class="data_label" data-realval="0"></span></td><td class="action">' + action_select + '</td><td>' + bundle + '</td><td class="kvp_result"></td><td class="deleteTd"><span class="deleteItem"></span></td></tr>');


		}, execFailed, 'Customizing', 'getABRVWList', ['']);

		/*
		$("#customers_header .ordersDetailsTable").append('<tr class="smallerFont"><td>' + $(this_el).find(".name").text() + '</td><td class="quantity_sold_car"><span class="data_number">0</span><br /><span class="data_label"></span></td><td class="quantity_fg_car"><span class="data_number">0</span><br /><span class="data_label"></span></td><td class="quantity_sold_stock"><span class="data_number">0</span><br /><span class="data_label"></span></td><td class="quantity_fg_stock"><span class="data_number">0</span><br /><span class="data_label"></span></td><td></td><td></td>');
		*/

        if(!$('#customers_createReturns').is(':visible')){
        	$('#but_article').trigger('click');
        }
	}
}



$(function(){
	document.addEventListener("deviceready", getProductsList, false);
	document.addEventListener("deviceready", getProductsFromLastOrders, false);
	document.addEventListener("deviceready", checkEditMode, false);

	/* Signature Capturing START */
	var sigCapture = null;
	sigCapture = new SignatureCapture( "signature" );

	//Clear Button:
	$("#clear_signature").on("click", function(){
		sigCapture.clear();
	});
	/* Signature Capturing END */


	//Set Req. Delivery Date:
	var date = new Date();
	for(i = 0; i < 4; i++){
		var someDate = new Date();
		var numberOfDaysToAdd = i;
		var future_date = new Date(someDate.setDate(someDate.getDate() + numberOfDaysToAdd)); 

		var calculated_day = calcBusinessDays(date, future_date);
		//If future day is Saturday or Sunday:
		if (future_date.getDay() != 0 && future_date.getDay() != 6){
			var stringDate = future_date.getFullYear() + "-" + ("0" + (future_date.getMonth()+1)).substr(-2) + "-" + ("0" + future_date.getDate()).substr(-2);
			$("#set_reqDelDate").val(stringDate);

			break;
		} else if ( (future_date.getDay() == 0 || future_date.getDay() == 6) && calculated_day > 0 ){
			var stringDate = future_date.getFullYear() + "-" + ("0" + (future_date.getMonth()+1)).substr(-2) + "-" + ("0" + future_date.getDate()).substr(-2);
			$("#set_reqDelDate").val(stringDate);

			break;
		}
	}
	

	//Styles Fix:
	$("#pageAside").css({
		"height": $('body').height() - ($("#pageHeader").height()) - $("#aside_nav").height()-1
	});

	$('#prior').on('click',function(){
		if($(this).val() == "30"){
			$(this).val('20');
		}
		else{
			$(this).val('30');	
		}
	});

	$('.save').on('click',function(){
		//If there is not a signature:
		if(!ifSignature){
			//Show Signature Error:
			var signatureErr = {};
			signatureErr[lang.createOrder_signatureError] = "error";
			customAlert(signatureErr);
			
			return false;
		}

		//Check if Selected day is today + 3 days:
		var req_date = $("#customers_header #set_reqDelDate").val();
		var req_year = req_date.substr(0, 4);
		var req_month = req_date.substr(5, 2) - 1;
		var req_day = req_date.substr(8, 2);

		var today = new Date();
		var selectedDate = new Date(req_year, req_month, req_day);
		
		var daycount = calcBusinessDays(today, selectedDate);
		//If today is week day:
		if ( (today.getDay() != 0 && today.getDay() != 6) && daycount < 0 ){
			//Show Date Error:
			var freeProduct = {};
			freeProduct[lang.createReturnAndExchange_dateError] = "error";
			customAlert(freeProduct);
			return false;
		}
		else if (selectedDate.getDay() == 0 || selectedDate.getDay() == 6){
			//Show Weekend Error:
			var freeProduct = {};
			freeProduct[lang.createOrder_dateError_weekend] = "error";
			customAlert(freeProduct);
			return false;
		}
		//If today is Weekend:
		else if ( (today.getDay() == 0 || today.getDay() == 6) && daycount < 1 ){
			//Show Date Error:
			var freeProduct = {};
			freeProduct[lang.createOrder_dateError] = "error";
			customAlert(freeProduct);
			return false;
		}

		//Else - Continue:

		var elements_array = [];
		var action_s = "";
		$('.ordersTable tbody tr').each(function(){
			action_s = "";
			zero_elems_check = 0;
			if($(this).find('#action_select').val() != null){
				action_s = $(this).find('#action_select').val();
			}
			if($(this).find('.quantity_sold_car').find('.data_number').text() != "0"){
				zero_elems_check = 1;
				elements_array.push({
								"BATCH"      : $(this).find('.quantity_sold_car').find('.data_label').text().split(' -')[0],
								"DLV_PRIO"   : $('#prior').val(),
								"ITEM_CATEG" : "RV01",
								"MATERIAL"   : $(this).data('productid'),
								"REQ_QTY" : parseInt($(this).find('.quantity_sold_car').find('.data_number').text()),
								"TARGET_QTY" : parseInt($(this).find('.quantity_sold_car').find('.data_number').text()),
								"DLVSCHEDUS" : ""
								});
			}
			if($(this).find('.quantity_fg_car').find('.data_number').text() != "0"){
				zero_elems_check = 1;
				elements_array.push({
								"BATCH"      : $(this).find('.quantity_fg_car').find('.data_label').text().split(' -')[0],
								"DLV_PRIO"   : $('#prior').val(),
								"ITEM_CATEG" : "RV02",
								"MATERIAL"   : $(this).data('productid'),
								"REQ_QTY" : parseInt($(this).find('.quantity_fg_car').find('.data_number').text()),
								"TARGET_QTY" : parseInt($(this).find('.quantity_fg_car').find('.data_number').text()),
								"DLVSCHEDUS" : action_s
								});
			}
			if(zero_elems_check == 0){
				var alert11 = {};
				alert11[lang.zero_NA] = "error";
				customAlert( alert11 ); 
				return false;
			}
		});
		if(elements_array.length == 0 || zero_elems_check == 0){
				var alert11 = {};
				alert11[lang.zero_NA] = "error";
				customAlert(alert11); 
				return;
			}
		
			if(editing==0){

				if(incommingParams.edit == 1 || help_ed_val == 1){ 
					var create_method = "updateSalesOrder";
				}
				else{
					var create_method = "createSalesOrder";
				}
				var head_array = { "SalesOrderHeader" : {
//															"DISTR_CHAN" : "VT",
//												            "SALES_ORG"  : "VT10",
//															"DIVISION"   : "01",
//															"SALES_GRP"  : "334",
//															"SALES_OFF"  : "DT24",
															"DOC_TYPE"	 : "YREV",
															"DOC_NUMBER" : new_doc_num.toString(),
															"KTEXT1"     : $('#note1').val(),
															"KTEXT2"     : $('#note2').val(),
															"REQ_DATE_H" : $('#set_reqDelDate').val().replace(/-/gi,''),
												            "DOC_DATE"   : $('#set_reqDelDate').val().replace(/-/gi,''),
												            "PURCH_DATE" : $('#set_reqDelDate').val().replace(/-/gi,''),
															"SOLD_TO"    : $("#customer_id").val(),
															"BSTKD"   	 : $('#cson').val(),
															"PURCH_NO"	 : $('#cson').val(),
															"VBELV"      : $("#sa_id").val(),
															//"VSBED"		 : $(".save").data("vsbed"),
															"BSTDK"      : date_today,
															"KETDAT"     : date_today,
															"SIGNATURE"	 : sigCapture.toString()
					},
				  "SalesOrderItems" : elements_array 
				};
			}
			else{
				//new_doc_num.replace('*', '+');
				var create_method = "updateSalesOrder";
				var head_array = { "SalesOrderHeader" : {
															"RELEASE"    : "X",
//															"DISTR_CHAN" : "VT",
//												            "SALES_ORG"  : "VT10",
//															"DIVISION"   : "01",
//															"SALES_GRP"  : "334",
//															"SALES_OFF"  : "DT24",
															"DOC_TYPE"	 : "YREV",
															"DOC_NUMBER" : new_doc_num.toString(),
															"KTEXT1"     : $('#note1').val(),
															"KTEXT2"     : $('#note2').val(),
															"REQ_DATE_H" : $('#set_reqDelDate').val().replace(/-/gi,''),
												            "DOC_DATE"   : $('#set_reqDelDate').val().replace(/-/gi,''),
												            "PURCH_DATE" : $('#set_reqDelDate').val().replace(/-/gi,''),
															"SOLD_TO"    : $("#customer_id").val(),
															"BSTKD"   	 : $('#cson').val(),
															"PURCH_NO"	 : $('#cson').val(),
															"VBELV"      : $("#sa_id").val(),
															//"VSBED"		 : $(".save").data("vsbed"),
															"BSTDK"      : date_today,
															"KETDAT"     : date_today,
															"SIGNATURE"	 : sigCapture.toString()
					},
				  "SalesOrderItems" : elements_array 
				};
			}
			//alert(JSON.stringify(elements_array, null, 4));
			
			//Uncomment when done!
			cordova.exec(function(){ 
				if(create_method == "createSalesOrder"){
					$("#customers_header .delete").on("click", function(){
						cordova.exec(function(param){
							$("#customers_header #back_salesOrders").click();
						}, execFailed, 'Order', 'deleteSalesOrder', [new_doc_num]);
					});
				}
				if(editing == 0){
					var saveSucc = {};
					saveSucc[lang.saved_ret] = "success";
					customAlert(saveSucc);
					//customAlert({ "You have successfully saved a Return!" : "success" });
					$("#customers_header .delete").show();
				}
				else{
					customAlert({ "You have successfully released a Return!" : "success" });
					$("#customers_header .delete").hide();
					$("#customers_header #back_salesOrders").click();
				}
				editing=1;
				help_ed_val = 0;
			 }, execFailed, 'Order', create_method, [head_array]);
			$('.save').text('Release');
	});

	//aside_nav
	var categories_result;
	$('#aside_nav a').on("click", function(){
		$("#pageAside").scrollTop(0);
		
		$('#aside_nav a').removeAttr('class');
		$(this).addClass('active');
		
		switch($(this).attr('id')){
			case 'lastproducts':
				$('#listed_lastproducts li.active, #listed_categories li.active').removeClass();
				//$('.back_button').fadeOut();
				$('#listed_categories').fadeOut(function(){
					$('#listed_lastproducts').fadeIn();
				});

				return false;
				break;

			case 'categories':
				$('#listed_lastproducts li.active, #listed_categories li.active').removeClass();
				$('#listed_lastproducts').fadeOut(function(){
					$('#listed_categories').fadeIn();
				});

				
				$("#pageAside ul#listed_categories").off("click", "li");
				$("#pageAside ul#listed_categories").on("click", "li", function(){
					$("#pageAside ul#listed_categories li").removeClass("active");

					var category_id = $(this).find(".number").data("productid");
					var category_name = $(this).find(".name").data("realname");
					var has_children = $(this).find(".number").data("haschildren");
					$('.save').show();
					if(has_children == 0){
						//alert("No more subcategories");
						getProductListByCategory(category_id);
					} else {
						//alert("SubCategories!");
						getSubCategories(category_name, category_id);
					}
				});
				
				getCategories();
				
				/*
				$("#pageAside ul#listed_categories").off("click", "li");
				$("#pageAside ul#listed_categories").on("click", "li", function(){
					$("#pageAside ul#listed_categories li").removeClass("active");

					var category_id = $(this).find(".number").text();
					var category_name = $(this).find(".name").data("realname");
					var has_children = $(this).find(".number").data("haschildren");
					if(has_children == 0){
						//alert("No more subcategories");
						getProductListByCategory(category_id);
					} else {
						//alert("SubCategories!");
						getSubCategories(category_name, category_id);
					}
				});
				
				getCategories();
				
				
				*/
				return false;
				break;
		}
	});


	$("#goBack").on("click", function(){
		getParentCategory();

		$("#pageAside ul#listed_categories").off("click", "li");
		$("#pageAside ul#listed_categories").on("click", "li", function(){
			var category_id = $(this).find(".number").data("productid");
			var category_name = $(this).find(".name").data("realname");
			var has_children = $(this).find(".number").data("haschildren");
			if(has_children == 0){
				//alert("No more subcategories");
				getProductListByCategory(category_id);
			} else {
				//alert("SubCategories!");
				getSubCategories(category_name, category_id);
			}
		});
	});

	/*
	$("#pageAside ul#listed_categories").on("click", "li", function(){
		var category_id = $(this).find(".number").text();
		var category_name = $(this).find(".name").data("realname");
		var has_children = $(this).find(".number").data("haschildren");
		if(has_children == 0){
			//alert("No more subcategories");
			getProductListByCategory(category_id);
		} else {
			//alert("SubCategories!");
			getSubCategories(category_name, category_id);
		}
	});
	*/

	
	$("#but_header").on("click", function(){
		
		//Header Totals:
		var totals = $("#totals_group_ret");
		totals.find(".kpv_sold_car").text($(".total_of_sold_car").data("totalsum"));
		totals.find(".kpv_fg_car").text($(".total_of_fg_car").data("totalsum"));

		//Total - KVP:
		var totalOfEverything = (parseFloat(totals.find(".kpv_sold_car").text()) + parseFloat(totals.find(".kpv_fg_car").text())).toFixed(2);
		if ( isNaN(totalOfEverything) ){
			totalOfEverything = "-";
		}
		totals.find(".total_of_everything").text(totalOfEverything);
		
		//Total items:
		totals.find(".total_items").text($("#customers_createReturns .ordersTable tbody tr").length);

		showSection("#customers_header", ".contentWrapper:visible", this);
	});
	$("#but_article").on("click", function(){
		showSection("#customers_createReturns", ".contentWrapper:visible", this);
	});


	//Table Pop Up:
	$('.ordersTable').on('click', 'td', function() {
		var current = $(this).attr("class").split(" ")[0];
        set_unrel();
		if(current.indexOf("quantity_") != "-1"){

			if(current == "quantity_sold_car" || current == "quantity_fg_car"){
				$('#quantity').val($(this).find(".data_number").text());
				$('#quantity').data("id_td", $(this).parent().data("productid"));
				$('#quantity').data("id_item", $(this).attr("class"));
				
				var batches_arr = $('#so_'+$(this).parent().data("productid")).data("batches").split('_');
				$("#batch_select").html('');

				for(j=0;j<batches_arr.length; j++){
					var kvp = batches_arr[j].split("-")[1].split(" ")[1];
					var el_val = batches_arr[j].split(" -")[0];
					$("#batch_select").append('<option data-kvpval="' + kvp + '" value="' + el_val + '">'+batches_arr[j]+'</option>');
				}


				//Set Default BATCH:
				var selected = $(this).find(".data_label").text();
				if (selected != ""){
					$("#batch_select").val(selected.substring(0, 3));
				}

				$("#dialog").dialog({
					title: lang.dialog_title,
					show: 'fade',
					hide: 'fade',
					width: 400
				});
				$("#add_but").val(lang.dialog_add);
				$("#closeDialog_but").val(lang.dialog_cancel);
				
				$("#batch_select").show();
			} else {
				$('#quantity').val($(this).find(".data_number").text());
				$('#quantity').data("id_td", $(this).parent().data("productid"));
				$('#quantity').data("id_item", $(this).attr("class"));

				$( "#dialog" ).dialog();
				$("#batch_select").hide();
			}
			
		}
	});

	/*
	//Action list:
	var abrvwList;
	cordova.exec(function(abrvw_param){
		var abrvwList = eval('(' + abrvw_param + ')');
		
		var abrvwSelect = "<select class='data_select'>";
		for(i = 0; i < abrvwList.length; i++){
			abrvwSelect += "<option value='" + abrvwList["VAL"] + "'>" + abrvwList["TEXT"]  + "</option>";
		}
		abrvwSelect += "</select>";

	}, execFailed, 'Customizing', 'getABRVWList', ['']);
	*/

	$("#pageAside ul#listed_products").on("click", "li", function(event, target) {
		$("#pageAside li, #pageDownNav li").removeClass("active");
		$(this).parent().addClass("active");
		$("#pageDownNav #general").addClass("active");
		$('.save').show();
		fireProductInBasket(this);
        
	});

	$("#pageAside ul#listed_lastproducts").on("click", "li", function(){
		$('.save').show();
		fireProductInBasket(this);
	});

	function DayCount(year, month, day)
	{ 
	   //var d1 = new Date(2004,09,29);
	   var d1 = new Date();
	   var d2 = new Date(year, month, day);
	   var flag=true;
	   var day,daycount=0;
	   while(flag) 
	    {
	      day=d1.getDay();
	      if(day != 0 && day != 6)
	         daycount++;
	      d1.setDate(d1.getDate()+1) ;
	      if(d1.getDate() == d2.getDate() && 
	                  d1.getMonth()== d2.getMonth())
	      {
	          flag=false;
	      }
		}
		
		return daycount;
	}


	
	/*
	$("#set_reqDelDate").blur(function(){

		var req_date = $("#customers_header #set_reqDelDate").val();
		var req_year = req_date.substr(0, 4);
		var req_month = req_date.substr(5, 2);
		var req_day = req_date.substr(8, 2);

		var diff = DayCount(req_year, req_month, req_day);

		if(diff < 3){
			$(this).val("");
		}

		//alert(diff);
	});
	*/
	
	
	
	/*
	$("#customers_header #set_reqDelDate").blur(function(event){
		var a = $(this).val();
		console.log(a);
	});
	*/
	
	

	
	/*
	$("#customers_header #set_reqDelDate").datepicker({
		//showOn: "button",
		onClose: function(dateText, inst) { $(this).attr("readonly", false); },
		beforeShow: function(dateText, inst) { $(this).attr("readonly", true); },
		//buttonImage: "/images/calendar.png",
		//buttonImageOnly: true
	});
	*/

	/*
	$("#customers_header #set_reqDelDate").datepicker({
		beforeShow: function(){
			$("#customers_header #set_reqDelDate").blur();
		}
	});
	*/


	/*
	$("#customers_header #set_reqDelDate").blur(function(event) {
		//alert($(this).val());
		var req_date = $("#customers_header #set_reqDelDate").val();
		var req_year = req_date.substr(0, 4);
		var req_month = req_date.substr(5, 2);
		var req_day = req_date.substr(8, 2);

		var diff = DayCount(req_year, req_month, req_day);

		console.log(diff);
	});
	*/

	$("#customers_header #back_salesOrders").on("click", function(){
		window.location = "sales_activities.html?uid=0";
	});

	$("#closeDialog_but").on("click", function(){
		$("td").removeClass("currentElement");
		$('.ui-dialog-titlebar-close').trigger('click');
	});


	//Delete item from the orders basket:
	$("#customers_createReturns .ordersTable tbody").on("click", ".deleteItem", function(){
		set_unrel();
		$(this).parent().parent().fadeOut(function(){
			$(this).remove();
			calculateArticleTotals();
		});
	});

});
