var reports_en = {
	"report_jan" : "January",
	"report_feb" : "February",
	"report_mar" : "March",
	"report_apr" : "April",
	"report_may" : "May",
	"report_jun" : "June",
	"report_jul" : "July",
	"report_aug" : "August",
	"report_sep" : "September",
	"report_oct" : "October",
	"report_nov" : "November",
	"report_dec" : "December"
};

var reports_de = {
	"report_jan" : "Januar",
	"report_feb" : "Februar",
	"report_mar" : "März",
	"report_apr" : "April",
	"report_may" : "Mai",
	"report_jun" : "Juni",
	"report_jul" : "Juli",
	"report_aug" : "August",
	"report_sep" : "September",
	"report_oct" : "Oktober",
	"report_nov" : "November",
	"report_dec" : "Dezember"
};
var ppb = 0
var gallery_indicator = 0;
var generalBut_indicator = 0;
var gallery_inDetails_mode = 0;
var current_username = "";
var current_password ="";
function getUserCredentials(){
	cordova.exec(storeCredentials, execFailed, 'Login', 'getCurrentUserCredentials', ['']);	

	function storeCredentials(param){
		var result = eval('(' + param + ')');
		current_username = result.username;
		current_password = result.password;
	}	
}
/*
var style = $('#product_detail').attr('style');
$('.product').on("click", function(){
	if ($(this).attr('id')=='bg') {
		$('#product_detail').attr('style',style);
		$(this).removeAttr('id').css('display','table-cell');
		$('#pageHeader, #pageAside, #pageDownNav').show();
		$('#product_detail').fadeIn();
	} else {
		$('#product_detail').attr('style','');
		$('#product_detail').css('display','block');
		$('#pageHeader, #pageAside, #pageDownNav').hide();
		$(this).attr('id','bg');
	}
});
*/
/* Products page END */


//Indicate if product from the left list is selected:
var selectedProduct = false;

var getParam = getParams();

function getProductsList(){
	//Hide Reports button:
	selectedProduct = false;
    cordova.exec(fireProductsList, execFailed, 'Product', 'getProductList', [0, 100]);

    function fireProductsList(param) {
		function record(product_name, product_id){
			var productRecord = "";

			//set active product
			if(product_id==getParam.matnr && gallery_inDetails_mode == 1){
				$('#categories').removeClass();
				$('#all_products').addClass('active');
				$('#listed_products li.active, #listed_categories li.active').removeClass();
				$('.back_button').hide();
				$('#listed_categories').hide(function(){
					$('#listed_products').show();
					$('#search_product').show();
				});
				productRecord += '<li class="active"><div class="arrow group"><div class="cart"><div class="products_count">cart_width</div><div class="cart_icon"></div></div><span class="name">';
			}
			else
			{
				productRecord += '<li><div class="arrow group"><div class="cart"><div class="products_count">cart_width</div><div class="cart_icon"></div></div><span class="name">';
			}
			productRecord += product_name;
			productRecord += '</span><span class="number" data-productid="' + product_id + '">'
			productRecord += product_id.replace(/^0+/, '');
			productRecord += '</span></div></li>';
		
			return productRecord;
		};
		
	
		var result = eval('(' + param + ')');
		var result_html = "";

		for(i = 0; i < result.length; i++){
			var product_name = result[i]["MAKTX"]+'<br>'+result[i]["MAKTX2"];
			var product_id = result[i]["MATNR"];
			var image_link = result[i]["IMAGE"];

			result_html += record(product_name, product_id);
			
		}
		$("#pageAside ul#listed_products").html(result_html);
		
		//if we have active products which is set from home page.
		/*if(getParam.matnr && gallery_inDetails_mode == 1){
			getProductDetails(getParam.matnr);
			//$(".swiper-wrapper").hide();
			var activeArrow = '<span class="active_arrow"></span>';
			$("#pageNav ul li:eq(2)").append(activeArrow);
			setTimeout(function() {
				$("#pageAside").scrollTo("#listed_products .active", 300);

				$('#pageDownNav li').removeClass('active');
				$('#pageDownNav li#general').addClass('active');

			}, 500);

			gallery_inDetails_mode = 0;
		}*/
		
		var cart = $('.cart').width();
		var holder_width = $('.arrow.group').width();
		var width, saved_distance;
		var count = 0;
		$("#pageAside ul#listed_products").off("click", "li");
		$("#pageAside ul#listed_products").on("click", "li", function(event, target) {
			$("#pageAside li, #pageDownNav li").removeClass("active");
			$(this).addClass("active");
			$("#pageDownNav #general").addClass("active");

			//$("#pageContent").fadeIn();
			var productId = $(this).find(".number").data("productid");
            getProductDetails(productId);
		});
		$('#loadingBar').hide();
		$('#categories').removeClass('active');
		$('#all_products').addClass('active');
		
		$('#goBack').hide();
		$("ul#listed_categories").addClass("noBack");

		$('#search_product').show();
		$('#listed_categories').hide();
		$('#listed_products').show();
		$("#pageAside ul#listed_products li:eq(0)").click();
		//Main Swiper - END

		//Scroll to the fist record:
		$("#pageAside").scrollTo("#pageAside .active", 300);
	}
}


//Search Product:
function loadMoreProductsList(){
	var currentNumber = parseInt($("#pageAside ul#listed_products li").length);
	cordova.exec(fireGet100MoreProducts, execFailed, 'Product', 'getProductList', [currentNumber, currentNumber + 100]);

	function fireGet100MoreProducts(param){
		function record(product_name, product_id){
			var productRecord = "";
			productRecord += '<li><div class="arrow group"><div class="cart"><div class="products_count">cart_width</div><div class="cart_icon"></div></div><span class="name">';
			productRecord += product_name;
			productRecord += '</span><span class="number" data-productid="' + product_id + '">'
			productRecord += product_id.replace(/^0+/, '');
			productRecord += '</span></div></li>';
		
			return productRecord;
		};
	
		var result = eval('(' + param + ')');
		var result_html = "";
		for(i = 0; i < result.length; i++){
			var product_name = result[i]["MAKTX"]+'<br>'+result[i]["MAKTX2"];
			var product_id = result[i]["MATNR"];

			result_html += record(product_name, product_id);
		}

		$("#pageAside ul#listed_products").append(result_html);

		//Main Swiper - START
		var cart = $('.cart').width();
		var holder_width = $('.arrow.group').width();
		var width, saved_distance;
		var count = 0;
		$("#pageAside ul#listed_products").off("click", "li");
		$("#pageAside ul#listed_products").on("click", "li", function(event, target) {
			$("#pageAside li, #pageDownNav li").removeClass("active");
			$(this).addClass("active");
			$("#pageDownNav #general").addClass("active");

			//$("#pageContent").fadeIn();
			var productId = $(this).find(".number").data("productid");
            getProductDetails(productId);
		});
		//Main Swiper - END

		if(result.length < 100){
			//Turn off scroll event:
			$("#pageAside").off("scroll");
		}

	}
}



//Search Product:
function searchProduct(searchTerm){
	//Hide Reports Button:
	selectedProduct = false;

	cordova.exec(fireSearchProduct, execFailed, 'Product', 'searchProduct', [searchTerm]);

	function fireSearchProduct(param){
		var contSearch = '<li style="padding:25px 0 0 20px;" id="conts"><span class="number" style="top:6px; height:40px !important; position: relative;">'+lang.CSOS+'</span></div></li>';

		function record(product_name, product_id){
			var productRecord = "";
			productRecord += '<li><div class="arrow group"><div class="cart"><div class="products_count">cart_width</div><div class="cart_icon"></div></div><span class="name">';
			productRecord += product_name;
			productRecord += '</span><span class="number" data-productid="' + product_id + '">'
			productRecord += product_id.replace(/^0+/, '');
			productRecord += '</span></div></li>';
		
			return productRecord;
		};
	
		var result = eval('(' + param + ')');
		var result_html = "";
		for(i = 0; i < result.length; i++){
			var product_name = result[i]["MAKTX"]+'<br>'+result[i]["MAKTX2"];
			var product_id = result[i]["MATNR"];

			result_html += record(product_name, product_id);
		}
		result_html += contSearch;
		$("#pageAside ul#listed_products").html(result_html);
		//Main Swiper - START
		var cart = $('.cart').width();
		var holder_width = $('.arrow.group').width();
		var width, saved_distance;
		var count = 0;

		$("#pageAside ul#listed_products").off("click", "li");
		$("#pageAside ul#listed_products").on("click", "li", function(event, target) {
			if($(this).attr('id') == "conts"){
				search_on_server($("#search_product input").val(), current_username, current_password);
			}
			else{
				$("#pageAside li, #pageDownNav li").removeClass("active");
				$(this).addClass("active");
				$("#pageDownNav #general").addClass("active");

				//$("#pageContent").fadeIn();
				var productId = $(this).find(".number").data("productid");
	            getProductDetails(productId);
			}
		});
		//Main Swiper - END


		//Check if there are 0 results:
		if ( result.length == 0 ){
			//Only if the gallery is hidden:
			if ( $("#products_holder").is(":hidden") ) {
				$("#pageContent").fadeOut();
			}
		} else {
			$("#pageContent").fadeIn();

			//Simulate manual click on the first customer after search if another customer details or reports are visible:
			if ( $("#products_holder").is(":hidden") ) {
				$("#pageAside li, #pageDownNav li").removeClass("active");
				$("#listed_products li:first-child").addClass("active");
				$("#pageDownNav #general").addClass("active");
				var productId = $("#listed_products li:first-child").find(".number").data("productid");
	            getProductDetails(productId);
			}
		}

		//Scroll to the fist record:
		$("#pageAside").scrollTo("#pageAside .active", 300);
	}
}


//Product Categories:
function getCategories(){
	//Hide Reports Button:
	selectedProduct = false;

	//Set General Button Indicator:
	generalBut_indicator = 0;
	$("#general").hide();

	$('#goBack').hide();
	$("ul#listed_categories").addClass("noBack");

	cordova.exec(fireProductCategories, execFailed, 'Product', 'getCategories', ['']);
	function fireProductCategories(param){
		//alert(param);
		var categories_result = eval('(' + param + ')');

		function record(product_name, product_id, has_children, product_name_real, has_products){
			var productRecord = "";
			var indicator = '';
			if ( has_products > 0){
				indicator += '<span class="listViewFlag2"></span>';
			}
			else if( has_products == -1){
				indicator += '<span class="listViewFlag3"></span>';
			}
			productRecord += '<li><div class="arrow group"><div class="cart"><div class="products_count">cart_width</div><div class="cart_icon"></div></div><span class="name" data-realname="' + product_name_real + '">';
			productRecord += indicator + product_name;
			productRecord += '</span><span class="number" data-productid="' + product_id + '" data-haschildren="' + has_children + '">';
			productRecord += product_id.replace(/^0+/, '');
			productRecord += '</span></div></li>';
			return productRecord;
		};
	
		//var result = eval('(' + param + ')');
		var result_html = "";
		for(i = 0; i < categories_result.length; i++){
			if(i==0 && categories_result[i]["CATEGORYDESCRIPTION"] == "Root") continue;
			var product_name = categories_result[i]["CATEGORYDESCRIPTION"];
			var product_id = categories_result[i]["CATEGORYID"];
            var has_children = categories_result[i]["HASCHILDREN"];
			var product_name_real = categories_result[i]["CATALOGUENAME"];
			var has_products = categories_result[i]["HASPRODUCT"];
            result_html += record(product_name, product_id, has_children, product_name_real, has_products);
		}
		$("#pageAside ul#listed_categories").html(result_html);

		//Remove Loading Animation:
		$('#loadingBar').fadeOut(500);

		//$('#all_products').click();
		
		getProductDetails($("#pageAside ul#listed_products li:eq(0)").find('span.number').data('productid'));
		$('#general').show();
		$('#general').addClass('active');
		//$('#but_reports').show();
		//TODO:
		setTimeout(function(){
			$("#pageAside ul#listed_products li:eq(0)").addClass('active');
		},400);
	}
}

//Get Product SubCategories:
function getSubCategories(category_name, category_id){
	//Hide Reports Button:
	selectedProduct = false;

	$('#goBack').show();
	$("ul#listed_categories").removeClass("noBack");

	cordova.exec(fireProductSubCategories, execFailed, 'Product', 'getCategories', [category_name, category_id]);
	function fireProductSubCategories(param){
		var sub_categories_result = eval('(' + param + ')');

		function record(product_name, product_id, has_children, product_name_real, has_products){
			var productRecord = "";
			var indicator = '';
			if ( has_products > 0){
				indicator += '<span class="listViewFlag2"></span>';
			}
			else if( has_products == -1){
				indicator += '<span class="listViewFlag3"></span>';
			}
			productRecord += '<li><div class="arrow group"><div class="cart"><div class="products_count">cart_width</div><div class="cart_icon"></div></div><span class="name" data-realname="' + product_name_real + '">';
			productRecord += indicator + product_name;
			productRecord += '</span><span class="number" data-productid="' + product_id + '" data-haschildren="' + has_children + '">';
			productRecord += product_id.replace(/^0+/, '');
			productRecord += '</span></div></li>';
			//alert(product_name_real);
			return productRecord;
		};
	
		//var result = eval('(' + param + ')');
		var result_html = "";
		for(i = 0; i < sub_categories_result.length; i++){
			if(i==0 && sub_categories_result[i]["CATEGORYDESCRIPTION"] == "Root") continue;
			var product_name = sub_categories_result[i]["CATEGORYDESCRIPTION"];
			var product_id = sub_categories_result[i]["CATEGORYID"];
			var has_children = sub_categories_result[i]["HASCHILDREN"];
			var product_name_real = sub_categories_result[i]["CATALOGUENAME"];
			var has_products = sub_categories_result[i]["HASPRODUCT"];
			result_html += record(product_name, product_id, has_children, product_name_real, has_products);
		}
		$("#pageAside ul#listed_categories").html(result_html);
		
		$("#pageAside").scrollTo("#pageAside #goBack", 300);	
		// $("#goBack").data("parentCategoryName", sub_categories_result[0]["CATALOGUENAME"]);
		// $("#goBack").data("parentCategoryId", sub_categories_result[0]["PARENTCATEGORYID"]);
	}
}

function getProductListByCategory(category_id){
	//Set General Button Indicator:
	generalBut_indicator = 0;
	$("#general").hide();

	//Hide Reports Button:
	selectedProduct = false;

	$('#goBack').show();
	$("ul#listed_categories").removeClass("noBack");

	cordova.exec(fireProductListByCategory, execFailed, 'Product', 'getProductListByCategory', [category_id]);
	function fireProductListByCategory(param){
		var sub_categories_result = eval('(' + param + ')');
		
		function record(product_name, product_id, has_children, indica){
			var indicator = '<span class="listViewFlag"></span>';
			if (indica != "50"){
				var indicator = "";
			}
			var productRecord = "";
			productRecord += '<li><div class="arrow group"><div class="cart"><div class="products_count">cart_width</div><div class="cart_icon"></div></div><span class="name">';
			productRecord += indicator + product_name;
			productRecord += '</span><span class="number" data-productid="' + product_id + '" data-haschildren="' + has_children + '">';
			productRecord += product_id.replace(/^0+/, '');
			productRecord += '</span></div></li>';
		
			return productRecord;
		};
	
		//var result = eval('(' + param + ')');
		var result_html = "";
		for(i = 0; i < sub_categories_result.length; i++){
			var product_name = sub_categories_result[i]["MAKTX"]+'<br>'+sub_categories_result[i]["MAKTX2"];
			var product_id = sub_categories_result[i]["MATNR"];
			var indicator = sub_categories_result[i]["VMSTA"];
			result_html += record(product_name, product_id, 0, indicator);
		}
		$("#pageAside ul#listed_categories").html(result_html);

		$("#pageAside ul#listed_categories").off("click", "li");
		$("#pageAside ul#listed_categories").on("click", "li", function(){
			$("#pageAside ul#listed_categories li").removeClass("active");
			$(this).addClass("active");
			$("#pageDownNav li").removeClass("active");
			$("#pageDownNav #general").addClass("active");

			var productId = $(this).find(".number").data("productid");
			getProductDetails(productId);
			return false;
		});
		$("#pageAside").scrollTo("#pageAside #goBack", 300);
		// $("#goBack").data("parentCategoryName", sub_categories_result[0]["CATALOGUENAME"]);
		// $("#goBack").data("parentCategoryId", sub_categories_result[0]["PARENTCATEGORYID"]);
	}
}

function getParentCategory(){
	//Hide Reports Button:
	selectedProduct = false;

	cordova.exec(fireParentCategory, execFailed, 'Product', 'getParentCategory', ['']);
	function fireParentCategory(param){
		var categories_result = eval('(' + param + ')');

		//Detect if main category is reached!
		if(categories_result[0]["PARENTCATEGORYID"] == '0000000000'){
			$('#goBack').hide();
			$("ul#listed_categories").addClass("noBack");
		} else {
			$("ul#listed_categories").removeClass("noBack");
		}

		function record(product_name, product_id, has_children, product_name_real, has_products){
			var productRecord = "";
			var indicator = '';
			if ( has_products > 0){
				indicator += '<span class="listViewFlag2"></span>';
			}
			else if( has_products == -1){
				indicator += '<span class="listViewFlag3"></span>';
			}
			productRecord += '<li><div class="arrow group"><div class="cart"><div class="products_count">cart_width</div><div class="cart_icon"></div></div><span class="name" data-realname="' + product_name_real + '">';
			productRecord += indicator+product_name;
			productRecord += '</span><span class="number" data-productid="' + product_id + '" data-haschildren="' + has_children + '">';
			productRecord += product_id.replace(/^0+/, '');
			productRecord += '</span></div></li>';
			return productRecord;
		};
	
		//var result = eval('(' + param + ')');
		var result_html = "";
		for(i = 0; i < categories_result.length; i++){
			if(i==0 && categories_result[i]["CATEGORYDESCRIPTION"] == "Root") continue;
			var product_name = categories_result[i]["CATEGORYDESCRIPTION"];
			var product_id = categories_result[i]["CATEGORYID"];
            var has_children = categories_result[i]["HASCHILDREN"];
			var product_name_real = categories_result[i]["CATALOGUENAME"];
			var has_products = categories_result[i]["HASPRODUCT"];
			result_html += record(product_name, product_id, has_children, product_name_real, has_products);
		}
		$("#pageAside ul#listed_categories").html(result_html);
		$("#pageAside").scrollTo("#pageAside #goBack", 300);
	}
}


//Products SubCategories
/*
function getProductSubCategories(id_category){
	function fireProductsSubCategories(subCategory_result){
		function record(product_name, product_id){
			var productRecord = "";
			productRecord += '<li><div class="arrow group"><div class="cart"><div class="products_count">cart_width</div><div class="cart_icon"></div></div><span class="name">';
			productRecord += product_name;
			productRecord += '</span><span class="number">'
			productRecord += product_id;
			productRecord += '</span></div></li>';
		
			return productRecord;
		};
	
		//var result = eval('(' + param + ')');
		var result_html = "";
		for(i = 0; i < subCategory_result.length; i++){
			var product_name = subCategory_result[i]["CATALOGUENAME"];
			var product_id = subCategory_result[i]["CATEGORYID"];
			result_html += record(product_name, product_id);
		}
		alert(result_html);
		$("#pageAside ul#listed_categories").html(result_html);
	}

	function getSubCategories(obj_cat){

	}

	for(i=0; i < categories_result.length; i++) {

		if (categories_result[i]["CATEGORYID"] == id_category && categories_result[i]["subCategories"].length > 0) {
			fireProductsSubCategories(categories_result[i]["subCategories"]);
		} else if(categories_result[i]["subCategories"].length > 0) {
			for(k=0; k < categories_result[i]["subCategories"].length; k++) {
				if(categories_result[i]["CATEGORYID"][k]["CATEGORYID"] == id_category && categories_result[i]["CATEGORYID"][k]["subCategories"].length > 0) {
					fireProductsSubCategories(categories_result[i]["subCategories"]);
				} else {
					//List All Products
				}
			}
		} else {
			//List All Products
		}

	}
}
*/

//Product Details:
function getProductDetails(productId, onlineCheck){
	//Show Content:
	$("#pageContent").fadeIn();

	//Set General Button Indicator:
	generalBut_indicator = 1;
	//Show General Button:
	$("#general").show();
	//Show Reports Button:
	//$("#but_reports").show();
	selectedProduct = true;
	if(onlineCheck == 1){
		getProductDetailsOnline(productId, current_username, current_password, successful_get, get_failed);
		function successful_get(data){
			fireProductDetails(data, 1);	
		}

		function get_failed(data){
			execFailed(data);	
		}
	}
	else{
		cordova.exec(fireProductDetails, execFailed, 'Product', 'getProductDetails', [productId]);	
	}
	
	
	function fireProductDetails(param, online1){
		var result = eval('(' + param + ')');
		var visiblePage = $(".contentWrapper:visible");
		visiblePage.css('visibility', 'hidden');
		$('#product_detail').scrollTop(0);
		visiblePage.fadeOut(function(){
			visiblePage.css('visibility', 'visible');
			var product = $("#product_detail_holder");
			product.find("h2").html(result["MATL_DESC"]);
			if(result["IMAGE"] != ""){
				product.find("#head_information").find('.product').html('<img src="'+result["IMAGE"]+'" />');
			}
			else{
				product.find("#head_information").find('.product').html('<img src="img/product_image.png" />');
			}
			product.find("#product_maktx1").html(result["MAKTX"]);
			product.find("#product_maktx2").html(result["MAKTX2"]);
			product.find("#product_count").html(result["MATNR"].replace(/^0+/, ''));
			$("#stock_holder").data('matnr', result["MATNR"]);
			/*cordova.exec(fireUoM, execFailed, 'Customizing', 'getMaterialGroupList', ['']);

			function fireUoM(param2){
				var result_uom = eval('(' + param2 + ')');
				var one = "";
				var two = "";
				for(j=0;j<result_uom.length;j++){
					alert(result["BASE_UOM"] +'..'+ result_uom[j]['MSEHI']);
					if(result["BASE_UOM"] == result_uom[j]['MSEHI']){
						one = result_uom[j]['MSEHT'];
					}
				}
				
			}*/
			if(online1 == 1){
				var salesRels = result['SALES_REL'];
				var cItems = result['COND_ITEMS'];
				var uoms = result['UNITS'];
				var attaches = result['DOCUMENTS'];
			}
			else{
				var salesRels = result['SalesRels'];
				var cItems = result['ConditionItems'];
				var uoms = result['Units'];
				var attaches = result['Documents'];
			}

			product.find("#buom").html(result["BASE_UOM"]);
			product.find("#sunit").html(salesRels[0]["VRKME"]);
			product.find("#gweight").html(result["GROSS_WT"].formatMoney()+ " " +result["UNIT_OF_WT"]);
			product.find("#stock").html(salesRels[0]["STOCK"].formatMoney()+ " " +salesRels[0]["ALT_UNIT"]);
			$('#stock').css('color','#596175');
			if(online1 == 1){
				product.find("#gprice").html(cItems[0]["KBETR"]+ " &#128; per "+cItems[0]["KPEIN"].formatMoney() + " " +cItems[0]["KMEIN"]);
			}
			else
			{
				product.find("#gprice").html(parseFloat(cItems[0]["KBETR"]).formatMoney(2)+ " &#128; per "+cItems[0]["KPEIN"].formatMoney() + " " +cItems[0]["KMEIN"]);

			}
			
			product.find("#plant").html(salesRels[0]["WERKS"]);

			if( typeof cItems != 'undefined'){
				if( typeof cItems[0] != 'undefined'){
					product.find("#cat5").html(cItems[0]['KPEIN'].formatMoney());
					product.find("#cat6").html(cItems[0]['KBETR'] + " " + cItems[0]['KONWA']);	
				} else {
					product.find("#cat5").text("0");
					product.find("#cat6").text("0");
				}
			} else {
				product.find("#cat5").text("0");
				product.find("#cat6").text("0");
			}
			
			var html_string = "";
			for(ii=0;ii<uoms.length;ii++){
				//OLD Order: FACTOR ALT_UNIT GROSS_WT UNIT_OF_WT
				//html_string += '<tr><td><div style="width: 84%;" class="item">'+uoms[ii]['FACTOR'].formatMoney()+' '+uoms[ii]['ALT_UNIT']+' '+uoms[ii]['GROSS_WT'].formatMoney()+' '+uoms[ii]['UNIT_OF_WT']+'</div></td>';

				//NEW Order: 1 ALT_UNIT = FACTOR BASE_UOM GROSS_WT UNIT_OF_WT
				html_string += '<tr><td style="width:100%"><div style="width: 84%;" class="item"><div style="min-width:25%; width: auto;">';
				html_string += '1 '+ uoms[ii]['ALT_UNIT'] + ' = ';
				html_string +=  uoms[ii]['FACTOR'].formatMoney() + ' ';
				html_string += result["BASE_UOM"] + ' ';
				html_string += "</div>";
				html_string += uoms[ii]['GROSS_WT'].formatMoney() + ' ';
				html_string += uoms[ii]['UNIT_OF_WT'] + '</div></td></tr>';
			}
			$('#uoms_holder').html(html_string);

			var html_string2 = "";
			for(iii=0;iii<attaches.length;iii++){
				html_string2 += '<tr><td style="width:100%"><div style="width: 84%;" class="item"><a href="'+attaches[iii]['LINK']+'">'+attaches[iii]['DESCRIPTION']+'</a></div></td></tr>';
			}
			$('#attaches_holder').html(html_string2);
			$('#attaches_holder').off('click');
			$('#attaches_holder').on('click', 'tr',function(){
				// window.location.href = $(this).find('a').attr('href');
				window.open($(this).find('a').attr('href'), "_system");
			});
			
			$("#product_detail_holder").fadeIn(function(){
				$('#stock').prev().html('<span style="float:left">'+lang.product_General[3]+'</span><span class="inv_loader"></span>');
				setTimeout(function(){
					inventory_look(result["MATNR"], current_username, current_password);
				}, 500);
				
			});
		});
	}

}

function wopen(urll){
	var childWindow = window.open( urll, "_blank", "location=no, menubar=no, height=290, top=10" );
	setTimeout(function(){
        var arr = [];
		arr.code = 'var css = document.createElement("style"); css.type = "text/css"; css.innerHTML = "body { margin: 20px 0 0 0 !important }"; document.body.appendChild(css);';
		childWindow.executeScript(arr);
    }, 800);
}

function inventory_look(matnr, user, pass){
	if(!doIhaveInternet()){
		var errorDict = {};
        errorDict[lang.internet_err] = "error";
        customAlert(errorDict);
		return;
	}
	var data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions"><soapenv:Header/><soapenv:Body><urn:_-MSCMOBIL_-Z_INVENTORY_LOOKUP><IM_BNAME>'+user+'</IM_BNAME><IM_MATNR>'+matnr+'</IM_MATNR></urn:_-MSCMOBIL_-Z_INVENTORY_LOOKUP></soapenv:Body></soapenv:Envelope>';
		$.soap({
			url: baseOnlineURL+"/inventory_lookup?sap-client="+sapClient,
			method: "_-MSCMOBIL_-Z_INVENTORY_LOOKUP",
			soap12: false,
			data: data,
			wss: "",
			HTTPHeaders: {
				Authorization: 'Basic ' + btoa(user+':'+pass)
			},
			namespaceQualifier:  "myns",
			namespaceURL: "urn:_-MSCMOBIL_-Z_INVENTORY_LOOKUP",
			noPrefix: false,
			elementName: "",
			enableLogging: false,
			success: function(SOAPResponse) {
				var resp = SOAPResponse.toString();
				var inv_lookup_res = eval('('+$(resp).find("EX_JSON_RESULT").text()+')');
				$('#stock').text(inv_lookup_res['STOCK']+' '+inv_lookup_res['MEINS']);
				$('#stock').css('color','green');
				$('#stock').prev().text(lang.product_General[3]);
			},
			error: function(SOAPResponse) {
				//alert(SOAPResponse.toString());
			}
		});
}

function search_on_server(srch, user, pass){
	if(!doIhaveInternet()){
		var errorDict = {};
        errorDict[lang.internet_err] = "error";
        customAlert(errorDict);
		return;
	}
	var data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions"><soapenv:Header/><soapenv:Body><urn:_-MSCMOBIL_-Z_PRODUCT_LIST><IM_BNAME>'+user+'</IM_BNAME><IM_TERM>'+srch+'</IM_TERM></urn:_-MSCMOBIL_-Z_PRODUCT_LIST></soapenv:Body></soapenv:Envelope>';
		$.soap({
			url: baseOnlineURL+"/product_list?sap-client="+sapClient,
			method: "_-MSCMOBIL_-Z_PRODUCT_LIST",
			soap12: false,
			data: data,
			wss: "",
			HTTPHeaders: {
				Authorization: 'Basic ' + btoa(user+':'+pass)
			},
			namespaceQualifier:  "myns",
			namespaceURL: "urn:_-MSCMOBIL_-Z_PRODUCT_LIST",
			noPrefix: false,
			elementName: "",
			enableLogging: false,
			success: function(SOAPResponse) {
				
				function record_web(product_name, product_id){
					var productRecord = "";
					productRecord += '<li data-online="1"><div class="arrow group"><div class="cart"><div class="products_count">cart_width</div><div class="cart_icon"></div></div><span class="name">';
					productRecord += product_name;
					productRecord += '</span><span class="number" data-productid="' + product_id + '">'
					productRecord += product_id.replace(/^0+/, '');
					productRecord += '</span></div></li>';
				
					return productRecord;
				};

				var resp = SOAPResponse.toString();
				var srch_res = eval('('+'['+$(resp).find("EX_JSON_RESULT").text()+']'+')');
				var result_html = "";
				for(i = 0; i < srch_res.length; i++){
					var product_name = srch_res[i]["MAKTX"]+'<br>'+srch_res[i]["MAKTX2"];
					var product_id = srch_res[i]["MATNR"];

					result_html += record_web(product_name, product_id);
				}
				$("#pageAside ul#listed_products").find('#conts').remove();
				$("#pageAside ul#listed_products").html($("#pageAside ul#listed_products").html()+result_html);

				$("#pageAside ul#listed_products").off("click", "li");
				$("#pageAside ul#listed_products").on("click", "li", function(event, target) {
					$('#product_detail').scrollTop(0)
					$("#pageAside li, #pageDownNav li").removeClass("active");
					$(this).addClass("active");
					$("#pageDownNav #general").addClass("active");

					//$("#pageContent").fadeIn();
					var productId = $(this).find(".number").data("productid");
		            getProductDetails(productId, 1);
				});
			},
			error: function(SOAPResponse) {
				//alert(SOAPResponse.toString());
			}
		});
}

function getProductDetailsOnline(matnr, user, pass, success_get, get_failed){
	if(!doIhaveInternet()){
		var errorDict = {};
        errorDict[lang.internet_err] = "error";
        customAlert(errorDict);
		return;
	}
	if(currentLang=="de"){
		var data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions"><soapenv:Header/><soapenv:Body><urn:_-MSCMOBIL_-Z_PRODUCT_DETAIL><IM_BNAME>'+user+'</IM_BNAME><IM_SPRAS>D</IM_SPRAS><IM_MATNR>'+matnr+'</IM_MATNR></urn:_-MSCMOBIL_-Z_PRODUCT_DETAIL></soapenv:Body></soapenv:Envelope>';
	}
	else{
		var data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions"><soapenv:Header/><soapenv:Body><urn:_-MSCMOBIL_-Z_PRODUCT_DETAIL><IM_BNAME>'+user+'</IM_BNAME><IM_SPRAS>E</IM_SPRAS><IM_MATNR>'+matnr+'</IM_MATNR></urn:_-MSCMOBIL_-Z_PRODUCT_DETAIL></soapenv:Body></soapenv:Envelope>';
	}
	$.soap({
		url: baseOnlineURL+"/product_detail?sap-client="+sapClient,
		method: "_-MSCMOBIL_-Z_PRODUCT_DETAIL",
		soap12: false,
		data: data,
		wss: "",
		HTTPHeaders: {
			Authorization: 'Basic ' + btoa(user+':'+pass)
		},
		namespaceQualifier:  "myns",
		namespaceURL: "urn:_-MSCMOBIL_-Z_PRODUCT_DETAIL",
		noPrefix: false,
		elementName: "",
		enableLogging: false,
		success: function(SOAPResponse) {
			var resp = SOAPResponse.toString();
			//var srch_res = eval('('+'['+$(resp).find("EX_JSON_RESULT").text()+']'+')');
			success_get($(resp).find("EX_JSON_RESULT").text());
		},
		error: function(SOAPResponse) {
			get_failed(SOAPResponse.toString());
		}
	});
}

function getAdditionalStockInfoOnline(matnr, user, pass, success_get, get_failed){
	if(!doIhaveInternet()){
		var errorDict = {};
        errorDict[lang.internet_err] = "error";
        customAlert(errorDict);
		return;
	}
	
	var data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions"><soapenv:Header/><soapenv:Body><urn:_-MSCMOBIL_-Z_PLANT_INVENTORY><IM_BNAME>'+user+'</IM_BNAME><IM_MATNR>'+matnr+'</IM_MATNR></urn:_-MSCMOBIL_-Z_PLANT_INVENTORY></soapenv:Body></soapenv:Envelope>';

	$.soap({
		url: baseOnlineURL+"/plant_inventory?sap-client="+sapClient,
		method: "_-MSCMOBIL_-Z_PLANT_INVENTORY",
		soap12: false,
		data: data,
		wss: "",
		HTTPHeaders: {
			Authorization: 'Basic ' + btoa(user+':'+pass)
		},
		namespaceQualifier:  "myns",
		namespaceURL: "urn:_-MSCMOBIL_-Z_PLANT_INVENTORY",
		noPrefix: false,
		elementName: "",
		enableLogging: false,
		success: function(SOAPResponse) {
			var resp = SOAPResponse.toString();
			//var srch_res = eval('('+'['+$(resp).find("EX_JSON_RESULT").text()+']'+')');

			success_get($(resp).find("EX_JSON_RESULT").text());
		},
		error: function(SOAPResponse) {
			get_failed(SOAPResponse.toString());
		}
	});
}

$(function(){

	document.addEventListener("deviceready", getUserCredentials, false);
	document.addEventListener("deviceready", getProductsList, false);
	//document.addEventListener("deviceready", getCategories, false);

	//Styles Fix:
	$("#pageAside").css({
		"height": $('body').height() - ($("#pageHeader").height()) - $("#aside_nav").height()-1
	});
	$('#productDetailsBut').on('click',function(){
		var currentMatnr = $('#pageAside .active .number').data('productid');
		var data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions"><soapenv:Header/><soapenv:Body><urn:_-MSCMOBIL_-Z_GET_PRODUCT_DETAIL><IM_BNAME>'+current_username+'</IM_BNAME><IM_MATNR>'+currentMatnr+'</IM_MATNR></urn:_-MSCMOBIL_-Z_GET_PRODUCT_DETAIL></soapenv:Body></soapenv:Envelope>';	
		$.soap({
			url: baseOnlineURL+"/get_product_detail?sap-client="+sapClient,
			method: "_-MSCMOBIL_-Z_GET_PRODUCT_DETAIL",
			soap12: false,
			data: data,
			wss: "",
			HTTPHeaders: {
				Authorization: 'Basic ' + btoa(current_username+':'+current_password)
			},
			namespaceQualifier:  "myns",
			namespaceURL: "urn:_-MSCMOBIL_-Z_GET_PRODUCT_DETAIL",
			noPrefix: false,
			elementName: "",
			enableLogging: false,
			success: function(SOAPResponse) {
				var resp = SOAPResponse.toString();
				var result = eval('(' + $(resp).find("EX_JSON_RESULT").text() + ')');
				console.log(result);
				//var result = $(resp).find("EX_JSON_RESULT").text();
				$('#headHolder').html('');
				$('#headHolder').append("<tr><td class='prDetsTD'><div>"+lang.ProductDetailsTable[0]+"</div><div>"+result['MATKL']+"</div></td><td class='prDetsTD'><div>"+lang.ProductDetailsTable[1]+"</div><div>"+result['MFRGR']+"</div></td><td><div>"+lang.ProductDetailsTable[2]+"</div><div>"+result['MVGR1']+"</div></td></tr>");
				$('#headHolder').append("<tr><td class='prDetsTD'><div>"+lang.ProductDetailsTable[3]+"</div><div>"+result['LP_KBETR']+" "+result['LP_WAERS']+"</div></td><td class='prDetsTD'><div>"+lang.ProductDetailsTable[8]+"</div><div>"+result['SP_KBETR']+" "+result['SP_WAERS']+"</div></td><td><div>"+lang.ProductDetailsTable[14]+" "+lang.ProductDetailsTable[15]+"</div><div>"+result['SP_KPEIN']+" "+result['SP_KMEIN']+"</div></td></tr>");
				$('#headHolder').append("<tr><td class='prDetsTD'><div>"+lang.ProductDetailsTable[4]+"</div><div>"+result['LS_KBETR']+"</div></td><td class='prDetsTD'><div>"+lang.ProductDetailsTable[9]+"</div><div>"+result['SS_KBETR']+"</div></td><td></td></tr>");
				$('#headHolder').append("<tr><td class='prDetsTD'><div>"+lang.ProductDetailsTable[5]+"</div><div>"+result['MSTAE']+" "+formatDate(result['MSDAE'])+"</div></td><td class='prDetsTD'><div>"+lang.ProductDetailsTable[10]+"</div><div>"+result['MSTAV']+" "+formatDate(result['MSTDV'])+"</div></td><td></td></tr>");
				$('#headHolder').append("<tr><td class='prDetsTD'><div>"+lang.ProductDetailsTable[6]+"</div><div>"+result['MMSTA']+" "+formatDate(result['MMSTD'])+"</div></td><td class='prDetsTD'><div>"+lang.ProductDetailsTable[11]+"</div><div>"+result['VMSTA']+" "+formatDate(result['VMSTD'])+"</div></td><td></td></tr>");
				
				$('#bodyHolder').html('');
				if(result['LT_BEST'].length > 0){
					$('.PDTle').show();
					$('.PDTle thead').html("<tr><th>"+lang.ProductDetailsTable[7]+"</th><th>"+lang.ProductDetailsTable[12]+"</th><th>"+lang.ProductDetailsTable[13]+"</th><th>"+lang.ProductDetailsTable[14]+" "+lang.ProductDetailsTable[15]+"</th><th>"+lang.ProductDetailsTable[16]+" - "+lang.ProductDetailsTable[14]+" "+lang.ProductDetailsTable[15]+"</th><th>"+lang.ProductDetailsTable[17]+" - "+lang.ProductDetailsTable[14]+" "+lang.ProductDetailsTable[15]+"</th></tr>");
					for(var ij=0;ij<result['LT_BEST'].length;ij++){
						var LBRecord = result['LT_BEST'][ij];
						var bodyHTML = "<tr>";
						bodyHTML += "<td>"+LBRecord['WERKS']+" "+LBRecord['NAME1']+"</td>";
						bodyHTML += "<td>"+LBRecord['LABST']+" "+LBRecord['MEINS']+"</td>";
						bodyHTML += "<td>"+LBRecord['VERPR']+" "+LBRecord['LKONWA']+"</td>";
						bodyHTML += "<td>"+LBRecord['PEINH']+"</td>";
						bodyHTML += "<td>"+LBRecord['LKBETR']+" "+LBRecord['LKONWA']+" - "+LBRecord['LKPEIN']+" "+LBRecord['LKMEIN']+"</td>";
						bodyHTML += "<td>"+LBRecord['VKBETR']+" "+LBRecord['VKONWA']+" - "+LBRecord['VKPEIN']+" "+LBRecord['VKMEIN']+"</td>";

						$('#bodyHolder').append(bodyHTML+"</tr>");
					}
				}
				else{
					$('.PDTle').hide();
				}
				$('#dialog_attachments').dialog({
					dialogClass: "with-close-productPopup4",
					title: lang.productPopUp_title,
					show: 'fade',
					hide: "fade",
					width: 900,
					height: 610
				});

				$("#popUp_overlap").on("click", function(){
					$(this).fadeOut();
					$(".with-close-productPopup4 .ui-dialog-titlebar-close").click();
					$("#popUp_overlap").off("click");

					//Remove active:
					$('#pageAside ul li').removeClass("activeLongPress");
				}).fadeIn();

			    $("#dialog_attachments").dialog('option', 'title', lang.productPopUp_title);
			},
			error: function(SOAPResponse) {
				execFailed(SOAPResponse.toString());
			}
		});
	});
	$('#stock_holder').on('click',function(){
		//$('#loadingBar').show();
		getAdditionalStockInfoOnline($(this).data('matnr'), current_username, current_password, function(param){
			var result = eval('([' + param + '])');
			$( ".stock_plants_body" ).html("");
			for (var i = 0; i < result.length; i++) {
				$( ".stock_plants_body" ).append("<tr><td>"+result[i]["WERKS"]+"</td><td style=\"border-right: 1px solid #c2c5cd;\">"+result[i]["LABST"]+"</td><td>"+result[i]["MEINS"]+"</td></tr>");
			};
			//Fire Dialog:
			$('#dialog_plant_inv').dialog({
				dialogClass: "with-close-productPopup2",
				title: lang.productPopUp_title,
				show: 'fade',
				hide: "fade",
				width: 500
			});
			$("#popUp_overlap").fadeIn();

			//Wait untill 1.5 secounds and then - attach the hide event:
			setTimeout(function(){
				$("#popUp_overlap").on("click", function(){
					$(this).fadeOut();
					$(".with-close-productPopup2 .ui-dialog-titlebar-close").click();
					$("#popUp_overlap").off("click");

					//Remove active:
					$('#pageAside ul li').removeClass("activeLongPress");
				});
			}, 1500);
		    $("#dialog_plant_inv").dialog('option', 'title', lang.available_stock);
		    //$('#loadingBar').hide('fast');
		}, execFailed);
		
	});
	//aside_nav
	var categories_result;
	$('#aside_nav a').on("click", function(){
		$("#pageAside").scrollTop(0);
		
		$('#aside_nav a').removeAttr('class');
		$(this).addClass('active');
		
		switch($(this).attr('id')){
			case 'all_products':
				//Hide General Button:
				//$("#general").hide();
				//Remove Active:
				$("#pageAside").off("scroll");
				$("#pageAside").on("scroll", function(){
			    	if($("#pageAside ul#listed_products").is(":visible")){
			    		if( ($(this).scrollTop() + $(this).innerHeight()) == $(this).get(0).scrollHeight){
				    		loadMoreProductsList();
				    	}
			    	}
			    	
			    });
				$('#listed_products li, #listed_categories li').removeClass("active");

				$('.back_button').fadeOut();
				$('#listed_categories').fadeOut(function(){
					getProductsList();
					$('#listed_products').fadeIn();
					$('#search_product').fadeIn();
				});

				//Clear the search term:
				$("#search_product input").val('');

				//Add Active:
				//$("#aside_nav a").removeClass("active");
				//$("#all_products").addClass("active");
				return false;
				break;

			case 'categories':
				//if content is hidden:
				$("#pageContent").fadeIn();

				//Hide General Button:
				$("#general").hide();
				//Remove Active:
				$('#listed_products li, #listed_categories li').removeClass("active");

				$('#search_product').fadeOut();
				$('#listed_products').fadeOut(function(){
					//$('.back_button').fadeIn();
					$('#listed_categories').fadeIn();
				});
				
				$("#pageAside ul#listed_categories").off("click", "li");
				$("#pageAside ul#listed_categories").on("click", "li", function(){
					$("#pageAside ul#listed_categories li").removeClass("active");

					var category_id = $(this).find(".number").data("productid");
					var category_name = $(this).find(".name").data("realname");
					var has_children = $(this).find(".number").data("haschildren");
					if(has_children == 0){
						//alert("No more subcategories");
						getProductListByCategory(category_id);
					} else {
						//alert("SubCategories!");
						getSubCategories(category_name, category_id);
					}
				});
				
				getCategories();

				//Add Active:
				//$("#aside_nav a").removeClass("active");
				//$("#categories").addClass("active");
				return false;
				break;
		}
	});

	$("#goBack").on("click", function(){
		//You are in category:
		generalBut_indicator = 0;
		$("#general").hide();

		getParentCategory();

		$("#pageAside ul#listed_categories").off("click", "li");
		$("#pageAside ul#listed_categories").on("click", "li", function(){
			var category_id = $(this).find(".number").data("productid");
			var category_name = $(this).find(".name").data("realname");
			var has_children = $(this).find(".number").data("haschildren");
			if(has_children == 0){
				//alert("No more subcategories");
				getProductListByCategory(category_id);
			} else {
				//alert("SubCategories!");
				getSubCategories(category_name, category_id);
			}
		});
	});

	/*$("#pageAside ul#listed_categories").on("click", "li", function(){
		var category_id = $(this).find(".number").data("productid");
		var category_name = $(this).find(".name").data("realname");
		var has_children = $(this).find(".number").data("haschildren");
		if(has_children == 0){
			//alert("No more subcategories");
			getProductListByCategory(category_id);
		} else {
			//alert("SubCategories!");
			getSubCategories(category_name, category_id);
		}
	});*/

	//Show product detail information
	$('#general').on("click", function(){
		//If a product is selected show/hide reports button:
		/*if ( selectedProduct ) {
			$("#but_reports").show();
		} else {
			$("#but_reports").hide();
		}*/

		showSection("#product_detail_holder", ".contentWrapper:visible", this);
	});
	/*$("#but_reports").on("click", function(){
		showSection("#products_reports", ".contentWrapper:visible", this);
		getUserReportList();
	});
	/*$(".swiper-wrapper").on("click", ".product_holder", function(){
		$("#pageDownNav li").removeClass("active");
		$("#pageDownNav #general").addClass("active");
		var product_id = $(this).find(".product_title").data("productid");
		getParam.matnr = product_id;

		gallery_inDetails_mode = 1;
		$("#all_products").click();
		getProductDetails(product_id);
	});*/
	//Show products image on full size
	$('.product').on("click", function(){
		var image = $(this).html();
			$("body").append("<div class='fullScreen_img'>" + image + "</div>");
			//$('.fullScreen_img img').css("padding-top", ($(window).height/2) - ($(".fullScreen_img img").height()/2) );
			$('.fullScreen_img').fadeIn();
	
		$(".fullScreen_img").on("click", function(){
			$('.fullScreen_img').fadeOut();
		});
			
	});


	/* Search Bar START */
	var timer;
	var searchPulse = 0;
	$("#search_product input").on("keyup", function(){
		var searchVal = $(this).val();
		var $clear_search = $(".search_del");
		if(searchVal.length > 0) {
			$clear_search.fadeIn();
		}
		else{
			$clear_search.fadeOut();
		}
		if(searchVal.length > 2) {
			clearTimeout(timer);
			timer = setTimeout(function(){
				searchVal = $("#search_product input").val();
				if(searchVal.length > 2) {
					searchProduct(searchVal);
					$("#pageAside").off("scroll");
				}
			}, 1000);
			searchPulse = 0;
		} else if(searchVal.length == 0 && searchPulse == 0){
			$clear_search.fadeOut();
			getProductsList();
			searchPulse = 1;
			$("#pageAside").on("scroll", function(){
		    	if($("#pageAside ul#listed_products").is(":visible")){
		    		if( ($(this).scrollTop() + $(this).innerHeight()) == $(this).get(0).scrollHeight){
			    		loadMoreProductsList();
			    	}
		    	}
		    	
		    });
		}
	});
	/* Search Bar END */


	//Catch when the user is scrolled to bottom:
    $("#pageAside").on("scroll", function(){
    	if($("#pageAside ul#listed_products").is(":visible")){
    		if( ($(this).scrollTop() + $(this).innerHeight()) == $(this).get(0).scrollHeight){
	    		loadMoreProductsList();
	    	}
    	}
    	
    });

});

//REPORTS LOGIC!
/*
function set_report(){
	switch(currentLang){
		case "de":
			reports = reports_de;
			break;
		case "en":
			reports = reports_en;
			break;
		case "us":
			reports = reports_en;
			break;
		default:
			reports = reports_de;
			break;
	}

}
function getUserReportList(){
	set_report();
	if(langset == 1){
		continue_with_logic();
	}
	else{
		window.setTimeout(function () { getUserReportList(); }, 200);
	}
	function continue_with_logic(){
	    cordova.exec(fireUserReportList, execFailed, 'Report', 'getProductReportList', [$('#pageAside').find('.active').find('.number').data('productid')]);

	    function fireUserReportList(param) {
			function record(report_name, report_desc, report_id){
				var productRecord = '<tr><td class="address" data-repid="'+report_id+'">';
				productRecord += report_name;
				productRecord += '</td><td>';
				productRecord += report_desc;
				productRecord += '</td></tr>';
				return productRecord;
			};

			var result = eval('(' + param + ')');
			var result_html = "";
			for(i = 0; i < result.length; i++){
				var report_name = result[i]["TEXT"];
				var report_desc = result[i]["DESCRIPTION"];
				var report_id = result[i]["ID"];
				result_html += record(report_name, report_desc, report_id);
				
			}
			$("#products_reports table tbody").html(result_html);
			$("#products_reports table tbody").on( 'click', "tr", function () {
				var repId = $(this).find(".address").data('repid').toString();
				getUserReportDetails(repId);
			});
		}
	}
}

function getUserReportDetails(repId){
	cordova.exec(fireUserReportDetails, execFailed, 'Report', 'getProductReportDetails', [repId]);
	function fireUserReportDetails(param) {
		var result = eval('(' + param + ')');
		$('#msc_chart').removeAttr('class');
		$('#msc_chart').removeAttr('style');
		$('#msc_chart').css('margin-top', '20px');
		$('#msc_chart').html('');
		var res_html = result['TEXT'];
		$('#functionality').html(res_html);
		if($('#functionality').html().search('set_custom_report') > -1){
			eval($('#functionality script').html());	
		}
		$(".contentWrapper:visible").hide();
		$("#products_reports_details").show();
		if($('#functionality').html().search('set_custom_report') > -1){
			set_custom_report();
		}
	}
}*/
