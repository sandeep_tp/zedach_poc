//Custom Message System, types: error, success, warning, information
function customAlert(messages) {
	var message,overlap,error,close;
	
	error = 0;
	overlap = '<div id="overlap"></div>';

	$('#overlap').remove();
	$('#message').remove();

	message = '';
	message += '<div id="message">';
		message += '<div id="message_header">';
			message += '<div id="message_heading">' + lang.customAlert_title + '</div>';
			//message += '<div id="message_close"></div>';
		message += '</div>';
		message += '<ul>';
			$.each(messages, function(index, value) {
				if (value=='error') error++;
				if(value=='sync') error++; value='information';
				message += '<li class="'+value+'">'+index+'</li>';
			});
		message += '</ul>';
	message += '</div>';
	
	//Overlap effect:
	//$('body').append(overlap, message);
	$('body').append(message);

	$('#overlap').fadeIn();
	$('#message').slideDown();

	$('#message').on("click", function(){
		$('#overlap').fadeOut();
		$('#message').slideUp();
		window.clearTimeout(close);
	});

	/*
	$("#overlap").on("click", function(){
		$('#message_close').click();
	});
	*/

	if (error==0) {
		close = setTimeout(function() {
		      $('#message_close').click();
		}, 7000);
	};
}

//Show/Hide Section:
function showSection(id, hideDiv, domEl){
	$(hideDiv).fadeOut(function(){
		$(id).fadeIn();
	});
	$("#pageDownNav li").removeClass("active");
	$(domEl).addClass("active");

	return false;
}

//Cordova Exec Failed:
function execFailed(param) {
	$('#loadingBar').fadeOut();
	var execFailedDict = {};
	if ( param == "05" ){
		execFailedDict[lang.internet_err] = "error";
        customAlert(execFailedDict);
	} else {
		execFailedDict[param] = "error";
		customAlert(execFailedDict);
	}
}

//GPS Error message:
function onGpsError(error) {
	var error_msg = lang.gps_err;
    var gpsErrorDict = {};
	gpsErrorDict[error_msg] = "error";
	customAlert(gpsErrorDict);

	//Remove Loading Animation:
	$('#loadingBar').fadeOut(500);
	$('#loadingBar2').fadeOut(500);
	$('#inner_loadingBar_map').fadeOut(500);
	$('#map_zoomLevel').hide();
}

//Check if there's internet connection:
function doIhaveInternet() {
    if(navigator.network.connection.type == Connection.NONE){
        return false;
    }else{
        return true;
    }
}

//Open external URL via the default browser:
function cleanURL(url){
	if ( url.substring(0, 7) == "http://" || url.substring(0, 7) == "https://" ){
		return url;
	} else if( url.substring(0, 7) == "Http://" || url.substring(0, 7) == "Https://"  ){
		return "http://" + url.substring(7, url.length);
	} else {
		return "http://" + url;
	}
}
var already_used = 0;
//Get Params from the URL via GET
function getParams() {
	var params = {};
	if(already_used == 0){
		var prmstr = window.location.search.substr(1);
		var prmarr = prmstr.split ("&");
		for ( var i = 0; i < prmarr.length; i++) {
		    var tmparr = prmarr[i].split("=");
		    params[tmparr[0]] = tmparr[1];
		}
	}
	return params;
}

//Scroll to specific element
jQuery.fn.scrollTo = function(elem, speed) {
	if( typeof $(elem).offset() != 'undefined' ){
		$(this).animate({
			scrollTop:  $(this).scrollTop() - $(this).offset().top + $(elem).offset().top - 10 - $(".fixed_search_holder").filter(":visible").height()
		}, speed == undefined ? 1000 : speed);
		return this;
	}
};


//Get the date formatted properly:
var todaysDate = new Date();
var offset = 1; 
todaysDate.setMonth(todaysDate.getMonth() + offset);


var reminderDate = todaysDate.getFullYear().toString() + '-' + ("0" + (todaysDate.getMonth() + 1)).slice(-2) + '-' +  ("0" + todaysDate.getDate()).slice(-2); 
var germanDatePattern = "dd.MM.yy";
var unitedKingdomDatePattern = "dd/MM/yyyy";
var frenchDatePattern = "dd/MM/yy";
var unitedStatesDatePattern = "M/d/yy";
var resubBUKRS;
var resub_TYP_Act = "1";
var resub_TYP_Doc = "0";

function validatePhoneNumber(phoneNumber){
    var valid = true;
    var stripped = phoneNumber.replace(/[\-\ \+\x]/g, '');

    if(phoneNumber == ""){
    	valid = false;
    } else if(phoneNumber.split(' ').length > 3){
    	valid = false;
    } else if(phoneNumber.split('-').length > 2){
    	valid = false;
    } else if(phoneNumber.split('+').length > 2){
    	valid = false;
    } else if(phoneNumber.indexOf("+") > 0){
    	valid = false;
    } else if(phoneNumber.indexOf("-") < 2 && phoneNumber.indexOf("-") > -1){
    	valid = false;
    }else if (isNaN(stripped)) {
        valid = false;
    }else if (stripped.length > 40) {
        valid = false;
    } else if ( phoneNumber.match(/[a-zA-Z]/g) ) {
	    valid = false;
	}
    return valid;
}

function validateEmail(value){
	return /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
}

function formatDate(date) { 
	if(date == undefined) return ""; 
	if(date.length != 8 || date == "00000000") return ""; 
    var year = date.slice(0,4);
    var month = date.slice(4,6);
    var day = date.slice(6,8);
    var formattedDate;
    switch(currentDatePattern){
        case germanDatePattern:
            formattedDate = day + "." + month + "." + year;
            break;
        case unitedKingdomDatePattern:
            formattedDate = day + "/" + month + "/" + year;
            break;
        case frenchDatePattern:
            formattedDate = day + "/" + month + "/" + year;
            break;
        case unitedStatesDatePattern:
            formattedDate = month + "/" + day + "/" + year;
            break;
    }
    
    return formattedDate;
}

function dateChangeHandler(e){
  
	var currentDate = new Date();
	var selectDate = new Date(e.target.value);

	currentDate.setHours(0,0,0,0);
	selectDate.setHours(0,0,0,0);

	if (currentDate > selectDate) {
		 e.target.value = currentDate;
	} 
}

function discountZRAMOut(e)
{
	var discValue = e.target.value;
	discValueD = discValue.match(/^(?:\d*\.\d{1,3}|\d+)$/);
	discValueC = discValue.match(/^(?:\d*\,\d{1,3}|\d+)$/);
	if (discValueC == null && discValueD == null)
	{
		e.target.value = 0;
	}

	var discFloat = e.target.value.replace(',','.');
	if (discFloat > 100) {
		e.target.value = 0;
    }
    $(e.target).trigger("keyup")
}

function validateYear(e)
{
	var validValue = e.target.value;
	validValue = validValue.match(/^(?:\d*|\d+)$/);
	if (validValue == null)
	{
		e.target.value = 0;
		return;
	}

	if (validValue > 9999) {
		e.target.value = 0;
    }
}

function validateTurnover(e)
{
	var validValue = e.target.value;
	validValueC = validValue.match(/^(?:\d*\,\d{1,2}|\d+)$/);
	validValueD = validValue.match(/^(?:\d*\.\d{1,2}|\d+)$/);
	if (validValueC == null && validValueD == null)
	{
		e.target.value = 0;
	}

	var discFloat = e.target.value.replace(',','.');
	if (discFloat.toString().length > 19) {
		e.target.value = 0;
    }
}

$(document).ready(function() {

	/* Fix Styles START */
	$("#pageAside").css({
		"height": $('body').height() - ($("#pageHeader").height()+1)
	});
	$(".fixedScrollBox").css({
		"height": $('body').height() - ($("#pageHeader").height()+1) - ($(".fixed_search_holder").height() + 1)
	});
	$(".contentWrapper, .scrollable").css({
		"height": $('body').height() - ( $("#pageHeader").height() + 1 + $("#pageDownNav").height() + 2 )
	});
	$(".noDownNav").css({
		"height": $('body').height() - (($("#pageHeader").height()+1))
	});
	/* Fix Styles END */

	//Set Active List Item:
	$("#pageAside ul").on('click', "a", function () {
   		$("#pageAside li").removeClass("active");
		$(this).parent().addClass("active");
	});
	$(".search_del").on("click", function(){
		$(this).hide();
		$("#search_box").val("").focus().keyup();
	});
});