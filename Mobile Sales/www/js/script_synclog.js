$(function(){
	//$('table th').off('click');
	document.addEventListener("deviceready", getSyncLog, false);
});

$(document).ready(function(){
	$('#delete_all_log').on('click', function(){
		var buttonsOpts = {};
		buttonsOpts[lang.cancelButton] = function() {
			$( "#deleteAll_confirm" ).dialog( "close" );
		};

		buttonsOpts[lang.confirm] = function() {
			$( "#deleteAll_confirm" ).dialog( "close" );
			$('#loadingBar').show();
	        cordova.exec(logDeleted, execFailed, 'SyncLog', 'deleteAllSyncLog', ['']);
			function logDeleted(){
				window.location.reload();
			}
		};
		
		$( "#deleteAll_confirm" ).dialog({
			resizable: false,
			draggable: false,
			height: 250,
			width: 450,
			modal: true,
			buttons : buttonsOpts,
			show: 'fade',
			hide: 'fade',
			title: lang.confirm_title
		});
	});
});

function getSyncLog(){
	
	if(typeof currentDatePattern == "undefined"){
		setTimeout(function(){getSyncLog()}, 50);
	}
	else{
		function formatDate (date) {
		    date = date.toString();
			var year = date.slice(0,4);
			var month = date.slice(4,6);
			var day = date.slice(6,8);
			var formattedDate = "";
			var germanDatePattern = "dd.MM.yy";
			var unitedKingdomDatePattern = "dd/MM/yyyy";
			var frenchDatePattern = "dd/MM/yy";
			var unitedStatesDatePattern = "M/d/yy";

			switch(currentDatePattern){
				case germanDatePattern:
					formattedDate = day + "." + month + "." + year;
					break;
				case unitedKingdomDatePattern:
					formattedDate = day + "/" + month + "/" + year;
					break;
				case frenchDatePattern:
					formattedDate = day + "/" + month + "/" + year;
					break;
				case unitedStatesDatePattern:
					formattedDate = month + "/" + day + "/" + year;
					break;
			}

			return formattedDate;
		}
		cordova.exec(fireGetSyncLog, execFailed, 'SyncLog', 'getSyncLog', ['']);
		function fireGetSyncLog(param){
			function record(timee, messagee, datee, messaget, entity){
				var syncLogRecord = "";
				syncLogRecord += "<tr><td>";
				syncLogRecord += formatDate(datee);
				syncLogRecord += "</td><td>";
				syncLogRecord += timee.charAt(0)+timee.charAt(1)+':'+timee.charAt(2)+timee.charAt(3)+':'+timee.charAt(4)+timee.charAt(5);
				syncLogRecord += "</td><td>";
				syncLogRecord += entity;
				syncLogRecord += "</td><td>";
				syncLogRecord += messaget;
				syncLogRecord += "</td><td>";
				syncLogRecord += messagee;
				syncLogRecord += "</td></tr>";
				return syncLogRecord;
			};

			var result = eval('(' + param + ')');
			var result_html = "";
			for(i = 0; i < result.length; i++){
				var timee = result[i]["Time"];
				var messagee = result[i]["Message"];
				switch (result[i]["Entity"]){
					case "1":
						var entity = lang.syncLogTypes[0];
						break;
					case "2":
						var entity = lang.syncLogTypes[1];
						break;
					case "3":
						var entity = lang.syncLogTypes[2];
						break;
					case "4":
						var entity = lang.syncLogTypes[3];
						break;
					default:
						var entity = "ERROR";
						break;
				}
				var datee = result[i]["Date"];
				var messaget = result[i]["MessageType"];
				result_html += record(timee, messagee, datee, messaget, entity);
			}

			$("#syncLogTable tbody").html(result_html);
			if($('#syncLogTable tbody tr').length > 0){
				$('#delete_all_log').show();
				$('#synchLogTitle').css('padding-left', '160px');
			}
			var date_sort_settings;
		    switch(currentDatePattern){
		    	case germanDatePattern:
		            date_sort_settings = {
						sorter: "GermanDate_format"
					};
		            break;
		        case unitedKingdomDatePattern:
		        case frenchDatePattern:
		        	date_sort_settings = {
						dateFormat: "ddmmyyyy"
					};
		            break;
		        case unitedStatesDatePattern:
		       		date_sort_settings = {
						dateFormat: "mmddyyyy"
					};
		            break;
		        default:
		        	date_sort_settings = {
						dateFormat: "ddmmyyyy"
					};
		    }
			$("#syncLogTable").tablesorter({
				headers: {
					0: date_sort_settings
				}
			});
		}
	}
	$('#loadingBar').fadeOut(500);
}