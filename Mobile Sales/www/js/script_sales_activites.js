var parent_vbeln;
var order_num = "0";
var return_num = "0";
var exchange_num = "0";
var dialog_builded = 0;
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
if(dd<10){dd='0'+dd}
if(mm<10){mm='0'+mm}
var date =  yyyy.toString()+mm.toString()+dd.toString();
var today_sa =  yyyy.toString()+"-"+mm.toString()+"-"+dd.toString();
var hour = today.getHours();
if(hour<10){hour='0'+hour}
var minutes = today.getMinutes();
if(minutes<10){minutes='0'+minutes}
var secs = today.getSeconds();
if(secs<10){secs='0'+secs}
var new_doc_num = "*"+yyyy.toString()+mm.toString()+dd.toString()+hour.toString()+minutes.toString()+secs.toString();
var must_be_clicked = 0;
var exch_q1 = 0;
var exch_q2 = 0;
var ex_total_currenty = "";
var ddp_g=0;
var searchArr = {};
var SetRanges = {};
var Ranges = {};
var get_all_activities = false;

var current_username = "";
var current_password ="";
var currentSalesDocDetails;
(typeof getParams().vbeln == "undefined") ? parent_vbeln = "" : parent_vbeln = getParams().vbeln;
function getUserCredentials(){
	cordova.exec(storeCredentials, execFailed, 'Login', 'getCurrentUserCredentials', ['']);

	function storeCredentials(param){
		var result = eval('(' + param + ')');
		current_username = result.username;
		current_password = result.password;
	}
}

function getUser(){
	cordova.exec(storeUser, execFailed, 'User', 'getUser', ['']);

	function storeUser(param){
		var result = eval('(' + param + ')');
		resubBUKRS = result.VKORG;
	}
}

function emailSalesActivity(subject, note) {
    window.open("mailto:?body=" + note + "&subject=" + subject, "_system");

}
$(document).ready(function(){
	$('#add_date').val(today_sa);
});
$(function(){
	//Catch when the user is scrolled to bottom:
    $("#pageAside").on("scroll", function(){
    	if( ($(this).scrollTop() + $(this).innerHeight()) == $(this).get(0).scrollHeight){
    		getSalesActivitesList("get100more",false);
    	}
    });

	//Generall Button:
	$("#but_general").on("click", function(){
		//Apply class active:
		$("#pageDownNav li").removeClass("active");
		$(this).addClass("active");

		$('#pageAside li.active').click();
		/*$(".customers_current").filter(":visible").fadeOut(function(){
			$("#sales_activites_add").hide();
			$("#sales_activities").fadeIn();
		});*/
	});


	/* Search Bar START */
	var timer;
	var searchPulse = 0;
	$("#search_sales_activity input").on("keyup", function(){
		var searchVal = $(this).val();
		var $clear_search = $(".search_del");
		if(searchVal.length > 0) {
			$clear_search.fadeIn();
		}
		else{
			$clear_search.fadeOut();
		}
		if(searchVal.length > 2) {
			clearTimeout(timer);
			timer = setTimeout(function(){
				searchVal = $("#search_sales_activity input").val();
				if(searchVal.length > 2){
					searchPP(searchVal, 0);
					$("#pageAside").on("scroll");
				}
			}, 1000);
		searchPulse = 0;
		} else if(searchVal.length == 0 && searchPulse == 0){
			$clear_search.fadeOut();
			if(($('#matl_type').val() == null || $('#matl_type').val().length == 0) && ($('#matl_group').val() == null || $('#matl_group').val().length == 0)){
				
				get_all_activities = false;
				getSalesActivitesList('',true);
				$('#aside_nav_activities a').removeAttr('class');
				$('#my_activities').addClass('active');
			}
			else{
				searchPP('', 1);
			}
			
			searchPulse = 1;
			$("#pageAside").on("scroll", function(){
		    	if( ($(this).scrollTop() + $(this).innerHeight()) == $(this).get(0).scrollHeight){
		    		getSalesActivitesList("get100more",false);
		    	}
		    });
		}
	 });
	/* Search Bar END */
	
	
	document.addEventListener("deviceready", getUser, false);
	document.addEventListener("deviceready", getUserCredentials, false);
	document.addEventListener("deviceready", populateAdvancedSearch, false);

	setTimeout(function(){ document.addEventListener("deviceready", getSalesActivitesList, false);  }, 300)

	//TO DO: Get Validation Info: AUFSD, LIFSD and VSBED:
	/*function getValidationInfo(){
		var customer_id = $('#customer_id').val();

		var a = 1;
		cordova.exec(function(param){
			var result = eval('(' + param + ')');

			a = 2;
			/*
			$("#customers_header .save").data("aufsd", result["AUFSD"]);
			$("#customers_header .save").data("lifsd", result["LIFSD"]);
			$("#customers_header .save").data("vsbed", result["VSBED"]);
			*/


		/*}, execFailed, 'Customer', 'getCustomerDetails', [customer_id]);

		return a;
	}*/
	$('.show_ref_sd').on("click", function(){
		getSalesOrderDetails($('.show_ref_sd').attr('docnum'), $('#customer_id').val(), 0, 1);
	});

	$(".create_order_but").on("click", function(){
		//Apply class active:
		//if( !$("#sales_activites_add").is(":visible") ){
			$("#pageDownNav li").removeClass("active");
			$(this).addClass("active");
		//}

		var customer_id = $("#pageAside li.active .sa_number").data("kunnr");
		cordova.exec(validationSuccess, execFailed, 'Customer', 'getCustomerDetails', [customer_id]);
		
		function validationSuccess(param){

			var result = eval('(' + param + ')');
			if(result["AUFSD"] != ""){
				//Show AUFSD Error:
				var freeProduct = {};
				freeProduct[lang.createOrder_AUFSDError] = "error";
				customAlert(freeProduct);
			} else {
				//It's ok:

				if($('#pageAside').find('.active table').data('dnr').toString().charAt(0) == "*"){

					cordova.exec(validate_docNumber, execFailed, 'Order', 'getAllListForSalesActivity', [$('#pageAside').find('.active table').data('dnr').toString()]);        
					function validate_docNumber(doc_param){
						var result_doc = eval('(' + doc_param + ')');
						//alert(doc_param);
						var containsStar = false;
						var order_id = "0";
						var chk = 0;
						for(i = 0; i < result_doc.length; i++){
							var current_item = result_doc[i]["DOC_NUMBER"].toString();
							//Released! Fire Details:
							if(current_item.charAt(0) == "+"){
								chk = 1;
								cordova.exec(checkSuccess, execFailed, 'OpenQuotation', 'isOpenQuotation', [$(this).data('docnumber')]);
								function checkSuccess(answer){ 
									var open_Q;
									(answer=="X") ? open_Q = 1 : open_Q = 0;
									getSalesOrderDetails(current_item, $('#customer_id').val(), open_Q);
								}
							}
							//Not Released yet!
							if(current_item.charAt(0) == "*"){
								order_id = result_doc[i]["DOC_NUMBER"];
								containsStar = true;
								break;
							}
						}
						if(chk==0){
							if(containsStar){
								window.location = "createOrder.html?said="+$('#pageAside .active table').data('dnr')+"&uid="+$('.active').find('.sa_number').data("kunnr")+"&edit=1&suba_id="+order_id;
							}
							else{
							 window.location = "createOrder.html?said="+$('#pageAside .active table').data('dnr')+"&uid="+$('.active').find('.sa_number').data("kunnr")+"&edit=0"; 
							}
						}
					}
				}
				else{
					cordova.exec(checkSuccess, execFailed, 'OpenQuotation', 'isOpenQuotation', [$(this).data('docnumber')]);
					function checkSuccess(answer){ 
						var open_Q;
						(answer=="X") ? open_Q = 1 : open_Q = 0;
						getSalesOrderDetails(order_num,$('#customer_id').val(),open_Q);
					}
				}
			}

		}
	});

	$("#cancel_but").on("click", function(){
		window.location = "customers.html?salesAct_customerid=" + $('#customer_id').val();
	});
	$("#cancel_but2").on("click", function(){
		$('#pageAside ul li.active').click();
	});
	
	$("#email_sa").on("click", function(){
		var customerData = "";
		var SalesDocNum = "";
        var emailBody = toEmailFriendlyBody($('#free_text').val());
        if ($('#current_customer_name').text() != "undefined")
        {
        	customerData = $('#current_customer_name').text() + " (" + $('#pageAside .active table tr').find('td[data-kunnr]').attr('data-kunnr') + ")  ";
        }
        var customerNum = $('#pageAside .active table tr').find('td[data-kunnr]').attr('data-kunnr');
        SalesDocNum = $('#pageAside .active table tr').find('td[data-suba_ord]').attr('data-suba_ord'); // lang.downMenu[3]
        emailBody = encodeURIComponent(emailBody);
		emailSalesActivity(lang.visit_report + " ("+ SalesDocNum +") "+ customerData +  lang.email_contact + " (" + $('#pageAside .active table').data('dnr') + ")", emailBody);
	});
	var translations_set =0;
	var dialog_title = "Create Follow-Up";
	var button1 = "Save";
	var button2 = "Cancel";
	$("#cfu").on("click", function(){
		if(dialog_builded == 0){
			if(translations_set == 0){
				var dialog_title = lang.HomeScreenDialog[0];
				var button1 = lang.saveButton;
				var button2 = lang.dialog_cancel;
				translations_set++;
			}
			var buttonsOpts = {};
			buttonsOpts[button1] = function() {
				createFollowUp();
	        	$( "#dialog" ).dialog( "close" );
			};
			buttonsOpts[button2] = function() {
	        	$( "#dialog" ).dialog( "close" );
			};
			$(".dialog_label_followUp").text(dialog_title);
			$( "#dialog" ).dialog({
				resizable: false,
				height: 250,
				width: 450,
				modal: true,
				buttons : buttonsOpts
		    });
			
		    $("#dialog").dialog('option', 'title', lang.create_follow_up);
		    dialog_builded = 1;
		}
		else{
			$( "#dialog" ).dialog( "open" );
		}
	});
  
  /**
   replaces newlines characters (\n) with ASCII representation (%0A) so newlines are displayed properly in email
   **/
  function toEmailFriendlyBody(text)
  {
    var lines = text.split('\n');
    var newText = lines.join("%0A");
    return newText;
  }
  
//  $("#quick_deleteSA").on("click", function(){
//                          cordova.exec(deleteSuccess, execFailed, 'SalesActivity', 'deleteSalesActivity', [$("#company").text()]);
//                          function deleteSuccess(){
//                          customAlert({ "Successfuly deleted Sales Activity!" : "success" });
//                          getSalesActivitesList();
//                          }
//                          });
	function createFollowUp(){
		$('#loadingBar').show();
		cordova.exec(fireSaleActivityDetailsForFU, execFailed, 'SalesActivity', 'getSalesActivityDetails', [$('#pageAside').find('.active').find('table').data('dnr').toString()]);
		function fireSaleActivityDetailsForFU(param){
			var result = eval('(' + param + ')');
			var myArray22 = {
				"ACTIVITY_TYPE"    : result["ACTIVITY_TYPE"],
				"CONTACT"          : result["CONTACT"],
				"PARTN_ID"         : result["PARTN_ID"],
				"FROM_DATE"        : $('#fdate').val().replace('-', '').replace('-', ''),
				"FROM_TIME"        : result["FROM_TIME"].toString(),
				"TO_TIME"          : result["TO_TIME"].toString(),
				"DOC_NUMBER"       : new_doc_num,
				"VBELV_SA"         : result["DOC_NUMBER"].toString(),
				"RELEASE"          : "",
				"ACTIVITY_COMMENT" : result["ACTIVITY_COMMENT"],
				"STATE"			   : "0",
				"CREATED_BY"	   : current_username.toUpperCase()
			};
			cordova.exec(createSuccess, execFailed, 'SalesActivity', 'createSalesActivity', [myArray22]);	
			function createSuccess(){
				window.location = "sales_activities.html?salesActivityDocNumber=" + new_doc_num + "&uid=" + result["PARTN_ID"];
			}
		}
		$('#loadingBar').hide();
	}
	$('#sales_activities_edit').on("click", function(){
								$(".quickNav").fadeOut();
								editting = "1";
								$('#heading').text(lang.edit_sales_act);
								$('#freetext_add').val($('#free_text').val());
								var edit_dets = $('#edit_details').val().split("_");
								/*if($('#customer_id').val() != "0"){
								//$("#add_sacu").parent().parent().hide();
								}
								else{
								cordova.exec(fireCustomerList, execFailed, 'Customer', 'getCustomerList', ['']);
								}*/
								//cordova.exec(fireCustomerList, execFailed, 'Customer', 'getCustomerList', ['']);
								cordova.exec(fireContactList, execFailed, 'Contact', 'getContactListByCustomerNr', [edit_dets[0]]);
								cordova.exec(fireSAResultList, execFailed, 'Customizing', 'getKTAERList', ['']);
								cordova.exec(fireSATypeList, execFailed, 'Customizing', 'getKTAARList', ['']);
								
								
								/*function fireCustomerList(param){
									var result = eval('(' + param + ')');
									var selectHtml = "";
									for(i = 0; i < result.length; i++){
										selectHtml += '<input id="add_sacu" class="custom_input addNewPerson_input" ';
										if(result[i]["KUNNR"] == edit_dets[0]){
											selectHtml += ' data-cuid="'+edit_dets[0]+'" disabled value="'+result[i]["NAME1"]+'"> ';
										}
									}*/
									$("#add_sacu").parent().html('<input id="add_sacu" class="custom_input addNewPerson_input2" data-cuid="'+edit_dets[0]+'" disabled value="'+edit_dets[5]+'"> ');
									//$("#add_sacu").select2();
									//$("#add_sacu").attr('disabled', 'disabled');
								//}
								
								function fireSATypeList(param){
								var result = eval('(' + param + ')');
								var selectHtml = "";
								for(i = 0; i < result.length; i++){
									if(result[i]["VKORG"] == resubBUKRS && result[i]["VTWEG"] == "01"){
										selectHtml += '<option ';
										//$('.create_order_but').show();

										if(result[i]["KTAAR"] == edit_dets[1]){
											if(result[i]["ORDER_LOCK"] == "X"){
												$('.create_order_but').hide();
											}
											selectHtml += ' selected="selected" ';
										}
										selectHtml += ' value="' + result[i]["KTAAR"] + '">' + result[i]["VTEXT"] + '</option>';
									}
								}
								$("#add_sat").html(selectHtml);
								if(edit_dets[2].toString().charAt(0) != "*"){
									$("#add_sat").attr('disabled', 'disabled');
									$("#add_saco").attr('disabled', 'disabled');
									$("#add_date").attr('disabled', 'disabled');
									$("#add_ftime").attr('disabled', 'disabled');
									$("#add_ttime").attr('disabled', 'disabled');
									$('.salesResult_holder').css('visibility', 'visible');
									$("#cancel_but2").show();
								}
								else{
									$("#add_sat").removeAttr('disabled');
									$("#add_saco").removeAttr('disabled');
									$("#add_date").removeAttr('disabled');
									$("#add_ftime").removeAttr('disabled');
									$("#add_ttime").removeAttr('disabled');
									//$('.create_order_but').show();
									$("#cancel_but2").hide();
								}
								$('#customers_salesDocuments').fadeOut();
								showSection("#sales_activites_add", "#sales_activities", '#but_general');
								}
								
								function fireContactList(param){
								var result = eval('(' + param + ')');
								var select = lang.dropDownMenu_select;
								var selectHtml = '<option value="0000000000">' + select + '</option>';
								for(i = 0; i < result.length; i++){
									if(result[i]["NEW"] != "X"){
										selectHtml += '<option ';
										if(result[i]["PARNR"] == edit_dets[3]){
														selectHtml += ' selected="selected" ';
										}
										selectHtml += ' value="' + result[i]["PARNR"] + '">'+ result[i]["NAMEV"] + ' ' + result[i]["NAME1"] + '</option>';
									}
								}
								$("#add_saco").html(selectHtml);
								//$("#add_saco").attr('disabled', 'disabled');
								}

				function fireSAResultList(param){
								var result = eval('(' + param + ')');
								var selectHtml = "";
								var alreadySelected = 0;
								for(i = 0; i < result.length; i++){
									selectHtml += '<option ';
									if(result[i]["KTAER"] == edit_dets[4]){
										selectHtml += ' selected="selected" ';
										alreadySelected = 1;
									}
									if(alreadySelected == 0){
										if(result[i]["KTAER"] == "009"){
											selectHtml += ' selected="selected" ';
										}
									}
									selectHtml += ' value="' + result[i]["KTAER"] + '">' + result[i]["VTEXT"] + '</option>';
								}
								$("#add_sares").html(selectHtml);
								//$("#add_sares").attr('disabled', 'disabled');
								}
								var customer_id = $("#pageAside li.active .sa_number").data("kunnr");
								cordova.exec(validationSuccess, execFailed, 'Customer', 'getCustomerDetails', [customer_id]);
		
								function validationSuccess(param){

									var result = eval('(' + param + ')');
									if(result["AUFSD"] != ""){
										$('.create_order_but').hide();
									}
								} 
								});
	/* Add New Contact START */
	$("#quick_createSA").on("click", function(){
		$(".quickNav").fadeOut();
		$("#cancel_but").show();
		/* Remove "readonly" on all selects: */
		$("#sales_activity_add select").removeAttr("disabled");
		editting = "0";
		var string = lang.sale_activity_add_header;
			
		$('#heading').text(string+formatDate(date)+' - '+hour+':'+minutes);
		//$('#heading').text('Add New Sales Activity1: '+formatDate(yyyy+mm+dd)+' - '+hour+':'+minutes);
		if($('#customer_id').val() != "0"){
			//$("#add_sacu").parent().parent().hide();
			cordova.exec(fireContactList, execFailed, 'Contact', 'getContactListByCustomerNr', [$('#customer_id').val()]);
			cordova.exec(fireCustomerList, execFailed, 'Customer', 'getCustomerDetails', [$('#customer_id').val()]);
		}
		else {
			cordova.exec(fireCustomerList, execFailed, 'Customer', 'getCustomerList', ['']);
		}
			
		cordova.exec(fireSAResultList, execFailed, 'Customizing', 'getKTAERList', ['']);
		cordova.exec(fireSATypeList, execFailed, 'Customizing', 'getKTAARList', ['']);
			
		$('#freetext_add').val('');
			
		function fireCustomerList(param){
			var result = eval('(' + param + ')');
			var customer_id = $('#customer_id').val();
			var select = lang.dropDownMenu_select;
			var selectHtml = "";
			if($('#customer_id').val() != "0"){
				var cust_id = $('#customer_id').val();
				selectHtml += '<input id="add_sacu" class="custom_input addNewPerson_input2 data-cuid="'+cust_id+'" disabled value="'+result["NAME1"]+'"> ';
			}
			else{
				var cust_id = edit_dets[0];
				for(i = 0; i < result.length; i++){
					selectHtml += '<input id="add_sacu" class="custom_input addNewPerson_input2" ';
					if(result[i]["KUNNR"] == cust_id){
						selectHtml += ' data-cuid="'+cust_id+'" disabled value="'+result[i]["NAME1"]+'"> ';
					}
				}
			}
			$("#add_sacu").parent().html(selectHtml);
			$("#add_sacu").trigger('change');
			}
			
			function fireSATypeList(param){
				var result = eval('(' + param + ')');
				var select = lang.dropDownMenu_select;
				var selectHtml = '<option value="0">' + select + '</option>';
				for(i = 0; i < result.length; i++){
					if(result[i]["VKORG"] == resubBUKRS && result[i]["VTWEG"] == "01"){
						selectHtml += '<option ';
						if(result[i]["DEFAULT_SEL"] == "X"){
							selectHtml += ' selected="selected" ';
						}
						selectHtml += ' value="' + result[i]["KTAAR"] + '">' + result[i]["VTEXT"] + '</option>';
					}
					
				}

				//Fill the create sales activity type select:
				$("#add_sat").html(selectHtml);
				$('.create_order_but').hide();
				showSection("#sales_activites_add", "#sales_activities", '#but_general');
			}
			
			function fireContactList(param){
			var result = eval('(' + param + ')');
			var select = lang.dropDownMenu_select;
			var selectHtml = '<option value="0000000000">' + select + '</option>';
			var no_results = 1;
			for(i = 0; i < result.length; i++){
				if(result[i]["NEW"] != "X"){
					no_results = 0;
					selectHtml += '<option value="' + result[i]["PARNR"] + '">'+ result[i]["NAMEV"] + ' ' + result[i]["NAME1"] + '</option>';
				}
			}
			if(no_results == 1){
			//alert(lang.NoContacts1+' '+$('#customer_name').val()+' '+lang.NoContacts2)
			}
			$("#add_saco").html(selectHtml);
			}

			function fireSAResultList(param){
			var result = eval('(' + param + ')');
			var selectHtml = "";
			for(i = 0; i < result.length; i++){
				selectHtml += '<option';
				if(result[i]["KTAER"] == "009"){
					selectHtml += ' selected="selected" ';
				}
			selectHtml += ' value="' + result[i]["KTAER"] + '">' + result[i]["VTEXT"] + '</option>';
			}
			$("#add_sares").html(selectHtml);
			}
		});
	$("#add_sacu").on('change', function(){
		cordova.exec(fireContactList, execFailed, 'Contact', 'getContactListByCustomerNr', [$("#add_sacu").data('cuid')]);
		
		function fireContactList(param){
			var result = eval('(' + param + ')');
			var select = lang.dropDownMenu_select;
			var selectHtml = '<option value="0000000000">' + select + '</option>';
			var no_results = 1;
			for(i = 0; i < result.length; i++){
				if(result[i]["NEW"] != "X"){
					no_results = 0;
					selectHtml += '<option value="' + result[i]["PARNR"] + '">'+ result[i]["NAMEV"] + ' ' + result[i]["NAME1"] + '</option>';
				}
			}
			if(no_results == 1){
				//alert(no_contact1+' '+$('option[value='+$("#add_sacu").data('cuid')+']').text()+' '+no_contact2)
			}
			$("#add_saco").html(selectHtml);
		}
	});
	
	//Click
	$("#pageAside ul").on( 'click', "li", function () {
		if (typeof(event) != 'undefined') {
			if (event.target.id == 'salesActStar' || event.target.id == 'salesActStarSvg') { return; } // Prevent Default navigation if favourite button is clicked}
		}
		

		$("#cancel_but").hide();
		$('.salesResult_holder').css('visibility', 'hidden');  //This hides the result select!
		$("#pageAside ul li").each(function(){
			 $(this).removeClass('active');
		});
		$(this).addClass('active');
		$("#pageDownNav li").removeClass("active");
		$("#but_general").addClass("active");
		
		//$("#pageContent").fadeIn();
		
		/*if($(this).find(".sa_number").data('doc_num').toString().charAt(0) == "*"){
			$("#sales_activites_add").fadeIn();
		}*/
		var saleId = $(this).find(".sa_number").data('doc_num').toString();
		
		order_num = $(this).find('.suba_dets').data('suba_ord');
		return_num = $(this).find('.suba_dets').data('suba_ret');
		exchange_num = $(this).find('.suba_dets').data('suba_exc');
		$('.create_order_but').hide();

		//Show the default text on the button:
		$('#save_but').text(lang.saveButton);

		editting = 1;
		getSaleActivityDetails(saleId.toString());
	});
	//Load Sales Activities List:
	//document.addEventListener("deviceready", getSalesActivitiesList, false);
	/* Quick Nav START */
	function fireMenu(){
		$(".quickNav_holder").fadeIn(500);
		$(".circles_holder").slideDown(500);
		$("#quickNav_but").stop().animate(
			{rotation: -90},
			{
				duration: 500,
				step: function(now, fx) {
					$(this).css({"transform": "rotate("+now+"deg)"});
					$(this).css({"-ms-transform": "rotate("+now+"deg)"});
					$(this).css({"-webkit-transform": "rotate("+now+"deg)"});
				}
			}
		);
	}
	function hideMenu(){
		$(".circles_holder").slideUp(500);
		$(".quickNav_holder").fadeOut(500);
		$("#quickNav_but").stop().animate(
			{rotation: -45},
			{
				duration: 500,
				step: function(now, fx) {
					$(this).css({"transform": "rotate("+now+"deg)"});
					$(this).css({"-ms-transform": "rotate("+now+"deg)"});
					$(this).css({"-webkit-transform": "rotate("+now+"deg)"});
				}
			}
		);
	}
	hideMenu();
	$("#quickNav_but").on("click", function(){
		if($(".quickNav_holder").css("display") == "none") {
			fireMenu();
		} else {
			hideMenu();
		}
		$(".quickNav_holder").on("click", function(){		
			hideMenu();
		});
	});
	/* Quick Nav END */

	$("#sales_activites_add #save_but").on('click', function(){
		debugger;
		if($("#add_date").val().length < 2 || $("#freetext_add").val().length < 1){
			var please_sel_data = {};
			please_sel_data[lang.select_date_note] = "error";
			customAlert(please_sel_data);
			return;
		}

		//Create New Sales Activity:
		if(editting == "0"){
			//If type is not selected: throw error:
			if($('#add_sat').val() == "0" || typeof $('#add_sat').val() == "object"){
				var please_sel_sat = {};
				please_sel_sat[lang.please_select_sat] = "error";
				customAlert(please_sel_sat);
				return false;
			}

			var from_time = hour.toString()+minutes.toString()+secs.toString();
			if($('#customer_id').val() != "0"){
				var cuid = $('#customer_id').val();
			}
			else{
				var cuid = $("#add_sacu").data('cuid');
			}
			var myArray = {
				"ACTIVITY_TYPE"    : $('#add_sat').val(),
				"CONTACT"          : $('#add_saco').val(),
				"PARTN_ID"         : cuid,
				"FROM_DATE"        : $('#add_date').val().replace('-', '').replace('-', ''),
				"FROM_TIME"        : $('#add_ftime').val().replace(':', '')+"00",
				"TO_TIME"          : $('#add_ttime').val().replace(':', '')+"00",
				"DOC_NUMBER"       : new_doc_num,
				"VBELV_SD"		   : parent_vbeln,
				"RELEASE"          : "",
				"ACTIVITY_COMMENT" : $('#freetext_add').val(),
				"STATE"			   : "0",
				"CREATED_BY"	   : current_username.toUpperCase()
			};
			//alert(JSON.stringify(myArray, null, 4));
			$('#loadingBar').fadeIn(function(){
				cordova.exec(createSuccess, execFailed, 'SalesActivity', 'createSalesActivity', [myArray]);
			});
			function createSuccess(data){
				$('#loadingBar').fadeOut();
				/*var saveSucc = {};
				saveSucc[lang.saved_sact] = "success";
				customAlert(saveSucc);*/
				//customAlert({ "You have successfully saved a Sales Activity!" : "success" });
				//getContactListByCustomer(customer_id);
				//getSalesActivitesList();
				editting = "1";
				//$('.save').text('Save');
				//Clear all inputs:
				//$("#customers_addNewContact :input").val("");

				//Change Back button to Delete:
				$("#delete_but").show();
				$("#cancel_but").hide();
				$('#sales_activities').find("#company").html('<input type="hidden" id="edit_details" value="'+cuid+'_'+$('#add_sat').val()+'_'+new_doc_num+'_'+$('#add_saco').val()+'_0_'+$("#add_sacu").val()+'">');
				
				var newRecord = '<li class="active" id="tempor"><table cellspacing="0" cellpadding="0" border="0" class="sa_table" data-dnr="'+new_doc_num+'"><tbody><tr><td id="sa_'+new_doc_num+'" class="sale_activity1">';
	       		newRecord += "<span>" + $('#freetext_add').val().substring(0,40)+ "</span>";
				newRecord += '</td></tr><tr><td colspan="2">'+$('#add_sacu').val()+'</td></tr><tr><td data-doc_num="'+new_doc_num+'" data-kunnr="'+cuid+'" id="ty_'+new_doc_num+'" class="sale_activity3 sa_number">';
				newRecord += $('#add_sat option[value='+$('#add_sat').val()+']').text();
				newRecord += '</td></tr><tr><td class="sale_activity3 suba_dets" data-suba_ord="0" id="suba_'+new_doc_num+'">';
				newRecord += formatDate($('#add_date').val().replace('-', '').replace('-', ''));
				newRecord += '</td></tr></tbody></table></li>';
				$('#pageAside ul').html(newRecord+$('#pageAside ul').html());
				setTimeout(function(){$("#sales_activites_add #save_but").click()}, 500);
			}

		}
		else if(editting == "2"){

			/* Check if there is a star in doc. number START */
			var current_docNumber = $('#pageAside .active table').data('dnr').toString();
			
			var edit_dets3 = $('#edit_details').val().split("_");
			var today_new = new Date();
			var dd_new = today_new.getDate();
			var mm_new = today_new.getMonth()+1;
			var yyyy_new = today_new.getFullYear();
			if(dd_new<10){dd_new='0'+dd_new}
			if(mm_new<10){mm_new='0'+mm_new}
			var date_new =  yyyy_new.toString()+mm_new.toString()+dd_new.toString();
			var hour_new = today_new.getHours();
			if(hour_new<10){hour_new='0'+hour_new}
			var minutes_new = today_new.getMinutes();
			if(minutes_new<10){minutes_new='0'+minutes_new}
			var secs_new = today_new.getSeconds();
			if(secs_new<10){secs_new='0'+secs_new}
			var to_time = hour_new.toString()+minutes_new.toString()+secs_new.toString();

			var myArray3 = {
				"RESULT"     	   : $('#add_sares').val(),
				"ACTIVITY_TYPE"    : $('#add_sat').val(),
				"CONTACT"          : $('#add_saco').val(),
				"FROM_DATE"        : $('#add_date').val().replace('-', '').replace('-', ''),
				"FROM_TIME"        : $('#add_ftime').val().replace(':', '')+"00",
				"TO_TIME"          : $('#add_ttime').val().replace(':', '')+"00",
				"ACTIVITY_COMMENT" : $('#freetext_add').val(),
				"RELEASE"    : "X",
				"DOC_NUMBER" : edit_dets3[2]//.replace('*', '+')
			};
			$('#loadingBar').fadeIn(function(){
				cordova.exec(releaseSuccess, execFailed, 'SalesActivity', 'updateSalesActivity', [myArray3]);
			});
			function releaseSuccess(data){
				$('#loadingBar').fadeOut();
				//customAlert({ "You have successfully released a Sales Activity!" : "success" });
				//getContactListByCustomer(customer_id);
				editting = "-1";
				$('#add_sat').removeAttr('disabled');
				//$('#add_sacu').removeAttr('disabled');
				$('#add_saco').removeAttr('disabled');
				window.location = "sales_activities.html?uid=" + $('#customer_id').val() + "&salesActivityDocNumber=" + edit_dets3[2];
                                           
				//$("#pageAside ul li").find('.active').click();
				//bojo
				//getSalesActivitesList();
				//Clear all inputs:
				//$("#customers_addNewContact :input").val("");
			}
		}
		else{
			//$('.save').text('Save');
			var edit_dets = $('#edit_details').val().split("_");
			var myArray2 = {
			//"ACTIVITY_TYPE"    : $('#add_sat').val(),
			"ACTIVITY_TYPE"    : $('#add_sat').val(),
			"RESULT"     	   : $('#add_sares').val(),
			"CONTACT"          : $('#add_saco').val(),
			"FROM_DATE"        : $('#add_date').val().replace('-', '').replace('-', ''),
			"FROM_TIME"        : $('#add_ftime').val().replace(':', '')+"00",
			"TO_TIME"          : $('#add_ttime').val().replace(':', '')+"00",
			"ACTIVITY_COMMENT" : $('#freetext_add').val(),
			"DOC_NUMBER"       : edit_dets[2]
			};
			/*cordova.exec(validate_docNumber, execFailed, 'Order', 'getAllListForSalesActivity', [edit_dets[2]]);
			function validate_docNumber(doc_param){
				alert(edit_dets[2]);
				var result_doc = eval('(' + doc_param + ')');
				for(i = 0; i < result_doc.length; i++){
					var current_item = result_doc[i]["DOC_NUMBER"].toString();
					if(current_item.charAt(0).toString() != "+"){*/
						$('.salesResult_holder').css('visibility', 'visible');
					/*}
				}
			}*/
			$('#loadingBar').fadeIn(function(){
				cordova.exec(updateSuccess, execFailed, 'SalesActivity', 'updateSalesActivity', [myArray2]);
			});
			function updateSuccess(datas){
				$('#loadingBar').fadeOut();
				if(edit_dets[2].toString().charAt(0) != "*" && edit_dets[2].toString().charAt(0) != "+"){
					var ecust2 = getParams().from_customer;
					if(ecust2 == "X"){
						window.location = "sales_activities.html?salesActivityDocNumber=" + edit_dets[2]+"&from_customer=X";
					}
					else{
						window.location = "sales_activities.html?salesActivityDocNumber=" + edit_dets[2] + "&uid=" + $('#customer_id').val();	
					}
					
				}
				var saveSucc = {};
				saveSucc[lang.saved_sact] = "success";
				customAlert(saveSucc);
				//getContactListByCustomer(customer_id);
				editting = "2";
				/*cordova.exec(validate_docNumber, execFailed, 'Order', 'getAllListForSalesActivity', [$('#pageAside').find('.active table').data('dnr').toString()]);        
				
				function validate_docNumber(doc_param){
					var result_doc = eval('(' + doc_param + ')');
					//alert(doc_param);
					var containsStar = false;
					var order_id = "0";
					var chk = 0;
					for(i = 0; i < result_doc.length; i++){
						if(result_doc[i]["DOC_TYPE"] == "ZMTA" || result_doc[i]["DOC_TYPE"] == "ZMAG"){
							var current_item = result_doc[i]["DOC_NUMBER"].toString();
							if(current_item.charAt(0) == "+"){
								chk = 1;
							}
						}
					}
					if(chk==0) $('.result_holder').css('visibility', 'visible');;
				}*/
				getSalesActivitesList("1",false);
				$('#pageAside .active').find('.suba_dets').text(formatDate($('#add_date').val().replace('-', '').replace('-', '')));
				var new_free_text = $('#freetext_add').val();
				if(new_free_text.length > 40){
					var new_free_text = new_free_text.substring(0,40);
				}
				if($('#pageAside .active').find('.sale_activity1').find('span').length > 1){
					var which_span = 'span:eq(1)';
				}
				else{
					var which_span = 'span:eq(0)';
				}
				$('#pageAside .active').find('.sale_activity1').find(which_span).text(new_free_text);
				$('#pageAside .active').find('.sa_number').text($("#add_sat option[value='"+$('#add_sat').val()+"']").text());
				$('#save_but').text(lang.release);
				//$('.create_order_but').show();

				// Show back button after save if activity created and navigated from customer.
				if((getParams().uid != '0') || (getParams().uid != ''))
				{
					$("#back_but").show();
				} 
				else
				{
					$("#back_but").show();
				}


				$('#add_date').on('focus', function(){
					set_save();
				});
				$('#add_ftime').on('focus', function(){
					set_save();
				});
				$('#add_ttime').on('focus', function(){
					set_save();
				});
				$('#freetext_add').on('focus', function(){
					set_save();
				});
				$('#add_saco').on('focus', function(){
					set_save();
				});
				$('#add_sat').on('focus', function(){
					set_save();
				});
				//Clear all inputs:
				//bbbb
				//$("#customers_addNewContact :input").val("");
			}
		}

		});
	});
function set_save(){
	editting = 1;
	$('#save_but').text(lang.saveButton);
	$('.salesResult_holder').css('visibility', 'hidden');
	$('#add_sares').val($('#add_sares option:first').val());
}

function populateAdvancedSearch(){
	if(langset != 1){
		window.setTimeout(function () { populateAdvancedSearch(); }, 200);
	}

	cordova.exec(populateMATL, execFailed, "Customizing", "getKTASTList", []);
	
	function populateMATL(param){
		var result = eval('('+param+')');
		$('#matl_type').html('<option disabled value="0">'+lang.dropDownMenu_select+'</option>');
		for(var i = 0; i<result.length; i++){
			$('#matl_type').append('<option value="'+result[i]['KTAST']+'">'+result[i]['VTEXT']+'</option>');
		}

		$("#matl_type").on('change',function(){
			if($("#matl_type").find('option:selected').length > 1) $("#matl_type option:eq(0)").removeAttr("selected");
			if($("#matl_type").find('option:selected').length == 0) $("#matl_type option:eq(0)").attr("selected", "selected");
		});
	}

	cordova.exec(populateMATLG, execFailed, "Customizing", "getKTAERList", []);

	function populateMATLG(param){
		var result = eval('('+param+')');
		$('#matl_group').html('<option disabled value="0">'+lang.dropDownMenu_select+'</option>');
		for(var i = 0; i<result.length; i++){
			$('#matl_group').append('<option value="'+result[i]['KTAER']+'">'+result[i]['VTEXT']+'</option>');
		}

		$("#matl_group").on('change',function(){
			if($("#matl_group").find('option:selected').length > 1) $("#matl_group option:eq(0)").removeAttr("selected");
			if($("#matl_group").find('option:selected').length == 0) $("#matl_group option:eq(0)").attr("selected", "selected");
		});
	}
}

function resetAdvancedSearch(fromSearchBox){
	if(fromSearchBox != 1){
		$('#search_box').val('');
		get_all_activities = false;
		getSalesActivitesList('',true);
		$('#aside_nav_activities a').removeAttr('class');
		$('#my_activities').addClass('active');
		$( "#advancedSearchDialog" ).dialog( "close" );
	}
	var aaa = advSearchForm.getElementsByTagName('select');
	
	for(var i = 0; i < aaa.length; i++) {
	  aaa[i].selectedIndex =0;
	}  
}

function searchPP(sht, where){
	var simples = 0;
	if(($('#matl_type').val() == null || $('#matl_type').val().length == 0) && ($('#matl_group').val() == null || $('#matl_group').val().length == 0)){
		if(where == 1){
			var searchErr = {};
	        searchErr[lang.filterMessage1] = "error";
	        customAlert(searchErr);
	        return false;
		}
		else{
			simples = 1;
		}
	}
	else if((parseFloat($('#price_from').val()) > parseFloat($('#price_to').val())) && $('#price_to').val() != ""){
		var searchErr = {};
        searchErr[lang.filterMessage2] = "error";
        customAlert(searchErr);
        return false;
	}
	else if((parseFloat($('#gwt_from').val()) > parseFloat($('#gwt_to').val())) && $('#gwt_to').val() != ""){
		var searchErr = {};
        searchErr[lang.filterMessage3] = "error";
        customAlert(searchErr);
        return false;
	}

	if($('.advancedSearchButtonHolder').css('fill') != "#ff0000"){
		$('.advancedSearchButtonHolder').css('fill', '#ffffff');
	}
	$('.advancedSearchButtonHolder').css('background-color', 'transparent');

	if(where == 1){
		$('.advancedSearchButtonHolder').css('fill', '#ff0000');
	}
	searchArr = {
		"SearchCriteria"    : $('#search_box').val().toString()
	}

	if($('#matl_type').val() != null && $('#matl_type').val().length > 0){
		SetRanges["cuKTAST.KTAST"] = "X";
		Ranges["cuKTAST.KTAST_VALUES"] = { "VAL": $('#matl_type').val() };
	}
	else{
		SetRanges["cuKTAST.KTAST"] = "";	
	}

	if($('#matl_group').val() != null && $('#matl_group').val().length > 0){
		SetRanges["cuKTAER.KTAER"] = "X";
		Ranges["cuKTAER.KTAER_VALUES"] = { "VAL": $('#matl_group').val() };
	}
	else{
		SetRanges["cuKTAER.KTAER"] = "";	
	}

	searchArr["SetRanges"] =  SetRanges;
	searchArr["Ranges"] =  Ranges;
	if(simples == 1){
		searchSalesActivity($('#search_box').val().toString(), 0);
		
	}
	else{
		searchSalesActivity(searchArr, 1);
	}
	if(where == 1) $( "#advancedSearchDialog" ).dialog( "close" )
}

function searchSalesActivity(searchTerm, advancedS){

	var sales_activity_param;
	$('#loadingBar').fadeIn(function(){
		if(advancedS == 1){
			cordova.exec(fireSalesList, execFailed, 'SalesActivity', 'searchSalesActivityAdvanced', [searchTerm]);
		}
		else{
			cordova.exec(fireSalesList, execFailed, 'SalesActivity', 'searchSalesActivity', [searchTerm]);
		}
	});


	function fireSalesList(param) {

		var result = eval('(' + param + ')');
		sales_activity_param = param;
		var contactList = new Array();
		$("#pageAside ul").html('');
		for(i = 0; i < result.length; i++){ 
			if (result[i]['CONTACT'] != 0) 
			{
				contactList.push(result[i]['CONTACT']);
			}
		}

		if ( result.length == 0 )
		{
			if (create_operation == "")
			{
				$(".customers_current").filter(":visible").fadeOut();
				$(".quickNav, #pageDownNav").fadeOut();
			}
		}
		else{
			$(".quickNav, #pageDownNav").fadeIn();
		}

		var uniquecontactList = [];
		$.each(contactList, function(i, el){
		    if($.inArray(el, uniquecontactList) === -1) uniquecontactList.push(el);
		});

		if (uniquecontactList.length != 0) {
			cordova.exec(fireContactDetailsList, execFailed, 'Contact', 'getContactDetailsList', uniquecontactList);

			function fireContactDetailsList (paramContact){
				var resultContact = eval('([' + paramContact + '])');
				fireSalesListComplete(sales_activity_param,resultContact);
			}			
		}
		else
			fireSalesListComplete(param,uniquecontactList);

		// Set All Activities as active.
		$('#aside_nav_activities a').removeAttr('class');
		$('#all_activities').addClass('active');
		$("#pageAside").off("scroll");
	}


	function fireSalesListComplete(param, contactList) {
		function record(doc_num, doc_num2, date, from_time, to_time, type, customerId, cname, ord, ret, exc, indication, indication2,cusName,activityFavourite,showFavourite,salesOrdNo,contact_name){
			var indicator = '';
			var salesOrdNo = salesOrdNo.toString();
			//TODO
			if ( indication != ""){
				indicator += '<span class="listViewFlag"></span>';
			}
			var clientRecord = "";
			clientRecord += '<li><table cellspacing="0" cellpadding="0" border="0" class="sa_table" data-dnr="'+doc_num2+'"><tbody><tr><td colspan="2" id="sa_'+doc_num+'" class="sale_activity1">';
       		clientRecord += indicator + "<span>" + cname+ "</span>";
			clientRecord += '</td> <td>';
			// remove favorites icon */
			/*clientRecord += '<div id="salesActStarDivList" data-doc_num="' + doc_num +   '"class="item_star ' + activityFavourite +'" style="visibility:'  +  showFavourite  +'">';
			clientRecord += '<svg id="salesActStarSvg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" fill="#333" width="30px" height="30px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510; fill:grey;" xml:space="preserve">';
			clientRecord += '<polygon id="salesActStar" points="255,402.212 412.59,497.25 370.897,318.011 510,197.472 326.63,181.738 255,12.75 183.371,181.738 0,197.472 139.103,318.011 97.41,497.25"/>';
			clientRecord += '</svg>';
			clientRecord += '</div> </td>';*/

			clientRecord += '</tr><tr><td colspan="2">'+cusName+'</td></tr><tr><td style="width:100%;" data-doc_num="'+doc_num2+'" data-kunnr="'+customerId+'" id="ty_'+doc_num+'" class="sale_activity3 sa_number">';
			clientRecord += type;
			clientRecord += '</td><td style="text-align:right;">' + indication2 + '</tr><tr><td colspan="2" class="sale_activity3 suba_dets" data-suba_ord="'+ord+'" id="suba_'+doc_num+'">';
			clientRecord += date;
			clientRecord += '</td></tr>' + '<tr><td><a style="font-size:12px;" href="#" x-apple-data-detectors="false">' + salesOrdNo + '</a></td></tr><tr><td>'+contact_name +'</td></tr></tbody></table></li>';
			return clientRecord;
		};

		var result = eval('(' + param + ')');
		var listCount = 0;
		$("#pageAside ul").html('');
		//for(i = 0; i < result.length; i++){
		var no_results = 1;
		for(i = 0; i < result.length; i++){
			//getActFavourite(result , i , function(result, i, isActivityFavourite){
				listCount++;
				no_results = 0;
				//alert(result[i]["DOC_NUMBER"]);
				var date = formatDate(result[i]["FROM_DATE"]);
				var from_time = result[i]["FROM_TIME"];
				var to_time = result[i]["TO_TIME"];
				var show_fav;// = result[i]["DOC_NUMBER"].toString().charAt(0) != "*" ? "visible" : "hidden";
				var doc_num = result[i]["DOC_NUMBER"].replace("*", "");
				var doc_num2 = result[i]["DOC_NUMBER"].toString();
				var type = result[i]["ACTIVITY_TYPE_TEXT"];
				var customer_name = result[i]['ACTIVITY_COMMENT'];	
				var cusName = result[i]['NAME1'];	
				var indication = result[i]['VBELV_SD'];	
				var salesOrdNo = result[i]['ORDER_NUMS'];
				var ord = result[i]['ORDER_NUMS'];
				if(customer_name == undefined){
					customer_name = "-";
				}

				
				show_fav = "hidden";

				if(customer_name.length > 40){
					var cname = customer_name.substring(0,40);
				}
				else{
					var cname = customer_name;
				}

				var indication2 = "";
				if(result[i]["ORDER_NUMS"] != ""){
					
					indication2 = '<img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDI2IDI2IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNiAyNiIgd2lkdGg9IjE2cHgiIGhlaWdodD0iMTZweCI+CiAgPHBhdGggZD0ibS4zLDE0Yy0wLjItMC4yLTAuMy0wLjUtMC4zLTAuN3MwLjEtMC41IDAuMy0wLjdsMS40LTEuNGMwLjQtMC40IDEtMC40IDEuNCwwbC4xLC4xIDUuNSw1LjljMC4yLDAuMiAwLjUsMC4yIDAuNywwbDEzLjQtMTMuOWgwLjF2LTguODgxNzhlLTE2YzAuNC0wLjQgMS0wLjQgMS40LDBsMS40LDEuNGMwLjQsMC40IDAuNCwxIDAsMS40bDAsMC0xNiwxNi42Yy0wLjIsMC4yLTAuNCwwLjMtMC43LDAuMy0wLjMsMC0wLjUtMC4xLTAuNy0wLjNsLTcuOC04LjQtLjItLjN6IiBmaWxsPSIjOTFEQzVBIi8+Cjwvc3ZnPgo="/>';
				}

				var contact_name = "";
				for (var j = 0; j < contactList.length; j++) {
					if (contactList[j]['PARNR'] == result[i]["CONTACT"]) 
						contact_name = contactList[j]['NAMEV'] +' '+ contactList[j]['NAME1'];
				}
				if (contact_name == "")
				 	contact_name = lang.no_contact;			 
			 	
				if (ord == "") {
					ord = result[i]['VBELV_SD'];
				}

			 	$("#pageAside ul").html($("#pageAside ul").html() + record(doc_num, doc_num2, date, from_time, to_time, type, result[i]["PARTN_ID"], cname, ord, result[i]["RETURN_NUMS"], result[i]["EXCHANGE_NUMS"], indication, indication2,cusName,"",show_fav,ord,contact_name));

			 	if (listCount == result.length-1)
			 	{
			 		$('#loadingBar').fadeOut(function(){
						$("#pageAside ul li:eq(0)").click();
					});
			 	}

			//});

			
		}
		$('#loadingBar').fadeOut(function(){
			$("#pageAside ul li:eq(0)").click();
		});
		//Scroll to the first record:
		if ( $("#pageAside .active").length > 0 ) {
			$("#pageAside").scrollTo("#pageAside .active", 300);
		}
	}
}

$(document).ready(function(){
	$('.advancedSearchButtonHolder').on('click', function(){
		if($('.ui-dialog').css('display') == "block") {
			$('.ui-dialog-buttonset').find('button:eq(0)').trigger('click');
			return false;
		}
		if($('.advancedSearchButtonHolder').css('fill') != "#ff0000"){
			$('.advancedSearchButtonHolder').css('fill', '#000000');
			$('#gwt_from').val('');
			$('#gwt_to').val('');
			$('#price_from').val('');
			$('#price_to').val('');
			
			var aaa = advSearchForm.getElementsByTagName('select');
			
			for(var i = 0; i < aaa.length; i++) {
			  aaa[i].selectedIndex =0;
			} 
		}
		$('.advancedSearchButtonHolder').css('background-color', '#ffffff');
		var buttonsOpts = {};
		buttonsOpts[lang.cancelButton] = function() {
			$( "#advancedSearchDialog" ).dialog( "close" );
			if($('.advancedSearchButtonHolder').css('fill') != "#ff0000"){
				$('.advancedSearchButtonHolder').css('fill', '#ffffff');
			}
			$('.advancedSearchButtonHolder').css('background-color', 'transparent');
		};
		buttonsOpts["Reset"] = function() {
			resetAdvancedSearch(69);
			$('.advancedSearchButtonHolder').css('fill', '#ffffff');
			$('.advancedSearchButtonHolder').css('background-color', 'transparent');
		};

		buttonsOpts[lang.applyText] = function() {
			searchPP('', 1);
		};
		$.ui.dialog.prototype._focusTabbable = function(){};
		$( "#advancedSearchDialog" ).dialog({
			resizable: false,
			height: 250,
			width: 590,
			modal: true,
			buttons : buttonsOpts,
			show: 'fade',
			hide: "fade",
			title: "Filter"
		});	
		$( "#advancedSearchDialog" ).parent().css('top', '74px');
		$( "#advancedSearchDialog" ).parent().css('left', '294px');
		$('.ui-widget-overlay').on('click', function(){ $('.ui-dialog-buttonset button:eq(0)').trigger('click'); });

	    $('.ui-dialog-buttonset button:eq(0)').css('color', 'white');
	    $('.ui-dialog-buttonset button:eq(0)').css('background', 'red');
	    $('.ui-dialog-buttonset button:eq(0) span').css('padding', '.2em 0.5em');

	    $('.ui-dialog-buttonset button:eq(1)').css('color', 'white');
	    $('.ui-dialog-buttonset button:eq(1)').css('background', 'red');
		$('.ui-dialog-buttonset button:eq(1) span').css('padding', '.2em 0.5em');

	    $('.ui-dialog-buttonset button:eq(2)').css('color', 'white');
	    $('.ui-dialog-buttonset button:eq(2)').css('background', '#008dbc');
	    $('.ui-dialog-buttonset button:eq(2) span').css('padding', '.2em 0.5em');

	});

   $(".fixedScrollBox").css({
		"height": $('body').height() - ($("#pageHeader").height()+1) - ($("#aside_nav_activities").height() + 45)
	});
});


function removeReminderSalesActivity(salesDocNo,forSalesAct)
{
	//Remove Reminder
	if (forSalesAct) 
		{}// Do nothing text cleadred in callingmethod
	else
		$("#salesDoc_ReminderDate").text("");

	cordova.exec(function(){
		            var errorDict = {};
					errorDict[lang.succ_del_reminder] = "success";
			        customAlert(errorDict);
				}, execFailed, 'Resubmission', 'deleteResub', [salesDocNo]);
}


function addReminderDateSalesActivity(vbeln,remindDate,act_type)
{
	// Append Reminder Date
	var resub_array = {
			"BUKRS"	 		: resubBUKRS,
			"RESUB_TYP" 	: act_type,
			"VBELN"     	: vbeln,
			"UNAME_AD"  	: current_username.toUpperCase(),
			"UNAME" 		: current_username.toUpperCase(),
			"RESUB_DATE"	: remindDate	
		};

		cordova.exec(function(){			
			var errorDict = {};
			errorDict[lang.succ_add_reminder] = "success";
			customAlert(errorDict);
	 }, execFailed, 'Resubmission', 'createResub', [resub_array]);	
}

// function addReminderSalesActivity(salesActivityNo,self,fromList,fromSalesAct,act_type)
// {	
// 	var salesActDocNo = salesActivityNo;
// 	var self = self;
// 	var fromList = fromList;
// 	var act_type = act_type;

// 	var resub_array = {
// 			"BUKRS"	 		: resubBUKRS,
// 			"RESUB_TYP" 	: act_type,
// 			"VBELN"     	: salesActDocNo,
// 			"UNAME_AD"  	: current_username.toUpperCase(),
// 			"UNAME" 		: current_username.toUpperCase(),
// 			"RESUB_DATE"	: reminderDate	
// 		};

// 	//cordova.exec(function(){			

// 		$('#add_reminder_date').val(reminderDate);
// 		var buttonsOpts = {};
// 		buttonsOpts[lang.confirm] = function() {

// 			remDate = new Date($('#add_reminder_date').val());
// 			if(remDate == 'Invalid Date')
// 				return;
			
// 			addReminderDateSalesActivity(salesActDocNo, $('#add_reminder_date').val(),act_type);
// 			$( "#selectDateReminder" ).dialog( "close" );

// 			// Mark Reminder Star
// 			$(self).addClass('favorite_activity');

// 			if (fromList) 
// 			{
// 				if ($(self).parent().closest('li').hasClass('active')) {
// 					$('#sales_activities #salesActStarDivDtls').addClass('favorite_activity');
// 					$("#actReminderDate").html(lang.SalesOrderDetails_titles[7] + " " + formatDate($('#add_reminder_date').val().replace(/-/g,"")));
// 				}
// 			}
// 			else
// 			{
// 				if (fromSalesAct) 
// 				{
// 					$('#salesActivityList li.active #salesActStarDivList').addClass('favorite_activity');
// 					$("#actReminderDate").html(lang.SalesOrderDetails_titles[7] + " " + formatDate($('#add_reminder_date').val().replace(/-/g,"")));

// 				}
// 				else
// 				{
// 					// Show newly created reminder date
// 					$('#salesDoc_ReminderDate').text( formatDate($('#add_reminder_date').val().replace(/-/g,"")));
// 				}
// 			}
			
// 		};
// 		buttonsOpts[lang.cancelButton] = function() {
// 			$( "#selectDateReminder" ).dialog( "close" );
// 		};

// 		$( "#selectDateReminder" ).dialog({
// 			resizable: false,
// 			height: 250,
// 			width: 450,
// 			modal: true,
// 			buttons : buttonsOpts,
// 			hide: "fade",
// 			show:  500 ,
// 			title: lang.addReminderDate
// 		});
// 		$('#add_reminder_date').blur();

// 	 //}, execFailed, 'Resubmission', 'createResub', [resub_array]);	

// }


/*$(document).on("click", '#sales_activities #salesActStarDivDtls', function () {

	var self = this;
	var salesActivityNo = $(self).attr('data-doc_num');
	if ($(self).hasClass('favorite_activity')) {
		$(self).removeClass('favorite_activity');
		$('#salesActivityList li.active #salesActStarDivList').removeClass('favorite_activity');
		removeReminderSalesActivity(salesActivityNo,true);
		$("#actReminderDate").html(lang.SalesOrderDetails_titles[7] + " " + "");
	} else {
		// $(self).addClass('favorite_activity');
		// $('#salesActivityList li.active #salesActStarDivList').addClass('favorite_activity');

		addReminderSalesActivity(salesActivityNo,self,false,true,resub_TYP_Act);
	}	

});
/** disabled functions for favorites */
/*$(document).on("click", '#salesActivityList #salesActStarDivList', function () {

	var self = this;
	var salesActivityNo = $(self).attr('data-doc_num');
	if ($(self).hasClass('favorite_activity')) {

		$(self).removeClass('favorite_activity');
		
		if ($(this).parent().closest('li').hasClass('active')) // If the selected list item favourite is removed remove the star from Details screen
		{
			$('#sales_activities #salesActStarDivDtls').removeClass('favorite_activity');
			$("#actReminderDate").html(lang.SalesOrderDetails_titles[7] + " " + "");
		}
				
		removeReminderSalesActivity(salesActivityNo,true);

	} else {
		// $(self).addClass('favorite_activity');
		// if ($(this).parent().closest('li').hasClass('active')) {
		// 	$('#sales_activities #salesActStarDivDtls').addClass('favorite_activity');			
		// }
		addReminderSalesActivity(salesActivityNo,self,true,true,resub_TYP_Act);
	}	

});

$(document).on("click", "#customers_salesDocuments #salesDocStarDivDtls" ,function () {
	
	var self = this;
	var salesDocNo = $(self).attr('data-doc_num');
	if ($(self).hasClass('favorite_activity')) {
			$(self).removeClass('favorite_activity');	
			removeReminderSalesActivity(salesDocNo,false);	

	} else {
		//$(self).addClass('favorite_activity');	
		addReminderSalesActivity(salesDocNo,self,false,false,resub_TYP_Doc);	
	}	

});*/

// function addValiditySalesDocument(currentSalesDocDetails)
// {
// 	var currentSalesDocDetailsClone = jQuery.extend({}, currentSalesDocDetails);

// 	currentSalesDocDetailsClone.SalesOrderHeader = currentSalesDocDetailsClone.Header;
// 	currentSalesDocDetailsClone.SalesOrderItems = currentSalesDocDetailsClone.Items;

// 	delete currentSalesDocDetailsClone.Header;
//  	delete currentSalesDocDetailsClone.Items;
//  	if (currentSalesDocDetailsClone.SalesOrderHeader.DOC_TYPE != "AG")
//  	{
//  		cordova.exec(function(){
// 			var errorDict = {};
// 		  	errorDict[lang.succ_update_sd] = "success";
// 	        customAlert(errorDict);
// 	        getSalesOrderDetails(currentSalesDocDetails["Header"].DOC_NUMBER,0);
		
// 		}, execFailed, 'Order', "updateSalesOrderValidity", [currentSalesDocDetailsClone]);
//  	}
//  	else
//  	{
//  		// Update Quotations
//  		cordova.exec(function(){
// 			var errorDict = {};
// 		  	errorDict[lang.succ_update_sd] = "success";
// 	        customAlert(errorDict);
// 	        getSalesOrderDetails(currentSalesDocDetails["Header"].DOC_NUMBER,1);		
// 		}, execFailed, 'OpenQuotation', "updateQuotationValidity", [currentSalesDocDetailsClone]);

//  	}

// };


// $(document).on("click", "#customers_salesDocuments #salesDoc_ValidityDate" ,function () {

// 	var salesDocValidity = new Date($('#salesDoc_ValidityDate').html());

// 	var validTo_day = salesDocValidity.getDate();
//     var validTo_month = salesDocValidity.getMonth() + 1;
//     var validTo_year = salesDocValidity.getFullYear();

//     if (validTo_month < 10) validTo_month = "0" + validTo_month;
//     if (validTo_day < 10) validTo_day = "0" + validTo_day;
//     var validDefault = validTo_year + "-" + validTo_month + "-" + validTo_day;

//     var salesDocDateUpdate ;

// 	var buttonsOpts = {};
// 	buttonsOpts[lang.confirm] = function() {

// 		salesDocValidity = new Date($("#add_validity_to").val());
// 		if(salesDocValidity == 'Invalid Date')
// 			return;
// 		validTo_day = salesDocValidity.getDate();
// 	    validTo_month = salesDocValidity.getMonth() + 1;
// 	    validTo_year = salesDocValidity.getFullYear();

// 	    if (validTo_month < 10) validTo_month = "0" + validTo_month;
//     	if (validTo_day < 10) validTo_day = "0" + validTo_day;

// 	    salesDocDateUpdate = validTo_year.toString() + validTo_month.toString() + validTo_day.toString();
		
// 		$( "#selectValidityTo" ).dialog( "close" );
// 		currentSalesDocDetails.Header["QT_VALID_T"] = salesDocDateUpdate;
// 		if (currentSalesDocDetails.Header["QT_VALID_F"] == "") 
// 		{
// 			currentSalesDocDetails.Header["QT_VALID_F"] = currentSalesDocDetails.Header["PURCH_DATE"]
// 		}
// 		addValiditySalesDocument(currentSalesDocDetails);
// 	};

// 	buttonsOpts[lang.cancelButton] = function() {		
// 		$( "#selectValidityTo" ).dialog( "close" );
// 	};

// 	$( "#selectValidityTo" ).dialog({
// 		resizable: false,
// 		height: 250,
// 		width: 450,
// 		modal: true,
// 		buttons : buttonsOpts,
// 		hide: "fade",
// 		show:  500 ,
// 		title: lang.changeSalesDocValidity
// 	});

//     $("#add_validity_to").val(validDefault);
// 	$('#add_validity_to').blur();	

// });

// function getActFavourite(result, i, callback){			

// 	cordova.exec(function(favParam){
// 		var favResult = eval('(' + favParam + ')');
// 		var isActivityFavourite;
// 		if(favResult.length > 0 ) 
// 			isActivityFavourite  = "favorite_activity";
// 		else
// 			isActivityFavourite = "";
// 		callback(result, i, isActivityFavourite);					

// 	}, execFailed, 'Resubmission', 'getResubDetails', [ result[i]["DOC_NUMBER"] ]);
// }

function getSalesActivitesList(where,my_activities){
	/** remove my/all sales activities*/
	var get_my_activities;
	// if (typeof(my_activities) == "undefined")  
	// {
	// 	get_my_activities = true; 
	// }
	// else
	// 	get_my_activities = my_activities;
	/** end changes*/
	// $('#all_activities').removeClass();
	// $('#my_activities').addClass('active');

	resetAdvancedSearch(1);
	if(where == "1"){
		return;
	}
	//cordova.exec(fireSalesList, execFailed, 'Customizing', 'getSalesDocTypeList', ['']);
	var customer_id = $('#customer_id').val();
	var sales_activity_param;
		var create_operation = $('#create_operation').val();
		//alert(customer_id +'--' + create_operation);
	var the_id = '';
	if(where == "get100more"){
		var currentNumber = parseInt($("#pageAside ul li").length);
		 if(currentNumber == 0){
		// 	$("#pageAside").off("scroll");
		 	return false;
		 }
		var the_id = [currentNumber, currentNumber + 50];
		var method = 'getSalesActivityList';
		get_all_activities = true;
	}
	else if(customer_id == "0"){
		var the_id = [0, 50];
		var method = 'getSalesActivityList';
	}
	else{
		var the_id = [customer_id];
		var method = 'getSalesActivityListByCustomerNr';
	}
	get_all_activities = true;
	/** remove my/all sales activities*/
	// if (get_my_activities == true && method != "getSalesActivityListByCustomerNr")
	// {
	// 	if ( get_all_activities == false )
	// 		cordova.exec(fireSalesList, execFailed, 'SalesActivity' , 'searchSalesActivityByUNAME' , [current_username]);
	// 	else
	// 		cordova.exec(fireSalesList, execFailed, 'SalesActivity' , method, the_id);
	// }
	// else
	// {
		cordova.exec(fireSalesList, execFailed, 'SalesActivity' , method, the_id);
	// }
	/*end of changes */

	function fireSalesList(param) {

		var result = eval('(' + param + ')');

		sales_activity_param = param;
		var contactList = new Array();
		// $("#pageAside ul").html('');
		for(i = 0; i < result.length; i++){ 
			if (result[i]['CONTACT'] != 0) 
			{
				contactList.push(result[i]['CONTACT']);
			}
		}

		if ((where == "get100more" && ((result.length % 50)) != 0) || (method == 'getSalesActivityListByCustomerNr') || (get_my_activities == true)) {
			$("#pageAside").off("scroll");
		}
		else
		{
			if (jQuery._data( $("#pageAside")[0], "events" ) == undefined) 
			{
				$("#pageAside").on("scroll", function(){
			    	if( ($(this).scrollTop() + $(this).innerHeight()) == $(this).get(0).scrollHeight){
			    		getSalesActivitesList("get100more",false);
			    	}
		    	});
			}
		}

		if ( result.length == 0 )
		{
			if (create_operation == "")
			{
				// $(".customers_current").filter(":visible").fadeOut();
				// $(".quickNav, #pageDownNav").fadeOut();
				$('#loadingBar').fadeOut();
				return;
			}			
		}
		else{
			$("#pageAside ul").html('');
			$(".quickNav, #pageDownNav").fadeIn();
		}

		var uniquecontactList = [];
		$.each(contactList, function(i, el){
		    if($.inArray(el, uniquecontactList) === -1) uniquecontactList.push(el);
		});

		$('#loadingBar').fadeIn( function(){

			if (uniquecontactList.length != 0) {
				cordova.exec(fireContactDetailsList, execFailed, 'Contact', 'getContactDetailsList', uniquecontactList);

				function fireContactDetailsList (paramContact){
					var resultContact = eval('([' + paramContact + '])');
					fireSalesListComplete(sales_activity_param,resultContact);
				}			
			}
			else
				fireSalesListComplete(param,uniquecontactList);
		});
	}
	
	function fireSalesListComplete(param,contactList) {
		
		function record(doc_num, doc_num2, date, from_time, to_time, type, customerId, cname, ord, ret, exc, indication, indication2,cusName,activityFavourite, showFavourite,salesOrdNo,contact_name){
			var indicator = '';
			var salesOrdNo = salesOrdNo.toString();
			//TODO
			if ( indication != ""){
				indicator += '<span class="listViewFlag"></span>';
			}
			var clientRecord = "";
			clientRecord += '<li><table cellspacing="0" cellpadding="0" border="0" class="sa_table" data-dnr="'+doc_num2+'"><tbody><tr><td colspan="1" id="sa_'+doc_num+'" class="sale_activity1">';
       		clientRecord += indicator + "<span>" + cname+ "</span>";
			clientRecord += '</td> <td>';
			/** changes to hide the favorites*/
			/*clientRecord += '<div id="salesActStarDivList" data-doc_num="' + doc_num +   '" class="item_star ' + activityFavourite +'" style="visibility:'  +  showFavourite  +'">';
			clientRecord += '<svg id="salesActStarSvg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" fill="#333" width="30px" height="30px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510; fill:grey;" xml:space="preserve">';
			clientRecord += '<polygon id="salesActStar" points="255,402.212 412.59,497.25 370.897,318.011 510,197.472 326.63,181.738 255,12.75 183.371,181.738 0,197.472 139.103,318.011 97.41,497.25"/>';
			clientRecord += '</svg>';
			clientRecord += '</div> </td>';*/

			clientRecord += '</tr><tr><td colspan="2">'+cusName+'</td></tr><tr><td data-doc_num="'+doc_num2+'" data-kunnr="'+customerId+'" id="ty_'+doc_num+'" class="sale_activity3 sa_number">';
			clientRecord += type;
			clientRecord += '</td><td style="text-align:right; padding-right: 20px">' + indication2 + '</tr><tr><td colspan="2" class="sale_activity3 suba_dets" data-suba_ord="'+ord+'" id="suba_'+doc_num+'">';
			clientRecord += date;
			clientRecord += '</td></tr>' + '<tr><td><a style="font-size:12px;" href="#" x-apple-data-detectors="false">' + salesOrdNo + '</a></td></tr><tr><td>'+ contact_name +'</td></tr></tbody></table>';
			clientRecord += '</li>';
			return clientRecord;
		};

		var result = eval('(' + param + ')');
		var listCount = 0;
		$("#pageAside ul").html('');

		var no_results = 1;
		var my_customer_count = 0;
		for(i = 0; i < result.length; i++){
			if ( get_all_activities == false && method != 'getSalesActivityListByCustomerNr')
				if (result[i]['CREATED_BY'].toUpperCase() != current_username.toUpperCase())
				{
					my_customer_count++;
					continue;
				}
			
			 	//getActFavourite(result , i , function(result, i, isActivityFavourite){
					listCount++;
					no_results = 0;
					//alert(result[i]["DOC_NUMBER"]);
					var date = formatDate(result[i]["FROM_DATE"]);
					var from_time = result[i]["FROM_TIME"];
					var to_time = result[i]["TO_TIME"];
					var show_fav;// = result[i]["DOC_NUMBER"].toString().charAt(0) != "*" ? "visible" : "hidden";
					var doc_num = result[i]["DOC_NUMBER"].replace("*", "");
					var doc_num2 = result[i]["DOC_NUMBER"].toString();
					var type = result[i]["ACTIVITY_TYPE_TEXT"];
					var customer_name = result[i]['ACTIVITY_COMMENT'];
					var cusName = result[i]['NAME1'];
					var indication = result[i]['VBELV_SD'];	
					var salesOrdNo = result[i]['ORDER_NUMS'];
					var ord = result[i]['ORDER_NUMS'];
					
					
					show_fav = "hidden";

					if(customer_name == undefined){
						customer_name = "-";
					}

					if(customer_name.length > 40){
						var cname = customer_name.substring(0,40);
					}
					else{
						var cname = customer_name;
					}

					var indication2 = "";
					if(result[i]["ORDER_NUMS"] != ""){

						indication2 = '<img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDI2IDI2IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAyNiAyNiIgd2lkdGg9IjE2cHgiIGhlaWdodD0iMTZweCI+CiAgPHBhdGggZD0ibS4zLDE0Yy0wLjItMC4yLTAuMy0wLjUtMC4zLTAuN3MwLjEtMC41IDAuMy0wLjdsMS40LTEuNGMwLjQtMC40IDEtMC40IDEuNCwwbC4xLC4xIDUuNSw1LjljMC4yLDAuMiAwLjUsMC4yIDAuNywwbDEzLjQtMTMuOWgwLjF2LTguODgxNzhlLTE2YzAuNC0wLjQgMS0wLjQgMS40LDBsMS40LDEuNGMwLjQsMC40IDAuNCwxIDAsMS40bDAsMC0xNiwxNi42Yy0wLjIsMC4yLTAuNCwwLjMtMC43LDAuMy0wLjMsMC0wLjUtMC4xLTAuNy0wLjNsLTcuOC04LjQtLjItLjN6IiBmaWxsPSIjOTFEQzVBIi8+Cjwvc3ZnPgo="/>';

					}

					var contact_name = "";
					for (var j = 0; j < contactList.length; j++) {
						if (contactList[j]['PARNR'] == result[i]["CONTACT"]) 
							contact_name = contactList[j]['NAMEV'] +' '+ contactList[j]['NAME1'];
					}
					if (contact_name == "")
					 	contact_name = lang.no_contact;

					 if(ord == ""){
					 	ord = result[i]['VBELV_SD'];
					 }
					 $("#pageAside ul").html($("#pageAside ul").html() + record(doc_num, doc_num2, date, from_time, to_time, type, result[i]["PARTN_ID"], cname, ord, result[i]["RETURN_NUMS"], result[i]["EXCHANGE_NUMS"], indication, indication2,cusName,"",show_fav,ord,contact_name));	

			 		
 			 		if (get_all_activities == false ) {
				 		if ((my_customer_count + listCount) == result.length) 
				 		{
				 			get_all_activities = false;
	 						$('#loadingBar').fadeOut(function(){
								if(where != "get100more" && create_operation != 'x'){
									$("#pageAside ul li:eq("+must_be_clicked+")").click();

								}
							});
				 		}
				 		
				 	}

			 		if (listCount == result.length)
				 	{

				 		var isFromCustomersSalesAct_id = getParams().salesActivityDocNumber;
				 		var isFromReminder = getParams().fromReminder;

						if (isFromCustomersSalesAct_id != undefined){
							$("#pageAside ul li").each(function(index, el){
								if(isFromCustomersSalesAct_id == $(el).find("table").data("dnr") || isFromCustomersSalesAct_id.replace('*','') == $(el).find("table").data("dnr")){
									must_be_clicked = index;
									$("#pageAside").scrollTop(($(el).position().top) - $(el).height());
									var ecust = getParams().from_customer;
									if(ecust != "undefined"){
										if (isFromReminder != undefined)
											$("#back_customersSalesActivities, #back_but").hide();
										else
											$("#back_customersSalesActivities, #back_but").show();
									}
								}
							});
						}
						else{
							$("#back_customersSalesActivities, #back_but").hide();
						}

				 		$('#loadingBar').fadeOut(function(){
							if(where != "get100more" && create_operation != 'x'){
								$("#pageAside ul li:eq("+must_be_clicked+")").click();
								get_all_activities = false;
							}
						});
				 	}


				//});			
			
		}

		
		if(no_results==1){
			//$("#pageContent").fadeIn();
			//$('#add_sacu').parent().parent().hide();
			//$('#but_general').hide()
			$('.create_order_but').hide();
			$(".quickNav").fadeOut();
			//$("#quick_createSA").click();
		}
		if (no_results==1 && result.length < 1) 
		{
			$('#loadingBar').fadeOut();
		}
		//alert($("#pageAside ul li").length);
				
		//alert($("#pageAside ul li").length);
				
		//if(("#pageAside ul li").length>0) $('#loadingBar').fadeOut(500);
		//$('#loadingBar').fadeOut(500);
		//Check if it's comming from Customers - Sales Activities:
		// var isFromCustomersSalesAct_id = getParams().salesActivityDocNumber;

		// if (isFromCustomersSalesAct_id != undefined){
		// 	$("#pageAside ul li").each(function(index, el){
		// 		if(isFromCustomersSalesAct_id == $(el).find("table").data("dnr") || isFromCustomersSalesAct_id.replace('*','') == $(el).find("table").data("dnr")){
		// 			must_be_clicked = index;
		// 			$("#pageAside").scrollTop(($(el).position().top) - $(el).height());
		// 			var ecust = getParams().from_customer;
		// 			if(ecust != "undefined"){
		// 				$("#back_customersSalesActivities, #back_but").show();
		// 			}
		// 		}
		// 	});
		// }
		// else{
		// 	$("#back_customersSalesActivities, #back_but").hide();
		// }
		$("#delete_but").on("click", function(){
			var buttonsOpts = {};
			buttonsOpts[lang.cancelButton] = function() {
				$( "#deleteSA_confirm" ).dialog( "close" );
			};
			buttonsOpts[lang.confirm] = function() {
				cordova.exec(function(){
		            //Go Back to Customers:
					window.location = "customers.html?salesAct_customeridDelete=" + $("#pageAside li.active .sa_number").data("kunnr");
				}, execFailed, 'SalesActivity', 'deleteSalesActivity', [$("#pageAside li.active table").data('dnr')]);
			};
			$( "#deleteSA_confirm" ).dialog({
				resizable: false,
				height: 250,
				width: 450,
				modal: true,
				buttons : buttonsOpts,
				show: 'fade',
				hide: "fade",
				title: lang.deleteSalesDoc_title
			});
		});
		$("#back_customersSalesActivities, #back_but").on("click", function(){
			if(getParams().fromSalesDocuments != "true")
				window.location = "customers.html?backCustomerFromSA=" + $("#pageAside li.active .sa_number").data("kunnr");
			else
				window.location = "customers.html?salesAct_customerid=" + $("#current_customer_name").data("kunnr") + "&salesOrderDocNumberReminder=" + $("#salesActivityList >li.active .sale_activity3.suba_dets").data("suba_ord") + "&fromReminder=true" ;

		});

		// if(where != "get100more" && create_operation != 'x'){
		// 	$("#pageAside ul li:eq("+must_be_clicked+")").click();
		// }
				
		//scroll active customer to top
		/*if($('#pageAside .active').length > 0) {
				 $("#pageAside").scrollTop(($('#pageAside .active').position().top) - $('#pageAside .active').height());
				 $("#pageAside .active a").click();
				 }
				 
				 var activeArrow = '<span class="active_arrow"></span>';
				 $("#pageNav ul li:eq(1)").append(activeArrow);*/
		//if(editting == "-1"){
						
		//}
				//alert(create_operation);
		if (create_operation == 'x'){
			//$('.create_order_but').show();
			$('#quick_createSA').click();
			$('#create_operation').val('');
			$("#pageAside .active").removeClass('active');
		}

		//Scroll to the first record:
		if ( $("#pageAside .active").length > 0 ) {
			$("#pageAside").scrollTo("#pageAside .active", 300);
		}
	}
}

function navigateToCustomer(sourceElement)
{
	var customer_id = sourceElement.attributes["data-kunnr"].value;
	window.location = "customers.html?salesAct_customerid=" + customer_id ;

}

function getSaleActivityDetails(saleId){
	
    cordova.exec(fireSaleActivityDetails, execFailed, 'SalesActivity', 'getSalesActivityDetails', [saleId]);

	function set_customer_name (customer, sa_type, resultat, contact, status,customer_id){
		var customer_name = customer;
		cordova.exec(fireContactDetails, execFailed, 'Contact', 'getContactDetails', [contact]);
		cordova.exec(fireGetResult, execFailed, 'Customizing', 'getKTAERList', ['']);

		var gray_field = sa_type + '<br/><span id="current_customer_name" data-kunnr="' + customer_id + '"   >' + customer + '</span><br/>';
			
		function fireContactDetails (param){
			var result = eval('(' + param + ')');
			if(typeof result['NAMEV'] != "undefined"){
				var contact_name = result['NAMEV'] +' '+ result['NAME1'];
			}
			else{
				var contact_name = lang.no_contact;
			}
						
			gray_field += contact_name + '<br/>'+ status + '<br/>';
		}
			
		function fireGetResult (param){
			var result = eval('(' + param + ')');
			for(i = 0; i < result.length; i++){
				if(result[i]["KTAER"] == resultat){
					gray_field += result[i]["VTEXT"];
					break;
				}
			}
			$('#place').find("span").html(gray_field);

			// Code for navigating to Customer from Sales Activity
			if (customer_name != undefined)
			{
				if ($("#back_customersSalesActivities").is(":visible")) 
				{
					// Dont set the navigation
					$("#current_customer_name").removeClass("customerNavigation");
					
				}
				else
				{
					// Set the customer Navigation
					$("#current_customer_name").addClass("customerNavigation");
					$(".customerNavigation").click(function(){
 						navigateToCustomer(this);
					});
				}


			}
		}
	}
		
	function fireSaleActivityDetails(param){
		//todo
		//alert(param)
		$('.show_ref_sd').hide();
		var result = eval('(' + param + ')');

		$("#sales_activities").fadeOut(function(){
			if(result["CREATED_BY"] != ""){
				$('#cfu').show();
				$('#sales_activities_edit').show();
			}
			else{
				$('#cfu').hide();
				if(result["DOC_NUMBER"].toString().charAt(0) != "*" ){
					$('#sales_activities_edit').hide();
				}
			}
		//$(".customers_current").filter("visible").fadeOut(function(){
			var from_date_s = result["FROM_DATE"].toString();
			var converted_date = from_date_s.charAt(0)+from_date_s.charAt(1)+from_date_s.charAt(2)+from_date_s.charAt(3)+'-'+from_date_s.charAt(4)+from_date_s.charAt(5)+'-'+from_date_s.charAt(6)+from_date_s.charAt(7);
			if(result["FROM_TIME"].toString().length > 2 && result["FROM_TIME"].toString() != "000000"){ 
				var from_time_s = result["FROM_TIME"].toString();
				var converted_ftime = from_time_s.charAt(0)+from_time_s.charAt(1)+':'+from_time_s.charAt(2)+from_time_s.charAt(3);
			}
			else{
				var converted_ftime = "";
			}
			if(result["TO_TIME"].toString().length > 2 && result["TO_TIME"].toString() != "000000"){	
				var to_time_s = result["TO_TIME"].toString();
				var converted_ttime = to_time_s.charAt(0)+to_time_s.charAt(1)+':'+to_time_s.charAt(2)+to_time_s.charAt(3);
			}
			else{
				var converted_ttime = "";
			}
			if (create_operation == 'x' && $('#tempor').length == 0){
				$('#create_operation').val('');
				$('#add_date').val('');
				$('#add_ftime').val('');
				$('#add_ttime').val('');
			}
			else{
				$('#add_date').val(converted_date);
				$('#add_ftime').val(converted_ftime);
				$('#add_ttime').val(converted_ttime);
			}
			if(result["DOC_NUMBER"].toString().charAt(0) == "*" ){//&& result["TO_TIME"].length < 2){
				// $('.create_order_but').show();
				$('#sales_activities_edit').show();
				$('.delete').show();
				$('#quick_deleteSA').show();
				$('#delete_but').show();
			 
			 //$('#quick_createSA').hide();
			 //$('#quickNav_but').hide();
			}
			else{
			 //$('#sales_activities_edit').hide();
			 $('.delete').hide();
			 $('#quick_deleteSA').hide();
			 //$('#quick_createSA').hide();
			 //$('#quickNav_but').hide();
			}

			 var sales_activities = $("#sales_activities");
			 var from_time = result["FROM_TIME"];
			 var to_time = result["TO_TIME"];

			 if(result["ORDER_NUMS"] != ""){
			 	// $('.create_order_but').show();
			 }

			set_customer_name(result["NAME1"], result["ACTIVITY_TYPE_TEXT"], result["RESULT"], result["CONTACT"], result["ACTIVITY_STATUS_TEXT"],result["PARTN_ID"]);

			var sales_act_note = result['ACTIVITY_COMMENT'];

			if(sales_act_note.length > 40){
				var sanote = sales_act_note.substring(0,40);
			}
			else{
				var sanote = sales_act_note;
			}
			 sales_activities.find("#company").html(sanote+'<input type="hidden" id="edit_details" value="'+result["PARTN_ID"]+'_'+result["ACTIVITY_TYPE"]+'_'+result["DOC_NUMBER"]+'_'+result["CONTACT"]+'_'+result["RESULT"]+'_'+result["NAME1"]+'">');
			 sales_activities.find("#user").html(lang.created_by + result["CREATED_BY"]);
			 sales_activities.find("#start_date").html(formatDate(result["FROM_DATE"]));
			 if(from_time.length < 2){
			 	sales_activities.find("#start_time").html('');
			 }
			 else{
			 	sales_activities.find("#start_time").html(from_time.charAt(0)+from_time.charAt(1)+':'+from_time.charAt(2)+from_time.charAt(3));	
			 }
			 sales_activities.find("#end_date").html(formatDate(result["TO_DATE"]));
			 if(to_time.length < 2){
			 	sales_activities.find("#end_time").html('');
			 }
			 else{
			 	sales_activities.find("#end_time").html(to_time.charAt(0)+to_time.charAt(1)+':'+to_time.charAt(2)+to_time.charAt(3));
			 }
			 
			 sales_activities.find("#free_text").val(result["ACTIVITY_COMMENT"]);
			 if(result["VBELV_SD"] != ""){
	 			$('.show_ref_sd').attr('docnum', result["VBELV_SD"]);
	 			//$('.show_ref_sd').show();
			 }
		 	if(result["DOC_NUMBER"].toString().charAt(0) == "*" ){//&& result["TO_TIME"].length < 2){
			 	 $(".customers_current").filter(":visible").fadeOut(function(){
				 	$('#sales_activities_edit').show();
				 });
				 $('.delete').show();
				 $('#quick_deleteSA').show();
				 $('#sales_activities_edit').trigger('click');
			 }
			 else{
				 //$('#sales_activities_edit').hide();
				 $('.delete').hide();
				 $('#quick_deleteSA').hide();
				 var currentScreen = $(".customers_current").filter(":visible");
				 /** remove favorites */
				 // Check if the selected sales activity is marked as favourite, accordingly set the favourite icon in details
				 // if ($('#salesActivityList li.active #salesActStarDivList').hasClass('favorite_activity')) 
					// {
					// 	$('#sales_activities #salesActStarDivDtls').addClass('favorite_activity');
					// }
				 // else
				 // 	$('#sales_activities #salesActStarDivDtls').removeClass('favorite_activity');
				 
				 //$('#sales_activities #salesActStarDivDtls').attr('data-doc_num',result["DOC_NUMBER"].toString());
				 		 
				// hide favourite icon if document is in saved.
				 // if (  $('#salesActivityList li.active #salesActStarDivList').css("visibility") == "hidden" ) 
				 // 	$('#sales_activities #salesActStarDivDtls').hide(); 	
			 	//  else
			 	//  {
			 	//  	$('#sales_activities #salesActStarDivDtls').show();
			 	 	
			 	 	// Show Reminder Date 
					// cordova.exec(function(favParam){
					// 	var favResult = eval('(' + favParam + ')');
					// 	if(favResult.length > 0 )
					// 	{
					// 		sales_activities.find("#actReminderDate").html(lang.SalesOrderDetails_titles[7] + " " + formatDate(favResult[0]["RESUB_DATE"]));
					// 	} 
					// 	else
					// 	{
					// 		sales_activities.find("#actReminderDate").html(lang.SalesOrderDetails_titles[7] + " " + "");
					// 	}

					// }, execFailed, 'Resubmission', 'getResubDetails', [ result["DOC_NUMBER"] ]);
			 	//  }
			 		
				 if (currentScreen.length == 0){
				 	$("#sales_activities").fadeIn();
				 } else {
				 	 currentScreen.fadeOut(function(){
						$("#sales_activities").fadeIn();
					 });
				 }
				 $(".quickNav").fadeIn();
			 }
		});
	}
}

function calculate_ddp(){
	$(".ex_gga").text(exch_q2.toFixed(2) + " " + ex_total_currenty);
	$(".ex_gr").text(exch_q1.toFixed(2) + " " + ex_total_currenty);
	//if($('#ex_ddp').val() == "") $('#ex_ddp').val(0);
	ddp = parseFloat(ddp_g)/100;
	$('#ddp_l').text(ddp_g);
	if(isNaN(ddp)) ddp=0;
	ddp = 1 - ddp;
	if(ddp > 1) ddp = 1;
	if(ddp < 0) ddp = 0;
	difff = parseFloat(((exch_q1*ddp)-exch_q2)).toFixed(2);
	if(difff > 0){
		var final_diff = '<span style="color: green">'+ difff + " " + ex_total_currenty+'</span>';
	}
	else{
		var final_diff = '<span style="color: red">'+ difff + " " + ex_total_currenty+'</span>';
	}
	$(".ex_gd").html(final_diff);
}

$(function(){
	$('#ex_ddp').on('keyup', function(){
		calculate_ddp();
	})
	$("#customers_salesDocuments_ordersTable tbody").on('click', 'tr', function(){
		if($(this).data('posnr') != 0 && $(this).data('posnr') != ""){
			var answer = $(this).data('posnr');	
		}
		else{
			var answer = 0;
		}
		$('#loadingBar').show();
		getAdditionalPricingData(answer, $(this).data('posnr'), $(this).data('vbeln'), $(this).data('matnr'), $(this).find('td:eq(2)').text(), $(this).data('meins'), $(this).data('kbetr'), current_username, current_password, function(param){
			var result = eval('([' + param + '])');
			$( ".stock_plants_body" ).html("");
			for (var i = 0; i < result.length; i++) {
				$( ".stock_plants_body" ).append("<tr><td><span class='img"+result[i]["XICON"].replace(/@/g, '')+"'></span></td><td>"+result[i]["COND_TYPE"]+"</td><td>"+result[i]["XVTEXT"]+"</td><td>"+result[i]["COND_VALUE"]+"</td><td>&#128;</td><td>"+result[i]["COND_P_UNT"]+"</td><td>"+result[i]["COND_D_UNT"]+"</td><td>"+result[i]["CONDVALUE"]+"</td><td>&#128;</td></tr>");
			};
			//todo styling:
			//Fire Dialog:
			$('#dialog_plant_inv').dialog({
				dialogClass: "with-close-productPopup3",
				title: lang.productPopUp_title,
				show: 'fade',
				hide: "fade",
				width: 780,
				height: 650
			});

			$('#dialog_plant_inv #head_information').scrollTop(0);
			
			$("#popUp_overlap").on("click", function(){
				$(this).fadeOut();
				$(".with-close-productPopup3 .ui-dialog-titlebar-close").click();
				$("#popUp_overlap").off("click");

				//Remove active:
				$('#pageAside ul li').removeClass("activeLongPress");
			}).fadeIn();

		    $("#dialog_plant_inv").dialog('option', 'title', lang.add_price_details);
		    $('#loadingBar').fadeOut();
		}, execFailed);
	});


	$('#aside_nav_activities a').on("click", function(){
		$("#pageAside").scrollTop(0);
		
		$('#search_box').val('');
		$('#aside_nav_activities a').removeAttr('class');
		$(this).addClass('active');
		$("#sales_activities").fadeOut();
		
		switch($(this).attr('id')){
			case 'all_activities':

				$('#salesActivityList').fadeOut(function(){
					get_all_activities = true;
					getSalesActivitesList('',false);
					$('#salesActivityList').fadeIn();
				});

				return false;
				break;

			case 'my_activities':
				//if content is hidden:
				$("#pageContent").fadeIn();
				$('#salesActivityList').fadeOut(function(){
					get_all_activities = false;
					getSalesActivitesList('',true);
					$('#salesActivityList').fadeIn();
				});

				return false;
				break;
		}
	});


})

function getAdditionalPricingData(synched, posnr, vbeln, matnr, quant, meins, kbetr, user, pass, success_get, get_failed){
	if( !doIhaveInternet() ){
		var errorDict = {};
        errorDict[lang.internet_err] = "error";
        customAlert(errorDict);
		return;
	}
	if(synched != 0){
		var data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions"><soapenv:Header/><soapenv:Body><urn:_-MSCMOBIL_-Z_PRICE_QUOTATION><IM_BNAME>'+user+'</IM_BNAME><IM_POSNR>'+posnr+'</IM_POSNR><IM_VBELN>'+vbeln+'</IM_VBELN></urn:_-MSCMOBIL_-Z_PRICE_QUOTATION></soapenv:Body></soapenv:Envelope>';	
		$.soap({
			url: baseOnlineURL+"/price_quotation?sap-client="+sapClient,
			method: "_-MSCMOBIL_-Z_PRICE_QUOTATION",
			soap12: false,
			data: data,
			wss: "",
			HTTPHeaders: {
				Authorization: 'Basic ' + btoa(user+':'+pass)
			},
			namespaceQualifier:  "myns",
			namespaceURL: "urn:_-MSCMOBIL_-Z_PRICE_QUOTATION",
			noPrefix: false,
			elementName: "",
			enableLogging: false,
			success: function(SOAPResponse) {
				var resp = SOAPResponse.toString();
				//var result = eval('(' + $(resp).find("EX_JSON_RESULT").text() + ')');
				var result = $(resp).find("EX_JSON_RESULT").text();
				console.log(resp);
				success_get(result);
			},
			error: function(SOAPResponse) {
				get_failed(SOAPResponse.toString());
			}
		});
	}
	else{
		var data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions"><soapenv:Header/><soapenv:Body><urn:_-MSCMOBIL_-Z_GET_QUOTATION><IM_BNAME>'+user+'</IM_BNAME><IM_KUNNR>'+$('#pageAside .active .sa_number').data('kunnr')+'</IM_KUNNR><IM_MATNR>'+matnr+'</IM_MATNR><IM_QUANTITY>'+quant+'</IM_QUANTITY><IM_MEINS>'+meins+'</IM_MEINS><IM_KBETR>'+kbetr+'</IM_KBETR></urn:_-MSCMOBIL_-Z_GET_QUOTATION></soapenv:Body></soapenv:Envelope>';	
		$.soap({
			url: baseOnlineURL+"/get_quotation?sap-client="+sapClient,
			method: "_-MSCMOBIL_-Z_GET_QUOTATION",
			soap12: false,
			data: data,
			wss: "",
			HTTPHeaders: {
				Authorization: 'Basic ' + btoa(user+':'+pass)
			},
			namespaceQualifier:  "myns",
			namespaceURL: "urn:_-MSCMOBIL_-Z_GET_QUOTATION",
			noPrefix: false,
			elementName: "",
			enableLogging: false,
			success: function(SOAPResponse) {
				var resp = SOAPResponse.toString();
				//var result = eval('(' + $(resp).find("EX_JSON_RESULT").text() + ')');
				var result = $(resp).find("EX_JSON_RESULT").text();
				console.log(resp);
				success_get(result);
			},
			error: function(SOAPResponse) {
				get_failed(SOAPResponse.toString());
			}
		});
	}
}

//Online Call: Get Product Details:
function getProductDetailsOnline(matnr, user, pass, success_get, get_failed){
	if( !doIhaveInternet() ){
		var errorDict = {};
        errorDict[lang.internet_err] = "error";
        customAlert(errorDict);
		return;
	}
	var data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions"><soapenv:Header/><soapenv:Body><urn:_-MSCMOBIL_-Z_PRODUCT_DETAIL><IM_BNAME>'+user+'</IM_BNAME><IM_MATNR>'+matnr+'</IM_MATNR></urn:_-MSCMOBIL_-Z_PRODUCT_DETAIL></soapenv:Body></soapenv:Envelope>';
	$.soap({
		url: baseOnlineURL+"/product_detail?sap-client="+sapClient,
		method: "_-MSCMOBIL_-Z_PRODUCT_DETAIL",
		soap12: false,
		data: data,
		wss: "",
		HTTPHeaders: {
			Authorization: 'Basic ' + btoa(user+':'+pass)
		},
		namespaceQualifier:  "myns",
		namespaceURL: "urn:_-MSCMOBIL_-Z_PRODUCT_DETAIL",
		noPrefix: false,
		elementName: "",
		enableLogging: false,
		success: function(SOAPResponse) {
			var resp = SOAPResponse.toString();
			//var result = eval('(' + $(resp).find("EX_JSON_RESULT").text() + ')');
			var result = $(resp).find("EX_JSON_RESULT").text();
			success_get(result);
		},
		error: function(SOAPResponse) {
			//get_failed(SOAPResponse.toString());
		}
	});
}

// function getSalesOrderDetails(sales_order_nr, cust_soldto, openQuot, reff){

// 	var isOpenQuotation = openQuot;
// 	if( sales_order_nr == undefined ){
// 		sales_order_nr = $('#pageAside .active').find('.suba_dets').data('suba_ord');
// 	}
// 	//???
// 	var ktext1 = "";
// 	var ktext2 = ""; 
// 	var cust_salesOrderNr = "";  
// 	var purch_date = ""; 
// 	var delivery_date = ""; 
// 	var delivery_priority = ""; 
// 	//???
// 	if(openQuot == 1){
// 		cordova.exec(fireSalesOrderDetails, execFailed, 'OpenQuotation', 'getOpenQuotationDetails', [sales_order_nr]);
// 	}
// 	else{
// 		cordova.exec(fireSalesOrderDetails, execFailed, 'Order', 'getSalesOrderDetails', [sales_order_nr]);
// 	}
// 	function fireSalesOrderDetails(param){
// 		var result = eval('(' + param + ')');
// 		currentSalesDocDetails = result;
// 		if(result["Header"] == undefined){
// 			var missSD = {};
// 				missSD[lang.missingSD] = "error";
// 				customAlert(missSD);
// 				return;
// 		}
// 		if(reff == 1){
// 			$("#pageDownNav li").removeClass("active");
// 			$('.show_ref_sd').addClass("active");
// 		}
// 		var attaches2 = result['ATTACHMENTS']

// 		if(typeof attaches2 != "undefined" && attaches2.length > 0){
// 			$('#attachments_links').css('opacity', '1');
// 			var html_string2 = "";
// 			for(iii=0;iii<attaches2.length;iii++){
// 				html_string2 += '<tr><td><div style="width: 84%;" class="item"><a style="color: #069deb;" href="'+attaches2[iii]['LINK']+'">'+attaches2[iii]['DESCRIPTION']+'</a></div></td>';
// 			}
// 			$('#attaches_holder').html(html_string2);
// 			$('#attaches_holder').off('click');
// 			$('#attaches_holder').on('click', 'tr',function(){
// 				window.location.href = $(this).find('a').attr('href');
// 			});
// 			$('#attachments_links').on('click', function(){
// 				$('#dialog_attachments').dialog({
// 					dialogClass: "with-close-productPopup3",
// 					title: lang.productTableHeadings[2],
// 					show: 'fade',
// 					hide: "fade",
// 					width: 580,
// 					height: 600
// 				});

// 				$("#popUp_overlap").on("click", function(){
// 					$(this).fadeOut();
// 					$(".with-close-productPopup3 .ui-dialog-titlebar-close").click();
// 					$("#popUp_overlap").off("click");

// 					//Remove active:
// 					$('#pageAside ul li').removeClass("activeLongPress");
// 				}).fadeIn();

// 			    $("#dialog_attachments").dialog('option', 'title', lang.productTableHeadings[2]);
// 			})
// 		}
// 		else{
// 			$('#attachments_links').off('click');
// 			$('#attachments_links').css('opacity', '.7');
// 		}

// 		var $order_details = $("#customers_salesDocuments");
// 		if(typeof result["Header"]["ORD_REASON"] != "undefined"){
// 			switch(result["Header"]["ORD_REASON"]){
// 				case "03":
// 					var indicate = '<span class="listViewFlag" style="width: 13px; height: 13px; margin: 3px 5px 5px 0;"></span>';
// 					break;
// 				case "01":
// 					var indicate = '<span class="listViewFlag2" style="width: 13px; height: 13px; margin: 3px 5px 5px 0;"></span>';
// 					break;
// 				case "02":
// 					var indicate = '<span class="listViewFlag3" style="width: 13px; height: 13px; margin: 3px 5px 5px 0;"></span>';
// 					break;
// 				default:
// 					var indicate = "";
// 					break;
// 			}	
// 		}
// 		else{
// 			var indicate = "";
// 		}
// 		$order_details.find("#salesDoc_number").html(indicate+sales_order_nr.replace(/^0+/, ''));
// 		$order_details.find("#salesDoc_purchNumber").text(result["Header"]["BSTKD"]);
// 		if(result["Header"]["PURCH_DATE"].length > 2){
// 			$order_details.find("#salesDoc_orderDate").text(formatDate(result["Header"]["PURCH_DATE"]));
// 		}
// 		else{
// 			$order_details.find("#salesDoc_orderDate").text("");
// 		}
// 		if(result["Header"]["REQ_DATE_H"].length > 2){
// 			$order_details.find("#salesDoc_deliveryDate").text(formatDate(result["Header"]["REQ_DATE_H"]));
// 		}
// 		else{
// 			$order_details.find("#salesDoc_deliveryDate").text("");
// 		}

// 		if(result["Header"]["QT_VALID_T"].length > 2){
// 			$order_details.find("#salesDoc_ValidityDate").text( formatDate(result["Header"]["QT_VALID_T"]));
// 		}
// 		else{
// 			$order_details.find("#salesDoc_ValidityDate").text("");
// 		}
// 		$order_details.find("#salesDoc_ValidityDate").attr('data-salesDocNo',result["Header"]["DOC_NUMBER"]);

// 		// if (isOpenQuotation == 1) 
// 		// 	$order_details.find("#salesDoc_ValidityDate").parent().fadeOut();
// 		// else
// 		// 	$order_details.find("#salesDoc_ValidityDate").parent().fadeIn();

// 		//Get the values from customizing:
// 		cordova.exec(setSalesDocTypesList, execFailed, 'Customizing', 'getAUARTList', ['']);
// 		function setSalesDocTypesList(param){
// 			var doctypes_result = eval('(' + param + ')');
// 			//Set Default:
// 			$order_details.find("#salesDoc_type").text(result["Header"]["DOC_TYPE"]);
// 			//Find Value:
// 			for( i = 0; doctypes_result.length > i; i++ ){
// 				if( result["Header"]["DOC_TYPE"] == doctypes_result[i]["AUART"] ){
// 					$order_details.find("#salesDoc_type").text(doctypes_result[i]["BEZEI"]);
// 					break;
// 				}
// 			}
// 		}
		
// 		//Get Customer Details:
// 		var cust_soldto = result["Header"]["SOLD_TO"];
// 		cordova.exec(function(details_param){
// 			var details_param = eval('(' + details_param + ')');
// 			var row3 = "";
// 			(details_param["STRAS"] != "" || details_param["HOUSENUM"] != "") ? row3 = details_param["STRAS"] + " " + details_param["HOUSENUM"] + ",<br />" : ''; 
// 			$order_details.find("#salesDoc_customer").html(details_param["NAME1"] + " " + details_param["NAME2"] + " (" + cust_soldto.replace(/^0+/, '') + "),<br />" + row3 + details_param["PSTLZ"] + " " + details_param["ORT01"]);
		
// 		}, execFailed, 'Customer', 'getCustomerDetails', [cust_soldto]);

// 		function salesDocument_tableRecord(conUnit, material_number, quantity, conVal, sales_unit, price_per_unit, roll_total, product_name, vbeln, posnr, nr,altern,discntPercent){
// 			if(typeof altern != 'undefined' && altern != '' && altern != "000000"){
// 				var indi = '<span class="listViewFlag3" style="width: 13px; height: 13px; margin: 2px 5px 5px 0;"></span>';
// 			}
// 			else{
// 				var indi = '';	
// 			}
// 			var record = "";
// 			(posnr == "") ? posnr=0 : '';
// 			record += '<tr data-seq_nr="'+nr+'" data-vbeln="'+vbeln+'" data-matnr="'+material_number+'" data-meins="'+conUnit+'" data-kbetr="'+conVal+'" data-posnr="'+posnr+'"><td>';
// 			record += indi+material_number.replace(/^0+/, '');
// 			record += "</td><td>";
// 			record += product_name;
// 			record += "</td><td>";
// 			record += quantity;
// 			record += "</td><td>";
// 			record += sales_unit;
// 			record += "</td><td>";
// 			record += price_per_unit;
// 			record += "</td><td>" +  discntPercent + " %" + "</td><td data-realval='" + roll_total + "'>";
// 			record += roll_total;
// 			record += "</td></tr>";
			
// 			return record;
// 		}

// 		function getProductNameAndUnits(result, i, callback){
// 			//Check if product comes from the online service:
// 			cordova.exec(function(param){
// 				if( param == "X" ){
// 					cordova.exec(setProductDetails, execFailed, 'Product', 'getProductDetails', [ result["Items"][i]["MATERIAL"] ]);
// 				} else {
// 					getProductDetailsOnline(result["Items"][i]["MATERIAL"], current_username, current_password, successful_get, execFailed);
// 					function successful_get(data){
// 						setProductDetails(data, 1);
// 					}
// 				}
// 			}, execFailed, 'Product', 'isProductLocal', [ result["Items"][i]["MATERIAL"] ]);

// 			function setProductDetails(param, online_call){
// 				var productDetails = eval('(' + param + ')');
// 				//Because the names from the back-end are messed up :(
// 				if(online_call == 1){
// 					var salesRels = productDetails['SALES_REL'];
// 					var cItems = productDetails['COND_ITEMS'];
// 					var current_unitsOfMeasure = productDetails['UNITS'];
// 					var attaches = productDetails['DOCUMENTS'];
// 				}
// 				else{
// 					var salesRels = productDetails['SalesRels'];
// 					var cItems = productDetails['ConditionItems'];
// 					var current_unitsOfMeasure = productDetails['Units'];
// 					var attaches = productDetails['Documents'];
// 				}

// 				if(result["Header"]["CREATED_BY"] != "" && result["Header"]["CREATED_BY"].length > 0){
// 					product_name = result["Items"][i]["SHORT_TEXT"];	
// 				}
// 				else{
// 					product_name = productDetails["MAKTX"] + "<br />" + productDetails["MAKTX2"];
// 				}
				
// 				var sales_unit_text = "";
// 				var sales_unit_factor = "";
// 				for(f = 0; f < current_unitsOfMeasure.length; f++ ){
// 					if( result["Items"][i]["SALES_UNIT"] == current_unitsOfMeasure[f]["ALT_UNIT"] ){
// 						sales_unit_text = current_unitsOfMeasure[f]["ALT_UNIT"];
// 						sales_unit_factor = current_unitsOfMeasure[f]["FACTOR"];
// 					}
// 				}
// 				//Add the factor + default unit:
// 				sales_unit_text += "<br />" + sales_unit_factor + " " + productDetails["BASE_UOM"];
// 				callback(result, i, product_name, sales_unit_text, cItems);
// 			}
// 		}
		
// 		var salesDocument_table_html = "";
// 		$salesDocItemsTable = $("#customers_salesDocuments_ordersTable tbody");
// 		$salesDocItemsTable.html("");
// 		//result = result["Items"];
// 		for(i = 0; i < result["Items"].length; i++){
// 			//Use callback to get the product name:
// 			getProductNameAndUnits(result, i, function(result, i, product_name, doc_type, cItems){
// 				var material_number = result["Items"][i]["MATERIAL"];
// 				var vbeln 			= result["Header"]["DOC_NUMBER"];
// 				var posnr 			= result["Items"][i]["ITM_NUMBER"];
// 				var altern 			= result["Items"][i]["ALTERN_ITM"];
// 				var conUnit         = result["Items"][i]["SALES_UNIT"];
// 				var conVal       	= result["Items"][i]["COND_VALUE"];
// 				var quantity 		= result["Items"][i]["REQ_QTY"];
// 				var price_per_unit 	= result["Items"][i]["COND_VALUE"].formatMoney(2) + " &#0128; <br />/ " + result["Items"][i]["COND_P_UNT"] + " " + cItems[0]["KMEIN"];
// 				var roll_total 		= result["Items"][i]["NET_VALUE"].formatMoney(2) + " &#0128;";
// 				var discntPercent 	= result["Items"][i]["DISCOUNT_VALUE"];

// 				$salesDocItemsTable.append(salesDocument_tableRecord(conUnit,material_number, quantity, conVal, doc_type, price_per_unit, roll_total, product_name, vbeln, posnr, i, altern,discntPercent));
// 			});
// 		}
		
// 		$(".customers_current:visible").fadeOut(function(){
// 			//Value calculated by SAP:
// 			if(result["Header"]["CREATED_BY"] != "" && result["Header"]["CREATED_BY"].length > 0){
// 				$("#total_price").html( result["Header"]["NET_VAL_HD"].formatMoney(2) + " &#0128;" );
// 			}
// 			else{
// 				var total_price = 0;
// 				$salesDocItemsTable.find("tr").each(function(){
// 					var value = $(this).find("td:last").data("realval").replace(" €", "");
// 					value = value.replace(",", ".");
// 					total_price += parseFloat( value );
// 				});
// 				$("#total_price").html( total_price.formatMoney(2) + " &#0128;" );
// 			}

// 			$("#total_items").text( $salesDocItemsTable.find("tr").length );
// 			$order_details.fadeIn();
// 		});

// 		// Show reminder icon if the document status is released
// 		// if (result["Header"]["DOC_TYPE"] != 'AG') {
// 			$('#customers_salesDocuments #salesDocStarDivDtls').attr('data-doc_num',result["Header"]["DOC_NUMBER"].toString());
// 			if ((result["Header"]["DOC_NUMBER"].toString().charAt(0) != "*") && (result["Header"]["DOC_NUMBER"].toString().charAt(0) != "+"))
// 				$('#customers_salesDocuments #salesDocStarDivDtls').show();
// 			else
// 				$('#customers_salesDocuments #salesDocStarDivDtls').hide();
// 		// }

// 		// show reminder if reminder is added
// 		cordova.exec(function(favParam){
// 			var favResult = eval('(' + favParam + ')');
// 			if(favResult.length > 0 )
// 			{
// 				$('#customers_salesDocuments #salesDocStarDivDtls').addClass('favorite_activity');
// 				$order_details.find("#salesDoc_ReminderDate").text( formatDate(favResult[0]["RESUB_DATE"])   );
// 			} 
// 			else
// 			{
// 				$('#customers_salesDocuments #salesDocStarDivDtls').removeClass('favorite_activity');
// 				$order_details.find("#salesDoc_ReminderDate").text("");
// 			}

// 		}, execFailed, 'Resubmission', 'getResubDetails', [ result["Header"]["DOC_NUMBER"] ]);
		

// 	}
// }
