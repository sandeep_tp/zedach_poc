var reports_en = {
	"report_jan" : "January",
	"report_feb" : "February",
	"report_mar" : "March",
	"report_apr" : "April",
	"report_may" : "May",
	"report_jun" : "June",
	"report_jul" : "July",
	"report_aug" : "August",
	"report_sep" : "September",
	"report_oct" : "October",
	"report_nov" : "November",
	"report_dec" : "December"
};

var reports_de = {
	"report_jan" : "Januar",
	"report_feb" : "Februar",
	"report_mar" : "März",
	"report_apr" : "April",
	"report_may" : "Mai",
	"report_jun" : "Juni",
	"report_jul" : "Juli",
	"report_aug" : "August",
	"report_sep" : "September",
	"report_oct" : "Oktober",
	"report_nov" : "November",
	"report_dec" : "Dezember"
};
var current_customer = 0; //sync_back
var lastSectionFromEdit = "#customers";
var langChanged = 0;
var exch_q1 = 0;
var exch_q2 = 0;
var ex_total_currenty = "";
var ddp_g=0;
var pafkt_list = [];
var ifSearchIsExecutedAtFirst = 0;
var AUARTList = [];
var firstTimeShowSearch = true;
var currentSalesDocDetails;
var calledFromReminder = false;

var current_username = "";
var current_password ="";
function getUserCredentials(){
	cordova.exec(storeCredentials, execFailed, 'Login', 'getCurrentUserCredentials', ['']);

	function storeCredentials(param){
		var result = eval('(' + param + ')');
		current_username = result.username;
		current_password = result.password;
	}
}

function getUser(){
	cordova.exec(storeUser, execFailed, 'User', 'getUser', ['']);

	function storeUser(param){
		var result = eval('(' + param + ')');
		resubBUKRS = result.VKORG;
	}
}

/**
Removed my customers for SuperUser App
*/
function getCustomerList(updateMode, updateCustomer, my_customers){
	var get_my_customers;
	/** Uncommented code for my/all customers tab*/
	// if (typeof(my_customers) == "undefined")  
	// {
	// 	get_my_customers = true; 
	// 	$('#all_customers').removeClass();
	// 	$('#my_customers').addClass('active');
	// }
	// else
	// 	get_my_customers = my_customers;
	/** end changes*/
	
	//Get the VSBED Values:
	//var result_vsbed;
	//	result_vsbed = eval('(' + param_vsbed + ')');
	$('#loadingBar').fadeIn( function() {
		if (get_my_customers) 
			cordova.exec(fireCustomersList, execFailed, 'Customer', 'searchCustomerByUNAME', [current_username]);
		else
			cordova.exec(fireCustomersList, execFailed, 'Customer', 'getCustomerList', [0,100]);
	});
		
	//}, execFailed, 'Customizing', 'getVSBEDList', ['']);

	var getParam = getParams();

    function fireCustomersList(param) {
		function record(client_name, client_id, client_address, client_postalcode, client_lisfd, client_ausfd, client_klimk, client_visit_date, client_street, client_house_num, is_member){
			
			//Check if we need to display red dot:
			var indicator = '';
			if ( client_lisfd != "" || client_ausfd != "" || client_klimk != ""){
				indicator += '<span class="listViewFlag"></span>';
			}
			if ( is_member == "X"){
				indicator += '<span class="listViewFlag2"></span>';
			}

			var clientRecord = "";
			clientRecord += '<li><a href="#"><span class="name">';
			clientRecord += indicator + client_name;
			clientRecord += '</span><div class="arrow group"><span data-kunnr="'+client_id+'" class="number">';
			clientRecord += client_id.replace(/^0+/, '');
            clientRecord += '</span></div><span class="address">';
			clientRecord += client_postalcode + " " + client_address;
			clientRecord += '<br />';
			clientRecord += client_street + " " + client_house_num;
            if (client_visit_date != "")
                clientRecord += '</span></a></li>';

			return clientRecord;
		};
	
		var result = eval('(' + param + ')');
		var result_html = "";
		$('#loadingBar').fadeOut( );
		
		for(i = 0; i < result.length; i++){
			var client_name       = result[i]["NAME1"];
			var client_id         = result[i]["KUNNR"];
			var client_address    = result[i]["ORT01"];
			var client_postalcode = result[i]["PSTLZ"];
			var client_lisfd      = result[i]["LIFSD"];
			var client_ausfd      = result[i]["AUFSD"];
			var client_klimk      = result[i]["KLIMK2"];
            var client_visit_date = result[i]["LAST_VISIT_DATE"];
            var client_street     = result[i]["STRAS"];
            var client_house_num  = result[i]["HOUSENUM"];
            var is_member         = result[i]["IS_MEMBER"];
            /** Uncommented code for my/all customers tab*/
            // if (get_my_customers) 
            // {
            	// if ((current_username == result[i]["UNAME"]) || ( current_username.toLowerCase() == result[i]["UNAME"].toLowerCase() )) {
            		result_html = result_html + record(client_name, client_id, client_address, client_postalcode, client_lisfd, client_ausfd, client_klimk,client_visit_date, client_street, client_house_num, is_member);
            	// }

            // }
            // else
            // {
            // 	result_html = result_html + record(client_name, client_id, client_address, client_postalcode, client_lisfd, client_ausfd, client_klimk,client_visit_date, client_street, client_house_num, is_member);
            // }
            /** end changes*/
			
		}

		if ( result.length == 0 )
		{
			$(".quickNav, #pageDownNav").fadeOut();
			$(".customers_current").filter(":visible").fadeOut(function(){
				$("#blank_screen").fadeIn();
			});
		}
		else
			$(".quickNav, #pageDownNav").fadeIn();

		$('#loadingBar').fadeOut(500);
		$("#pageAside ul").html(result_html);

		var activeArrow = '<span class="active_arrow"></span>';
		$("#pageNav ul li:eq(0)").append(activeArrow);

		//Fire first customer details or fire Edit Customer:
		var edit_customer = getParams().edit_customerid;
		//Sales Activity screen --> Back to Customers - Sales Activities:
		var salesAct_customer = getParams().salesAct_customerid;
		var salesAct_customerD = getParams().salesAct_customeridDelete;
		//If back from Sales Activity:
		var backFromSA = getParams().backCustomerFromSA;
		if (edit_customer != undefined){
			searchCustomer(edit_customer);
		} else if(salesAct_customer != undefined && firstTimeShowSearch ) {
			$("#search_box").val(salesAct_customer).trigger("keyup");
			firstTimeShowSearch = false;
		} else if( salesAct_customerD != undefined && firstTimeShowSearch ) {
			var saveSucc = {};
			saveSucc[lang.deleted_sact] = "success";
			customAlert(saveSucc);
			$("#search_box").val(salesAct_customerD).trigger("keyup");
			firstTimeShowSearch = false;
		} else if(updateMode == 1){
			$("#pageAside li").find(".number[data-kunnr=" + updateCustomer + "]").closest("li").addClass("active");
			//Scroll to the active li:
			$("#pageAside").scrollTo("#pageAside .active", 300);
			
			return false;
		} else if(backFromSA != undefined){
			searchCustomer(backFromSA, 1);
		} else {
			//Fire First Customer Details:
			$("#pageAside ul li:first a").click();
		}

		//Scroll to the fist record:
		$("#pageAside").scrollTo("#pageAside .active", 300);
	}
}


//Load more customers within range
function loadMoreCustomerList(){
	var result_left_html = "";
	var currentNumber = parseInt($("#pageAside ul li").length);

	//Get the VSBED Values:
	/*var result_vsbed;
	cordova.exec(function (param_vsbed){
		result_vsbed = eval('(' + param_vsbed + ')');*/
		cordova.exec(fire100moreCustomers, execFailed, 'Customer', 'getCustomerList', [currentNumber, currentNumber + 100]);
	//}, execFailed, 'Customizing', 'getVSBEDList', ['']);

	function record(client_name, client_id, client_address, client_postalcode, client_lisfd, client_ausfd, client_klimk, client_visit_date, client_street, client_house_num, is_member){

		//Check if we need to display red dot:
		var indicator = '';
		if ( client_lisfd != "" || client_ausfd != "" || client_klimk != ""){
			indicator += '<span class="listViewFlag"></span>';
		}
		if ( is_member == "X"){
			indicator += '<span class="listViewFlag2"></span>';
		}

		var clientRecord = "";
		clientRecord += '<li><a href="#"><span class="name">';
		clientRecord += indicator + client_name;
		clientRecord += '</span><div class="arrow group"><span data-kunnr="'+client_id+'" class="number">';
		clientRecord += client_id.replace(/^0+/, '');
        clientRecord += '</span></div><span class="address">';
		clientRecord += client_postalcode + " " + client_address;
		clientRecord += '<br />';
		clientRecord += client_street + " " + client_house_num;
        if (client_visit_date != "")
            clientRecord += '</span></a></li>';

		return clientRecord;
	};

	function fire100moreCustomers(param_next100){
		var result_left = eval('(' + param_next100 + ')');

		for(i = 0; i < result_left.length; i++){
			var client_name 	  = result_left[i]["NAME1"];
			var client_id 		  = result_left[i]["KUNNR"];
			var client_address 	  = result_left[i]["ORT01"];
			var client_postalcode = result_left[i]["PSTLZ"];
			var client_lisfd 	  = result_left[i]["LIFSD"];
			var client_ausfd 	  = result_left[i]["AUFSD"];
			var client_klimk 	  = result_left[i]["KLIMK2"];
	        var client_visit_date = result_left[i]["LAST_VISIT_DATE"];
	        var client_street     = result_left[i]["STRAS"];
	        var client_house_num  = result_left[i]["HOUSENUM"];
	        var is_member  		  = result_left[i]["IS_MEMBER"];
			result_left_html = result_left_html + record(client_name, client_id, client_address, client_postalcode, client_lisfd, client_ausfd, client_klimk, client_visit_date, client_street, client_house_num, is_member);
		}

		$("#pageAside ul").append(result_left_html);

		if(result_left.length < 100){
			//Turn off scroll event:
			$("#pageAside").off("scroll");
		}
	}
	
}


function getCustomerDetails(customerId){
    cordova.exec(fireCustomerDetails, execFailed, 'Customer', 'getCustomerDetails', [customerId]);

    function fireCustomerDetails(param){

    	var result = eval('(' + param + ')');
    	current_customer = result["KUNNR"]; //sync_back
    	//alert(JSON.stringify(result));
    	//$("#blank_screen").hide();
    	if ($(".customers_current").filter(":visible").length > 1)
    	{
    		$("#blank_screen").hide();
    	}

		$(".customers_current").filter(":visible").fadeOut(function(){
			var customers = $("#customers");
			var customers2 = $("#customers2");
			customers.find("#company").html(result["NAME1"] + " " + result["NAME2"] + " (" + result["KUNNR"].replace(/^0+/, '') + ")");
			customers2.find("#company").html(result["NAME1"] + " " + result["NAME2"] + " (" + result["KUNNR"].replace(/^0+/, '') + ")");
			
			customers.find("#company").data("kunnr", result["KUNNR"]);
			customers2.find("#company").data("kunnr", result["KUNNR"]);
			
			customers.find("#address").html(result["STRAS"] + " " + result["HOUSENUM"] + "<br/>" + result["PSTLZ"] + " " + result["ORT01"]);
			customers2.find("#address").html(result["STRAS"] + " " + result["HOUSENUM"] + "<br/>" + result["PSTLZ"] + " " + result["ORT01"]);
			
			customers.find("#cust_tel").html(result["TELF1"]);
			customers2.find("#cust_tel").html(result["TELF1"]);

			customers.find("#cust_fax").html(result["TELFX"]);
			customers2.find("#cust_fax").html(result["TELFX"]);

			customers.find("#cust_mail").html( "<a href='mailto:" + result["EMAIL"] + "'>" + result["EMAIL"] + "</a>" );
			customers2.find("#cust_mail").html( "<a href='mailto:" + result["EMAIL"] + "'>" + result["EMAIL"] + "</a>" );
			
			customers.find("#cust_url").html("<a href='" + cleanURL(result["KNURL"]) + "'>" + result["KNURL"] +  "</a>");
			customers2.find("#cust_url").html("<a href='" + cleanURL(result["KNURL"]) + "'>" + result["KNURL"] +  "</a>");

			if(result["AUFSD"] != "" && result["AUFSD"] != undefined){
				customers.find("#cust_aufsd").html(lang.AUFSD);
				customers2.find("#cust_aufsd").html(lang.AUFSD);
			} else {
				customers.find("#cust_aufsd").html("");
				customers2.find("#cust_aufsd").html("");
			}

			if(result["LIFSD"] != "" && result["LIFSD"] != undefined){
				customers.find("#cust_lifsd").html(lang.LIFSD);
				customers2.find("#cust_lifsd").html(lang.LIFSD);
			} else {
				customers.find("#cust_lifsd").html("");
				customers2.find("#cust_lifsd").html("");
			}

			var current_klimk = result["KLIMK2"];
			if(current_klimk == "X"){
				customers.find("#cust_vsbed").html(lang.KLIMK);
				customers2.find("#cust_vsbed").html(lang.KLIMK);
			}
			else{
				customers.find("#cust_vsbed").html("");
				customers2.find("#cust_vsbed").html("");
			}
			
			customers.find("#zterm").html(result["ZTERM"]+" - "+result["ZTERM_TEXT1"]+" "+result["ZTERM_TEXT2"]);
			customers.find("#sales_rep").html(result["SALES_REPR"]);
			var industryText = (result["BRTEXT"] != undefined) ? result["BRTEXT"] : result["BRSCH"];
			customers.find("#bran").html(industryText);
			var kdgrpText = (result["KTEXT"] != undefined) ? result["KTEXT"] : result["KDGRP"];
			customers.find("#kund").html(kdgrpText);

			result["JMZAH"] = (result["JMZAH"] == "") ? "0" : result["JMZAH"];
			var emplText = (result["JMJAH"] == "0000") ? "" : parseInt(result["JMZAH"]) +' ('+result["JMJAH"]+')';

			customers.find("#anza").html(emplText);
			customers.find("#sales_yr1").html(result["SALES_YEAR"].formatMoney(2) + " " +result["WAERS"]);
			customers.find("#sales_yr2").html(result["SALES_YEAR_1"].formatMoney(2) + " " +result["WAERS"]);
			customers.find("#sales_yr3").html(result["SALES_YEAR_2"].formatMoney(2) + " " +result["WAERS"]);
			customers.find("#sales_yr4").html(result["SALES_YEAR_3"].formatMoney(2) + " " +result["WAERS"]);
			customers.find("#konda").html(result["KONDA"]);

			// Fill the Sales poetential values
			//customers.find("#trnovr_year").html(customers.find("#trnovr_year").html() + "2020"); 
			var turnovr_year =  (result["TURNOVER_YEAR"] == "0000") ? "" : ( " (" + result["TURNOVER_YEAR"] + ")" ); 
			var turnovr_val =  (result["TURNOVER"] == 0) ? "" : (result["TURNOVER"].formatMoney(2) + " " + result["WAERS"]); 

			customers.find("#trnovr_value").html(turnovr_val +  turnovr_year );

			customers2.find("#klimk").html(result["KLIMK"].formatMoney(2) + " " +result["WAERS"]);
			customers2.find("#klprz").html(result["KLPRZ"]+" %");
			
			
			//lang.customer_header2[6]
			var result2 = result["BonusData"];
			var bonusesHtml = '<tr><td><div style="width: 42%;" class="item">'+lang.customer_header2[5]+'</div><div id="bonus">'+result["BONUS"].formatMoney(2)+ " " +result["WAERS"]+'</div></td><td>';

			bonusesHtml += '<div style="width: 68%;" class="item">'+lang.customer_header2[7]+'</div><div style="font-weight: normal;">'+result["SALESBONUS"].formatMoney(2)+ " " +result["WAERS"] + '</div>';
			bonusesHtml += '<div style="width: 68%; float: left;" class="item">'+lang.customer_header2[8]+'</div><div>'+result["DISTOSCLAE"].formatMoney(2)+ " " +result["WAERS"] + '</div>' + '</td></tr>';

			for(var j=0; j<result2.length; j++){
				
				var indicator = '';
				if (result2[j]['ACTIVE'] == "X")
					indicator += '<span class="bonusGreenBubble"></span>';

				if(j%2 == 0){
					bonusesHtml += '<tr><td>'+ indicator  +'<div style="width: 42%; float: left;" class="item">'+lang.customer_header2[6]+' '+result2[j]['SEQ_ID']+'</div><div id="bonus">'+result2[j]['AMOUNT'].formatMoney(2)+ ' ' +result["WAERS"]+' ' + '</div></td>';
				}
				else{
					bonusesHtml += '<td>'+ indicator  +'<div style="width: 42%; float: left;" class="item">'+lang.customer_header2[6]+' '+result2[j]['SEQ_ID']+'</div><div id="bonus">'+result2[j]['AMOUNT'].formatMoney(2)+ ' ' +result["WAERS"]+' ' + '</div></td></tr>';
				}
				
			}
			if((j-1)%2 == 0 && j > 0){
				bonusesHtml += '<td></td></tr>';
			}
			$('#bonusTableHolder tbody').html(bonusesHtml);
			
			$('#bonusTableHolder thead').off('click');
			$('#bonusTableHolder thead').on('click', 'th',function(){
				if ( $( "#bonusTable" ).is( ":hidden" ) ) {
					$( "#bonusTable" ).fadeIn();
				} else {
					$( "#bonusTable" ).fadeOut();
				}
			});
			customers2.find("#stocks_vol").html(result["STOCK_VOL"]);
			customers2.find("#stocks_obl").html(result["STOCK_OBL"]);
			customers2.find("#funds").html(result["FUNDS"].formatMoney(2)+ " " +result["WAERS"]);
			
			var customerIdr = result["KUNNR"];
			switch(lastSectionFromEdit){	
				case "#customers_contacts":
					//$('#customers_contacts').fadeIn();
					getContactListByCustomer(customerIdr);
					$("#pageDownNav li").removeClass("active");
					$('#but_contacts').addClass('active');
					break;
				// case "#customers_orders":
				// 	getSalesDocumentsListByCustomer(customerIdr);
				// 	$('#customers_orders').fadeIn(function(){
				// 		var isFromReminder = getParams().fromReminder;
				// 		if (isFromReminder != undefined)
				// 		{
				// 			if (!calledFromReminder) {
				// 				setTimeout(function(){
				// 					var incommingParams = getParams();
				// 					var salesOrderNumberReminder = incommingParams.salesOrderDocNumberReminder;
				// 					$('#customers_salesDocuments_table tr[data-docnumber="'+ salesOrderNumberReminder +'"]').click();
				// 					calledFromReminder = true;	
				// 				},100);
				// 			}
						
				// 		}	
					
				// 	});
				// 	$("#pageDownNav li").removeClass("active");
				// 	$('#but_orders').addClass('active');
				// 	break;
				case "#customers_sales_activities":
					getSalesActivityListByCustomer(customerIdr);
					$('#customers_sales_activities').fadeIn();
					$("#pageDownNav li").removeClass("active");
					$('#but_visits').addClass('active');
					break;
				case "#customers_reports":
					$('#but_reports').addClass('active');
					$(lastSectionFromEdit).fadeIn();
					break;
				case "#customers2":
					$(".header2").addClass("active");
					$(lastSectionFromEdit).fadeIn();
					break;
				default:
					$(lastSectionFromEdit).fadeIn();
					$(".header1").addClass("active");
					break;
			}
			
			if(getParams().edit_customerid != "" && typeof getParams().edit_customerid != 'undefined'){
				$("#customer_edit").click();
			}
		});
    }
}

function edit_update_industry2(param_ind1_rel){
	var ind2 = $("#edit_cust_ind2_hidden option");

	$("#edit_cust_ind2").html("");
	var result_select = "";
	for(i = 0; i < ind2.length; i++){
		if($(ind2[i]).data("rel") == param_ind1_rel) {
			var current = $(ind2[i])[0].outerHTML;
			result_select += current;
		}
	}
	$("#edit_cust_ind2").html("<option value='' selected disabled></option>" + result_select);
	$("#edit_cust_ind3").html("");
}

function edit_update_industry3(param_ind2_rel){

	var ind3 = $("#edit_cust_ind3_hidden option");

	$("#edit_cust_ind3").html("");
	var result_select = "";
	for(i = 0; i < ind3.length; i++){
		if($(ind3[i]).data("rel") == param_ind2_rel) {
			var current = $(ind3[i])[0].outerHTML;
			result_select += current;
		}
	}
	$("#edit_cust_ind3").html("<option value='' selected disabled></option>" + result_select);

}


function edit_getCustomerDetails(customerId){
    cordova.exec(fireEditCustomerDetails, execFailed, 'Customer', 'getCustomerDetails', [customerId]);

    function fireEditCustomerDetails(param){
    	var result = eval('(' + param + ')');
    	
		$(".customers_current:visible").fadeOut(function(){
			var edit_customer = $("#customers_editCustomer");
			var $edit_year_num_employee = edit_customer.find("#edit_year_num_employee");
			var $edit_turnovr_year = edit_customer.find("#edit_turnovr_year");

			edit_customer.find("#edit_cust_name").val(result["NAME1"]);
			edit_customer.find("#edit_cust_url").val(result["KNURL"]);
			edit_customer.find("#edit_cust_tel").val(result["TELF1"]);
			edit_customer.find("#edit_cust_fax").val(result["TELFX"]);
			edit_customer.find("#edit_cust_mail").val(result["EMAIL"]);
			edit_customer.find("#edit_cust_noe").val(parseInt(result["JMZAH"]));
			edit_customer.find("#edit_cust_noe").data('lastVal', parseInt(result["JMZAH"]));

			//edit_customer.find("#edit_turnovr_year").val((result["TURNOVER_YEAR"]));
			edit_customer.find("#edit_turnovr_value").val((result["TURNOVER"]).toString().replace('.',currentNumberDecimal));

			// Clear the options firts
			$edit_year_num_employee.children().remove();
			$edit_turnovr_year.children().remove();
			edit_customer.find("#edit_cust_industry").children().remove();

			var slectYearHTML =  '<option selected="selected" value="' + new Date().getFullYear() + '">' + new Date().getFullYear() + '</option>';
			var selectedYearKey = "";
			if (result["JMJAH"] != "0000" && result["JMJAH"] != "" && result["JMJAH"] !=  new Date().getFullYear()) 
			{
				slectYearHTML += '<option value="' + result["JMJAH"] + '">' + result["JMJAH"] + '</option>';
				//selectedYearKey = result["JMJAH"];
			}

			//Fill the Turnover year dropdown values
			var slectTurnoverYearHTML = '<option selected="selected" value="' + new Date().getFullYear() + '">' + new Date().getFullYear() + '</option>';

			
			if (result["TURNOVER_YEAR"] !=  "0000" && result["TURNOVER_YEAR"] != "" && result["TURNOVER_YEAR"] != new Date().getFullYear())
				slectTurnoverYearHTML += '<option value="' + result["TURNOVER_YEAR"] + '">' + result["TURNOVER_YEAR"] + '</option>';
			
			$edit_turnovr_year.append(slectTurnoverYearHTML);

			// if (result["TURNOVER_YEAR"] != "" && result["TURNOVER_YEAR"] != "0000") 
			// 	$edit_turnovr_year.val(result["TURNOVER_YEAR"]);
			// else
			$edit_turnovr_year.val(new Date().getFullYear());
			 
			// if (result["JMJAH"].toString() !=  (new Date().getFullYear()).toString() ) {
			// 	slectYearHTML += '<option selected="selected" value="' + new Date().getFullYear() + '">' + new Date().getFullYear() + '</option>';
			// }

			if (selectedYearKey == "")
				selectedYearKey = new Date().getFullYear();

			$edit_year_num_employee.append(slectYearHTML);
			//$edit_year_num_employee.val(selectedYearKey);

			cordova.exec(function(params){
				var resultss = eval('(' + params + ')');
				var current_industry = result["BRSCH"];
				var selectHtml = "";
				for(i = 0; i < resultss.length; i++){
					selectHtml += '<option value="' + resultss[i]["BRSCH"] + '">' + resultss[i]["BRTEXT"] + '</option>';
				}
				//Fire Defaults:
				var $edit_cust_industry = edit_customer.find("#edit_cust_industry");
				if(current_industry == "" || current_industry == undefined){
					$edit_cust_industry.append("<option value='' selected disabled></option>" + selectHtml);
				} else {
					$edit_cust_industry.append(selectHtml);
					//Set Default Value:
					var default_val = $("#edit_cust_industry option[value=" + current_industry + "]").val();
					$edit_cust_industry.val(default_val);
				}
				edit_customer.fadeIn();
			}, function(){
				edit_customer.fadeIn();	
			}, 'Customizing', 'getBRSCHList', []);
		});
    }
}


function editContactDetails(contact_id){
	cordova.exec(fireEditContactDetails, execFailed, 'Contact', 'getContactDetails', [contact_id]);
	
	function fireEditContactDetails(param) {
		//Clear All inputs:
		$("#customers_editContact :input").val("");
		$("#customers_editContact select").html("");

		var result = eval('(' + param + ')');

		var $edit_contact = $("#customers_editContact");
		$edit_contact.data("contactid", contact_id);
		$edit_contact.find("#edit_fname").val(result["NAMEV"]);
		$edit_contact.find("#edit_lname").val(result["NAME1"]);
		$edit_contact.find("#edit_tel").val(result["TEL1_NUMBR"]);
		$edit_contact.find("#edit_fax").val(result["FAX_NUMBER"]);
		$edit_contact.find("#edit_mobile").val(result["MOB_NUMBER"]);
		$edit_contact.find("#edit_email").val(result["E_MAIL"]);
		$edit_contact.find("#edit_notes").val(result["PARAU"]);
		
		var birthdayFull = result["GBDAT"];
		if ( birthdayFull !== "" ){
			$edit_contact.find("#edit_birthday").val(birthdayFull.substring(0, 4) + "-" + birthdayFull.substring(4, 6) + "-" +  birthdayFull.substring(6, 8));
		} else {
			$edit_contact.find("#edit_birthday").val("");
		}

		//Get Function Select:
		var current_function = result["PAFKT"];
		cordova.exec(function (param_select){
			var result_select = eval('(' + param_select + ')');
			//Get All Values:
			var selectHtml = "";
			for(i = 0; i < result_select.length; i++){
				selectHtml += '<option value="' + result_select[i]["PAFKT"] + '">' + result_select[i]["VTEXT"] + '</option>';
			}
			//Fire Defaults:
			var $edit_function = $("#edit_function");
			if(current_function == ""){
				$edit_function.append("<option value='' selected disabled></option>" + selectHtml);
			} else {
				$edit_function.append(selectHtml);
				//Set Default Value:
				var default_val = $("#edit_function option[value=" + current_function + "]").val();
				$edit_function.val(default_val);
			}
		}, execFailed, 'Customizing', 'getPAFKTList', ['']);
		
		
		//Get Hobby 1 Select:
		var current_hobby_1 = result["PARH1"];
		cordova.exec(function (param_select){
			var result_select = eval('(' + param_select + ')');
			//Get All Values:
			var selectHtml = "";
			for(i = 0; i < result_select.length; i++){
				selectHtml += '<option value="' + result_select[i]["PARH1"] + '">' + result_select[i]["VTEXT"] + '</option>';
			}
			//Fire Defaults:
			var $edit_hobby_1 = $("#edit_hobby_1");
			if(current_hobby_1 == ""){
				$edit_hobby_1.append("<option value='' selected disabled></option>");
				$edit_hobby_1.append(selectHtml);
			} else {
				$edit_hobby_1.append(selectHtml);
				//Set Default Value:
				var default_val = $("#edit_hobby_1 option[value=" + current_hobby_1 + "]").val();
				$edit_hobby_1.val(default_val);
			}
		}, execFailed, 'Customizing', 'getPARH1List', ['']);


		//Get Hobby 2 Select:
		var current_hobby_2 = result["PARH2"];
		cordova.exec(function (param_select){
			var result_select = eval('(' + param_select + ')');
			//Get All Values:
			var selectHtml = "";
			for(i = 0; i < result_select.length; i++){
				selectHtml += '<option value="' + result_select[i]["PARH2"] + '">' + result_select[i]["VTEXT"] + '</option>';
			}
			//Fire Defaults:
			var $edit_hobby_2 = $("#edit_hobby_2");
			if(current_hobby_2 == ""){
				$edit_hobby_2.append("<option value='' selected disabled></option>");
				$edit_hobby_2.append(selectHtml);
			} else {
				$edit_hobby_2.append(selectHtml);
				//Set Default Value:
				var default_val = $("#edit_hobby_2 option[value=" + current_hobby_2 + "]").val();
				$edit_hobby_2.val(default_val);
			}
		}, execFailed, 'Customizing', 'getPARH2List', ['']);


		//Get Children Select:
		var current_children = result["PARH3"];
		cordova.exec(function (param_select){
			var result_select = eval('(' + param_select + ')');
			//Get All Values:
			var selectHtml = "";
			for(i = 0; i < result_select.length; i++){
				selectHtml += '<option value="' + result_select[i]["PARH3"] + '">' + result_select[i]["VTEXT"] + '</option>';
			}
			//Fire Defaults:
			var $edit_children = $("#edit_children");
			if(current_children == ""){
				$edit_children.append("<option value='' selected disabled></option>");
				$edit_children.append(selectHtml);
			} else {
				$edit_children.append(selectHtml);
				//Set Default Value:
				var default_val = $("#edit_children option[value=" + current_children + "]").val();
				$edit_children.val(default_val);
			}
		}, execFailed, 'Customizing', 'getPARH3List', ['']);


		$(".customers_current").filter(":visible").fadeOut(function(){
			$edit_contact.fadeIn();
		});
	}
}

function getFunctionValue(current_function, cb){
	cordova.exec(function (param_select){
		pafkt_list = eval('(' + param_select + ')');
	}, execFailed, 'Customizing', 'getPAFKTList', ['']);
}

function getContactListByCustomer(customerId){
	cordova.exec(fireContactList, execFailed, 'Contact', 'getContactListByCustomerNr', [customerId]);
	
	function fireContactList(param){

		function record(contact_firstName, contact_lastName, contact_id, tel_number, mobile_number, email, contact_function){
			var contactRecord = "";
			contactRecord += "<tr data-contact_id=" + contact_id + "><td>";
			contactRecord += contact_firstName + " " + contact_lastName;
			contactRecord += "</td><td>";
			contactRecord += contact_function;
			contactRecord += "</td><td>";
			contactRecord += tel_number;
			contactRecord += "</td><td>";
			contactRecord += mobile_number;
			contactRecord += "</td><td>";
			contactRecord += "<a href='mailto:" + email + "'>" + email + "</a>";
			contactRecord += "</td></tr>";
			return contactRecord;
		};

		var result = eval('(' + param + ')');
		var result_html = "";
		for(i = 0; i < result.length; i++){
			var contact_firstName = result[i]["NAMEV"];
			var contact_lastName = result[i]["NAME1"];
			var contact_id = result[i]["PARNR"];
			var contact_tel = result[i]["TEL1_NUMBR"];
			var contact_mobile = result[i]["MOB_NUMBER"];
			var contact_email = result[i]["E_MAIL"];
			var contact_function = "";
			for(var f=0; pafkt_list.length>f;f++){
				if(result[i]["PAFKT"] == pafkt_list[f]["PAFKT"]){
					contact_function = pafkt_list[f]["VTEXT"];
				}
			}
			result_html += record(contact_firstName, contact_lastName, contact_id, contact_tel, contact_mobile, contact_email, contact_function);
		}

		$("#customers_contactsTable tbody").html(result_html);

		//Show screen if is hidden:
		if($(".customers_current").filter(":visible").length > 0){
			$(".customers_current").filter(":visible").fadeOut(function(){
				$("#customers_contacts").fadeIn();
			});	
		}
		else{
			$("#customers_contacts").fadeIn();
		}
	}
}


function getContactDetails(contact_id){
	cordova.exec(fireContactDetails, execFailed, 'Contact', 'getContactDetails', [contact_id]);
	
	function fireContactDetails(param) {
		var result = eval('(' + param + ')');
		var $person = $("#personDetails_holder");
		$person.data("contactid", contact_id);
		$person.data("parnr", result["PARNR"]);

		$person.find("#contact_name").html(result["NAMEV"] + " " + result["NAME1"]);
		$person.find("#cust_tel").text(result["TEL1_NUMBR"]);
		$person.find("#cust_fax").text(result["FAX_NUMBER"]);
		$person.find("#cust_mobile").text(result["MOB_NUMBER"]);
		$person.find("#cust_email").html( "<a href='mailto:" + result["E_MAIL"] + "'>" + result["E_MAIL"] + "</a>" );
		$person.find("#cust_notes").text(result["PARAU"]);
		$person.find("#cust_birthday").text("");
		if( result["GBDAT"] !== "" ){
			$person.find("#cust_birthday").text( formatDate(result["GBDAT"]) );
		}

		var cust_hobby1 = result["PARH1"];
		//In case nothing in Customizing is found:
		$person.find("#cust_hobby1").text("");
		if( cust_hobby1 !== "" ){
			cordova.exec(function(hobby1_PARH1){
				var hobby1_PARH1 = eval('(' + hobby1_PARH1 + ')');
				for(i = 0; i < hobby1_PARH1.length; i++){
					if(cust_hobby1 == hobby1_PARH1[i]["PARH1"]){
						$person.find("#cust_hobby1").text(hobby1_PARH1[i]["VTEXT"]);
					}
				}
			}, execFailed, 'Customizing', 'getPARH1List', ['']);
		}

		var cust_hobby2 = result["PARH2"];
		//In case nothing in Customizing is found:
		$person.find("#cust_hobby2").text("");
		if( cust_hobby2 !== "" ){
			cordova.exec(function(hobby2_PARH2){
				var hobby2_PARH2 = eval('(' + hobby2_PARH2 + ')');
				for(i = 0; i < hobby2_PARH2.length; i++){
					if(cust_hobby2 == hobby2_PARH2[i]["PARH2"]){
						$person.find("#cust_hobby2").text(hobby2_PARH2[i]["VTEXT"]);
					}
				}
			}, execFailed, 'Customizing', 'getPARH2List', ['']);
		}

		var cust_children = result["PARH3"];
		//In case nothing in Customizing is found:
		$person.find("#cust_children").text("");
		if( cust_children !== "" ){
			cordova.exec(function(children_PARH3){
				var children_PARH3 = eval('(' + children_PARH3 + ')');
				for(i = 0; i < children_PARH3.length; i++){
					if(cust_children == children_PARH3[i]["PARH3"]){
						$person.find("#cust_children").text(children_PARH3[i]["VTEXT"]);
					}
				}
			}, execFailed, 'Customizing', 'getPARH3List', ['']);
		}

		var cust_function = result["PAFKT"];
		//In case nothing in Customizing is found:
		$person.find("#cust_function").text("");
		if( cust_function !== "" ){
			cordova.exec(function(pafkt_param){
				var pafkt_param = eval('(' + pafkt_param + ')');
				for(i = 0; i < pafkt_param.length; i++){
					if(cust_function == pafkt_param[i]["PAFKT"]){
						$person.find("#cust_function").text(pafkt_param[i]["VTEXT"]);
					}
				}
			}, execFailed, 'Customizing', 'getPAFKTList', ['']);
		}

		//Show Screen:
		$(".customers_current").filter(":visible").fadeOut(function(){
			$("#personDetails_holder").fadeIn();
		});
	}
}

function removeReminderSalesDocument(salesDocNo)
{
	//Remove Reminder
	$("#salesDoc_ReminderDate").text("");
	cordova.exec(function(){
		            var errorDict = {};
					errorDict[lang.succ_del_reminder] = "success";
			        customAlert(errorDict);
				}, execFailed, 'Resubmission', 'deleteResub', [salesDocNo]);
}


function addReminderDateSalesDoc(vbeln,remindDate)
{
	// Append Reminder Date
	// Append Reminder Date
	var resub_array = {
			"BUKRS"	 		: resubBUKRS,
			"RESUB_TYP" 	: resub_TYP_Doc,
			"VBELN"     	: vbeln,
			"UNAME_AD"  	: current_username.toUpperCase(),
			"UNAME" 		: current_username.toUpperCase(),
			"RESUB_DATE"	: remindDate	
		};

		cordova.exec(function(){			
			var errorDict = {};
			errorDict[lang.succ_add_reminder] = "success";
			customAlert(errorDict);

	 }, execFailed, 'Resubmission', 'createResub', [resub_array]);	
}

function addReminderSalesDocument(salesDocNo,salesDocTyp,self,fromList)
{

	var salesActDocNo = salesDocNo;
	var self = self;
	var fromList = fromList;

	var resub_array = {
			"BUKRS"	 		: resubBUKRS,
			"RESUB_TYP" 	: resub_TYP_Doc,
			"VBELN"     	: salesActDocNo,
			"UNAME_AD"  	: current_username.toUpperCase(),
			"UNAME" 		: current_username.toUpperCase(),
			"RESUB_DATE"	: reminderDate	
		};

	//cordova.exec(function(){			

		$('#add_reminder_date').val(reminderDate);
		var buttonsOpts = {};
		buttonsOpts[lang.confirm] = function() {

			remDate = new Date($('#add_reminder_date').val());
			if(remDate == 'Invalid Date')
				return;

			addReminderDateSalesDoc(salesActDocNo, $('#add_reminder_date').val());
			$( "#selectDateReminder" ).dialog( "close" );
			
			$(self).addClass('favorite_activity');
			if (fromList) 
				$(self.parentNode.children[0]).text('X');
			else
			{
				$("#salesDoc_ReminderDate").text(formatDate($('#add_reminder_date').val().replace(/-/g,"")));
			}
			$("#customers_salesDocuments_table").trigger("update");		

		};

		buttonsOpts[lang.cancelButton] = function() {
			$( "#selectDateReminder" ).dialog( "close" );
		};

		$( "#selectDateReminder" ).dialog({
			resizable: false,
			height: 250,
			width: 450,
			modal: true,
			buttons : buttonsOpts,
			hide: "fade",
			show:  500 ,
			title: lang.addReminderDate
		});
		$('#add_reminder_date').blur();

	 //}, execFailed, 'Resubmission', 'createResub', [resub_array]);	

}

// $(document).on("click", "#customers_salesDocuments_table #salesDocStarDivList" ,function () {

// 	var self = this;
// 	var salesDocNo = $(self).attr('data-doc_num');
// 	var salesDocTyp = $(self).attr('data-doc_typ');
	
// 	if ($(self).hasClass('favorite_activity')) {
// 			$(self).removeClass('favorite_activity');		
// 			$(this.parentNode.children[0]).text('');
// 			removeReminderSalesDocument(salesDocNo);
// 			$("#customers_salesDocuments_table").trigger("update");	

// 	} else {
// 		// $(self).addClass('favorite_activity');
// 		//$(this.parentNode.children[0]).text('X');
// 		addReminderSalesDocument(salesDocNo,salesDocTyp,self,true);
// 	}

// });

// $(document).on("click", "#customers_salesDocuments #salesDocStarDivDtls" ,function () {
	
// 	var self = this;
// 	var salesDocNo = $(self).attr('data-doc_num');
// 	var salesDocTyp = $(self).attr('data-doc_typ');
// 	if ($(self).hasClass('favorite_activity')) {
// 			$(self).removeClass('favorite_activity');	
// 			removeReminderSalesDocument(salesDocNo);
// 			$("#customers_salesDocuments_table").trigger("update");	

// 	} else {
// 		// $(self).addClass('favorite_activity');	
// 		addReminderSalesDocument(salesDocNo,salesDocTyp,self,false);	
// 	}	

// });

// function addValiditySalesDocument(currentSalesDocDetails)
// {
// 	var currentSalesDocDetailsClone = jQuery.extend({}, currentSalesDocDetails);

// 	currentSalesDocDetailsClone.SalesOrderHeader = currentSalesDocDetailsClone.Header;
// 	currentSalesDocDetailsClone.SalesOrderItems = currentSalesDocDetailsClone.Items;

// 	delete currentSalesDocDetailsClone.Header;
//  	delete currentSalesDocDetailsClone.Items;

//  	if (currentSalesDocDetailsClone.SalesOrderHeader.DOC_TYPE != "AG")
//  	{
//  		cordova.exec(function(){
// 			var errorDict = {};
// 		  	errorDict[lang.succ_update_sd] = "success";
// 	        customAlert(errorDict);
// 	        getSalesOrderDetails(currentSalesDocDetails["Header"].DOC_NUMBER,0);		
// 		}, execFailed, 'Order', "updateSalesOrderValidity", [currentSalesDocDetailsClone]);
//  	}
//  	else
//  	{
//  		// Update Quotations
//  		cordova.exec(function(){
// 			var errorDict = {};
// 		  	errorDict[lang.succ_update_sd] = "success";
// 	        customAlert(errorDict);
// 	        getSalesOrderDetails(currentSalesDocDetails["Header"].DOC_NUMBER,1);		
// 		}, execFailed, 'OpenQuotation', "updateQuotationValidity", [currentSalesDocDetailsClone]);
//  	}

// };

// $(document).on("click", "#customers_salesDocuments #salesDoc_ValidityDate" ,function () {
	
// 	var salesDocValidity = new Date($('#salesDoc_ValidityDate').html());

// 	var validTo_day = salesDocValidity.getDate();
//     var validTo_month = salesDocValidity.getMonth() + 1;
//     var validTo_year = salesDocValidity.getFullYear();

//     if (validTo_month < 10) validTo_month = "0" + validTo_month;
//     if (validTo_day < 10) validTo_day = "0" + validTo_day;
//     var validDefault = validTo_year + "-" + validTo_month + "-" + validTo_day;

//     var salesDocDateUpdate ;

// 	var buttonsOpts = {};
// 	buttonsOpts[lang.confirm] = function() {

// 		salesDocValidity = new Date($("#add_validity_to").val());

// 		if(salesDocValidity == 'Invalid Date')
// 			return;

// 		validTo_day = salesDocValidity.getDate();
// 	    validTo_month = salesDocValidity.getMonth() + 1;
// 	    validTo_year = salesDocValidity.getFullYear();

// 	    if (validTo_month < 10) validTo_month = "0" + validTo_month;
//     	if (validTo_day < 10) validTo_day = "0" + validTo_day;

// 	    salesDocDateUpdate = validTo_year.toString() + validTo_month.toString() + validTo_day.toString();
		
// 		$( "#selectValidityTo" ).dialog( "close" );
// 		currentSalesDocDetails.Header["QT_VALID_T"] = salesDocDateUpdate;
// 		if (currentSalesDocDetails.Header["QT_VALID_F"] == "") 
// 		{
// 			currentSalesDocDetails.Header["QT_VALID_F"] = currentSalesDocDetails.Header["PURCH_DATE"]
// 		}
// 		addValiditySalesDocument(currentSalesDocDetails);
// 	};

// 	buttonsOpts[lang.cancelButton] = function() {		
// 		$( "#selectValidityTo" ).dialog( "close" );
// 	};

// 	$( "#selectValidityTo" ).dialog({
// 		resizable: false,
// 		height: 250,
// 		width: 450,
// 		modal: true,
// 		buttons : buttonsOpts,
// 		hide: "fade",
// 		show:  500 ,
// 		title: lang.changeSalesDocValidity
// 	});

//     $("#add_validity_to").val(validDefault);
// 	$('#add_validity_to').blur();	

// });


// function getSalesDocumentsListByCustomer(customerId){


// 	var resultLenCount = 0;
// 	var resultLenLoop = 0;

// 	cordova.exec(fireOrderList, execFailed, 'Order', 'getAllListForCustomer', [customerId]);
// 	function fireOrderList(param){
// 		//todo: sales activity id:
// 		function record(doc_number, doc_type, doc_purchNumber, netVal, doc_soldto, doc_said, indicaciq, kur, activityFavourite, salesDocType, validTo){
// 			if(indicaciq == "03"){
// 				var indicate = '<span class="listViewFlag" style="width: 13px; height: 13px; margin: 3px 5px 5px 0;"></span>';
// 			}
// 			else if(indicaciq == '01'){
// 				var indicate = '<span class="listViewFlag2" style="width: 13px; height: 13px; margin: 3px 5px 5px 0;"></span>';
// 			}
// 			else if(indicaciq == '02'){
// 				var indicate = '<span class="listViewFlag3" style="width: 13px; height: 13px; margin: 3px 5px 5px 0;"></span>';
// 			}
// 			else{
// 				var indicate = "";
// 			}

// 			if (doc_said != "")
// 				indicate += '<span class="listViewFlag_SalesActy" style="width: 13px; height: 13px; margin: 3px 5px 5px 0;"></span>';

// 			var showFavourite; // = doc_number.toString().charAt(0) != "*" ? "visible" : "hidden";
// 			var indicateStar = (activityFavourite == "") ? "" : "X" ; 

// 			if ((doc_number.toString().charAt(0) != "*") && (doc_number.toString().charAt(0) != "+")) 
// 				showFavourite = "visible";			
// 			else
// 				showFavourite = "hidden";

// 			var record = "";
// 			record += "<tr data-salesactivity='" + doc_said + "' data-soldto='" + doc_soldto + "' data-docnumber='" + doc_number + "' ";
// 			record += "><td>";
// 			record += indicate+doc_number.replace(/^0+/, '');
// 			record += "</td><td>";

// 			record += doc_purchNumber;
// 			record += "</td><td>";
// 			record += netVal.formatMoney(2) + " " + kur;
// 			record += "</td> <td> <span style='display:none'>"+ indicateStar +"</span>";
// 			record += '<div id="salesDocStarDivList" data-doc_num="'  + doc_number + '" data-doc_typ="' + salesDocType +'"class="item_star_doc_list ' + activityFavourite +'" style="visibility:'  +  showFavourite  +'">';
// 			record += '<svg id="salesDocStarSvg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" fill="#333" width="30px" height="30px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510; fill:grey;" xml:space="preserve">';
// 			record += '<polygon id="salesDocStar" points="255,402.212 412.59,497.25 370.897,318.011 510,197.472 326.63,181.738 255,12.75 183.371,181.738 0,197.472 139.103,318.011 97.41,497.25"/>';
// 			record += '</svg>';
// 			record += '</div>';

// 			record += '</td> <td>' + formatDate(validTo) + '</td>';

// 			record += "</tr>";
			
// 			return record;
// 		}

// 		function getDocTypes(result, i, callback){
// 			cordova.exec(setSalesDocTypesList, execFailed, 'Customizing', 'getAUARTList', ['']);
			
// 			function setSalesDocTypesList(param){
// 				var result2 = eval('(' + param + ')');
// 				//Set the global list:
// 				AUARTList = result2;
// 				var doc_type_text = result[i]["DOC_TYPE"];
// 				for( f = 0; f < result2.length; f++ ) {
// 					if( result[i]["DOC_TYPE"] == result2[f]["AUART"] ){
// 						doc_type_text = result2[f]["BEZEI"];
// 					}
// 				}
// 				callback(result, i, doc_type_text);
// 			}
// 		}
		
// 		var result = eval('(' + param + ')');
// 		resultLenLoop = result.length;
// 		var $salesDocumentsList = $("#customers_salesDocuments_table tbody");
// 		$salesDocumentsList.html("");
// 		for( i = 0; i < result.length; i++ ){
// 			getDocTypes(result, i, function(result, i, doc_type){
// 				if(result[i]["NET_VAL_HD"] == "" || result[i]["NET_VAL_HD"] == undefined || result[i]["NET_VAL_HD"] == null){
// 					var netValue = 0;
// 					var currenc = "EUR";
// 					function calNV(jj){
// 						if(result[i]['Items'] == undefined)
// 							return;
						
// 						if(result[i]['Items'].length == jj) 
// 							cont1();						
// 						var res2 = result[i]['Items'][jj];
// 						console.log(res2);
// 						netValue+=parseFloat(res2['NET_VALUE']);
// 						calNV(jj+1);
// 					}
// 					calNV(0);
// 				}
// 				else{
// 					var netValue = result[i]["NET_VAL_HD"];
// 					var currenc = result[i]["CURRENCY"];
// 					cont1();
// 				}
// 				function cont1(){

// 					resultLenLoop --;
// 					cordova.exec(function(favParam){

// 						resultLenCount++;
// 						var favResult = eval('(' + favParam + ')');
// 						var docisFavourite = favResult.length > 0 ? "favorite_activity" : "" ;

// 						$salesDocumentsList.append( record(result[i]["DOC_NUMBER"], doc_type, result[i]["BSTKD"], netValue, result[i]["SOLD_TO"], result[i]["VBELV"], result[i]["ORD_REASON"], currenc, docisFavourite, result[i]["DOC_TYPE"], result[i]["QT_VALID_T"]) );
// 						//Refresh the table sorter: Logic added because the result length is not always the number if entries displayed in the table.
// 						if ((resultLenCount +  resultLenLoop) == (result.length )) { 
// 							setTimeout(function(){
// 								$("#customers_salesDocuments_table").trigger("update");
// 							},100);
// 						}

// 					}, execFailed, 'Resubmission', 'getResubDetails', [ result[i]["DOC_NUMBER"] ]);
// 				}

// 			});
// 		}

// 	}
// }


function getSalesActivityListByCustomer(customerId){
	cordova.exec(fireSalesActivityList, execFailed, 'SalesActivity', 'getSalesActivityListByCustomerNr', [customerId]);
	function fireSalesActivityList(param){
		function record(sa_title, sa_type, sa_date, sa_dnum,followUp,docu_num){
			if(followUp != ''){
				var indicate = '<span class="listViewFlag" style="width: 13px; height: 13px; margin: 3px 5px 5px 0;"></span>';
			}
			else{
				var indicate = "";
			}
			var record = "";
			record += "<tr data-docnumberfull='"+sa_dnum+"'><td>";
			record += indicate+sa_title;
			/**remove activity type from table*/
			// record += "</td><td>";
			// record += sa_type;
			record += "</td><td>";
			record += formatDate(sa_date);
			record += "</td><td>";
			record += sa_dnum;
			record += "</td><td>";
			record += docu_num;
			record += "</td></tr>";
			return record;
		}
		
		var result = eval('(' + param + ')');

		var $sales_activities_table = $("#sales_activities_table");

		var sa_result = "";
		for(i = 0; i < result.length; i++){
			var sa_title 	= result[i]["ACTIVITY_COMMENT"].substring(0,40); //Note title, limited to 40
			var sa_type 	= result[i]["ACTIVITY_TYPE_TEXT"]; //Sales Activity Type 
			var sa_date 	= result[i]["FROM_DATE"]; //Date
			var sa_dnum		= result[i]["DOC_NUMBER"];
			var followUp    = result[i]["VBELV_SD"];
			var docu_num 	= "";
			if(result[i]["ORDER_NUMS"] != "")
				docu_num    = result[i]["ORDER_NUMS"];
			else
				docu_num    = result[i]["VBELV_SD"];

			// Get the Sales Document PO name
			

			sa_result += record(sa_title, sa_type, sa_date, sa_dnum,followUp,docu_num);
		}
			//Refresh the table sorter:
		$sales_activities_table.trigger("update");
		$sales_activities_table.find("tbody").html(sa_result);
		
	}
}


//Online Call: Get Product Details:
function getProductDetailsOnline(matnr, user, pass, success_get, get_failed){
	if( !doIhaveInternet() ){
		var errorDict = {};
        errorDict[lang.internet_err] = "error";
        customAlert(errorDict);
		return;
	}
	var data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions"><soapenv:Header/><soapenv:Body><urn:_-MSCMOBIL_-Z_PRODUCT_DETAIL><IM_BNAME>'+user+'</IM_BNAME><IM_MATNR>'+matnr+'</IM_MATNR></urn:_-MSCMOBIL_-Z_PRODUCT_DETAIL></soapenv:Body></soapenv:Envelope>';
	$.soap({
		url: baseOnlineURL+"/product_detail?sap-client="+sapClient,
		method: "_-MSCMOBIL_-Z_PRODUCT_DETAIL",
		soap12: false,
		data: data,
		wss: "",
		HTTPHeaders: {
			Authorization: 'Basic ' + btoa(user+':'+pass)
		},
		namespaceQualifier:  "myns",
		namespaceURL: "urn:_-MSCMOBIL_-Z_PRODUCT_DETAIL",
		noPrefix: false,
		elementName: "",
		enableLogging: false,
		success: function(SOAPResponse) {
			var resp = SOAPResponse.toString();
			//var result = eval('(' + $(resp).find("EX_JSON_RESULT").text() + ')');
			var result = $(resp).find("EX_JSON_RESULT").text();
			success_get(result);
		},
		error: function(SOAPResponse) {
			//get_failed(SOAPResponse.toString());
		}
	});
}


// function getSalesOrderDetails(current_salesDoc_nr, openQuot){
// 	var isOpenQuotation = openQuot;
// 	if(openQuot == 1){
// 		$('#quick_createSA2').show();
// 		cordova.exec(fireSalesOrderDetails, execFailed, 'OpenQuotation', 'getOpenQuotationDetails', [current_salesDoc_nr]);
// 	}
// 	else{
// 		cordova.exec(fireSalesOrderDetails, execFailed, 'Order', 'getSalesOrderDetails', [current_salesDoc_nr]);
// 	}
	
// 	function fireSalesOrderDetails(param){
// 		var result = eval('(' + param + ')');
// 		currentSalesDocDetails = result;
		
// 		var attaches = result['ATTACHMENTS']

// 		if(typeof attaches != "undefined" && attaches.length > 0){
// 			$('#attachments_links').css('opacity', '1');
// 			var html_string2 = "";
// 			for(iii=0;iii<attaches.length;iii++){
// 				html_string2 += '<tr><td><div style="width: 84%;" class="item"><a style="color: #069deb;" href="'+attaches[iii]['LINK']+'">'+attaches[iii]['DESCRIPTION']+'</a></div></td>';
// 			}
// 			$('#attaches_holder').html(html_string2);
// 			$('#attaches_holder').off('click');
// 			$('#attaches_holder').on('click', 'tr',function(){
// 				// window.location.href = $(this).find('a').attr('href');
// 				window.open($(this).find('a').attr('href'), "_system");
// 			});
// 			$('#attachments_links').on('click', function(){
// 				$('#dialog_attachments').dialog({
// 					dialogClass: "with-close-productPopup3",
// 					title: lang.productTableHeadings[2],
// 					show: 'fade',
// 					hide: "fade",
// 					width: 580,
// 					height: 600
// 				});

// 				$("#popUp_overlap").on("click", function(){
// 					$(this).fadeOut();
// 					$(".with-close-productPopup3 .ui-dialog-titlebar-close").click();
// 					$("#popUp_overlap").off("click");

// 					//Remove active:
// 					$('#pageAside ul li').removeClass("activeLongPress");
// 				}).fadeIn();

// 			    $("#dialog_attachments").dialog('option', 'title', lang.productTableHeadings[2]);
// 			});


// 		}
// 		else{
// 			$('#attachments_links').off('click');
// 			$('#attachments_links').css('opacity', '.7');
// 		}

// 		var $order_details = $("#customers_salesDocuments");
// 		$order_details.find("#salesDoc_number").data('vbeln', current_salesDoc_nr);
// 		if (result["Header"]["VBELV"] != undefined && result["Header"]["VBELV"] != "") {
// 			$order_details.find("#salesDoc_number").attr('data-activity_num',result["Header"]["VBELV"].toString());
// 			$order_details.find("#salesDoc_number").attr('data-customer_num',result["Header"]["SOLD_TO"].toString());
// 			$('#sales_activity_link').prop('disabled',false);
// 			$('#sales_activity_link').on('click', function(){
// 				$(".customers_current").filter(":visible").fadeOut(function(){
// 					var sales_act_number = $("#customers_salesDocuments").find("#salesDoc_number").attr('data-activity_num');
// 					var customer_number = $("#customers_salesDocuments").find("#salesDoc_number").attr('data-customer_num');
// 					// Fire  Sales Activity
// 					getSalesActivitiesDetails( sales_act_number, customer_number);
// 				});		
// 			});
// 			$('#sales_activity_link').css('opacity', '1');
// 		}
// 		else
// 		{
			
// 			$('#sales_activity_link').off('click');
// 			$('#sales_activity_link').css('opacity', '.7');
// 		}
		

// 		if(typeof result["Header"]["ORD_REASON"] != "undefined"){
// 			switch(result["Header"]["ORD_REASON"]){
// 				case "03":
// 					var indicate = '<span class="listViewFlag" style="width: 13px; height: 13px; margin: 3px 5px 5px 0;"></span>';
// 					break;
// 				case "01":
// 					var indicate = '<span class="listViewFlag2" style="width: 13px; height: 13px; margin: 3px 5px 5px 0;"></span>';
// 					break;
// 				case "02":
// 					var indicate = '<span class="listViewFlag3" style="width: 13px; height: 13px; margin: 3px 5px 5px 0;"></span>';
// 					break;
// 				default:
// 					var indicate = "";
// 					break;
// 			}	
// 		}
// 		else{
// 			var indicate = "";
// 		}
// 		$order_details.find("#salesDoc_number").html(indicate+current_salesDoc_nr.replace(/^0+/, ''));
// 		$order_details.find("#salesDoc_purchNumber").text(result["Header"]["BSTKD"]);
// 		if(result["Header"]["PURCH_DATE"].length > 2){
// 			$order_details.find("#salesDoc_orderDate").text( formatDate(result["Header"]["PURCH_DATE"]));
// 		}
// 		else{
// 			$order_details.find("#salesDoc_orderDate").text("");
// 		}

// 		if (result["Header"]["QT_VALID_T"] != undefined) {
// 				if(result["Header"]["QT_VALID_T"].length > 2){
// 					$order_details.find("#salesDoc_ValidityDate").text( formatDate(result["Header"]["QT_VALID_T"]));
// 			}
// 			else{
// 				$order_details.find("#salesDoc_ValidityDate").text("");
// 			}
// 		}
	
		
// 		$order_details.find("#salesDoc_ValidityDate").attr('data-salesDocNo',result["Header"]["DOC_NUMBER"]);

// 		// if (isOpenQuotation == 1) 
// 		// 	$order_details.find("#salesDoc_ValidityDate").parent().fadeOut();
// 		// else
// 		// 	$order_details.find("#salesDoc_ValidityDate").parent().fadeIn();

// 		if( result["Header"]["REQ_DATE_H"].length > 2 ){
// 			if ( result["Items"][0]["DLV_PRIO"] == "20" ){
// 				$order_details.find("#salesDoc_deliveryDate").text(formatDate(result["Header"]["REQ_DATE_H"]) + " (Fix)" );
// 			} else {
// 				$order_details.find("#salesDoc_deliveryDate").text( formatDate(result["Header"]["REQ_DATE_H"]) );
// 			}
// 		}
// 		else{
// 			$order_details.find("#salesDoc_deliveryDate").text("");
// 		}
		
// 		//Set the DocType:
// 		if( AUARTList.length > 0 ){
// 			//Set default:
// 			$order_details.find("#salesDoc_type").text(result["Header"]["DOC_TYPE"]);
// 			//Used the cashed values and find:
// 			for( i = 0; AUARTList.length > i; i++ ){
// 				if( result["Header"]["DOC_TYPE"] == AUARTList[i]["AUART"] ){
// 					$order_details.find("#salesDoc_type").text(AUARTList[i]["BEZEI"]);
// 					break;
// 				}
// 			}
// 		} else {
// 			//Get the values from customizing:
// 			cordova.exec(setSalesDocTypesList, execFailed, 'Customizing', 'getAUARTList', ['']);
// 			function setSalesDocTypesList(param){
// 				var doctypes_result = eval('(' + param + ')');
// 				//Set Default:
// 				$order_details.find("#salesDoc_type").text(result["Header"]["DOC_TYPE"]);
// 				//Find value:
// 				for( i = 0; doctypes_result.length > i; i++ ){
// 					if( result["Header"]["DOC_TYPE"] == doctypes_result[i]["AUART"] ){
// 						$order_details.find("#salesDoc_type").text(doctypes_result[i]["BEZEI"]);
// 						break;
// 					}
// 				}
// 			}
// 		}
		

// 		//Get Customer Details:
// 		var cust_soldto = result["Header"]["SOLD_TO"];
// 		cordova.exec(function(details_param){
// 			var details_param = eval('(' + details_param + ')');

// 			var customers_info_roll_1 = details_param["NAME1"] + " " + details_param["NAME2"] + " (" + cust_soldto.replace(/^0+/, '') + "),";

// 			var customers_info_roll_2;
// 			if( details_param["STRAS"].length > 0 && details_param["HOUSENUM"].length > 0 ){
// 				customers_info_roll_2 = "<br />" + details_param["STRAS"] + " " + details_param["HOUSENUM"] + ",";
// 			} else if( details_param["STRAS"].length == 0 && details_param["HOUSENUM"].length == 0 ){
// 				customers_info_roll_2 = "";
// 			} else if( details_param["STRAS"].length > 0 && details_param["HOUSENUM"].length == 0 ){
// 				customers_info_roll_2 = "<br />" + details_param["STRAS"] + ",";
// 			} else if( details_param["STRAS"].length == 0 && details_param["HOUSENUM"].length > 0 ){
// 				customers_info_roll_2 = "<br />" + details_param["HOUSENUM"] + ",";
// 			}

// 			var customers_info_roll_3;
// 			if( details_param["PSTLZ"].length > 0 && details_param["ORT01"].length > 0 ){
// 				customers_info_roll_3 = "<br />" + details_param["PSTLZ"] + " " + details_param["ORT01"];
// 			} else if( details_param["PSTLZ"].length == 0 && details_param["ORT01"].length == 0 ) {
// 				customers_info_roll_3 = "";
// 			} else if( details_param["PSTLZ"].length > 0 && details_param["ORT01"].length == 0 ){
// 				customers_info_roll_3 = "<br />" + details_param["PSTLZ"];
// 			} else if ( details_param["PSTLZ"].length == 0 && details_param["ORT01"].length > 0 ){
// 				customers_info_roll_3 = "<br />" + details_param["ORT01"];
// 			}
			
// 			$order_details.find("#salesDoc_customer").html(customers_info_roll_1 + customers_info_roll_2 + customers_info_roll_3);
		
// 		}, execFailed, 'Customer', 'getCustomerDetails', [cust_soldto]);

// 		function salesDocument_tableRecord(conUnit, material_number, quantity, conVal, sales_unit, price_per_unit, roll_total, product_name, vbeln, posnr, nr, altern,discntPercent){
// 			if(typeof altern != 'undefined' && altern != '' && altern != "000000"){
// 				var indi = '<span class="listViewFlag3" style="width: 13px; height: 13px; margin: 2px 5px 5px 0;"></span>';
// 			}
// 			else{
// 				var indi = '';	
// 			}
// 			var record = "";
// 			(posnr == "") ? posnr=0 : '';
// 			record += '<tr data-vbeln="'+vbeln+'" data-matnr="'+material_number+'" data-meins="'+conUnit+'" data-kbetr="'+conVal+'" data-posnr="'+posnr+'"><td>';
// 			record += indi+material_number.replace(/^0+/, '');
// 			record += "</td><td>";
// 			record += product_name;
// 			record += "</td><td>";
// 			record += quantity;
// 			record += "</td><td>";
// 			record += sales_unit;
// 			record += "</td><td>";
// 			record += price_per_unit;
// 			record += "</td><td>" +  discntPercent + " %" +"</td><td data-realval='" + roll_total + "'>";
// 			record += roll_total;
// 			record += "</td></tr>";
			
// 			return record;
// 		}

// 		function getProductNameAndUnits(result, i, callback){
// 			//Check if product comes from the online service:
// 			cordova.exec(function(param){
// 				function return_back_when_done(callback2){

// 					if( param == "X" ){
// 						cordova.exec(setProductDetails, execFailed, 'Product', 'getProductDetails', [ result["Items"][i]["MATERIAL"] ]);
// 					} else {
// 						getProductDetailsOnline(result["Items"][i]["MATERIAL"], current_username, current_password, successful_get, execFailed);
// 						function successful_get(data){
// 							setProductDetails(data, 1);
// 						}
// 					}
// 					function setProductDetails(param, online_call){
// 						var productDetails = eval('(' + param + ')');
// 						//Because the names from the back-end are messed up :(
// 						if(online_call == 1){
// 							current_unitsOfMeasure = productDetails["UNITS"];
// 							var cItems = productDetails['COND_ITEMS'];
// 						}
// 						else {
// 							current_unitsOfMeasure = productDetails["Units"];
// 							var cItems = productDetails['ConditionItems'];
// 						}
// 						if(result["Header"]["CREATED_BY"] != "" && result["Header"]["CREATED_BY"].length > 0){
// 							product_name = result["Items"][i]["SHORT_TEXT"];	
// 						}
// 						else{
// 							product_name = productDetails["MAKTX"] + "<br />" + productDetails["MAKTX2"];
// 						}
// 						var sales_unit_text = "";
// 						var sales_unit_factor = "";
// 						for(f = 0; f < current_unitsOfMeasure.length; f++ ){
// 							if( result["Items"][i]["SALES_UNIT"] == current_unitsOfMeasure[f]["ALT_UNIT"] ){
// 								sales_unit_text = current_unitsOfMeasure[f]["ALT_UNIT"];
// 								sales_unit_factor = current_unitsOfMeasure[f]["FACTOR"];
// 							}
// 						}
// 						//Add the factor + default unit:
// 						if ( typeof productDetails["SalesRels"] != 'undefined' && productDetails["SalesRels"].length > 0 && typeof productDetails["SalesRels"][0]["VRKME"] != 'undefined' ){
// 							sales_unit_text += "<br />" + sales_unit_factor + " " + productDetails['BASE_UOM'];
// 						}

// 						callback2(result, i, product_name, sales_unit_text, cItems);
// 					}
				
// 				}
// 				return_back_when_done(function(result, i, product_name, sales_unit_text, cItems){
// 					callback(result, i, product_name, sales_unit_text, cItems);
// 				})
// 			}, execFailed, 'Product', 'isProductLocal', [ result["Items"][i]["MATERIAL"] ]);

// 		}
		
// 		var salesDocument_table_html = "";
// 		$salesDocItemsTable = $("#customers_salesDocuments_ordersTable tbody");
// 		$salesDocItemsTable.html("");
// 		//result = result["Items"];
// 		for(i = 0; i < result["Items"].length; i++){
// 			//Use callback to get the product name:
// 			getProductNameAndUnits(result, i, function(result, i, product_name, sales_unit_text, cItems){
// 				var material_number = result["Items"][i]["MATERIAL"];
// 				var vbeln 			= result["Header"]["DOC_NUMBER"];
// 				var posnr 			= result["Items"][i]["ITM_NUMBER"];
// 				var altern 			= result["Items"][i]["ALTERN_ITM"];
// 				var quantity 		= result["Items"][i]["REQ_QTY"];
// 				var conUnit         = result["Items"][i]["SALES_UNIT"];
// 				var conVal 			= result["Items"][i]["COND_VALUE"];
// 				var price_per_unit 	= result["Items"][i]["COND_VALUE"].formatMoney(2) + " &#0128; <br />/ " + result["Items"][i]["COND_P_UNT"] + " " + cItems[0]["KMEIN"];
// 				var roll_total 		= result["Items"][i]["NET_VALUE"].formatMoney(2) + " &#0128;";
// 				var discntPercent 	= result["Items"][i]["DISCOUNT_VALUE"];
					
// 				$salesDocItemsTable.append(salesDocument_tableRecord(conUnit, material_number, quantity, conVal, sales_unit_text, price_per_unit, roll_total, product_name, vbeln, posnr, i,altern,discntPercent));
// 				$salesDocItemsTable.find('tr').sort(function (a, b) {
// 				    return +a.dataset.seq_nr - +b.dataset.seq_nr;
// 				}).appendTo( $salesDocItemsTable );
				
// 			});
// 		}
		
// 		$(".customers_current").filter(":visible").fadeOut(function(){
// 			//Value calculated by SAP:
// 			if(result["Header"]["CREATED_BY"] != "" && result["Header"]["CREATED_BY"].length > 0){
// 				$("#total_price").html( result["Header"]["NET_VAL_HD"].formatMoney(2) + " &#0128;" );
// 			}
// 			else{
// 				var total_price = 0;
// 				$salesDocItemsTable.find("tr").each(function(){
// 					var value = $(this).find("td:last").data("realval").replace(" €", "");
// 					value = value.replace(",", ".");
// 					total_price += parseFloat( value );
// 				});
// 				$("#total_price").html( total_price.formatMoney(2) + " &#0128;" );
// 			}

// 			$("#total_items").text( $salesDocItemsTable.find("tr").length );
// 			$order_details.fadeIn();
// 		});

// 		// Show hide Reminder favourite button if sales document is not released
// 		// if ((result["Header"]["DOC_NUMBER"].toString().charAt(0) != "*") && (result["Header"]["DOC_NUMBER"].toString().charAt(0) != "+"))
// 		// {
// 		// 	var classString = "salesDocNo_";

// 		// 	$('#customers_salesDocuments #salesDocStarDivDtls').show();
			
// 		// 	$('#customers_salesDocuments #salesDocStarDivDtls').attr('data-doc_num',result["Header"]["DOC_NUMBER"].toString());
// 		// 	$('#customers_salesDocuments #salesDocStarDivDtls').attr('data-doc_typ',result["Header"]["DOC_TYPE"].toString());
// 		// }
// 		// else
// 		// {
// 		// 	$('#customers_salesDocuments #salesDocStarDivDtls').hide(); 	
// 		// }



// 		cordova.exec(function(favParam){
// 			var favResult = eval('(' + favParam + ')');
// 			if(favResult.length > 0 )
// 			{
// 				$('#customers_salesDocuments #salesDocStarDivDtls').addClass('favorite_activity');
// 				$order_details.find("#salesDoc_ReminderDate").text(  formatDate(favResult[0]["RESUB_DATE"])   );
// 			} 
// 			else
// 			{
// 				$('#customers_salesDocuments #salesDocStarDivDtls').removeClass('favorite_activity');
// 				$order_details.find("#salesDoc_ReminderDate").text("");
// 			}

// 		}, execFailed, 'Resubmission', 'getResubDetails', [ result["Header"]["DOC_NUMBER"] ]);
		
// 	}
// }
// 
function getSalesActivitiesDetails(sales_activity_nr, customer_number){
	if (sales_activity_nr != "" && customer_number !="")
	{
		window.location = "sales_activities.html?uid=" + customer_number + "&salesActivityDocNumber=" + sales_activity_nr + "&fromSalesDocuments=true" ;
	}
	
}

// function getSalesActivitiesDetails(sales_activity_nr){
// 	var gray_field = "";

// 	cordova.exec(fireSaleActivityDetails, execFailed, 'SalesActivity', 'getSalesActivityDetails', [sales_activity_nr]);


// 	function set_customer_name (customer, sa_type, resultat, contact, status){
// 		cordova.exec(fireContactDetails, execFailed, 'Contact', 'getContactDetails', [contact]);
// 		cordova.exec(fireGetResult, execFailed, 'Customizing', 'getKTAERList', ['']);

// 		var gray_field = sa_type + '<br/><span id="current_customer_name">' + customer + '</span><br/>';
			
// 		function fireContactDetails (param){
// 			var result = eval('(' + param + ')');
// 			if(typeof result['NAMEV'] != "undefined"){
// 				var contact_name = result['NAMEV'] +' '+ result['NAME1'];
// 			}
// 			else{
// 				var contact_name = lang.no_contact;
// 			}
						
// 			gray_field += contact_name + '<br/>'+ status + '<br/>';
// 		}
			
// 		function fireGetResult (param){
// 			var result = eval('(' + param + ')');
// 			for(i = 0; i < result.length; i++){
// 				if(result[i]["KTAER"] == resultat){
// 					gray_field += result[i]["VTEXT"];
// 					break;
// 				}
// 			}
// 			$('#details_SA').find("span").html(gray_field);
// 		}
// 	}
	
// 	function fireSaleActivityDetails(param){

// 		var result = eval('(' + param + ')');

// 		$("#sales_activities").fadeIn(500,function(){
			
// 		//$(".customers_current").filter("visible").fadeOut(function(){
// 			var sales_activities = $("#sales_activities");
// 			if (param == "{}")
// 			{
// 				 sales_activities.find("#company").html(lang.no_data);
// 				 sales_activities.find("#user").html(lang.no_data);
// 				 sales_activities.find("#start_date").html(lang.no_data);
// 				 sales_activities.find("#start_time").html(lang.no_data);
// 				 sales_activities.find("#end_time").html(lang.no_data);
// 				 sales_activities.find("#free_text").html(lang.no_data);
// 				 $('#details_SA').find("span").html("");
// 				return;
// 			}
// 			var from_date_s = result["FROM_DATE"].toString();
// 			var converted_date = from_date_s.charAt(0)+from_date_s.charAt(1)+from_date_s.charAt(2)+from_date_s.charAt(3)+'-'+from_date_s.charAt(4)+from_date_s.charAt(5)+'-'+from_date_s.charAt(6)+from_date_s.charAt(7);
// 			if(result["FROM_TIME"].toString().length > 2 && result["FROM_TIME"].toString() != "000000"){ 
// 				var from_time_s = result["FROM_TIME"].toString();
// 				var converted_ftime = from_time_s.charAt(0)+from_time_s.charAt(1)+':'+from_time_s.charAt(2)+from_time_s.charAt(3);
// 			}
// 			else{
// 				var converted_ftime = "";
// 			}
// 			if(result["TO_TIME"].toString().length > 2 && result["TO_TIME"].toString() != "000000"){	
// 				var to_time_s = result["TO_TIME"].toString();
// 				var converted_ttime = to_time_s.charAt(0)+to_time_s.charAt(1)+':'+to_time_s.charAt(2)+to_time_s.charAt(3);
// 			}
// 			else{
// 				var converted_ttime = "";
// 			}

			
// 			 var from_time = result["FROM_TIME"];
// 			 var to_time = result["TO_TIME"];
			

// 			set_customer_name(result["NAME1"], result["ACTIVITY_TYPE_TEXT"], result["RESULT"], result["CONTACT"], result["ACTIVITY_STATUS_TEXT"]);

// 			var sales_act_note = result['ACTIVITY_COMMENT'];

// 			if(sales_act_note.length > 40){
// 				var sanote = sales_act_note.substring(0,40);
// 			}
// 			else{
// 				var sanote = sales_act_note;
// 			}
// 			 sales_activities.find("#company").html(sanote+'<input type="hidden" id="edit_details" value="'+result["PARTN_ID"]+'_'+result["ACTIVITY_TYPE"]+'_'+result["DOC_NUMBER"]+'_'+result["CONTACT"]+'_'+result["RESULT"]+'_'+result["NAME1"]+'">');
// 			 sales_activities.find("#user").html(lang.created_by + result["CREATED_BY"]);
// 			 sales_activities.find("#start_date").html(formatDate(result["FROM_DATE"]));
// 			 if(from_time.length < 2){
// 			 	sales_activities.find("#start_time").html('');
// 			 }
// 			 else{
// 			 	sales_activities.find("#start_time").html(from_time.charAt(0)+from_time.charAt(1)+':'+from_time.charAt(2)+from_time.charAt(3));	
// 			 }
// 			 sales_activities.find("#end_date").html(formatDate(result["TO_DATE"]));
// 			 if(to_time.length < 2){
// 			 	sales_activities.find("#end_time").html('');
// 			 }
// 			 else{
// 			 	sales_activities.find("#end_time").html(to_time.charAt(0)+to_time.charAt(1)+':'+to_time.charAt(2)+to_time.charAt(3));
// 			 }
			 
// 			 sales_activities.find("#free_text").html(result["ACTIVITY_COMMENT"]);
			 
// 		});
// 	}	

// }


function calculate_ddp(){
	$(".ex_gga").text(exch_q2.toFixed(2) + " " + ex_total_currenty);
	$(".ex_gr").text(exch_q1.toFixed(2) + " " + ex_total_currenty);
	// /if($('#ex_ddp').val() == "") $('#ex_ddp').val(0);
	ddp = parseFloat(ddp_g)/100;
	$('#ddp_l').text(ddp_g);
	if(isNaN(ddp)) ddp=0;
	ddp = 1 - ddp;
	if(ddp > 1) ddp = 1;
	if(ddp < 0) ddp = 0;
	difff = parseFloat(((exch_q1*ddp)-exch_q2)).toFixed(2);
	if(difff > 0){
		var final_diff = '<span style="color: green">'+ difff + " " + ex_total_currenty+'</span>';
	}
	else{
		var final_diff = '<span style="color: red">'+ difff + " " + ex_total_currenty+'</span>';
	}
	$(".ex_gd").html(final_diff);
}

$(function(){
	$('#ex_ddp').on('keyup', function(){
		calculate_ddp();
	})
});

function addMarker(map, myLocation, label, html, default_pin) {
	var marker = new google.maps.Marker({
		position: myLocation,
		animation: google.maps.Animation.DROP,
		icon: default_pin, 
		map: map,
		title: label
	});
	
	infowindow = new google.maps.InfoWindow();

	google.maps.event.addListener(marker, 'click', function() {
		infowindow.setContent(html + label);
		infowindow.open(map, marker);
	});
	
	return marker;
}
function clearAllMarkers(){
	for (var i = 0; i < markersArray.length; i++ ) {
		markersArray[i].setMap(null);
	}
	markersArray = [];
}

var map, customer_lat, customer_long, customerLocation, myLocation, directionsDisplay, directionsService;
var markersArray = [];
function getCustomerLocation(customerId){
	cordova.exec(getCustomerLongLat, execFailed, 'Customer', 'getCustomerDetails', [customerId]);

	function getCustomerLongLat(param){
		var result = eval('(' + param + ')');
		customer_lat = result["MSC_GEOLAT"];
		customer_long = result["MSC_GEOLON"];
		customerLocation = new google.maps.LatLng(result["MSC_GEOLAT"], result["MSC_GEOLON"]);
		navigator.geolocation.getCurrentPosition(getMyLocation, onGpsError);

		function getMyLocation(position){
			myLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			if($("#map-canvas").html() == ""){
				fireMap(customerLocation, myLocation);
			} else {
				$("#directions-panel").html("");
				calcRoute(customerLocation, myLocation);
			}
		}
	}

	function fireMap(customerLocation, myLocation) {
		var mapOptions = {
			center: myLocation,
			zoom: 12,
			disableDefaultUI: true,
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.SMALL,
				position: google.maps.ControlPosition.LEFT_TOP
			},
			mapTypeControl: true,
			streetViewControl: true,
			overviewMapControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

		//Directions:
		directionsDisplay = new google.maps.DirectionsRenderer();
		directionsService = new google.maps.DirectionsService();
		directionsDisplay.setMap(map);

  		calcRoute(customerLocation, myLocation);
	}
}

function calcRoute(customerLocation, myLocation) {
	directionsDisplay.setPanel(document.getElementById('directions-panel'));
	
	var request = {
		origin: myLocation,
		destination: customerLocation,
		travelMode: google.maps.TravelMode.DRIVING,
	};
	
	directionsService.route(request, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			//Clear All Overlays
			clearAllMarkers();
			directionsDisplay.setDirections(response);
			directionsDisplay.setMap(map);
			//Add Driving Route
			var route = response.routes[0].legs[0];
			$("#contacts_map").animate({"opacity": 1});
			$("#directions-panel").fadeIn();
		}
		else if(customer_lat == 0 && customer_long == 0) {
			//Clear All Overlays
			clearAllMarkers();
			directionsDisplay.setMap(null);
			
			markersArray.push(addMarker(map, myLocation, "You are here!", "", 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|FE7569'));
			map.panTo(markersArray[0].getPosition());
			$("#directions-panel").fadeOut();
			$("#contacts_map").animate({"opacity": 1});

            var errorDict = {};
            errorDict[lang.customAlert_noLocCustomer] = "error";
            customAlert(errorDict);

			return false;
		}
		else {
			//Clean Al Overlays:
			clearAllMarkers();
			directionsDisplay.setMap(null);

			markersArray.push(addMarker(map, customerLocation, "", "", 'img/default_pin.png'));
			markersArray.push(addMarker(map, myLocation, "You are here!", "", 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|FE7569'));
			map.panTo(markersArray[0].getPosition());
			$("#directions-panel").fadeOut();
			$("#contacts_map").animate({"opacity": 1});

			map.setZoom(15);
                            
            var warnDict = {};
            warnDict[lang.customAlert_noRouteCustomer] = "warning";
            customAlert(warnDict);
		
			return false;
		}
	});
}


function searchCustomer(searchTerm, sact){
	//Get the VSBED Values:
	/*var result_vsbed;
	cordova.exec(function (param_vsbed){
		result_vsbed = eval('(' + param_vsbed + ')');*/
		cordova.exec(fireSearchCustomer, execFailed, 'Customer', 'searchCustomer', [searchTerm]);
	//}, execFailed, 'Customizing', 'getVSBEDList', ['']);
	
	function fireSearchCustomer(param){
		function record(client_name, client_id, client_address, client_postalcode, client_lisfd, client_ausfd, client_klimk, client_visit_date, client_street, client_house_num, is_member){

			//Check if we need to display red dot:
			var indicator = '';
			if ( client_lisfd != "" || client_ausfd != "" || client_klimk != ""){
				indicator += '<span class="listViewFlag"></span>';
			}

			if ( is_member == "X"){
				indicator += '<span class="listViewFlag2"></span>';
			}

			var clientRecord = "";
			clientRecord += '<li><a href="#"><span class="name">';
			clientRecord += indicator + client_name;
			clientRecord += '</span><div class="arrow group"><span data-kunnr="'+client_id+'" class="number">';
			clientRecord += client_id.replace(/^0+/, '');
			clientRecord += '</span></div><span class="address">';
			clientRecord += client_postalcode + " " + client_address;
			clientRecord += '<br />';
			clientRecord += client_street + " " + client_house_num;
            if (client_visit_date != "")
                clientRecord += '</span></a></li>';
		
			return clientRecord;
		};

		// Set the All Customers Tab Active.
		$('#aside_nav_cust a').removeAttr('class');
		$("#all_customers").addClass('active');
	
		var result = eval('(' + param + ')');
		var result_html = "";
		
		for(i = 0; i < result.length; i++){
			var client_name = result[i]["NAME1"];
			var client_id = result[i]["KUNNR"];
			var client_address = result[i]["ORT01"];
			var client_postalcode = result[i]["PSTLZ"];
			var client_lisfd = result[i]["LIFSD"];
			var client_ausfd = result[i]["AUFSD"];
			var client_klimk = result[i]["KLIMK2"];
            var client_visit_date = result[i]["LAST_VISIT_DATE"];
            var client_street     = result[i]["STRAS"];
            var client_house_num  = result[i]["HOUSENUM"];
            var is_member  = result[i]["IS_MEMBER"];
			result_html = result_html + record(client_name, client_id, client_address, client_postalcode, client_lisfd, client_ausfd, client_klimk, client_visit_date, client_street, client_house_num, is_member);
		}
		$("#pageAside ul").html(result_html);


		//Check if there are 0 results and hide the quick nav, else click on the first customer:
		if ( result.length == 0 ){
			$(".quickNav, #pageDownNav").fadeOut();
			$(".customers_current").filter(":visible").fadeOut(function(){
				$("#blank_screen").fadeIn();
			});
		} else {
			$(".quickNav, #pageDownNav").fadeIn();
			//Click on the first customer after search:
			$("#pageAside ul li:first-child a").click();
		}
		

		//Check if Customer from Home Screen:
		var incommingParams = getParams();
		var checkIfHomeScreen = incommingParams.customerFromHomeScreen;
		if(checkIfHomeScreen != undefined && checkIfHomeScreen != "" && ifSearchIsExecutedAtFirst == 0){
			$("#pageAside ul li:first a").click();
			$("#search_box").val(checkIfHomeScreen);
			$('#loadingBar').fadeOut(500);
			ifSearchIsExecutedAtFirst = 1;
		}

		var checkIfAroundMeScreen = incommingParams.customerFromAroundMeScreen;
		if(checkIfAroundMeScreen != undefined && checkIfAroundMeScreen != "" && ifSearchIsExecutedAtFirst == 0){
			$("#pageAside ul li:first a").click();
			$("#search_box").val(checkIfAroundMeScreen);
			$('#loadingBar').fadeOut(500);
			ifSearchIsExecutedAtFirst = 1;
		}

		var checkIfSalesActivitiesScreen = incommingParams.salesAct_customerid;
		if(checkIfSalesActivitiesScreen != undefined && checkIfSalesActivitiesScreen != "" && ifSearchIsExecutedAtFirst == 0){
			//Commented Sandeep issue when navigating from reminder
			//$("#pageAside ul li:first a").click();
			$("#search_box").val(searchTerm);
			$('#loadingBar').fadeOut(500);
			ifSearchIsExecutedAtFirst = 1;
		}

		var checkIfEdit_customer = incommingParams.edit_customerid;
		if(checkIfEdit_customer != undefined && checkIfEdit_customer != "" && ifSearchIsExecutedAtFirst == 0){
			$("#pageAside ul li:first a").click();
			$("#search_box").val(checkIfEdit_customer);
			$('#loadingBar').fadeOut(500);
			ifSearchIsExecutedAtFirst = 1;
		}
		if( sact == 1 ){
			$('#pageAside').find('li a').click();
			setTimeout(function(){$('#but_visits').click()}, 500);
			$("#search_box").val(searchTerm);
			already_used = 1;
		}

		var openQuotation_homeScreen = incommingParams.openq;
		if(typeof openQuotation_homeScreen != 'undefined' && openQuotation_homeScreen != "" && ifSearchIsExecutedAtFirst == 0){
			$("#search_box").val(searchTerm);
			lastSectionFromEdit = "#customers_orders";
			
			//Go for the open quotation check:
			cordova.exec(checkSuccess, execFailed, 'OpenQuotation', 'isOpenQuotation', [ openQuotation_homeScreen ]);
			function checkSuccess(answer){
				(answer=="X") ? getSalesOrderDetails(openQuotation_homeScreen, 1) : getSalesOrderDetails(openQuotation_homeScreen);
			}

			//Adjust the back button:
			$("#back_salesOrders").hide();
			$("#back_to_home").on("click", function(){
				window.location = "home.html";
			}).show();

			//Set Active Tab:
			$("#pageDownNav li").removeClass("active");
			$("#but_orders").addClass("active");
			
			//Hide the loading bar:
			$('#loadingBar').fadeOut(500);
			ifSearchIsExecutedAtFirst = 1;
		}

		if( typeof incommingParams.SalesDocList_cust != 'undefined' && incommingParams.SalesDocList_cust != "" ) {
			$("#search_box").val(searchTerm);
			lastSectionFromEdit = "#customers_orders";
			$("#but_orders").trigger("click");

			//Hide the loading bar:
			$('#loadingBar').fadeOut(500);
			ifSearchIsExecutedAtFirst = 1;
		}

		//Scroll to the fist record:
		$("#pageAside").scrollTo("#pageAside .active", 300);
	}
}


$(document).ready(function(){
	var incommingParams = getParams();
	var checkIfHomeScreen = incommingParams.customerFromHomeScreen;
	var checkIfAroundMeScreen = incommingParams.customerFromAroundMeScreen;
	var checkIfOpenQuot = incommingParams.openq;
	var checkIfCreateSalesDoc = incommingParams.SalesDocList_cust;

	var salesOrderNumberReminder = incommingParams.salesOrderDocNumberReminder;

	if (salesOrderNumberReminder != undefined)
	{
		setTimeout(function(){
			$('#but_orders').click();
		},500);
	}
	
	//debugger;

	$(".fixedScrollBox").css({
		"height": $('body').height() - ($("#pageHeader").height()+1) - ($("#aside_nav_cust").height() + 45)
	});

	document.addEventListener("deviceready", getFunctionValue, false);
	document.addEventListener("deviceready", getUser, false);
	document.addEventListener("deviceready", getUserCredentials, false);

	if(checkIfHomeScreen != undefined && checkIfHomeScreen != ""){
		document.addEventListener("deviceready", function(){
			searchCustomer(checkIfHomeScreen);
		}, false);
	} else if( checkIfAroundMeScreen != undefined && checkIfAroundMeScreen != "" ){
		document.addEventListener("deviceready", function(){
			searchCustomer(checkIfAroundMeScreen);
		}, false);
	} else if( typeof checkIfOpenQuot != 'undefined' && checkIfOpenQuot != "" ){
		document.addEventListener("deviceready", function(){
			searchCustomer(incommingParams.customerid);
		}, false);
	} else if( typeof checkIfCreateSalesDoc != 'undefined' && checkIfCreateSalesDoc != "" ){
		document.addEventListener("deviceready", function(){
			searchCustomer(checkIfCreateSalesDoc);
		}, false);
	} else {
		//Load Customers List:
		setTimeout(function () { document.addEventListener("deviceready", getCustomerList, false); }, 500);		
	}
	
	
	//Styles Fix:
	$("#pageContent .wrapper").height($(window).height() - ($("#pageHeader").height()+1)-($("#pageDownNav").height()+2));

	//Customer Details:
	$("#pageAside ul").on( 'click', "a", function () {
		$('#quick_createSA2').hide();
		$("#edit_contact").hide();
		$("#delete_cont").hide();
		$("#customer_edit").show();
		$("#pageDownNav li").removeClass("active");

		$("#pageContent").fadeIn();
		var customerId = $(this).find(".number").data('kunnr');
		//lastSectionFromEdit = "#customers";
	    getCustomerDetails(customerId);
	    getUserReportList();
	});

	//Customer - Contacts
	$("#but_contacts").on("click", function(){
		lastSectionFromEdit = "#customers_contacts";
		$('#quick_createSA2').hide();
		$("#edit_contact").hide();
		$("#delete_cont").hide();
		$("#customer_edit").hide();
		var customerId = $("#pageAside ul .active").find(".number").data('kunnr');
		getContactListByCustomer(customerId);
		showSection("#customers_contacts", ".customers_current:visible", this);
	});
	//Customer - Contact - Details:
	/*
	$("#customers_contacts tbody tr").on("click", function(){
		$("#customers_contacts").fadeOut(function(){
			$("#personDetails_holder").fadeIn();
		});
	});
	*/
	$("#customers_contactsTable tbody").on("click", "tr", function(){
		$("#edit_contact").show();
		$("#delete_cont").show();
		var contact_id = $(this).data('contact_id').toString();
		getContactDetails(contact_id);
	});

	$("#but_reports").on("click", function(){
		$('#quick_createSA2').hide();
		$("#edit_contact").hide();
		$("#delete_cont").hide();
		$("#customer_edit").hide();
		lastSectionFromEdit = "#customers_reports";
		showSection("#customers_reports", ".customers_current:visible", this);
	});
	$(".header1").on("click", function(){
		$('#quick_createSA2').hide();
		$("#edit_contact").hide();
		$("#delete_cont").hide();
		$("#customer_edit").show();
		var customerId = $("#pageAside .active").find(".number").data('kunnr');
		getCustomerDetails(customerId);
		lastSectionFromEdit = "#customers";
		showSection("#customers", ".customers_current:visible", this);
	});
	$(".header2").on("click", function(){
		$('#quick_createSA2').hide();
		$( "#bonusTable" ).hide();
		$("#edit_contact").hide();
		$("#delete_cont").hide();
		$("#customer_edit").show();
		var customerId = $("#pageAside .active").find(".number").data('kunnr');
		lastSectionFromEdit = "#customers2";
		showSection("#customers2", ".customers_current:visible", this);
	});
	// $("#but_orders").on("click", function(){
	// 	$('#quick_createSA2').hide();
	// 	$("#edit_contact").hide();
	// 	$("#delete_cont").hide();
	// 	$("#customer_edit").hide();
	// 	showSection("#customers_orders", ".customers_current:visible", this);
	// 	lastSectionFromEdit = "#customers_orders";
	// 	var customerId = $("#pageAside ul .active").find(".number").data('kunnr');
	// 	getSalesDocumentsListByCustomer(customerId);
	// });
	$("#but_visits").on("click", function(){
		$('#quick_createSA2').hide();
		$("#edit_contact").hide();
		$("#delete_cont").hide();
		$("#customer_edit").hide();
		var customerId = $("#pageAside ul .active").find(".number").data('kunnr');
		lastSectionFromEdit = "#customers_sales_activities";
		getSalesActivityListByCustomer(customerId);
		showSection("#customers_sales_activities", ".customers_current:visible", this);
	});
	// $("#customers_salesDocuments_table tbody").on("click", "tr", function(){
	// 	if (typeof(event) != 'undefined') {
	// 		if (event.target.id == 'salesDocStar' || event.target.id == 'salesDocStarSvg') { return; } // Prevent Default navigation if favourite button is clicked}
	// 	}
	// 	var current_tr = $(this);
	// 	cordova.exec(checkSuccess, execFailed, 'OpenQuotation', 'isOpenQuotation', [$(this).data('docnumber')]);

	// 	//Show the correct back button:
	// 	$("#back_salesOrders").show();
	// 	$("#back_to_home").hide();
		
	// 	function checkSuccess(answer){ 
	// 		var sales_order_nr = current_tr.data("docnumber");
	// 		var cust_soldto = current_tr.data("soldto");
	// 		var salesActivity_nr = current_tr.data("salesactivity");
	// 		//If it's not released, go to Edit Order Document:
	// 		if( sales_order_nr.toString().charAt(0) == "*" ){
	// 			window.location = "createOrder.html?said="+ salesActivity_nr +"&uid=" + cust_soldto + "&edit=1&suba_id="+sales_order_nr + "&fromCustomersScreen=1";
	// 		} else {
	// 			(answer=="X") ? getSalesOrderDetails(sales_order_nr, 1) : getSalesOrderDetails(sales_order_nr);
	// 		}
	// 	}
	// });


	$("#sales_activities_table tbody").on("click", "tr", function(){
		//Go go Sales Activities Screen:
		var current_docNr = $(this).data("docnumberfull");
        var customerId = $("#pageAside ul .active").find(".number").data('kunnr');
        window.location = "sales_activities.html?uid=" + customerId + "&salesActivityDocNumber=" + current_docNr;

		/*
		var sales_order_nr = $(this).data("doc_number");
		var act_type = $(this).data("sa_type");
		var from_time = $(this).data("from_time");
		var to_time = $(this).data("to_time");
		var from_date = $(this).data("from_date");
		var to_date = $(this).data("to_date");
		var comment = $(this).data("comment");
		var customer = $(this).data("customer");
		var result = $(this).data("result");
		var conta = $(this).data("conta");

		getSalesActivitiesDetails(sales_order_nr, from_time, from_date, to_time, to_date, comment, customer, result, conta, act_type);
		*/
	});


	/* Quick Nav START */
	function fireMenu(){
		$(".quickNav_holder").fadeIn(500);
		$(".circles_holder").slideDown(500);
		$("#quickNav_but").stop().animate(
			{rotation: -90},
			{
				duration: 500,
				step: function(now, fx) {
					$(this).css({"transform": "rotate("+now+"deg)"});
					$(this).css({"-ms-transform": "rotate("+now+"deg)"});
					$(this).css({"-webkit-transform": "rotate("+now+"deg)"});
				}
			}
		);
	}
	function hideMenu(){
		$(".circles_holder").slideUp(500);
		$(".quickNav_holder").fadeOut(500);
		$("#quickNav_but").stop().animate(
			{rotation: -45},
			{
				duration: 500,
				step: function(now, fx) {
					$(this).css({"transform": "rotate("+now+"deg)"});
					$(this).css({"-ms-transform": "rotate("+now+"deg)"});
					$(this).css({"-webkit-transform": "rotate("+now+"deg)"});
				}
			}
		);
	}
	hideMenu();
	$("#quickNav_but").on("click", function(){
		if($(".quickNav_holder").css("display") == "none") {
			fireMenu();
		} else {
			hideMenu();
		}
		$(".quickNav_holder").on("click", function(){		
			hideMenu();
		});
	});
	/* Quick Nav END */


	/* Search Bar START */
	var timer;
	var searchPulse = 0;
	$("#search_box").on("keyup", function(){
		var searchVal = $(this).val();
		var $clear_search = $(".search_del");
		if(searchVal.length > 0) {
			$clear_search.fadeIn();
		}
		else{
			$clear_search.fadeOut();
		}
		if(searchVal.length > 2) {
			clearTimeout(timer);
			timer = setTimeout(function(){
				searchVal = $("#search_box").val();
				if(searchVal.length > 2){
					searchCustomer(searchVal);
					$("#pageAside").off("scroll");
				}
			}, 1000);
			searchPulse = 0;
		} else if(searchVal.length == 0 && searchPulse == 0){
			$clear_search.fadeOut();
			getCustomerList();
			searchPulse = 1;
			$("#pageAside").on("scroll", function(){
		    	if( ($(this).scrollTop() + $(this).innerHeight()) == $(this).get(0).scrollHeight){
		    		loadMoreCustomerList();
		    	}
		    });
		}
	});
	/* Search Bar END */


	/* Create Contact START */
	$("#add, #quick_addContact").on("click", function(){

		//Clear all inputs:
		$("#customers_addNewContact :input").val("");
		$("#customers_addNewContact select").html("");

		//Add PAFKT Select:
		cordova.exec(function (param_select){
			var result_select = eval('(' + param_select + ')');
			//Get All Values:
			var selectHtml = "";
			for(i = 0; i < result_select.length; i++){
				selectHtml += '<option value="' + result_select[i]["PAFKT"] + '">' + result_select[i]["VTEXT"] + '</option>';
			}
			$("#add_function").html("<option value='' selected disabled></option>" + selectHtml);
		}, execFailed, 'Customizing', 'getPAFKTList', ['']);
		
		
		//Add Hobby 1 Select:
		cordova.exec(function (param_select){
			var result_select = eval('(' + param_select + ')');
			//Get All Values:
			var selectHtml = "";
			for(i = 0; i < result_select.length; i++){
				selectHtml += '<option value="' + result_select[i]["PARH1"] + '">' + result_select[i]["VTEXT"] + '</option>';
			}
			//Fire Defaults:
			$("#add_hobby_1").html("<option value='' selected disabled></option>" + selectHtml);
		}, execFailed, 'Customizing', 'getPARH1List', ['']);


		//Add Hobby 2 Select:
		cordova.exec(function (param_select){
			var result_select = eval('(' + param_select + ')');
			//Get All Values:
			var selectHtml = "";
			for(i = 0; i < result_select.length; i++){
				selectHtml += '<option value="' + result_select[i]["PARH2"] + '">' + result_select[i]["VTEXT"] + '</option>';
			}
			$("#add_hobby_2").html("<option value='' selected disabled></option>" + selectHtml);
		}, execFailed, 'Customizing', 'getPARH2List', ['']);


		//Get Children Select:
		cordova.exec(function (param_select){
			var result_select = eval('(' + param_select + ')');
			//Get All Values:
			var selectHtml = "";
			for(i = 0; i < result_select.length; i++){
				selectHtml += '<option value="' + result_select[i]["PARH3"] + '">' + result_select[i]["VTEXT"] + '</option>';
			}
			$("#add_children").html("<option value='' selected disabled></option>" + selectHtml);
		}, execFailed, 'Customizing', 'getPARH3List', ['']);


		showSection("#customers_addNewContact", ".customers_current:visible", "#but_contacts");
	});

	$("#customers_addNewContact .save").click(function(){
		already_used = 1;
		var $addContactDiv = $("#customers_addNewContact");

		var customer_id = $("#pageAside ul .active").find(".number").data('kunnr').toString();
		var add_fname = $addContactDiv.find("#add_fname").val().toString();
		var add_lname = $addContactDiv.find("#add_lname").val().toString();
		var add_tel = $addContactDiv.find("#add_tel").val().toString();
		var add_fax = $addContactDiv.find("#add_fax").val().toString();
		var add_mobile = $addContactDiv.find("#add_mobile").val().toString();
		var add_email = $addContactDiv.find("#add_email").val().toString();
		var add_notes = $addContactDiv.find("#add_notes").val().toString();

		var add_birthday = "";
		if( $addContactDiv.find("#add_birthday").val() != "" ){
			add_birthday = $addContactDiv.find("#add_birthday").val().toString().replace(new RegExp("-", 'g'), '');
		}

		if($addContactDiv.find("#add_function").val() == null){
			var add_function = "";
		} else {
			var add_function = $addContactDiv.find("#add_function").val().toString();
		}

		if($addContactDiv.find("#add_hobby_1").val() == null){
			var add_hobby1 = "";
		} else {
			var add_hobby1 = $addContactDiv.find("#add_hobby_1").val().toString();
		}

		if($addContactDiv.find("#add_hobby_2").val() == null){
			var add_hobby2 = "";
		} else {
			var add_hobby2 = $addContactDiv.find("#add_hobby_2").val().toString();
		}

		if($addContactDiv.find("#add_children").val() == null){
			var add_children = "";
		} else {
			var add_children = $addContactDiv.find("#add_children").val().toString();
		}

		var myArray = {
			"KUNNR": customer_id,
			"NAMEV": add_fname,
			"NAME1": add_lname,
			"TEL1_NUMBR": add_tel,
			"FAX_NUMBER": add_fax,
			"MOB_NUMBER": add_mobile,
			"E_MAIL": add_email,
			"PARAU": add_notes,
			"GBDAT": add_birthday,
			"PAFKT": add_function,
			"PARH1": add_hobby1,
			"PARH2": add_hobby2,
			"PARH3": add_children
		};

		var execFailedDict = {};
		if(add_fname == "" || add_lname == ""){
			execFailedDict[lang.createContact_validation] = "error";
			customAlert(execFailedDict);
		} else if( (add_tel != "" && !validatePhoneNumber(add_tel)) ||  (add_mobile != "" && !validatePhoneNumber(add_mobile)) ){
			execFailedDict[lang.phone_error] = "error";
			customAlert(execFailedDict);
		} else if( add_email != ""  && !validateEmail(add_email) ) {
			execFailedDict[lang.email_error] = "error";
			customAlert(execFailedDict);
		} else if( add_fax != ""  && !validatePhoneNumber(add_fax) ) {
			execFailedDict[lang.fax_error] = "error";
			customAlert(execFailedDict);
		}  else {
			$('#loadingBar').fadeIn(function(){
				cordova.exec(createSuccess, execFailed, 'Contact', 'createContact', [myArray]);
			});
		}
		
		function createSuccess(data){
			$('#loadingBar').fadeOut();             
            var createSuccessDict = {};
            createSuccessDict[lang.customAlert_createContactSuccess] = "success";
            customAlert(createSuccessDict);

			//Refresh Customer List:
			var updateMode = 1;
			// Bug fix for update contact
			//getCustomerList(updateMode, customer_id);
			getContactListByCustomer(customer_id);
		}
	});
	/* Create Contact END */

	$("#company").on("click", function(){
		$('.newsTester').fadeIn();
		$('#loadingBar').fadeIn();
		var timeoutInternet = 1;
		var customer_name = encodeURIComponent($('#pageAside').find('.active').find('.name').text());
		if(typeof google != "undefined"){
			var feed_lang = (currentLang == "de" || currentLang == "de-US" || currentLang == "de-DE") ? "de" : "en" ;
			var feed = new google.feeds.Feed('https://news.google.com/news/feeds?q='+customer_name+'&hl='+feed_lang+'&output=rss');
			var newsTimeout = window.setTimeout(function () { 
				if($('#loadingBar').is(":visible")){
					$('#loadingBar').fadeOut(function(){
						var execFailedDict = {};
						execFailedDict[lang.internet_err] = "error";
						customAlert(execFailedDict);		
					});
				}
			}, 5000);
			feed.load(function (data) {
				timeoutInternet == 0;
			    // Parse data depending on the specified response format, default is JSON.
				if(data.feed.entries.length == 0){
					$('#loadingBar').fadeOut(function(){
						var execFailedDict = {};
						execFailedDict[lang.noNews] = "error";
						customAlert(execFailedDict);		
					});
					clearTimeout(newsTimeout);
					return;
				}
			    $('#gNewsHolder').html(data.feed.entries[0]['content']);
			    open_popup();
			    $('#gNewsHolder').find('table').find('td:eq(0)').css('width', '0');
			    $('#gNewsHolder').find('table').find('td:eq(1) > font > br').remove();
			    $('#gNewsHolder').find('table').find('td:eq(1) > font').find('div:eq(0)').remove();
			    if($('#gNewsHolder').find('table').find('img').length > 0){
			    	$('#gNewsHolder').find('table').find('img').each(function(){
			    		$(this).attr('src', "http://"+$(this).attr('src').substring(2));
			    	});
			    }
			    $('#customer_news').parent().find('.ui-dialog-title').text(data.feed.entries[0]['title']);
			    $('#loadingBar').fadeOut();
			    clearTimeout(newsTimeout);
			}); 
			function open_popup(){
				$( "#customer_news" ).dialog({
					resizable: false,
					draggable: false,
					height: 370,
					width: 550,
					modal: true,
					show: 'fade',
					hide: 'fade'
				});
				$("#pageAside").css("overflow", "hidden");
				$("#customers").css("overflow", "hidden");
				$('.ui-widget-overlay').on('click', function(){
					$('.ui-widget-overlay').off('click');
					$( "#customer_news" ).dialog( "close" );
					$("#pageAside").css("overflow", "auto");
					$("#customers").css("overflow", "auto");
				});
			}
		}
		else{
			$('#loadingBar').fadeOut(function(){
				var execFailedDict = {};
				execFailedDict[lang.internet_err] = "error";
				customAlert(execFailedDict);	
			});
			return;
		}
	});

	/* Edit Contact START */
	$("#edit_contact").on("click", function(){
		$("#personDetails_holder").data("contactid");
		var customer_id = $("#personDetails_holder").data("contactid");
		editContactDetails(customer_id);
	});
	$("#customers_editContact .save").on("click", function(){
		var $editContactDiv = $("#customers_editContact");

		var customer_id = $("#pageAside ul .active").find(".number").data('kunnr').toString();
		var contact_id = $editContactDiv.data("contactid");
		var add_fname = $editContactDiv.find("#edit_fname").val().toString();
		var add_lname = $editContactDiv.find("#edit_lname").val().toString();
		var add_tel = $editContactDiv.find("#edit_tel").val().toString();
		var add_fax = $editContactDiv.find("#edit_fax").val().toString();
		var add_mobile = $editContactDiv.find("#edit_mobile").val().toString();
		var add_email = $editContactDiv.find("#edit_email").val().toString();
		var add_notes = $editContactDiv.find("#edit_notes").val().toString();

		var add_birthday = "";
		if( $editContactDiv.find("#edit_birthday").val() !== "" ){
			add_birthday = $editContactDiv.find("#edit_birthday").val().toString().replace(new RegExp("-", 'g'), '');
		}

		if($editContactDiv.find("#edit_function").val() == null){
			var add_function = "";
		} else {
			var add_function = $editContactDiv.find("#edit_function").val().toString();
		}

		if($editContactDiv.find("#edit_hobby_1").val() == null){
			var add_hobby1 = "";
		} else {
			var add_hobby1 = $editContactDiv.find("#edit_hobby_1").val().toString();
		}

		if($editContactDiv.find("#edit_hobby_2").val() == null){
			var add_hobby2 = "";
		} else {
			var add_hobby2 = $editContactDiv.find("#edit_hobby_2").val().toString();
		}

		if($editContactDiv.find("#edit_children").val() == null){
			var add_children = "";
		} else {
			var add_children = $editContactDiv.find("#edit_children").val().toString();
		}

		var settingsArray = {
			"PARNR": contact_id,
			"KUNNR": customer_id,
			"NAMEV": add_fname,
			"NAME1": add_lname,
			"TEL1_NUMBR": add_tel,
			"FAX_NUMBER": add_fax,
			"MOB_NUMBER": add_mobile,
			"E_MAIL": add_email,
			"PARAU": add_notes,
			"GBDAT": add_birthday,
			"PAFKT": add_function,
			"PARH1": add_hobby1,
			"PARH2": add_hobby2,
			"PARH3": add_children
		};

		var execFailedDict = {};
		if(add_fname == "" || add_lname == ""){
			execFailedDict[lang.editContact_validation] = "error";
			customAlert(execFailedDict);
		} else if( (add_tel != "" && !validatePhoneNumber(add_tel)) ||  (add_mobile != "" && !validatePhoneNumber(add_mobile)) ){
			execFailedDict[lang.phone_error] = "error";
			customAlert(execFailedDict);
		} else if( add_email != ""  && !validateEmail(add_email) ) {
			execFailedDict[lang.email_error] = "error";
			customAlert(execFailedDict);
		} else if( add_fax != ""  && !validatePhoneNumber(add_fax) ) {
			execFailedDict[lang.fax_error] = "error";
			customAlert(execFailedDict);
		} else {
			$('#loadingBar').fadeIn(function(){
				cordova.exec(updateSuccess, execFailed, 'Contact', 'updateContact', [settingsArray]);
			});
		}
		
		function updateSuccess(param){
			$('#loadingBar').fadeOut();
            var updateSuccessDict = {};
            updateSuccessDict[lang.customAlert_updateContactSuccess] = "success";
            customAlert(updateSuccessDict);
            
            //Refresh Customer List:
            var updateMode = 1;
            // Bug fix for update contact
			//getCustomerList(updateMode, customer_id);
			getContactDetails(contact_id);
		}
	});
	/* Edit Contact Contact END */

	//Delete Contact:
	$("#delete_cont").on("click", function(){
		var buttonsOpts = {};
		buttonsOpts[lang.cancelButton] = function() {
			$( "#deleteContact_confirm" ).dialog( "close" );
		};
		buttonsOpts[lang.confirm] = function() {
			var parnr = {
	            "PARNR": $("#personDetails_holder").data("parnr")
	        };
			cordova.exec(function(){
				var updateSuccessDict = {};
	            updateSuccessDict[lang.customAlert_deleteContactSuccess] = "success";
	            customAlert(updateSuccessDict);
	            
				//Refresh Customer List:
				var updateMode = 1;
				var my_customer = $("#pageAside li.active .number").data("kunnr");
				// Bug fix for update contact
				//getCustomerList(updateMode, my_customer);
				getContactListByCustomer(my_customer);
				$( "#deleteContact_confirm" ).dialog( "close" );

			}, execFailed, 'Contact', 'deleteContact', [parnr]);
		};
		$( "#deleteContact_confirm" ).dialog({
			resizable: false,
			height: 250,
			width: 450,
			modal: true,
			buttons : buttonsOpts,
			show: 'fade',
			hide: "fade",
			title: lang.deleteSalesDoc_title
		});
	});
	

	/* Edit Customer START */
	$("#customer_edit").on("click", function(){
		$("#customer_edit").hide();
		var customer_id = $("#pageAside ul .active").find(".number").data('kunnr');
		edit_getCustomerDetails(customer_id);
	});
	$("#customers_editCustomer .cancel").on("click", function(){
		$("#customer_edit").show();
		if(lastSectionFromEdit == "#customers"){
			var tab_visible = ".header1";
		}
		else{
			var tab_visible = ".header2";
		}
		showSection(lastSectionFromEdit, ".customers_current:visible", tab_visible);
	});
	$("#customers_editCustomer .save").on("click", function(){
		$("#customer_edit").show();
		var customer_id = $("#pageAside ul .active").find(".number").data('kunnr');

		var edit_customer = $("#customers_editCustomer");
		var cust_url  = edit_customer.find("#edit_cust_url").val();
		var cust_tel  = edit_customer.find("#edit_cust_tel").val();
		var cust_fax  = edit_customer.find("#edit_cust_fax").val();
		var cust_mail = edit_customer.find("#edit_cust_mail").val();
		var Cnoe      = edit_customer.find("#edit_cust_noe").val().toString();
		var Cindustry = edit_customer.find("#edit_cust_industry").val();
		var CnoeY 	  = edit_customer.find("#edit_year_num_employee").val();//new Date().getFullYear().toString();
		var turnovr_year = edit_customer.find("#edit_turnovr_year").val();
		var turnovr_val = edit_customer.find("#edit_turnovr_value").val();
		
		var settingsArray = {
			"KUNNR": customer_id,
			"TELF1": cust_tel,
			"KNURL": cust_url,
			"TELFX": cust_fax,
			"EMAIL": cust_mail,
			"BRSCH": Cindustry,
			"TURNOVER": parseFloat(turnovr_val.replace(',','.')),
			"TURNOVER_YEAR": turnovr_year,
		};

		//if(edit_customer.find("#edit_cust_noe").val().toString() != edit_customer.find("#edit_cust_noe").data('lastVal').toString()){
			settingsArray.JMZAH = Cnoe;
			settingsArray.JMJAH = CnoeY;
		//}
		var execFailedDict = {};
		if( cust_tel != "" && !validatePhoneNumber(cust_tel) ){
			execFailedDict[lang.phone_error] = "error";
			customAlert(execFailedDict);
		} else if( cust_mail != ""  && !validateEmail(cust_mail) ) {
			execFailedDict[lang.email_error] = "error";
			customAlert(execFailedDict);
		} else if( cust_fax != ""  && !validatePhoneNumber(cust_fax) ) {
			execFailedDict[lang.fax_error] = "error";
			customAlert(execFailedDict);
		} else {
			$('#loadingBar').fadeIn(function(){
				cordova.exec(updateSuccess, execFailed, 'Customer', 'updateCustomer', [settingsArray]);
			});
		}
		function updateSuccess(param){
			$('#loadingBar').fadeOut();
            var updateSuccessDict = {};
            updateSuccessDict[lang.customAlert_updateCustomerSuccess] = "success";
            customAlert(updateSuccessDict);

			getCustomerDetails(customer_id);
		}

		return false;
	});
	/* Edit Customer END */

	
	$("#pageAside a").click(function(){
		$(".customers_current:visible").fadeOut(function(){
			$("#customers").fadeIn(function(){
			});
		});
	});

	$("#back_salesOrders").on("click", function(){
		$("#quick_createSA2").hide();
		$("#but_orders").trigger("click");
		//changed to reload the customer sales documents again
		//showSection("#customers_orders", ".customers_current:visible", "#pageDownNav #but_orders");

	});

	$("#back_salesActivities").on("click", function(){
		$('#but_visits').click();
	});

	$("#back_salesDocument").on("click", function(){
		
		$(".customers_current").filter(":visible").fadeOut(function(){
					
			$('#customers_salesDocuments').fadeIn();
		});

	});

	$("#quick_createOrder").on("click", function(){
		window.location = "createOrder.html?uid="+$('.active').find('.number').data("kunnr");
	});
  	
  	$("#quick_createReturn").on("click", function(){
		window.location = "createReturn.html?uid="+$('.active').find('.number').data("kunnr");
	});

    $("#quick_createSA").on("click", function(){
        window.location = "sales_activities.html?uid="+$('#customers #company').data("kunnr")+"&create=x&customer=" + $("#pageAside li.active").find(".name").text();
    });

    $("#quick_createSA2").on("click", function(){
        window.location = "sales_activities.html?uid="+$('#customers #company').data("kunnr")+"&create=x&vbeln=" + $("#customers_salesDocuments #salesDoc_number").data('vbeln') + "&customer=" + $("#pageAside li.active").find(".name").text();
    });

    $("#customers_editContact .cancel").on("click", function(){
    	showSection("#personDetails_holder", ".customers_current:visible", "#but_contacts");
    });
    //Customer Create - Close button:
    $("#customers_addNewContact .cancel").on("click", function(){
    	showSection("#customers_contacts", ".customers_current:visible", "#but_contacts");
    });

    //Catch when the user is scrolled to bottom:
    $("#pageAside").on("scroll", function(){
    	if( ($(this).scrollTop() + $(this).innerHeight()) == $(this).get(0).scrollHeight){
    		loadMoreCustomerList();
    	}
    });
});
$(function(){
	$("#customers_salesDocuments_ordersTable tbody").on('click', 'tr', function(){
		var elemm = $(this);
		if($(this).data('posnr') != 0 && $(this).data('posnr') != ""){
			var answer = $(this).data('posnr');	
		}
		else{
			var answer = 0;
		}
		$('#loadingBar').show();
		getAdditionalPricingData(answer, elemm.data('posnr'), elemm.data('vbeln'), elemm.data('matnr'), elemm.find('td:eq(2)').text(), elemm.data('meins'), elemm.data('kbetr'), current_username, current_password, function(param){
			var result = eval('([' + param + '])');
			$( ".stock_plants_body" ).html("");
			for (var i = 0; i < result.length; i++) {
				$( ".stock_plants_body" ).append("<tr><td><span class='img"+result[i]["XICON"].replace(/@/g, '')+"'></span></td><td>"+result[i]["COND_TYPE"]+"</td><td>"+result[i]["XVTEXT"]+"</td><td>"+result[i]["COND_VALUE"]+"</td><td>&#128;</td><td>"+result[i]["COND_P_UNT"]+"</td><td>"+result[i]["COND_D_UNT"]+"</td><td>"+result[i]["CONDVALUE"]+"</td><td>&#128;</td></tr>");
			};
			//todo styling:
			//Fire Dialog:
			$('#dialog_plant_inv').dialog({
				dialogClass: "with-close-productPopup3",
				title: lang.productPopUp_title,
				show: 'fade',
				hide: "fade",
				width: 780,
				height: 650
			});

			$('#dialog_plant_inv #head_information').scrollTop(0);
			
			$("#popUp_overlap").on("click", function(){
				$(this).fadeOut();
				$(".with-close-productPopup3 .ui-dialog-titlebar-close").click();
				$("#popUp_overlap").off("click");

				//Remove active:
				$('#pageAside ul li').removeClass("activeLongPress");
			}).fadeIn();

		    $("#dialog_plant_inv").dialog('option', 'title', lang.add_price_details);
		    $('#loadingBar').fadeOut();
		}, execFailed);
	});

	$('#aside_nav_cust a').on("click", function(){
		$("#pageAside").scrollTop(0);
		
		$('#aside_nav_cust a').removeAttr('class');
		$(this).addClass('active');
		$("#search_box").val("");
		
		switch($(this).attr('id')){
			case 'all_customers':

				$('.back_button').fadeOut();
				$('#listed_customers').fadeOut(function(){
					getCustomerList(null, null , false);
					$('#listed_customers').fadeIn();
					//$('#search_product').fadeIn();
				});

				//Clear the search term:
				$("#search_product input").val('');

				return false;
				break;

			case 'my_customers':
				//if content is hidden:
				$("#pageContent").fadeIn();
						
				
				getCustomerList(null, null , true);

				return false;
				break;
		}
	});
})

function getAdditionalPricingData(synched, posnr, vbeln, matnr, quant, meins, kbetr, user, pass, success_get, get_failed){
	if( !doIhaveInternet() ){
		var errorDict = {};
        errorDict[lang.internet_err] = "error";
        customAlert(errorDict);
		return;
	}
	if(synched != 0){
		var data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions"><soapenv:Header/><soapenv:Body><urn:_-MSCMOBIL_-Z_PRICE_QUOTATION><IM_BNAME>'+user+'</IM_BNAME><IM_POSNR>'+posnr+'</IM_POSNR><IM_VBELN>'+vbeln+'</IM_VBELN></urn:_-MSCMOBIL_-Z_PRICE_QUOTATION></soapenv:Body></soapenv:Envelope>';	
		$.soap({
			url: baseOnlineURL+"/price_quotation?sap-client="+sapClient,
			method: "_-MSCMOBIL_-Z_PRICE_QUOTATION",
			soap12: false,
			data: data,
			wss: "",
			HTTPHeaders: {
				Authorization: 'Basic ' + btoa(user+':'+pass)
			},
			namespaceQualifier:  "myns",
			namespaceURL: "urn:_-MSCMOBIL_-Z_PRICE_QUOTATION",
			noPrefix: false,
			elementName: "",
			enableLogging: false,
			success: function(SOAPResponse) {
				var resp = SOAPResponse.toString();
				//var result = eval('(' + $(resp).find("EX_JSON_RESULT").text() + ')');
				var result = $(resp).find("EX_JSON_RESULT").text();
				console.log(resp);
				success_get(result);
			},
			error: function(SOAPResponse) {
				get_failed(SOAPResponse.toString());
			}
		});
	}
	else{
		var data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions"><soapenv:Header/><soapenv:Body><urn:_-MSCMOBIL_-Z_GET_QUOTATION><IM_BNAME>'+user+'</IM_BNAME><IM_KUNNR>'+$('#pageAside .active .number').data('kunnr')+'</IM_KUNNR><IM_MATNR>'+matnr+'</IM_MATNR><IM_QUANTITY>'+quant+'</IM_QUANTITY><IM_MEINS>'+meins+'</IM_MEINS><IM_KBETR>'+kbetr+'</IM_KBETR></urn:_-MSCMOBIL_-Z_GET_QUOTATION></soapenv:Body></soapenv:Envelope>';	
		console.log(data);
		$.soap({
			url: baseOnlineURL+"/get_quotation?sap-client="+sapClient,
			method: "_-MSCMOBIL_-Z_GET_QUOTATION",
			soap12: false,
			data: data,
			wss: "",
			HTTPHeaders: {
				Authorization: 'Basic ' + btoa(user+':'+pass)
			},
			namespaceQualifier:  "myns",
			namespaceURL: "urn:_-MSCMOBIL_-Z_GET_QUOTATION",
			noPrefix: false,
			elementName: "",
			enableLogging: false,
			success: function(SOAPResponse) {
				var resp = SOAPResponse.toString();
				//var result = eval('(' + $(resp).find("EX_JSON_RESULT").text() + ')');
				var result = $(resp).find("EX_JSON_RESULT").text();
				console.log(resp);
				success_get(result);
			},
			error: function(SOAPResponse) {
				get_failed(SOAPResponse.toString());
			}
		});
	}
}
//REPORTS LOGIC!

function set_report(){
	switch(currentLang){
		case "de":
			reports = reports_de;
			break;
		case "en":
			reports = reports_en;
			break;
		case "us":
			reports = reports_en;
			break;
		default:
			reports = reports_de;
			break;
	}

}

function getOnlineReportLink(repID1,success_get){
	if( !doIhaveInternet() ){
		var errorDict = {};
        errorDict[lang.internet_err] = "error";
        customAlert(errorDict);
		return;
	}
	var data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions"><soapenv:Header/><soapenv:Body><urn:_-MSCMOBIL_-Z_GET_ONLINE_REPORT><IM_BNAME>'+current_username+'</IM_BNAME><IM_REPID>'+repID1+'</IM_REPID></urn:_-MSCMOBIL_-Z_GET_ONLINE_REPORT></soapenv:Body></soapenv:Envelope>';
	$.soap({
		url: baseOnlineURL+"/get_online_report?sap-client="+sapClient,
		method: "_-MSCMOBIL_-Z_GET_ONLINE_REPORT",
		soap12: false,
		data: data,
		wss: "",
		HTTPHeaders: {
			Authorization: 'Basic ' + btoa(current_username+':'+current_password)
		},
		namespaceQualifier:  "myns",
		namespaceURL: "urn:_-MSCMOBIL_-Z_GET_ONLINE_REPORT",
		noPrefix: false,
		elementName: "",
		enableLogging: false,
		success: function(SOAPResponse) {
			var resp = SOAPResponse.toString();
			var result = $(resp).find("EX_LINK").text();
			success_get(result);
		},
		error: function(SOAPResponse) {
			execFailed(SOAPResponse.toString());
		}
	});
}

function getUserReportList(){
	if(langset == 1){
		continue_with_logic();
	}
	else{
		window.setTimeout(function () { getUserReportList(); }, 200);
	}
	function continue_with_logic(){
		set_report();
	    cordova.exec(fireUserReportList, execFailed, 'Report', 'getCustomerReportList', [$('#pageAside').find('.active').find('.number').data('kunnr')]);

	    function fireUserReportList(param) {
			function record(report_name, report_desc, report_id, online){
				var productRecord = '<tr data-online="'+online+'"><td class="address" data-repid="'+report_id+'">';
				productRecord += report_name;
				productRecord += '</td><td>';
				productRecord += report_desc;
				productRecord += '</td></tr>';
				return productRecord;
			};

			var result = eval('(' + param + ')');
			var result_html = "";
			for(i = 0; i < result.length; i++){
				var report_name = "";
				(result[i]["TEXT"].length > 0) ? report_name = result[i]["TEXT"] : report_name = "???";
				var report_desc = result[i]["DESCRIPTION"];
				var report_id = result[i]["ID"];
				var online = result[i]["ONLINE_REPORT"];
				result_html += record(report_name, report_desc, report_id, online);
				
			}
			$("#customers_reports table tbody").html(result_html);
			$("#customers_reports table tbody").on( 'click', "tr", function (e) {
				var elem = $(this);
				$('div.spinner').css('left', '350px');
				$('div.spinner').css('top', '350px');
				$("#loadingBar2").fadeIn(function(){
					if(elem.data('online') == "X"){
						getOnlineReportLink(elem.find(".address").data('repid').toString(), function(data){
							$('#loadingBar2').fadeOut();
							 	// window.location.href = data;
								window.open(data, "_system"); 
							
						})
					}
					else{
						if(elem.attr('class') == "bo_link"){
							$('#loadingBar2').fadeOut();
							window.location.href = elem.find('a').attr('href');
							window.open(elem.find('a').attr('href'), "_system");
							return false;
						}
						else{
							var repId = elem.find(".address").data('repid').toString();
							getUserReportDetails(repId);
						}
					}
				});
			});
		}
		cordova.exec(fireUserBOReportList, execFailed, 'Report', 'getCustomerBOReportList', [$('#pageAside').find('.active').find('.number').data('kunnr')]);

	    function fireUserBOReportList(param) {
			function record(report_name, report_desc, report_id){
				var productRecord = '<tr class="bo_link"><td class="address" data-repid="'+report_id+'">SAP Business Object Mobile</td><td><a href="';
				productRecord += report_desc;
				productRecord += '">';
				(report_name.length < 2) ? report_name = "???" : "";
				productRecord += report_name;
				productRecord += '</a></td></tr>';
				return productRecord;
			};

			var result = eval('(' + param + ')');
			var result_htmlBO = "";
			for(i = 0; i < result.length; i++){
				var report_name = result[i]["TEXT"];
				var report_desc = result[i]["URL"];
				var report_id = result[i]["ID"];
				result_htmlBO += record(report_name, report_desc, report_id);
				
			}
			$("#customers_reports table tbody").append(result_htmlBO);
		}
	}
}

function getUserReportDetails(repId){
	cordova.exec(fireUserReportDetails, execFailed, 'Report', 'getCustomerReportDetails', [repId]);
	function fireUserReportDetails(param) {
		var result = eval('(' + param + ')');
		$('#msc_chart').removeAttr('class');
		$('#msc_chart').removeAttr('style');
		$('#msc_chart').css('margin-top', '20px');
		$('#msc_chart').html('');
		var res_html = result['TEXT'];
		$("#loadingBar").hide();
		$("#loadingBar2").hide();
		$('#functionality').html(res_html);
		if($('#functionality').html().search('set_custom_report') > -1){
			eval($('#functionality script').html());	
		}
		$(".customers_current:visible").hide();
		$("#customers_reports_details").show();
		if($('#functionality').html().search('set_custom_report') > -1){
			set_custom_report();
		}
	}
}
