
var current_username = "";
var current_password ="";

function readFromStorage(key) {
    return window.localStorage.getItem(key);
}

function getUserCredentials(){
    cordova.exec(storeCredentials, execFailed, 'Login', 'getCurrentUserCredentials', ['']); 

    function storeCredentials(param){
        var result = eval('(' + param + ')');
        current_username = result.username;
        current_password = result.password;
    }   
}


function getActDetails(result,i,callback)
{
    cordova.exec(function(salesActParam){
                callback(result,i,salesActParam);

            }, execFailed, 'SalesActivity', 'getSalesActivityDetails', [result[i]["VBELN"]]);       
}

function getOrdDetails(result,i,callback)
{
    var result = result;
    var i = i;
    var callback = callback;
    cordova.exec(checkSuccess, execFailed, 'OpenQuotation', 'isOpenQuotation', [result[i]["VBELN"]]);
    function checkSuccess(answer){ 
        var open_Q;
        (answer=="X") ? open_Q = 1 : open_Q = 0;
        getSalesOrderDetails(result, i, callback, open_Q);
    }
}

function getSalesOrderDetails(result,i,callback, openQuot)
{
    if(openQuot == 1){
        cordova.exec(fireSalesOrderDetails, execFailed, 'OpenQuotation', 'getOpenQuotationDetails', [result[i]["VBELN"]]);
    }
    else{
        cordova.exec(fireSalesOrderDetails, execFailed, 'Order', 'getSalesOrderDetails', [result[i]["VBELN"]]);
    }

    function fireSalesOrderDetails(salesDocParam)
    {
        callback(result,i,salesDocParam);
    }
}

function getReminderDetails(result,i,callback)
{
    if (result[i]["RESUB_TYP"] == "0")
        getOrdDetails(result,i,callback);
    else
        getActDetails(result,i,callback);

}




function fireReminderListHome(param){
    var result = eval('(' + param + ')');

    result.sort(function(a, b) {
        return parseInt(a.RESUB_DATE) - parseInt(b.RESUB_DATE);
    });

    result = result.slice(0,7);


    for(i = 0; i < result.length; i++)
    {

        getReminderDetails(result , i , function(result, i, remDetailsParam){

            var reminderDetails = eval('(' + remDetailsParam + ')');
            var resubdate = formatDate(result[i]["RESUB_DATE"]);// result[i]["RESUB_DATE"].slice(4,6) + "/" + result[i]["RESUB_DATE"].slice(6,8) + "/" +result[i]["RESUB_DATE"].slice(0,4) ;
            var index = (i+1);
            if(remDetailsParam.toString() == "{}"){
                return;
            }
            if (result[i]["RESUB_TYP"] == "0")
            {
                if((typeof reminderDetails.Header["SOLD_TO"] != 'undefined' && reminderDetails.Header["SOLD_TO"] !="" ) && (typeof reminderDetails.Header["DOC_TYPE"] != 'undefined' && reminderDetails.Header["DOC_TYPE"] != "" )){
                    var DocText = reminderDetails["Header"]["BSTKD"];
                    if(DocText.length > 36)
                    {
                    	DocText = DocText.substring(0, 34) + '...';
                    }
                    $('#product_highlights ul .position'+index).next().html(DocText +'<br />'+resubdate);
                    $('#product_highlights ul .openQlink'+index).attr('data-cust_number', reminderDetails["Header"]["SOLD_TO"]);
                }
            }               
        
            else
            {
                if((typeof reminderDetails["PARTN_ID"] != 'undefined' && reminderDetails["PARTN_ID"] != "" ) && (typeof reminderDetails["ACTIVITY_TYPE_TEXT"] != 'undefined' && reminderDetails["ACTIVITY_TYPE_TEXT"] != "" ))
                {   
                	var  ActText =  reminderDetails["ACTIVITY_COMMENT"];
                	if(ActText.length > 36)
                    {
                    	ActText = ActText.substring(0, 34) + '...';
                    }
                    $('#product_highlights ul .position'+index).next().html(ActText+'<br />'+resubdate);
                    $('#product_highlights ul .openQlink'+index).attr('data-cust_number', reminderDetails["PARTN_ID"]);
                }
            }

            $('#product_highlights ul .openQlink'+index).attr('data-doc_number', result[i]["VBELN"]);
            $('#product_highlights ul .openQlink'+index).attr('data-doc_type', result[i]["RESUB_TYP"]);
            $('#product_highlights ul .openQlink'+(index)).parent().css('visibility', 'visible');

            console.log(i);
            console.log($('#product_highlights ul .openQlink'+index));

            $('#product_highlights ul .openQlink'+index).click(function( event) {

                event.preventDefault();
                var $current_element = $(this);
                var docu_number = $current_element.attr("data-doc_number");
                var customer_num = $current_element.attr("data-cust_number");

                if ($current_element.attr("data-doc_type") == "1")
                {
                    // Sales Activity
                    if ((docu_number != "") && (customer_num != "")) 
                        window.location = "sales_activities.html?uid=" + customer_num + "&salesActivityDocNumber=" + docu_number + "&fromReminder=true" ;
                }
                else
                {
                    // Sales Document
                    if ((docu_number != "") && (customer_num != ""))
                        window.location = "customers.html?salesAct_customerid=" + customer_num + "&salesOrderDocNumberReminder=" + docu_number + "&fromReminder=true" ;
                }
            
            });


            

        });
 
    }

   

}

function getHomeData(){
	$(function(){
		//load highlighted products
		//cordova.exec(openQuotations, execFailed, 'OpenQuotation', 'getLastOpenQuotations', ['']);
        //check the saved data for the start and end location longitude are still valid.
        var today = new Date();
        var dd_new = today.getDate();
        var mm_new = today.getMonth()+1;
        var yyyy_new = today.getFullYear();
        if(dd_new<10){dd_new='0'+dd_new;}
        if(mm_new<10){mm_new='0'+mm_new;}
        var formattedDate =  yyyy_new.toString()+mm_new.toString()+dd_new.toString();
    
        var home_date  = readFromStorage('home_date');
        var start_lon  = readFromStorage('start_lon');
        var start_lat  = readFromStorage('start_lat');
        var end_lon    = readFromStorage('end_lon');
        var end_lat    = readFromStorage('end_lat');
        if (home_date != formattedDate){
            window.localStorage.setItem('home_date', new Date());
            window.localStorage.setItem('start_lon', '');
            window.localStorage.setItem("start_lat", "");
            window.localStorage.setItem("end_lon", "");
            window.localStorage.setItem("end_lat", "");
        }else{
            $('.start_title > span').text(lang.start);
            $(".end_title > span").text(lang.end);
            if ((start_lon != '' && start_lon != null) && (start_lat != '' && start_lat != null) ){
                $(".start_title").show();
                $("#start_lat_results").show();
                $("#start_lon_results").text("Longitude: "+start_lon);
                $("#start_lat_results").text("Latitude: "+start_lat);
            }else if (start_lat == '' || start_lat == null){
                //this is an address
                $(".start_title").show();
                $("#start_lat_results").hide();
                $("#start_lon_results").text("Address: "+start_lon);
            }
            if ((end_lon != '' && end_lon != null) && (end_lat != '' && end_lat != null) ){
                $(".end_title").show();
                $("#end_lat_results").show();
                $("#end_lon_results").text("Longitude: "+end_lon);
                $("#end_lat_results").text("Latitude: "+end_lat);
            }else if (end_lat == '' || end_lat == null){
                //this is an address
                $(".end_title").show();
                $("#end_lat_results").hide();
                $("#end_lon_results").text("Address: "+end_lon);
            }
        }
    

		function getOpenQuotations(){

            function readFromStorage(key) {
                return window.localStorage.getItem(key);
            }
            var username                = "";
            var userpassword            = "";
            var serverProtocol_Key      = "serverProtocol";
            var server_Key              = "serverURL";
            var serverSyncPort_Key      = "syncPort";
            var serverPort_Key          = "port";
            var serverFarmID_Key        = "farmID";
            var serverSynchFarmID_Key   = "synch_farmID"
            var protocol                = readFromStorage(serverProtocol_Key);
            var server                  = readFromStorage(server_Key);
            var syncPort                = readFromStorage(serverSyncPort_Key);
            var port                    = readFromStorage(serverPort_Key);
            var farmID                  = readFromStorage(serverFarmID_Key);
            var synch_farmID            = readFromStorage(serverSynchFarmID_Key);

            whenSynchronized();
            function whenSynchronized(){

                // Get Reminders instead of open quotations

                cordova.exec(fireReminderListHome, execFailed, 'Resubmission', 'getResubList', [current_username]);  

                // $('#customers7around li').each(function(index, el){
                //     just_a_check=0;
                //     var current_li = $(this);
                //     cordova.exec(openQuotationss, execFailed, 'OpenQuotation', 'getLastOpenQuotationForCustomer', [current_li.attr('customerid')]);

                //     function openQuotationss (param){
                //         if(param.length < 10) return;
                //         var result1 = eval('(' + param + ')');
                //         // var result = result1[0]["Header"] ;
                //         var current_index = index+1;
                //         if(result["PURCH_DATE"].toString().length > 2){
                //             var the_date = formatDate(result["PURCH_DATE"].toString());
                //         }
                //         else{
                //             var the_date = "";
                //         }
                //         $('#product_highlights ul .position'+current_index).next().html(result["CREATED_BY"]+'<br />'+the_date);
                //         $('#product_highlights ul .openQlink'+current_index).attr('href', 'customers.html?openq='+result["DOC_NUMBER"] + "&customerid=" + current_li.attr('customerid') );
                //         $('#product_highlights ul .openQlink'+current_index).parent().css('visibility', 'visible');
                //     }
                // });
                $('#loadingBar2').fadeOut();
            }
		}

		//Get My Location:
		navigator.geolocation.getCurrentPosition(getMyLocation, onGpsError);
        getOpenQuotations(); 

		function getMyLocation(position){
			
			myLocation_long = position.coords.longitude;
			myLocation_lat = position.coords.latitude;
			//Get Customers Locations:
			cordova.exec(customersLocation, execFailed, 'Customer', 'getCustomerListWithinRange', [position.coords.latitude, position.coords.longitude]);
			function customersLocation(result){
				var currentClientsList = eval('(' + result + ')');
				var customer_html='';
				var counter = 1;
				var oddOrEven = '';
				$.each( currentClientsList, function( key, value ) {
					//get first 7 nearby customers
					if (counter == 8) return false;

					if (counter % 2 == 0) oddOrEven = 'even';
					else oddOrEven = 'odd';

					var customer_id = value['KUNNR'].replace(/^0+/, '');

					customer_html += '<li class="'+oddOrEven+'" customerid="'+value['KUNNR']+'"><a href="customers.html?customerFromHomeScreen='+value['KUNNR']+'"><div class="hexagon position'+counter+'">';
					customer_html += value['DISTANCE']+'<br /><span class="km">km</span>';
					customer_html += '</div><div class="customer_info">';
					customer_html += value['NAME1']+'<br />'+customer_id+','+value['STRAS'];
					customer_html += '</div>';
					customer_html += '</a></li>';

					counter++;
				});

				$('#customers_around ul').html(customer_html);
                //$('#loadingBar2').fadeIn();
               
                $('#loadingBar').fadeOut();
                //Remove the loading bar:
			}
		}
	});
}

$(function() {
    //getUserCredentials();
	//Load Customers List:
    //This is triggered from script_settings, because the logo needs to be set first!
    document.addEventListener("deviceready", getUserCredentials, false);
});
