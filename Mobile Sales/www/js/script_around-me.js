var langChanged = 0;
var firstTime = true;

function addMarker(map, myLocation, label, html, default_pin) {
	var marker = new google.maps.Marker({
		position: myLocation,
		animation: google.maps.Animation.DROP,
		icon: default_pin, 
		map: map,
		title: label
	});
	
	infowindow = new google.maps.InfoWindow();

	google.maps.event.addListener(marker, 'click', function() {
		infowindow.setContent(html + label);
		infowindow.open(map, marker);
	});
	
	return marker;
}
function setRadius(map, circle, radius_km){
	//Default - set the map radius to 100;
	if( radius_km == 0 ){
		radius_km = 100;
	}

	circle.setRadius(radius_km * 1000);
	map.fitBounds(circle.getBounds());
}

document.addEventListener("deviceready", start_GPS_setLang, false);

	function start_GPS_setLang(){
		if(langChanged == 0){
			setTimeout(function(){
				langChanged = 1;
				var script = document.createElement("script");
				document.body.appendChild(script);
				script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBC5ujME4sUS3UGc6fri6BiEl0WLQCQLKc&sensor=false&callback=start_GPS_setLang&language=" + currentLang;
				return;	
			}, 500);
		}
		else{
			//google.maps.event.addDomListener(window, 'load', getData);
			getData();
		}
	}

function getData(){
	document.addEventListener("deviceready", start_GPS, false);
	function start_GPS(){
		$(function(){
			//Fix style:
			$("#pageContent .wrapper").height($(window).height() - ($("#pageHeader").height()+1));

			//Get My Location:
			navigator.geolocation.getCurrentPosition(getMyLocation, onGpsError);
			
			function getMyLocation(position){
				myLocation_long = position.coords.longitude;
				myLocation_lat = position.coords.latitude;
				myLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);	
				//Get Customers Locations:
				cordova.exec(customersLocation, execFailed, 'Customer', 'getCustomerListWithinRange', [position.coords.latitude, position.coords.longitude]);
				function customersLocation(result){
					currentClientsList = result;
					locationAquired(result, myLocation);
				}
			}
			
		});
	}
}


var myLocation, customersParams, map, watchID, myLocation_long, myLocation_lat, currentClientsList;

//Default radius is 30km
// 0 - give me all customers eveywhere
var radius_km = 10;

var markersArray = [];
function locationAquired(param, myLocation) {
	var mapOptions = {
		center: myLocation,
		zoom: 12,
		disableDefaultUI: true,
		zoomControl: true,
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.SMALL,
			position: google.maps.ControlPosition.LEFT_TOP
		},
		mapTypeControl: true,
		streetViewControl: true,
		overviewMapControl: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

	//Detect street view
	var thePanorama = map.getStreetView();
	google.maps.event.addListener(thePanorama, 'visible_changed', function() {
	    if (thePanorama.getVisible()) {
	    	$('#map_zoomLevel').hide();
	    } else {
			$('#map_zoomLevel').show();
	    }

	});

	//Zoom to Radius:
	var circle = new google.maps.Circle({
		center: myLocation,
		fillColor: '#005282',
		//fillOpacity: .2,
		fillOpacity: 0,
		strokeColor: '#002941',
		//strokeOpacity: 0.6,
		strokeOpacity: 0,
		strokeWeight: 1,
		map: map
	});
	setRadius(map, circle, radius_km);
	$("#map_zoomLevel ul li a").click(function(){
		if ( !$(this).hasClass("active") ) {
			$('#inner_loadingBar_map').show();

			$("#map_zoomLevel a").removeClass("active");
			$(this).addClass("active");

			radius_km = parseInt($(this).find("span").data("radius"));
			setRadius(map, circle, radius_km);
			clearAllMarkers();
			if ( currentClientsList.length == 0 ){
				populateData(param, myLocation);
			} else {
				populateData(currentClientsList, myLocation);
			}
		}
	});

	$("#level_10").trigger("click");

	//Watch For Location Changes:
	//watchID = navigator.geolocation.watchPosition(positionChanged, onGpsError, { timeout: 30000, maximumAge: 60000 });
}

function populateData(param, myLocation) {
	var result = eval('(' + param + ')');
	var client_list_html = "";
	function record(client_name, client_id, client_distance, client_address, real_customer_id){
		var clientRecord = "";
		clientRecord += '<li><a href="#"><span class="name">';
		clientRecord += client_name;
		clientRecord += '</span><div class="arrow group"><span class="number" data-kunnr="' + real_customer_id + '">';
		clientRecord += client_id;
		clientRecord += '</span><span class="distance">';
		clientRecord += client_distance;
		clientRecord += '</span></div><span class="address">';
		clientRecord += client_address;
		clientRecord += '</span></a></li>'
	
		return clientRecord;
	};
	//The Pin with My Location:
	addMarker(
		map,
		myLocation,
		lang.here,
		"",
		'img/myPosition_pin.png'
	);
	
	//If radius is 0 - default action:
	if ( radius_km == 0 ) {
		//Earth diameter:
		radius_km = 50000;
	}

	//TODO: Prevent crash:
	var number_items = result.length;
	if(result.length > 100){
		number_items = 100;
	}
	
	for( i = 0; i < number_items; i++ ){
		//Set All the markers on the map:
		if(result[i]["DISTANCE"] <= radius_km) {
			var currentCustomer = new google.maps.LatLng(result[i]["MSC_GEOLAT"], result[i]["MSC_GEOLON"]);
			markersArray.push(addMarker(
				map,
				currentCustomer,
				'<p class="insideP">' + result[i]["STRAS"] + " " + result[i]["HOUSENUM"] + "<br />" + result[i]["ORT01"] + ", " + result[i]["LAND1"] + '</p><hr class="insideHr" /><p class="insideArrow customer_arrow"><img class="insideImg map_arrow" onclick="window.location=\'customers.html?customerFromAroundMeScreen=' + result[i]["KUNNR"] + '\'" src="img/maps_arrow.png" alt="Maps Arrow" width="28" height="28" /></p>',
				'<span class="insideSpan">' + result[i]["NAME1"] + '</span>',
				'img/default_pin.png'
			));
			//Populate the Customers List:
			client_list_html += record(result[i]["NAME1"], result[i]["KUNNR"].replace(/^0+/, ''), result[i]["DISTANCE"] + " km", result[i]["STRAS"] + " " + result[i]["HOUSENUM"] + ", " + result[i]["ORT01"], result[i]["KUNNR"]);
		}
	}
	$("#pageAside ul").html(client_list_html);
	//Trigger Click event:
	$("#pageAside ul li").on("click", function(e){
		google.maps.event.trigger(markersArray[$(this).index()],"click");
		map.panTo(markersArray[$(this).index()].getPosition());
	});
	if (markersArray[0])
		map.panTo(markersArray[0].getPosition());


	//If there are 0 customers in the default range - fire all:
	if ( markersArray.length == 0 && firstTime ) {
		$("#map_zoomLevel a").removeClass("active");
		$("#level_100").trigger("click");
	}

	firstTime = false;

	//Remove Loading Animation:
	$('#loadingBar, #inner_loadingBar_map').fadeOut(500);
}

function clearAllMarkers(){
	for (var i = 0; i < markersArray.length; i++ ) {
		markersArray[i].setMap(null);
	}
	markersArray = [];
}

/*
function positionChanged(position) {
	alert(myLocation_long + "/" + position.coords.longitude);
	alert(myLocation_lat + "/" + position.coords.latitude);
	if(myLocation_long != position.coords.longitude || myLocation_lat != position.coords.latitude) {
		alert("Location Changed!");
		clearAllMarkers();
		myLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		cordova.exec(fireNewData, execFailed, 'Customer', 'getCustomerListWithinRange', [position.coords.latitude, position.coords.longitude]);
		function fireNewData(param) {
			populateData(param, myLocation);
		}
	}

}
*/