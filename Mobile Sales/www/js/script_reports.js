var reports_en = {
	"report_jan" : "January",
	"report_feb" : "February",
	"report_mar" : "March",
	"report_apr" : "April",
	"report_may" : "May",
	"report_jun" : "June",
	"report_jul" : "July",
	"report_aug" : "August",
	"report_sep" : "September",
	"report_oct" : "October",
	"report_nov" : "November",
	"report_dec" : "December"
};

var reports_de = {
	"report_jan" : "Januar",
	"report_feb" : "Februar",
	"report_mar" : "März",
	"report_apr" : "April",
	"report_may" : "Mai",
	"report_jun" : "Juni",
	"report_jul" : "Juli",
	"report_aug" : "August",
	"report_sep" : "September",
	"report_oct" : "Oktober",
	"report_nov" : "November",
	"report_dec" : "Dezember"
};
var cur_html = "";
var current_username = "";
var current_password ="";
function getUserCredentials(){
	cordova.exec(storeCredentials, execFailed, 'Login', 'getCurrentUserCredentials', ['']);

	function storeCredentials(param){
		var result = eval('(' + param + ')');
		current_username = result.username;
		current_password = result.password;
	}
}

$(function(){
	document.addEventListener("deviceready", getUserCredentials, false);
	document.addEventListener("deviceready", getUserReportList, false);
});
function set_report(){
	switch(currentLang){
		case "de":
			reports = reports_de;
			break;
		case "en":
			reports = reports_en;
			break;
		case "us":
			reports = reports_en;
			break;
		default:
			reports = reports_de;
			break;
	}

}

function getOnlineReportLink(repID1,success_get){
	if( !doIhaveInternet() ){
		var errorDict = {};
        errorDict[lang.internet_err] = "error";
        customAlert(errorDict);
		return;
	}
	var data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions"><soapenv:Header/><soapenv:Body><urn:_-MSCMOBIL_-Z_GET_ONLINE_REPORT><IM_BNAME>'+current_username+'</IM_BNAME><IM_REPID>'+repID1+'</IM_REPID></urn:_-MSCMOBIL_-Z_GET_ONLINE_REPORT></soapenv:Body></soapenv:Envelope>';
	$.soap({
		url: baseOnlineURL+"/get_online_report?sap-client="+sapClient,
		method: "_-MSCMOBIL_-Z_GET_ONLINE_REPORT",
		soap12: false,
		data: data,
		wss: "",
		HTTPHeaders: {
			Authorization: 'Basic ' + btoa(current_username+':'+current_password)
		},
		namespaceQualifier:  "myns",
		namespaceURL: "urn:_-MSCMOBIL_-Z_GET_ONLINE_REPORT",
		noPrefix: false,
		elementName: "",
		enableLogging: false,
		success: function(SOAPResponse) {
			var resp = SOAPResponse.toString();
			var result = $(resp).find("EX_LINK").text();
			success_get(result);
		},
		error: function(SOAPResponse) {
			execFailed(SOAPResponse.toString());
		}
	});
}

function getUserReportList(){
	$("#loadingBar").show();
	if(langset == 1){
		continue_with_logic();
	}
	else{
		window.setTimeout(function () { getUserReportList(); }, 200);
	}
	function continue_with_logic(){
		set_report();
	    cordova.exec(fireUserReportList, execFailed, 'Report', 'getUserReportList', ['']);

	    function fireUserReportList(param) {
			function record(report_name, report_desc, report_id, online){
				var productRecord = '<li class="active" data-online="'+online+'"><a href="#"><span class="name">';
				productRecord += report_name;
				productRecord += '</span><div class="arrow group"><span class="address" data-repid="'+report_id+'">';
				productRecord += report_desc;
				productRecord += '</span></div></a></li>';
				return productRecord;
			};

			var result = eval('(' + param + ')');
			var result_html = '<li id="bolinks"><a href="#"><span class="name" style="padding-top:20px;">SAP Business Objects Mobile</span><div class="arrow group"><span class="address" ></span></div></a></li>';
			for(i = 0; i < result.length; i++){
				var report_name = "";
				(result[i]["TEXT"].length > 0) ? report_name = result[i]["TEXT"] : report_name = "???";
				var report_desc = result[i]["DESCRIPTION"];
				var report_id = result[i]["ID"];
				var online = result[i]["ONLINE_REPORT"];
				result_html += record(report_name, report_desc, report_id, online);
				
			}
			$("#pageAside ul").html(result_html);
			$("#pageAside ul").on( 'click', "li", function () {
				var elem = $(this);
				$("#loadingBar2").fadeIn(function(){
					$('div.spinner').css('left', '350px');
					$('div.spinner').css('top', '350px');
					$("#pageAside ul li").each(function(){
						$(this).removeClass('active');
					});
					elem.addClass('active');
					if(elem.data('online') == "X"){
						getOnlineReportLink(elem.find(".address").data('repid').toString(), function(data){
							var productRecord = '<tr class="bo_link"><td><a style="color: #596175;" href="';
							productRecord += data;
							productRecord += '">';
							productRecord += lang.repText;
							productRecord += '</a></td></tr>';
							$("#online_links table tbody").html('');
							$("#online_links table tbody").append(productRecord);
							$("#online_links table tbody").off('click');
							$("#online_links table tbody").on( 'click', "tr", function () {
								// window.location.href = $(this).find('a').attr('href');
								window.open($(this).find('a').attr('href'), "_system");
							});
							$("#loadingBar").fadeOut();
							$(".reports_current:visible").hide();
							$("#online_links").fadeIn();
							$("#loadingBar2").hide();
						})
					}
					else{
						if(elem.attr('id') == "bolinks"){
							$(".reports_current:visible").hide();
							$("#bo_links").fadeIn();
							$("#loadingBar2").hide();
						}
						else{
							var repId = elem.find(".address").data('repid').toString();
							$(".reports_current:visible").hide();
							$(".addNew_wrapper").fadeIn();
							getUserReportDetails(repId);
						}
					}
				});
			});
			$("#pageAside ul li:eq(0)").click();

			//Hide the loading bar if:
			if(result.length == 0){
				$("#loadingBar").fadeOut();
			}
			
		}
		cordova.exec(fireUserBOReportList, execFailed, 'Report', 'getuserBOReportList', ['']);

	    function fireUserBOReportList(param) {
			function record(report_name, report_desc){
				var productRecord = '<tr class="bo_link"><td><a style="color: #596175;" href="';
				productRecord += report_desc;
				productRecord += '">';
				if(report_name == "") report_name = "???";
				productRecord += report_name;
				productRecord += '</a></td></tr>';
				return productRecord;
			};

			var result = eval('(' + param + ')');
			var result_htmlBO = "";
			for(i = 0; i < result.length; i++){
				var report_name = result[i]["TEXT"];
				var report_desc = result[i]["URL"];
				result_htmlBO += record(report_name, report_desc);	
			}
			$("#bo_links table tbody").append(result_htmlBO);
			$("#bo_links table tbody").on( 'click', "tr", function () {
				window.location.href = $(this).find('a').attr('href');
				window.open($(this).find('a').attr('href'), "_system");
			});
			$("#loadingBar").fadeOut();
		}
	}
}

function getUserReportDetails(repId){
	cordova.exec(fireUserReportDetails, execFailed, 'Report', 'getUserReportDetails', [repId]);
	function fireUserReportDetails(param) {
		var result = eval('(' + param + ')');
		var res_html = result['TEXT'];
		cur_html = res_html;
		$('#contents').html(res_html);
		$("#loadingBar").hide();
		$("#loadingBar2").hide();
		$('#contents #functionality script').each(function(index, elem){
			eval($(elem).html());
			if($(elem).html().search('set_custom_report') > -1){
			set_custom_report();
		}
		});
	}
}