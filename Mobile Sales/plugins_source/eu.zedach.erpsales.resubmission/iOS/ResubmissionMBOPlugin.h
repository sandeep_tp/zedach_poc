//
//  ResubmissionMBOPlugin.h
//  ERPSales
//
//  Created by Klaus Rollinger on 2017-08-03.
//
//

#import <Cordova/CDV.h>
#import "ResubmissionManager.h"

@interface ResubmissionMBOPlugin : CDVPlugin
@property (retain,nonatomic) ResubmissionManager *manager;



-(void)getResubDetails:(CDVInvokedUrlCommand*)command;
-(void)getResubList:(CDVInvokedUrlCommand*)command;
-(void)updateResubmission:(CDVInvokedUrlCommand*)command;
-(void)createResubmission:(CDVInvokedUrlCommand*)command;
-(void)createResubmissionPending:(CDVInvokedUrlCommand*)command;
-(void)deleteResubmission:(CDVInvokedUrlCommand*)command;


@end
