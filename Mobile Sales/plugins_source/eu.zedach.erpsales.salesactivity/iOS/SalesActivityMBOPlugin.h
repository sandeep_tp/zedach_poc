//
//  SalesActivityMBOPlugin.h
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-08-12.
//
//

#import <Cordova/CDV.h>
#import "SalesActivityManager.h"

@interface SalesActivityMBOPlugin : CDVPlugin
@property (retain,nonatomic) SalesActivityManager *manager;

-(void)searchSalesActivity:(CDVInvokedUrlCommand*)command;

-(void)searchSalesActivityByUNAME:(CDVInvokedUrlCommand*)command;

-(void)searchSalesActivityAdvanced:(CDVInvokedUrlCommand *)command;

-(void)getSalesActivityDetails:(CDVInvokedUrlCommand*)command;

-(void)getSalesActivityList:(CDVInvokedUrlCommand *)command;

-(void)getSalesActivityListByCustomerNr:(CDVInvokedUrlCommand*)command;

-(void)saveDayStartEndLocation:(CDVInvokedUrlCommand*)command;

-(void)createSalesActivity:(CDVInvokedUrlCommand *)command;

-(void)updateSalesActivity:(CDVInvokedUrlCommand *)command;

-(void)deleteSalesActivity:(CDVInvokedUrlCommand *)command;
@end
