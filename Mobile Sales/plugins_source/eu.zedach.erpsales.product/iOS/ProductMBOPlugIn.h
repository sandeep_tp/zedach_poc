//
//  ProductMBOPlugIn.h
//  ERPSales
//
//  Created by Alexander Ilg on 5/11/13.
//
//

#import <Foundation/Foundation.h>
#import <Cordova/CDV.h>
#import "ProductManager.h"

@interface ProductMBOPlugIn : CDVPlugin {
    NSMutableArray *categoryArguments; //array keeps a track of the arguments passed while navigating the categories and used to get the parent category when back button is hit
}

@property (strong,nonatomic) ProductManager *manager;

-(void)getProductList:(CDVInvokedUrlCommand*)command;

-(void)getProductDetails:(CDVInvokedUrlCommand*)command;

-(void)getCategories:(CDVInvokedUrlCommand*)command;

-(void)getProductListByCategory:(CDVInvokedUrlCommand*)command;

-(void)searchProduct:(CDVInvokedUrlCommand*)command;

-(void)addFavorite:(CDVInvokedUrlCommand*)command;

-(void)removeFavorite:(CDVInvokedUrlCommand*)command;

-(void)getFavoriteList:(CDVInvokedUrlCommand*)command;

-(void)getParentCategory:(CDVInvokedUrlCommand*)command;

-(void)isProductLocal:(CDVInvokedUrlCommand*)command;

@end
