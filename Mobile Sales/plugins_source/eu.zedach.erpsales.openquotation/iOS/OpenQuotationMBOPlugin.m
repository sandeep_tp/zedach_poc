//
//  OpenQuotationPlugin.m
//  ERPSales
//
//  Created by YohanK on 2014-01-28.
//
//

#import "OpenQuotationMBOPlugin.h"
#import "../eu.zedach.erpsales.login/Util.h"
@implementation OpenQuotationMBOPlugin


@synthesize manager = _manager;

-(void)pluginInitialize{
    self.manager = [OpenQuotationManager defaultManager];
}

-(void)getLastOpenQuotationForCustomer:(CDVInvokedUrlCommand *)command {
    MBOLog(@"getLastOpenQuotationForCustomer called");
    NSString *customerNumber = [command argumentAtIndex:0];
    NSArray *quotationList =  customerNumber ? [self.manager getLastOpenQuotationForCustomer:customerNumber] : [NSArray array];
    NSString *result = [Util convertNSArrayToJSON: quotationList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)isOpenQuotation:(CDVInvokedUrlCommand *)command {
    MBOLog(@"isOpenQuotation called");
    NSString *docNumber = [command argumentAtIndex:0];
    NSString *result =  docNumber ? [self.manager isOpenQuotation:docNumber] : @"";
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
}

- (void)getOpenQuotationDetails:(CDVInvokedUrlCommand *)command {
    MBOLog(@"getOpenQuotationDetails called");
    
    NSString *docNumber = [command argumentAtIndex:0];
    NSDictionary *order = docNumber ? [self.manager salesQuotationDetails:docNumber] : [NSDictionary dictionary];
    NSString *result = [Util convertNSDictionaryToJSON: order];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)updateQuotationValidity:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"updateQuotationValidity called");
    NSDictionary *values = [command argumentAtIndex:0];
    if (!self.manager){
        self.manager = [OpenQuotationManager defaultManager];
    }
    CDVPluginResult* pluginResult;
    bool result = [self.manager updateQuotationValidity:values];
    if (result == YES){
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    }else{
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
