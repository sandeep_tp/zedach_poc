//
//  OpenQuotationPlugin.h
//  ERPSales
//
//  Created by YohanK on 2014-01-28.
//
//

#import <Cordova/CDV.h>
#import "OpenQuotationManager.h"

@interface OpenQuotationMBOPlugin : CDVPlugin

@property (nonatomic, retain) OpenQuotationManager *manager;

-(void)getLastOpenQuotationForCustomer:(CDVInvokedUrlCommand *)command;
-(void)isOpenQuotation:(CDVInvokedUrlCommand *)command;
-(void)getOpenQuotationDetails:(CDVInvokedUrlCommand*)command;
-(void)updateQuotationValidity:(CDVInvokedUrlCommand*)command;
@end
