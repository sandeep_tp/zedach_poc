//
//  SUPAbstractLocalEntity+ERPSales.h
//  ERPSales
//
//  Created by YohanK on 2013-12-30.
//
//

#import "SUPAbstractLocalEntity.h"

@interface SUPAbstractLocalEntity (ERPSales)

/* converts all attributes of the MBO into JSON format */
- (NSString*)toJSONString;

- (id)valueForAttributeKey:(NSString *)attributeKey;
- (NSMutableDictionary *)toDictionary;
- (NSMutableDictionary*)toDictionaryWithKeys:(NSArray *)keys;
@end
