//
//  LoginPlugIn.h
//  ERPSales
//
//  Created by Alexander Ilg on 5/11/13.
//
//

#import <Foundation/Foundation.h>
#import <Cordova/CDV.h>

#define kSUP101DataVaultID @"SUP101DataVaultID"
#define kSUP101DataVaultSalt @"SUP101DataVaultSalt"

#define kSUP101ErrorBadPin -11111
#define kSUP101ErrorNoSettings -11112
#define kSUP101ErrorKeyNotAvailable -11113
#define kSUP101ErrorFailure -11114

@interface LoginPlugIn : CDVPlugin {
    CDVInvokedUrlCommand* cdvURLCommand;
}


@property (nonatomic, retain) NSDate *connectStartTime;


@property (nonatomic, assign) BOOL firstrun;
@property (nonatomic, retain) UIAlertView *passwordAlert;
@property (nonatomic, retain) UIAlertView *noTransportAlert;

@property (nonatomic, retain) NSString *pin;
@property (nonatomic, retain) NSString *SUPPassword;

@property (nonatomic, retain) NSString *_username;
@property (nonatomic, retain) NSString *_password;

@property (nonatomic, retain) NSString *serverName;
@property (nonatomic, retain) NSString *serverProtocol;


//@property (nonatomic, retain) NSString *SUPServerName;
//@property (nonatomic, retain) NSString *SUPServerPort;
//@property (nonatomic, retain) NSString *SUPUserName;
//@property (nonatomic, retain) NSString *SUPFarmID;
//@property (nonatomic, retain) NSString *SUPConnectionName;
//@property (nonatomic, retain) NSString *SUPActivationCode;
//@property (nonatomic, assign) BOOL SUPManualRegistration;
@property (nonatomic, assign) BOOL connectionFailed;

//-(void)connect:(CDVInvokedUrlCommand*)command;
//+(id) defaultManager;
-(void)synchronize:(CDVInvokedUrlCommand*)command;

-(bool)synchronizeUserMBO:(CDVInvokedUrlCommand*)command;

-(void)synchronizeQuotationMBO:(CDVInvokedUrlCommand*)command;

-(void)syncLogQuotationMBO:(CDVInvokedUrlCommand*)command;

-(void)synchronizeSalesMBO:(CDVInvokedUrlCommand*)command;

-(void)changeUser:(CDVInvokedUrlCommand*)command;

-(void)login:(CDVInvokedUrlCommand*)command;

-(void)getCurrentUserCredentials:(CDVInvokedUrlCommand*)command;

@end
