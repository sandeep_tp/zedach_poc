//
//  BaseManager.m
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-06-26.
//
//

#import "BaseManager.h"
#import "SUPCompositeTest.h"
#import "SUPAttributeTest.h"
#import "SUPSortCriteria.h"
#import "SUPAttributeSort.h"

@interface BaseManager()
@property (strong,nonatomic) NSString *dbTableName;
@end

@implementation BaseManager

/* Implementing classes need to call this init method and pass the database name. */
- (id) initWithTableName:(NSString *)tableName{
    self = [super init];
    if (self){
        self.dbTableName = tableName;
    }
    return self;
}

/* Creates a selectAll query and returns the columns passed in the keys parameters */
- (SUPQuery *)selectAllQuery:(NSArray *)keys{
    SUPQuery *query = [SUPQuery getInstance];
    NSMutableString *select = [NSMutableString string];
    
    [keys enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [select appendFormat:@"x.%@",obj];
        if (idx < keys.count-1){
            [select appendString:@","];
        }
    }];
    
    [query select:select];
    [query from:self.dbTableName:@"x"];
    
    return query;
}

/* Similar to selectAllQuery but additionally only return a subset of the table */
- (SUPQuery *)selectAllQuery:(NSArray *)keys from:(SUPInt)skip to:(SUPInt)take orderBy:(NSString *)column ascending:(bool)ascending{
    SUPQuery *query = [self selectAllQuery:keys];
    query.skip = skip;
    query.take = take;
    if (![column isEqualToString:@""]){
        // Define the ordering:
        SUPSortCriteria *sort = [SUPSortCriteria getInstance];
        if (ascending){
            [sort add:[SUPAttributeSort ascending:column]];
        }else{
            [sort add:[SUPAttributeSort descending:column]];
        }
        query.sortCriteria = sort;
    }
    return query;
}

- (SUPQuery *)searchQueryForValue:(NSString*)searchString inKeys:(NSArray *)searchKeys withReturnKeys:(NSArray *)returnKeys{
    
    SUPQuery *query = [SUPQuery getInstance];
    
    NSMutableString *select = [NSMutableString string];
    [returnKeys enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [select appendFormat:@"x.%@",obj];
        if (idx < returnKeys.count-1){
            [select appendString:@","];
        }
    }];
    
    [query select:select];
    [query from:self.dbTableName:@"x"];
    
    SUPCompositeTest *compositeTest = [SUPCompositeTest getInstance];
    [compositeTest setOperator:SUPCompositeTest_OR];
    
    for (NSString *key in searchKeys) {
        SUPAttributeTest *at = [SUPAttributeTest getInstance];
        at.attribute = key;
        at.testValue = searchString;
        at.operator = SUPAttributeTest_CONTAINS;
        [compositeTest add:at];
    }

    query.testCriteria = compositeTest;
    
    return query;
}

//override in sub classe to clear any cached objects.
- (void)clearCache{
}
@end
