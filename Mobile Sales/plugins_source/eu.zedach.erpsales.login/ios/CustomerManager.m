//
//  CustomerManager.m
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-06-26.
//
//

#import "CustomerManager.h"
#import "SUPObjectList.h"
#import "SUPAbstractLocalEntity+ERPSales.h"
#import "NSDictionary+ERPSales.h"
#import "SUPAttributeTest.h"
#import "SUPQuery.h"
#import "SUPQueryResultSet.h"
#import "SUPDataValue.h"
#import "SUPDataValueList.h"
#import "SUPCompositeTest.h"
#import "SalesPlusCustomerSalesData.h"
#import "SalesPlusCustIndustry.h"
#import "SalesPlusERPSalesPlus_SuperUserDB.h"
#import "SalesPlusSalesActivity.h"
#import "SalesPlusContact.h"
#import "SalesPlusContact.h"
#import "CustomizingManager.h"
#import "SalesPlusCustomerBonus.h"

@implementation CustomerManager

@synthesize customerLightList = _customerLightList;
@synthesize customerKeys = _customerKeys;

+ (id) defaultManager{
    static CustomerManager *manager  = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[CustomerManager alloc]initWithTableName:@"Customer"];
        manager.customerKeys = [NSArray arrayWithObjects:
                                @"KUNNR",      // Customer No.
                                @"NAME1",      // Customer Name
                                @"NAME2",
                                @"ORT01",      // City
                                @"PSTLZ",      // Postal Code
                                @"STRAS",      // Street
                                @"HOUSENUM",   // House No.
                                @"TELF1",      // Telephone
                                @"TELFX",      // Fax
                                @"EMAIL",      // Email
                                @"KNURL",      // URL
                                @"REGIO",      // Region
                                @"LAND1",      // Country
                                @"MSC_GEOLAT", // Latitude coordinate
                                @"MSC_GEOLON", // Longitute coordinate
                                @"BRSCH",
                                @"JMZAH",
                                @"JMJAH",
                                @"UNAME",
                                @"TURNOVER",
                                @"TURNOVER_YEAR",
                                nil];
    });
    return manager;
}

- (NSArray *)getAllCustomers {
    SUPObjectList *resultList = [SalesPlusCustomer findWithQuery:[self selectAllQuery:self.customerKeys]];
    if (resultList == nil){
        return [NSArray array];
    }else{
        NSMutableArray *customerList = [NSMutableArray arrayWithCapacity:resultList.length];
        for(SalesPlusCustomer *c in resultList){
            NSDictionary *temp = [c toDictionaryWithKeys:self.customerKeys];
            [customerList addObject:temp];
        }
        return customerList;
    }
}

- (NSArray *)getCustomerListFrom:(int)skip To:(int)take {
    // For perfomance reason only query the first 100 entries needs to be adjusted incase of scrolling
    SUPObjectList *resultList = [SalesPlusCustomer findSortedAll:skip take:take];
    if (resultList == nil){
        NSLog(@"query failed!!");
        return [NSArray array];
    }else{
        NSMutableArray *customerList = [NSMutableArray arrayWithCapacity:resultList.length];
        for(SalesPlusCustomer *c in resultList){
            NSMutableDictionary *temp = [c toDictionaryWithKeys:self.customerKeys];
            [temp addEntriesFromDictionary:[self salesDataForCustomer:c]];
            [customerList addObject:temp];
            
        }
        _customerLightList = [NSArray arrayWithArray:customerList];
    }
    return _customerLightList;
}

- (SUPAttributeTest *)getAttributeForKey:(NSString *)key andValue:(NSString *)value {
    // get the language the useris logged in
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString* preferredLang = [languages objectAtIndex:0];
    NSString *language = [[preferredLang capitalizedString] substringToIndex:1];
    SUPAttributeTest *at = [SUPAttributeTest getInstance];
    at.attribute = key;
    NSDate *d;
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    if ([language isEqualToString:@"D"]){
        [df setDateFormat:@"MM/dd/yyyy"];
        d = [df dateFromString:value];
        if (!d){
            [df setDateFormat:@"MM.dd.yyyy"];
            d = [df dateFromString:value];
        }
    }else if ([language isEqualToString:@"E"]){
        [df setDateFormat:@"dd/MM/yyyy"];
        d = [df dateFromString:value];
        if (!d){
            [df setDateFormat:@"dd.MM.yyyy"];
            d = [df dateFromString:value];
        }
    }
    if (d){
        at.testValue = d;
        at.operator = SUPAttributeTest_EQUAL;
    }else{
        at.testValue = value;
        at.operator = SUPAttributeTest_CONTAINS;
    }
    return at;
}

// Queries the database for the given value on multiple fields.
- (NSArray *)searchCustomersWithCriteria:(NSString *)searchString{
    //searchString = @"10/07/2013";
    
    
    //Stores the keys to be used for the search
    NSArray *searchKeys = [NSArray arrayWithObjects:
                           @"c.KUNNR",    // Customer No.
                           @"c.NAME1",    // Name 1
                           @"c.NAME2",    // Name 2
                           @"c.ORT01",    // City
                           @"c.PSTLZ",    // Zip Code
                           @"c.STRAS",    // Street
                           @"c.HOUSENUM", // House Number
                           @"c.TELF1",    // Telephone
                           @"c.TELFX",    // Fax
                           @"c.EMAIL",    // Email
                           @"c.KNURL",    // URL
                           @"csd.VKGRP",  // Sales group VKGRP
                           @"csd.ZTERM",  // ZTerm
                           @"csd.SALES_REPR",  // Sales Representive
                           @"csd.KONDA",   // Price Group
                           @"cuZTERM.TEXT1",
                           @"cuZTERM.TEXT2",
                           nil];
    
    SUPQuery *query = [SUPQuery getInstance];
    query.distinct = YES;
    [query select:@"c.KUNNR, c.NAME1"];
    [query from:@"Customer":@"c"];
    [query join:@"CustomerSalesData":@"csd":@"c.KUNNR":@"csd.KUNNR"];
    [query join:@"CustZTERM":@"cuZTERM":@"cuZTERM.ZTERM":@"csd.ZTERM"];
    //[query join:@"SalesActivity":@"sa":@"sa.PARTN_ID":@"c.KUNNR"];
    
    NSArray* searchStringTokens = [searchString componentsSeparatedByString: @" "];
    SUPCompositeTest *outerCompositeTest = [SUPCompositeTest getInstance];
    [outerCompositeTest setOperator:SUPCompositeTest_AND];
    for (int i = 0; i < searchStringTokens.count; i++){
        SUPCompositeTest *ct = [SUPCompositeTest getInstance];
        [ct setOperator:SUPCompositeTest_OR];
        NSString *searchStringToken = [searchStringTokens objectAtIndex:i];
        for (NSString *key in searchKeys) {
            SUPAttributeTest *at = [self getAttributeForKey:key andValue:searchStringToken];
            [ct add:at];
        }
        [outerCompositeTest add:ct];
    }
    [query orderBy:@"c.NAME1" : [SUPSortOrder ASCENDING]];
    [query where:outerCompositeTest];
    SUPQueryResultSet* resultList = [SalesPlusERPSalesPlus_SuperUserDB executeQuery:query];
    
    if (resultList == nil){
        //do a basic search on the customerMBO
        NSArray *searchKeys = [NSArray arrayWithObjects:
                               @"KUNNR",    // Customer No.
                               @"NAME1",    // Name 1
                               @"NAME2",    // Name 2
                               @"ORT01",    // City
                               @"PSTLZ",    // Zip Code
                               @"STRAS",    // Street
                               @"HOUSENUM", // House Number
                               @"TELF1",    // Telephone
                               @"TELFX",    // Fax
                               @"EMAIL",    // Email
                               @"KNURL",    // URL
                               nil];
        
        SUPQuery *query = [SUPQuery getInstance];
        [query select:@"c.KUNNR, c.NAME1"];
        [query from:@"Customer":@"c"];
        NSArray* searchStringTokens = [searchString componentsSeparatedByString: @" "];
        SUPCompositeTest *outerCompositeTest = [SUPCompositeTest getInstance];
        [outerCompositeTest setOperator:SUPCompositeTest_AND];
        for (int i = 0; i < searchStringTokens.count; i++){
            SUPCompositeTest *ct = [SUPCompositeTest getInstance];
            [ct setOperator:SUPCompositeTest_OR];
            for (NSString *key in searchKeys) {
                SUPAttributeTest *at = [SUPAttributeTest getInstance];
                at.attribute = key;
                at.testValue = [searchStringTokens objectAtIndex:i];
                at.operator = SUPAttributeTest_CONTAINS;
                [ct add:at];
            }
            [outerCompositeTest add:ct];
        }
        [query orderBy:@"NAME1" : [SUPSortOrder ASCENDING]];
        [query where:outerCompositeTest];
        resultList = [SalesPlusERPSalesPlus_SuperUserDB executeQuery:query];
        MBOLog(@"Simple Customer Search");
    }
    if (resultList == nil){
        return [NSArray array];
    }else{
        NSMutableArray *customerList = [NSMutableArray arrayWithCapacity:resultList.length];
        
        for(SUPDataValueList* result in resultList){
            SalesPlusCustomer *c = [SalesPlusCustomer findByPrimaryKey:[SUPDataValue  getNullableString:[result item:0]]];
            NSMutableDictionary *temp = [c toDictionaryWithKeys:self.customerKeys];
            [temp addEntriesFromDictionary:[self customerDetailsForKUNNR:c.KUNNR]];
            [customerList addObject:temp];
        }
        
        return [NSArray arrayWithArray:customerList];
    }
}


// Search customers with UNAME for My Customers
- (NSArray *)searchCustomersWithUNAME:(NSString *)searchString{
    
    //Stores the keys to be used for the search
    NSArray *searchKeys = [NSArray arrayWithObjects:
                           @"c.UNAME",    // User Name.
                           nil];
    
    SUPQuery *query = [SUPQuery getInstance];
    query.distinct = YES;
    [query select:@"c.KUNNR, c.NAME1"];
    [query from:@"Customer":@"c"];
    [query join:@"CustomerSalesData":@"csd":@"c.KUNNR":@"csd.KUNNR"];
    [query join:@"CustZTERM":@"cuZTERM":@"cuZTERM.ZTERM":@"csd.ZTERM"];
    //[query join:@"SalesActivity":@"sa":@"sa.PARTN_ID":@"c.KUNNR"];
    
    NSArray* searchStringTokens = [searchString componentsSeparatedByString: @" "];
    SUPCompositeTest *outerCompositeTest = [SUPCompositeTest getInstance];
    [outerCompositeTest setOperator:SUPCompositeTest_AND];
    for (int i = 0; i < searchStringTokens.count; i++){
        SUPCompositeTest *ct = [SUPCompositeTest getInstance];
        [ct setOperator:SUPCompositeTest_OR];
        NSString *searchStringToken = [searchStringTokens objectAtIndex:i];
        for (NSString *key in searchKeys) {
            //SUPAttributeTest *at = [self getAttributeForKey:key andValue:searchStringToken];
            
            SUPAttributeTest *at = [SUPAttributeTest getInstance];
            at.attribute = key;
            at.testValue = searchStringToken;
            at.Operator = SUPAttributeTest_EQUAL;
            
            [ct add:at];
        }
        [outerCompositeTest add:ct];
    }
    [query orderBy:@"c.NAME1" : [SUPSortOrder ASCENDING]];
    [query where:outerCompositeTest];
    SUPQueryResultSet* resultList = [SalesPlusERPSalesPlus_SuperUserDB executeQuery:query];
    
    if (resultList == nil){
        //do a basic search on the customerMBO
        NSArray *searchKeys = [NSArray arrayWithObjects:
                               @"KUNNR",    // User Name
                               nil];
        
        SUPQuery *query = [SUPQuery getInstance];
        [query select:@"c.KUNNR, c.NAME1"];
        [query from:@"Customer":@"c"];
        NSArray* searchStringTokens = [searchString componentsSeparatedByString: @" "];
        SUPCompositeTest *outerCompositeTest = [SUPCompositeTest getInstance];
        [outerCompositeTest setOperator:SUPCompositeTest_AND];
        for (int i = 0; i < searchStringTokens.count; i++){
            SUPCompositeTest *ct = [SUPCompositeTest getInstance];
            [ct setOperator:SUPCompositeTest_OR];
            for (NSString *key in searchKeys) {
                SUPAttributeTest *at = [SUPAttributeTest getInstance];
                at.attribute = key;
                at.testValue = [searchStringTokens objectAtIndex:i];
                at.operator = SUPAttributeTest_EQUAL;
                [ct add:at];
            }
            [outerCompositeTest add:ct];
        }
        [query orderBy:@"NAME1" : [SUPSortOrder ASCENDING]];
        [query where:outerCompositeTest];
        resultList = [SalesPlusERPSalesPlus_SuperUserDB executeQuery:query];
        MBOLog(@"Simple Customer Search");
    }
    if (resultList == nil){
        return [NSArray array];
    }else{
        NSMutableArray *customerList = [NSMutableArray arrayWithCapacity:resultList.length];
        
        for(SUPDataValueList* result in resultList){
            SalesPlusCustomer *c = [SalesPlusCustomer findByPrimaryKey:[SUPDataValue  getNullableString:[result item:0]]];
            NSMutableDictionary *temp = [c toDictionaryWithKeys:self.customerKeys];
            [temp addEntriesFromDictionary:[self customerDetailsForKUNNR:c.KUNNR]];
            [customerList addObject:temp];
        }
        
        return [NSArray arrayWithArray:customerList];
    }
}


- (NSDictionary *)customerBasicDetailsForKUNNR:(NSString *)kunnr{
    SalesPlusCustomer *customer = [SalesPlusCustomer findByPrimaryKey:kunnr];
    if (customer){
        NSMutableDictionary *temp = [customer toDictionaryWithKeys:self.customerKeys];
        return temp;
    }else{
        return [NSDictionary dictionary];
    }
}

- (NSDictionary *)customerDetailsForKUNNR:(NSString *)kunnr{
    SalesPlusCustomer *customer = [SalesPlusCustomer findByPrimaryKey:kunnr];
    if (customer){
        NSMutableDictionary *temp = [customer toDictionaryWithKeys:self.customerKeys];
        NSDictionary *salesData = [self salesDataForCustomer:customer];
        [temp addEntriesFromDictionary:salesData];
        NSMutableArray *bonusData = [self bonusDataForCustomer:customer];
        [temp setObject:bonusData forKey:@"BonusData"];
        NSArray *brschs = [[CustomizingManager defaultManager] brsch];
        for (int i = 0; i < brschs.count; i++){
            NSDictionary *brsch = [brschs objectAtIndex:i];
            if ([[brsch objectForKey:@"BRSCH"] isEqualToString:customer.BRSCH]){
                [temp setObject:[brsch objectForKey:@"BRTEXT"] forKey:@"BRTEXT"];
            }
        }
        NSArray *kdgrps = [[CustomizingManager defaultManager] kdgrp];
        for (int i = 0; i < kdgrps.count; i++){
            NSDictionary *kdgrp = [kdgrps objectAtIndex:i];
            if ([[kdgrp objectForKey:@"KDGRP"] isEqualToString:[temp objectForKey:@"KDGRP"]]){
                [temp setObject:[kdgrp objectForKey:@"KTEXT"] forKey:@"KTEXT"];
            }
        }
        return temp;
    }else{
        return [NSDictionary dictionary];
    }
    
}

- (NSMutableArray *)bonusDataForCustomer:(SalesPlusCustomer *)customer{
    SUPObjectList * resultList = customer.customerBonuss;
    NSArray *customerBonusKeys = [NSArray arrayWithObjects:
                                  @"SEQ_ID",
                                  @"AMOUNT",
                                  @"PERCENT",
                                  @"ACTIVE",
                                  nil];
    NSMutableArray *customerBonusList = [NSMutableArray arrayWithCapacity:resultList.length];
    for(SalesPlusCustomerBonus *cb in resultList){
        NSDictionary *temp = [cb toDictionaryWithKeys:customerBonusKeys];
        [customerBonusList addObject:temp];
    }
    return customerBonusList;
}

- (NSDictionary *)salesDataForCustomer:(SalesPlusCustomer *)customer{
    SUPObjectList * resultList = customer.customerSalesDatas;
    if (resultList == nil || resultList.length == 0) {
        return [NSDictionary dictionary];
    }
    SalesPlusCustomerSalesData *salesData = [resultList objectAtIndex:0];
    NSArray *customerSalesKeys = [NSArray arrayWithObjects:
                                  @"LIFSD",         // Delivery Lock
                                  @"AUFSD",         // Order Lock
                                  @"VKORG",
                                  @"VTWEG",
                                  @"SPART",
                                  @"VKGRP",
                                  @"VKBUR",         // Sales Office
                                  @"KLIMK",         // Credit Limit
                                  @"KLPRZ",         // Credit Limit Used
                                  @"SALES_YEAR",    // Sales Year
                                  @"SALES_YEAR_1",  // Sales Year 1
                                  @"SALES_YEAR_2",  // Sales Year 2
                                  @"SALES_YEAR_3",  // Sales Year 3
                                  @"SALES_REPR",    // Sales Representative
                                  @"BONUS",         // Cumulated Bonus
                                  @"ZTERM",         // Terms of Payment
                                  @"STOCK_VOL",     // Stocks (voluntary)
                                  @"STOCK_OBL",     // Stocks (obilgatory)
                                  @"PREPAYMENT",     // Prepayments
                                  @"FUNDS",          // Funds
                                  @"KONDA",          // Price Group
                                  @"IS_MEMBER",
                                  @"WAERS",
                                  @"KDGRP",
                                  @"SALESBONUS",
                                  @"DISTOSCLAE",
                                  nil];
    
    if (salesData){
        NSMutableDictionary *temp = [salesData toDictionaryWithKeys:customerSalesKeys];
        NSArray *zterms = [[CustomizingManager defaultManager] zterm];
        
        //Overwrite EUR with &#128; because we need the euro sign
        if ([[temp objectForKey:@"WAERS"] isEqualToString:@"EUR"] || [[temp objectForKey:@"WAERS"] isEqualToString:@""]){
            [temp setObject:@"&#128;" forKey:@"WAERS"];
        }
        // Get ZTERM text1 and text2 customizing
        for (int i = 0; i < zterms.count; i++){
            NSDictionary *zterm = [zterms objectAtIndex:i];
            if ([[zterm objectForKey:@"ZTERM"] isEqualToString:[temp objectForKey:@"ZTERM"]]){
                [temp setObject:[zterm objectForKey:@"TEXT1"] forKey:@"ZTERM_TEXT1"];
                [temp setObject:[zterm objectForKey:@"TEXT2"]  forKey:@"ZTERM_TEXT2"];
            }
        }
        int i = [(NSNumber *)[temp objectForKey:@"KLPRZ"] intValue];
        NSString *klimk2 = @"";
        if (i >= 100){
            klimk2 = @"X";
        }
        [temp setObject:klimk2 forKey:@"KLIMK2"];
        [temp setObject:@"" forKey:@"MEMBER"];
        return temp;
    }else{
        return [NSDictionary dictionary];
    }
    
}

- (void)updateCustomer:(NSMutableDictionary *)values
{
    NSString *kunnr = [values objectForKey:@"KUNNR"];
    SalesPlusCustomer *updateCustomer = [SalesPlusCustomer findByPrimaryKey:kunnr];
    if (updateCustomer){
        [values setValuesInMBO:updateCustomer];
        [updateCustomer update];
        [updateCustomer submitPending];
    }
}

@end
