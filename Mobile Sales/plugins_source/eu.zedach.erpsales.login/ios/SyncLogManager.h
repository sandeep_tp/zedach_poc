//
//  SyncLogManager.h
//  ERPSales
//
//  Created by YohanK on 2014-01-30.
//
//

#import "BaseManager.h"

@interface SyncLogManager : BaseManager

@property (nonatomic,strong) NSArray* syncLogKeys;

+ (id) defaultManager;

-(void)deleteAllEntries;
-(void)saveSyncLogs;
-(void)saveSyncLogforQuot:(NSArray *)values;
-(NSArray *)getSyncLog;

@end
