//
//  MaterialPictureParserJSON.m
//  MaterialPicture
//
//  Created by YohanK on 2013-10-02.
//  Copyright (c) 2013 Yohan K. All rights reserved.
//

#import "MaterialPictureParser.h"
#import "CDVReachability.h"
#import "MaterialPicture.h"
#import "SUPConnectionProfile.h"
#import "SalesPlusERPSalesPlus_SuperUserDB.h"

@implementation MaterialPictureParser

-(bool)getPictures{
    //check if its running in wifi as pictures are only synced when its run using WIFI
    NSString *remoteHostName = @"www.google.com";
    CDVReachability *reachability = [CDVReachability reachabilityWithHostName:remoteHostName];
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    if (netStatus != ReachableViaWiFi){
        NSLog(@"Not on WIFI");
        return NO;
    }else{
        pictures = [[NSMutableArray alloc]init];
        //get all the product ids its pictures
        productList = [SalesPlusProduct findAll];
        currentIndex = 0;
        [self getPicsNeedingUpdate];
        return YES;
    }
}

// get the list of all the product pictures that need to be downloaded from the server
-(void)getPicsNeedingUpdate {
    // Create the request.
    //    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://213.179.144.160:50001/PictureServiceRS/GetPicsNeedingUpdate"]];

    SUPConnectionProfile *sp = [SalesPlusERPSalesPlus_SuperUserDB getSynchronizationProfile];
    NSString *serverName = @"http://mdm.msc-mobile.com"; //[NSString stringWithFormat:@"%@://%@",[sp getNetworkProtocol],[sp getServerName]];
    NSString *url = [NSString stringWithFormat:@"%@:50001/PictureServiceRS/GetPicsNeedingUpdate",serverName];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json;" forHTTPHeaderField:@"Content-Type"];
    NSString *jsonBody = @"[ ";// = [NSString stringWithFormat:@"­ ",mode];

    for (int i = currentIndex; i < productList.length; i++){
        SalesPlusProduct *p = [productList objectAtIndex:i];
        NSString *fileName;
        //check if the image exist in the folder
        //remove the leading 0
        NSRange range = [p.MATNR rangeOfString:@"^0*" options:NSRegularExpressionSearch];
        currentMATNR = [p.MATNR stringByReplacingCharactersInRange:range withString:@""];
        MaterialPicture *mp = [MaterialPicture loadMaterialPictureObjectWithKey:currentMATNR];
        if (mp){
            //picture exist locally
            currentVersion = mp.version;
            currentSUP = mp.sup;
        }else{
            currentVersion = @"-1";
            currentSUP = @"dev";
        }
        fileName = @"";//[NSString stringWithFormat:@"%@_%@.jpg",currentMATNR,currentVersion];
        jsonBody = [NSString stringWithFormat:@"%@ { \"FileName\" : \"%@\" , \"Version\" : \"%@\" , \"Matnr\" : \"%@\" } , ",jsonBody, fileName,currentVersion, currentMATNR];
    }
    jsonBody = [NSString stringWithFormat:@"%@ ]",jsonBody];
    NSData* requestBodyData=[jsonBody dataUsingEncoding: [NSString defaultCStringEncoding] ];
    //NSData *requestBodyData = [jsonBody dataUsingEncoding:NSUTF8StringEncoding];
    request.HTTPBody = requestBodyData;
    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    getPicsNeedingUpdate = YES;
}

//download the product image one at a time
- (void)manageRequests {
    // Create the request.
    SUPConnectionProfile *sp = [SalesPlusERPSalesPlus_SuperUserDB getSynchronizationProfile];
    NSString *serverName = @"http://mdm.msc-mobile.com"; //[NSString stringWithFormat:@"%@://%@",[sp getNetworkProtocol],[sp getServerName]];
    NSString *url = [NSString stringWithFormat:@"%@:50001/PictureServiceRS/GetPicAsJSON",serverName];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json;" forHTTPHeaderField:@"Content-Type"];
    
    for (int i = currentIndex; i < picsNeedingUpdate.count; i++){
        NSDictionary *p = [picsNeedingUpdate objectAtIndex:i];
        NSString *fileName;
        currentMATNR = [p objectForKey:@"Matnr"];
        currentVersion = [p objectForKey:@"Version"];
        NSString *jsonBody = [NSString stringWithFormat:@"\"%@\"",currentMATNR];
        if (!requestSent){
            // Convert your data and set your request's HTTPBody property
            NSData* requestBodyData=[jsonBody dataUsingEncoding: [NSString defaultCStringEncoding] ];
            request.HTTPBody = requestBodyData;
            
            // Create url connection and fire request
            NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            requestSent = YES;
            currentIndex = i;
            break;
        }
    }
    if (currentIndex == picsNeedingUpdate.count){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SyncProductImagesDone" object:nil];
    }
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // parse out the json data
    NSError* error;
    NSDictionary *json = [NSJSONSerialization
                          JSONObjectWithData:responseData
                          options:kNilOptions
                          error:&error];
    NSString* newStr = [[NSString alloc] initWithData:responseData
                                             encoding:NSUTF8StringEncoding];
    if (getPicsNeedingUpdate){
        [picsNeedingUpdate removeAllObjects];
        picsNeedingUpdate = [NSJSONSerialization
                             JSONObjectWithData:responseData
                             options:kNilOptions
                             error:&error];
        getPicsNeedingUpdate = NO;
        [self manageRequests];
    }else{
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSArray  * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * documentsDirectory = [paths objectAtIndex:0];
        NSString * ProductsDirectory = [documentsDirectory stringByAppendingPathComponent:@"/Products"];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:ProductsDirectory])
            [[NSFileManager defaultManager] createDirectoryAtPath:ProductsDirectory withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
        if (json != nil){
            NSString *jsonPictureString = [json objectForKey:@"AsJSON"];
            currentMATNR = [json objectForKey:@"Matnr"];
            currentVersion = [json objectForKey:@"Version"];
            if (![jsonPictureString isEqualToString:@""]){
                NSString *fileType = @"jpg";
                NSString *str = @"data:image/jpg;base64,";
                str = [str stringByAppendingString:jsonPictureString];
                NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:str]];
                // save the image in the local directory
                NSString *filePath = [NSString stringWithFormat:@"%@/%@_%@.%@", ProductsDirectory, currentMATNR, currentVersion, fileType];
                NSLog(@"file path : %@", filePath);
                [imageData writeToFile:filePath atomically:YES];
                MaterialPicture *mp = [[MaterialPicture alloc] init];
                mp.matnr = currentMATNR;
                mp.version = currentVersion;
                mp.sup = currentSUP;
                mp.path = filePath;
                NSData *encodedMaterialPictureObject = [NSKeyedArchiver archivedDataWithRootObject:mp];
                [defaults setObject:encodedMaterialPictureObject forKey:mp.matnr];
                [defaults synchronize];
            }
        }
        requestSent = NO;
        currentIndex++;
        [self manageRequests];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    NSLog(@"Sync Product Images fail");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SyncProductImagesDone" object:nil];
}

@end
