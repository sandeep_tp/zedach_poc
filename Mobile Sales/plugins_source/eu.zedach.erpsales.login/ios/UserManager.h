//
//  UserManager.h
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-08-12.
//
//

#import "BaseManager.h"
#import "SalesPlusUser.h"

@interface UserManager : BaseManager

+ (id) defaultManager;

- (NSDictionary *)currentUserDetails;
- (SalesPlusUser *)currentUserMBO;

@end
