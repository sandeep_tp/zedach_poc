//
//  ResubmissionManager.m
//  ERPSales Dev
//
//  Created by Klaus Rollinger on 03.08.17.


#import "ResubmissionManager.h"
#import "SUPAbstractLocalEntity+ERPSales.h"
#import "SalesPlusResub.h"
#import "NSDictionary+ERPSales.h"

@implementation ResubmissionManager

@synthesize resubKeys = _resubKeys;

+ (id) defaultManager{
    static ResubmissionManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ResubmissionManager alloc]initWithTableName:@"Resub"];
        manager.resubKeys = [NSArray arrayWithObjects:
                             @"BUKRS",      //primary key
                             @"RESUB_TYP",  //primary key
                             @"VBELN",      //primary key, sales doc number
                             @"UNAME_AD",   //primary key
                             @"UNAME",      //primary key
                             @"RESUB_DATE",
                             nil];
    });
    return manager;
}


- (NSArray *)getResubmissionsForVBELN:(NSString *)vbeln{
    SUPObjectList *resubList = [SalesPlusResub findByVBELN:vbeln];
    if(resubList == nil){
        return [NSArray array];
    }else{
        NSMutableArray *result = [NSMutableArray arrayWithCapacity:resubList.length];
        for (SalesPlusResub *rs in resubList) {
            if ([rs pendingChange] != (SUPEntityPendingState) PendingDelete) {
                NSMutableDictionary *temp = [rs toDictionaryWithKeys:self.resubKeys];
                [result addObject:temp];
            }
        }
        return result;
    }
}

- (NSArray *)getResubmissionsForUNAME:(NSString *)uname{
    SUPObjectList *resubList = [SalesPlusResub findByUNAME:uname];
    if(resubList == nil){
        return [NSArray array];
    }else{
        NSMutableArray *result = [NSMutableArray arrayWithCapacity:resubList.length];
        for (SalesPlusResub *rs in resubList) {
            if ([rs pendingChange] != (SUPEntityPendingState) PendingDelete) {
                NSMutableDictionary *temp = [rs toDictionaryWithKeys:self.resubKeys];
                [result addObject:temp];
            }
            
        }
        return result;
    }
}


-(void)createResubmission:(NSDictionary *)values{
    SalesPlusResub *resubObject = [[SalesPlusResub alloc] init];
    NSDictionary *header = values;
    [header setValuesInMBO:resubObject];
    
    NSDate *resubValidDate;
    
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"yyyy-MM-dd"];
    [df setTimeZone: [NSTimeZone timeZoneWithName:@"GMT"]];
    resubValidDate = [df dateFromString: [header objectForKey:@"RESUB_DATE"]];
    
    resubObject.RESUB_DATE = resubValidDate;
    [resubObject create];
    [resubObject submitPending];
}



- (bool)updateResubmission:(NSDictionary *)values{
    NSDictionary *header = values ;
    SUPObjectList *objList = [SalesPlusResub findByVBELN:[header objectForKey:@"VBELN"] ] ; //findByPrimar:[header objectForKey:@"DOC_NUMBER"]];
    if (objList) {
        SalesPlusResub *resubObject =  [objList getObject:0] ;
        if (resubObject){
            NSDate *resubValidDate;
            
            NSDateFormatter* df = [[NSDateFormatter alloc] init];
            
            [df setDateFormat:@"yyyy-MM-dd"];
            [df setTimeZone: [NSTimeZone timeZoneWithName:@"GMT"]];
            resubValidDate = [df dateFromString: header[@"RESUB_DATE"]];
            
            
            //            bool status = [self deleteResubmissionNoPending:resubObject.VBELN];
            //            if (status == YES){
            ////                [self createResubmissionPending:values];
            //            }
            //resubObject.RESUB_DATE = resubValidDate;
            //[resubObject update];
            //[resubObject submitPending];
            return YES;
        }else{
            return NO;
        }
        
    }
}

-(bool)deleteResubmission:(NSString *)doc_number{
    SUPObjectList *objList = [SalesPlusResub findByVBELN:doc_number ] ;
    if (objList) {
        
        for (SalesPlusResub *resubObject in objList) {
            if (resubObject){
                //vm[resubObject cancelPending];
                [resubObject delete];
                [resubObject submitPending];
            }else{
                return NO;
            }
        }
        return YES;
    }
}


@end
