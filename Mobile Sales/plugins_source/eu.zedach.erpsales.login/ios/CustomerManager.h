//
//  CustomerManager.h
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-06-26.
//
//

#import <Foundation/Foundation.h>
#import "BaseManager.h"
#import "SalesPlusCustomer.h"

@interface CustomerManager : BaseManager

@property (strong,nonatomic) NSArray* customerLightList;
@property (strong,nonatomic) NSArray* customerKeys;

+(id) defaultManager;

- (NSArray *)getAllCustomers;

- (NSArray *)searchCustomersWithCriteria:(NSString*)searchString;

- (NSArray *)searchCustomersWithUNAME:(NSString*)searchString;

- (void)updateCustomer:(NSMutableDictionary *)values;

- (NSDictionary *)customerDetailsForKUNNR:(NSString *)kunnr;

- (NSDictionary *)customerBasicDetailsForKUNNR:(NSString *)kunnr;

- (NSMutableArray *)bonusDataForCustomer:(SalesPlusCustomer *)customer;

- (NSDictionary *)salesDataForCustomer:(SalesPlusCustomer *)customer;

- (NSArray *)getCustomerListFrom:(int)skip To:(int)take;
@end
