/*
 
 Copyright (c) Sybase, Inc. 2012   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform (Program), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file (Code).  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */


#import "CallbackHandler.h"
#import "SUPConnectionUtil.h"
#import "SUPSyncStatusInfo.h"
#import "SUPSyncStatusListener.h"

@implementation CallbackHandler

+ (CallbackHandler*)getInstance
{
    CallbackHandler* _me_1 = [[CallbackHandler alloc] init];
    return _me_1;
}

- (void)sendNotification:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

- (void)postNotification:(NSString *)notification withObject:(id)obj;
{
    // All callback notifications other than onSubscribe: will happen on a thread other than the main UI thread. So, if you
    // want to update the UI in response to a callback you need to post the notification from the main thread.
    NSNotification *n = [NSNotification notificationWithName:notification object:obj];
    [self performSelectorOnMainThread:@selector(sendNotification:) withObject:n waitUntilDone:NO];
}

- (void)onReplaySuccess:(id)theObject
{
    MBOLogInfo(@"================================================");
    MBOLogInfo(@"Replay Successful");
    MBOLogInfo(@"=================================================");
    
    [self postNotification:ON_REPLAY_SUCCESS withObject:theObject];
}

- (void)onReplayFailure:(id)theObject
{
    MBOLogInfo(@"================================================");
    MBOLogInfo(@"Replay Failure");
    MBOLogInfo(@"=================================================");
    
    [self postNotification:ON_REPLAY_FAILURE withObject:theObject];
}



- (void)onLoginSuccess
{
    MBOLogInfo(@"================================================");
    MBOLogInfo(@"Login Successful");
    MBOLogInfo(@"=================================================");
    
    [self postNotification:ON_LOGIN_SUCCESS withObject:nil];
}

- (void)onLoginFailure
{
	MBOLog(@"=============================");
	MBOLogError(@"Login Failed");
	MBOLog(@"=============================");
    
	[self postNotification:ON_LOGIN_FAILURE withObject:nil];
}

- (void)onSubscribeSuccess
{
    MBOLogInfo(@"================================================");
    MBOLogInfo(@"Subscribe Successful");
    MBOLogInfo(@"=================================================");
}

- (void)onImportSuccess
{
    MBOLogInfo(@"================================================");
    MBOLogInfo(@"import ends Successful");
    MBOLogInfo(@"=================================================");
    
    [self postNotification:ON_IMPORT_SUCCESS withObject:nil];
}

//The onSynchronize method overrides the
//onSynchronize method from SUPDefaultCallbackHandler.
- (SUPSynchronizationActionType)onSynchronize:(SUPObjectList *)syncGroupList withContext:(SUPSynchronizationContext *)context
{
    NSString *notification = nil;
    switch ([context status]) {
        case SUPSynchronizationStatus_STARTING:
            // perform necessary action
            break;
        case SUPSynchronizationStatus_ERROR:
            notification = ON_SYNCHRONISATION_FAILURE;
            break;
        case SUPSynchronizationStatus_FINISHING:
            notification = ON_SYNCHRONISATION_SUCCESS;
            break;
        case SUPSynchronizationStatus_ASYNC_REPLAY_UPLOADED:
            // perform necessary action
            break;
        case SUPSynchronizationStatus_ASYNC_REPLAY_COMPLETED:
            // perform necessary action
            break;
        case SUPSynchronizationStatus_DOWNLOADING:
            // perform necessary action
            break;
        case SUPSynchronizationStatus_STARTING_ON_NOTIFICATION:
            // perform necessary action
            break;
        case SUPSynchronizationStatus_UPLOADING:
            // perform necessary action
            break;
        case SUPSynchronizationStatus_CANCELED:
            break;
        case SUPSynchronizationStatus_INTERRUPTED_WITH_PARTIAL_DATA:
            break;
        default:
            break;
    }
    if (notification != nil)
        [self postNotification:notification withObject:nil];
    return SUPSynchronizationAction_CONTINUE;
}
@end
