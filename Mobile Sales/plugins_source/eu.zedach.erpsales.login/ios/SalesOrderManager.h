//
//  SalesOrderHistoryManager.h
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-06-27.
//
//

#import "BaseManager.h"

@interface SalesOrderManager : BaseManager

@property (nonatomic, retain) NSArray *salesOrderKeys;

+(id) defaultManager;

- (NSArray *)getAllListForCustomer:(NSString*)kunnr;

//- (NSArray *)salesOrderListForCustomer:(NSString*)kunnr;
//
//- (NSArray *)returnListForCustomer:(NSString*)kunnr;
//
//- (NSArray *)exchangeListForCustomer:(NSString*)kunnr;

- (NSArray *)getAllListForSalesActivity:(NSString*)vbelv;

//- (NSArray *)salesOrderListForSalesActivity:(NSString*)vbelv;
//
//- (NSArray *)returnListForSalesActivity:(NSString*)vbelv;
//
//- (NSArray *)exchangeListForSalesActivity:(NSString*)vbelv;

- (NSArray *)getRecentSalesOrderProductsForCustomer:(NSString *)kunnr withLimit:(int)number;

- (NSDictionary *)salesOrderDetails:(NSString *)vbeln;

- (void)createSalesOrder:(NSDictionary *)values;

- (bool)updateSalesOrder:(NSDictionary *)values;

- (bool)updateSalesOrderValidity:(NSDictionary *)values;

- (bool)deleteSalesOrder:(NSString *)doc_number;

+ (NSString*)base64forData:(NSData*)theData;

- (NSString *)isOpenQuotation:(NSString *)VBELN;

//- (void)createReturn:(NSDictionary *)values;
//
//- (bool)updateReturn:(NSDictionary *)values;
//
//- (bool)deleteReturn:(NSString *)doc_number;
//
//- (void)createExchange:(NSDictionary *)values;
//
//- (bool)updateExchange:(NSDictionary *)values;
//
//- (bool)deleteExchange:(NSString *)doc_number;

@end
