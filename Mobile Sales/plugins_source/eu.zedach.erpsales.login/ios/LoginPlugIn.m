//
//  LoginPlugIn.m
//  ERPSales
//
//  Created by Alexander Ilg on 5/11/13.
//
//

#import "LoginPlugIn.h"
#import "Util.h"

#import "SUPApplication.h"
#import "ApplicationCallbackHandler.h"
#import "SUPRegistrationStatus.h"

#import "SalesPlusERPSalesPlus_SuperUserDB.h"
#import "CallbackHandler.h"
#import "SUPEngine.h"
#import "SUPDataVault.h"
#import "SUPConnectionProfile.h"
#import "SalesPlusLocalKeyGenerator.h"
#import "SalesPlusCustomer.h"
#import <dispatch/dispatch.h>
#import "ContactManager.h"
#import "CustomerManager.h"
#import "CustomizingManager.h"
#import "ProductManager.h"
#import "ReportManager.h"
#import "SyncLogManager.h"
#import "UserManager.h"
#import "SalesPlusPersonalizationParameters.h"
#import "SalesPlusCustTitles.h"
#import "SalesPlusCustTitlesSubscription.h"
#import "SalesPlusCustCountry.h"
#import "SalesPlusCustCountrySubscription.h"
#import "SalesPlusCustCurrencies.h"
#import "SalesPlusCustCurrenciesSubscription.h"
#import "SalesPlusCustomerSalesDataSubscription.h"
#import "SalesPlusCustomerSalesData.h"
#import "SalesPlusSalesActivitySubscription.h"
#import "SalesPlusSalesActivity.h"
#import "SalesPlusSalesDocument.h"
#import "SalesPlusSalesDocumentSubscription.h"
#import "KeychainItemWrapper.h"
#import "SalesPlusCustKTAAR.h"
#import "SalesPlusCustKTAARSubscription.h"
#import "SalesPlusCustTitles.h"
#import "SalesPlusCustTitlesSubscription.h"
#import "SalesPlusProductSalesRel.h"
#import "SalesPlusProductSalesRelSubscription.h"
#import "SalesPlusContact.h"
#import "SalesPlusCustCountry.h"
#import "SalesPlusCustCountrySubscription.h"
#import "SalesPlusCustCurrencies.h"
#import "SalesPlusCustCurrenciesSubscription.h"
#import "SalesPlusCustMaterialGroups.h"
#import "SalesPlusCustMaterialGroupsSubscription.h"
#import "SalesPlusCustMaterialTypes.h"
#import "SalesPlusCustMaterialTypesSubscription.h"
#import "SalesPlusCustKTAER.h"
#import "SalesPlusCustKTAERSubscription.h"
#import "SalesPlusCustPARLA.h"
#import "SalesPlusCustPARLASubscription.h"
#import "SalesPlusReportingMasterUser.h"
#import "SalesPlusReportingMasterUserSubscription.h"
#import "SalesPlusReportingText.h"
#import "SalesPlusReportingTextSubscription.h"
#import "SalesPlusCustAUART.h"
#import "SalesPlusCustAUARTSubscription.h"
#import "SalesPlusCustGEWEI.h"
#import "SalesPlusCustGEWEISubscription.h"
#import "SalesPlusCustKONDA.h"
#import "SalesPlusCustKONDASubscription.h"
#import "SalesPlusCustKTAST.h"
#import "SalesPlusCustKTASTSubscription.h"
#import "SalesPlusCustPAFKT.h"
#import "SalesPlusCustPAFKTSubscription.h"
#import "SalesPlusCustPARH1.h"
#import "SalesPlusCustPARH1Subscription.h"
#import "SalesPlusCustPARH2.h"
#import "SalesPlusCustPARH2Subscription.h"
#import "SalesPlusCustPARH3.h"
#import "SalesPlusCustPARH3Subscription.h"
#import "SalesPlusCustUnitOfMeasures.h"
#import "SalesPlusCustUnitOfMeasuresSubscription.h"
#import "SalesPlusCustVKBUR.h"
#import "SalesPlusCustVKBURSubscription.h"
#import "SalesPlusCustZTERM.h"
#import "SalesPlusCustZTERMSubscription.h"
#import "MaterialPictureParser.h"
#import "SUPSyncStatusInfo.h"
#import "SalesPlusOpenQuotation.h"
#import "SalesPlusOpenQuotationSubscription.h"
#import "SalesPlusBOLinkCustomerText.h"
#import "SalesPlusBOLinkCustomerTextSubscription.h"
#import "SalesPlusContactReturn.h"
#import "SalesPlusContactReturnSubscription.h"
#import "SalesPlusCustomerReturn.h"
#import "SalesPlusCustomerReturnSubscription.h"
#import "SalesPlusCustStatusIndicator.h"
//#import "SalesPlusCustStatusIndicatorSubscription.h"
#import "SalesPlusCustCustomerGroup.h"
#import "SalesPlusCustCustomerGroupSubscription.h"
#import "SalesPlusCustIndustry.h"
#import "SalesPlusCustIndustrySubscription.h"
#import "SalesPlusSalesDocumentReturn.h"
#import "SalesPlusSalesDocumentReturnSubscription.h"
#import "SalesPlusSalesActivityReturn.h"
#import "SalesPlusSalesActivityReturnSubscription.h"
#import "SalesPlusBOLinkUser.h"
#import "SalesPlusBOLinkUserSubscription.h"
#import "SalesPlusBOLinkUserText.h"
#import "SalesPlusBOLinkUserTextSubscription.h"
#import "SalesPlusCustUserClosePlant.h"
#import "SalesPlusCustUserClosePlantSubscription.h"
#import "SalesPlusCRUDSyncLog.h"
#import "SalesPlusCRUDSyncLogSubscription.h"
#import "AppDelegate.h"
#import "SalesPlusResub.h"

//#include <stdexcept>

@implementation LoginPlugIn

@synthesize connectStartTime, firstrun, pin;

@synthesize passwordAlert, noTransportAlert, connectionFailed;

@synthesize _username, _password;

@synthesize serverName, serverProtocol;
NSString* _domain = @"com.zedach.salesplus";
int _ecEnterConnectionSettings = 1;
int _ecEnterCredentials = 2;
int _ecCredentialsWrong = 3;
int _ecRegistrationFailed = 4;
int _ecSyncFailed = 5;
int _ecPluginInit = 6;
//+ (id) defaultManager{
//    static LoginPlugIn *manager = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        manager = [[LoginPlugIn alloc] init];
//    });
//    return manager;
//}

- (void)initialSync:(CDVInvokedUrlCommand*)command {
    if([SUPApplication registrationStatus] == SUPRegistrationStatus_REGISTERED) {
        MBOLog(@"initialSync called");
        [self synchronize:command];
    }
}

- (void) syncProductImagesDone:(NSNotification *) notification{
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:cdvURLCommand.callbackId];
    [[NSNotificationCenter defaultCenter] removeObserver:@"SyncProductImagesDone"];
    MBOLog(@"Sync Product Images finished");
}

-(void)setPreSyncSettings:(CDVInvokedUrlCommand*)command
{
    serverProtocol = [command.arguments objectAtIndex:2];
    serverName = [command.arguments objectAtIndex:3];
    NSString* syncPort = [command.arguments objectAtIndex:4];
    int syncPortNum =  syncPort ? [syncPort intValue] : 0;
    NSString* port = [command.arguments objectAtIndex:5];
    int portNum =  port ? [port intValue] : 0;
    NSString* farmID = [command.arguments objectAtIndex:6];
    NSString* syncFarmID = [command.arguments objectAtIndex:7];
    
    SUPConnectionProfile *sp = [SalesPlusERPSalesPlus_SuperUserDB getSynchronizationProfile];
    //[sp enableTrace:YES];
    [sp setAsyncReplay:NO];  //If you want to do asynchronous replay ,you can set to YES.
    [sp setNetworkProtocol:serverProtocol];
    [sp setServerName:serverName];
    [sp setPortNumber:syncPortNum];
    // Relay server
    NSString *urlSuffix = [NSString stringWithFormat:@"url_suffix=/ias_relay_server/client/rs_client.dll/%@",syncFarmID];
    [sp setString:@"mlUrlSuffix":urlSuffix];
    [sp setNetworkStreamParams:@"trusted_certificates=zedach_root.cer"];
    
    SUPConnectionProfile *cp = [SalesPlusERPSalesPlus_SuperUserDB getConnectionProfile];
    [cp setNetworkProtocol:serverProtocol];
    [cp setServerName:serverName];
    [cp setPortNumber:syncPortNum];
    [cp setUser:_username];   //Login User name
    [cp setPassword:_password]; //Login password
    [cp.syncProfile setUser:_username];
    [cp.syncProfile setPassword:_password];
}

- (void)synchronize:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"synchronize called");
    if (command != nil){
        cdvURLCommand = command;
        //[self setPreSyncSettings:command];
    }
    
    UserManager *manager = [UserManager defaultManager];
    SalesPlusUser *userMBO = [manager currentUserMBO];
    [self setSynchronisationKeys:userMBO];
    
    MBOLog(@"Sync started");
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    @try {
        [SalesPlusERPSalesPlus_SuperUserDB synchronize];
    }
    @catch(SUPPersistenceException *pe){
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"05"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        if ([[pe message] rangeOfString:@"loginFail,Sync failed"].location == NSNotFound) {
            //[self showNoTransportAlert:@"An error occurred when attempting to log in."];
        } else {
            //[self showNoTransportAlert:@"Incorrect username or password."];
        }
        return;
    }
    @catch (SUPSynchronizeException *se) {
        //[self showNoTransportAlert:@"Sync Error"];
        return;
    }
    @catch (SUPConnectionPropertyException *cpe) {
        //[self showNoTransportAlert:@"Error in connection"];
        return;
    }
    @catch (SUPApplicationRuntimeException *re) {
        //[self showNoTransportAlert:@"Incorrect username or password."];
        return;
    }
    @finally {
        //Write  code for datavault lock if you have one
    }
    ProductManager *pm = [ProductManager defaultManager];
    [pm clearCache];
    CustomizingManager *cm = [CustomizingManager defaultManager];
    [cm clearCache];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0ul);
    dispatch_async(queue, ^{
        [[CustomizingManager defaultManager] loadAllCustoming];
        [pm performSelectorInBackground:@selector(updateProductCountForCategories) withObject:nil];
    });
    MBOLog(@"Sync finished");
    // save the sync log
    SyncLogManager *syncManager = [SyncLogManager defaultManager];
    [syncManager saveSyncLogs];
    //get product images
    //    MaterialPictureParser *mpp = [[MaterialPictureParser alloc] init];
    //    if ([mpp getPictures]){
    //        MBOLog(@"Sync Product Images started");
    //        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncProductImagesDone:) name:@"SyncProductImagesDone"
    //                                                   object:nil];
    //    }else{
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:cdvURLCommand.callbackId];
    //    }
    return;
}

- (void)synchronizeQuotationMBO:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"synchronizeQuotationMBO called");
    if (command != nil){
        cdvURLCommand = command;
        //[self setPreSyncSettings:command];
    }
    
    MBOLog(@"OpenQuotations Sync started");
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    @try {
        [SalesPlusERPSalesPlus_SuperUserDB synchronize:@"OpenQuotations"];
    }
    @catch(SUPPersistenceException *pe){
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"05"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        MBOLog(@"OpenQuotations Sync Failed");
    }
    @finally {
        
    }
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    MBOLog(@"OpenQuotations Sync finished");
    
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:cdvURLCommand.callbackId];
}

- (void)syncLogQuotationMBO:(CDVInvokedUrlCommand*)command
{
    NSArray *values = [command arguments];
    // Create Sync log for MBO
    SyncLogManager *syncManager = [SyncLogManager defaultManager];
    [syncManager saveSyncLogforQuot:values];
    
}

- (void)synchronizeSalesMBO:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"synchronizeSalesMBO called");
    if (command != nil){
        cdvURLCommand = command;
        //[self setPreSyncSettings:command];
    }
    
    MBOLog(@"Sales Activity Orders Sync started");
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    @try {
        [SalesPlusERPSalesPlus_SuperUserDB synchronize:@"SalesSynchronization"];
    }
    @catch(SUPPersistenceException *pe){
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"05"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        MBOLog(@"SalesSynchronization Sync Failed");
    }
    @finally {
        
    }
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    MBOLog(@"SalesSynchronization Sync finished");
    
    SyncLogManager *syncManager = [SyncLogManager defaultManager];
    [syncManager saveSyncLogs];
    
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:cdvURLCommand.callbackId];
}

- (bool)synchronizeUserMBO:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"synchronizeUserMBO called");
    //[self setPreSyncSettings:command];
    
    MBOLog(@"User Sync started");
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    @try {
        [SalesPlusERPSalesPlus_SuperUserDB synchronize:@"User"];
    }
    @catch(SUPPersistenceException *pe){
        if ([pe errorCode] == 3217 || [[pe reason] rangeOfString:@"loginFail"].location != NSNotFound) {
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"03"];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            //[self showNoTransportAlert:@"An error occurred when attempting to log in."];
        } else {
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"05"];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
        return NO;
    }
    @finally {
        
    }
    // get the current sync user
    UserManager *manager = [UserManager defaultManager];
    SalesPlusUser *userMBO = [manager currentUserMBO];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:userMBO.BNAME forKey:@"currentUser"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    MBOLog(@"User Sync finished");
    return YES;
}


- (void)setSynchronizationParams{
    //get personalization key values
    SalesPlusPersonalizationParameters *pp =  [SalesPlusERPSalesPlus_SuperUserDB getPersonalizationParameters];
    pp.username = _username;
    pp.password = _password;
    [pp save];
    while ([SalesPlusERPSalesPlus_SuperUserDB hasPendingOperations])
        [NSThread sleepForTimeInterval:0.2];
    pp =  [SalesPlusERPSalesPlus_SuperUserDB getPersonalizationParameters];
    MBOLog(@"Username %@ Password %@ ", pp.username, pp.password);
}

- (void)changeUser:(CDVInvokedUrlCommand*)command
{
    NSLog(@"changeUser called");
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getCurrentUserCredentials:(CDVInvokedUrlCommand *)command
{
    MBOLog(@"getCurrentUserCredentials called");
    UserManager *manager = [UserManager defaultManager];
    NSString *mboUser = @"";
    NSString *mboPwd = @"";
    SalesPlusUser *userMBO;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *currentUser = [defaults objectForKey:@"currentUser"];
    if (currentUser && ![currentUser isEqualToString:@""])
    {
        userMBO = [manager currentUserMBO];
    }
    if (userMBO) {
        mboUser = userMBO.BNAME;
        mboPwd = userMBO.PASSWORD;
    }
    NSDictionary *credentials = [NSDictionary dictionaryWithObjectsAndKeys:mboUser, @"username", mboPwd, @"password", nil];
    NSString *result = [Util convertNSDictionaryToJSON:credentials];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)login:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"login called");
    bool status = false;
    NSLog(@"command.arguments %@", command.arguments);
    NSString* msg;
    
    // create the plugin result (PhoneGap)
    CDVPluginResult* pluginResult = nil;
    
    // get the username and password information from PhoneGap
    NSString* user = [command.arguments objectAtIndex:0];
    NSString* pwd = [command.arguments objectAtIndex:1];
    _username = user;
    _password = pwd;
    
    serverProtocol = [command.arguments objectAtIndex:2];
    serverName = [command.arguments objectAtIndex:3];
    NSString* syncPort = [command.arguments objectAtIndex:4];
    //int syncPortNum =  syncPort ? [syncPort intValue] : 0;
    NSString* port = [command.arguments objectAtIndex:5];
    //int portNum =  port ? [port intValue] : 0;
    NSString* farmID = [command.arguments objectAtIndex:6];
    
    if([serverProtocol length] == 0 || [serverName length] == 0 || [syncPort length] == 0 || [port length] == 0 || [farmID length] == 0){
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsInt:_ecEnterConnectionSettings];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    } else {
        // Check if user name and password are entered.
        // if username or password are empty, we return an error to the UI
        if([user length] == 0 || [pwd length] == 0){
            // If not, display an error message
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsInt:_ecEnterCredentials];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
        else {
            // If yes, continue
            //Initialize SMP layer
            NSError* error;
            [self initializeSmpLayer:command error:&error];
            if(error != NULL){
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsInt:error.code];
            }
            else{
                UserManager *manager = nil;
                SalesPlusUser *userMBO = nil;
                
                //case: app not register -> register with SMP
                if([SUPApplication registrationStatus] != SUPRegistrationStatus_REGISTERED){
                    @try{
                        [self resetApp];
                    }
                    @catch(NSException* ex){
                        //this is no show stopper, redownload of DB might still work
                        NSLog(@"resetting app failed");
                    }
                    
                    NSError* error;
                    [self registerApplication2:command error:&error];
                    pluginResult = error == NULL ?
                    [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"triggerSync"]:
                    [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsInt:_ecRegistrationFailed];
                }
                else {
                    manager = [UserManager defaultManager];
                    userMBO = [manager currentUserMBO];
                    NSString *mboUser = userMBO.BNAME;
                    NSString *mboPwd = userMBO.PASSWORD;
                    
                    //case: app already registered -> check user credentials
                    if ([mboUser isEqualToString:user] && [mboPwd isEqualToString:pwd]) {
                        // login successful
                        NSLog(@"login success");
                        [self setSynchronisationKeys:userMBO];
                        [self initializeMandatoryManagers];
                        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
                    }
                    else{
                        //case: username is different -> reset app and download DB for new user
                        if(![mboUser isEqualToString:user]){
                            //reset app
                            @try{
                                [self resetApp];
                            }
                            @catch(NSException* ex){
                                //this is no show stopper, redownload of DB might still work
                                NSLog(@"resetting app failed");
                            }
                            
                            //register app with SMP server
                            if(pluginResult == nil){
                                NSError* error;
                                [self registerApplication2:command error:&error];
                                if(error == NULL){
                                    [self setSynchronisationKeys:userMBO];
                                    [self initializeMandatoryManagers];
                                    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"triggerSync"];
                                }
                                else{
                                    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsInt:error.code];
                                }
                            }
                        }
                        //case: password is different -> try to sync the userMBO
                        else {
                            status = [self synchronizeUserMBO:command];
                            [self setSynchronisationKeys:userMBO];
                            [self initializeMandatoryManagers];
                            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
                        }
                    }
                }
            }
            if (pluginResult != nil){
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            }
        }
    }
}


- (void) registerApplication2:(CDVInvokedUrlCommand*)command error:(NSError**)error{
    @try{
        SUPApplication *app = [SUPApplication getInstance];
        
        while ([SUPApplication registrationStatus] != SUPRegistrationStatus_REGISTERED) {
            @try {
                [app registerApplication:60]; //also starts the connection
            }
            @catch (NSException *ex) {
                MBOLog(@"******************");
                MBOLog(@"Exception thrown in registration: %@, %@, %@",[ex description], [ex name], [ex reason]);
                MBOLog(@"******************");// login not successful
                if(error != NULL){
                    NSMutableDictionary* userInfo= [NSMutableDictionary dictionary];
                    [userInfo setObject:[ex reason] forKey:NSLocalizedDescriptionKey];
                    *error = [[NSError alloc] initWithDomain:_domain code:4 userInfo:userInfo];
                }
                return;
            }
        }
        MBOLog(@"Registration succeeded");
        
        // Synchronize the Synchronisation Group "User" to get the User MBO
        [self synchronizeUserMBO:command];
    }
    @catch(NSException* ex){
        if(error != NULL){
            NSMutableDictionary* userInfo= [NSMutableDictionary dictionary];
            [userInfo setObject:[ex reason] forKey:NSLocalizedDescriptionKey];
            *error = [[NSError alloc] initWithDomain:_domain code:_ecPluginInit userInfo:userInfo];
        }
        [self logException:ex defaultMessage:@"registering app failed"];
    }
}

-(void) resetApp{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"" forKey:@"currentUser"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    SUPApplication* app = [SUPApplication getInstance];
    
    //KR: not sure if this code block is really needed
    [self removeSubscriptions];
    
    
    //first try unregister at SMP server
    @try {
        [SUPApplication unregisterApplication:60];
    }
    @catch (NSException *ex) {
        [self logException:ex defaultMessage:@"unregistering app failed"];
        [app resetApplication];//resets SMP registration on the client
    }
    
    [app stopConnection];
    if([SalesPlusERPSalesPlus_SuperUserDB databaseExists]){
        //[app clearApplicationState];
        [SalesPlusERPSalesPlus_SuperUserDB cleanAllData:NO];
    }
    
    //just in case
    [self clearManagerCaches];
}

-(void) clearManagerCaches{
    ContactManager* cManager = [ContactManager defaultManager];
    [cManager clearCache];
    
    //CustomerManager* customerManager = [CustomerManager defaultManager];
    
    CustomizingManager* customizingManager = [CustomizingManager defaultManager];
    [customizingManager clearCache];
    
    ProductManager* pManager = [ProductManager defaultManager];
    [pManager clearCache];
    
    //ReportManager* rmanager = [ReportManager defaultManager];
    
    SyncLogManager* sManager = [SyncLogManager defaultManager];
    [sManager clearCache];
    
    UserManager* uManager = [UserManager defaultManager];
    [uManager clearCache];
}

-(void) initializeMandatoryManagers{
    //update the product catalog to find out about the available products
    ProductManager *pm = [ProductManager defaultManager];
    [pm clearCache];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0ul);
    dispatch_async(queue, ^{
        [[CustomizingManager defaultManager] loadAllCustoming];
        [pm performSelectorInBackground:@selector(updateProductCountForCategories) withObject:nil];
    });
}

- (void) initializeSmpLayer:(CDVInvokedUrlCommand*)command error:(NSError**)error{
    @try{
        NSString* user = [command.arguments objectAtIndex:0];
        NSString* pwd = [command.arguments objectAtIndex:1];
        NSString* serverProtocol = [command.arguments objectAtIndex:2];
        NSString* serverName = [command.arguments objectAtIndex:3];
        NSString* syncPort = [command.arguments objectAtIndex:4];
        int syncPortNum =  syncPort ? [syncPort intValue] : 0;
        NSString* port = [command.arguments objectAtIndex:5];
        int portNum =  port ? [port intValue] : 0;
        NSString* mbsFarmId = [command.arguments objectAtIndex:6];
        NSString* rbsFarmId = [command.arguments objectAtIndex:7];
        
        SUPLoginCredentials* login = [SUPLoginCredentials getInstance];
        login.username = user;
        login.password = pwd;
        
        //login save:<#(NSString *)#> withVault:<#(SUPDataVault *)#>
        
        SUPApplication *app = [SUPApplication getInstance];
        if (app.applicationIdentifier == nil) {
            app.applicationIdentifier = @"ERPSalesPlus_SuperUser";
        }
        
        SUPConnectionProperties* props= app.connectionProperties;
        [props setNetworkProtocol:serverProtocol];
        [props setServerName:serverName];
        [props setPortNumber:portNum];
        [props setFarmId:mbsFarmId];
        [props setSecurityConfiguration:@"bsplogin"];
        props.loginCredentials = login;
        // Relay server
        [props setUrlSuffix:@"/ias_relay_server/client/rs_client.dll"];
        props.farmId = [NSString stringWithFormat:@"%@",mbsFarmId];
        
        [SalesPlusERPSalesPlus_SuperUserDB setApplication:app];
        
        CallbackHandler* handler = [CallbackHandler getInstance];
        [SalesPlusERPSalesPlus_SuperUserDB registerCallbackHandler:handler];
        ApplicationCallbackHandler *appHandler = [ApplicationCallbackHandler getInstance];
        app.applicationCallback = appHandler;
        
        //set MBS properties
        SUPConnectionProfile *cp = [SalesPlusERPSalesPlus_SuperUserDB getConnectionProfile];
        [cp.syncProfile setDomainName:@"Default"];
        //[cp enableTrace:YES];
        //[cp.syncProfile enableTrace:YES];
        [cp.syncProfile setUser:user];
        [cp.syncProfile setPassword:pwd];
        [cp.syncProfile setServerName:serverName];
        [cp.syncProfile setPortNumber:syncPortNum];
        [cp.syncProfile setAsyncReplay:NO];
        [cp.syncProfile setNetworkProtocol:serverProtocol];
        
        //set RBS properties
        SUPConnectionProfile *sp = [SalesPlusERPSalesPlus_SuperUserDB getSynchronizationProfile];
        //[sp enableTrace:YES];
        [sp setAsyncReplay:NO];  //If you want to do asynchronous replay ,you can set to YES.
        [sp setNetworkProtocol:serverProtocol];
        [sp setServerName:serverName];
        [sp setPortNumber:syncPortNum];
        // Relay server
        NSString *urlSuffix = [NSString stringWithFormat:@"url_suffix=/ias_relay_server/client/rs_client.dll/%@",rbsFarmId];
        [sp setString:@"mlUrlSuffix":urlSuffix];
        [sp setNetworkStreamParams:@"trusted_certificates=zedach_root.cer"];
        
    }
    @catch(NSException* ex){
        [self logException:ex defaultMessage:@"could not initialize SMP layer"];
        if(error != NULL){
            NSMutableDictionary* userInfo= [NSMutableDictionary dictionary];
            [userInfo setObject:[ex reason] forKey:NSLocalizedDescriptionKey];
            *error = [[NSError alloc] initWithDomain:_domain code:_ecPluginInit userInfo:userInfo];
        }
    }
    
}

-(void)logException:(NSException* )ex defaultMessage:(NSString* )msg{
    NSString* logMsg = [NSString stringWithFormat:@"%@: ex.name: %@ ex.reason: %@ ex.description: %@", msg, [ex name], [ex reason], [ex description]];
    MBOLog(logMsg);
}

- (void)removeSubscriptions{
    
    SUPObjectList *resub = [SalesPlusResub getSubscriptions];
    if (resub.length > 0){
        [SalesPlusResub removeSubscription:[resub objectAtIndex:0]];
    }
    
    SUPObjectList *r = [SalesPlusCustomerSalesData getSubscriptions];
    if (r.length > 0){
        [SalesPlusCustomerSalesData removeSubscription:[r objectAtIndex:0]];
    }
    //    SUPObjectList *r2 = [SalesPlusCustStatusIndicator getSubscriptions];
    //    if (r2.length > 0){
    //        [SalesPlusCustStatusIndicator removeSubscription:[r2 objectAtIndex:0]];
    //    }
    SUPObjectList* s = [SalesPlusSalesActivity getSubscriptions];
    if (s.length > 0){
        [SalesPlusSalesActivity removeSubscription:[s objectAtIndex:0]];
    }
    SUPObjectList* t = [SalesPlusSalesDocument getSubscriptions];
    if (t.length > 0){
        [SalesPlusSalesDocument removeSubscription:[t objectAtIndex:0]];
    }
    SUPObjectList* p = [SalesPlusProductSalesRel getSubscriptions];
    if (p.length > 0){
        [SalesPlusProductSalesRel removeSubscription:[p objectAtIndex:0]];
    }
    SUPObjectList* u = [SalesPlusReportingMasterUser getSubscriptions];
    if (u.length > 0){
        [SalesPlusReportingMasterUser removeSubscription:[u objectAtIndex:0]];
    }
}

- (void)setSynchronisationKeys:(SalesPlusUser*)userMBO{
    // Set Sychronisation Keys
    
    SUPObjectList *resub = [SalesPlusResub getSubscriptions];
    if (resub.length == 0 ){
        SalesPlusResubSubscription *resubSc = [SalesPlusResubSubscription getInstance];
        resubSc.BNAME = userMBO.BNAME;
        [SalesPlusResub addSubscription:resubSc];
    }
    
    
    SUPObjectList *r = [SalesPlusCustomerSalesData getSubscriptions];
    if (r.length == 0 ){
        SalesPlusCustomerSalesDataSubscription *sp = [SalesPlusCustomerSalesDataSubscription getInstance];
        sp.VKORG = userMBO.VKORG;
        //sp.VKBUR = userMBO.VKBUR;
        [SalesPlusCustomerSalesData addSubscription:sp];
    }
    
    //    SUPObjectList *r2 = [SalesPlusCustStatusIndicator getSubscriptions];
    //    if (r2.length == 0 ){
    //        SalesPlusCustStatusIndicatorSubscription *sp2 = [SalesPlusCustStatusIndicatorSubscription getInstance];
    //        sp2.VKORG = userMBO.VKORG;
    //        [SalesPlusCustStatusIndicator addSubscription:sp2];
    //    }
    
    SUPObjectList* s = [SalesPlusSalesActivity getSubscriptions];
    if (s.length == 0){
        SalesPlusSalesActivitySubscription *sasp = [SalesPlusSalesActivitySubscription getInstance];
        //        sasp.VKORG = userMBO.VKORG;
        //        sasp.VTWEG = userMBO.VTWEG;
        sasp.SALESORG = userMBO.VKORG;
        //sasp.VKBUR = userMBO.VKBUR;
        [SalesPlusSalesActivity addSubscription:sasp];
    }
    
    SUPObjectList* t = [SalesPlusSalesDocument getSubscriptions];
    if (t.length == 0){
        SalesPlusSalesDocumentSubscription *sosp = [SalesPlusSalesDocumentSubscription getInstance];
        //        sosp.VKORG = userMBO.VKORG;
        //        sosp.VTWEG = userMBO.VTWEG;
        //        sosp.SPART = userMBO.SPART;
        //sosp.VKBUR = userMBO.VKBUR;
        [SalesPlusSalesDocument addSubscription:sosp];
    }
    
    SUPObjectList* q = [SalesPlusOpenQuotation getSubscriptions];
    if (q.length == 0){
        SalesPlusOpenQuotationSubscription *oqsp = [SalesPlusOpenQuotationSubscription getInstance];
        oqsp.VKORG = userMBO.VKORG;
        //oqsp.VTWEG = userMBO.VTWEG;
        oqsp.SPART = userMBO.SPART;
        //oqsp.VKBUR = userMBO.VKBUR;
        [SalesPlusOpenQuotation addSubscription:oqsp];
    }
    
    SUPObjectList* p = [SalesPlusProductSalesRel getSubscriptions];
    if (p.length == 0){
        SalesPlusProductSalesRelSubscription *srsp = [SalesPlusProductSalesRelSubscription getInstance];
        //        srsp.VKORG = userMBO.VKORG;
        //        srsp.VTWEG = userMBO.VTWEG;
        srsp.WERKS = userMBO.WERK;
        [SalesPlusProductSalesRel addSubscription:srsp];
    }
    
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString* preferredLang = [languages objectAtIndex:0];
    NSString *language = [[preferredLang capitalizedString] substringToIndex:1];
    
    //Country List
    SUPObjectList* list = [SalesPlusCustCountry getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustCountry removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustCountry getSubscriptions];
    if (list.length == 0){
        SalesPlusCustCountrySubscription *s = [SalesPlusCustCountrySubscription getInstance];
        s.Language = language;
        [SalesPlusCustCountry addSubscription:s];
    }
    
    //Customer Group
    list = [SalesPlusCustCustomerGroup getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustCustomerGroup removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustCustomerGroup getSubscriptions];
    if (list.length == 0){
        SalesPlusCustCustomerGroupSubscription *s = [SalesPlusCustCustomerGroupSubscription getInstance];
        s.Language = language;
        [SalesPlusCustCustomerGroup addSubscription:s];
    }
    
    //Customer Industry
    list = [SalesPlusCustIndustry getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustIndustry removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustIndustry getSubscriptions];
    if (list.length == 0){
        SalesPlusCustIndustrySubscription *s = [SalesPlusCustIndustrySubscription getInstance];
        s.Language = language;
        [SalesPlusCustIndustry addSubscription:s];
    }
    
    //Currency List
    list = [SalesPlusCustCurrencies getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustCurrencies removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustCurrencies getSubscriptions];
    if (list.length == 0){
        SalesPlusCustCurrenciesSubscription *s = [SalesPlusCustCurrenciesSubscription getInstance];
        s.Language = language;
        [SalesPlusCustCurrencies addSubscription:s];
    }
    
    //MaterialGroup List
    list = [SalesPlusCustMaterialGroups getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustMaterialGroups removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustMaterialGroups getSubscriptions];
    if (list.length == 0){
        SalesPlusCustMaterialGroupsSubscription *s = [SalesPlusCustMaterialGroupsSubscription getInstance];
        s.Language = language;
        [SalesPlusCustMaterialGroups addSubscription:s];
    }
    
    //MaterialType List
    list = [SalesPlusCustMaterialTypes getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustMaterialTypes removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustMaterialTypes getSubscriptions];
    if (list.length == 0){
        SalesPlusCustMaterialTypesSubscription *s = [SalesPlusCustMaterialTypesSubscription getInstance];
        s.Language = language;
        [SalesPlusCustMaterialTypes addSubscription:s];
    }
    
    //Titles
    list = [SalesPlusCustTitles getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustTitles removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustTitles getSubscriptions];
    if (list.length == 0){
        SalesPlusCustTitlesSubscription *s = [SalesPlusCustTitlesSubscription getInstance];
        s.Language = language;
        [SalesPlusCustTitles addSubscription:s];
    }
    
    //KTAAR
    list = [SalesPlusCustKTAAR getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustKTAAR removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustKTAAR getSubscriptions];
    if (list.length == 0){
        SalesPlusCustKTAARSubscription *s = [SalesPlusCustKTAARSubscription getInstance];
        s.Language = language;
        //        s.VKORG = userMBO.VKORG;
        //        s.VTWEG = userMBO.VTWEG;
        //        s.SPART = userMBO.SPART;
        [SalesPlusCustKTAAR addSubscription:s];
    }
    
    //KTAER
    list = [SalesPlusCustKTAER getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustKTAER removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustKTAER getSubscriptions];
    if (list.length == 0){
        SalesPlusCustKTAERSubscription *s = [SalesPlusCustKTAERSubscription getInstance];
        s.Language = language;
        [SalesPlusCustKTAER addSubscription:s];
    }
    
    //PARLA
    list = [SalesPlusCustPARLA getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustPARLA removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustPARLA getSubscriptions];
    if (list.length == 0){
        SalesPlusCustPARLASubscription *s = [SalesPlusCustPARLASubscription getInstance];
        s.Language = language;
        [SalesPlusCustPARLA addSubscription:s];
    }
    
    //AUART
    list = [SalesPlusCustAUART getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustAUART removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustAUART getSubscriptions];
    if (list.length == 0){
        SalesPlusCustAUARTSubscription *s = [SalesPlusCustAUARTSubscription getInstance];
        s.Language = language;
        [SalesPlusCustAUART addSubscription:s];
    }
    
    //GEWEI
    list = [SalesPlusCustGEWEI getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustGEWEI removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustGEWEI getSubscriptions];
    if (list.length == 0){
        SalesPlusCustGEWEISubscription *s = [SalesPlusCustGEWEISubscription getInstance];
        s.Language = language;
        [SalesPlusCustGEWEI addSubscription:s];
    }
    
    //KONDA
    list = [SalesPlusCustKONDA getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustKONDA removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustKONDA getSubscriptions];
    if (list.length == 0){
        SalesPlusCustKONDASubscription *s = [SalesPlusCustKONDASubscription getInstance];
        s.Language = language;
        [SalesPlusCustKONDA addSubscription:s];
    }
    
    //KTAST
    list = [SalesPlusCustKTAST getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustKTAST removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustKTAST getSubscriptions];
    if (list.length == 0){
        SalesPlusCustKTASTSubscription *s = [SalesPlusCustKTASTSubscription getInstance];
        s.Language = language;
        [SalesPlusCustKTAST addSubscription:s];
    }
    
    //PAFKT
    list = [SalesPlusCustPAFKT getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustPAFKT removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustPAFKT getSubscriptions];
    if (list.length == 0){
        SalesPlusCustPAFKTSubscription *s = [SalesPlusCustPAFKTSubscription getInstance];
        s.Language = language;
        [SalesPlusCustPAFKT addSubscription:s];
    }
    
    //PARH1
    list = [SalesPlusCustPARH1 getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustPARH1 removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustPARH1 getSubscriptions];
    if (list.length == 0){
        SalesPlusCustPARH1Subscription *s = [SalesPlusCustPARH1Subscription getInstance];
        s.Language = language;
        [SalesPlusCustPARH1 addSubscription:s];
    }
    
    //PARH2
    list = [SalesPlusCustPARH2 getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustPARH2 removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustPARH2 getSubscriptions];
    if (list.length == 0){
        SalesPlusCustPARH2Subscription *s = [SalesPlusCustPARH2Subscription getInstance];
        s.Language = language;
        [SalesPlusCustPARH2 addSubscription:s];
    }
    
    //PARH3
    list = [SalesPlusCustPARH3 getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustPARH3 removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustPARH3 getSubscriptions];
    if (list.length == 0){
        SalesPlusCustPARH3Subscription *s = [SalesPlusCustPARH3Subscription getInstance];
        s.Language = language;
        [SalesPlusCustPARH3 addSubscription:s];
    }
    
    //Unit of Measures
    list = [SalesPlusCustUnitOfMeasures getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustUnitOfMeasures removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustUnitOfMeasures getSubscriptions];
    if (list.length == 0){
        SalesPlusCustUnitOfMeasuresSubscription *s = [SalesPlusCustUnitOfMeasuresSubscription getInstance];
        s.Language = language;
        [SalesPlusCustUnitOfMeasures addSubscription:s];
    }
    
    //VKBUR
    list = [SalesPlusCustVKBUR getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustVKBUR removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustVKBUR getSubscriptions];
    if (list.length == 0){
        SalesPlusCustVKBURSubscription *s = [SalesPlusCustVKBURSubscription getInstance];
        s.Language = language;
        [SalesPlusCustVKBUR addSubscription:s];
    }
    
    //ZTERM
    list = [SalesPlusCustZTERM getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustZTERM removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustZTERM getSubscriptions];
    if (list.length == 0){
        SalesPlusCustZTERMSubscription *s = [SalesPlusCustZTERMSubscription getInstance];
        s.Language = language;
        [SalesPlusCustZTERM addSubscription:s];
    }
    
    //Close Plant/WERK
    list = [SalesPlusCustUserClosePlant getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustUserClosePlant removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustUserClosePlant getSubscriptions];
    if (list.length == 0){
        SalesPlusCustUserClosePlantSubscription *s = [SalesPlusCustUserClosePlantSubscription getInstance];
        s.bname = userMBO.BNAME;
        [SalesPlusCustUserClosePlant addSubscription:s];
    }
    
    
    //BO Customer Report Language
    list = [SalesPlusBOLinkCustomerText getSubscriptions];
    if (list.length > 0){
        [SalesPlusBOLinkCustomerText removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusBOLinkCustomerText getSubscriptions];
    if (list.length == 0){
        SalesPlusBOLinkCustomerTextSubscription *s = [SalesPlusBOLinkCustomerTextSubscription getInstance];
        s.Language = language;
        [SalesPlusBOLinkCustomerText addSubscription:s];
    }
    
    //BO User Reports List based on user
    list = [SalesPlusBOLinkUser getSubscriptions];
    if (list.length > 0){
        [SalesPlusBOLinkUser removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusBOLinkUser getSubscriptions];
    if (list.length == 0){
        SalesPlusBOLinkUserSubscription *s = [SalesPlusBOLinkUserSubscription getInstance];
        s.bname = userMBO.BNAME;
        [SalesPlusBOLinkUser addSubscription:s];
    }
    
    //BO User Report Language
    list = [SalesPlusBOLinkUserText getSubscriptions];
    if (list.length > 0){
        [SalesPlusBOLinkUserText removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusBOLinkUserText getSubscriptions];
    if (list.length == 0){
        SalesPlusBOLinkUserTextSubscription *s = [SalesPlusBOLinkUserTextSubscription getInstance];
        s.Language = language;
        [SalesPlusBOLinkUserText addSubscription:s];
    }
    
    //BO Report UserID
    //        list = [SalesPlusBOLinkUserRel getSubscriptions];
    //        if (list.length > 0){
    //            [SalesPlusBOLinkUserRel removeSubscription:[list objectAtIndex:0]];
    //        }
    //        list = [SalesPlusBOLinkUserRel getSubscriptions];
    //        if (list.length == 0){
    //            SalesPlusBOLinkUserRelSubscription *s = [SalesPlusBOLinkUserRelSubscription getInstance];
    //            s.bname = userMBO.BNAME;
    //            [SalesPlusBOLinkUserRel addSubscription:s];
    //        }
    
    //Report Text language
    list = [SalesPlusReportingText getSubscriptions];
    if (list.length > 0){
        [SalesPlusReportingText removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusReportingText getSubscriptions];
    if (list.length == 0){
        SalesPlusReportingTextSubscription *s = [SalesPlusReportingTextSubscription getInstance];
        s.Language = language;
        [SalesPlusReportingText addSubscription:s];
    }
    
    //Report UserID
    list = [SalesPlusReportingMasterUser getSubscriptions];
    if (list.length > 0){
        [SalesPlusReportingMasterUser removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusReportingMasterUser getSubscriptions];
    if (list.length == 0){
        SalesPlusReportingMasterUserSubscription *s = [SalesPlusReportingMasterUserSubscription getInstance];
        s.UserID = userMBO.BNAME;
        [SalesPlusReportingMasterUser addSubscription:s];
    }
    
    //CustomerReturn language
    list = [SalesPlusCustomerReturn getSubscriptions];
    if (list.length > 0){
        [SalesPlusCustomerReturn removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCustomerReturn getSubscriptions];
    if (list.length == 0){
        SalesPlusCustomerReturnSubscription *s = [SalesPlusCustomerReturnSubscription getInstance];
        s.Language = language;
        [SalesPlusCustomerReturn addSubscription:s];
    }
    
    //ContactReturn language
    list = [SalesPlusContactReturn getSubscriptions];
    if (list.length > 0){
        [SalesPlusContactReturn removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusContactReturn getSubscriptions];
    if (list.length == 0){
        SalesPlusContactReturnSubscription *s = [SalesPlusContactReturnSubscription getInstance];
        s.Language = language;
        [SalesPlusContactReturn addSubscription:s];
    }
    
    //SalesActivityReturn language
    list = [SalesPlusSalesActivityReturn getSubscriptions];
    if (list.length > 0){
        [SalesPlusSalesActivityReturn removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusSalesActivityReturn getSubscriptions];
    if (list.length == 0){
        SalesPlusSalesActivityReturnSubscription *s = [SalesPlusSalesActivityReturnSubscription getInstance];
        s.Language = language;
        [SalesPlusSalesActivityReturn addSubscription:s];
    }
    
    //SalesOrderReturn language
    list = [SalesPlusSalesDocumentReturn getSubscriptions];
    if (list.length > 0){
        [SalesPlusSalesDocumentReturn removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusSalesDocumentReturn getSubscriptions];
    if (list.length == 0){
        SalesPlusSalesDocumentReturnSubscription *s = [SalesPlusSalesDocumentReturnSubscription getInstance];
        s.Language = language;
        [SalesPlusSalesDocumentReturn addSubscription:s];
    }
    
    //CRUDSyncLog language
    list = [SalesPlusCRUDSyncLog getSubscriptions];
    if (list.length > 0){
        [SalesPlusCRUDSyncLog removeSubscription:[list objectAtIndex:0]];
    }
    list = [SalesPlusCRUDSyncLog getSubscriptions];
    if (list.length == 0){
        SalesPlusCRUDSyncLogSubscription *s = [SalesPlusCRUDSyncLogSubscription getInstance];
        s.Language = language;
        [SalesPlusCRUDSyncLog addSubscription:s];
    }
    
}

@end

