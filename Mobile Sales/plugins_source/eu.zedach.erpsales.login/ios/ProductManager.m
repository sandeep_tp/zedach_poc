//
//  ProductManager.m
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-06-26.
//
//

#import "ProductManager.h"
#import "SalesPlusProduct.h"
#import "SalesPlusProductText.h"
#import "SalesPlusProductCategory.h"
#import "SalesPlusProductCategoryRelationship.h"
#import "SUPAbstractLocalEntity+ERPSales.h"
#import "SalesPlusERPSalesPlus_SuperUserDB.h"
#import "SUPQuery.h"
#import "SUPQueryResultSet.h"
#import "SUPDataValue.h"
#import "SUPDataValueList.h"
#import "SUPAttributeTest.h"
#import "SalesPlusProductConditionItem.h"
#import "SalesPlusProductSalesRel.h"
#import "SalesPlusProductAttachments.h"
#import "SalesPlusProductUnits.h"
#import "SUPCompositeTest.h"
#import "MaterialPicture.h"
#import "CustomizingManager.h"
#import "UserManager.h"

@implementation ProductManager

@synthesize productCategories = _productCategories;
@synthesize productHighlightList = _productHighlightList;
@synthesize productKeys = _productKeys;
@synthesize productLightList = _productLightList;
@synthesize productCountForCategory = _productCountForCategory;

+ (id) defaultManager{
    static ProductManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ProductManager alloc]initWithTableName:@"Product"];
        manager.productKeys = [NSArray arrayWithObjects:
                               @"MATNR",        // Product No.
                               @"GROSS_WT",     // Gross Weight
                               @"UNIT_OF_WT",   // Weight Unit
                               @"BASE_UOM",     // Base Unit of Measure
                               //                               @"MATL_TYPE",
                               //                               @"MATL_GROUP",
                               //
                               //                               @"SIZE_DIM",
                               //
                               //                               @"NET_WEIGHT",
                               //
                               //                               @"WIDTH",
                               //                               @"HEIGHT",
                               //                               @"UNIT_DIM",
                               //                               @"PRICE_UNIT",
                               //                               @"STD_PRICE",
                               nil];
        manager.productCountForCategory = [NSMutableDictionary dictionary];
        
    });
    return manager;
}

// Returns a product list containing a limited amount of fields for list displays.
-(NSArray *)getProductListFrom:(int)skip To:(int)take{
    NSArray *salesRelKeys = [NSArray arrayWithObjects:
                             @"VRKME",      // Sale Unit
                             @"STOCK",      // Stock
                             @"ALT_UNIT",   // Stock Unit
                             @"WERKS",      // Werks
                             nil];
    // For perfomance reason only query the first 100 entries needs to be adjusted incase of scrolling
    SUPObjectList *resultList = [SalesPlusProduct findWithQuery:[self selectAllQuery:self.productKeys from:skip to:take orderBy:@"MATNR" ascending:YES]];
    // get the current logged in language
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString* preferredLang = [languages objectAtIndex:0];
    NSString *language = [[preferredLang capitalizedString] substringToIndex:1];
    if (resultList == nil){
        NSLog(@"query failed!!");
        //productLightList = [NSDictionary dictionary];
        _productLightList = [NSArray array];
    }else{
        NSMutableArray *productList = [NSMutableArray arrayWithCapacity:resultList.length];
        for(SalesPlusProduct *p in resultList){
            NSMutableDictionary *temp = [p toDictionaryWithKeys:self.productKeys];
            // reading product group text
//            NSArray *productGroupText = [[CustomizingManager defaultManager] materialGroupList];
//            for (int i=0; i < productGroupText.count; i++){
//                NSDictionary *pgt = [productGroupText objectAtIndex:i];
//                if ([[pgt objectForKey:@"MATKL"] isEqualToString:p.MATL_GROUP]){
//                    [temp setObject:[pgt objectForKey:@"WGBEZ"] forKey:@"MATL_GROUP_TXT"];
//                    break;
//                }
//            }
            //      reading product text
            SUPObjectList *textList =[SalesPlusProductText findByMatnrSpras:p.MATNR withSPRAS:language];
            NSMutableString *maktx = [NSMutableString string];
            NSMutableString *maktx2 = [NSMutableString string];
            for (SalesPlusProductText *t in textList) {
                [maktx appendString:t.MAKTX];
                [maktx2 appendString:t.MAKTX2];
            }
            [temp setObject:maktx forKey:@"MAKTX"];
            [temp setObject:maktx2 forKey:@"MAKTX2"];
            //    reading product sales rel
            NSMutableArray *salesRelResult = [NSMutableArray arrayWithCapacity:p.productSalesRels.length];
            SUPObjectList *productSalesRelItems = p.productSalesRels;
            for (SalesPlusProductSalesRel *sr in productSalesRelItems) {
                [salesRelResult addObject:[sr toDictionaryWithKeys:salesRelKeys]];
            }
            [temp setObject:salesRelResult forKey:@"SalesRels"];

            // reading product image
            NSRange range = [p.MATNR rangeOfString:@"^0*" options:NSRegularExpressionSearch];
            NSString *compressedMATNR = [p.MATNR stringByReplacingCharactersInRange:range withString:@""];
            MaterialPicture *mp = [MaterialPicture loadMaterialPictureObjectWithKey:compressedMATNR];
            NSString *imageContentString = @"";
            if (mp){
                //picture exists
                //                    UIImage *productPic = [UIImage imageWithContentsOfFile:mp.path];
                //                    NSData *imageData = UIImagePNGRepresentation(productPic);
                //                    imageContentString = [self base64forData:imageData];
                imageContentString = mp.path;
            }
            [temp setObject:imageContentString forKey:@"IMAGE"];
            [productList addObject:temp];
        }
        _productLightList = [NSArray arrayWithArray:productList];
        //productLightList = [NSDictionary dictionaryWithDictionary:productList];
    }
    return _productLightList;
}

// Returns a product highlight list with its id and description
- (NSArray *)productHighlightList{
    if (!_productHighlightList){
        SUPQuery *query = [SUPQuery getInstance];
        [query select:@"ph.MATNR"];
        [query from:@"ProductHighlight":@"ph"];
        [query join:@"Product":@"p":@"p.MATNR":@"ph.MATNR"];
        SUPQueryResultSet* resultSet = [SalesPlusERPSalesPlus_SuperUserDB executeQuery:query];
        if(resultSet == nil)
        {
            NSLog(@"query failed!!");
            _productHighlightList = [NSArray array];
        }else{
            NSMutableArray *phList = [NSMutableArray arrayWithCapacity:resultSet.length];
            for(SUPDataValueList* result in resultSet)
            {
                NSMutableDictionary *ph = [[NSMutableDictionary alloc] initWithCapacity:2];
                [ph setObject:[SUPDataValue  getNullableString:[result item:0]] forKey:@"MATNR"];
                //[ph setObject:[SUPDataValue  getNullableString:[result item:1]] forKey:@"MATL_DESC"];
                // reading product image
                NSRange range = [[SUPDataValue  getNullableString:[result item:0]] rangeOfString:@"^0*" options:NSRegularExpressionSearch];
                NSString *compressedMATNR = [[SUPDataValue  getNullableString:[result item:0]] stringByReplacingCharactersInRange:range withString:@""];
                MaterialPicture *mp = [MaterialPicture loadMaterialPictureObjectWithKey:compressedMATNR];
                NSString *imageContentString = @"";
                if (mp){
                    imageContentString = mp.path;
                }
                [ph setObject:imageContentString forKey:@"IMAGE_PATH"];
                [phList addObject:ph];
            }
            _productHighlightList = [NSArray arrayWithArray:phList];
        }
    }
    return _productHighlightList;
}

-(NSString *)isProductLocal:(NSString *)MATNR{
    SalesPlusProduct *product = [SalesPlusProduct findByPrimaryKey:MATNR];
    if (product){
        return @"X";
    }else{
        return @"";
    }
}

// returns a dictionary containing all product fields for a given material number (MATNR).
- (NSDictionary *)productDetailsForMATNR:(NSString *)MATNR{
    
    NSArray *conditionKeys = [NSArray arrayWithObjects:
                              @"VKORG",
                              @"VTWEG",
                              @"KBETR",     // Gross Price
                              @"KPEIN",     // Unit of Price
                              @"KMEIN",     // Unit
                              //                              @"KNUMH",
                              //                              @"KOPOS",
                              //                              @"MATNR",
                              //                              @"KONWA",
                              nil];
    NSArray *salesRelKeys = [NSArray arrayWithObjects:
                             @"VRKME",      // Sale Unit
                             @"STOCK",      // Stock
                             @"ALT_UNIT",   // Stock Unit
                             @"WERKS",      // Werks
                             //                             @"MATNR",
                             //                             @"VKORG",
                             //                             @"VTWEG",
                             //                             @"VMSTA",
                             //                             @"MTPOS",
                             //                             @"DWERK",
                             nil];
    NSArray *documentKeys = [NSArray arrayWithObjects:
                             @"DESCRIPTION",// Description
                             @"LINK",
                             @"SPARS",
                             nil];
    NSArray *unitKeys = [NSArray arrayWithObjects:
                         @"MATNR",
                         @"ALT_UNIT",   // Unit
                         @"GROSS_WT",   // Gross Weight
                         @"UNIT_OF_WT", // Weight Unit
                         @"FACTOR",     // Factor
                         nil];
    SalesPlusProduct *product = [SalesPlusProduct findByPrimaryKey:MATNR];
    NSMutableArray *conditionResult = nil;
    NSMutableArray *salesRelResult = nil;
    NSMutableArray *documentsResult = nil;
    NSMutableArray *unitsResult = nil;
    
    // get the current logged in language
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString* preferredLang = [languages objectAtIndex:0];
    NSString *language = [[preferredLang capitalizedString] substringToIndex:1];
    
    UserManager *um = [UserManager defaultManager];
    NSString *currentVKORG = [[um currentUserDetails] objectForKey:@"VKORG"];
    if (product){
        NSMutableDictionary *productDetails = [product toDictionaryWithKeys:self.productKeys];
        // reading product group text
//        NSArray *productGroupText = [[CustomizingManager defaultManager] materialGroupList];
//        for (int i=0; i < productGroupText.count; i++){
//            NSDictionary *pgt = [productGroupText objectAtIndex:i];
//            if ([[pgt objectForKey:@"MATKL"] isEqualToString:product.MATL_GROUP]){
//                [productDetails setObject:[pgt objectForKey:@"WGBEZ"] forKey:@"MATL_GROUP_TXT"];
//                break;
//            }
//        }
        //      reading product image
        NSRange range = [product.MATNR rangeOfString:@"^0*" options:NSRegularExpressionSearch];
        NSString *compressedMATNR = [product.MATNR stringByReplacingCharactersInRange:range withString:@""];
        MaterialPicture *mp = [MaterialPicture loadMaterialPictureObjectWithKey:compressedMATNR];
        NSString *imageContentString = @"";
        if (mp){
            //picture exists
            //            UIImage *productPic = [UIImage imageWithContentsOfFile:mp.path];
            //            NSData *imageData = UIImagePNGRepresentation(productPic);
            //            imageContentString = [self base64forData:imageData];
            imageContentString = mp.path;
        }
        [productDetails setObject:imageContentString forKey:@"IMAGE"];
        //      reading product text
        //SUPObjectList *textList =product.productTexts;
        SUPObjectList *textList =[SalesPlusProductText findByMatnrSpras:product.MATNR withSPRAS:language];
        NSMutableString *maktx = [NSMutableString string];
        NSMutableString *maktx2 = [NSMutableString string];
        for (SalesPlusProductText *t in textList) {
            [maktx appendString:t.MAKTX];
            [maktx2 appendString:t.MAKTX2];
        }
        [productDetails setObject:maktx forKey:@"MAKTX"];
        [productDetails setObject:maktx2 forKey:@"MAKTX2"];
        
        //      reading product condition items
        conditionResult = [NSMutableArray array];
        //SUPObjectList *productConditionItems = product.productConditionItems;
        SUPObjectList *productConditionItems = [SalesPlusProductConditionItem findByMatnrVkorg:product.MATNR withVKORG:currentVKORG];
        for (SalesPlusProductConditionItem *ci in productConditionItems) {
            [conditionResult addObject:[ci toDictionaryWithKeys:conditionKeys]];
        }
        [productDetails setObject:conditionResult forKey:@"ConditionItems"];
        //      reading product sales rel
        salesRelResult = [NSMutableArray arrayWithCapacity:product.productSalesRels.length];
        //SUPObjectList *productSalesRelItems = product.productSalesRels;
        SUPObjectList *productSalesRelItems = [SalesPlusProductSalesRel findByMATNR:product.MATNR];
        for (SalesPlusProductSalesRel *sr in productSalesRelItems) {
            [salesRelResult addObject:[sr toDictionaryWithKeys:salesRelKeys]];
        }
        [productDetails setObject:salesRelResult forKey:@"SalesRels"];
        //      reading product documents
        documentsResult = [NSMutableArray array];
        //SUPObjectList *docuementItems = product.productAttachmentss;
        SUPObjectList *docuementItems = [SalesPlusProductAttachments findByMatnrSpras:product.MATNR withSPRAS:language];
        for (SalesPlusProductAttachments *pa in docuementItems) {
            NSMutableDictionary *temp = [pa toDictionaryWithKeys:documentKeys];
            [documentsResult addObject:temp];
        }
        [productDetails setObject:documentsResult forKey:@"Documents"];
        //      reading product units
        unitsResult = [NSMutableArray array];
        SUPObjectList *unitItems = [SalesPlusProductUnits findByMatnrSorted:product.MATNR];
        for (SalesPlusProductUnits *pu in unitItems) {
            [unitsResult addObject:[pu toDictionaryWithKeys:unitKeys]];
        }
        [productDetails setObject:unitsResult forKey:@"Units"];
        return productDetails;
    }
    return [NSDictionary dictionary];
    
}

- (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

// returns an array of NSDictionary with product categories and hierarchy.
// array formatted as follows:
// [{CATALOGUENAME="",CATEGORYID="",PARENTCATEGORYID="",CATEGORYDESCRIPTION="",subCategories=[...]}]
// the subCategories can be multiple levels.
//- (NSArray *)productCategories{
//    if (!_productCategories){
//        NSArray *keys = [NSArray arrayWithObjects:
//                         @"CATALOGUENAME",
//                         @"CATEGORYID",
//                         @"PARENTCATEGORYID",
//                         @"CATEGORYDESCRIPTION",
//                         nil];
//
//        SUPObjectList *resultList = [SalesPlusProductCategory findAll];
//
//        if (resultList == nil){
//            NSLog(@"query failed!!");
//        }else{
//            NSMutableArray *mboList = [resultList array];
//
//            NSMutableArray *categoryList = [NSMutableArray array];
//
//            NSArray *topLevel = [mboList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"PARENTCATEGORYID == %@",@"0000000000"]];
//
//            NSArray *sortedCatalogues = [topLevel sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"CATALOGUENAME" ascending:YES]]];
//
//            // Create a distinct list of catalogues
//            NSString *name = nil;
//
//            for (SalesPlusProductCategory *c in sortedCatalogues) {
//                if ([c.CATALOGUENAME isEqualToString:name] == NO){
//                    name = c.CATALOGUENAME;
//                    [categoryList addObject: [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                              c.CATALOGUENAME,      @"CATALOGUENAME",
//                                              c.CATEGORYDESCRIPTION,@"CATEGORYDESCRIPTION",nil]];
//                }
//            }
//
//
//            for (SalesPlusProductCategory *category in sortedCatalogues) {
//                NSMutableDictionary *categoryDictionary = category.toDictionary;
//                [self addSubCategoriesForCategory:categoryDictionary fromMBOList:mboList];
//                [categoryList addObject:categoryDictionary];
//            }
//            _productCategories = [NSArray arrayWithArray:categoryList];
//        }
//    }
//    return _productCategories;
//}
//
//// Adds one level of sub categories for a given product category dictionary.
//// This method is called recursively in order to fill all levels of sub categories.
//-(void)addSubCategoriesForCategory:(NSMutableDictionary *)category fromMBOList:(NSArray*)mboList{
//    NSString *name  = [category valueForKey:@"CATALOGUENAME"];
//    NSString *catId = [category valueForKey:@"CATEGORYID"];
//
//    NSArray *categories = [mboList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"CATALOGUENAME == %@ AND PARENTCATEGORYID == %@",name,catId]];
//
//    if (categories.count > 0){
//        NSMutableArray *subCategoryList = [NSMutableArray arrayWithCapacity:categories.count];
//
//        for (SalesPlusProductCategory *subCategory in categories) {
//            NSDictionary *subCategoryDictionary = subCategory.toDictionary;
//            [self addSubCategoriesForCategory:subCategoryDictionary fromMBOList:mboList];
//            [subCategoryList addObject:subCategoryDictionary];
//        }
//        [category setObject:subCategoryList forKey:@"subCategories"];
//    }
//}

//This method returns the top level product categories and call the getSubCategoriesForCategoryName method to find out if there are any children for this category
- (NSMutableArray *)getTopLevelCategories{
    NSMutableArray *categoryList = [NSMutableArray array];
    SUPObjectList *resultList = [SalesPlusProductCategory findSortedAll];
    if (resultList == nil){
        NSLog(@"query failed!!");
    }else{
        NSMutableArray *mboList = [resultList array];
        
        NSArray *topLevel = [mboList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"PARENTCATEGORYID == %@ && CATEGORYDESCRIPTION != %@",@"0000000000", @"Root"]];  //REMOVE ROOT AFTER
        
        NSArray *sortedCatalogues = [topLevel sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"CATEGORYID" ascending:YES]]];
        
        // Create a distinct list of catalogues
        NSString *name = nil;
        
        for (SalesPlusProductCategory *c in sortedCatalogues) {
            if ([c.CATALOGUENAME isEqualToString:name] == NO){ //REMOVE ROOT AFTER
                int hasChildren = 0;
                int hasProduct = [self.productCountForCategory objectForKey:@"0000000000"] ? [[self.productCountForCategory objectForKey:@"0000000000"] integerValue] : -1;
                name = c.CATALOGUENAME;
                NSArray *children = [self getSubCategoriesForCategoryName:name withId:@"0000000000" getChildren:NO];
                if (children.count > 0) {
                    hasChildren = 1;
                }
                [categoryList addObject: [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          c.CATALOGUENAME,      @"CATALOGUENAME",
                                          c.CATEGORYDESCRIPTION,@"CATEGORYDESCRIPTION",
                                          @"0000000000", @"CATEGORYID",
                                          [NSString stringWithFormat:@"%d",hasChildren] , @"HASCHILDREN",
                                          [NSString stringWithFormat:@"%d",hasProduct] , @"HASPRODUCT",
                                          nil]];
            }
        }
    }
    //int counter = [self getProductForCategoryName:@"MANUS" withId:@"0010202000"];
    return categoryList;
}

-(int)getProductCount:(NSString *)categoryID {
    NSArray *productList = [self getProductListForCategoryId:categoryID withDetails:NO];
    if (productList.count > 0){
        // found a product
        return productList.count;
    }else{
        return 0;
    }
}

// This method used to update the product count for each category and called recursively and the value is saved in the productCountForCategory dictionary
- (int)getProductForCategoryName:(NSString *)parentCatName withId:(NSString *)parentCatId{
    int totalProductCount = 0;
    int productCount = 0;
    // get all the subcategories for the parent product
    SUPObjectList *resultList = [SalesPlusProductCategory findByCatalogueNameAndParentCategoryId:parentCatName withParentCatId:parentCatId];
    if (resultList.length > 0){
        // loop through each child
        for(SalesPlusProductCategory *pc in resultList){
            productCount = 0;
            if ([pc.CATEGORYDESCRIPTION isEqualToString:@"Root"] == NO){
                NSMutableArray *children = [self getSubCategoriesForCategoryName:pc.CATALOGUENAME withId:pc.CATEGORYID getChildren:YES];
                if (children.count > 0) {
                    for (int i = 0; i < children.count; i++){
                        NSDictionary *child = [children objectAtIndex:i];
                        int val = [self getProductForCategoryName:[child objectForKey:@"CATALOGUENAME"] withId:[child objectForKey:@"CATEGORYID"]];
                        productCount += val;
                        totalProductCount += val;
                    }
                }else{
                    // child has no more sub categories
                    int tempCount = [self getProductCount:pc.CATEGORYID];
                    productCount += tempCount;
                    totalProductCount += tempCount;
                }
            }
            // save the product count for each child
            [self.productCountForCategory setObject:[NSString stringWithFormat:@"%d",productCount] forKey:pc.CATEGORYID];
        }
    }else{
        // get the product list
        totalProductCount += [self getProductCount:parentCatId];
    }
    // save the product count for parent category
    [self.productCountForCategory setObject:[NSString stringWithFormat:@"%d",totalProductCount] forKey:parentCatId];
    return totalProductCount;
}

// method updates the amount of products within each product category in the tree structure
-(void)updateProductCountForCategories{
    NSLog(@"updateProductCountForCategories Started");
    NSArray *categoryList = [self getTopLevelCategories];
    if (categoryList.count == 1){
        //there is only one top level category drill to its sub categories right away
        NSDictionary *values = [categoryList objectAtIndex:0];
        NSString *categoryName = [values objectForKey:@"CATALOGUENAME"];
        NSString *categoryId = [values objectForKey:@"CATEGORYID"];
        categoryList = [self getSubCategoriesForCategoryName:categoryName withId:categoryId getChildren:YES];
    }
    for (int i=0; i < categoryList.count; i++) {
        NSDictionary *c = [categoryList objectAtIndex:i];
        if ([[c objectForKey:@"CATEGORYDESCRIPTION"] isEqualToString:@"Root"] == NO){
            int counter =[self getProductForCategoryName:[c objectForKey:@"CATALOGUENAME"] withId:[c objectForKey:@"CATEGORYID"]];
            [c setValue:[NSString stringWithFormat:@"%d",counter] forKey:@"HASPRODUCT"];
        }
    }
    NSLog(@"updateProductCountForCategories Finished");
}

// method used to find out if a given category name and id has any subchildren, calls itself to find out if this category has any children
- (NSMutableArray *)getSubCategoriesForCategoryName:(NSString *)parentCatName withId:(NSString *)parentcatId getChildren:(bool)readChildren{
    NSArray *keys = [NSArray arrayWithObjects:
                     @"CATALOGUENAME",
                     @"CATEGORYID",
                     @"PARENTCATEGORYID",
                     @"CATEGORYDESCRIPTION",
                     @"HASCHILDREN",
                     @"HASPRODUCT",
                     nil];
    NSMutableArray *categoryList = [NSMutableArray array];
    //_productCategoriesDict = [NSMutableDictionary dictionary];
    
    SUPObjectList *resultList = [SalesPlusProductCategory findByCatalogueNameAndParentCategoryId:parentCatName withParentCatId:parentcatId];
    NSMutableArray *children;
    for(SalesPlusProductCategory *pc in resultList){
        NSMutableDictionary *categoryDictionary = [pc toDictionaryWithKeys:keys];
        
        int hasChildren = 0;
        int hasProduct = 0;
        if (readChildren){
            
            // calles it self and set readChildren to NO avoids calling it self recursively
            children = [self getSubCategoriesForCategoryName:pc.CATALOGUENAME withId:pc.CATEGORYID getChildren:NO];
            if (children.count > 0) {
                hasChildren = 1;
            }
            //            if (topLevelCalled){
            //                if (hasChildren == 1){
            //                    // if the category has other sub categories navigate the categories to find out if there is a product
            //                    hasProduct = [self getProductForCategoryName:pc.CATALOGUENAME withId:pc.CATEGORYID getProduct:YES];
            //                }else{
            //                    hasProduct = [self getProductCount:pc.CATEGORYID];
            //                }
            //            }
            hasProduct = [self.productCountForCategory objectForKey:pc.CATEGORYID] ? [[self.productCountForCategory objectForKey:pc.CATEGORYID] integerValue] : -1;
            [categoryDictionary setObject:[NSString stringWithFormat:@"%d",hasProduct] forKey:@"HASPRODUCT"];
            [categoryDictionary setObject:[NSString stringWithFormat:@"%d",hasChildren] forKey:@"HASCHILDREN"];
        }else{
            hasProduct = [self getProductCount:pc.CATEGORYID];
            [categoryDictionary setObject:[NSString stringWithFormat:@"%d",hasProduct] forKey:@"HASPRODUCT"];
            [categoryDictionary setObject:[NSString stringWithFormat:@"%d",0] forKey:@"HASCHILDREN"];
            
        }
        [categoryList addObject:categoryDictionary];
    }
    return categoryList;
}

// returns a list of products that matches the category
- (NSArray *)getProductListForCategoryId:(NSString *)catId withDetails:(bool)flag{
    //    Currently the product is read from the internal buffered dictionary, or you can read the product from SUP MBO
    //    NSArray *keys = [NSArray arrayWithObjects:
    //                     @"MATNR",      // Product No.
    //                     @"MATL_DESC",  // Product Description
    //                     nil];
    NSMutableArray *productList = [NSMutableArray array];
    SUPObjectList *resultList = [SalesPlusProductCategoryRelationship findByCATEGORYID:catId];
    //SUPObjectList *resultList = [SalesPlusProductCategoryRelationship findByCATEGORYID:@"0000000010"];
    if (resultList == nil){
        NSLog(@"query failed!!");
    }else{
        for(SalesPlusProductCategoryRelationship *pcr in resultList){
            NSDictionary *p;
            if (flag)
                p = [[ProductManager defaultManager] productDetailsForMATNR:pcr.MATNR];
            else{
                SalesPlusProduct *product = [SalesPlusProduct findByPrimaryKey:pcr.MATNR];
                p = [product toDictionaryWithKeys:self.productKeys];
            }
            [productList addObject:p];
            //[productList addObject:[productLightList objectForKey:pcr.MATNR]];
            //            SalesPlusProduct *p = [SalesPlusProduct findByPrimaryKey:pcr.MATNR];
            //            [productList addObject:[p toDictionaryWithKeys:keys]];
        }
    }
    return productList;
}

// Queries the database for the given value on multiple fields.
- (NSArray *)searchProductsWithCriteria:(NSString *)searchString{
    NSArray *salesRelKeys = [NSArray arrayWithObjects:
                             @"VRKME",      // Sale Unit
                             @"STOCK",      // Stock
                             @"ALT_UNIT",   // Stock Unit
                             @"WERKS",      // Werks
                             nil];

    NSArray *searchKeys = [NSArray arrayWithObjects:
                           @"p.MATNR",      // Product No.
                           //@"p.MATL_DESC",  // Product Description
                           @"pt.MAKTX",
                           @"pt.MAKTX2",
                           //@"p.ANZGEB",
                           //@"p.BASE_UOM",
                           //@"p.ANZPAK",
                           nil];
    // get the current logged in language
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString* preferredLang = [languages objectAtIndex:0];
    NSString *language = [[preferredLang capitalizedString] substringToIndex:1];
    
    SUPQuery *query = [SUPQuery getInstance];
    [query select:@"p.MATNR"];
    [query from:@"Product":@"p"];
    [query join:@"ProductText":@"pt":@"pt.MATNR":@"p.MATNR"];
    query.distinct = YES;
    //    [query join:@"Customer":@"c":@"c.KUNNR":@"sa.PARTN_ID"];
    //    [query join:@"CustKTAAR":@"cuKTAAR":@"cuKTAAR.KTAAR":@"sa.ACTIVITY_TYPE"];
    
    NSArray* searchStringTokens = [searchString componentsSeparatedByString: @" "];
    SUPCompositeTest *outerCompositeTest = [SUPCompositeTest getInstance];
    [outerCompositeTest setOperator:SUPCompositeTest_AND];
    for (int i = 0; i < searchStringTokens.count; i++){
        SUPCompositeTest *ct = [SUPCompositeTest getInstance];
        [ct setOperator:SUPCompositeTest_OR];
        for (NSString *key in searchKeys) {
            SUPAttributeTest *at = [SUPAttributeTest getInstance];
            at.attribute = key;
            at.testValue = [searchStringTokens objectAtIndex:i];
            at.operator = SUPAttributeTest_CONTAINS;
            [ct add:at];
        }
        [outerCompositeTest add:ct];
    }
    
    [query where:outerCompositeTest];
    SUPQueryResultSet* resultList = [SalesPlusERPSalesPlus_SuperUserDB executeQuery:query];
    
    if (resultList == nil){
        return [NSArray array];
    }else{
        NSMutableArray *productList = [NSMutableArray arrayWithCapacity:resultList.length];
        
        for(SUPDataValueList* result in resultList){
            SalesPlusProduct *p = [SalesPlusProduct findByPrimaryKey:[SUPDataValue  getNullableString:[result item:0]]];
            NSMutableDictionary *temp = [p toDictionaryWithKeys:self.productKeys];
            //reading product text
            SUPObjectList *textList =[SalesPlusProductText findByMatnrSpras:p.MATNR withSPRAS:language];
            NSMutableString *maktx = [NSMutableString string];
            NSMutableString *maktx2 = [NSMutableString string];
            for (SalesPlusProductText *t in textList) {
                [maktx appendString:t.MAKTX];
                [maktx2 appendString:t.MAKTX2];
            }
            [temp setObject:maktx forKey:@"MAKTX"];
            [temp setObject:maktx2 forKey:@"MAKTX2"];
            //    reading product sales rel
            NSMutableArray *salesRelResult = [NSMutableArray arrayWithCapacity:p.productSalesRels.length];
            SUPObjectList *productSalesRelItems = p.productSalesRels;
            for (SalesPlusProductSalesRel *sr in productSalesRelItems) {
                [salesRelResult addObject:[sr toDictionaryWithKeys:salesRelKeys]];
            }
            [temp setObject:salesRelResult forKey:@"SalesRels"];
            // reading product image
            MaterialPicture *mp = [MaterialPicture loadMaterialPictureObjectWithKey:p.MATNR];
            NSString *imageContentString = @"";
            if (mp){
                //picture exists
                UIImage *productPic = [UIImage imageWithContentsOfFile:mp.path];
                NSData *imageData = UIImagePNGRepresentation(productPic);
                imageContentString = [self base64forData:imageData];
            }
            [temp setObject:imageContentString forKey:@"IMAGE"];
            [productList addObject:temp];
        }
        
        return [NSArray arrayWithArray:productList];
    }
}

-(void)clearCache{
    [self.productCountForCategory removeAllObjects];
}
@end
