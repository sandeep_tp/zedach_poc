//
//  SyncLogManager.m
//  ERPSales
//
//  Created by Bozhidar Borisov on 2014-04-24.
//
//

#import "SyncLogManager.h"
#import "SalesPlusSyncLog.h"
#import "SalesPlusContactReturn.h"
#import "SalesPlusCustomerReturn.h"
#import "SalesPlusSalesActivityReturn.h"
#import "SalesPlusSalesDocumentReturn.h"
#import "SUPAbstractLocalEntity+ERPSales.h"
#import "SalesPlusCRUDSyncLog.h"
#import "SUPAttributeTest.h"

@implementation SyncLogManager
@synthesize syncLogKeys = _syncLogKeys;

+ (id) defaultManager{
    static SyncLogManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[SyncLogManager alloc]initWithTableName:@"SyncLog"];
        manager.syncLogKeys = [NSArray arrayWithObjects:
                               @"Date",
                               @"Time",
                               @"MessageType",
                               @"Message",
                               @"Entity",
                               nil];
        
    });
    return manager;
}

- (bool)isSyncLogExistForDate:(NSDate *)d andTime:(NSDate *)t {
    SUPQuery *syncLogQuery = [SUPQuery getInstance];
    syncLogQuery = [self selectAllQuery:self.syncLogKeys];
    syncLogQuery.testCriteria = [SUPAttributeTest equal:@"Date" :d];
    syncLogQuery.testCriteria = [SUPAttributeTest equal:@"Time" :t];
    SUPObjectList *resultList = [SalesPlusSyncLog findWithQuery:syncLogQuery];
    if (resultList.length == 0)
        return NO;
    else
        return YES;
}

-(void)deleteAllEntries {
    SUPObjectList *allEntriesList = [SalesPlusSyncLog findAll];
    for (SalesPlusSyncLog *SyncLogEntry in allEntriesList){
        [SyncLogEntry delete];
    }
}

-(void)saveSyncLogforQuot:(NSArray *)values {
    
    NSLocale* currentLocale = [NSLocale currentLocale];
    NSDate* sdate = [[NSDate date] descriptionWithLocale:currentLocale];
    
    SalesPlusSyncLog *sl = [[SalesPlusSyncLog alloc] init];
    sl.Message = values[1];
    sl.MessageType = values[0];
    sl.Date = [NSDate date];
    sl.Time = [NSDate date];
    sl.Entity = @"4";
    [sl create];
}

-(void)saveSyncLogs {
    SUPObjectList *resultList = [SalesPlusContactReturn findAll];
    for (SalesPlusContactReturn *cr in resultList){
        if (![self isSyncLogExistForDate:cr.DATE andTime:cr.TIME]){
            SalesPlusSyncLog *sl = [[SalesPlusSyncLog alloc] init];
            sl.Message = cr.MESSAGE;
            sl.MessageType = cr.TYPE;
            sl.Date = cr.DATE;
            sl.Time = cr.TIME;
            sl.Entity = cr.ENTITY;
            [sl create];
        }
    }
    resultList = [SalesPlusCustomerReturn findAll];
    for (SalesPlusCustomerReturn *cur in resultList){
        if (![self isSyncLogExistForDate:cur.DATE andTime:cur.TIME]){
            SalesPlusSyncLog *sl = [[SalesPlusSyncLog alloc] init];
            sl.Message = cur.MESSAGE;
            sl.MessageType = cur.TYPE;
            sl.Date = cur.DATE;
            sl.Time = cur.TIME;
            sl.Entity = cur.ENTITY;
            [sl create];
        }
    }
    resultList = [SalesPlusSalesActivityReturn findAll];
    for (SalesPlusSalesActivityReturn *sar in resultList){
        if (![self isSyncLogExistForDate:sar.DATE andTime:sar.TIME]){
            SalesPlusSyncLog *sl = [[SalesPlusSyncLog alloc] init];
            sl.Message = sar.MESSAGE;
            sl.MessageType = sar.TYPE;
            sl.Date = sar.DATE;
            sl.Time = sar.TIME;
            sl.Entity = sar.ENTITY;
            [sl create];
        }
    }
    resultList = [SalesPlusSalesDocumentReturn findAll];
    for (SalesPlusSalesDocumentReturn *sor in resultList){
        if (![self isSyncLogExistForDate:sor.DATE andTime:sor.TIME]){
            SalesPlusSyncLog *sl = [[SalesPlusSyncLog alloc] init];
            sl.Message = sor.MESSAGE;
            sl.MessageType = sor.TYPE;
            sl.Date = sor.DATE;
            sl.Time = sor.TIME;
            sl.Entity = sor.ENTITY;
            [sl create];
        }
    }
    resultList = [SalesPlusCRUDSyncLog findAll];
    for (SalesPlusCRUDSyncLog *crudsl in resultList) {
        SalesPlusSyncLog *sl = [[SalesPlusSyncLog alloc] init];
        sl.Message = crudsl.MESSAGE;
        sl.MessageType = crudsl.TYPE;
        sl.Date = crudsl.M_DATE;
        sl.Time = crudsl.M_TIME;
        sl.Entity = crudsl.ENTITY;
        [sl create];
    }
}

-(NSArray *)getSyncLog{
    SUPObjectList *resultList = [SalesPlusSyncLog findSortedAll];
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:resultList.length];
    for (SalesPlusSyncLog *sl in resultList) {
        [result addObject:[sl toDictionaryWithKeys:self.syncLogKeys]];
    }
    return result;
}

@end
