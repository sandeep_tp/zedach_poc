//
//  CustomizingManager.m
//  ERPSales
//
//  Created by YohanK on 2013-07-18.
//
//

#import "CustomizingManager.h"
#import "SUPAbstractLocalEntity+ERPSales.h"
#import "SalesPlusERPSalesPlus_SuperUserDB.h"
#import "SUPMobileBusinessObject.h"
#import "SalesPlusCustCountry.h"
#import "SalesPlusCustCurrencies.h"
#import "SalesPlusCustMaterialGroups.h"
#import "SalesPlusCustMaterialTypes.h"
#import "SalesPlusCustTitles.h"
#import "SalesPlusCustUnitOfMeasures.h"
#import "SalesPlusCustKTAAR.h"
#import "SalesPlusCustKTAER.h"
#import "SalesPlusCustPARLA.h"
#import "SalesPlusCustAUART.h"
#import "SalesPlusCustGEWEI.h"
#import "SalesPlusCustKONDA.h"
#import "SalesPlusCustKTAST.h"
#import "SalesPlusCustPAFKT.h"
#import "SalesPlusCustPARH1.h"
#import "SalesPlusCustPARH2.h"
#import "SalesPlusCustPARH3.h"
#import "SalesPlusCustVKBUR.h"
#import "SalesPlusCustZTERM.h"
#import "SalesPlusCustIndustry.h"
#import "SalesPlusCustStatusIndicator.h"
#import "SalesPlusCustCustomerGroup.h"
#import "SalesPlusCustVRKMEactive.h"
#import "SalesPlusCustUserClosePlant.h"
#import "SUPSortCriteria.h"
#import "SUPAttributeSort.h"

@implementation CustomizingManager
@synthesize countryList = _countryList;
@synthesize currencyList = _currencyList;
@synthesize materialGroupList = _materialGroupList;
@synthesize materialTypeList = _materialTypeList;
@synthesize storageLocationList = _storageLocationList;
@synthesize titleList = _titleList;
@synthesize unitOfMeasureList = _unitOfMeasureList;
@synthesize ktaar = _ktaar;
@synthesize ktaer = _ktaer;
@synthesize parla = _parla;
@synthesize auart = _auart;
@synthesize gewei = _gewei;
@synthesize konda = _konda;
@synthesize ktast = _ktast;
@synthesize pafkt = _pafkt;
@synthesize parh1 = _parh1;
@synthesize parh2 = _parh2;
@synthesize parh3 = _parh3;
@synthesize vkbur = _vkbur;
@synthesize zterm = _zterm;
@synthesize csimt = _csimt;
@synthesize brsch = _brsch;
@synthesize kdgrp = _kdgrp;
@synthesize vrkmeActive = _vrkmeActive;
@synthesize userClosePlant = _userClosePlant;

+ (id) defaultManager{
    static CustomizingManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[CustomizingManager alloc]initWithTableName:@"CustCountry"];
        //[manager loadAllCustoming];
    });
    return manager;
}

- (void)loadAllCustoming
{
    [self countryList];
    [self currencyList];
    [self materialGroupList];
    [self materialTypeList];
    [self storageLocationList];
    [self titleList];
    [self unitOfMeasureList];
    [self ktaar];
    [self ktaer];
    [self parla];
    [self auart];
    [self gewei];
    [self konda];
    [self ktast];
    [self pafkt];
    [self parh1];
    [self parh2];
    [self parh3];
    [self vkbur];
    [self zterm];
    [self csimt];
    [self brsch];
    [self kdgrp];
    [self vrkmeActive];
    [self userClosePlant];
}

-(void) clearCache{
    self.countryList = nil;
    self.currencyList = nil;
    self.materialGroupList = nil;
    self.materialTypeList = nil;
    self.storageLocationList = nil;
    self.titleList = nil;
    self.unitOfMeasureList = nil;
    self.ktaar = nil;
    self.ktaer = nil;
    self.parla = nil;
    self.auart = nil;
    self.gewei = nil;
    self.konda = nil;
    self.ktast = nil;
    self.pafkt = nil;
    self.parh1 = nil;
    self.parh2 = nil;
    self.parh3 = nil;
    self.vkbur = nil;
    self.zterm = nil;
    self.csimt = nil;
    self.brsch = nil;
    self.kdgrp = nil;
    self.vrkmeActive = nil;
    self.userClosePlant = nil;
}

// Returns a country list
- (NSArray *)countryList{
    if (!_countryList){
//        SUPQuery *query = [SUPQuery getInstance];
//        [query select:@"cc.LAND1,cc.LANDX"];
//        [query from:@"CustCountry":@"cc"];
//        SUPQueryResultSet* resultSet = [SalesPlusERPSalesPlusDB executeQuery:query];
//        if(resultSet == nil)
//        {
//            NSLog(@"query failed!!");
//            _countryList = [NSArray array];
//        }else{
//            NSMutableArray *ccList = [NSMutableArray arrayWithCapacity:resultSet.length];
//            for(SUPDataValueList* result in resultSet)
//            {
//                NSMutableDictionary *cc = [[NSMutableDictionary alloc] initWithCapacity:2];
//                [cc setObject:[result item:0] forKey:@"LAND1"];
//                [cc setObject:[result item:0] forKey:@"LANDX"];
//                MBOLog(@"LAND1 - LANDX = %@ %@", [SUPDataValue  getNullableString:[result item:0]], [SUPDataValue getNullableString:[result item:1]]);
//                [ccList addObject:cc];
//            }
//            _countryList = [NSArray arrayWithArray:ccList];
//        }
        [self setDbTableName:@"CustCountry"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"LAND1",  // Country Key
                         @"LANDX",  // Country Description
                         nil];
        SUPQuery *query = [SUPQuery getInstance];
        [query select:@"cc.LAND1,cc.LANDX"];
        [query from:@"CustCountry":@"cc"];
        SUPSortCriteria *sort = [SUPSortCriteria getInstance];
        [sort add:[SUPAttributeSort ascending:@"LANDX"]];
        query.sortCriteria = sort;
        SUPObjectList* resultList = [SalesPlusCustCountry findWithQuery:query];
        //SUPObjectList *resultList = [SalesPlusCustCountry findWithQuery:[self selectAllQuery:keys]];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _countryList = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustCountry *x in resultList){
                [list addObject:[x toDictionaryWithKeys:keys]];
            }
            _countryList = [NSArray arrayWithArray:list];
        }
    }
    return _countryList;
}

// Returns a currency list
- (NSArray *)currencyList{
    if (!_currencyList){
        [self setDbTableName:@"CustCurrencies"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"SPRAS",  // Currency
                         @"WAERS",  // Currency Key
                         @"KTEXT",  // Currency Description
                         nil];
        
        
        SUPObjectList *resultList = [SalesPlusCustCurrencies findWithQuery:[self selectAllQuery:keys]];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _currencyList = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustCurrencies *x in resultList){
                [list addObject:[x toDictionaryWithKeys:keys]];
            }
            _currencyList = [NSArray arrayWithArray:list];
        }
    }
    return _currencyList;
}

// Returns material group list
- (NSArray *)materialGroupList{
    if (!_materialGroupList){
        [self setDbTableName:@"CustMaterialGroups"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"MATKL",  //
                         @"WGBEZ",  //
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustMaterialGroups findWithQuery:[self selectAllQuery:keys]];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _materialGroupList = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustMaterialGroups *x in resultList){
                [list addObject:[x toDictionaryWithKeys:keys]];
            }
            _materialGroupList = [NSArray arrayWithArray:list];
        }
    }
    return _materialGroupList;
}

// Returns material type list
- (NSArray *)materialTypeList{
    if (!_materialTypeList){
        [self setDbTableName:@"CustMaterialTypes"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"MTART",  //
                         @"MTBEZ",  //
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustMaterialTypes findWithQuery:[self selectAllQuery:keys]];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _materialGroupList = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustMaterialTypes *x in resultList){
                [list addObject:[x toDictionaryWithKeys:keys]];
            }
            _materialTypeList = [NSArray arrayWithArray:list];
        }
    }
    return _materialTypeList;
}

// Returns a title list
- (NSArray *)titleList{
    if (!_titleList){
        [self setDbTableName:@"CustTitles"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"TITLE",  // Currency Key
                         @"TITLE_MEDI",  // Currency Description
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustTitles findWithQuery:[self selectAllQuery:keys]];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _titleList = [NSArray array];
        }else{
            NSMutableArray *titlesList = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustTitles *t in resultList){
                [titlesList addObject:[t toDictionaryWithKeys:keys]];
            }
            _titleList = [NSArray arrayWithArray:titlesList];
        }
    }
    return _titleList;
}

// Returns unit of measure list
- (NSArray *)unitOfMeasureList{
    if (!_unitOfMeasureList){
        [self setDbTableName:@"CustUnitOfMeasures"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"MSEHI",  //
                         @"MSEHT",  // 
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustUnitOfMeasures findWithQuery:[self selectAllQuery:keys]];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _unitOfMeasureList = [NSArray array];
        }else{
            NSMutableArray *titlesList = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustUnitOfMeasures *t in resultList){
                [titlesList addObject:[t toDictionaryWithKeys:keys]];
            }
            _unitOfMeasureList = [NSArray arrayWithArray:titlesList];
        }
    }
    return _unitOfMeasureList;
}

// Returns KTAAR
- (NSArray *)ktaar{
    if (!_ktaar){
        [self setDbTableName:@"CustKTAAR"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"KTAAR",  //
                         @"VTEXT",  //
                         @"DEFAULT_SEL",
                         @"ORDER_LOCK",
                         @"VKORG",
                         @"VTWEG",
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustKTAAR findWithQuery:[self selectAllQuery:keys]];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _ktaar = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustKTAAR *t in resultList){
                [list addObject:[t toDictionaryWithKeys:keys]];
            }
            _ktaar = [NSArray arrayWithArray:list];
        }
    }
    return _ktaar;
}


// Returns KTAER
- (NSArray *)ktaer{
    if (!_ktaer){
        [self setDbTableName:@"CustKTAER"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"KTAER",  //
                         @"VTEXT",  //
                         @"STATUS", //
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustKTAER findWithQuery:[self selectAllQuery:keys]];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _ktaer = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustKTAER *t in resultList){
                [list addObject:[t toDictionaryWithKeys:keys]];
            }
            _ktaer = [NSArray arrayWithArray:list];
        }
    }
    return _ktaer;
}

// Returns PARLA
- (NSArray *)parla{
    if (!_parla){
        [self setDbTableName:@"CustPARLA"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"PARLA",  //
                         @"SPTXT",  //
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustPARLA findWithQuery:[self selectAllQuery:keys]];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _parla = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustPARLA *t in resultList){
                [list addObject:[t toDictionaryWithKeys:keys]];
            }
            _parla = [NSArray arrayWithArray:list];
        }
    }
    return _parla;
}

// Returns AUART (SalesDocumentType)
- (NSArray *)auart{
    if (!_auart){
        [self setDbTableName:@"CustAUART"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"AUART",  //
                         @"BEZEI",  //
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustAUART findWithQuery:[self selectAllQuery:keys]];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _auart = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustAUART *t in resultList){
                [list addObject:[t toDictionaryWithKeys:keys]];
            }
            _auart = [NSArray arrayWithArray:list];
        }
    }
    return _auart;
}

// Returns GEWEI (WeightUnit)
- (NSArray *)gewei{
    if (!_gewei){
        [self setDbTableName:@"CustGEWEI"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"MSEHI",  //
                         @"MSEHT",  //
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustGEWEI findWithQuery:[self selectAllQuery:keys]];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _gewei = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustGEWEI *t in resultList){
                [list addObject:[t toDictionaryWithKeys:keys]];
            }
            _gewei = [NSArray arrayWithArray:list];
        }
    }
    return _gewei;
}

// Returns KONDA
- (NSArray *)konda{
    if (!_konda){
        [self setDbTableName:@"CustKONDA"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"KONDA",  //
                         @"VTEXT",  //
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustKONDA findWithQuery:[self selectAllQuery:keys]];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _konda = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustKONDA *t in resultList){
                [list addObject:[t toDictionaryWithKeys:keys]];
            }
            _konda = [NSArray arrayWithArray:list];
        }
    }
    return _konda;
}

// Returns KTAST (SalesActivityStatus)
- (NSArray *)ktast{
    if (!_ktast){
        [self setDbTableName:@"CustKTAST"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"KTAST",  //
                         @"VTEXT",  //
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustKTAST findWithQuery:[self selectAllQuery:keys]];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _ktast = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustKTAST *t in resultList){
                [list addObject:[t toDictionaryWithKeys:keys]];
            }
            _ktast = [NSArray arrayWithArray:list];
        }
    }
    return _ktast;
}

// Returns PAFKT
- (NSArray *)pafkt{
    if (!_pafkt){
        [self setDbTableName:@"CustPAFKT"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"PAFKT",  //
                         @"VTEXT",  //
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustPAFKT findAllSorted];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _pafkt = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustPAFKT *t in resultList){
                [list addObject:[t toDictionaryWithKeys:keys]];
            }
            _pafkt = [NSArray arrayWithArray:list];
        }
    }
    return _pafkt;
}

// Returns PARH1 (Hobby)
- (NSArray *)parh1{
    if (!_parh1){
        [self setDbTableName:@"CustPARH1"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"PARH1",  //
                         @"VTEXT",  //
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustPARH1 findAllSorted];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _parh1 = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustPARH1 *t in resultList){
                [list addObject:[t toDictionaryWithKeys:keys]];
            }
            _parh1 = [NSArray arrayWithArray:list];
        }
    }
    return _parh1;
}

// Returns PARH2 (Hobby)
- (NSArray *)parh2{
    if (!_parh2){
        [self setDbTableName:@"CustPARH2"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"PARH2",  //
                         @"VTEXT",  //
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustPARH2 findAllSorted];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _parh2 = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustPARH2 *t in resultList){
                [list addObject:[t toDictionaryWithKeys:keys]];
            }
            _parh2 = [NSArray arrayWithArray:list];
        }
    }
    return _parh2;
}

// Returns PARH3 (Number of children)
- (NSArray *)parh3{
    if (!_parh3){
        [self setDbTableName:@"CustPARH3"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"PARH3",  //
                         @"VTEXT",  //
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustPARH3 findAll];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _parh3 = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustPARH3 *t in resultList){
                [list addObject:[t toDictionaryWithKeys:keys]];
            }
            _parh3 = [NSArray arrayWithArray:list];
        }
    }
    return _parh3;
}


// Returns VKBUR (Sales Bureaus)
- (NSArray *)vkbur{
    if (!_vkbur){
        [self setDbTableName:@"CustVKBUR"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"VKBUR",  //
                         @"BEZEI",  //
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustVKBUR findWithQuery:[self selectAllQuery:keys]];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _vkbur = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustVKBUR *t in resultList){
                [list addObject:[t toDictionaryWithKeys:keys]];
            }
            _vkbur = [NSArray arrayWithArray:list];
        }
    }
    return _vkbur;
}

// Returns ZTERM
- (NSArray *)zterm{
    if (!_zterm){
        [self setDbTableName:@"CustZTERM"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"VKORG",  //
                         @"ZTERM",  //
                         @"TEXT1",
                         @"TEXT2",
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustZTERM findWithQuery:[self selectAllQuery:keys]];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _zterm = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustZTERM *t in resultList){
                [list addObject:[t toDictionaryWithKeys:keys]];
            }
            _zterm = [NSArray arrayWithArray:list];
        }
    }
    return _zterm;
}

- (NSArray *)csimt{
    if (!_csimt){
        [self setDbTableName:@"CustCSIMT"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"AUGRU",  //
                         @"INDIC",  //
                         nil];
        SUPObjectList *resultList = [SalesPlusCustStatusIndicator findAll];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _csimt = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustStatusIndicator *t in resultList){
                [list addObject:[t toDictionaryWithKeys:keys]];
            }
            _csimt = [NSArray arrayWithArray:list];
        }
    }
    return _csimt;
}

// Returns BRSCH
- (NSArray *)brsch{
    if (!_brsch){
        [self setDbTableName:@"CustIndustry"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"BRSCH",  //
                         @"BRTEXT",  //
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustIndustry findAllSorted];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _brsch = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustIndustry *t in resultList){
                [list addObject:[t toDictionaryWithKeys:keys]];
            }
            _brsch = [NSArray arrayWithArray:list];
        }
    }
    return _brsch;
}

- (NSArray *)kdgrp{
    if (!_kdgrp){
        [self setDbTableName:@"CustCustomerGroup"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"KDGRP",  //
                         @"KTEXT",  //
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustCustomerGroup findAll];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _kdgrp = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustCustomerGroup *t in resultList){
                [list addObject:[t toDictionaryWithKeys:keys]];
            }
            _kdgrp = [NSArray arrayWithArray:list];
        }
    }
    return _kdgrp;
}

// Returns VRKMEActive
- (NSArray *)vrkmeActive{
    if (!_vrkmeActive){
        [self setDbTableName:@"CustVRKMEactive"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"VKORG",  //
                         @"ACTIV",  //
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustVRKMEactive findWithQuery:[self selectAllQuery:keys]];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _vrkmeActive = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustVRKMEactive *t in resultList){
                [list addObject:[t toDictionaryWithKeys:keys]];
            }
            _vrkmeActive = [NSArray arrayWithArray:list];
        }
    }
    return _vrkmeActive;
}

// Returns User Close Plant
- (NSArray *)userClosePlant{
    if (!_userClosePlant){
        [self setDbTableName:@"CustUserClosePlant"];
        NSArray *keys = [NSArray arrayWithObjects:
                         @"WERKS", 
                         nil];
        
        SUPObjectList *resultList = [SalesPlusCustUserClosePlant findWithQuery:[self selectAllQuery:keys]];
        
        if (resultList == nil){
            NSLog(@"query failed!!");
            _userClosePlant = [NSArray array];
        }else{
            NSMutableArray *list = [NSMutableArray arrayWithCapacity:resultList.length];
            
            for(SalesPlusCustUserClosePlant *t in resultList){
                [list addObject:[t toDictionaryWithKeys:keys]];
            }
            _userClosePlant = [NSArray arrayWithArray:list];
        }
    }
    return _userClosePlant;
}
@end
