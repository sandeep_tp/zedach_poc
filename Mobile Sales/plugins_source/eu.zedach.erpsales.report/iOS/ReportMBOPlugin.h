//
//  ReportingMBOPlugin.h
//  ERPSales
//
//  Created by YohanK on 2013-12-09.
//
//

#import <Cordova/CDV.h>
#import "ReportManager.h"

@interface ReportMBOPlugin : CDVPlugin
@property (retain,nonatomic) ReportManager *manager;

-(void)getUserReportList:(CDVInvokedUrlCommand*)command;
-(void)getUserReportDetails:(CDVInvokedUrlCommand*)command;
-(void)getProductReportList:(CDVInvokedUrlCommand*)command;
-(void)getProductReportDetails:(CDVInvokedUrlCommand*)command;
-(void)getCustomerReportList:(CDVInvokedUrlCommand*)command;
-(void)getCustomerReportDetails:(CDVInvokedUrlCommand*)command;
-(void)getCustomerBOReportList:(CDVInvokedUrlCommand*)command;
-(void)getuserBOReportList:(CDVInvokedUrlCommand*)command;

@end
