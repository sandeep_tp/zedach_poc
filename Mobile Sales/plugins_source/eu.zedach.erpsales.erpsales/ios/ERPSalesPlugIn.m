//
//  ERPSalesPlugIn.m
//  ERPSales
//
//  Created by Alexander Ilg on 5/11/13.
//
//

#import "ERPSalesPlugIn.h"
#import "SalesPlusERPSalesPlusDB.h"
#import "SUPQuery.h";
#import "SUPQueryResultSet.h";
#import "SUPDataValue.h";
#import "SUPAttributeTest.h";
#import "SUPCompositeTest.h";
#import "SUPAttributeTest.h";

@implementation ERPSalesPlugIn


- (void)test:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"test called");
    
    NSNumber *n1 = [NSNumber numberWithInt:1203];
    NSNumber *n2 = [NSNumber numberWithInt:3048];
    NSNumber *n3 = [NSNumber numberWithInt:7645];
    NSNumber *n4 = [NSNumber numberWithInt:4435];
    NSNumber *n5 = [NSNumber numberWithInt:9298];
    NSNumber *n6 = [NSNumber numberWithInt:7413];
    
    NSDictionary *account;
    account = [NSDictionary dictionaryWithObjectsAndKeys:
               n1,@"Jan",n2,@"Feb",n3,@"Mrz",n4,@"Apr",n5,@"Mai",n6,@"Jun",nil];
    
    NSArray *ships = [NSArray arrayWithObjects:@"Enterprise",@"Galileo",@"Excelsior",nil];
    NSArray *crew = [NSArray arrayWithObjects:@"Spock",@"Kirk",@"Uhura",nil];
    NSArray *testArray = [NSArray arrayWithObjects:ships, crew, @"Terra", account, nil];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:testArray.JSONString];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)deleteDatabase:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"deleteDatabase called");
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)activateDemoMode:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"activateDemoMode called");
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)activateProductiveMode:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"activateProductiveMode called");
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getSettings:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"getSettings called");
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getAttachments:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"getAttachments called");
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)updateSettings:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"updateSettings called");
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getCustomizing:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"getCustomizing called");
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)changeLanguage:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"changeLanguage called");
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end

