//
//  ERPSalesPlugIn.h
//  ERPSales
//
//  Created by Alexander Ilg on 5/11/13.
//
//

#import <Foundation/Foundation.h>
#import <Cordova/CDV.h>

@interface ERPSalesPlugIn : CDVPlugin


-(void)test:(CDVInvokedUrlCommand*)command;

-(void)deleteDatabase:(CDVInvokedUrlCommand*)command;

-(void)activateDemoMode:(CDVInvokedUrlCommand*)command;

-(void)activateProductiveMode:(CDVInvokedUrlCommand*)command;

-(void)getSettings:(CDVInvokedUrlCommand*)command;

-(void)getAttachments:(CDVInvokedUrlCommand*)command;

-(void)updateSettings:(CDVInvokedUrlCommand*)command;

-(void)getCustomizing:(CDVInvokedUrlCommand*)command;

-(void)changeLanguage:(CDVInvokedUrlCommand*)command;

@end
