//
//  CustomizingMBOPlugin.h
//  ERPSales
//
//  Created by Yohan Kariyawasan on 2013-07-19.
//
//
#import <Foundation/Foundation.h>
#import <Cordova/CDV.h>
#import "../eu.zedach.erpsales.login/CustomizingManager.h"

@interface CustomizingMBOPlugin : CDVPlugin

@property (strong,nonatomic) CustomizingManager *manager;

-(void)getCountryList:(CDVInvokedUrlCommand*)command;
-(void)getCurrencyList:(CDVInvokedUrlCommand*)command;
-(void)getMaterialGroupList:(CDVInvokedUrlCommand*)command;
-(void)getMaterialTypeList:(CDVInvokedUrlCommand*)command;
-(void)getStorageLocationList:(CDVInvokedUrlCommand*)command;
-(void)getTitleList:(CDVInvokedUrlCommand*)command;
-(void)getUnitOfMeasureList:(CDVInvokedUrlCommand*)command;
-(void)getVSBEDList:(CDVInvokedUrlCommand*)command;
-(void)getKTAARList:(CDVInvokedUrlCommand*)command;
-(void)getKTAERList:(CDVInvokedUrlCommand*)command;
-(void)getPARLAList:(CDVInvokedUrlCommand*)command;
-(void)getAUARTList:(CDVInvokedUrlCommand*)command;
-(void)getGEWEIList:(CDVInvokedUrlCommand*)command;
-(void)getKONDAList:(CDVInvokedUrlCommand*)command;
-(void)getKTASTList:(CDVInvokedUrlCommand*)command;
-(void)getPAFKTList:(CDVInvokedUrlCommand*)command;
-(void)getPARH1List:(CDVInvokedUrlCommand*)command;
-(void)getPARH2List:(CDVInvokedUrlCommand*)command;
-(void)getPARH3List:(CDVInvokedUrlCommand*)command;
-(void)getVKBURList:(CDVInvokedUrlCommand*)command;
-(void)getZTERMList:(CDVInvokedUrlCommand*)command;
-(void)getSalesOrgsSDdropdown:(CDVInvokedUrlCommand*)command;
-(void)getUserClosePlantList:(CDVInvokedUrlCommand*)command;

// method to get the three industries
@end
