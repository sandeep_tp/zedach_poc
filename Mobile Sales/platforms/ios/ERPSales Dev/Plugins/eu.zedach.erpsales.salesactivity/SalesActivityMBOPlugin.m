//
//  SalesActivityMBOPlugin.m
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-08-12.
//
//

#import "SalesActivityMBOPlugin.h"
#import "Util.h"

@implementation SalesActivityMBOPlugin
@synthesize manager = _manager;

-(void)pluginInitialize{
    self.manager = [SalesActivityManager defaultManager];
}

-(void)getSalesActivityDetails:(CDVInvokedUrlCommand *)command{
    MBOLog(@"getSalesActivityDetails called");
    NSString *vbeln = [command argumentAtIndex:0];
    NSDictionary *salesActivity = vbeln ? [self.manager salesActivityForVBELN:vbeln] : [NSDictionary dictionary];
    NSString *result = [Util convertNSDictionaryToJSON: salesActivity];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getSalesActivityList:(CDVInvokedUrlCommand *)command{
    MBOLog(@"getSalesActivityList called");
    int skip = 0;
    int take = 100;
    if (command.arguments.count == 2){
        skip = [[command.arguments objectAtIndex:0] integerValue];
        take = [[command.arguments objectAtIndex:1] integerValue];
    }
    NSArray *activityList = [self.manager getSalesActivityListFrom:skip To:take];
    NSString *result = [Util convertNSArrayToJSON: activityList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getSalesActivityListByCustomerNr:(CDVInvokedUrlCommand *)command{
    MBOLog(@"getSalesActivityListByCustomerNr called");
    NSString *kunnr = [command argumentAtIndex:0];
    NSArray *activityList = kunnr ? [self.manager salesActivityListForKunnr:kunnr] : [NSArray array];
    NSString *result = [Util convertNSArrayToJSON: activityList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)searchSalesActivity:(CDVInvokedUrlCommand *)command{
    MBOLog(@"searchSalesActivity called");
    NSString *searchCriteria =  [command argumentAtIndex:0];
    NSArray *sActivities = [self.manager searchSalesActivityWithCriteria:searchCriteria];
    NSString *result = [Util convertNSArrayToJSON: sActivities];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)searchSalesActivityByUNAME:(CDVInvokedUrlCommand *)command{
    MBOLog(@"searchSalesActivity called");
    NSString *searchCriteria =  [command argumentAtIndex:0];
    NSArray *sActivities = [self.manager searchSalesActivityWithUNAME:searchCriteria];
    NSString *result = [Util convertNSArrayToJSON: sActivities];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)searchSalesActivityAdvanced:(CDVInvokedUrlCommand *)command{
    MBOLog(@"searchSalesActivity called");
    
    NSMutableDictionary *values = [[NSMutableDictionary alloc] initWithDictionary:[command argumentAtIndex:0]];
    NSArray *sActivities = [self.manager searchSalesActivityWithCriteriaAndMultipleRanges:values];
    NSString *result = [Util convertNSArrayToJSON: sActivities];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)createSalesActivity:(CDVInvokedUrlCommand *)command{
    MBOLog(@"createSalesActivity called");
    NSMutableDictionary *values = [[NSMutableDictionary alloc] initWithDictionary:[command argumentAtIndex:0]];
    NSString *activityID = [self.manager createSalesActivity:values];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:activityID];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)saveDayStartEndLocation:(CDVInvokedUrlCommand *)command{
    MBOLog(@"createDayStartEndLocation called");
    //NSMutableDictionary *values = [[NSMutableDictionary alloc] initWithDictionary:[command argumentAtIndex:0]];
    //[self.manager saveDayStartEndLocation:values];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)updateSalesActivity:(CDVInvokedUrlCommand *)command{
    MBOLog(@"editSalesActivity called");
    NSMutableDictionary *values = [[NSMutableDictionary alloc] initWithDictionary:[command argumentAtIndex:0]];
    bool result = [self.manager updateSalesActivity:values];
    CDVPluginResult* pluginResult;
    if (result == YES){
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    }else{
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)deleteSalesActivity:(CDVInvokedUrlCommand *)command{
    MBOLog(@"deleteSalesActivity called");
    NSString *doc_number = [command argumentAtIndex:0];
    bool result = [self.manager deleteSalesActivity:doc_number];
    CDVPluginResult* pluginResult;
    if (result == YES){
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    }else{
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
