//
//  MaterialPicture.m
//  MaterialPicture
//
//  Created by YohanK on 2013-09-23.
//  Copyright (c) 2013 Yohan K. All rights reserved.
//

#import "MaterialPicture.h"

@implementation MaterialPicture

@synthesize matnr = _matnr;
@synthesize version = _version;
@synthesize path = _path;
@synthesize sup = _sup;

- (void)encodeWithCoder:(NSCoder *)encoder
{
    //Encode properties, other class variables, etc
	[encoder encodeObject:self.matnr forKey:@"matnr"];
	[encoder encodeObject:self.version forKey:@"version"];
	[encoder encodeObject:self.path forKey:@"path"];
    [encoder encodeObject:self.sup forKey:@"sup"];
    
}

- (id)initWithCoder:(NSCoder *)decoder
{
	self = [super init];
	if( self != nil )
	{
        //decode properties, other class vars
		self.matnr = [decoder decodeObjectForKey:@"matnr"];
		self.version = [decoder decodeObjectForKey:@"version"];
		self.path = [decoder decodeObjectForKey:@"path"];
        self.sup = [decoder decodeObjectForKey:@"sup"];
	}
	return self;
}

+ (MaterialPicture *)loadMaterialPictureObjectWithKey:(NSString *)matnr {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedMP = [defaults objectForKey:matnr];
    MaterialPicture *mp = (MaterialPicture *)[NSKeyedUnarchiver unarchiveObjectWithData: encodedMP];
    return mp;
}
@end
