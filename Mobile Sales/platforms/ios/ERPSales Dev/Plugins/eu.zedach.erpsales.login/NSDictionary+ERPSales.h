//
//  NSDictionary+ERPSales.h
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-08-12.
//
//

#import <Foundation/Foundation.h>
#import "SUPAbstractEntityRBS.h"
@interface NSDictionary (ERPSales)

-(void)setValuesInMBO:(SUPAbstractEntityRBS*)mbo;

@end
