//
//  Util.h
//  ERPSales Dev
//
//  Created by Klaus Rollinger on 05.07.17.
//
//

#ifndef Util_h
#define Util_h
@interface Util : NSObject
+(NSString *) convertNSArrayToJSON:(NSArray *)array;
+(NSString *) convertNSDictionaryToJSON:(NSDictionary *)dic;
@end

#endif /* Util_h */
