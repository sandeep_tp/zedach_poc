//
//  BaseManager.h
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-06-26.
//
//

#import <Foundation/Foundation.h>
#import "SUPQuery.h"

@interface BaseManager : NSObject

- (id) initWithTableName:(NSString *)tableName;
- (SUPQuery *)selectAllQuery:(NSArray *)keys;
- (SUPQuery *)selectAllQuery:(NSArray *)keys from:(SUPInt)skip to:(SUPInt)take orderBy:(NSString *)column ascending:(bool)ascending;
- (SUPQuery *)searchQueryForValue:(NSString*)searchString inKeys:(NSArray *)searchKeys withReturnKeys:(NSArray *)returnKeys;
- (void)setDbTableName:(NSString *)dbTableName;
- (void)clearCache;

@end
