//
//  OpenQuotation.h
//  ERPSales
//
//  Created by YohanK on 2014-01-23.
//
//

#import "BaseManager.h"

@interface OpenQuotationManager : BaseManager

@property (nonatomic, retain) NSArray *openQuotationKeys;

+ (id) defaultManager;

- (NSArray *)getLastOpenQuotationForCustomer:(NSString *)customer;
- (NSString *)isOpenQuotation:(NSString *)VBELN;
- (NSDictionary *)salesQuotationDetails:(NSString *)docNumber;
- (bool)updateQuotationValidity:(NSDictionary *)values;

@end
