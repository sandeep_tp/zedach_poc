//
//  ProductManager.h
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-06-26.
//
//

#import "BaseManager.h"

@interface ProductManager : BaseManager {
//    NSDictionary *productLightList;
}
@property (strong,nonatomic) NSMutableDictionary *productCountForCategory;
@property (strong,nonatomic) NSArray* productLightList;
@property (strong,nonatomic) NSArray* productCategories;

@property (strong,nonatomic) NSArray* productHighlightList;
@property (strong,nonatomic) NSArray* productKeys;
+(id) defaultManager;

- (NSDictionary *)productDetailsForMATNR:(NSString *)MATNR;
- (NSMutableArray *)getTopLevelCategories;
- (NSMutableArray *)getSubCategoriesForCategoryName:(NSString *)parentCatName withId:(NSString *)parentCatId getChildren:(bool)readChildren;
- (NSArray *)getProductListForCategoryId:(NSString *)catId withDetails:(bool)flag;
- (NSArray *)getProductListFrom:(int)skip To:(int)take;
- (NSArray *)searchProductsWithCriteria:(NSString*)searchString;
- (int)getProductForCategoryName:(NSString *)parentCatName withId:(NSString *)parentCatId;
- (void)updateProductCountForCategories;
- (void)clearCache;
- (NSString *)isProductLocal:(NSString *)MATNR;
@end
