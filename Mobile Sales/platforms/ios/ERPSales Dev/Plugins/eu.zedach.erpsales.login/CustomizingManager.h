//
//  CustomizingManager.h
//  ERPSales
//
//  Created by Yohan Kariyawasan on 2013-07-18.
//
//

#import "BaseManager.h"

@interface CustomizingManager : BaseManager

@property (strong,nonatomic) NSArray* countryList;
@property (strong,nonatomic) NSArray* currencyList;
@property (strong,nonatomic) NSArray* materialGroupList;
@property (strong,nonatomic) NSArray* materialTypeList;
@property (strong,nonatomic) NSArray* storageLocationList;
@property (strong,nonatomic) NSArray* titleList;
@property (strong,nonatomic) NSArray* unitOfMeasureList;
@property (strong,nonatomic) NSArray* vsbed;
@property (strong,nonatomic) NSArray* ktaar;
@property (strong,nonatomic) NSArray* ktaer;
@property (strong,nonatomic) NSArray* parla;
@property (strong,nonatomic) NSArray* auart;
@property (strong,nonatomic) NSArray* gewei;
@property (strong,nonatomic) NSArray* konda;
@property (strong,nonatomic) NSArray* ktast;
@property (strong,nonatomic) NSArray* pafkt;
@property (strong,nonatomic) NSArray* parh1;
@property (strong,nonatomic) NSArray* parh2;
@property (strong,nonatomic) NSArray* parh3;
@property (strong,nonatomic) NSArray* vkbur;
@property (strong,nonatomic) NSArray* zterm;
@property (strong,nonatomic) NSArray* csimt;
@property (strong,nonatomic) NSArray* brsch;
@property (strong,nonatomic) NSArray* kdgrp;
@property (strong,nonatomic) NSArray* vrkmeActive;
@property (strong,nonatomic) NSArray* userClosePlant;

+(id) defaultManager;
- (void)loadAllCustoming;
-(void) clearCache;
@end
