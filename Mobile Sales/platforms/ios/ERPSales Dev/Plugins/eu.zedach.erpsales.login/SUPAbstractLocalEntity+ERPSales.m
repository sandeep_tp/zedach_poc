//
//  SUPAbstractLocalEntity+ERPSales.m
//  ERPSales
//
//  Created by YohanK on 2013-12-30.
//
//

#import "SUPAbstractLocalEntity+ERPSales.h"
#import "SUPClassWithMetaData.h"
#import "SUPClassMetaDataRBS.h"
#import "SUPDataType.h"
#import "Util.h"
#import <Cordova/CDV.h>

@interface SUPAbstractLocalEntity (ERPSales)

/* stores the list of MBO properties to exclude from JSON result */
- (NSArray*) exclusions;
- (NSDateFormatter*) dateFormatter;
- (NSDateFormatter*) timeFormatter;

- (id)valueforAttribute:(id<SUPAttributeMetaDataProtocol>)attribute;
@end

@implementation SUPAbstractLocalEntity (ERPSales)


- (NSString*)toJSONString{
    NSString *result = [Util convertNSDictionaryToJSON:self.toDictionary];
    //return self.toDictionary.JSONString;
}

- (id)valueForAttribute:(id<SUPAttributeMetaDataProtocol>)attribute{
    SUPDataType *dataType = [attribute dataType];
    id value = [self valueForKey:[attribute name]];
    if (dataType.code == SUPDataType.DATE){
        value = value == nil ? @"" : [self.dateFormatter stringFromDate:value];
    }else if (dataType.code == SUPDataType.TIME){
        value = value == nil ? @"" : [self.timeFormatter stringFromDate:value];
    }else if (dataType.code == SUPDataType.LIST){
        value = @"";
    }else if (dataType.code == SUPDataType_OBJECT){ //incase when the MBO has a relationship to another object that object values are ignored
        value = @"";
    }
    return value = value == nil ? @"" : value;
}

- (id)valueForAttributeKey:(NSString *)attributeKey{
    if (attributeKey){
        SUPClassMetaDataRBS *metaData = [((id<SUPClassWithMetaData>)self)getClassMetaData];
        SUPAttributeMap *attributeMap = metaData.attributeMap;
        id<SUPAttributeMetaDataProtocol> attribute = [attributeMap item:attributeKey];
        if (attribute){
            return [self valueForAttribute:attribute];
        }
    }
}

- (NSMutableDictionary*)toDictionaryWithKeys:(NSArray *)keys{
    if (keys){
        SUPClassMetaDataRBS *metaData = [((id<SUPClassWithMetaData>)self)getClassMetaData];
        SUPAttributeMap *attributeMap = metaData.attributeMap;
        
        NSMutableDictionary *contents = [NSMutableDictionary dictionaryWithCapacity:keys.count];
        
        for (NSString *key in keys) {
            id<SUPAttributeMetaDataProtocol> attribute = [attributeMap item:key];
            if (attribute){
                [contents setObject:[self valueForAttribute:attribute] forKey:key];
            }
        }
        return contents;
    }
    return [NSMutableDictionary dictionaryWithCapacity:0];
    
}


- (NSMutableDictionary*)toDictionary{
    if ([self conformsToProtocol:@protocol(SUPClassWithMetaData) ]){
        SUPClassMetaDataRBS *metaData = [((id<SUPClassWithMetaData>)self)getClassMetaData];
        
        SUPObjectList *attributes = metaData.attributes;
        NSArray *exclusions = self.exclusions;
        
        NSMutableDictionary *contents = [NSMutableDictionary dictionaryWithCapacity:attributes.length - exclusions.count];
        
        for (id<SUPAttributeMetaDataProtocol> att in attributes) {
            NSString *key = [att name];
            if ([exclusions containsObject:key] == NO){
                [contents setObject:[self valueForAttribute:att] forKey:key];
            }
        }
        
        return contents;
    }
    return [NSMutableDictionary dictionaryWithCapacity:0];
}

- (NSArray*) exclusions {
    static NSArray* exclusionArray = nil;
    
    if (exclusionArray == nil){
        exclusionArray = [NSArray arrayWithObjects:
                          @"cvpOperation",
                          @"cvpOperationLength",
                          @"cvpOperationLobs",
                          @"cvpOperationLobsLength",
                          @"disableSubmit",
                          @"pending",
                          @"pendingChange",
                          @"replayCounter",
                          @"replayFailure",
                          @"replayPending",
                          @"surrogateKey",
                          nil];
    }
    return exclusionArray;
}

- (NSDateFormatter *)dateFormatter{
    static NSDateFormatter *df = nil;
    if (df == nil){
        df = [[NSDateFormatter alloc]init];
        [df setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT+0:00"]];
        df.dateFormat = @"yyyMMdd";
    }
    return df;
}

- (NSDateFormatter *)timeFormatter{
    static NSDateFormatter *tf = nil;
    if (tf == nil){
        tf = [[NSDateFormatter alloc]init];
        [tf setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT+0:00"]];
        tf.dateFormat = @"HHmmss";
    }
    return tf;
}

@end
