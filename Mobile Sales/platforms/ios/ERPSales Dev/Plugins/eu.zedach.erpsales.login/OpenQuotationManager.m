//
//  OpenQuotation.m
//  ERPSales
//
//  Created by YohanK on 2014-01-23.
//
//

#import "OpenQuotationManager.h"
#import "SalesPlusOpenQuotation.h"
#import "SalesPlusOpenQuotationItem.h"
#import "SalesPlusOpenQuotationSignature.h"
#import "SUPAbstractLocalEntity+ERPSales.h"
#import "CustomizingManager.h"
#import "SalesOrderManager.h"
#import "SalesPlusOpenQuotationAttachments.h"

@implementation OpenQuotationManager

@synthesize openQuotationKeys = _openQuotationKeys;

+ (id) defaultManager{
    static OpenQuotationManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[OpenQuotationManager alloc]initWithTableName:@"OpenQuotationManager"];
        manager.openQuotationKeys = [NSArray arrayWithObjects:
                                     @"DOC_NUMBER", // Document No. or VBELN
                                     @"DOC_TYPE",
                                     @"SALES_ORG",  // Sales Organization
                                     @"DISTR_CHAN", // Distribution Channel
                                     @"DIVISION",   // Division
                                     @"SOLD_TO",    // Customer No. or KUNNR
                                     @"BSTKD",      // Purchase Order No.
                                     @"PURCH_DATE", // Order Date
                                     @"REQ_DATE_H", // Delivery Date
                                     @"KTEXT1",     // Note 1
                                     @"KTEXT2",     // Note 2
                                     @"CREATED_BY",
                                     @"CURRENCY",
                                     @"VBELV",      // Sales Activity
                                     @"NET_VAL_HD",
                                     @"ORD_REASON",
                                     @"QT_VALID_F", //offer valid from
                                     @"QT_VALID_T", //offer valid until
                                     nil];
    });
    return manager;
}

- (NSArray *) getLastOpenQuotationForCustomer:(NSString *)customer {
    NSMutableArray *lastQuotations = [NSMutableArray array];
    SalesPlusOpenQuotation *oq = [SalesPlusOpenQuotation findLastOfCustomer:customer];
    [lastQuotations addObject:[self salesQuotationDetails:oq.DOC_NUMBER]];
    return lastQuotations;
}

- (NSDictionary *)salesQuotationDetails:(NSString *)docNumber{
    NSArray *itemKeys = [NSArray arrayWithObjects:
                         @"DLV_PRIO",   // Delivery Order Date Fixed
                         @"ITM_NUMBER", // Position or POSNR
                         @"MATERIAL",   // Material or MATNR
                         @"REQ_QTY",    // Order Quantity or KWMENG
                         @"SALES_UNIT",
                         @"SHORT_TEXT",
                         @"COND_VALUE",
                         @"COND_P_UNT",
                         @"COND_UNIT",
                         @"NET_VALUE",
                         @"ALTERN_ITM",
                         @"DISCOUNT_VALUE",
                         nil];
    NSArray *attachmentsKeys = [NSArray arrayWithObjects:
                         @"LINK",
                         @"DESCRIPTION",
                         nil];
    
    SalesPlusOpenQuotation *openQuotation = [SalesPlusOpenQuotation findByPrimaryKey:docNumber];
    NSMutableDictionary *result = [[NSMutableDictionary alloc] initWithCapacity:0];
    NSMutableDictionary *headerDict = [openQuotation toDictionaryWithKeys:self.openQuotationKeys];
    NSArray *csimts = [[CustomizingManager defaultManager] csimt];
    for (int ii = 0; ii < csimts.count; ii++){
        NSDictionary *csimt = [csimts objectAtIndex:ii];
        if ([[csimt objectForKey:@"AUGRU"] isEqualToString:[headerDict objectForKey:@"ORD_REASON"]]){
            [headerDict setObject:[csimt objectForKey:@"INDIC"] forKey:@"ORD_REASON"];
        }
    }
    SalesPlusOpenQuotationSignature *oqSignature = openQuotation.openQuotationSignatures;
    if (oqSignature && ![oqSignature.SIGNATURE isNull]){
        SUPBigBinary *fileContent = oqSignature.SIGNATURE;
        NSData *imageData = [oqSignature.SIGNATURE value];
        NSString *imageString = [SalesOrderManager base64forData:imageData];
        [headerDict setObject:imageString forKey:@"SIGNATURE"];
    }else{
        [headerDict setObject:@"" forKey:@"SIGNATURE"];
    }
    [result setValue:headerDict forKey:@"Header"];
    NSMutableArray *itemResult = nil;
    if (openQuotation){
        SUPObjectList *openQuotationItemList = [SalesPlusOpenQuotationItem findByDOCsorted:openQuotation.DOC_NUMBER];
        //SUPObjectList *openQuotationItemList = openQuotation.openQuotationItems;
        itemResult = [NSMutableArray arrayWithCapacity:openQuotationItemList.length];
        for (SalesPlusOpenQuotationItem *i in openQuotationItemList) {
            NSDictionary *soi = [i toDictionaryWithKeys:itemKeys];
            [itemResult addObject:soi];
        }
        if(itemResult.count > 0){
            [result setObject:itemResult forKey:@"Items"];
        }
        // get the current logged in language
        NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
        NSArray* languages = [defs objectForKey:@"AppleLanguages"];
        NSString* preferredLang = [languages objectAtIndex:0];
        NSString *language = [[preferredLang capitalizedString] substringToIndex:1];
        
        NSMutableArray *attachmentsResult = nil;
        SUPObjectList *attachmentItems = [SalesPlusOpenQuotationAttachments findByVbelnSpras:openQuotation.DOC_NUMBER withSPRAS:language];
        attachmentsResult = [NSMutableArray arrayWithCapacity:attachmentItems.length];
        for (SalesPlusOpenQuotationAttachments *atta in attachmentItems) {
            NSDictionary *oqa = [atta toDictionaryWithKeys:attachmentsKeys];
            [attachmentsResult addObject:oqa];
        }
        if(attachmentsResult.count > 0){
            [result setObject:attachmentsResult forKey:@"ATTACHMENTS"];
        }
    }else{
        result = [NSMutableDictionary dictionaryWithCapacity:0];
    }
    return result;
}

-(NSString *)isOpenQuotation:(NSString *)VBELN {
    SalesPlusOpenQuotation *oq = [SalesPlusOpenQuotation findByPrimaryKey:VBELN];
    if (oq){
        return @"X";
    }
    return @"";
}

- (bool)updateQuotationValidity:(NSDictionary *)values{
    NSDictionary *header = [values objectForKey:@"SalesOrderHeader"];
    SalesPlusOpenQuotation *so = [SalesPlusOpenQuotation findByPrimaryKey:[header objectForKey:@"DOC_NUMBER"]];
    if (so){
        NSDate *salesDocValidDate;
        
        NSDateFormatter* df = [[NSDateFormatter alloc] init];
        
        [df setDateFormat:@"yyyyMMdd"];
        [df setTimeZone: [NSTimeZone timeZoneWithName:@"GMT"]];
        salesDocValidDate = [df dateFromString: header[@"QT_VALID_T"]];
        
        //so.QT_VALID_T = salesDocValidDate;
        //[so update];
        //[so submitPending];
        return YES;
    }else{
        return NO;
    }
}



@end
