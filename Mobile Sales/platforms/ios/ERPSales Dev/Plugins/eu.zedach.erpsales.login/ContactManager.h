//
//  ContactManager.h
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-06-26.
//
//

#import "BaseManager.h"

@interface ContactManager : BaseManager

@property (strong,nonatomic) NSArray* contactKeys;
@property (strong,nonatomic) NSArray* contactLightList;

+(id) defaultManager;

-(NSArray *)contactListForCustomer:(NSString *)kunnr;
-(NSDictionary *)contactDetails:(NSString *)parnr;
-(void)createContact:(NSDictionary *)values;
-(void)updateContact:(NSDictionary *)values;
-(void)deleteContact:(NSDictionary *)values;
@end
