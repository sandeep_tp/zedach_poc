//
//  ReportingManager.m
//  ERPSales
//
//  Created by YohanK on 2013-12-09.
//
//

#import "ReportManager.h"
#import "SalesPlusReportingText.h"
#import "SalesPlusReportingData.h"
#import "SalesPlusReportingCustomizing.h"
#import "SalesPlusReportingMasterUser.h"
#import "SalesPlusReportingMasterProduct.h"
#import "SalesPlusReportingMasterCustomer.h"
#import "SUPAbstractLocalEntity+ERPSales.h"
#import "SUPAttributeTest.h"
#import "SUPSortCriteria.h"
#import "SUPAttributeSort.h"
#import "SalesPlusBOLinkCustomer.h"
#import "SalesPlusBOLinkCustomerText.h"
#import "SalesPlusBOLinkUser.h"
#import "SalesPlusBOLinkUserText.h"

@implementation ReportManager

@synthesize reportKeys = _reportKeys;

+ (id) defaultManager{
    static ReportManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ReportManager alloc]initWithTableName:@"SalesActivity"];
        manager.reportKeys = [NSArray arrayWithObjects:
                                     @"LAST_EXEC_DATE",
                                     nil];
    });
    return manager;
}

- (NSArray *)getUserReportList{
    SUPObjectList *userReportList = [SalesPlusReportingMasterUser findAll];
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:userReportList.length];
    
    for (SalesPlusReportingMasterUser *ru in userReportList) {
        SalesPlusReportingCustomizing *rc = [SalesPlusReportingCustomizing findByPrimaryKey:ru.REP_C_REF];
        NSMutableDictionary *values = [[NSMutableDictionary alloc] initWithDictionary:[rc toDictionaryWithKeys:self.reportKeys]];
        [values setObject:ru.ID forKey:@"ID"];
        if([rc.ACTIVE isEqualToString:@"O"]){
            [values setObject:@"X" forKey:@"ONLINE_REPORT"];
        }
        else{
            [values setObject:@"" forKey:@"ONLINE_REPORT"];
        }
        SUPObjectList *reportingTextList = [SalesPlusReportingText findByID:rc.DESCRIPTION_ID];
        SalesPlusReportingText *rt_description = reportingTextList.length > 0 ? [reportingTextList objectAtIndex:0] : nil;
        NSString *descriptionText = rt_description != nil ? rt_description.TEXT : @"";
        [values setObject:descriptionText forKey:@"DESCRIPTION"];
        reportingTextList = [SalesPlusReportingText findByID:rc.TITLE_ID];
        SalesPlusReportingText *rt_title = reportingTextList.length > 0 ? [reportingTextList objectAtIndex:0] : nil;
        NSString *reportingText = rt_title != nil ? rt_title.TEXT : @"";
        [values setObject:reportingText forKey:@"TEXT"];
        [result addObject:values];
    }
    return result;
}

- (NSDictionary *)getReportingData:(NSString *)reportNumber{
//    SUPQuery *myquery = [SUPQuery getInstance];
//    myquery.testCriteria = [SUPAttributeTest match:@"ID_REF" :reportNumber];
//    SUPSortCriteria *sort = [SUPSortCriteria getInstance];
//    [sort add:[SUPAttributeSort ascending:@"NR"]];
//    myquery.sortCriteria = sort;
//    SUPObjectList *list  = [SalesPlusReportingData findWithQuery: myquery];
    SUPObjectList *list = [SalesPlusReportingData findByID_REFsorted:reportNumber];
    NSString *reportingdata = @"";
    for (SalesPlusReportingData *rd in list) {
        // workaround where the space is missing in the end of the line
        if (rd.LINE.length != 255){
            rd.LINE = [NSString stringWithFormat:@"%@ ",rd.LINE];
        }
        reportingdata = [NSString stringWithFormat:@"%@%@",reportingdata,rd.LINE];
    }
    NSMutableDictionary *reportDetails = [[NSMutableDictionary alloc] initWithCapacity:1];
    [reportDetails setObject:reportingdata forKey:@"TEXT"];
    [reportDetails setObject:reportNumber forKey:@"ID"];
    return reportDetails;
}

- (NSDictionary *)getUserReportDetails:(NSString *)reportNumber{
    return [self getReportingData:reportNumber];
}

- (NSArray *)getProductReportList:(NSString *)productNumber {
    SUPObjectList *productReportList = [SalesPlusReportingMasterProduct findByProduct:productNumber];
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:productReportList.length];
    
    for (SalesPlusReportingMasterProduct *rp in productReportList) {
        //read the report title, description and last execute day
        SalesPlusReportingCustomizing *rc = [SalesPlusReportingCustomizing findByPrimaryKey:rp.REP_C_REF];
        NSMutableDictionary *values = [[NSMutableDictionary alloc] initWithDictionary:[rc toDictionaryWithKeys:self.reportKeys]];
        [values setObject:rp.ID forKey:@"ID"];
        SUPObjectList *reportingTextList = [SalesPlusReportingText findByID:rc.DESCRIPTION_ID];
        SalesPlusReportingText *rt_description = reportingTextList.length > 0 ? [reportingTextList objectAtIndex:0] : nil;
        NSString *descriptionText = rt_description != nil ? rt_description.TEXT : @"";
        [values setObject:descriptionText forKey:@"DESCRIPTION"];
        reportingTextList = [SalesPlusReportingText findByID:rc.TITLE_ID];
        SalesPlusReportingText *rt_title = reportingTextList.length > 0 ? [reportingTextList objectAtIndex:0] : nil;
        NSString *reportingText = rt_title != nil ? rt_title.TEXT : @"";
        [values setObject:reportingText forKey:@"TEXT"];
        [result addObject:values];
    }
    return result;
}

- (NSDictionary *)getProductReportDetails:(NSString *)reportNumber {
    return [self getReportingData:reportNumber];
}

- (NSArray *)getCustomerReportList:(NSString *)customerNumber {
    SUPObjectList *customerReportList = [SalesPlusReportingMasterCustomer findByCustomer:customerNumber];
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:customerReportList.length];
    
    for (SalesPlusReportingMasterCustomer *rmc in customerReportList) {
        //read the report title, description and last execute day
        SalesPlusReportingCustomizing *rc = [SalesPlusReportingCustomizing findByPrimaryKey:rmc.REP_C_REF];
        NSMutableDictionary *values = [[NSMutableDictionary alloc] initWithDictionary:[rc toDictionaryWithKeys:self.reportKeys]];
        [values setObject:rmc.ID forKey:@"ID"];
        if([rc.ACTIVE isEqualToString:@"O"]){
            [values setObject:@"X" forKey:@"ONLINE_REPORT"];
        }
        else{
            [values setObject:@"" forKey:@"ONLINE_REPORT"];
        }
        SUPObjectList *reportingTextList = [SalesPlusReportingText findByID:rc.DESCRIPTION_ID];
        SalesPlusReportingText *rt_description = reportingTextList.length > 0 ? [reportingTextList objectAtIndex:0] : nil;
        NSString *descriptionText = rt_description != nil ? rt_description.TEXT : @"";
        [values setObject:descriptionText forKey:@"DESCRIPTION"];
        reportingTextList = [SalesPlusReportingText findByID:rc.TITLE_ID];
        SalesPlusReportingText *rt_title = reportingTextList.length > 0 ? [reportingTextList objectAtIndex:0] : nil;
        NSString *reportingText = rt_title != nil ? rt_title.TEXT : @"";
        [values setObject:reportingText forKey:@"TEXT"];
        [result addObject:values];
    }
    return result;
}

- (NSDictionary *)getCustomerReportDetails:(NSString *)reportNumber {
    return [self getReportingData:reportNumber];
}

- (NSArray *)getCustomerBOReportList:(NSString *)customerNumber {
    SUPObjectList *customerReportList = [SalesPlusBOLinkCustomer findByKUNNR:customerNumber];
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:customerReportList.length];
    
    for (SalesPlusBOLinkCustomer *bocr in customerReportList) {
        NSMutableDictionary *temp = [[NSMutableDictionary alloc] initWithCapacity:3];
        [temp setObject:bocr.ID forKey:@"ID"];
        [temp setObject:bocr.URL forKey:@"URL"];
        SalesPlusBOLinkCustomerText *boct = [[SalesPlusBOLinkCustomerText findByID:bocr.T_ID] objectAtIndex:0];
        NSString *reportingText = boct != nil ? boct.TEXT : @"";
        [temp setObject:reportingText forKey:@"TEXT"];
        [result addObject:temp];
    }
    return result;
}

- (NSArray *)getUserBOReportList{
    SUPObjectList *userReportList = [SalesPlusBOLinkUser findAll];
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:userReportList.length];
    
    for (SalesPlusBOLinkUser *bour in userReportList) {
        NSMutableDictionary *temp = [[NSMutableDictionary alloc] initWithCapacity:3];
        [temp setObject:bour.ID forKey:@"ID"];
        [temp setObject:bour.URL forKey:@"URL"];
        SalesPlusBOLinkUserText *bout = [[SalesPlusBOLinkUserText findByID:bour.T_ID] objectAtIndex:0];
        NSString *reportingText = bout != nil ? bout.TEXT : @"";
        [temp setObject:reportingText forKey:@"TEXT"];
        [result addObject:temp];
    }
    
    return result;
}
@end
