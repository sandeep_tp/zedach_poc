//
//  ContactMBOPlugIn.h
//  ERPSales
//
//  Created by Alexander Ilg on 5/11/13.
//
//

#import <Foundation/Foundation.h>
#import <Cordova/CDV.h>
#import "ContactManager.h"

@interface ContactMBOPlugIn : CDVPlugin

@property (strong, nonatomic) ContactManager *manager;

-(void)getContactListByCustomerNr:(CDVInvokedUrlCommand*)command;

-(void)getContactDetails:(CDVInvokedUrlCommand*)command;

-(void)getContactDetailsList:(CDVInvokedUrlCommand*)command;

-(void)updateContact:(CDVInvokedUrlCommand*)command;

-(void)createContact:(CDVInvokedUrlCommand*)command;

-(void)deleteContact:(CDVInvokedUrlCommand*)command;

@end

