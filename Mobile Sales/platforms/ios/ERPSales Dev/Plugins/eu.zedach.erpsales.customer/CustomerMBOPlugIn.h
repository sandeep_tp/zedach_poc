//
//  CustomerMBOPlugIn.h
//  ERPSales
//
//  Created by Alexander Ilg on 5/11/13.
//
//

#import <Foundation/Foundation.h>
#import <Cordova/CDV.h>
#import "../eu.zedach.erpsales.login/CustomerManager.h"

@interface CustomerMBOPlugIn : CDVPlugin
@property (strong, nonatomic) CustomerManager *manager;

-(void)getCustomerList:(CDVInvokedUrlCommand*)command;

-(void)getCustomerDetails:(CDVInvokedUrlCommand*)command;

-(void)searchCustomer:(CDVInvokedUrlCommand*)command;

-(void)searchCustomerByUNAME:(CDVInvokedUrlCommand*)command;

-(void)updateCustomer:(CDVInvokedUrlCommand*)command;

-(void)startGPS:(CDVInvokedUrlCommand*)command;

-(void)getCustomerListByLocation:(CDVInvokedUrlCommand*)command;

-(void)addFavorite:(CDVInvokedUrlCommand*)command;

-(void)removeFavorite:(CDVInvokedUrlCommand*)command;

-(void)getFavoriteList:(CDVInvokedUrlCommand*)command;

@end
