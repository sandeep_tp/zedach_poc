//
//  CustomerMBOPlugIn.m
//  ERPSales
//
//  Created by Alexander Ilg on 5/11/13.
//
//

#import "CustomerMBOPlugIn.h"
#import "SalesPlusCustomer.h"
#import "SalesPlusERPSalesPlus_SuperUserDB.h"
#import "SalesPlusCustomerMetaData.h"
#import "SUPQuery.h"
#import "SUPQueryResultSet.h"
#import "SUPDataValue.h"
#import "SUPAttributeTest.h"
#import "SUPCompositeTest.h"
#import "SUPAttributeTest.h"
#import "SUPAbstractLocalEntity+ERPSales.h"
#import "CustomerManager.h"
#import "../eu.zedach.erpsales.login/SalesOrderManager.h"
#import "../eu.zedach.erpsales.login/Util.h"
#import <CoreLocation/CoreLocation.h>

@implementation CustomerMBOPlugIn

@synthesize manager = _manager;

- (void)pluginInitialize{    
    self.manager = [CustomerManager defaultManager];
}

- (void)getCustomerList:(CDVInvokedUrlCommand*)command
{
    NSDate *methodStart = [NSDate date];
    MBOLog(@"getCustomerList called %@",[NSDate date]);
    int skip = 0;
    int take = 100;
    if (command.arguments.count == 2){
        skip = [[command.arguments objectAtIndex:0] integerValue];
        take = [[command.arguments objectAtIndex:1] integerValue];
    }
    NSArray *customerList = [self.manager getCustomerListFrom:skip To:take];
    NSString *result = [Util convertNSArrayToJSON: customerList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    NSDate *methodFinish = [NSDate date];
    NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
    NSLog(@"executionTime getCustomerList = %f", executionTime);

}

- (void)getCustomerDetails:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"getCustomerDetails called");
    
    // get the kunnr (customer number) from PhoneGap
    NSString* kunnr = [command.arguments objectAtIndex:0];
    NSDictionary *customer = [self.manager customerDetailsForKUNNR:kunnr];
    NSString *result = [Util convertNSDictionaryToJSON: customer];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)searchCustomer:(CDVInvokedUrlCommand*)command{
    MBOLog(@"searchCustomer called");
    NSString *searchCriteria =  [command argumentAtIndex:0];
    
    NSArray *customers = [self.manager searchCustomersWithCriteria:searchCriteria];
    NSString *result = [Util convertNSArrayToJSON: customers];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)searchCustomerByUNAME:(CDVInvokedUrlCommand*)command{
    MBOLog(@"searchCustomerByUNAME called");
    NSString *searchCriteria =  [command argumentAtIndex:0];
    
    NSArray *customers = [self.manager searchCustomersWithUNAME:searchCriteria];
    NSString *result = [Util convertNSArrayToJSON: customers];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)updateCustomer:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"updateCustomer called");
    NSDictionary *updateCustomerValues = [command argumentAtIndex:0];
    [self.manager updateCustomer:updateCustomerValues];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getCustomerListByLocation:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getCustomerListByLocation called");
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}


// Returns all customers within 100KM of the current location.
// HTML sends current latitude and longitude as array parameter [latitude, longitude].
- (void)getCustomerListWithinRange:(CDVInvokedUrlCommand*)command{
    
    NSNumber *latitude  = [command argumentAtIndex:0];
    NSNumber *longitude = [command argumentAtIndex:1];
    
    NSMutableArray *resultList = [NSMutableArray array];
    //VILLIGER DOEST CARE
    
    if (latitude && longitude){
        double currentLatitude  = latitude.doubleValue;
        double currentLongitude = longitude.doubleValue;
        
        CLLocation *currentLocation = [[CLLocation alloc]initWithLatitude:currentLatitude longitude:currentLongitude];
        NSArray *customerAllList = [self.manager getAllCustomers];
        for (NSDictionary *c in customerAllList) {
            BOOL isGeolatNumber = [[c valueForKey:@"MSC_GEOLAT"] isKindOfClass:[NSNumber class]];
            BOOL isGeolonNumber = [[c valueForKey:@"MSC_GEOLON"] isKindOfClass:[NSNumber class]];
            if(isGeolatNumber && isGeolonNumber){
                
            //if ([c valueForKeyIsNumber:@"MSC_GEOLAT"] && [c valueForKeyIsNumber:@"MSC_GEOLON"]){
                double geolat = ((NSNumber*)[c valueForKey:@"MSC_GEOLAT"]).doubleValue;
                double geolon = ((NSNumber*)[c valueForKey:@"MSC_GEOLON"]).doubleValue;
                
                CLLocation *customerLocation = [[CLLocation alloc]initWithLatitude:geolat longitude:geolon];
                
                int distanceFromLocation = abs([customerLocation distanceFromLocation:currentLocation]/1000);
                //if (distanceFromLocation <= 3000){
                    NSMutableDictionary *cust = [NSMutableDictionary dictionaryWithDictionary:c];
                    [cust setObject:[NSNumber numberWithInt:distanceFromLocation] forKey:@"DISTANCE"];
                    [resultList addObject:cust];
                //}
            }
        }
        
    [resultList sortUsingDescriptors:[NSArray arrayWithObject:
                                      [NSSortDescriptor sortDescriptorWithKey:@"DISTANCE" ascending:YES]]];

    }
//    NSArray *subsetList;
//    if (resultList.count > 100){
//        subsetList = [resultList subarrayWithRange:NSMakeRange(0, 100)];
//    }else{
//        subsetList = resultList;
//    }
    //NSLog(@"count:%d\nvalues:%@",resultList.count,resultList);
    NSString *result = [Util convertNSArrayToJSON: resultList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)addFavorite:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"addFavorite called");
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)removeFavorite:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"removeFavorite called");
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getFavoriteList:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"getFavoriteList called");
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
