//
//  ProductMBOPlugIn.m
//  ERPSales
//
//  Created by Alexander Ilg on 5/11/13.
//
//

#import "ProductMBOPlugin.h"
#import "SalesPlusERPSalesPlus_SuperUserDB.h"
#import "SalesPlusProduct.h"
#import "SalesPlusProductText.h"
#import "SUPQuery.h"
#import "SUPQueryResultSet.h"
#import "SUPDataValue.h"
#import "SUPAttributeTest.h"
#import "SUPCompositeTest.h"
#import "SUPDataValueList.h"
#import "ProductManager.h"
#import "Util.h"

@implementation ProductMBOPlugIn

- (void)pluginInitialize{
    self.manager = [ProductManager defaultManager];
    categoryArguments = [[NSMutableArray alloc] initWithCapacity:0];
}

- (void)getProductList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getProductList called");
    int skip = 0;
    int take = 100;
    if (command.arguments.count == 2){
        skip = [[command.arguments objectAtIndex:0] integerValue];
        take = [[command.arguments objectAtIndex:1] integerValue];
    }
    NSArray *products = [self.manager getProductListFrom:skip To:take];
    NSString *result = [Util convertNSArrayToJSON: products];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

}

- (void)getProductDetails:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"getProductDetails called");
    
    // get the matnr (product number) from PhoneGap
    NSString* matnr = [command argumentAtIndex:0];
    
    NSDictionary *product = [self.manager productDetailsForMATNR:matnr];
    NSString *result = [Util convertNSDictionaryToJSON: product];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getCategories:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"getCategories called");
    NSString* categoryId;
    NSString* categoryName;
    NSMutableArray *categoryList = [NSMutableArray array];
    NSMutableArray *arguments;
    if (command.arguments.count >= 2){
        categoryName = [command.arguments objectAtIndex:0];
        categoryId = [command.arguments objectAtIndex:1];
        arguments = [[NSMutableArray alloc] initWithObjects:categoryName, categoryId, nil];
        categoryList = [self.manager getSubCategoriesForCategoryName:categoryName withId:categoryId getChildren:YES];
    }else{
        [categoryArguments removeAllObjects];
        categoryList = [self.manager getTopLevelCategories];
        //        self.manager.updateProductCount = YES;
        //        [self.manager getTopLevelCategories];
        //        self.manager.updateProductCount = NO;
        if (categoryList.count == 1){
            //there is only one top level category drill to its sub categories right away
            NSDictionary *values = [categoryList objectAtIndex:0];
            categoryName = [values objectForKey:@"CATALOGUENAME"];
            categoryId = [values objectForKey:@"CATEGORYID"];
            arguments = [[NSMutableArray alloc] initWithObjects:categoryName, categoryId, nil];
            categoryList = [self.manager getSubCategoriesForCategoryName:categoryName withId:categoryId getChildren:YES];
        }else{
            categoryId = [command.arguments objectAtIndex:0];
            arguments = [[NSMutableArray alloc] initWithObjects: categoryId, nil];
        }
    }
//    for (int i=0; i < categoryList.count; i++) {
//        NSDictionary *c = [categoryList objectAtIndex:i];
//        if ([[c objectForKey:@"CATEGORYDESCRIPTION"] isEqualToString:@"Root"] == NO){
//            [c setValue:[NSString stringWithFormat:@"%@-%@",[c objectForKey:@"HASPRODUCT"],[c objectForKey:@"CATEGORYDESCRIPTION"] ] forKey:@"CATEGORYDESCRIPTION"];
//        }
//    }
    [categoryArguments addObject:arguments];
    NSString *result = [Util convertNSArrayToJSON: categoryList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getProductListByCategory:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"getProductListByCategory called");
    NSString* categoryId;
    NSString* categoryName;
    NSArray *categoryList;
    if (command.arguments.count > 0){
        categoryId = [command.arguments objectAtIndex:0];
        categoryList = [self.manager getProductListForCategoryId:categoryId withDetails:YES];
    }
    [categoryArguments addObject:command];
    NSString *result = [Util convertNSArrayToJSON: categoryList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getParentCategory:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"getBackCategory called");
    //this LIFO structure remove the last item as it represent the current list of categires or product list for category
    if (categoryArguments.count > 1){
        [categoryArguments removeObjectAtIndex:categoryArguments.count-1];
        NSMutableArray *argument = [categoryArguments objectAtIndex:categoryArguments.count-1];
        NSString* categoryId;
        NSString* categoryName;
        NSMutableArray *categoryList = [NSMutableArray array];
        if (argument.count >= 2){
            categoryName = [argument objectAtIndex:0];
            categoryId = [argument objectAtIndex:1];
            categoryList = [self.manager getSubCategoriesForCategoryName:categoryName withId:categoryId getChildren:YES];
        }else{
            categoryList = [self.manager getTopLevelCategories];
        }
//        for (int i=0; i < categoryList.count; i++) {
//            NSDictionary *c = [categoryList objectAtIndex:i];
//            //int counter =[self.manager getProductForCategoryName:[c objectForKey:@"CATALOGUENAME"] withId:[c objectForKey:@"CATEGORYID"]];
//            //[c setValue:[NSString stringWithFormat:@"%d",counter] forKey:@"HASPRODUCT"];
//            [c setValue:[NSString stringWithFormat:@"%@-%@",[c objectForKey:@"HASPRODUCT"],[c objectForKey:@"CATEGORYDESCRIPTION"] ] forKey:@"CATEGORYDESCRIPTION"];
//        }
        NSString *result = [Util convertNSArrayToJSON: categoryList];
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)addFavorite:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"addFavorite called");
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)removeFavorite:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"removeFavorite called");
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getFavoriteList:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"getFavoriteList called");
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)searchProduct:(CDVInvokedUrlCommand *)command{
    MBOLog(@"searchProduct called");
    NSString *sCriteria =  [command argumentAtIndex:0];
    NSArray *products = [self.manager searchProductsWithCriteria:sCriteria];
    NSString *result = [Util convertNSArrayToJSON: products];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)isProductLocal:(CDVInvokedUrlCommand *)command{
    MBOLog(@"isProductLocal called");
    NSString* matnr = [command argumentAtIndex:0];
    NSString *found = [self.manager isProductLocal:matnr];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:found];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
}

@end
