
var ppb = 0
var gallery_indicator = 0;
var generalBut_indicator = 0;
var gallery_inDetails_mode = 0;
var current_username = "";
var current_password ="";
function getUserCredentials(){
	cordova.exec(storeCredentials, execFailed, 'Login', 'getCurrentUserCredentials', ['']);	

	function storeCredentials(param){
		var result = eval('(' + param + ')');
		current_username = result.username;
		current_password = result.password;
	}	
}



//Indicate if product from the left list is selected:
var selectedProduct = false;

var getParam = getParams();

function recordReminder(doc_number, doc_date, activity_name, activity_type,cust_number){
	var resubRecord = "";

	doc_date = doc_date.slice(4,6) + "/" + doc_date.slice(6,8) + "/" + doc_date.slice(0,4)

	//set active product

	// resubRecord += '<li data-doc_number="' + doc_number + '" data-cust_number="' + cust_number  +'" ><div class="arrow group"><div class="cart"><div class="products_count">cart_width</div><div class="cart_icon"></div></div>';

	// resubRecord += '<span class="name sale_activity1">'
	// resubRecord += activity_name;
	// resubRecord += '</span>';


	// resubRecord += '<span class="number">';
	// resubRecord += doc_number;
	// resubRecord += '</span><span class="number" data-doc_number="' + doc_number + '">'
	// resubRecord += doc_date;
	// resubRecord += '</span>';

	// resubRecord += '<span class="number">'
	// resubRecord += activity_type;
	// resubRecord += '</span>';


	// resubRecord +='</div></li>';


	return resubRecord;
}

function showResults(tableHTML){
	$("#reminderDtlsBodyHolder").html("");
	$("#reminderDtlsBodyHolder").html(tableHTML);
	$("#reminderDtlsTable").trigger("update");	
	$("#reminderDtlsBodyHolder tr").on("click", function(){
	// Navigate to Sales Activities or Sales Document.
		var docu_number = "";
		var customer_num = "";
		docu_number = $(this).data("doc_number");
		customer_num = $(this).data("cust_number");
			
		var selecTab = $('#aside_nav_reminders a[class="active"]').attr('id');
		if (selecTab == "fltr_activities")
		{
			//TODO
			// Selected tab is activities Navigate to Activity
			if ((docu_number != "") && (customer_num != "")) 
				window.location = "sales_activities.html?uid=" + customer_num + "&salesActivityDocNumber=" + docu_number + "&fromReminder=true" ;
		}
		else
		{
			// Selected tab is document, Navigate to customer

			window.location = "customers.html?salesAct_customerid=" + customer_num + "&salesOrderDocNumberReminder=" + docu_number + "&fromReminder=true" ;
		}
	});
}

function fireReminderListAct(param) 
{
	var result = eval('(' + param + ')');
	var result_html = "";
	var salesActCount = 0;
	var loopCount = 0;
	var tableHTML = "";

	for(i = 0; i < result.length; i++)
		if (result[i]["RESUB_TYP"] == "1")
			salesActCount++;

	for(i = 0; i < result.length; i++){
		if (result[i]["RESUB_TYP"] == "1") {
			loopCount++;

			//TODO BUILD IN THIS FUNCTION THE TABLE
			getActDetails(result , i , function(result, i, salesActParam){

				var resultSalesAct = eval('(' + salesActParam + ')');
				if(salesActParam.toString() == "{}"){
                	return;
            	}
				if((typeof resultSalesAct["PARTN_ID"] != 'undefined' && resultSalesAct["PARTN_ID"] != "" ) && (typeof resultSalesAct["ACTIVITY_TYPE_TEXT"] != 'undefined' && resultSalesAct["ACTIVITY_TYPE_TEXT"] != "" ))
				{				
					var date = formatDate(result[i]["RESUB_DATE"]);//.slice(4,6) + "/" + result[i]["RESUB_DATE"].slice(6,8) + "/" +result[i]["RESUB_DATE"].slice(0,4) ;
					var from_date = formatDate(resultSalesAct["FROM_DATE"]);//.slice(4,6) + "/" + resultSalesAct["FROM_DATE"].slice(6,8) + "/" +resultSalesAct["FROM_DATE"].slice(0,4) ;
					tableHTML += "<tr data-doc_number=" + result[i]["VBELN"] + " data-cust_number="+resultSalesAct["PARTN_ID"] +" ><td><div class='dtlsCustNo'>" + resultSalesAct["PARTN_ID"] + "</div></td>" +
								"<td><div class='dtlsDocNum'></div>" + result[i]["VBELN"] + "</td>" +
								"<td><div class="+ resultSalesAct["ACTIVITY_TYPE_TEXT"] +">" + resultSalesAct["ACTIVITY_TYPE_TEXT"] + "</div></td>" +
								"<td><div class="+ result[i]["UNAME"] +">" + result[i]["UNAME"] + "</div></td>" +
								"<td><div class="+ resultSalesAct["FROM_DATE"] +">" + from_date + "</div></td>" +
								"<td><div class="+ date +">" + date + "</div></td></tr>";					
				}
				$('#loadingBar').hide();
						
				if(salesActCount == loopCount)
					showResults(tableHTML);

			});
			
		}

	}
	if(salesActCount == 0)
		showResults("");

	$("#pageAside ul#resub_activities").off("click", "li");
	
	$("#pageAside ul#resub_activities").on("click", "li", function(event, target) {
			
			$("#pageAside ul#resub_activities li").removeClass("active")
			$(this).addClass("active");

			var document_number = $(this).data("doc_number");
            getReminderDetails(document_number,resub_TYP_Act);
	});
		
	
	$('#loadingBar').hide();

}

function getActDetails(result,i,callback)
{
	cordova.exec(function(salesActParam){
				callback(result,i,salesActParam);

			}, execFailed, 'SalesActivity', 'getSalesActivityDetails', [result[i]["VBELN"]]);		
}

function getOrdDetails(result,i,callback)
{
	var result = result;
	var i = i;
	var callback = callback;
	cordova.exec(checkSuccess, execFailed, 'OpenQuotation', 'isOpenQuotation', [result[i]["VBELN"]]);
	function checkSuccess(answer){ 
		var open_Q;
		(answer=="X") ? open_Q = 1 : open_Q = 0;
		getSalesOrderDetails(result, i, callback, open_Q);
	}
}

function getSalesOrderDetails(result,i,callback, openQuot)
{
	if(openQuot == 1){
		cordova.exec(fireSalesOrderDetails, execFailed, 'OpenQuotation', 'getOpenQuotationDetails', [result[i]["VBELN"]]);
	}
	else{
		cordova.exec(fireSalesOrderDetails, execFailed, 'Order', 'getSalesOrderDetails', [result[i]["VBELN"]]);
	}

	function fireSalesOrderDetails(salesDocParam)
	{
		callback(result,i,salesDocParam);
	}
}






function fireReminderListDoc(param) 
{
	var result = eval('(' + param + ')');
	var result_html = "";

	var salesDocCount = 0;
	var loopCount = 0;
	var tableHTML = "";

	for(i = 0; i < result.length; i++)
		if (result[i]["RESUB_TYP"] == "0")
			salesDocCount++;

	for(i = 0; i < result.length; i++){

		if (result[i]["RESUB_TYP"] == "0") {
			loopCount++;
			var doc_number = result[i]["VBELN"];
			var resub_date = result[i]["RESUB_DATE"];		


			getOrdDetails(result , i , function(result, i, salesDocsParam){

				var resultSalesDoc = eval('(' + salesDocsParam + ')');
				if(salesDocsParam.toString() == "{}"){
                	return;
            	}
				var date = formatDate(result[i]["RESUB_DATE"]);
				var valid_from_date = formatDate(resultSalesDoc.Header["REQ_DATE_H"]);
				if((typeof resultSalesDoc.Header["SOLD_TO"] != 'undefined' && resultSalesDoc.Header["SOLD_TO"] !="" ) && (typeof resultSalesDoc.Header["DOC_TYPE"] != 'undefined' && resultSalesDoc.Header["DOC_TYPE"] != "" ))
				{
					tableHTML += "<tr data-doc_number=" + result[i]["VBELN"] + " data-cust_number="+resultSalesDoc.Header["SOLD_TO"]+" ><td><div class="+resultSalesDoc.Header["SOLD_TO"]+">" + resultSalesDoc.Header["SOLD_TO"] + "</div></td>" +
								"<td><div class=" + result[i]["VBELN"] + "></div>" + result[i]["VBELN"] + "</td>" +
								"<td><div class="+ resultSalesDoc.Header["DOC_TYPE"] +">"+ resultSalesDoc.Header["DOC_TYPE"] + "</div></td>" +
								"<td><div class="+ result[i]["UNAME"] +">" + result[i]["UNAME"] + "</div></td>" +
								"<td><div class="+ resultSalesDoc.Header["QT_VALID_F"] +">" + valid_from_date + "</div></td>" + 
								"<td><div class="+ date +">" + date + "</div></td></tr>";						

				}
				$('#loadingBar').hide();
					
				if(salesDocCount == loopCount)
					showResults(tableHTML);
			});
			

		}
	}
	if(salesDocCount == 0)
		showResults("");
	// function showResults(){
	// 	$("#reminderDtlsBodyHolder").html(tableHTML);
	// }

	// $("#pageAside ul#resub_documents").off("click", "li");
	// 	$("#pageAside ul#resub_documents").on("click", "li", function(event, target) {
			
	// 		$("#pageAside ul#resub_documents li").removeClass("active")
	// 		$(this).addClass("active");

	// 		//$("#pageContent").fadeIn();
	// 		var document_number = $(this).data("doc_number");
 //            getReminderDetails(document_number,resub_TYP_Doc);
	// });

}

function getReminderListAct(){

	cordova.exec(fireReminderListAct, execFailed, 'Resubmission', 'getResubList', [current_username]);	
	$('#fltr_activities').addClass('active');
}

function getReminderListDoc(){
	cordova.exec(fireReminderListDoc, execFailed, 'Resubmission', 'getResubList', [current_username]);	
}

//Product Details:
function getProductDetails(productId, onlineCheck){
	//Show Content:
	
}


$(function(){

	document.addEventListener("deviceready", getUserCredentials, false);
	setTimeout(function(){ document.addEventListener("deviceready", getReminderListAct, false); }, 300 );



	
	
	//aside_nav_reminders
	var categories_result;
	$('#aside_nav_reminders a').on("click", function(){
		$("#pageAside").scrollTop(0);
		
		$('#aside_nav_reminders a').removeAttr('class');
		$(this).addClass('active');

		$('#reminderDetails').fadeOut();

		// $('#dtlsDocNum').text("");
		// $('#dtlsCreatedBy').text("");
		// $('#dtlsReminderDate').text("");
		$('#reminderDetails').fadeIn();
		
		switch($(this).attr('id')){
			case 'fltr_activities':
				//Hide General Button:
				//$("#general").hide();
				//Remove Active:
				$("#pageAside").off("scroll");
				
				$('#resub_activities li, #resub_documents li').removeClass("active");


				$('#resub_documents').fadeOut(function(){
					$('#resub_activities').fadeIn();
					
				});

				getReminderListAct();

				
				return false;
				break;

			case 'fltr_documents':
				//if content is hidden:
				$("#pageContent").fadeIn();

				//Hide General Button:
				$("#general").hide();
				//Remove Active:
				// $('#resub_activities li, #resub_documents li').removeClass("active");


				// $('#resub_activities').fadeOut(function(){
				// 	//$('.back_button').fadeIn();
				// 	$('#resub_documents').fadeIn();
				// });
				
				// $("#pageAside ul#resub_documents").off("click", "li");
				// $("#pageAside ul#resub_documents").on("click", "li", function(){
				// 	$("#pageAside ul#resub_documents li").removeClass("active");

				// });
				
				getReminderListDoc();

				//Add Active:
				return false;
				break;
		}
	});

	//Catch when the user is scrolled to bottom:
    $("#pageAside").on("scroll", function(){

    	
    });

});


