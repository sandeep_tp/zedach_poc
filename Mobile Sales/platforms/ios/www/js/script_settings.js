var currentTimeVar = new Date();
var curyr = parseInt(currentTimeVar.getFullYear());
var lastyr = curyr-1;
var lastyr2 = curyr-2;
var lastyr3 = curyr-3;
var currentLang;
var langset = 0;
var currentDatePattern;
var currentNumberDecimal;
var currentNumberGrouping;
var lang;
var gv_farm_id = "pmbs-dev-new"; //  "pmbs-dev-new" "pmbs-q-new"
var gv_synch_farm_id = "prbs-dev-new"; //  "prbs-dev-new" "prbs-q-new"
var lang_en = {
	"filterMessage1": "Filter not set. Please provide any input!",
	"filterSA": [
		"Status",
		"Result"
	],
	"email_contact": "Contact",
	"applyText": "Apply",
	"missingSD": "Sales Document unavailable",
	"syncLogQuotMBO": "Quotation Sync completed",
	"repText": "Call Online Reporting",
	"noNews": "No News available!",
	"ProductDetailsTable": [
    	"Material group",
    	"Discount group",
    	"Condition group",
    	"Gross Sales Price",
    	"Stock-%",
    	"Material Status",
    	"Plant Mat. St.",
    	"Plant",
    	"Leg Gross price",
    	"Leg-%",
    	"x-distr.Chain Status",
    	"Dchain-spez. Status",
    	"ATP-Stock",
    	"Moving price",
    	"CPU",
    	"CU",
    	"l. rep. Price",
    	"penu. rep. Price",
    ],
	"links_word": "Links",
	"link_sls_act": "Sales Activity",
	"gps_err": "No GPS",
	"no_data": "No Data",
	"internet_err": "No Internet Connection!",
	"userPass_error1": "Please fill out your connection settings first.",
	"userPass_error2": "Please specify a user name and password.",
	"userPass_error3": "Username and/or password are incorrect.",
	"userPass_error4": "Device Registration failed.",
	"userPass_error5": "Synchronization failure.",
    "userPass_error6": "Plugin initialization failure.",
	"email_error": "Please provide a valid e-mail adress.",
	"phone_error": "Please provide a valid phone number.<br>This may contain numbers and following signs: \"+\", \"-\", \" \"",
	"fax_error": "Please provide a valid fax number.<br>This may contain numbers and following signs: \"+\", \"-\", \" \"",
	"gr_fix" : "Goods Received:",
	"used_frameworks": "Frameworks in use",
	"noContact": "No Contact!",
	"gr_unfix" : "Sold (car)",
	"created_by": "Created By: ",
	"visit_report": "Visit Report ",
	"send_mail": "Send E-Mail",
	"create_follow_up": "Create Follow-Up",
	"available_stock": "Available Stock in close by Plants",
	"add_price_details": "Price Details",
	"succ_saved_sd": "You have successfully saved a Sales Document!<br>Please don't forget to release!",
	"succ_add_reminder": "You have successfully added reminder",
	"succ_del_reminder": "You have deleted the reminder",
	"succ_update_sd": "You have successfully saved a Sales Document!",
	"succ_created_sd": "You have successfully created a Sales Document!",
	"select_date_note" : "Date and Free Text are mandatory fields!",
	"please_select_sat": "Please select Sales Activity Type!",
	"createContact_validation": "First name and last name are mandatory fields!",
	"zero_NA" : "Zero quantity not allowed. Please maintain quantity",
	"editContact_validation": "First name and last name are mandatory fields!",
	"freeProduct_message": "This is a product doesn't have a price. It can be only added to the columns FG (car) and FG (stock).",
	"createOrder_dateError": "The Delivery Date should not be earlier than today!",
	"createOrder_AUFSDError": "It is not possible to create an order, return or exchange for this customer since the central order block is active!",
	"customer_maintenance_dialog_validation_title": "Values that are not maintained",
	"customer_maintenance_dialog_validation_text": "Fax number or e-mail address for this customer are not maintained. Do you want to add them now?",
	"customer_maintenance_dialog_tables_text": "In which table you want to add this product?",
	"customer_maintenance_dialog_validation_yes": "Yes",
	"customer_maintenance_dialog_validation_no": "No",
	"salesActivity_releaseCheck": "There is a Sales Order/Exchange/Return that's not released for this Sales Activity!",
	"release": "Release",
	"here": "You are here!",
	"createExchange_dialogProduct": "Choose in which table you want this product",
	"goods_given": "Goods given away",
	"goods_received": "Goods received",

	"saved_sact" : "You have successfully saved a sales activity<br>Please don't forget to release!",
	"deleted_sact" : "You have successfully deleted a sales activity",
	"saved_sorder" : "You have successfully saved a sales order",
	"saved_ret" : "You have successfully saved a return",
	"saved_exch" : "You have successfully saved an exchange",
	"edit_sales_act" : "Edit Sales Activity",
	"del_status" : "Delivery Status",
	"confirm_title": "Confirm this action?",

	"productPopUp_title": "Product Details",

	"activity_salesOrder": "Sales Order", //remove!
	"activity_return": "Return", //remove!
	"activity_exchange": "Exchange", //remove!
	"NoContacts1": "No Contacts exist for Customer",
	"NoContacts2": "Please create a new one!",
	"no_contact": "No Contact Available!",
	"CSOS": "Continue Search on Server...",
	"AUFSD": "Order Lock",
	"LIFSD": "Delivery Lock",
	"KLIMK": "Limit reached",
	"Login_heading": "Login",
	"Username_title": "Username",
	"Pass_title": "Pass",
	"Login_button": "Login",
	"Settings_button": "Settings",
	"home_customersTitle": "Customers Around Me",
	"home_productsTitle": "Reminders",
	"home_cap_start": "Capture Start Position",
	"home_cap_end": "Capture End Position", //?????
	"home_cap_start_end_manual": "Capture Start / End Manually ", //?????
    "normal" : "Normal",
    "fixed" : "Fix",
    "start"	: "Start",
    "end"	: "End",
    "reports_table": [
    	"Title",
    	"Description"
    ],
    "HomeDialog_title": "Set Location Manually",
    "HomeDialog_error": "You need to fill all fields!",
    "HomeScreenDialog": [
    	"Date",
    	"Type",
    	"House Number",
    	"Street",
    	"City",
    	"Postal Code",
    	"Country"
    ],
    "StartEndLocation": [
    	"Start Location",
    	"End Location"
    ],
    "stock_plants_head": [
    	"Plant",
    	"Stock",
    	"Unit"
    ],
    "add_price_head": [
    	"Activ",
    	"Kondty",
    	"Description",
    	"Amount",
    	"Currency",
    	"per",
    	"Unit",
    	"Value",
    	"Currency",
    ],
	"mainMenu": [
		"Around me",
		"Customers",
		"Products",
		"Sales Activities",
		"Reports",
		"Reminders"
	],
	"downMenu": [
		"Header 1",
		"Header 2",
		"Contacts",
		"Sales Documents",
		"Sales Activities",
		"Reports"
	],
	"salesOrderBottomMenu": [
		"Header",
		"Article"
	],
	"quickMenu": [
		"Edit Customer",
		"Create Contact",
		"Edit Contact",
		"Delete Contact",
		"Create Sales Activity (Customer)",
		"Create Sales Activity (Quotation)"
	],
	"AroundMe_zoom": [
		"1 km",
		"10 km",
		"30 km",
		"60 km",
		"100+ km"
	],
	"customer_details_top": [
		"T:",
		"F:",
		"E:",
		"W:"
	],
	"customer_header1": [
		"Terms of Payment",
		"Sales Representative",
		"Industry",
		"Customer Group",
		"Price Group",
		"Sales "+curyr,
		"Sales "+lastyr,
		"Sales "+lastyr2,
		"Sales "+lastyr3,
		"Customer sales potential",
		"No of Employees"
	],
	"customer_header2": [
		"Credit Limit",
		"Credit Limit Used",
		"Stocks (obilgatory)",
		"Stocks (voluntary)",
		"Funds",
		"Cumulated Bonus",
		"Scale of Bonus",
		"Sales qualifying for bonus",
		"Distance to next scale"
	],
	"personalInfo": [
		""
	],
	"sync_buttons": [
		"Synchronize",
		"Synchronize Quotations",
		"Synchronization Log",
	],
	"downMenu_products": [
		"General",
		"Reports"
	],
	"customer_contact_details": [
		"Telephone",
		"FAX",
		"Mobile",
		"E-mail",
		"Notes",
		"Hobby 1",
		"Hobby 2",
		"Birthday",
		"Amount of Children"
	],
	"contactTable": [
		"Name",
		"Function",
		"Phone",
		"Mobile",
		"E-mail"
	],
	"ordersTable": [
		"Division",
		"Dist. Channel",
		"Sales Org.",
		"Customer Number",
		"Date",
		"Status",
		"Order No."
	],
	"SAordersTable": [
		"Title",
		"Type",
		"Date",
		"Document No",
		"PO No"
	],
	/*"orderDetails": [
		"Position",
		"Material",
		"Order Quantity",
		"UOM",
		"Short text"
	],*/
	"orderDetailsTable": [
		"No",
		"Product",
		"Quant.",
		"SU",
		"Net Price",
		"Discount",
		"Total"
	],
	"total_orderDetailsTable": "Total:",
	"customer_SalesDocuments_table": [
		"SD No",
		"Order No",
		"Value",
		"Reminder",
		"Valid Until"
	],
	"ordersTotals": [
		"Total - Price:",
		"Total - Items:"
	],
	"returnsTotals": [
		"KVP - Sold (car):",
		"KVP - FG (car):",
		"Total - Price:",
		"Total - Items:"
	],
	"settings_title": "Settings",
    "SettingsTable":[
        "Settings",
        "Protocol",
        "Server",
        "Synchronization Port",
        "Registration Port",
        "Synchronization Farm ID",
        "Registration Farm ID",
    ],
    "cancelButton": "Cancel",

    "regMessage": "Registration started",
	"startSyncMessage": "Synchronization started",

	"SalesOrderDetails_titles":[
		"Sales Document Number:",
		"Order Type:",
		"Customer:",
		"Purchase Order Number:",
		"Order Date:",
		"Delivery Date:",
		"Validity Date:",
		"Reminder Date:"

	],
	"createSalesOrder_header":[
		"Customer:",
		"Purchase Date:",
		"Sales Document Type:",
		"Plant:",
		"Customer Sales Order Number",
		"Req. Delivery Date",
		"Valid To",
		"Note 1",
		"Note 2"
	],
	"deleteSalesDoc_title": "Confirm this action?",
	"addReminderDate": "Add Reminder Date",
	"changeSalesDocValidity": "Change Validity To",
	"deleteSalesDoc_msg": "Are you sure you want to delete the current sales document?",
	"deleteContact_msg": "Are you sure you want to delete this contact?",
	"deleteAll_msg": "Are you sure you want to delete all Synch Log entries?",
	"deleteSA_msg": "Are you sure you want to delete the current sales activity?",
	"confirm": "Confirm",
	"captureSignature_button": "Capture Signature",
	"captureAnotherSignature": "Capture Again",
	"signature_confirm": "Confirm",
	"signature_title": "Capture Signature",
	"createOrderTable": [
		"Material No",
		"Product",
		"Quantity",
		"SU",
		"Price Per Unit",
		"Discount",
		"Total Price"
		
	],
	"createReturnsTable": [
		"Product",
		"Sold (car)",
		" FG  (car)",
		"Action",
		"Pack. per Bundle",
		"KVP"
	],
	"createExchangesTable1": [
		"Product",
		"Goods given away",
		"pack. per Bundle",
		"KVP"
	],
	"createExchangesTable2": [
		"Product",
		"Goods received",
		"Pack. per Bundle",
		"KVP"
	],
	"addNewContact_heading": "Add New Contact",
	"addNewContact": [
		"First Name",
		"Last Name",
		"Function",
		"Telephone",
		"FAX",
		"Mobile",
		"E-mail",
		"Notes",
		"Hobby 1",
		"Hobby 2",
		"Birthday",
		"Amount of Children"
	],
	"editContact_heading": "Edit Contact",
	"editContact": [
		"First Name",
		"Last Name",
		"Function",
		"Telephone",
		"FAX",
		"Mobile",
		"E-mail",
		"Notes",
		"Hobby 1",
		"Hobby 2",
		"Birthday",
		"Amount of Children"
	],
	"editCustomer_heading": "Edit Customer",
	"editCustomer": [
		"Telephone Nr",
		"FAX Nr",
		"E-mail",
		"WWW",
		"No of Employees",
		"Year",
		"Industry",
		"Turnover Value",
		"Turnover Year"
		
	],
	"searchBox": "Search...",
	"editButton": "Edit",
	"addPersonButton": "Add New",
	"deleteButton": "Delete",
	"deleteButtonAllSyncLog": "Delete old entries",
	"clearButton": "Clear",
	"saveButton": "Save",
	"productTableHeadings": [
		"General Data",
		"Units of Measures",
		"Attachments"
	],
	"header2_tableheadz": [
		"Credit Information",
		"Member Information",
		"Bonus Information"
	],
	"product_General": [
		"Base UoM",
		"Sales Unit",
		"Gross Weight",
		"Stock",
		"Gross Price",
		"Plant"
	],
	"backButton": "Back",
	"product_downNav": [
		"Categories",
		"All products"
	],

	"customer_downNav": [
		"My Customers",
		"All Customers"
	],

	"activities_downNav": [
		"My Activities",
		"All Activities"
	],

	"reminder_downNav": [
		"Activities",
		"Offers"
	],

	"reminder_details": [
		"Customer No.",
		"Document/Activity Number",
		"Type",
		"Created By",
		"Creation Date",
		"Reminder Date"
	],

	"createOrderDownNav": [
		"Last Products",
		"Categories"
	],
	"salesActivities": [
		"Date",
		"Start Time",
		"End Time",
		"Free Text"
	],
	"downMenuSalesActivities": [
		"General",
		"Sales Document",
		"Ref Sales Document",
		//"Shelf Audit"
	],
	"quickMenu_sales_activities": [
		"Create Sales Activity"
		/*
		,"Delete"
		*/
	],
	"sale_activity_add": [
		"Customer",
		"Contact",
		"Type",
		"Result",
		"Date",
		"Start Time",
		"End Time",
		"Free Text"
	],
	"syncLogHeader2": [
		"Date",
		"Time",
		"Entity",
		"Type",
		"Message"
	],
	"syncLogTypes": [
		"Contact",
		"Customer",
		"Sales Activity",
		"Sales Document"
	],
	"syncLogHeader1": "Synchronization Log",
	"sale_activity_add_header": "Add New Sales Activity: ",
	"sale_activity_contact": "Contact Name",
	"dropDownMenu_select": "Select",
	"dialog_title": "Change Quantity",
	"dialog_add": "Add",
	"dialog_cancel": "Cancel",

	"customAlert_title": "Messages",
	"customAlert_syncStart": "Synchronization started.",
	"customAlert_syncEnd": "Synchronization finished.",
	"customAlert_syncFail": "Synchronization failed.",
	"customAlert_noLocCustomer": "There is no location added for this customer.",
	"customAlert_noRouteCustomer": "There is no driving path between you and this customer.",
	"customAlert_createContactSuccess": "You have successfully created a contact!",
	"customAlert_updateContactSuccess": "You have successfully edited this contact!",
	"customAlert_deleteContactSuccess": "You have successfully deleted this contact!",
	"customAlert_updateCustomerSuccess": "You have successfully edited this customer!",
	"customAlert_createActSuccess": "You have successfully created a Sale Activity!",
	"customAlert_updateActSuccess": "You have successfully edited a Sale Activity!",
	"customAlert_releaseActSuccess": "You have successfully released a Sale Activity!",
	"customAlert_deleteActSuccess": "You have successfuly deleted Sale Activity!"
};


var lang_de = {
	"filterMessage1": "Filter ist nicht gesetzt. Bitte einen Wert zum Filtern setzen!",
	"filterSA": [
		"Status",
		"Result"
	],
	"email_contact": "Kontakt",
	"applyText": "Anwenden",
	"missingSD": "Vertriebsbeleg ist nicht verfügbar",
	"syncLogQuotMBO": "Auftrags-Sync beendet",
	"repText": "Aufruf Online Reporting",
	"noNews": "Keine Neuigkeiten verfügbar!",
	"ProductDetailsTable": [
    	"Warengruppe",
    	"Rabattgruppe",
    	"Konditionsgruppe",
    	"Brutto-VK",
    	"Lager-%",
    	"Materialstatus",
    	"Werks.Matst.",
    	"Werk",
    	"Streckenbrutto",
    	"Strecke-%",
    	"VTL-überg. Status",
    	"VTL-spez. Status",
    	"ATP Bestand",
    	"GLD",
    	"PE",
    	"PME",
    	"LEK Einh",
    	"VLEK Einh",
    ],
	"links_word": "Anhänge",
	"link_sls_act": "Kontakt",
	"gps_err": "Kein GPS-Signal",
	"no_data": "Keine Daten",
	"internet_err": "Keine Internet Verbindung!",
	"userPass_error1": "Bitte zuerst Einstellungen setzen.",
	"userPass_error2": "Bitte Benutzername und Passwort angeben.",
	"userPass_error3": "Benutzername und/oder Passwort falsch.",
	"userPass_error4": "Geräteregistrierung fehlgeschlagen.",
	"userPass_error5": "Synchronisation fehlgeschlagen.",
	"email_error": "Bitte eine gültige E-Mail Adresse angeben.",
	"phone_error": "Bitte eine gültige Telefonnummer angeben.<br>Diese kann aus Ziffern (0 - 9) und folgenden Zeichen bestehen:\"+\", \"-\", \" \"",
	"fax_error": "Bitte eine gültige Fax-Nummer angeben.<br>Diese kann aus Ziffern (0 - 9) und folgenden Zeichen bestehen:\"+\", \"-\", \" \"",
	"gr_fix" : "Erhaltene Ware:",
	"used_frameworks": "Verwendete Frameworks",
	"noContact": "Kein Ansprechpartner!",
	"gr_unfix" : "VL",
	"created_by": "Erstellt von: ",
	"visit_report": "Besuchsbericht ",
	"send_mail": "E-Mail senden",
	"create_follow_up": "Folgekontakt anlegen",
	"available_stock": "Bestandsübersicht in den benachbarten Werken",
	"add_price_details": "Preisübersicht",
	"succ_saved_sd": "Sie haben erfolgreich einen Vertriebsbeleg gespeichert!<br>Bitte an Freigabe denken!",
	"succ_add_reminder": "Einnerung erfolgreich erstellt",
	"succ_del_reminder": "Erinnerung gelöscht",
	"succ_created_sd": "Sie haben erfolgreich einen Vertriebsbeleg angelegt!<br>Bitte an Freigabe denken!",
	"succ_update_sd": "Vertriebsbeleg erfolgreich aktualisiert!",
	"select_date_note" : "Datum und Kurztext sind Pflichtfelder!",
	"please_select_sat": "Bitte Kontakttyp auswählen!",
	"createContact_validation": "Vorname und Nachname sind Pflichtfelder!",
	"zero_NA" : "Nullmengen nicht erlaubt. Bitte Mengen pflegen",
	"editContact_validation": "Vorname und Nachname sind Pflichtfelder!",
	"freeProduct_message": "Dieses Produkt besitzt keinen Preis. Es kann nur zu den Spalten 'NR VL' und 'NR ZL' hinzugefügt werden.",
	"createOrder_dateError": "Das gewünschte Lieferdatum darf nicht in der Vergangenheit liegen!",
	"createOrder_AUFSDError": "Es ist nicht möglich einen Auftrag für diesen Kunden zu erstellen, solange das Auftragsmenü aktiviert ist!",
	"customer_maintenance_dialog_validation_title": "Nicht beinhaltende Werte",
	"customer_maintenance_dialog_validation_text": "Fax oder E-Mail sind für diesen Kunden nicht beinhaltet. Wollen Sie diese jetzt hinzufügen?",
	"customer_maintenance_dialog_validation_yes": "Ja",
	"customer_maintenance_dialog_validation_no": "Nein",
	"salesActivity_releaseCheck": "Es gibt einen Auftrag, Rücksendung oder Umtausch der nicht für diesen Kontakt freigegeben ist!",
	"release": "Freigeben",
	"here": "Sie sind hier!",
	"saved_sact" : "Kontakt wurde erfolgreich gespeichert.<br>Bitte an Freigabe denken!",
	"deleted_sact" : "Sie haben erfolgreich diesen Kontakt gelöscht!",
	"saved_sorder" : "Auftrag wurde erfolgreich gespeichert.",
	"saved_ret" : "Retoure wurde erfolgreich gespeichert.",
	"saved_exch" : "Umtausch wurde erfolgreich gespeichert.",
	"edit_sales_act" : "Kontakt bearbeiten",
	"del_status" : "Lieferstatus",
	"confirm_title": "Bestätigung",
	"productPopUp_title": "Produkt Details",

	"createExchange_dialogProduct": "Bitte Zieltabelle wählen",
	"goods_given": "Abgegebene Ware",
	"goods_received": "Erhaltene Ware",
	
	"activity_salesOrder": "Auftrag", //remove!
	"activity_return": "Rücksendung", //remove!
	"activity_exchange": "Umtausch", //remove!

	"NoContacts1": "Kein Ansprechpartner für Kunde",
	"NoContacts2": "vorhanden. Bitte zuerst einen Ansprechpartner anlegen!",
	"no_contact": "Kein Ansprechpartner vorhanden!",
	"CSOS": "Suche auf Server fortsetzen...",
	"AUFSD": "Auftragssperre",
	"LIFSD": "Liefersperre",
	"KLIMK": "Limit erreicht",
	"Login_heading": "Login",
	"Username_title": "Benutzername",
	"Pass_title": "Passwort",
	"Login_button": "Login",
    "Settings_button": "Einstellungen",
	"home_customersTitle": "Kunden in der Nähe",  
	"home_productsTitle": "Wiedervorlagen",
	"home_cap_start": "Start erfassen",
	"home_cap_end": "Ziel erfassen",
	"home_cap_start_end_manual": "Start / Ziel manuell erfassen",
    "normal" : "Usual", //???
    "fixed" : "Fix", //????
    "start"	: "Start", 
    "end"	: "Ziel",
    "reports_table": [
    	"Titel",
    	"Beschreibung"
    ],
    "HomeDialog_title": "Start / Ziel manuell erfassen",
    "HomeDialog_error": "Bitte alle Pflichtfelder pflegen!",
    "HomeScreenDialog": [
    	"Datum",
    	"Typ",
    	"Nr.",
    	"Strasse",
    	"Stadt",
    	"PLZ",
    	"Land"
    ],
    "StartEndLocation": [
    	"Start-Lokation",
    	"Ziel-Lokation"
    ],
    "stock_plants_head": [
    	"Werk",
    	"Lagerbestand",
    	"Mengeneinheit"
    ],
    "add_price_head": [
    	"Aktiv",
    	"Kondart",
    	"Bezeichnung",
    	"Betrag",
    	"Währung",
    	"pro",
    	"ME",
    	"Wert",
    	"Währung",
    ],
	"mainMenu": [
		"Around me",
		"Kunden",
		"Produkte",
		"Kontakte",
		"Berichte",
		"Erinnerungen"
	],
	"downMenu": [
		"Kopfdaten 1",
		"Kopfdaten 2",
		"Ansp.partner",
		"Vertriebsbelege",
		"Kontakte",
		"Berichte"
	],
	"salesOrderBottomMenu": [
		"Kopfdaten",
		"Artikel"
	],
	"quickMenu": [
		//"Kunde erstellen",
		"Kunde bearbeiten",
		"Ansprechpartner erstellen",
		"Ansprechpartner bearbeiten",
		"Ansprechpartner löschen",
		"Kontakt erstellen (Kunde)",
		"Kontakt erstellen (Angebot)"
		//"Besuchsbericht erstellen",
		//"Auftrag erstellen",
		//"Return",
		//"Sales Activity"
	],
	"AroundMe_zoom": [
		"1 km",
		"10 km",
		"30 km",
		"60 km",
		"100+ km"
	],

	"customer_details_top": [
		"T:",
		"F:",
		"E:",
		"W:"
	],
	"customer_header1": [
		"Zahlungsbedingungen",
		"Vertriebsbeauftragter",
		"Branche",
		"Kundengruppe",
		"Konditionsgruppe",
		"Umsatz "+curyr,
		"Umsatz "+lastyr,
		"Umsatz "+lastyr2,
		"Umsatz "+lastyr3,
		"Potenzial",
		"Anzahl Mitarbeiter"
	],
	"customer_header2": [
		"Kreditlimit",
		"Ausschöpfungsgrad",
		"Anteile (pflicht)",
		"Anteile (freiwillig)",
		"Geschäftsguthaben",
		"Aktueller Bonus",
		"Bonusstaffel",
		"Bonusfähiger Umsatz",
		"Differenz zur nächten Staffel"
	],
	"personalInfo": [
		""
	],
	"sync_buttons": [
		"Synchronisieren",
		"Synchronisation Angebote",
		"Synchronisierungs-Log",
	],
	"downMenu_products": [
		"Kopfdaten",
		"Berichte"
	],
	"customer_contact_details": [
		"Telefon",
		"FAX",
		"Mobil",
		"E-Mail",
		"Notiz",
		"Hobby 1",
		"Hobby 2",
		"Geburtsdatum",
		"Kinderanzahl"
	],
	"contactTable": [
		/*
		"Vorname",
		"Nachname",
		*/
		"Name",
		"Funktion",
		"Telefon",
		"Mobil",
		"E-Mail"
	],

	"ordersTable": [
		"Sparte",
		"Vertriebsweg",
		"Verkaufsorg.",
		"Kundennummer",
		"Datum",
		"Status",
		"Auftragsnummer"
	],
	"SAordersTable": [
		"Titel",
		"Typ",
		"Datum",
		"Kontakt Nr",
		"Dokumenten Nr"
	],
	"orderDetailsTable": [
		"Material",
		"Produkt",
		"Menge",
		"ME",
		"Nettopreis",
		"Rabatt",
		"Gesamt"
	],

	"total_orderDetailsTable": "Gesamt:",

	"customer_SalesDocuments_table": [
		"Belegnr",
		"Best Nr",
		"Wert",
		"Erinnerung",
		"Gültig bis"
	],

	"ordersTotals": [
		"Gesamt - Preis:",
		"Gesamt - Artikel:"
	],
	"returnsTotals": [
		"KVP - VL:",
		"KVP - NR VL:",
		"Gesamt - Preis:",
		"Gesamt - Artikel:"
	],
	"settings_title": "Einstellungen",

    "SettingsTable":[
        "Einstellungen",
        "Protokoll",
        "Server",
        "Synchronisationsport",
        "Registrierungsport",
        "Synchronization Farm ID",
        "Registration Farm ID",
    ],
    "cancelButton": "Abbrechen",

    "regMessage": "Registrierung gestartet",
	"startSyncMessage": "Synchronisierung gestartet",

	"SalesOrderDetails_titles":[
		"Belegnummer:",
		"Auftragsart:",
		"Kunde:",
		"Bestellnummer:",
		"Bestelldatum:",
		"Lieferdatum:",
		"Gültigkeitsdatum:",
		"Erinnerungsdatum:"
	],

	"createSalesOrder_header":[
		"Kunde:",
		"Bestelldatum:",
		"Auftragsart:",
		"Werk:",
		"Bestellnummer:",
		"Lieferdatum:",
		"Gültig bis",
		"Notiz 1",
		"Notiz 2"
	],
	"deleteSalesDoc_title": "Bestätigung",
	"addReminderDate": "Erinnerung hinzufügen",
	"changeSalesDocValidity": "Gültigkeitsdatum hinzufügen",
	"deleteSalesDoc_msg": "Soll dieser Vertriebsbeleg gelöscht werden?",
	"deleteContact_msg": "Soll dieser Ansprechpartner gelöscht werden?",
	"deleteAll_msg": "Sollen wirklich alle Einträge gelöscht werden?",
	"deleteSA_msg": "Soll dieser Kontakt gelöscht werden?",
	"confirm": "Bestätigen",
	"captureSignature_button": "Signatur erfassen",
	"captureAnotherSignature": "Erneut erfassen",
	"signature_confirm": "Bestätigen",
	"signature_title": "Signatur erfassen",
	"createOrderTable": [
		"Material",
		"Produkt",
		"Menge",
		"ME",
		"Preis pro Einheit",
		"Rabatt",
		"Gesamt"
	],
	"createReturnsTable": [
		"Produkt",
		"VL",
		"NR VL",
		"Aktion",
		"Pack pro Gebinde",
		"KVP"
	],
	"createExchangesTable1": [
		"Produkt",
		"Abgegebene Ware",
		"Pack pro Gebinde",
		"KVP"
	],
	"createExchangesTable2": [
		"Produkt",
		"Erhaltene Ware",
		"Pack pro Gebinde",
		"KVP"
	],
	"addNewContact_heading": "Ansprechpartner hinzufügen",

	"addNewContact": [
		"Vorname",
		"Nachname",
		"Funktion",
		"Telefon",
		"FAX",
		"Mobil",
		"E-Mail",
		"Notiz",
		"Hobby 1",
		"Hobby 2",
		"Geburtsdatum",
		"Kinderanzahl"
	],

	"editContact_heading": "Ansprechpartner bearbeiten",

	"editContact": [
		"Vorname",
		"Nachname",
		"Funktion",
		"Telefon",
		"FAX",
		"Mobil",
		"E-Mail",
		"Notiz",
		"Hobby 1",
		"Hobby 2",
		"Geburtsdatum",
		"Kinderanzahl"
	],

	"editCustomer_heading": "Kunde bearbeiten",

	"editCustomer": [
		"Telefon",
		"Fax",
		"E-Mail",
		"WWW",
		"Anzahl Mitarbeiter",
		"Jahr",
		"Branche",
		"Potenzial Wert",
		"Potenzial Jahr"
		
	],

	"searchBox": "Suche...",
	"editButton": "Bearbeiten",
	"addPersonButton": "Ansprechpartner hinzufügen",
	"deleteButton": "Löschen",
	"deleteButtonAllSyncLog": "Alte Einträge löschen",
	"clearButton": "Löschen",
	"saveButton": "Speichern",
	"backButton": "Zurück",

	"productTableHeadings": [
		"Kopfdaten",
		"Alternative Mengeneinheiten",
		"Anhänge"
	],
	"header2_tableheadz": [
		"Kreditinformationen",
		"Mitglied Informationen",
		"Bonus Informationen"
	],
	"product_General": [
		"Mengeneinheit",
		"Verkausmengeneiheit",
		"Bruttogewicht",
		"Bestand",
		"Bruttopreis",
		"Werk"
	],

	"product_downNav": [
		"Kategorien",
		"Alle Produkte"
	],

	"customer_downNav": [
		"Meine Kunden",
		"Alle Kunden"
	],

	"activities_downNav": [
		"Meine Kontakte",
		"Alle Kontakte"
	],

	"reminder_downNav": [
		"Kontakte",
		"Angebote"
	],

	"reminder_details": [
		"Kunden Nr.",
		"Dokument/Kontakt Nr.",
		"Typ",
		"Erstellt durch",
		"Erstellungs- datum",
		"Erinnerungs- datum"
	],

	"createOrderDownNav": [
		"Letzte Produkte",
		"Kategorien"
	],

	"salesActivities": [
		"Datum",
		"Startzeit",
		"Endzeit",
		"Notiz"
	],

	"downMenuSalesActivities": [
		"Kopfdaten",
		"Vertriebsbeleg",
		"Ref Vertriebsbeleg",
	],

	"quickMenu_sales_activities": [
		"Erstellen",
		"Löschen"
	],

	"sale_activity_add": [
		"Kunde",
		"Ansprechpartner",
		"Typ",
		"Resultat",
		"Datum",
		"Startzeit",
		"Endzeit",
		"Kurztext"
	],
	"syncLogHeader2": [
		"Datum",
		"Zeit",
		"Entität",
		"Typ",
		"Meldung"
	],
	"syncLogTypes": [
		"Ansprechpartner",
		"Kunde",
		"Kontakt",
		"Vertriebsbeleg"
	],
	"syncLogHeader1": "Synchronisierungs-Log",
	"sale_activity_add_header": "Neuen Kontakt hinzufügen: ",
	"sale_activity_contact": "Ansprechpartner",
	"dropDownMenu_select": "Auswählen",
	"dialog_title": "Menge ändern",
	"dialog_add": "Hinzufügen",
	"dialog_cancel": "Abbrechen",

	"customAlert_title": "Meldung",
	"customAlert_syncStart": "Synchronisierung gestartet.",
	"customAlert_syncEnd": "Synchronisierung beendet.",
	"customAlert_syncFail": "Synchronisierung fehlgeschlagen.",
	"customAlert_noLocCustomer": "Es ist kein Ort für diesen Kunden hinterlegt.",
	"customAlert_noRouteCustomer": "Es gibt keine befahrbare Route zwischen Ihnen und dem Kunden.",
	"customAlert_createContactSuccess": "Sie haben erfolgreich einen Ansprechpartner erstellt!",
	"customAlert_updateContactSuccess": "Sie haben erfolgreich einen Ansprechpartner aktualisiert!",
	"customAlert_deleteContactSuccess": "Sie haben erfolgreich diesen Ansprechpartner gelöscht!",
	"customAlert_updateCustomerSuccess": "Sie haben erfolgreich einen Kunden aktualisiert!",
	"customAlert_createActSuccess": "Sie haben erfolgreich einen Kontakt erstellt!",
	"customAlert_updateActSuccess": "Sie haben erfolgreich einen Kontakt aktualisiert!",
	"customAlert_releaseActSuccess": "Sie haben erfolgreich einen Kontakt freigegeben!",
	"customAlert_deleteActSuccess": "Sie haben erfolgreich einen Kontakt gelöscht!"
};

function formatNumber (number) {

	var formattedNumber;
	var containsDecimal = number.indexOf(currentNumberDecimal);

	if (containsDecimal > 0) {

		var splitArray = number.split('.');

		var sepPart = Math.floor(splitArray[0]).toString().split('');
		var index = -3; 
	    while (sepPart.length + index > 0 ) { 
	        sepPart.splice(index, 0, currentNumberGrouping);              
	        index -= 4;
	    }
	    var sepNumber = sepPart.join('');

		formattedNumber = sepNumber + currentNumberDecimal + splitArray[1];

	} else {
		// todo
		formattedNumber = number;
	}

	return formattedNumber;
}

function setMessageText(param) {
	$("#login .warningMsg").text(param);
}

/* END Changes after Villiger Transport */

$(function(){

	//Set current language:
	currentLang = "en";
	lang = lang_en;

	
	function whereAmI() {
		var location = window.location.href.match(/([^/])+/g);
		location = location[location.length - 1].split("?");
		return location[0];
	}


	/* START Changes after Villiger Transport */
	document.addEventListener("deviceready", getUsersGlobalization, false);

	function getUsersGlobalization() {
  		navigator.globalization.getPreferredLanguage(successGetPreferredLanguage, errorGetPreferredLanguage);
  		navigator.globalization.getDatePattern(successGetDatePattern, errorGetDatePattern, {formatLength:'short', selector:'date'});
  		navigator.globalization.getNumberPattern(successGetNumberPattern, errorGetNumberPattern, {type:'decimal'});
  	}

  	function successGetDatePattern(datePattern) {
  		currentDatePattern = datePattern.pattern;

  		//Fire Table Sorters:
  		if ( $(".sortable_table").length > 0 ){
  			tableSorters();
  		}
  		
  	}

  	function tableSorters() {
  		//Add Parser for German Date Format:
		$.tablesorter.addParser({
			id: "GermanDate_format",
			is: function(s) {
		        return false;
		    },
		    format: function(s) {
		        s = s.toString();
		        var hit = s.match(/(\d{1,2})\.(\d{1,2})\.(\d{4})/);
		        if (hit && hit.length == 4) {
		            return hit[3] + hit[2] + hit[1];
		        } else {
		            return s;
		        }
		    },
		    type: "text"
		});


		// $.tablesorter.addParser({
		// 	id: "Reminder_format",
		// 	is: function(s) {
		//         return false;
		//     },
		//     format: function(s) {
		//     	debugger;
		//         // s = s.toString();
		//         // var hit = s.match(/(\d{1,2})\.(\d{1,2})\.(\d{4})/);
		//         // if (hit && hit.length == 4) {
		//         //     return hit[3] + hit[2] + hit[1];
		//         // } else {
		//         //     return s;
		//         // }
		//     },
		//     type: "text"
		// });
		
		var date_sort_settings;
	    switch(currentDatePattern){
	    	case germanDatePattern:
	            date_sort_settings = {
					sorter: "GermanDate_format"
				};
	            break;
	        case unitedKingdomDatePattern:
	        case frenchDatePattern:
	        	date_sort_settings = {
					dateFormat: "ddmmyyyy"
				};
	            break;
	        case unitedStatesDatePattern:
	       		date_sort_settings = {
					dateFormat: "mmddyyyy"
				};
	            break;
	        default:
	        	date_sort_settings = {
					dateFormat: "ddmmyyyy"
				};
	    }
	    //Sales Activities Table Sort:
		$("#sales_activities_table").tablesorter({
			headers: {
				0: {sorter: "text"},
				2: date_sort_settings
			}
		});
		// Reminders table sorter:
		$("#reminderDtlsTable").tablesorter({
			headers: {
				0: {sorter: "text"},
				1: {sorter: "text"},
				2: {sorter: "text"},
				3: {sorter: "text"},
				4: date_sort_settings,
				5: date_sort_settings
			}
		});
		$("#customers_salesDocuments_table").tablesorter({
			headers: {
				4: date_sort_settings
			}
		});
		$("#reports_table").tablesorter();

		// //Sales Activities Table Sort:
		// $("#customers_salesDocuments_table").tablesorter({
		// 	headers: {
		// 		0: {sorter: "text"},
		// 		4: {sorter: "Reminder_format"}
		// 	}
		// });


  	}

	function successGetNumberPattern(numberPattern) {
  		currentNumberDecimal = numberPattern.decimal;
  		currentNumberGrouping = numberPattern.grouping;
  	}

  	function successGetPreferredLanguage(language) {
    	currentLang = language.value.split('-')[0];
    	switch(currentLang){
			case "de":
				lang = lang_de;
				break;
			case "en":
				lang = lang_en;
				break;
			case "fr":
				lang = lang_fr;
				break;
			case "us":
				lang = lang_en;
				break;
		}
		langset = 1;
		setCurrentLangLabels();
	}



	function setCurrentLangLabels() {
		Number.prototype.formatMoney = function(places){
			var n = this;
			if(!isNaN(places)){
				var c = places;
			}
			else{
				var n_as_string = n.toString(),
					n_array_dot = n_as_string.split('.'),
					n_array_com = n_as_string.split(',');

				if(n_array_dot.length > 1 && n_array_com.length == 1){
					var c = n_array_dot[n_array_dot.length-1].length;
				}
				else if(n_array_com.length > 1 && n_array_dot.length == 1){
					var c = n_array_com[n_array_com.length-1].length;
				}else{
					var c = 0;
				}
			}
			if(currentLang == "de"){
				var decimals = ",";
				var thousands = ".";
			}
			else{
				var decimals = ".";
				var thousands = ",";
			}
			var c = isNaN(c = Math.abs(c)) ? 2 : c, 
		    decimals = decimals == undefined ? "," : decimals, 
		    thousands = thousands == undefined ? "." : thousands, 

		    s = n < 0 ? "-" : "", 
		    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
		    j = (j = i.length) > 3 ? j % 3 : 0;
		   return s + (j ? i.substr(0, j) + thousands : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (c ? decimals + Math.abs(n - i).toFixed(c).slice(2) : "");
		 };
		 String.prototype.formatMoney = function(places){
			if(this.indexOf(",") > -1 && this.indexOf(".") == -1){
				var n = this.replace(',', '.');
			}
			else{
				var n = this.replace(',', '');
			}
			if(!isNaN(places)){
				var c = places;
			}
			else{
				var n_as_string = n.toString(),
					n_array_dot = n_as_string.split('.'),
					n_array_com = n_as_string.split(',');

				if(n_array_dot.length > 1 && n_array_com.length == 1){
					var c = n_array_dot[n_array_dot.length-1].length;
				}
				else if(n_array_com.length > 1 && n_array_dot.length == 1){
					var c = n_array_com[n_array_com.length-1].length;
				}else{
					var c = 0;
				}
			}
			if(currentLang == "de"){
				var decimals = ",";
				var thousands = ".";
			}
			else{
				var decimals = ".";
				var thousands = ",";
			}
			var c = isNaN(c = Math.abs(c)) ? 2 : c, 
		    decimals = decimals == undefined ? "," : decimals, 
		    thousands = thousands == undefined ? "." : thousands, 

		    s = n < 0 ? "-" : "", 
		    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
		    j = (j = i.length) > 3 ? j % 3 : 0;
		   return s + (j ? i.substr(0, j) + thousands : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (c ? decimals + Math.abs(n - i).toFixed(c).slice(2) : "");
		 };
		//Loat the Main Menu from mainComponents/nav.html:
		$("#pageHeader").load("mainComponents/nav.html", function(){
			$("#pageNav ul li").each(function(index, domEle){
				$(domEle).find("a").html(lang.mainMenu[index]);
			});
			$(".sync_menu span").each(function(index, domEle){
				$(domEle).text(lang.sync_buttons[index]);
			});
			//Set Active Menu:
			var currentHref = whereAmI();
			if(typeof sa_fromCustomer != "undefined" && sa_fromCustomer != "0"){
				var from_customer = 1;
			}
			else{
				var from_customer = 0;
			}
			var activeArrow = '<span class="active_arrow"></span>';
			switch(currentHref){
				case "around-me.html":
					$("#pageNav ul li:eq(0)").append(activeArrow);
					break;
				case "customers.html":
				case "createReturn.html":
				case "createOrder.html":
					$("#pageNav ul li:eq(1)").append(activeArrow);
					break;
				case "products.html":
					$("#pageNav ul li:eq(2)").append(activeArrow);
					break;
				case "sales_activities.html":
					if(from_customer == 0){
						$("#pageNav ul li:eq(3)").append(activeArrow);
					}
					else{
						$("#pageNav ul li:eq(1)").append(activeArrow);
					}
					break;
				case "reports.html":
					$("#pageNav ul li:eq(4)").append(activeArrow);
					break;
				case "reminders.html":
					$("#pageNav ul li:eq(5)").append(activeArrow);
					break;	
			}

			//Set Logo:
			cordova.exec(function(param){
				var result = eval('(' + param + ')');		
				var current = result["VKORG"];
				if(current == "0400"){
					$(".header_logo").addClass("logo0400");
				} 
				else if(current == "0500") {
					$(".header_logo").addClass("logo0500");
				}
				else if(current == "0600") {
					$(".header_logo").addClass("logo0600");
				}
				else if(current == "0900") {
					$(".header_logo").addClass("logo0900");
				}
				else if(current == "1100") {
					$(".header_logo").addClass("logo1100");
				}
				else if(current == "1200") {
					$(".header_logo").addClass("logo1200");
				}
				else if(current == "1300") {
					$(".header_logo").addClass("logo1300");
				}
				else if(current == "1400") {
					$(".header_logo").addClass("logo1400");
				}
				else if(current == "1500") {
					$(".header_logo").addClass("logo1500");
				}
				else if(current == "1800") {
					$(".header_logo").addClass("logo1800");
				}
				else if(current == "1900") {
					$(".header_logo").addClass("logo1900");
				}
				else if(current == "2000") {
					$(".header_logo").addClass("logo2000");
				}else{
					$(".header_logo").addClass("logoDefault");
				}

				//Fire the Home logic:
				if ( currentHref == "home.html" ){
					getHomeData();
				}

			}, execFailed, 'User', 'getUser', ['']);
			
		});

		// Login screen:
		$("#login .username_title").text(lang.Username_title);
		$("#login .pass_title").text(lang.Pass_title);
		$("#login_but").text(lang.Login_button);
	   	$("#settings_but").text(lang.Settings_button);
	    $("#settingsHeader .settings_title").text(lang.settings_title);

		/* Apply Language Settings START */
		$("#pageDownNav ul li").each(function(index, domEle){
			$(domEle).find("span").text(lang.downMenu[index]);
		});
		$("#settings_holder .settingsTable .settingsTableTitle").each(function(index, domEle){
			$(domEle).text(lang.SettingsTable [index]);
		});
		$("#pageDownNav.downNav_products ul li").each(function(index, domEle){
			$(domEle).find("span").text(lang.downMenu_products[index]);
		});
		$("#personal_information .title").each(function(index, domEle){
			$(domEle).text(lang.personalInfo[index]);
		});
		$("#personDetails_holder .info_column .title").each(function(index, domEle){
			$(domEle).text(lang.customer_contact_details[index]);
		});
		$("#customers .info_column .title").each(function(index, domEle){
			$(domEle).text(lang.personalInfo[index]);
		});
		$(".DownNavSalesActivities ul li").each(function(index, domEle){
			$(domEle).find("span").text(lang.downMenuSalesActivities[index]);
		});
		$("#sale_activity_information .title").each(function(index, domEle){
			$(domEle).text(lang.salesActivities[index]);
		});

		$("#sales_activity_add .title").each(function(index, domEle){
			$(domEle).text(lang.sale_activity_add[index]);
		});
		$("#customers_contactsTable th").each(function(index, domEle){
			$(domEle).text(lang.contactTable[index]);
		});
		$("#customers_addNewContact .title").each(function(index, domEle){
			$(domEle).text(lang.addNewContact[index]);
		});
		$("#customers_editContact .title").each(function(index, domEle){
			$(domEle).text(lang.editContact[index]);
		});
		$("#customers_editCustomer .title").each(function(index, domEle){
			$(domEle).text(lang.editCustomer[index]);
		});

		$(".circles_holder li span").each(function(index, domEle){
			$(domEle).text(lang.quickMenu[index]);
		});
		$(".circles_holder_sa li span").each(function(index, domEle){
			$(domEle).text(lang.quickMenu_sales_activities[index]);
		});
		$("#sales_activities_table th").each(function(index, domEle){
			$(domEle).text(lang.SAordersTable[index]);
		});
		$("#customers_salesDocuments_table th").each(function(index, domEle){
			$(domEle).text(lang.customer_SalesDocuments_table[index]);
		});
		$("#customers_salesDocuments #personal_information .title").each(function(index, domEle){
			$(domEle).text(lang.orderDetails[index]);
		});
		$("#customers_salesDocuments .order_title").each(function(index, domEle){
			$(domEle).text(lang.SalesOrderDetails_titles[index]);
		});
		$("#customers_salesDocuments_ordersTable th.label").each(function(index, domEle){
			$(domEle).text(lang.orderDetailsTable[index]);
		});
		$("#totals_group .t_label").each(function(index, domEle){
			$(domEle).text(lang.ordersTotals[index]);
		});
		
		$("#syncLogTable th").each(function(index, domEle){
			$(domEle).text(lang.syncLogHeader2[index]);
		});
		$("#synchLogTitle").text(lang.syncLogHeader1);

		$("#customers .header_title").each(function(index, domEle){
			$(domEle).text(lang.customer_details_top[index]);
		});

		$("#customers2 .header_title").each(function(index, domEle){
			$(domEle).text(lang.customer_details_top[index]);
		});

		$("#customers #personal_information .title").each(function(index, domEle){
			$(domEle).text(lang.customer_header1[index]);
		});
		$("#customers2 #personal_information .title").each(function(index, domEle){
			$(domEle).text(lang.customer_header2[index]);
		});
		

		$("#map_zoomLevel .zoom_level").each(function(index, domEle){
			$(domEle).text(lang.AroundMe_zoom[index]);
		});

		$(".productsTable:eq(0) thead th:eq(0)").text(lang.productTableHeadings[0]);
		$(".productsTable:eq(1) thead th:eq(0)").text(lang.productTableHeadings[1]);
		$(".productsTable:eq(2) thead th:eq(0)").text(lang.productTableHeadings[2]);
		
		$(".productsTable:eq(0) tbody tr td .item").each(function(index, domEle){
			$(domEle).text(lang.product_General[index]);
		});
		$("#customers2 #personal_information .item").each(function(index, domEle){
			$(domEle).text(lang.customer_header2[index]);
		});
		$("#customers2 .productsTable:eq(0) thead th:eq(0)").text(lang.header2_tableheadz[0]);
		$("#customers2 .productsTable:eq(1) thead th:eq(0)").text(lang.header2_tableheadz[1]);
		$("#customers2 .productsTable:eq(2) thead th:eq(0)").text(lang.header2_tableheadz[2]);

		$("#aside_nav a").each(function(index, domEle){
			$(domEle).text(lang.product_downNav[index]);
		});
		$("#aside_nav_cust a").each(function(index, domEle){
			$(domEle).text(lang.customer_downNav[index]);
		});
		$("#aside_nav_activities a").each(function(index, domEle){
			$(domEle).text(lang.activities_downNav[index]);
		});
		$("#aside_nav_reminders a").each(function(index, domEle){
			$(domEle).text(lang.reminder_downNav[index]);
		});

		$("#reminderDtlsTable th div").each(function(index, domEle){
			$(domEle).text(lang.reminder_details[index]);
		});

		$("#aside_nav.createSalesOrderNav a").each(function(index, domEle){
			$(domEle).text(lang.createOrderDownNav[index]);
		});

		$("#createOrders_articleTable th").each(function(index, domEle){
			$(domEle).text(lang.createOrderTable[index]);
		});

		$(".ex1 th.createOrder_label").each(function(index, domEle){
			$(domEle).text(lang.createExchangesTable1[index]);
		});
		$(".ex2 th.createOrder_label").each(function(index, domEle){
			$(domEle).text(lang.createExchangesTable2[index]);
		});
		$(".stock_plants_head th").each(function(index, domEle){
			$(domEle).text(lang.stock_plants_head[index]);
		});
		$(".add_price_head th").each(function(index, domEle){
			$(domEle).text(lang.add_price_head[index]);
		});
		$(".salesOrderBottomMenu .item_title").each(function(index, domEle){
			$(domEle).text(lang.salesOrderBottomMenu[index]);
		});

		$("#personal_information #totals_group .t_label").each(function(index, domEle){
			$(domEle).text(lang.ordersTotals[index]);
		});
		$("#personal_information #totals_group_ret .t_label").each(function(index, domEle){
			$(domEle).text(lang.returnsTotals[index]);
		});
		$(".reportTable th").each(function(index, domEle){
			$(domEle).text(lang.reports_table[index]);
		});
		$('#reports_table thead th').text(lang.reports_table[1]);
		$('.SD_attachHeader').text(lang.productTableHeadings[2]);
		
		//$('#dialog').dialog('option', 'title', lang.dialog_title);

		$("#form_heading").text(lang.Login_heading);
		$(".total_label_sum, .total_label").text(lang.total_orderDetailsTable);
		$("#sales_activities .backBut, #back_but, #back_salesOrders, #back_to_home, #goBack, #back_salesActivities, #back_to_customer, #back_toCustomersSalesDocumentsList").text(lang.backButton);
		$(".home_customersTitle").text(lang.home_customersTitle);
		$(".home_productsTitle").text(lang.home_productsTitle);
		$("#search_box").attr("placeholder", lang.searchBox);
		$("#advancedSearchDialog span.title1").each(function(index, domEle){
			$(domEle).text(lang.filterSA[index]);
		});
		$("#customers_addNewContact #heading").text(lang.addNewContact_heading);
		$("#customers_editContact #heading").text(lang.editContact_heading);
		$("#customers_editCustomer #heading").text(lang.editCustomer_heading);
		$(".edit").text(lang.editButton);
		$("#delete, .deleteItem, .delete").text(lang.deleteButton);
		$(".delete_all_log").text(lang.deleteButtonAllSyncLog);
		$("#save, .save").text(lang.saveButton);
		$("#home_saveButton").val(lang.saveButton);
		$("#save_but").text(lang.saveButton);
  		$("#delete_but").text(lang.deleteButton);
  		$(".clear_but").text(lang.clearButton);
		$("#cancel, .cancel").text(lang.cancelButton);

		//Sales Activities Screen
		$("#email_sa span").text(lang.send_mail);
		$("#cfu span").text(lang.create_follow_up);
		
		//home screen
		$("#start_but").text(lang.home_cap_start);
		$("#end_but").text(lang.home_cap_end);
		$("#start_end_man_but").text(lang.home_cap_start_end_manual);

		//Dialog:
		$("#add_but").val(lang.dialog_add);
		$("#closeDialog_but").val(lang.dialog_cancel);
		$("#dialog_validation_text").text(lang.customer_maintenance_dialog_validation_text);
		$("#dialog_tables_text").text(lang.customer_maintenance_dialog_tables_text);
		$("#validation_yes").val(lang.customer_maintenance_dialog_validation_yes);
		$("#validation_no").val(lang.customer_maintenance_dialog_validation_no);

		//Home Page Dialog:
		$("#dialog_home .home_popup_title").each(function(index, domEle){
			$(domEle).text(lang.HomeScreenDialog[index]);
		});
		$("#dialog_home #type option[value=YS01]").text(lang.StartEndLocation[0]);
		$("#dialog_home #type option[value=YE01]").text(lang.StartEndLocation[1]);

		//Exchange dialog:
		$(".goods_givenAway").text(lang.goods_given);
		$(".goods_received").text(lang.goods_received);

		//Signature:
		$("#confirm_signature").text(lang.signature_confirm);
		$(".signature_title").text(lang.signature_title);
		$("#cancel_signature").text(lang.cancelButton);
		$("#captureSignature_button span").text(lang.captureSignature_button);

		$(".order_header_create .order_title").each(function(index, domEle){
			$(domEle).text(lang.createSalesOrder_header[index]);
		});

		$('#sales_activity_link').text(lang.link_sls_act);

		$('#attachments_links').text(lang.links_word);
		//Delete Sales Document Confirm:
		$("#delete_msg").text(lang.deleteSalesDoc_msg);

		$("#deleteAll_msg").text(lang.deleteAll_msg);
		//Delete Contact Confirm:
		$("#deleteContact_msg").text(lang.deleteContact_msg);

		//Delete Sales Activity confirm:
		$("#deleteSA_msg").text(lang.deleteSA_msg);
		
		//$("#dialog .dialog_title title").text(lang.dialog_title);
		/* Apply Language Settings END */
	}

	function errorGetPreferredLanguage() {}
	function errorGetDatePattern() {}
	function errorGetNumberPattern() {}

});

function execFailed(param) {
	customAlert({ param : "error" });
}
