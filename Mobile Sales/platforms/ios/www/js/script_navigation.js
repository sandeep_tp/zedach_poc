function addMarker(map, latlng, label, html) {
	var marker = new google.maps.Marker({
		position: latlng,
		animation: google.maps.Animation.DROP,
		icon: 'img/default_pin.png', 
		map: map,
		title: label
	});
	infowindow = new google.maps.InfoWindow();

	google.maps.event.addListener(marker, 'click', function() {
		infowindow.setContent(label+ '<br />'+ html);
		infowindow.open(map, marker);
	});

	return marker;
}
function setRadius(map, circle, radius_km){
	circle.setRadius(radius_km * 1000);
	map.fitBounds(circle.getBounds());
}

function initialize() {
	$(document).ready(function(){
		// Map-Canvas Size Fix:
		$(document).ready(function(){
			$("#pageContent .wrapper").height($(window).height() - ($("#pageHeader").height()+1));
		});

		var myLocation = new google.maps.LatLng(52.519772, 13.406225); //Berlin

		var mapOptions = {
			center: myLocation,
			zoom: 12,
			disableDefaultUI: true,
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.SMALL,
				position: google.maps.ControlPosition.LEFT_TOP
			},
			mapTypeControl: true,
			streetViewControl: true,
			overviewMapControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);


		//Directions:
		var directionsDisplay = new google.maps.DirectionsRenderer();
		var directionsService = new google.maps.DirectionsService();
		directionsDisplay.setMap(map);

		var request = {
			origin: new google.maps.LatLng(52.524587, 13.409786),
			destination: new google.maps.LatLng(52.467706, 13.496182),
			waypoints: [
				{
					location: new google.maps.LatLng(52.538128, 13.489059),
					stopover: true
				},
				{
					location: new google.maps.LatLng(52.564577, 13.459786),
					stopover: true
				},
				{
					location: new google.maps.LatLng(52.644577, 13.429786),
					stopover: true
				},
			],
			optimizeWaypoints: true,
			travelMode: google.maps.TravelMode.DRIVING,
		};



		directionsService.route(request, function(response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
				var route = response.routes[0];
				var summaryPanel = document.getElementById('directions_panel');
				summaryPanel.innerHTML = '';
				// For each route, display summary information.
				for (var i = 0; i < route.legs.length; i++) {
				var routeSegment = i + 1;
				summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
				summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
				summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
				summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
				}
			}
		});



		//Markers:
		var tmpClientData = [
			new google.maps.LatLng(52.524587, 13.409786),
			new google.maps.LatLng(52.538128, 13.489059),
			new google.maps.LatLng(52.564577, 13.459786),
			new google.maps.LatLng(52.644577, 13.429786),
			new google.maps.LatLng(52.467706, 13.496182)
		];
		// var markersArray = [];
		// for(i = 0; i < $("#pageAside ul li").size(); i++){
		// 	markersArray.push(addMarker(
		// 		map,
		// 		tmpClientData[i],
		// 		$("#pageAside ul li:eq(" + i + ") .address").html(),
		// 		'Client Name: ' + $("#pageAside ul li:eq(" + i + ") .name").html()
		// 	));
		// }

		$("#pageAside ul li").click(function(e){
			google.maps.event.trigger(markersArray[$(this).index()],"click");
			map.panTo(markersArray[$(this).index()].getPosition());
		});


		//Zoom to Radius:
		var circle = new google.maps.Circle({
			center: myLocation,
			fillColor: '#005282',
			fillOpacity: .2,
			strokeColor: '#002941',
			strokeOpacity: 0.6,
			strokeWeight: 1,
			map: map
		});
		$("#map_zoomLevel ul li a").click(function(){
			$("#map_zoomLevel a").removeClass("active");
			$(this).addClass("active");

			var radius_km = parseInt($(this).find("span").html().substring(0, 3).trim());
			setRadius(map, circle, radius_km);
		});
	});
}

google.maps.event.addDomListener(window, 'load', initialize);