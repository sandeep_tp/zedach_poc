//
//  NSString+StringSubstitution.h
//  iota
//
//  Created by Alexey Piterkin on 2/9/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


//extern int g_iForceStringExtensionsToLink;

@interface NSString (AMPWidgets)

// Replaces all labels from the dictionary with their values inside the target string
// Survives any nils.
+ (void)substituteStringsFromDictionary:(NSDictionary*)dict target:(NSString**)target exclude:(NSSet*)exceptions;

// Convenience method that wraps xml entities
- (NSString*)stringByEncodingXMLEntities;

// Convenience method to check if the string is empty (nil, 0 length, or all whitespace)
+ (BOOL)isNotEmpty:(NSString*)string;

+ (BOOL)parseBoolean:(NSString*)string defaultValue:(BOOL)value;

@end
