/*******************************************************************************
* Source File : ListenerManager.h
* Date Created: 
* Copyright   : 2000 - 2010, Sybase, Inc.
* Description : 
* Notes       : 
*******************************************************************************/

#import <Foundation/Foundation.h>
#import "HybridAppViewController.h"
#import "MessagingClientLib.h"
#import "moDataTypes.h"

@class HybridAppListener;
@class MessageListener;
/*!
 @class AppListener
 @abstract This is the base class for all listeners.
 */
@interface AppListener : NSObject
{
   NSString* callbackName;
}
/*!
 @property
 @abstract callback name
 */
@property (nonatomic, retain) NSString* callbackName;

/*!
 @method
 @abstract init with callback name
 @param callback callback name
 @return return value description
 */
- (id) initWithCallback:(NSString*)callback;

@end

/*!
 @protocol AppDownloadListenerProtocol
 @abstract Protocol implemented by all app download listeners.
 */
@protocol AppDownloadListenerProtocol

/*!
 @method
 @abstract Called when a hybrid app has started downloading to the device.
 @param moduleId hybrid app module id
 @param version hybrid app version
 @param name hybrid app name
*/
- (void) onDownloadBegin:(I32)moduleId version:(I32)version name:(NSString*)name;

/*!
 @method
 @abstract Called when a hybrid app has finished downloading to the device.
 @param moduleId module id
 @param version version
 @param name hybrid app name
 */
- (void) onDownloadEnd:(I32)moduleId version:(I32)version name:(NSString*)name;

/*!
 @method
 @abstract Called when a hybrid app has failed to download.
 @param moduleId module id
 @param version version
 @param name hybrid app name
 @param designerVersion designer version
 @param containerVersion container version
 */
- (void) onDownloadFailed:(I32) moduleId version:(I32)version name:(NSString*)name designerVersion:(NSString*)designerVersion containerVersion:(NSString*)containerVersion;

@end

/*!
 @class AppDownloadListenerNative
 @abstract Listens for app download events, and passes notifications to a native listener.
 @discussion You must subclass this class, and provide implementations for the methods in
  the AppDownloadListenerProtocol, in order to process native callbacks.
 */
@interface AppDownloadListenerNative : AppListener <AppDownloadListenerProtocol>

/*!
 @method
 @abstract init with name
 @param name listener name
 */
- (id) initWithName:(NSString*)name;

@end

/*!
 @class AppListenerManager
 @abstract Manages the callbacks, and notifies callback objects when appropriate.
 */
@interface AppListenerManager : NSObject
{
   HybridAppListener* hybridAppListener;   
   MessageListener* messageListener;  
   
   NSMutableDictionary* appDownloadNativeListeners;
   id<AppDownloadListenerProtocol> appDownloadJSListener;
   
   id<ConnectionStateListenerDelegate> connectionStateNativeListener;
   id<PushNotificationListenerDelegate> pushNotificationNativeListener;
   BOOL connectionEventsEnabled;
   BOOL jsPushListenerStarted;
}
/*!
 @property
 @abstract HybridApp listener
 */
@property (nonatomic, retain) HybridAppListener* hybridAppListener;
/*!
 @property
 @abstract Message/Notification native listener
 */
@property (nonatomic, retain) MessageListener* messageListener;
/*!
 @property
 @abstract Dictionary of HybridApp download native listners
 */
@property (nonatomic, retain) NSMutableDictionary* appDownloadNativeListeners;
/*!
 @property
 @abstract Hybridapp download native listener
 */
@property (nonatomic, retain) id<AppDownloadListenerProtocol> appDownloadJSListener;
/*!
 @property
 @abstract Connection state native listener
 */
@property (nonatomic, retain) id<ConnectionStateListenerDelegate> connectionStateNativeListener;
/*!
 @property
 @abstract Push notification native listener
 */
@property (nonatomic, retain) id<PushNotificationListenerDelegate> pushNotificationNativeListener;
/*!
 @property
 @abstract If connection events enabled
 */
@property (nonatomic) BOOL connectionEventsEnabled;
/*!
 @property
 @abstract if JS push listener started
 */
@property (nonatomic) BOOL jsPushListenerStarted;
/*!
 @method
 @abstract initialize after MessagingClient has initialized
 */
- (void) initializeAfterMessagingClientInit;
/*!
 @method
 @abstract get HybridApp listener manager singleton
 @return HybridApp listener manager
 */
+ (AppListenerManager *) getInstance;
/*!
 @method
 @abstract add HybridApp listener
 */
- (void) addHybridAppListener;
/*!
 @method
 @abstract remove HybridApp listener
 */
- (void) removeHybridAppListener;
/*!
 @method
 @abstract add message listener
 */
- (void) addMessageListener;
/*!
 @method
 @abstract remove message listener
 */
- (void) removeMessageListener;
/*!
 @method
 @abstract add HybridApp download JS listener
 */
- (void) addAppDownloadJSListener;
/*!
 @method
 @abstract remove HybridApp download JS listener
 */
- (void) removeAppDownloadJSListener;
/*!
 @method
 @abstract add HybridApp download native listener
 @param listener HybridApp download native listener
 */
- (void) addAppDownloadNativeListener:(AppDownloadListenerNative*)listener;
/*!
 @method
 @abstract remove HybridApp download native listener
 @param callback listener callback name
 */
- (void) removeAppDownloadNativeListener:(NSString*)callback;
/*!
 @method
 @abstract remove all HybridApp download native listeners
 */
- (void) removeAllAppDownloadNativeListeners;
/*!
 @method
 @abstract remove all HybridApp download JS listeners
 */
- (void) removeAllAppDownloadListeners;
/*!
 @method
 @abstract add connection state listener
 */
- (void) addConnectionStateListener;
/*!
 @method
 @abstract remove connection state listener
 */
- (void) removeConnectionStateListener;
/*!
 @method
 @abstract start Push JS listener
 */
- (void) startJavascriptPushListener;
/*!
 @method
 @abstract stop Push JS listener
 */
- (void) stopJavascriptPushListener;
/*!
 @method
 @abstract add fake Push notification
 @param pushData pushData
 */
- (void) addFakePushNotification:(NSString*)pushData;
/*!
 @method
 @abstract remove all listeners
 */
- (void) removeAllListeners;
/*!
 @method
 @abstract remove all JS listeners
 */
- (void) removeAllJSListeners;
/*!
 @method
 @abstract remove all native listeners
 */
- (void) removeAllNativeListeners;
/*!
 @method
 @abstract notify when HybridApp begin downloading
 @param displayName HyrbidApp display name
 @param moduleId    HybridApp module id
 @param version     HybridApp version
 */
- (void) notifyOnAppDownloadBegin:(NSString*)displayName moduleId:(I32)moduleId version:(I32)version;
/*!
 @method
 @abstract notify when HybridApp failed to download
 @param displayName HyrbidApp display name
 @param moduleId    HybridApp module id
 @param version     HybridApp version
 @param designerVersion designer version
 @param containerVersion container version
 */
- (void) notifyOnAppDownloadFailed:(NSString*)displayName moduleId:(I32)moduleId version:(I32)version designerVersion:(NSString*)designerVersion containerVersion:(NSString*)containerVersion;

@end
