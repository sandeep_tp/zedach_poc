//
//  XmlParser.h
//  Widgets
//
//  Created by Alexey Piterkin on 2/27/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WidgetXmlParser;

@protocol Parseable <NSObject>

- (id <Parseable>)parser:(WidgetXmlParser*)parser startElement:(NSString*)name attributes:(NSDictionary*)attributes;
- (BOOL)parser:(WidgetXmlParser*)parser endElement:(NSString*)name;
- (void)parser:(WidgetXmlParser*)parser addString:(NSString*)string;

@end


#ifdef __IPHONE_4_0
@interface WidgetXmlParser : NSObject <NSXMLParserDelegate>
#else
@interface WidgetXmlParser : NSObject
#endif
{
   NSXMLParser * parser;
   
   NSMutableArray * stack;
   
   NSString * error;
   
   id <Parseable> root;
   
   int version;
}

@property (nonatomic, readonly) id <Parseable> top;
@property (nonatomic, retain) NSString * error;
@property (nonatomic, readonly) id <Parseable> root;
@property (nonatomic, assign) int version;

- (id)initWithString:(NSString*)xmlText;
- (void)parse;

@end
