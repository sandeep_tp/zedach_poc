/*
 
 Copyright (c) Sybase, Inc. 2009-2013   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */

#import "sybase_core.h"

@class SUPJsonArray;
@class SUPJsonBoolean;
@class SUPJsonCharToken;
@class SUPJsonException;
@class SUPJsonNumber;
@class SUPJsonObject;
@class SUPJsonString;
@class SUPObjectList;
@class SUPStringList;
@class SUPStringUtil;

@class SUPJsonReader;

/*!
 @class
 @abstract Class used to convert JSON-encoded strings into JSON objects, arrays, or values.
 @discussion
 */
@interface SUPJsonReader : NSObject
{
}

- (SUPJsonReader*)init;
- (void)dealloc;
+ (SUPJsonReader*)getInstance;

/*!
 @method
 @abstract Reads a string and returns a SUPJsonObject or SUPJsonArray.
 @discussion If the input string is invalid JSON, an exception will be thrown.
 @param text The string to read.
 @result Depending on the contents of the string passed in, the result can be a SUPJsonObject or a SUPJsonArray.
 @throws SUPJsonException
 */
+ (id)read:(SUPString)text;

@end
