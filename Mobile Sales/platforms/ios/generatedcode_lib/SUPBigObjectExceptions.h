/*
 
 Copyright (c) Sybase, Inc. 2011   All rights reserved.                                    
 
 In addition to the license terms set out in the Sybase License Agreement for 
 the Sybase Unwired Platform ("Program"), the following additional or different 
 rights and accompanying obligations and restrictions shall apply to the source 
 code in this file ("Code").  Sybase grants you a limited, non-exclusive, 
 non-transferable, revocable license to use, reproduce, and modify the Code 
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of 
 applications created in connection with your licensed use of the Program.  
 The Code may not be transferred, sold, assigned, sublicensed or otherwise 
 conveyed (whether by operation of law or otherwise) to another party without 
 Sybase's prior written consent.  The following provisions shall apply to any 
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or 
 implied, relating to modified Code or any problems that result from use of the 
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
 to indemnify, hold harmless, and defend Sybase from and against any claims or 
 lawsuits, including attorney's fees, that arise from or are related to the 
 modified Code or from use of the modified Code. 
 
 */



#import "sybase_core.h"
#import "SUPPersistenceException.h"
#import "SUPObjectNotFoundException.h"

/*!
 @class SUPObjectNotSavedException
 @abstract   SUPObjectNotSavedException is thrown if an SUPBigBinary or SUPBigString method is called that requires the object to already exist in the database.
 @discussion 
 */

@interface SUPObjectNotSavedException : SUPPersistenceException
{
}

/*!
 @method objectNotSavedExceptionWithErrorCode
 @abstract  Instantiates an SUPObjectNotSavedException object
 @param  errorCode The error code value
 @result The SUPObjectNotSavedException object.
 @discussion
 */
+ (SUPObjectNotSavedException*)objectNotSavedExceptionWithErrorCode:(int)errorCode;

/*!
 @method objectNotSavedExceptionWithErrorCode
 @abstract  Instantiates an SUPObjectNotSavedException object
 @param  errorCode The error code value
 @param  message the error message
 @result The SUPObjectNotSavedException object.
 @discussion
 */
+ (SUPObjectNotSavedException *)objectNotSavedExceptionWithErrorCode:(int)errorCode message:(NSString *)message;


/*!
 @method objectNotSavedExceptionWithErrorCode
 @abstract  Instantiates an SUPObjectNotSavedException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @result The SUPObjectNotSavedException object.
 @discussion
 */
+ (SUPObjectNotSavedException*)objectNotSavedExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments;

/*!
 @method objectNotSavedExceptionWithErrorCode
 @abstract  Instantiates an SUPObjectNotSavedException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @param  cause The cause of the exception
 @result The SUPObjectNotSavedException object.
 @discussion
 */
+ (SUPObjectNotSavedException*)objectNotSavedExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments cause:(NSException *)cause;


/*!
 @method     
 @abstract   Returns the SUPObjectNotSavedException object with "theMessage".
 @param theMessage The  message.
 @result The SUPObjectNotSavedException.
 @discussion 
 */
+ (SUPObjectNotSavedException*)withMessage:(SUPString)theMessage DEPRECATED_ATTRIBUTE;


/*!
 @method     
 @abstract   Returns a new instance of SUPObjectNotSavedException object.
 @result The SUPObjectNotSavedException object.
 @discussion 
 */
+ (SUPObjectNotSavedException*)getInstance DEPRECATED_ATTRIBUTE;

@end

/*!
 @class SUPStreamNotOpenException
 @abstract   SUPStreamNotOpenException is thrown if an SUPBigBinary or SUPBigString method is called that requires the object to be open.
 @discussion 
 */

@interface SUPStreamNotOpenException : SUPPersistenceException
{
}

/*!
 @method streamNotOpenExceptionWithErrorCode
 @abstract  Instantiates an SUPStreamNotOpenException object
 @param  errorCode The error code value
 @result The SUPStreamNotOpenException object.
 @discussion
 */
+ (SUPStreamNotOpenException*)streamNotOpenExceptionWithErrorCode:(int)errorCode;

/*!
 @method streamNotOpenExceptionWithErrorCode
 @abstract  Instantiates an SUPStreamNotOpenException object
 @param  errorCode The error code value
 @param  message the error message
 @result The SUPStreamNotOpenException object.
 @discussion
 */
+ (SUPStreamNotOpenException *)streamNotOpenExceptionWithErrorCode:(int)errorCode message:(NSString *)message;


/*!
 @method streamNotOpenExceptionWithErrorCode
 @abstract  Instantiates an SUPStreamNotOpenException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @result The SUPStreamNotOpenException object.
 @discussion
 */
+ (SUPStreamNotOpenException*)streamNotOpenExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments;

/*!
 @method streamNotOpenExceptionWithErrorCode
 @abstract  Instantiates an SUPStreamNotOpenException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @param  cause The cause of the exception
 @result The SUPStreamNotOpenException object.
 @discussion
 */
+ (SUPStreamNotOpenException*)streamNotOpenExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments cause:(NSException *)cause;


/*!
 @method     
 @abstract   (Deprecated) Returns the SUPStreamNotOpenException object with "theMessage".
 @param theMessage The  message.
 @result The SUPStreamNotOpenException.
 @discussion 
 */
+ (SUPStreamNotOpenException*)withMessage:(SUPString)theMessage DEPRECATED_ATTRIBUTE;

/*!
 @method     
 @abstract   (Deprecated) Returns a new instance of SUPStreamNotOpenException object.
 @result The SUPStreamNotOpenException object.
 @discussion 
 */
+ (SUPStreamNotOpenException*)getInstance DEPRECATED_ATTRIBUTE;


@end

/*!
 @class SUPStreamNotClosedException
 @abstract   SUPStreamNotClosedException is thrown if an SUPBigBinary or SUPBigString method is called that requires the object to not be open.
 @discussion 
 */

@interface SUPStreamNotClosedException : SUPPersistenceException
{
}

/*!
 @method streamNotClosedExceptionWithErrorCode
 @abstract  Instantiates a streamNotClosedExceptionWithErrorCode object
 @param  errorCode The error code value
 @result The SUPStreamNotClosedException object.
 @discussion
 */
+ (SUPStreamNotClosedException*)streamNotClosedExceptionWithErrorCode:(int)errorCode;

/*!
 @method streamNotClosedExceptionWithErrorCode
 @abstract  Instantiates a streamNotClosedExceptionWithErrorCode object
 @param  errorCode The error code value
 @param  message the error message
 @result The SUPStreamNotClosedException object.
 @discussion
 */
+ (SUPStreamNotClosedException *)streamNotClosedExceptionWithErrorCode:(int)errorCode message:(NSString *)message;


/*!
 @method streamNotClosedExceptionWithErrorCode
 @abstract  Instantiates an SUPStreamNotClosedException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @result The SUPStreamNotClosedException object.
 @discussion
 */
+ (SUPStreamNotClosedException*)streamNotClosedExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments;

/*!
 @method streamNotClosedExceptionWithErrorCode
 @abstract  Instantiates an SUPStreamNotClosedException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @param  cause The cause of the exception
 @result The SUPStreamNotClosedException object.
 @discussion
 */
+ (SUPStreamNotClosedException*)streamNotClosedExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments cause:(NSException *)cause;

/*!
 @method     
 @abstract   (Deprecated) Returns the SUPStreamNotClosedException object with "theMessage".
 @param theMessage The  message.
 @result The SUPStreamNotClosedException.
 @discussion 
 */
+ (SUPStreamNotClosedException*)withMessage:(SUPString)theMessage DEPRECATED_ATTRIBUTE;

/*!
 @method     
 @abstract   (Deprecated) Returns a new instance of SUPStreamNotClosedException object.
 @result The SUPStreamNotClosedException object.
 @discussion 
 */
+ (SUPStreamNotClosedException*)getInstance DEPRECATED_ATTRIBUTE;


@end

/*!
 @class SUPWriteAppendOnlyException
 @abstract   SUPWriteAppendOnlyException is thrown if an SUPBigBinary or SUPBigString method is called that writes to the middle of a value where only appending is allowed by the underlying DB.
 @discussion 
 */

@interface SUPWriteAppendOnlyException : SUPPersistenceException
{
}

/*!
 @method writeAppendOnlyExceptionWithErrorCode
 @abstract  Instantiates an SUPWriteAppendOnlyException object
 @param  errorCode The error code value
 @result The SUPWriteAppendOnlyException object.
 @discussion
 */
+ (SUPWriteAppendOnlyException*)writeAppendOnlyExceptionWithErrorCode:(int)errorCode;

/*!
 @method writeAppendOnlyExceptionWithErrorCode
 @abstract  Instantiates an SUPWriteAppendOnlyException object
 @param  errorCode The error code value
 @param  message the error message
 @result The SUPWriteAppendOnlyException object.
 @discussion
 */
+ (SUPWriteAppendOnlyException *)writeAppendOnlyExceptionWithErrorCode:(int)errorCode message:(NSString *)message;


/*!
 @method writeAppendOnlyExceptionWithErrorCode
 @abstract  Instantiates an SUPWriteAppendOnlyException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @result The SUPWriteAppendOnlyException object.
 @discussion
 */
+ (SUPWriteAppendOnlyException*)writeAppendOnlyExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments;

/*!
 @method writeAppendOnlyExceptionWithErrorCode
 @abstract  Instantiates an SUPWriteAppendOnlyException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @param  cause The cause of the exception
 @result The SUPWriteAppendOnlyException object.
 @discussion
 */
+ (SUPWriteAppendOnlyException*)writeAppendOnlyExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments cause:(NSException *)cause;

/*!
 @method     
 @abstract   (Deprecated) Returns the SUPWriteAppendOnlyException object with "theMessage".
 @param theMessage The  message.
 @result The SUPWriteAppendOnlyException.
 @discussion 
 */
+ (SUPWriteAppendOnlyException*)withMessage:(SUPString)theMessage DEPRECATED_ATTRIBUTE;

/*!
 @method     
 @abstract   (Deprecated) Returns a new instance of SUPWriteAppendOnlyException object.
 @result The SUPWriteAppendOnlyException object.
 @discussion 
 */
+ (SUPWriteAppendOnlyException*)getInstance DEPRECATED_ATTRIBUTE;


@end

/*!
 @class SUPWriteOverLengthException
 @abstract   SUPWriteOverLengthException is thrown if an SUPBigBinary or SUPBigString method is called that writes past the fixed length allowed for the value by the underlying DB.
 @discussion 
 */

@interface SUPWriteOverLengthException : SUPPersistenceException
{
}

/*!
 @method writeOverLengthExceptionWithErrorCode
 @abstract  Instantiates an SUPWriteOverLengthException object
 @param  errorCode The error code value
 @result The SUPWriteOverLengthException object.
 @discussion
 */
+ (SUPWriteOverLengthException*)writeOverLengthExceptionWithErrorCode:(int)errorCode;

/*!
 @method writeOverLengthExceptionWithErrorCode
 @abstract  Instantiates an SUPWriteOverLengthException object
 @param  errorCode The error code value
 @param  message the error message
 @result The SUPWriteOverLengthException object.
 @discussion
 */
+ (SUPWriteOverLengthException *)writeOverLengthExceptionWithErrorCode:(int)errorCode message:(NSString *)message;


/*!
 @method writeOverLengthExceptionWithErrorCode
 @abstract  Instantiates an SUPWriteOverLengthException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @result The SUPWriteOverLengthException object.
 @discussion
 */
+ (SUPWriteOverLengthException*)writeOverLengthExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments;

/*!
 @method writeOverLengthExceptionWithErrorCode
 @abstract  Instantiates an SUPWriteOverLengthException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @param  cause The cause of the exception
 @result The SUPWriteOverLengthException object.
 @discussion
 */
+ (SUPWriteOverLengthException*)writeOverLengthExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments cause:(NSException *)cause;

/*!
 @method     
 @abstract   (Deprecated) Returns the SUPWriteOverLengthException object with "theMessage".
 @param theMessage The  message.
 @result The SUPWriteOverLengthException.
 @discussion 
 */
+ (SUPWriteOverLengthException*)withMessage:(SUPString)theMessage DEPRECATED_ATTRIBUTE;

/*!
 @method     
 @abstract   (Deprecated) Returns a new instance of SUPWriteOverLengthException object.
 @result The SUPWriteOverLengthException object.
 @discussion 
 */
+ (SUPWriteOverLengthException*)getInstance DEPRECATED_ATTRIBUTE;


@end

