/*
 
 Copyright (c) Sybase, Inc. 2009-2013   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */

#import "sybase_core.h"

@class SUPBase64Encoding;
@class SUPBooleanUtil;
@class SUPDateTimeUtil;
@class SUPDateUtil;
@class SUPJsonArray;
@class SUPJsonBoolean;
@class SUPJsonException;
@class SUPJsonNumber;
@class SUPJsonObject;
@class SUPJsonString;
@class SUPNumberUtil;
@class SUPStringList;
@class SUPStringUtil;
@class SUPTimeUtil;

@class SUPJsonValue;

@interface SUPJsonValue : NSObject
{
}

+ (id)fromArray:(SUPJsonArray*)value;
+ (id)fromObject:(SUPJsonObject*)value;
+ (id)fromBoolean:(SUPBoolean)value;
+ (id)fromString:(SUPString)value;
+ (id)fromBinary:(SUPBinary)value;
+ (id)fromChar:(SUPChar)value;
+ (id)fromByte:(SUPByte)value;
+ (id)fromShort:(SUPShort)value;
+ (id)fromInt:(SUPInt)value;
+ (id)fromLong:(SUPLong)value;
+ (id)fromInteger:(SUPInteger)value;
+ (id)fromDecimal:(SUPDecimal)value;
+ (id)fromFloat:(SUPFloat)value;
+ (id)fromDouble:(SUPDouble)value;
+ (id)fromDate:(SUPDate)value;
+ (id)fromTime:(SUPTime)value;
+ (id)fromDateTime:(SUPDateTime)value;
+ (id)fromNullableBoolean:(SUPNullableBoolean)value;
+ (id)fromNullableString:(SUPNullableString)value;
+ (id)fromNullableBinary:(SUPNullableBinary)value;
+ (id)fromNullableChar:(SUPNullableChar)value;
+ (id)fromNullableByte:(SUPNullableByte)value;
+ (id)fromNullableShort:(SUPNullableShort)value;
+ (id)fromNullableInt:(SUPNullableInt)value;
+ (id)fromNullableLong:(SUPNullableLong)value;
+ (id)fromNullableInteger:(SUPNullableInteger)value;
+ (id)fromNullableDecimal:(SUPNullableDecimal)value;
+ (id)fromNullableFloat:(SUPNullableFloat)value;
+ (id)fromNullableDouble:(SUPNullableDouble)value;
+ (id)fromNullableDate:(SUPNullableDate)value;
+ (id)fromNullableTime:(SUPNullableTime)value;
+ (id)fromNullableDateTime:(SUPNullableDateTime)value;
+ (SUPJsonArray*)getArray:(id)value;
+ (SUPJsonObject*)getObject:(id)value;
+ (SUPBoolean)getBoolean:(id)value;
+ (SUPString)getString:(id)value;
+ (SUPBinary)getBinary:(id)value;
+ (SUPChar)getChar:(id)value;
+ (SUPByte)getByte:(id)value;
+ (SUPShort)getShort:(id)value;
+ (SUPInt)getInt:(id)value;
+ (SUPLong)getLong:(id)value;
+ (SUPInteger)getInteger:(id)value;
+ (SUPDecimal)getDecimal:(id)value;
+ (SUPFloat)getFloat:(id)value;
+ (SUPDouble)getDouble:(id)value;
+ (SUPDate)getDate:(id)value;
+ (SUPTime)getTime:(id)value;
+ (SUPDateTime)getDateTime:(id)value;
+ (SUPNullableBoolean)getNullableBoolean:(id)value;
+ (SUPNullableString)getNullableString:(id)value;
+ (SUPNullableBinary)getNullableBinary:(id)value;
+ (SUPNullableChar)getNullableChar:(id)value;
+ (SUPNullableByte)getNullableByte:(id)value;
+ (SUPNullableShort)getNullableShort:(id)value;
+ (SUPNullableInt)getNullableInt:(id)value;
+ (SUPNullableLong)getNullableLong:(id)value;
+ (SUPNullableInteger)getNullableInteger:(id)value;
+ (SUPNullableDecimal)getNullableDecimal:(id)value;
+ (SUPNullableFloat)getNullableFloat:(id)value;
+ (SUPNullableDouble)getNullableDouble:(id)value;
+ (SUPNullableDate)getNullableDate:(id)value;
+ (SUPNullableTime)getNullableTime:(id)value;
+ (SUPNullableDateTime)getNullableDateTime:(id)value;
+ (SUPJsonException*)unexpectedNull;

@end
