/*
 
 Copyright (c) Sybase, Inc. 2010-2012   All rights reserved.                                    
 
 In addition to the license terms set out in the Sybase License Agreement for 
 the Sybase Unwired Platform ("Program"), the following additional or different 
 rights and accompanying obligations and restrictions shall apply to the source 
 code in this file ("Code").  Sybase grants you a limited, non-exclusive, 
 non-transferable, revocable license to use, reproduce, and modify the Code 
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of 
 applications created in connection with your licensed use of the Program.  
 The Code may not be transferred, sold, assigned, sublicensed or otherwise 
 conveyed (whether by operation of law or otherwise) to another party without 
 Sybase's prior written consent.  The following provisions shall apply to any 
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or 
 implied, relating to modified Code or any problems that result from use of the 
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
 to indemnify, hold harmless, and defend Sybase from and against any claims or 
 lawsuits, including attorney's fees, that arise from or are related to the 
 modified Code or from use of the modified Code. 
 
*/

#import "SUPEntityPendingState.h"
#import "SUPAbstractROEntity.h"
#import "SUPParameterMetaData.h"
#import "SUPAttributeMetaDataRBS.h"
#import "SUPEntityMetaDataRBS.h"
#import "SUPOperationMetaData.h"
#import "SUPDataType.h"
#import "SUPCallbackHandler.h"
#import "SUPDefaultCallbackHandler.h"
#import "SUPStatementWrapper.h"
#import "SUPConnectionWrapper.h"
#import "SUPResultSetWrapper.h"

typedef enum
{
    RbsEntityPendingState_NotPending      = 'N',
    RbsEntityPendingState_PendingCreate   = 'C',
    RbsEntityPendingState_PendingUpdate   = 'U',
    RbsEntityPendingState_PendingDelete   = 'D',
    RbsEntityPendingState_HasPendingChild = 'P',
    RbsEntityPendingState_Snapshot        = 'S',
    
} SUPEntityPendingStateRBS DEPRECATED_ATTRIBUTE;

@class SUPJsonArray;
@class SUPJsonObject;
@class SUPAbstractDBRBS;
@class SUPEntityDelegate;
@class SUPAbstractLocalEntity;

/*!
 @class SUPAbstractEntityRBS
 @abstract   This is the base class for all entity classes.The methods in this class are documented in each entity class.
 @discussion 
 */
// Base class for entity classes
@interface SUPAbstractEntityRBS : SUPAbstractROEntity
{
    @protected
        SUPEntityPendingState _pendingChange;
        int64_t               _replayCounter;
        int64_t               _replayPending;
        int64_t               _replayFailure;
        SUPAbstractEntityRBS*    _originalState;
        BOOL                  _originalStateValid;
        SUPAbstractEntityRBS*	  _downloadState;
        BOOL				  _downloadStateValid;
        BOOL				  _disableSubmit;
        BOOL                  _disableSubmitChanged;
        BOOL                  _isOriginalState;
        BOOL                  _pending;
        BOOL                  _hasDbOriginalState;
    
    // Metadata defines the concrete subclass this abstract entity represents.
    //SUPEntityMetaDataRBS*	  _classMetadata;
}

// Public properties

/** Returns true if this a create operation is pending */
@property(readonly, assign, nonatomic) BOOL isCreated;

/** Returns true if this an update operation is pending */
@property(readonly, assign, nonatomic) BOOL isUpdated;

/** True for any row that represents a pending create, update, or delete operation (optionally also set for snapshot rows, and when pendingChange is 'P'). False otherwise. */
@property(readwrite, assign, nonatomic) BOOL pending;

/** If pending is true, then 'C' (create), 'U' (update), 'D' (delete), 'P' (to indicate that this row is a parent in a cascading relationship for one or more pending child objects,
 but this row itself has no pending create, update or delete operations) or 'S' (to indicate that this row is a snapshot row). If pending is false, then 'N'.*/
@property(readwrite, assign, nonatomic) SUPEntityPendingState pendingChange;

/** Updated each time a row is created or modified by the client.  This value is derived from the time in seconds since an epoch, so it always 
 ** increases each time the row is changed. */
@property(readwrite, assign, nonatomic) int64_t replayCounter;

/** When a pending row is submitted to the server, the value of replayCounter is copied to replayPending.  This allows client code to detect 
 ** if a row has been changed since it was submitted to the server --the test to look for : replayCounter > replayPending. */
@property(readwrite, assign, nonatomic) int64_t replayPending;

/** When the server responds with a replayFailure message for a row that was submitted to the server, the replayCounter value is copied to 
 ** replayFailure, and replayPending is set to 0. */
@property(readwrite, assign, nonatomic) int64_t replayFailure;

/* If set, changes in this entity will not be submitted to the server by a submitPending (or submitPendingOperations) call */
@property(readwrite, assign, nonatomic) BOOL disableSubmit;
@property(readwrite, assign, nonatomic) BOOL disableSubmitChanged;

/* The state of this entity as saved in the original state table */
@property(readwrite, retain, nonatomic) SUPAbstractEntityRBS* originalState;
/* The state of this entity at last download from the server, before any pending changes were made */
@property(readwrite, retain, nonatomic) SUPAbstractEntityRBS* downloadState;

// Init, dealloc

- (id)init;
- (void)dealloc;


// CUD methods
- (void)create;
- (void)update;
- (void)delete;
- (void)refresh;


- (void)cancelPending;
- (void)createPending:(BOOL)isPending;
- (BOOL)isPending;
- (void)submitPending;

// Callback handler methods
/*abstract*/ + (NSObject<SUPCallbackHandler>*)callbackHandler;
/*abstract*/ + (void)registerCallbackHandler:(NSObject<SUPCallbackHandler>*)newCallbackHandler;

// Get name of last operation executed on this entity
- (SUPString)getLastOperation;

@end


// Internal methods and properties -- these should only be called by SUP framework and generated code

@interface SUPAbstractEntityRBS()

@property(readwrite, assign, nonatomic) BOOL originalStateValid;
@property(readwrite, assign, nonatomic) BOOL isOriginalState;
@property(readwrite, assign, nonatomic) BOOL downloadStateValid;
@property(readwrite, assign, nonatomic) BOOL hasDbOriginalState;

+ (SUPAbstractEntityRBS *)getInstanceWithClass:(Class)entityClass;
- (void) cancelPending:(SUPNullableBoolean)forceDeleteChild :(int64_t)operationreplay_rc;
- (void)updatePending:(BOOL)isPending;
- (void) update_os;
- (void)deletePending:(BOOL)isPending;
- (void) deleteOSPending;
- (SUPAbstractEntityRBS*)i_getOriginalState;
- (SUPAbstractEntityRBS*)i_getOriginalStateInternal;
- (SUPAbstractEntityRBS*)i_getDownloadState;
+ (SUPQuery*)getLogRecordQuery:(NSString*)entityName :(NSString*)keyString;
+ (BOOL)checkIsCreateOperation:(id)obj :(SUPDatabaseDelegate*)dbDelegate;

- (void)update: (NSString*)cvpMethodName;
// TODO: check if we need the same change for create and delete 

@end
