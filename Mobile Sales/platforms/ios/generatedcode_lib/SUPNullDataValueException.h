#import "sybase_sup.h"
#import "SUPBaseException.h"


@class SUPNullDataValueException;

@interface SUPNullDataValueException : SUPBaseException
{
}
/*!
 @method nullDataValueExceptionWithErrorCode
 @abstract  Instantiates an SUPNullDataValueException object
 @param  errorCode The error code value
 @result The SUPNullDataValueException object.
 @discussion
 */
+ (SUPNullDataValueException*)nullDataValueExceptionWithErrorCode:(int)errorCode;

/*!
 @method nullDataValueExceptionWithErrorCode
 @abstract  Instantiates an SUPNullDataValueException object
 @param  errorCode The error code value
 @param  message the error message
 @result The SUPNullDataValueException object.
 @discussion
 */
+ (SUPNullDataValueException *)nullDataValueExceptionWithErrorCode:(int)errorCode message:(NSString *)message;


/*!
 @method nullDataValueExceptionWithErrorCode
 @abstract  Instantiates an SUPNullDataValueException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @result The SUPNullDataValueException object.
 @discussion
 */
+ (SUPNullDataValueException*)nullDataValueExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments;

/*!
 @method nullDataValueExceptionWithErrorCode
 @abstract  Instantiates an SUPNullDataValueException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @param  cause The cause of the exception
 @result The SUPNullDataValueException object.
 @discussion
 */
+ (SUPNullDataValueException*)nullDataValueExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments cause:(NSException *)cause;

+ (SUPNullDataValueException*)getInstance DEPRECATED_ATTRIBUTE;
- (SUPNullDataValueException*)init;
- (SUPNullDataValueException*)finishInit;


@end
