/*
 Copyright (c) Sybase, Inc. 2012 All rights reserved. 
 
 In addition to the license terms set out in the Sybase License Agreement for 
 the Sybase Unwired Platform ("Program"), the following additional or different 
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code"). Sybase grants you a limited, non-exclusive, 
 non-transferable, revocable license to use, reproduce, and modify the Code 
 solely for purposes of (i) maintaining the Code as reference material to better 
 understand the operation of the Program, and (ii) development and testing of 
 applications created in connection with your licensed use of the Program. 
 The Code may not be transferred, sold, assigned, sublicensed or otherwise 
 conveyed (whether by operation of law or otherwise) to another party without 
 Sybase's prior written consent. The following provisions shall apply to any 
 modifications you make to the Code: (i) Sybase will not provide any maintenance 
 or support for modified Code or problems that result from use of modified Code; 
 (ii) Sybase expressly disclaims any warranties and conditions, express or 
 implied, relating to modified Code or any problems that result from use of the 
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING 
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN 
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
 to indemnify, hold harmless, and defend Sybase from and against any claims or 
 lawsuits, including attorney's fees, that arise from or are related to the 
 modified Code or from use of the modified Code.
 */
 
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SUPE2ETraceLevel.h"
#import "moDataTypes.h"

/*!
 @defined
 @abstract   Low priority of an application message.
 @discussion This indicates the priority of the message associated with a hybrid app.
             is low and UI can be tweaked based upon these to indicate this to the user.
 */
#define IMPORTANCE_LOW                 0

/*!
 @defined
 @abstract   Normal priority of an application message.
 @discussion This indicates the priority of the message associated with a hybrid app.
             is normal and UI can be tweaked based upon these to indicate this to the user.
 */
#define IMPORTANCE_NORMAL              1

/*!
 @defined
 @abstract   High priority of an application message.
 @discussion This indicates the priority of the message associated with a hybrid app.
             is high and UI can be tweaked based upon these to indicate this to the user.
 */
#define IMPORTANCE_HIGH                2

/*!
 @defined
 @abstract   Notification name associated with hybrid app collection changes.
 @discussion This notification can be used to indicate that a collection should be
             reloaded and the UI displaying this collection can be refreshed.
 */
#define HYBRIDAPP_CHANGED_NOTIFICATION           @"HybridAppChanged"

/*!
 @defined
 @abstract   Notification name associated with coalesced hybrid app collection changes.
 @discussion This notification can be used to indicate that a collection should be
 reloaded and the UI displaying this collection can be refreshed. As opposed
 to HYBRIDAPP_CHANGED_NOTIFICATION, these notifications are coalesced in the
 notification queue while waiting to be delivered. This notification can be
 used as a general indication that the entire collection should be refreshed.
 */
#define HYBRIDAPP_CHANGED_NOTIFICATION_COALESCE  @"HybridAppChangedWCoalescing"

#define METADATA_VERSION @"version"
#define kTraceLevel @"TraceLevel"

extern NSString *  ICON_WIDTH;
extern NSString *  ICON_HEIGHT;
extern NSString *  ICON_FILE_TYPE;
extern NSString *  ICON_XML_TYPE;
extern NSString *  ICON_NAME;
extern NSString *  ICON_PATH;
extern NSString *  ICON_PROCESSED_PATH;
extern NSString *  ICON_XML_PROCESSED_PATH;
extern NSString *  METADATA_ITEMS;

/*!
 @class CustomIcon
 @abstract Describes custom icon associated with app.
 @discussion Used to return custom icon information to application, so that application  
             can have its own logic to choose the proper icon image.
 */
@interface CustomIcon : NSObject
{
    int width;
    int height;
    NSString* type;
    NSString* name;
    NSString* imagePath;
    NSString* processedImagePath;
}

/*!
 @property
 @abstract Width of the icon
 */
@property (nonatomic, readonly) int width;

/*!
 @property
 @abstract Height of the icon
 */
@property (nonatomic, readonly) int height;

/*!
 @property
 @abstract Type of the icon
 @discussion Types will be either "png", "jpeg", "jpg", "bmp", or "gif".
 */
@property (nonatomic, readonly) NSString* type;

/*!
 @property
 @abstract Name of the icon
 */
@property (nonatomic, readonly) NSString* name;

/*!
 @property
 @abstract Path to the icon
 */
@property (nonatomic, readonly) NSString* imagePath;

/*!
 @property
 @abstract Path to the icon indicating app was processed
 */
@property (nonatomic, readonly) NSString* processedImagePath;

/*!
 @method
 @abstract   Initializer
 @discussion Initializes CustomIcon object
 @param iconWidth The width of the icon.
 @param iconHeight The height of the icon.
 @param imageType The type of the icon.
 @param iconName The name of the icon.
 @param iconImagePath The path to the icon.
 @param iconProcessedImagePath The path to the processed icon.
 @return The allocated and initialized object.
 */
-(id)initWithParams:(int)iconWidth height:(int)iconHeight type:(NSString*)imageType name:(NSString*)iconName path:(NSString*)iconImagePath ppath:(NSString*)iconProcessedImagePath;
@end

/*!
 @class ClientVariables
 @abstract Used to return client variables information to application.
 */
@interface ClientVariables : NSObject
{
   int version;
   NSUInteger _count;
   NSMutableDictionary* items;
   NSString* dataInJson;
}

/*!
 @property
 @abstract Version of the client variables
 */
@property (nonatomic, readonly) int version;

/*!
 @property
 @abstract Number of client variables
 */
@property (nonatomic, readonly) NSUInteger count;

/*!
 @method
 @abstract   Initializer
 @discussion Initializes ClientVariables object
 @param clientVariablesInJson Json document containing client variables.
 @return The allocated and initialized object.
 */
-(id)initWithParams:(NSString*) clientVariablesInJson;

/*!
 @method
 @abstract Retrieves all client variable names.
 @return Array of variable names.
 */
-(NSArray*) getAllVariableNames;

/*!
 @method
 @abstract Retrieves client value associated with name.
 @param name The client variable name.
 @return The value associated with name.
 */
-(NSString*) getVariableValueByName: (NSString*)name;

/*!
 @method
 @abstract Determines if a client variable name exists.
 @return True if name exists, false otherwise.
 */
-(Boolean) containsName: (NSString*) name;

/*!
 @method
 @abstract Retrieves the json document containing the client variables.
 @return The json document
 */
-(NSString*) getJsonString;

@end

/*!
 @class HybridApp
 @abstract The class describing a HybridApp that can be opened
 */
@interface HybridApp : NSObject {
   I32 moduleId;       // A unique number assigned to the HybridApp by the server.
   I32 moduleVersion;  // A number identifying the version of the module as specified in the manifest.
   I32 msgId;          // For HybridApps accessed from the inbox, a unique ID associated with the message.
                             // For HybridApps accessed from the HybridApps folder this will be -1.
   NSString* moduleName;     // The HybridApp name from the manifest
   NSMutableArray* customIconList;     // custom icon list.
   ClientVariables* clientVariables; //client variables

   BOOL isProcessed;         // Indicates whether HybridApp has been processed or not.  Needed internally by the class.
   I32 iconIndex;      // Used to determine icon to return from getImage.  Needed internally by the class.
   NSString* from;           // Who HybridApp is from, only applies to HybridApp messages
   NSString* subject;        // Subject of HybridApp, only applies to HybridApp messages
   NSDate* date;             // Received date, only applies to HybridApp messages
   BOOL readFlag;            // Indicates whether or not HybridApp has been read, only applies to HybridApp messages
   I32 priority;       // Indicates HybridApp priorities (see above), only applies to HybridApp messages
   BOOL isDefault;           // Indicates whether this HybridApp is the single default one that can be opened
   BOOL isPrepackaged;       // Is this HybridApp compiled into the client rather than received from the server?
}

/*!
 @property
 @abstract A unique number assigned to the HybridApp by the server.
 */
@property(nonatomic, assign) I32 moduleId;

/*!
 @property
 @abstract A number identifying the version of the module as specified in the manifest.
 */
@property(nonatomic, assign) I32 moduleVersion;

/*!
 @property
 @abstract For HybridApps accessed from the inbox, a unique ID associated with the message.
 @discussion For HybridApps accessed from the HybridApps folder this will be -1.
 */
@property(nonatomic, assign) I32 msgId;

/*!
 @property
 @abstract The HybridApp name from the manifest.
 */
@property(nonatomic, assign) NSString* moduleName;

/*!
 @property
 @abstract Indicates whether HybridApp has been processed or not.  Needed internally by the class.
 */
@property(nonatomic, assign) BOOL isProcessed;

/*!
 @property
 @abstract A unique number assigned to the HybridApp by the server.
 */
@property(nonatomic, assign) I32 iconIndex;

/*!
 @property
 @abstract Who HybridApp is from, only applies to HybridApp messages.
 */
@property(nonatomic, assign) NSString* from;

/*!
 @property
 @abstract Subject of HybridApp, only applies to HybridApp messages.
 */
@property(nonatomic, assign) NSString* subject;

/*!
 @property
 @abstract Received date, only applies to HybridApp messages.
 */
@property(nonatomic, assign) NSDate* date;

/*!
 @property
 @abstract Indicates whether or not HybridApp has been read, only applies to HybridApp messages.
 */
@property(nonatomic, assign) BOOL readFlag;

/*!
 @property
 @abstract Indicates HybridApp priorities (see above), only applies to HybridApp messages.
 */
@property(nonatomic, assign) I32 priority;

/*!
 @property
 @abstract Indicates whether this HybridApp is the single default one that can be opened.
 */
@property(nonatomic, assign) BOOL isDefault;

/*!
 @property
 @abstract Is this HybridApp compiled into the client rather than received from the server?
 */
@property(nonatomic, assign) BOOL isPrepackaged;

/*!
 @property
 @abstract List of custom icons associated with the HybridApp.
 */
@property(nonatomic, assign) NSMutableArray* customIconList;

/*!
 @property
 @abstract List of client variables associated with the client variables.
 */
@property(nonatomic, retain) ClientVariables* clientVariables;

/*!
 @method
 @abstract Used to sort HybridApps in the HybridApps folder.
 @param hybridApp - The app to compare
 @return The result of the comparison
 */      
- (NSComparisonResult)compareByName:(HybridApp*)hybridApp;

/*!
 @method
 @abstract Returns an image to display that is associated with the HybridApp.
 @discussion The image may change after the HybridApp is processed.
             The files contained within HybridAppImages.zip must be added to the applications project in order 
             for this routine to operate properly, otherwise nil will be returned.
 @return The image data of the HybridApp.
 */
- (UIImage*)getImage;

/*!
 @method
 @abstract Returns an image image that indicates the read flag and priority of the message.
 @discussion The image may change after the HybridApp is processed.
             The files contained within HybridAppImages.zip must be added to the applications .
             project in order for this routine to operate properly, otherwise nil will be returned.
 @return The image data of the HybridApp.
 */
- (UIImage*)getReadFlagImage;

/*!
 @method
 @abstract Returns a HybridApp associated with certain parameters.
 @discussion Internal class level function used internally.
 @param moduleId The module ID of the HybridApp to be retrieved.
 @param moduleVersion The moduleVersion of the HybridApp to be retrieved.
 @param bFromInstalledHybridApps Indicates whether to include all applications or just those that have been fully installed when searching.
 @return The HybridApp
 */
+ (HybridApp*) hybridAppWithModuleId:(UI32)moduleId withModuleVersion:(UI32)moduleVersion scope:(bool)bFromInstalledHybridApps;

/*!
 @method
 @abstract Returns a HybridApp representation of a message associated with certain parameters.
 @discussion Internal class level function used internally.
 @param msgId The message ID of the HybridApp to be retrieved.
 @return The HybridApp
 */
+ (HybridApp*) hybridAppWithMessageId:(UI32)msgId;

/*!
 @method
 @abstract Call this to install a prepackaged HybridApp that is included as a resource.
 @param path The absolute path to the folder.
 */
+ (BOOL)installPackageByClient:(NSString*)path;

/*!
 @method
 @abstract Retrieves image based upon an an index and whether or not the application has been processed.
 @param index The icon index specified within the manifest (typically set via the mobile designer).
 @param bProcessed Indicates whether the image is for the processed application or the non-processed application.
 @return The image, nil if the index is invalid
 */
+ (UIImage*) getBuiltInImageByIndex:(I32)index isProcessed:(BOOL)bProcessed;

/*!
 @method
 @abstract Retrieves image based upon an a custom icon.
 @param customIcon The icon that the image is associated with.
 @return The image
 */
- (UIImage*) getCustomIconImage:(CustomIcon*)customIcon;

/*!
 @method
 @abstract Retrieves image indicating application was processed based upon an a custom icon.
 @param customIcon The icon that the image is associated with.
 @return The image
 */
- (UIImage*) getCustomIconProcessedImage:(CustomIcon*)customIcon;

/*!
 @method
 @abstract Retrieves custom icon
 @return The custom icon selected with default logic, returning null if no suitable custom icon is available.
 */
- (CustomIcon*) getDefaultCustomIcon;
@end

typedef enum {
   UnknownHybridAppFolder,
   HybridAppFolder,
   HybridAppMessageFolder
} HybridAppFolderType;

typedef enum {
   HYBRIDAPP_OPERATION_NEW = 1,
   HYBRIDAPP_OPERATION_MOD = 2,
   HYBRIDAPP_OPERATION_DELETE = 3,
   HYBRIDAPP_OPERATION_DELETE_ALL = 4
} HybridAppOperation;

/*!
 @class HybridAppCollection
 @abstract The class describing a number of HybridApps.
 @discussion Each folder of HybridApps should maintain a collection.
             This collection can be populated via loadFromHybridAppFolder or loadFromInbox depending upon the folder in use.
             For HybridApp folders the collection should be accessed via hybridAppAtIndexFromHybridAppFolder.
             For Inbox folders the collection should be accessed via hybridAppFromMsgId.
 */
@interface HybridAppCollection : NSObject {
   NSMutableArray* hybridApps;
   HybridAppFolderType folderType;
}

/*!
 @property
 @abstract Number of HybridApp objects in the collection.
 */
@property (nonatomic, readonly) UI32 count;

/*!
 @property
 @abstract Array of HybridApp objects.
 */
@property (nonatomic, readonly, copy) NSMutableArray* hybridApps;

/*!
 @method
 @abstract Populates a HybridAppCollection for a folder.
 @param folderTypeToLoad Which folder to populate from.
 */
- (void)loadForFolder:(HybridAppFolderType)folderTypeToLoad;

/*!
 @method
 @abstract Populates a HybridAppCollection for a folder.
 @param folderTypeToLoad Which folder to populate from.
 @param ignoreDefault Determines whether or not to ignore the default application.
 */
- (void)loadForFolder:(HybridAppFolderType)folderTypeToLoad ignoringDefaultApp:(BOOL)ignoreDefault;

/*!
 @method
 @abstract Populates collection of HybridApps from database.
 @discussion If a default app is installed, it will only return that one,
             unless ignoreDefault is true
 */
- (void)loadServerInitiatedApps;

/*!
 @method
 @abstract Retrieves the folder type associated with the collection.
 @return The folder type of the collection.
 */
- (HybridAppFolderType)getFolderType;

/*!
 @method
 @abstract Retrieves the HybridApp from the collection at a particular index in the array.
 @param index The index into the array.
 @return The HybridApp at index.
 */
- (HybridApp*)hybridAppAtIndex:(UI32)index;

/*!
 @method
 @abstract Retrieves the HybridApp from the collection matching particular parameters.
 @param moduleId The module ID to match for the HybridApp object in the collection.
 @param moduleVersion The module version for the HybridApp object in the collection.
 @return The HybridApp object of nil if not found.
 */
- (HybridApp*)hybridAppFromModuleId:(UI32)moduleId fromModuleVersion:(UI32)moduleVersion;

/*!
 @method
 @abstract Retrieves the HybridApp message matching a particular message ID.
 @param msgId The message ID to match for the HybridApp object in the collection.
 @return The HybridApp object of nil if not found.
 */
- (HybridApp*)hybridAppFromMsgId:(UI32)msgId;

/*!
 @method
 @abstract Retrieves the number of unread messages in the collection.
 @return The number of unread messages in the collection.
 */
+ (UI32)unreadCount;

/*!
 @method
 @abstract Deletes a HybridApp object at an index in the collection.
 @param index The index into the collection array.
 */
- (void)deleteHybridAppAtIndex:(UI32)index;

/*!
 @method
 @abstract Deletes a HybridApp object with a particular module ID and module version.
 @param moduleId The module ID to match for the HybridApp object in the collection.
 @param moduleVersion The module version for the HybridApp object in the collection.
 */
- (void)deleteHybridAppWithModuleId:(UI32)moduleId withModuleVersion:(UI32)moduleVersion;

@end

/*!
 @class HybridAppViewController
 @abstract The class to create, open, and manipulate hybrid applications.
 */
@class HybridAppViewController;

/*!
 @protocol HybridAppViewControllerDelegate
 @discussion Different delegates relating to application submission, loading, and closing
 */

@protocol HybridAppViewControllerDelegate

/*!
 @method
 @abstract This delegate will be caled after a HybridApp submission occurs.
 @discussion A delegate should be implemented for each folder.
             Depending upon the HybridApp definition, some HybridApps need to be immediately reopened after a
             submission occurs.  If the hybridAppToReopen parameter is not nil this hybridApp should be opened by
             the calling view and the it is up to the caller to release this parameter.
             Whether the hybridAppToReopen is nil or not, typically the delegate should reload its collection
             when called.
 @param controller The controller doing the submission.
 @param hybridApp The app to open when this delegate is called, nil if not applicable.
 */
- (void)hybridAppViewController:(HybridAppViewController*)controller hybridAppToReopen:(HybridApp*)hybridApp;

/*!
 @method
 @abstract This delegate will be caled when the HybridApp starts loading.
 @param msgId The message ID associated with the application.
 @param moduleId The module ID associated with the application.
 @param moduleVersion The module version associated with the application.
 */
- (void)hybridAppDidStartLoad:(I32)msgId withModuleId:(I32)moduleId withModuleVersion:(I32)moduleVersion;

/*!
 @method
 @abstract This delegate will be caled when the HybridApp finishes loading.
 @param msgId The message ID associated with the application.
 @param moduleId The module ID associated with the application.
 @param moduleVersion The module version associated with the application.
 */
- (void)hybridAppDidFinishLoad:(I32)msgId withModuleId:(I32)moduleId withModuleVersion:(I32)moduleVersion;

/*!
 @method
 @abstract This delegate will be caled when the HybridApp closes.
 @param msgId The message ID associated with the application.
 @param moduleId The module ID associated with the application.
 @param moduleVersion The module version associated with the application.
 */
- (void)hybridAppDidClose:(I32)msgId withModuleId:(I32)moduleId withModuleVersion:(I32)moduleVersion;

@end

/*!
 @class HybridAppViewController
 @abstract The class that provides the ability to open an application.
 */
@interface HybridAppViewController : UIViewController

/*!
 @method
 @abstract Open an application on an iPhone.
 @param hybridApp The application to open.
 @param viewController The view controller that is currently opening the application.
 @param delegate The delegate to call back based upon application flow.
 */
+ (NSInteger)openWithHybridApp:(HybridApp*)hybridApp
                fromController:(UIViewController*)viewController
                      delegate:(id <HybridAppViewControllerDelegate>)delegate;

/*!
 @method
 @abstract Open an application on an iPad.
 @param hybridApp The application to open.
 @param viewController The view controller that is currently opening the application.
 @param delegate The delegate to call back based upon application flow.
 @param fullScreen Indicates whether or not to open the application in full screen mode or split screen mode.
 */
+ (NSInteger)openWithHybridApp:(HybridApp*)hybridApp
                fromController:(UIViewController*)viewController
                      delegate:(id <HybridAppViewControllerDelegate>)delegate
          inIpadFullScreenMode:(BOOL)fullScreen;

/*!
 @method
 @abstract Delete a hybrid application message.
 @param msgId The ID of the message to delete.
 */
+ (void)deleteHybridAppMsg:(UI32)msgId;

/*!
 @method
 @abstract Close the currently opened hybrid application message.
 */
+ (void)closeCurrentHybridAppMsg;

/*!
 @method
 @abstract Close the currently opened hybrid application, before opening a new one.
 @discussion Use this function to close a currently open app and immediately open a new one. Use this function
             instead of closeCurrentHybridAppMsg followed by openWithHybridApp to avoid problems with animation.
 @param hybridApp The application to open.
 @param viewController The view controller that is currently opening the application.
 @param delegate The delegate to call back based upon application flow.
 */
+ (void) closeCurrentHybridAppOpeningNewApp:(HybridApp*)hybridApp
                             fromController:(UIViewController*)viewController
                                   delegate:(id <HybridAppViewControllerDelegate>)delegate;

/*!
 @method
 @abstract Close the currently opened hybrid application, before opening a new one.
 @discussion Use this function to close a currently open app and immediately open a new one. Use this function
             instead of closeCurrentHybridAppMsg followed by openWithHybridApp to avoid problems with animation.
             This overload allows for setting full screen mode on iPad.
 @param hybridApp The application to open.
 @param viewController The view controller that is currently opening the application.
 @param delegate The delegate to call back based upon application flow.
 @param fullScreen Indicates whether or not to open the application in full screen mode or split screen mode.
 */
+ (void) closeCurrentHybridAppOpeningNewApp:(HybridApp*)hybridApp
                             fromController:(UIViewController*)viewController
                                   delegate:(id <HybridAppViewControllerDelegate>)delegate
                       inIpadFullScreenMode:(BOOL)fullScreen;

/*!
 @method
 @abstract Close the currently opened hybrid application message.
 */
+ (BOOL)isContainerHasHybridAppOpened;

/*!
 @method
 @abstract Let the application redraw itself. For example, the application should call this method when in-call status bar
           height gets changed in iphone portrait mode.
 */
+ (void)refreshView;

/*!
 @method
 @abstract Execute some javascript within the application.
 @param jsToExecute The script
 @param synchronously Indicates whether or not script should be ran synchronously.
 */
+ (void)executeJavaScript:(NSString*)jsToExecute synchronously:(BOOL)synchronously;

/*!
 @method
 @abstract Execute some javascript within the application.
 @param jsToExecute The script
 */
+ (NSString*)executeJavascriptWithReturnValue:(NSString*)jsToExecute;

/*!
 @method
 @abstract Call this to at the start of any HWC app
 @discussion Needs to be called before the datavault is created / unlocked
 */
+ (void)initApps;

@end

// Used whenever the collection changes
// folderType specifies which folder was changed
// operation indicates new, modifify, or delete
// msgId only applies to MessageFolder
/*!
 @class HybridAppChangeNotification
 @abstract Used in notification to indicate application collection changes.
 */
@interface HybridAppChangeNotification: NSObject
{
   HybridAppFolderType folderType;
   HybridAppOperation operation;
   I32 moduleId;
   I32 moduleVersion;
   I32 msgId;
}

/*!
 @property
 @abstract Specifies which folder was changed
 */
@property (nonatomic, readonly) HybridAppFolderType folderType;

/*!
 @property
 @abstract Module ID of application that changed.
 */
@property (nonatomic, readonly) I32 moduleId;

/*!
 @property
 @abstract Module version of application that changed.
 */
@property (nonatomic, readonly) I32 moduleVersion;

/*!
 @property
 @abstract ID of message that changed.
 @discussion Only applies to message folder.
 */
@property (nonatomic, readonly) I32 msgId;

/*!
 @property
 @abstract Which type of change occurred.
 @discussion Possible values are HYBRIDAPP_OPERATION_NEW, HYBRIDAPP_OPERATION_MOD, HYBRIDAPP_OPERATION_DELETE, and HYBRIDAPP_OPERATION_DELETE_ALL,
 */
@property (nonatomic, readonly) HybridAppOperation operation;

// Internal API used for object creation by internal code
/*!
 @method
 @abstract Internal API used for object creation.
 @param type Folder type of application.
 @param op Operation of change of application.
 @param moduleID Module ID of application.
 @param version Module version of application.
 @param msgID ID of message.
 */
- (id)initWithType:(HybridAppFolderType)type withOperation:(HybridAppOperation)op withModuleId:(I32)moduleID withModuleVersion:(I32)version withMsgId:(I32)msgID;

@end

@interface TraceHelper : NSObject
{   
}

+(BOOL)isTraceEnabled;
//if tracelevel is not available, return default MEDIUM tracelevel
+(SUPE2ETraceLevel)getTraceLevel;
//return NO if tracelevel is invalid
+(BOOL)setTraceLevel:(SUPE2ETraceLevel)traceLevel;
+(void)startTrace;
+(void)stopTrace;
+(NSData*)getTraceData;
+(BOOL)uploadTrace;
+(void)resetTraceData;

@end

