//
//  WidgetScreenController.h
//  Widgets
//
//  Created by Alexey Piterkin on 3/10/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "moDataTypes.h"

@class Widget;

// When move-to-sent-items feature is implemented,
// a new mode may appear here, e.g. kWidgetMessageModeMovedToSentItems
typedef enum {
   kWidgetMessageModeDefault = 0,
   kWidgetMessageModeProcessed,
   kWidgetMessageModeCredentials,
   kWidgetMessageModeActivation,
   kWidgetMessageModePostCredentials, //this is the mode after container receiving the credential request
   kWidgetMessageModePostActivate //this is the mode after container receiving the activate request
                                    //the above two new modes are used to tell container whether the workflow is using the new format or old format
                                    //if container gets an submit request within the above mode, then changing the mode to default mode
                                    
} WidgetMessageMode;

@class WidgetContext, WidgetScreenController;

@protocol WidgetScreenControllerDelegate

- (void)widgetScreenController:(WidgetScreenController*)controller submitResponse:(WidgetContext*)context;

@end



@interface WidgetContext : NSObject 

@property (nonatomic, readonly) NSDictionary * record;
@property (nonatomic, readonly) NSString * xml;
// @property (nonatomic, readonly) long moduleId;
// @property (nonatomic, readonly) long moduleVersion;
@property (nonatomic, readonly) I32 originalRecordId;
@property (nonatomic, retain) id context; // Anything the caller wants saved, e.g. MessageItem
@property (nonatomic, readonly) NSString * error;
@property (nonatomic) WidgetMessageMode mode;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSData * password;
@property (nonatomic, readonly) Widget * widget;
@property (nonatomic, retain) NSString * usernameKey;
@property (nonatomic, retain) NSString * passwordKey;

#ifndef TESTAPP
+ (WidgetContext*)contextForWidget:(Widget*)widget
                           message:(NSString*)message 
                          original:(NSString*)original 
                  originalRecordId:(I32)recordId
                              mode:(WidgetMessageMode)mode
                    screenOverride:(NSString*)targetScreen;
#endif

+ (WidgetContext*)contextForWidget:(Widget*)widget
                              mode:(WidgetMessageMode)mode
                    screenOverride:(NSString*)targetScreen;

- (void)saveCredentials:(NSString*)userName password:(NSData*)password;


@end


@interface WidgetScreenController : UIViewController

// The main method to be called to open a widget message. If the current view controller is inside a Navigation controller, 
// then the new controller will be automatically pushed, otherwise just returned as autoreleased.
// Either original and/or message can be nil.  With the present implementation, message will only be non-nil
// when we are re-opening already submitted message.
+ (WidgetScreenController*)screenControllerForWidgetContext:(WidgetContext*)context
                                                   delegate:(id <WidgetScreenControllerDelegate>)delegate
                             workflowViewControllerDelegate:(id)workflowControllerDelegate;

+ (void) showScreenController:(WidgetScreenController*)screenController fromController:(UIViewController*)viewController;

@end
