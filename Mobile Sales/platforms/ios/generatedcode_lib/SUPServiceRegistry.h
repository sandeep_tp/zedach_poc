/*
 Copyright (c) Sybase, Inc. 2012   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 */


#import "SUPExceptionMessageService.h"

/*!
 @class SUPServiceRegistry
 @abstract Service registry to hold implementation instance for various services used by the mobile framework.
 @discussion  Service registry to hold implementation instances for various services used by the entity framework and applications.  A service represented by a protocol can be registered with the registry offering extensibility to the entity framework.
 */
@interface SUPServiceRegistry : NSObject
{

}


/*!
 @method
 @abstract Get the shared singleton instance for the SUPServiceRegistry
 @discussion 
 @result SUPServiceRegistry instance
 */
+ (SUPServiceRegistry *)sharedInstance;

#pragma mark -
#pragma mark Service Management Methods
#pragma mark -

/*!
 @method
 @abstract Retrieve the registered component of specified type (protocol)
 @discussion 
 @param protocol Protocol to retrieve an implementation for
 @result The implementation class instance registered for the specified protocol
 */
- (id)getService:(Protocol *)protocol;

/*!
 @method
 @abstract Register an implementation instance for a specific protocol
 @discussion 
 @param protocol The protocol to register
 @param service The service implementation instance
 @result The previous implementation instance registered using the specified protocol
 */
- (id)registerService:(Protocol *)protocol withImplementation:(id)service;

/*!
 @method
 @abstract Unregister a previously registered component for the specific class/interface
 @discussion 
 @param  protocol The implementation instance implementing the protocol to unregister
 @result The previously registered implementation instance for the specified protocol
 */
- (id)unregisterService:(Protocol *) protocol;

@end
