#import "sybase_core.h"
#import "SUPBaseException.h"

@class SUPJsonSmsException;

@interface SUPJsonSmsException : SUPBaseException
{
}
/*!
 @method jsonSmsExceptionWithErrorCode
 @abstract  Instantiates an SUPJsonSmsException object
 @param  errorCode The error code value
 @result The SUPJsonSmsException object.
 @discussion
 */
+ (SUPJsonSmsException*)jsonSmsExceptionWithErrorCode:(int)errorCode;

/*!
 @method jsonSmsExceptionWithErrorCode
 @abstract  Instantiates an SUPJsonSmsException object
 @param  errorCode The error code value
 @param  message the error message
 @result The SUPJsonSmsException object.
 @discussion
 */
+ (SUPJsonSmsException *)jsonSmsExceptionWithErrorCode:(int)errorCode message:(NSString *)message;


/*!
 @method jsonSmsExceptionWithErrorCode
 @abstract  Instantiates an SUPJsonSmsException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @result The SUPJsonSmsException object.
 @discussion
 */
+ (SUPJsonSmsException*)jsonSmsExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments;

/*!
 @method jsonSmsExceptionWithErrorCode
 @abstract  Instantiates an SUPJsonSmsException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @param  cause The cause of the exception
 @result The SUPJsonSmsException object.
 @discussion
 */
+ (SUPJsonSmsException*)jsonSmsExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments cause:(NSException *)cause;

+ (SUPJsonSmsException*)getInstance;
+ (SUPJsonSmsException*)jsonSmsExceptionWithErrorCode:(int32_t)errorVal withMessage:(NSString*)message;
+ (SUPJsonSmsException*)jsonSmsExceptionWithErrorCode:(int32_t)errorVal withMessage:(NSString*)message argList:(NSArray*)arguments;
- (SUPJsonSmsException*)init;

@end
