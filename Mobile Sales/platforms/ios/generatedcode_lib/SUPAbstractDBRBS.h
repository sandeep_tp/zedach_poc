/*
 
 Copyright (c) Sybase, Inc. 2009-2012   All rights reserved.                                    
 
 In addition to the license terms set out in the Sybase License Agreement for 
 the Sybase Unwired Platform ("Program"), the following additional or different 
 rights and accompanying obligations and restrictions shall apply to the source 
 code in this file ("Code").  Sybase grants you a limited, non-exclusive, 
 non-transferable, revocable license to use, reproduce, and modify the Code 
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of 
 applications created in connection with your licensed use of the Program.  
 The Code may not be transferred, sold, assigned, sublicensed or otherwise 
 conveyed (whether by operation of law or otherwise) to another party without 
 Sybase's prior written consent.  The following provisions shall apply to any 
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or 
 implied, relating to modified Code or any problems that result from use of the 
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
 to indemnify, hold harmless, and defend Sybase from and against any claims or 
 lawsuits, including attorney's fees, that arise from or are related to the 
 modified Code or from use of the modified Code. 
 
*/

@class SUPApplication;

#import "sybase_sup.h"

#import "SUPDatabaseManager.h"
#import "SUPMessageListener.h"
#import "SUPParameterMetaData.h"
#import "SUPAttributeMetaDataRBS.h"
#import "SUPEntityMetaDataRBS.h"
#import "SUPOperationMetaData.h"
#import "SUPDatabaseMetaDataRBS.h"
#import "SUPOperationMap.h"
#import "SUPAttributeMap.h"
#import "SUPEntityMap.h"
#import "SUPDataType.h"
#import "SUPStatementWrapper.h"
#import "SUPConnectionWrapper.h"
#import "SUPObjectNotFoundException.h"
#import "SUPPersistenceException.h"
#import "SUPProtocolException.h"
#import "SUPQuery.h"
#import "SUPQueryResultSet.h"
#import "SUPSortOrder.h"
#import "SUPAttributeTest.h"
#import "SUPLogger.h"
#import "SUPObjectList.h"
#import "SUPStringList.h"
#import "SUPOnlineLoginStatus.h"
//#import "SUPConcurrentReadWriteLock.h"
#import "SUPSyncStatusListener.h"
/*
#import "PimContact.h"
#import "PimCalendar.h"
#import "PimTask.h"
#import "PimDraft.h"
#import "PimOutbox.h"
#import "PimInbox.h"
#import "PimSentItem.h"
#import "PimDeletedItem.h"
#import "PimNotepad.h"
*/
#import "SUPCallbackHandler.h"

@protocol SUPConnectionWrapper;
@protocol SUPQueueConnection;

@class SUPConnectionProfile;
@class SUPDatabaseDelegate;
@class SUPDatabaseMetaDataRBS;
@class SUPLocalTransaction;
@class SUPMessageListenerMap;
@class SUPExceptionMessageManager;

@interface SUPAbstractDBRBS : NSObject<SUPMessageListener>
{
    NSObject<SUPQueueConnection> *_queueConnection;
    SUPMessageListenerMap *_messageListenerMap;
    Class _concreteSubclass;
}

@property(readwrite, retain, nonatomic) NSObject<SUPLogger>          *logger;
@property(readonly, retain, nonatomic)  SUPConnectionProfile         *connectionProfile;
@property(readonly, retain, nonatomic)  id<SUPReadWriteLockManager>  dblock;
@property(readwrite, retain, nonatomic) NSObject<SUPCallbackHandler> *callbackHandler;
@property(readwrite, retain, nonatomic) NSObject<SUPQueueConnection> *queueConnection;
@property(readwrite, retain, nonatomic) SUPOnlineLoginStatus         *onlineLoginStatus;
@property(readwrite, retain, nonatomic) NSObject<SUPDatabaseManagerRBS> *manager;
@property(readwrite, retain, nonatomic) NSString                    *appName;
@property(readwrite, retain, nonatomic) SUPMessageListenerMap       *messageListenerMap;
@property(readonly)                     SUPDatabaseMetaDataRBS      *metaData;


#pragma mark -
#pragma mark Public methods inherited or overridden by generated database classes

/*!
 @method
 @abstract Returns the database metadata
 @discussion
 */
+ (SUPDatabaseMetaDataRBS*)metaData;

/*!
 @method
 @abstract Returns the database delegate
 @discussion
 */
+ (SUPDatabaseDelegate*)delegate;

/*!
 @method
 @abstract Returns the callback handler for the database class.
 @discussion
 */
+ (NSObject<SUPCallbackHandler>*)callbackHandler;
/*!
 @method
 @abstract Returns the connection profile.
 @discussion
 */
+ (SUPConnectionProfile*)connectionProfile;
/*!
 @method
 @abstract Returns the connection profile.
 @discussion
 */
+ (SUPConnectionProfile*)getConnectionProfile;
/*!
 @method
 @abstract Returns the synchronization profile.
 @discussion
 */
+ (SUPConnectionProfile*)getSynchronizationProfile;
/*!
 @method
 @abstract Creates the database and initializes the tables for all MBOs in this package.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)createDatabase;
/*!
 @method
 @abstract Drop all data from all tables: if keepClientOnly = YES, do not drop data from client-only tables.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)cleanAllData:(BOOL) keepClientOnly;
/*!
 @method
 @abstract Drop all data from all tables, except client-only tables.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)cleanAllData;
/*!
 @method
 @abstract Deletes the database for this package.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)deleteDatabase;
/*!
 @method
 @abstract Return true if database exists, false otherwise.
 @discussion
 */
+ (BOOL)databaseExists;
/*!
 @method
 @abstract Return the database connection for this package.
 @discussion
 */
+ (id<SUPConnectionWrapper>)getConnectionWrapper;
/*!
 @method
 @abstract Opens database connection.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)openConnection;
/*!
 @method
 @abstract Closes database connection.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)closeConnection;
/*!
 @method
 @abstract Clears database connection.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)clearConnection;

/*!
 @method
 @abstract Return the username used in communcations with server.
 @discussion
 */
+ (NSString*)getSyncUsername;
/*!
 @method
 @abstract Return the package name.
 @discussion
 */
+ (NSString*)getPackageName;
/*!
 @method
 @abstract Return the current server domain name.
 @discussion
 */
+ (NSString*)getDomainName;
/*!
 @method
 @abstract Return the database schema version of this package.
 @discussion
 */
+ (int32_t)getSchemaVersion;
/*!
 @method
 @abstract Return the protocol version of this package.
 @discussion
 */
+ (int32_t)getProtocolVersion;
/*!
 @method
 @abstract Return the logger for this package.
 @discussion
 */
+ (id<SUPLogger>)getLogger;
/*!
 @method
 @abstract Return the SynchronizationGroup instance corresponding to the given group name.
 @discussion
 */
+ (id<SUPSynchronizationGroup>) getSynchronizationGroup:(NSString*)syncGroup;
/*!
 @method
 @abstract Returns true if this sync group has ever been synchronized.
 @discussion
 */
+ (BOOL)isSynchronized:(NSString*)syncGroup;
/*!
 @method
 @abstract Returns the last time this group was synchronized, or nil if it has not been synchronized.
 @discussion
 */
+ (NSDate*)getLastSynchronizationTime:(NSString*)syncGroup;
/*!
 @method
 @abstract Execute the passed in query and return a result set.
 @discussion
 @throws SUPPersistenceException
 */
+ (SUPQueryResultSet*)executeQuery:(SUPQuery*)query;
/*!
 @method
 @abstract Set the callback handler for the database class.
 @discussion
 */
+ (void)registerCallbackHandler:(NSObject<SUPCallbackHandler>*)handler;
/*!
 @method
 @abstract Set Application instance.
 @discussion
 */
+ (void)setApplication:(SUPApplication*)application;
/*!
 @method
 @abstract Subscribe to the server.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)subscribe;

/*!
 @method
 @abstract Send a message to the server to synchronize this client with the specified synchronization groups.
 @discussion
 */
+ (void)beginSynchronize:(SUPObjectList*)synchronizationGroups withContext:(NSString*)context;
/*!
 @method
 @abstract Sets a flag to disable synchronization and cancel any ongoing synchronizations for this SUP package.
 @discussion
 */
+ (void)disableSync;
/*!
 @method
 @abstract Enables or re-enables synchronization for this SUP package.
 @discussion
 */
+ (void)enableSync;
/*!
 @method
 @abstract Returns true if synchronization is enabled, false otherwise.
 @discussion
 */
+ (BOOL)syncEnabled;
/*!
 @method
 @abstract Submit all the pending operations (ready for sending to server)
 @discussion
 @throws SUPPersistenceException
 */
+ (void)submitPendingOperations;
/*!
 @method
 @abstract Submit all the pending operations for the given synchronization group (ready for sending to server).
 @discussion
 @throws SUPPersistenceException
 */
+ (void)submitPendingOperations:(NSString*)synchronizationGroup;
/*!
 @method
 @abstract Search through all entities in this package and cancel any pending changes.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)cancelPendingOperations;
/*!
 @method
 @abstract Search through all entities in this package for the given synchronization group and cancel any pending changes.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)cancelPendingOperations:(NSString*)synchronizationGroup;
/*!
 @method
 @abstract return TRUE if there are replay pending requests, false otherwise.
 @discussion
 */
+ (BOOL)hasPendingOperations;
/*!
 @method
 @abstract End the current subscription to the server.
 @discussion
 */
+ (void)unsubscribe;
/*!
 @method
 @abstract Generate a new encryption key.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)generateEncryptionKey;
/*!
 @method
 @abstract Change the encryption key used for database encryption.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)changeEncryptionKey:(NSString *)newKey;
/*!
 @method
 @abstract Return log records matching the passed in query.
 @discussion
 @throws SUPPersistenceException
 */
+ (SUPObjectList*)getLogRecords:(SUPQuery*)query;
/*!
 @method
 @abstract Submit any pending client-created log records to the server.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)submitLogRecords;
/*!
 @method
 @abstract synchronize the synchronizationGroup with server.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)synchronize:(NSString*)synchronizationGroup;
/*!
 @method
 @abstract synchronize all the synchronizationGroups with server.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)synchronize;
/*!
 @method
 @abstract synchronize the synchronizationGroups with server, using custom syncStatusListener.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)synchronizeWithListener:(id<SUPSyncStatusListener>) listener;
/*!
 @method
 @abstract synchronize the synchronizationGroup with server, using custom syncStatusListener.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)synchronize:(NSString *)synchronizationGroup withListener:(id<SUPSyncStatusListener>)listener;
/*!
 @method
 @abstract return YES if there is no replay pending request, NO otherwise
 @discussion
 */
+ (BOOL) isReplayQueueEmpty;
/*!
 @method
 @abstract Retrieve currently queued background synchronization requests.
 @discussion
 */
+ (SUPObjectList*) getBackgroundSyncRequests;
/*!
 @method
 @abstract enable Change Log.
 @discussion
 */
+ (void) enableChangeLog;
/*!
 @method
 @abstract disable Change Log.
 @discussion
 */
+ (void) disableChangeLog;
/*!
 @method
 @abstract Get the log record based on the query filtering
 @discussion
 */
+ (SUPObjectList*) getChangeLogs:(SUPQuery*)query;
/*!
 @method
 @abstract Delete all the change logs in the client.
 @discussion
 */
+ (void) deleteChangeLogs;
/*!
 @method
 @abstract Get the MBO class name string from change log entity type.
 @discussion
 */
+ (NSString*) getEntityName:(int)entityType;
/*!
 @method
 @abstract Authenticate against the server.
 @discussion
 @throws SUPPersistenceException
 */
+ (void) onlineLogin;
/*!
 @method
 @abstract Begin a local transaction.
 @discussion
 */
+ (SUPLocalTransaction*)beginTransaction;

// Initialization

- (id)initWithName:(NSString*)dbName NS_DESIGNATED_INITIALIZER;



@end

@interface SUPAbstractDBRBS(internal)

- (id) init;

// Singleton instance method (implemented in generated code
+ (id)instance;

// Internal properties
+ (NSString*)defaultDomainName;
- (NSString*) getRemoteId;
/*abstract*/ - (NSString*)syncParamsVersion;
/*abstract*/ - (NSMutableDictionary*)getTableMBOMap;
/*abstract*/ - (NSString*)packageVersionedPrefix;

// Query methods
+ (SUPQueryResultSet*)executeQuery:(SUPDatabaseMetaDataRBS*)metaData query:(SUPQuery*)query;

// Metadata methods
+ (SUPDatabaseMetaDataRBS*)metaData;

// Log record methods
+ (void)uploadLogs;

// Subscription and login methods
- (void)storeCredential: (SUPString)user passwordHash:(NSUInteger)passHash;
- (void)notifyOnlineLoginFailed:(NSString *)reqID withMessage:(NSString *)msg withCode:(int32_t)code;
- (void)notifyOnlineLoginSuccess:(NSString *)reqID;

- (void)startBackgroundSynchronization;
- (void)stopBackgroundSynchronization;



// Large object instance creation
+ (SUPBigString*)createBigString;
+ (SUPBigBinary*)createBigBinary;
@end

