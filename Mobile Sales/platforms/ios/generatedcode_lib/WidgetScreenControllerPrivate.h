//
//  WidgetScreenController.h
//  Widgets
//
//  Created by Alexey Piterkin on 2/9/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WidgetScreenController.h"
#import "WidgetMessage.h"
#import "WorkflowAttachmentShowViewController.h"
#import "HybridAppHelper.h"

#define N_RECEIVED_STATUS_BAR_EVENT @"received_statusbar_change"

#define kWorkflowTempDirectory @"workflowtempfiles"
#define kTempDirectoryRegex    @"-tmp"

// constants added for fixing CR 678959 A considerable delay occurs after leaving the credential or activation screen 
extern NSString* const DEFAULT_SCREEN;     //defaultScreen
extern NSString* const ACTIVATION_SCREEN;  //activationScreen
extern NSString* const CREDENTIALS_SCREEN; //credentialsScreen
extern NSString* const CREDENTIAL_REFRESH; //credentialRefresh

@class ScreenDefinition, WidgetDefinition, RMIRequest, Widget, LocalSubstitutionCache, TraceHelper, AttachmentHelper, ImagePickerHelper;

@interface WidgetContextImpl : WidgetContext {
   Widget * widget;
   I32 originalRecordId;
   
   id context; // Anything the caller wants saved, e.g. MessageItem
   
   NSString * targetScreen;
   
   WidgetMessage * original;
   WidgetMessage * message;
   WidgetDefinition * definition;
   
   // These 2 are used for RMI - they store the keys for the credential controls used when submitting to RMI server. 
   // When RMI returns successfully - notifications for these 2 will be sent out to refresh values.
   NSString * usernameKey;
   NSString * passwordKey;
   
   NSString * error;
}

@property (nonatomic, readonly) WidgetMessage * original;
@property (nonatomic, readonly) WidgetMessage * message;
@property (nonatomic, readonly) WidgetDefinition * definition;
@property (nonatomic) WidgetMessageMode mode;
@property (nonatomic, readonly) NSString * targetScreen;
@property (nonatomic, readonly) I32 originalRecordId;
   
- (id)initWithWidget:(Widget *)widget
             message:(NSString*)aMessage 
            original:(NSString*)anOriginal 
    originalRecordId:(I32)aRecordId
                mode:(WidgetMessageMode)aMode
      screenOverride:(NSString*)aTargetScreen;

@end

@interface SimpleQueue : NSObject
{
   NSMutableArray* objects;
   int size;
}

@property (nonatomic, retain) NSMutableArray* objects;
@property (nonatomic, readonly) int size;

- (void) enqueue:(id)newObject;
- (id) dequeue;
- (id) next;
- (NSArray*) elements;
- (void) clear;

@end

@interface RMIRequestManager : NSObject {
   RMIRequest* outStandingRequest;
   NSThread* outstandingRequestThread;
   SimpleQueue* pendingRequests;
   NSThread* pendingQueueThread;
   BOOL pendingQueueThreadRunning;
   BOOL shouldShutDown;
   WidgetScreenControllerPrivate* controller;
}

@property (nonatomic, retain) NSThread* outstandingRequestThread;
@property (nonatomic, retain) SimpleQueue* pendingRequests;
@property (nonatomic, retain) NSThread* pendingQueueThread;

- (id) initWithScreenController:(WidgetScreenControllerPrivate*)screenController;

- (void) executeRmiForAction:(NSDictionary*)rmiParams
                     timeout:(NSTimeInterval)timeout
                cacheTimeout:(NSTimeInterval)expirySecs
                    cacheKey:(NSString*)theCacheKey
              forCachePolicy:(NSString *)cachePolicy
              isAsynchronous:(BOOL)asynchronous
                    withData:(NSString*)data
                     withKey:(NSString*)attKey
               withUniqueKey:(NSString*)uniqueKey
      withOnDownloadComplete:(NSString*)onDownloadComplete
                withUsername:(NSString*)newUsername
                withPassword:(NSString*)newPassword;

- (void) shutDown;

@end

@interface WidgetScreenControllerPrivate : WidgetScreenController <UIWebViewDelegate, UINavigationControllerDelegate> {
   WidgetDefinition* widget;
   ScreenDefinition* screen;
   WidgetMessage* message;
   NSMutableDictionary* record;
   
   UIScrollView* scroll;
   UIWebView* html;
   
   UIViewController* parent;
   
   NSMutableArray* views;
   
   // Invocation related variables
   id <WidgetScreenControllerDelegate> delegate;
   WidgetContextImpl* context;
   
   RMIRequestManager* rmiManager;
   
   UIView* curtain;
   UIActivityIndicatorView* spinner;
   
   UIAlertView* progressDialog;
   
   CGFloat screenHeight;
   CGFloat screenWidth;

   LocalSubstitutionCache* webCache;
      
   AttachmentHelper* attachmentHelper;
   
   ImagePickerHelper* imagePicker;
   
   id wfControllerDelegate;
   
   NSString* urlLangParam;
   
   BOOL showingSpinner;
   
   BOOL bIpadFullScreen;
   
   NSString* workflowTempDirRandomString;
}

@property (nonatomic, retain) WidgetContextImpl* context;
@property (nonatomic, assign) id <WidgetScreenControllerDelegate> delegate;

@property (nonatomic, assign) id wfControllerDelegate;

@property (nonatomic, readonly) WidgetDefinition* widget;
@property (nonatomic, readonly) ScreenDefinition* screen;
@property (nonatomic, readonly) NSMutableArray* views;
@property (nonatomic, readonly) WidgetMessage* message;
@property (nonatomic, readonly) NSMutableDictionary* record;
@property (nonatomic, readonly) UIWebView* html;
@property (nonatomic, retain) RMIRequestManager* rmiManager;
// Keeps track of the view controller chain nested within the navigation controller.
// Used to implement CloseAll functionality
@property (nonatomic, assign) UIViewController* parent;

@property (nonatomic, readonly) CGFloat screenHeight;
@property (nonatomic, readonly) CGFloat screenWidth;

@property (nonatomic, retain) LocalSubstitutionCache* webCache;

@property (nonatomic, retain) AttachmentHelper* attachmentHelper;

@property (nonatomic, retain) ImagePickerHelper* imagePicker;

@property (nonatomic, readwrite, retain) NSArray* supportedOrientations;

@property (nonatomic, readwrite, retain) UIView* curtain;
@property (nonatomic, readwrite, retain) UIActivityIndicatorView* spinner;

@property (nonatomic, readwrite, retain) UIAlertView* progressDialog;

@property (nonatomic, readwrite) BOOL bIpadFullScreen;

@property (nonatomic, retain) NSString* workflowTempDirRandomString;

- (void) htmlHybrid:(UIWebView*)webView withFile:(NSString*)file;
- (NSString*) getLangHtmlHybridFile:(NSString*)srcFile;
- (void) navigationTo:(NSString*)screenToGoTo;
- (void) executeJavaScript:(NSString*)javaScript synchronously:(BOOL)synchronous;
- (NSString*) executeJavaScriptWithReturnValue:(NSString*)strJsMethod;

// At the top level, the record data from aMessage will be the same as the record.  
// However, when implementing grid control, drilling down on a row will result in aRecord corresponding to
// the record on which the user just tapped, while aMessage will be the same as with the upper level controller.
- (id) initWithScreen:(ScreenDefinition*)aScreen message:(WidgetMessage*)aMessage record:(NSMutableDictionary*)aRecord workflowViewControllerDelegate:(id)workflowControllerDelegate;

- (NSArray*) addInterfaceOrientations;

- (void) openScreen:(NSString*)name;
- (void) close;
- (void) closeAllUntil:(UIViewController*)target;
- (void) closeWithAnimation:(BOOL)animated;

- (void) blockScreen;
- (void) unblockScreen;
- (void) addSpinner;

-(void) showProgressDialog:(NSString*)msg;
-(void)hideProgressDialog;

- (void) closeWaitDialogWithCallback:(NSString*)callback withDialogId:(NSString*)dialogId;

- (void) displayAttachment:(WorkflowAttachment*)att;

- (void) showSSOCertCredController;

- (void) setViewRotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;
+ (int) getIPhoneType;
- (void) getScreenWidth:(CGFloat*)width height:(CGFloat*)height forInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;
+ (CGFloat) getStatusBarFrameHeight;
- (CGFloat) setAndReturnFullScreenHeight;
// contentHeight not used in the whole HWC, so delete this method.

- (NSString*) getCurrentTempWorkflowFileDirectory;

+ (NSData*) unicodeFromPassword:(NSString*)password encryptingForServer:(BOOL)bEncrypt;
@end
