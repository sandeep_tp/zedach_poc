/*
 
 Copyright (c) Sybase, Inc. 2009-2012   All rights reserved.                                    
 
 In addition to the license terms set out in the Sybase License Agreement for 
 the Sybase Unwired Platform ("Program"), the following additional or different 
 rights and accompanying obligations and restrictions shall apply to the source 
 code in this file ("Code").  Sybase grants you a limited, non-exclusive, 
 non-transferable, revocable license to use, reproduce, and modify the Code 
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of 
 applications created in connection with your licensed use of the Program.  
 The Code may not be transferred, sold, assigned, sublicensed or otherwise 
 conveyed (whether by operation of law or otherwise) to another party without 
 Sybase's prior written consent.  The following provisions shall apply to any 
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or 
 implied, relating to modified Code or any problems that result from use of the 
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
 to indemnify, hold harmless, and defend Sybase from and against any claims or 
 lawsuits, including attorney's fees, that arise from or are related to the 
 modified Code or from use of the modified Code. 
 
*/


#import "sybase_sup.h"

#import "SUPMessageListener.h"
#import "SUPParameterMetaData.h"
#import "SUPAttributeMetaData.h"
#import "SUPAttributeMetaData_DC.h"
#import "SUPEntityMetaData.h"
#import "SUPOperationMetaData.h"
#import "SUPDatabaseMetaData.h"
#import "SUPOperationMap.h"
#import "SUPAttributeMap.h"
#import "SUPEntityMap.h"
#import "SUPDataType.h"
#import "SUPStatementWrapper.h"
#import "SUPConnectionWrapper.h"
#import "SUPObjectNotFoundException.h"
#import "SUPPersistenceException.h"
#import "SUPProtocolException.h"
#import "SUPQuery.h"
#import "SUPQueryResultSet.h"
#import "SUPSortOrder.h"
#import "SUPAttributeTest.h"
#import "SUPLogger.h"
#import "SUPObjectList.h"
#import "SUPStringList.h"
#import "SUPOnlineLoginStatus.h"
#import "SUPReadWriteLock.h"
#import "SUPApplication.h"

/*
#import "PimContact.h"
#import "PimCalendar.h"
#import "PimTask.h"
#import "PimDraft.h"
#import "PimOutbox.h"
#import "PimInbox.h"
#import "PimSentItem.h"
#import "PimDeletedItem.h"
#import "PimNotepad.h"
*/
#import "SUPCallbackHandler.h"

@protocol SUPConnectionWrapper;
@protocol SUPDatabaseManager;
@protocol SUPQueueConnection;

@class SUPConnectionProfile;
@class SUPDatabaseMetaData;
@class SUPLocalTransaction;
@class SUPMessageListenerMap;
@class SUPJsonInputStream;


// Abstract superclass of the generated database class for all packages
// that use message based synchronization (MBS).
//
// Methods here are only for use by generated code or by other SUP framework code,
// and should never be called directly by application code.

@interface SUPAbstractDB : NSObject<SUPMessageListener>
{
@protected
    SUPMessageListenerMap*  _messageListenerMap;
    NSObject<SUPQueueConnection> *_queueConnection;
    NSObject<SUPLogger>   *_logger;
    NSObject<SUPDatabaseManager> *_manager;
        
@private
    NSObject<SUPCallbackHandler> *_callbackHandler;
    SUPConnectionProfile         *_connectionProfile;
    NSString                     *_name;
    BOOL                          _backgroundSyncStarted;
    BOOL                          _needSync;
    SUPOnlineLoginStatus         *_onlineLoginStatus;
    NSString                     *_onlineLoginReqID;
    NSString                     *_appName;
}

@property(readwrite, retain, nonatomic) NSObject<SUPCallbackHandler> *callbackHandler;
@property(readonly, retain, nonatomic)  SUPConnectionProfile         *connectionProfile;
@property(readonly, copy, nonatomic)    NSString                     *name;
@property(readwrite, retain, nonatomic) NSObject<SUPQueueConnection> *queueConnection;
@property(readonly, retain, nonatomic)  SUPConnectionProfile         *synchronizationProfile;
@property(readwrite, retain, nonatomic) NSObject<SUPLogger>          *logger;
@property(readwrite, retain, nonatomic) SUPOnlineLoginStatus         *onlineLoginStatus;
@property(readwrite, copy, nonatomic)   NSString                     *onlineLoginReqID;
@property(readonly, retain, nonatomic)  id                           dblock;
@property(readwrite, retain, nonatomic) NSObject<SUPDatabaseManager>  *manager;

#pragma mark -
#pragma mark Public methods inherited or overridden by generated database classes

/*!
 @method
 @abstract Begin a local transaction.
 @discussion
 */
+ (SUPLocalTransaction*)beginTransaction;
/*!
 @method
 @abstract Returns the callback handler for the database class.
 @discussion
 */
+ (NSObject<SUPCallbackHandler>*)callbackHandler;
/*!
 @method
 @abstract Returns the connection profile.
 @discussion
 */
+ (SUPConnectionProfile*)connectionProfile;
/*!
 @method
 @abstract Returns the connection profile.
 @discussion
 */
+ (SUPConnectionProfile*)getConnectionProfile;
/*!
 @method
 @abstract Returns the synchronization profile.
 @discussion
 */
+ (SUPConnectionProfile*)getSynchronizationProfile;
/*!
 @method
 @abstract Creates the database and initializes the tables for all MBOs in this package.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)createDatabase;
/*!
 @method
 @abstract Drop all data from all tables: if keepClientOnly = YES, do not drop data from client-only tables.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)cleanAllData:(BOOL) keepClientOnly;
/*!
 @method
 @abstract Drop all data from all tables, except client-only tables.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)cleanAllData;
/*!
 @method
 @abstract Deletes the database for this package.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)deleteDatabase;
/*!
 @method
 @abstract Return true if database exists, false otherwise.
 @discussion
 */
+ (BOOL)databaseExists;
/*!
 @method
 @abstract Return the database connection for this package.
 @discussion
 */
+ (id<SUPConnectionWrapper>)getConnectionWrapper;
/*!
 @method
 @abstract Opens database connection.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)openConnection;
/*!
 @method
 @abstract Closes database connection.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)closeConnection;
/*!
 @method
 @abstract Return the username used in communcations with server.
 @discussion
 */
+ (NSString*)getSyncUsername;
/*!
 @method
 @abstract Return the package name.
 @discussion
 */
+ (NSString*)getPackageName;
/*!
 @method
 @abstract Return the current server domain name.
 @discussion
 */
+ (NSString*)getDomainName;
/*!
 @method
 @abstract Return the database schema version of this package.
 @discussion
 */
+ (int32_t)getSchemaVersion;
/*!
 @method
 @abstract Return the protocol version of this package.
 @discussion
 */
+ (int32_t)getProtocolVersion;
/*!
 @method
 @abstract Return the logger for this package.
 @discussion
 */
+ (id<SUPLogger>)getLogger;
/*!
 @method
 @abstract Return current login status: SUPLoginPending, SUPLoginFailure, or SUPLoginSuccess.
 @discussion
 */
+ (SUPOnlineLoginStatus*)getOnlineLoginStatus;
/*!
 @method
 @abstract Return a list of all synchronization groups defined for the package.
 @discussion
 */
+ (SUPObjectList*)getAllSynchronizationGroups;
/*!
 @method
 @abstract Return a list of the synchronization groups corresponding to the given list of group names.
 @discussion
 */
+ (SUPObjectList*)getSynchronizationGroups:(SUPStringList*)groupNames;
/*!
 @method
 @abstract Execute the passed in query and return a result set.
 @discussion
 @throws SUPPersistenceException
 */
+ (SUPQueryResultSet*)executeQuery:(SUPQuery*)query;
/*!
 @method
 @abstract Set the callback handler for the database class.
 @discussion
 */
+ (void)registerCallbackHandler:(NSObject<SUPCallbackHandler>*)handler;

/*!
 @method
 @abstract Set Application instance.
 @discussion
 */
+ (void)setApplication:(SUPApplication*)application;
/*!
 @method
 @abstract Deprecated API
 @discussion
 */
+ (void)beginOnlineLogin:(NSString *)user password:(NSString *)pass DEPRECATED_ATTRIBUTE;
/*!
 @method
 @abstract Validate with the server, after sync profile already set up with username/password or SSO certificate.
 @discussion
 */
+ (void)beginOnlineLogin;
/*!
 @method
 @abstract Deprecated API
 @discussion
 */
+ (BOOL)offlineLogin:(NSString *)user password:(NSString *)pass DEPRECATED_ATTRIBUTE;
/*!
 @method
 @abstract Deprecated API
 @discussion
 */
+ (BOOL)offlineLogin DEPRECATED_ATTRIBUTE;
/*!
 @method
 @abstract Resume the subscription to the server after a call to suspendSubscription.
 @discussion
 */
+ (void)resumeSubscription;
/*!
 @method
 @abstract Subscribe to the server.
 @discussion
 */
+ (void)subscribe;
/*!
 @method
 @abstract Send a message to the server to synchronize this client with all groups in the package.
 @discussion
 */
+ (void)beginSynchronize;
/*!
 @method
 @abstract (Deprecated) Send a message to the server to synchronize this client with the specified synchronization groups.
 @discussion
 */
+ (void)beginSynchronize:(NSString*)synchronizationGroups :(NSString*)context DEPRECATED_ATTRIBUTE;
/*!
 @method
 @abstract Send a message to the server to synchronize this client with the specified synchronization groups.
 @discussion
 */
+ (void)beginSynchronize:(SUPObjectList*)synchronizationGroups withContext:(NSString*)context;
/*!
 @method
 @abstract Search through all entities in this package and send any pending changes to the server.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)submitPendingOperations;
/*!
 @method
 @abstract Search through all entities in this package for the given synchronization group and send any pending changes to the server.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)submitPendingOperations:(NSString*)synchronizationGroup;
/*!
 @method
 @abstract Search through all entities in this package and cancel any pending changes.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)cancelPendingOperations;
/*!
 @method
 @abstract Search through all entities in this package for the given synchronization group and cancel any pending changes.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)cancelPendingOperations:(NSString*)synchronizationGroup;
/*!
 @method
 @abstract return TRUE if there are replay pending requests, false otherwise.
 @discussion
 */
+ (BOOL)hasPendingOperations;
/*!
 @method
 @abstract Suspend the current server subscription.
 @discussion
 */
+ (void)suspendSubscription;
/*!
 @method
 @abstract Send the recover message to the server to have it send all the current data for this client.
 @discussion
 */
+ (void)recover;
/*!
 @method
 @abstract End the current subscription to the server.
 @discussion
 */
+ (void)unsubscribe;
/*!
 @method
 @abstract Returns YES if package is currently subscribed to the server, NO otherwise.
 @discussion
 */
+ (BOOL)isSubscribed;

/*!
 @method
 @abstract Generate a new encryption key.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)generateEncryptionKey;
/*!
 @method
 @abstract Change the encryption key used for database encryption.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)changeEncryptionKey:(NSString *)newKey;
/*!
 @method
 @abstract Return log records matching the passed in query.
 @discussion
 @throws SUPPersistenceException
 */
+ (SUPObjectList*)getLogRecords:(SUPQuery*)query;
/*!
 @method
 @abstract Submit any pending client-created log records to the server.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)submitLogRecords;
/*!
 @method
 @abstract Execute VACUUM on database to clean up internal database structure.
 @discussion
 @throws SUPPersistenceException
 */
+ (void)vacuum;

+ (void)clearConnection;


+ (SUPQueryResultSet*)executeQuery:(SUPDatabaseMetaData*)metaData query:(SUPQuery*)query;
+ (SUPDatabaseMetaData*)metaData;
+ (void)uploadLogs;

// Initialization

- (id)initWithName:(NSString*)dbName NS_DESIGNATED_INITIALIZER;

@end

#pragma mark -
#pragma mark Internal methods


@interface SUPAbstractDB(internal)

- (id) init;
// Singleton getter (implemented in generated code)
+ (id)instance;

// Properties used internally by generated code
+ (int32_t)getSchemaVersion;
+ (int32_t)getProtocolVersion;
+ (NSString*)defaultDomainName;
- (NSString*)packageHeaderString;


// Methods for handling server messages
- (void)onMessage:(SUPJsonMessage*)message;
+ (void)writeLogFromHeader:(SUPJsonObject*)o :(SUPNullableString)mbo :(SUPNullableString)method;

// Methods that send messages to the server
- (void)beginSynchronize:(SUPLong)counter withContent:(SUPJsonObject*)content;
- (void)recover:(SUPLong)counter;
- (void)resume:(SUPLong)counter;
- (void)subscribe:(SUPLong)counter;
- (void)suspend:(SUPLong)counter;
- (void)unsubscribe:(SUPLong)counter;
- (void)replay:(SUPString)mbo withId:(SUPLong)counter withContent:(SUPJsonArray*)content;
- (void)search:(SUPString)mbo withId:(SUPLong)counter withContent:(SUPJsonArray*)content;
- (void)replay:(SUPString)mbo withId:(SUPLong)counter withContentStream:(SUPJsonInputStream*)content;
- (void)search:(SUPString)mbo withId:(SUPLong)counter withContentStream:(SUPJsonInputStream*)content;

// Login methods
- (void)asyncLogin:(SUPLong)counter;
- (BOOL)offlineLogin:(SUPString)user password:(SUPString)pass;
- (BOOL)offlineLogin;
- (void)notifyOnlineLoginFailed:(NSString *)reqID withMessage:(NSString *)msg withCode:(int32_t)code;
- (void)notifyOnlineLoginSuccess:(NSString *)reqID;

// Subscription status methods
- (BOOL)packageIsSubscribed;
- (BOOL)packageIsSubscribePending;
- (BOOL)packageIsSuspended;
- (BOOL)packageIsResumed;
- (void)trackSubscriptionStatusOnMethod:(SUPString)method andRequestID:(SUPLong)counter;
- (void)recordSubscriptionStatus:(SUPJsonMessage*)message;

// Synchronization methods
- (void)setNeedSync;
- (void)synchronizeIfNeeded;
- (void)synchronize;
- (void)storeCredential: (SUPString)user passwordHash:(NSUInteger)passHash;

// Internal methods for connecting package to messaging engine
- (void)startBackgroundSynchronization;
- (void)stopBackgroundSynchronization;

// Large object methods
+ (SUPBigString*)createBigString;
+ (SUPBigBinary*)createBigBinary;

// Internal metadata methods
+ (SUPAttributeMetaData_DC*)createAttributeMetaData:(SUPInt)a_id :(SUPString)a_name :(SUPDataType*)a_type;
+ (SUPOperationMetaData*)createOperationMetaData:(SUPInt)o_id :(SUPString)o_name :(SUPDataType*)o_type :(bool)o_isStatic;
+ (SUPParameterMetaData*)createParameterMetaData:(SUPInt)p_id :(SUPString)p_name :(SUPDataType*)p_type;


//-(int)writeToPim:(id)entity;


@end
