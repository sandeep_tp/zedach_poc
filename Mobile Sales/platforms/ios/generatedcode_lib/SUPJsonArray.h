/*
 
 Copyright (c) Sybase, Inc. 2009-2013   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */

#import "sybase_core.h"

@class SUPJsonObject;
@class SUPJsonValue;
@class SUPJsonWriter;

@class SUPJsonArray;

/*!
 @class
 @abstract Class representing an array of JSON values.
 @discussion
 */
@interface SUPJsonArray : SUPArrayList
{
}

+ (SUPJsonArray*)getInstance;
+ (SUPJsonArray*)listWithCapacity:(SUPInt)capacity;

#pragma mark -
#pragma mark Methods for accessing array items

/*!
 @method
 @abstract Returns the JSON value at the given index as an array.
 @discussion
 @param index The index of the value to get.
 @result The array.
 */
- (SUPJsonArray*)getArray:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as an object.
 @discussion
 @param index The index of the value to get.
 @result The object.
 */
- (SUPJsonObject*)getObject:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a boolean.
 @discussion
 @param index The index of the value to get.
 @result The boolean.
 */
- (SUPBoolean)getBoolean:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a string.
 @discussion
 @param index The index of the value to get.
 @result The string.
 */
- (SUPString)getString:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a binary.
 @discussion
 @param index The index of the value to get.
 @result The binary.
 */
- (SUPBinary)getBinary:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a char.
 @discussion
 @param index The index of the value to get.
 @result The char.
 */
- (SUPChar)getChar:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a byte.
 @discussion
 @param index The index of the value to get.
 @result The byte.
 */
- (SUPByte)getByte:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a short.
 @discussion
 @param index The index of the value to get.
 @result The short.
 */
- (SUPShort)getShort:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as an int.
 @discussion
 @param index The index of the value to get.
 @result The int.
 */
- (SUPInt)getInt:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a long.
 @discussion
 @param index The index of the value to get.
 @result The long.
 */
- (SUPLong)getLong:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as an integer.
 @discussion
 @param index The index of the value to get.
 @result The integer.
 */
- (SUPInteger)getInteger:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a decimal.
 @discussion
 @param index The index of the value to get.
 @result The decimal.
 */
- (SUPDecimal)getDecimal:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a float.
 @discussion
 @param index The index of the value to get.
 @result The float.
 */
- (SUPFloat)getFloat:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a double.
 @discussion
 @param index The index of the value to get.
 @result The double.
 */
- (SUPDouble)getDouble:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a date.
 @discussion
 @param index The index of the value to get.
 @result The date.
 */
- (SUPDate)getDate:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a time.
 @discussion
 @param index The index of the value to get.
 @result The time.
 */
- (SUPTime)getTime:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a dateTime.
 @discussion
 @param index The index of the value to get.
 @result The dateTime.
 */
- (SUPDateTime)getDateTime:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a nullable boolean.
 @discussion
 @param index The index of the value to get.
 @result The nullable boolean.
 */
- (SUPNullableBoolean)getNullableBoolean:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a nullable string.
 @discussion
 @param index The index of the value to get.
 @result The nullable string.
 */
- (SUPNullableString)getNullableString:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a nullable binary.
 @discussion
 @param index The index of the value to get.
 @result The nullable binary.
 */
- (SUPNullableBinary)getNullableBinary:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a nullable char.
 @discussion
 @param index The index of the value to get.
 @result The nullable char.
 */
- (SUPNullableChar)getNullableChar:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a nullable byte.
 @discussion
 @param index The index of the value to get.
 @result The nullable byte.
 */
- (SUPNullableByte)getNullableByte:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a nullable short.
 @discussion
 @param index The index of the value to get.
 @result The nullable short.
 */
- (SUPNullableShort)getNullableShort:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a nullable int.
 @discussion
 @param index The index of the value to get.
 @result The nullable int.
 */
- (SUPNullableInt)getNullableInt:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a nullable long.
 @discussion
 @param index The index of the value to get.
 @result The nullable long.
 */
- (SUPNullableLong)getNullableLong:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a nullable integer.
 @discussion
 @param index The index of the value to get.
 @result The nullable integer.
 */
- (SUPNullableInteger)getNullableInteger:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a nullable decimal.
 @discussion
 @param index The index of the value to get.
 @result The nullable decimal.
 */
- (SUPNullableDecimal)getNullableDecimal:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a nullable float.
 @discussion
 @param index The index of the value to get.
 @result The nullable float.
 */
- (SUPNullableFloat)getNullableFloat:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a nullable double.
 @discussion
 @param index The index of the value to get.
 @result The nullable double.
 */
- (SUPNullableDouble)getNullableDouble:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a nullable date.
 @discussion
 @param index The index of the value to get.
 @result The nullable date.
 */
- (SUPNullableDate)getNullableDate:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a nullable time.
 @discussion
 @param index The index of the value to get.
 @result The nullable time.
 */
- (SUPNullableTime)getNullableTime:(SUPInt)index;

/*!
 @method
 @abstract Returns the JSON value at the given index as a nullable dateTime.
 @discussion
 @param index The index of the value to get.
 @result The nullable dateTime.
 */
- (SUPNullableDateTime)getNullableDateTime:(SUPInt)index;

#pragma mark -
#pragma mark Methods for adding items


/*!
 @method
 @abstract Adds the given array value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addArray:(SUPJsonArray*)value;

/*!
 @method
 @abstract Adds the given object value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addObject:(SUPJsonObject*)value;

/*!
 @method
 @abstract Adds the given boolean value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addBoolean:(SUPBoolean)value;

/*!
 @method
 @abstract Adds the given string value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addString:(SUPString)value;

/*!
 @method
 @abstract Adds the given binary value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addBinary:(SUPBinary)value;

/*!
 @method
 @abstract Adds the given char value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addChar:(SUPChar)value;

/*!
 @method
 @abstract Adds the given byte value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addByte:(SUPByte)value;

/*!
 @method
 @abstract Adds the given short value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addShort:(SUPShort)value;

/*!
 @method
 @abstract Adds the given int value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addInt:(SUPInt)value;

/*!
 @method
 @abstract Adds the given long value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addLong:(SUPLong)value;

/*!
 @method
 @abstract Adds the given integer value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addInteger:(SUPInteger)value;

/*!
 @method
 @abstract Adds the given decimal value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addDecimal:(SUPDecimal)value;

/*!
 @method
 @abstract Adds the given float value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addFloat:(SUPFloat)value;

/*!
 @method
 @abstract Adds the given double value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addDouble:(SUPDouble)value;

/*!
 @method
 @abstract Adds the given date value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addDate:(SUPDate)value;

/*!
 @method
 @abstract Adds the given time value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addTime:(SUPTime)value;

/*!
 @method
 @abstract Adds the given dateTime value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addDateTime:(SUPDateTime)value;

/*!
 @method
 @abstract Adds the given nullable boolean value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addNullableBoolean:(SUPNullableBoolean)value;

/*!
 @method
 @abstract Adds the given nullable string value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addNullableString:(SUPNullableString)value;

/*!
 @method
 @abstract Adds the given nullable binary value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addNullableBinary:(SUPNullableBinary)value;

/*!
 @method
 @abstract Adds the given nullable char value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addNullableChar:(SUPNullableChar)value;

/*!
 @method
 @abstract Adds the given nullable byte value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addNullableByte:(SUPNullableByte)value;

/*!
 @method
 @abstract Adds the given nullable short value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addNullableShort:(SUPNullableShort)value;

/*!
 @method
 @abstract Adds the given nullable int value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addNullableInt:(SUPNullableInt)value;

/*!
 @method
 @abstract Adds the given nullable long value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addNullableLong:(SUPNullableLong)value;

/*!
 @method
 @abstract Adds the given nullable integer value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addNullableInteger:(SUPNullableInteger)value;

/*!
 @method
 @abstract Adds the given nullable decimal value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addNullableDecimal:(SUPNullableDecimal)value;

/*!
 @method
 @abstract Adds the given nullable float value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addNullableFloat:(SUPNullableFloat)value;

/*!
 @method
 @abstract Adds the given nullable double value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addNullableDouble:(SUPNullableDouble)value;

/*!
 @method
 @abstract Adds the given nullable date value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addNullableDate:(SUPNullableDate)value;

/*!
 @method
 @abstract Adds the given nullable time value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addNullableTime:(SUPNullableTime)value;

/*!
 @method
 @abstract Adds the given nullable dateTime value to the end of this JSON array.
 @discussion
 @param value The value to add.
 */
- (void)addNullableDateTime:(SUPNullableDateTime)value;

#pragma mark -
#pragma mark Generic array methods

/*!
 @method
 @abstract Generic method for returning the array item at a given index.
 @discussion
 @param index The index of the value to get.
 @result The value.
 */
- (id)item:(SUPInt)index;

/*!
 @method
 @abstract Generic method for adding a value to the end of this JSON array.
 @discussion
 @param item The value to add.
 */
- (void)add:(id)item;

/*!
 @method
 @abstract   Returns an SUPBoolean value that indicates whether item is present in the SUPObjectList.
 @discussion
 @param item The item.
 @result YES if item is present, otherwise NO.
 */
- (SUPBoolean)contains:(id)item;

/*!
 @method
 @abstract Removes all objects from the array.
 @discussion
 */
- (void)clear;

- (SUPJsonArray*)addThis:(id)item;
- (SUPJsonArray*)slice:(SUPInt)start :(SUPInt)finish;
- (SUPInt)push:(id)item;
- (id)pop;

#pragma mark -
#pragma mark Description (string representation) methods

- (NSString*)toString;
- (NSString*)description;
@end
