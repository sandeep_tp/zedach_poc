//
//  WidgetDefinition.h
//  iota
//
//  Created by Alexey Piterkin on 2/6/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WidgetParser.h"
#import "NSString+Widgets.h"
#include "platmacro.h"

@class ScreenDefinition, WidgetMessage, WidgetScreenControllerPrivate, ControlDefinition;

@interface WidgetDefinition : NSObject <Parseable> {

   NSMutableDictionary * screens;
   NSString * defaultScreen;
   
   NSMutableDictionary * strings;
   
   // XML parsing state
   BOOL parsingScreens;
   BOOL parsingResources;
   BOOL parsingStrings;
   BOOL parsingLang;
   BOOL parsingString;
   BOOL parsingScrolling;
   BOOL scrolling;
   BOOL zoom;
   BOOL parsingZoom;
   BOOL parsingAdjustWebviewFrame;
   BOOL adjustWebViewFrame;
   NSString * defaultLang;
   
   NSMutableDictionary * languages;
   NSMutableDictionary * currentLanguage;
   
   NSMutableDictionary * language; // language, selected in the end
   NSString * src;
}

@property (nonatomic, readonly) NSMutableDictionary * language;

// The following properties are only needed for unit test
@property (nonatomic, readonly) NSMutableDictionary * screens;
@property (nonatomic, readonly) NSString * defaultScreen;
@property (nonatomic, readonly) NSString * src;

@property (nonatomic, readonly) BOOL scrolling;
@property (nonatomic, readonly) BOOL zoom;
@property (nonatomic, readonly) BOOL adjustWebViewFrame;

// XML-parsing API
- (void)setDefaultScreen:(NSString*)screenKey;
- (void)addScreen:(ScreenDefinition*)screenDef;
- (void)setString:(NSString*)string forKey:(NSString*)key;
- (NSString*)stringForKey:(NSString*)key;
- (void)processAfterLoading;
- (void)setSrc:(NSString*)src;

// Public API
+ (WidgetDefinition*) widgetDefinitionFromString:(NSString*)body error:(NSString**)error;

// Presentation-related API
- (WidgetScreenControllerPrivate*)openingScreenForMessage:(WidgetMessage*)message workflowViewControllerDelegate:(id)workflowControllerDelegate;
- (WidgetScreenControllerPrivate*)screenForName:(NSString*)name message:(WidgetMessage*)message record:(NSMutableDictionary*)record workflowViewControllerDelegate:(id)workflowControllerDelegate;

@end
