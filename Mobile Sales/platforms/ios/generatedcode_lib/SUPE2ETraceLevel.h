/*
 Copyright (c) Sybase, Inc. 2012   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 */

/*!
 @enum
 @abstract The various trace levels available to be set in a passport.  
 @discussion Currently this class defines 4 trace levels for the passport viz. NONE,LOW,MEDIUM and HIGH which are easy to read/identify from the user's point of view 
 SUPE2ETraceLevel_NONE  Focus on client-side analysis - No traces are triggered on server-side at all
 SUPE2ETraceLevel_LOW   Focus on response-time-distribution analysis.How much time is spent on each server component or where is the bottleneck?
 SUPE2ETraceLevel_MEDIUM  Focus on performance analysis ñ Performance traces are triggered on server-side, e.g. Introscope Transaction Trace, ABAP Trace, SQL Traces etc.
 SUPE2ETraceLevel_HIGH  Focus on functional analysis ñ Activation of detailed functional logging & tracing. 
 */
typedef enum
{
    SUPE2ETraceLevel_NONE = 0,
    SUPE2ETraceLevel_LOW = 1,
    SUPE2ETraceLevel_MEDIUM = 2,
    SUPE2ETraceLevel_HIGH = 3,
} SUPE2ETraceLevel;

#import <Foundation/Foundation.h>
/*!
 @class SUPE2ETraceLevelHelper 
 @abstract A helper class that maps the appropriate levels understood by the passport creation API Also provides other helper methods to handle the trace levels.
 @discussion 
 */ 
@interface SUPE2ETraceLevelHelper : NSObject
{
}


/*!
 @method 
 @abstract Gets the mapped passport trace level
 @param SUPE2ETraceLevel
 @return The passport's TraceLevel
 */
+ (int)mappedPassportTraceLevel:(SUPE2ETraceLevel)level;

/*!
 @method 
 @abstract Checks if the given trace level is amongst the valid passport trace levels defined for client
 @param level
 @return YES/NO if valid/invalid
 */
+ (BOOL)isValid:(SUPE2ETraceLevel)level;


/*!
 @method
 @abstract Constructs a E2ETraceLevel from the given int value.
 @param level
 @return trace level constructed from the given int value
 */
+ (SUPE2ETraceLevel)fromIntValue:(int)level;


@end
