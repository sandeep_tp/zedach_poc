/*
 Copyright (c) Sybase, Inc. 2012   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 */

#import "SUPE2ETraceLevel.h"

/*!
 @protocol 
 @abstract A protocol to handle E2E trace process and trace data transport management on the device client.
 <p>Client application wanting to use/include e2e tracing feature should be using this API</p>
 <p>This interface provides following functions:</p>
 <p>1. Start the e2e trace from client side when requested by user.</p>
 <p>2. Create a new passport  and make it available to the application code for sending to server with every new request</p>
 <p>3. Log each client request and received response in an in memory BTX (Business Transaction XML) structure.</p>
 <p>4. Upload the BTX to server after end of tracing session when requested by user.</p>  
 <p>
 @discussion
 */
@protocol SUPE2ETraceService <NSObject>

/*!
 @method
 @abstract Returns whether the client application has enabled (started) e2e trace.
 @return YES if it's enabled; otherwise, NO
 */
- (BOOL)isTraceEnabled;

/*!
 @method
 @abstract Set the passport trace level  
 @param traceLevel
 */
- (void)setTraceLevel:(SUPE2ETraceLevel)traceLevel;


/*!
 @method
 @abstract  Start tracing user actions and requests
 @discussion throws SUPE2ETraceIllegalStateException if tracing has already started.
 */
- (void)startTrace; 

/*!
 @method
 @abstract End tracing user actions and requests. Also completes the BTX creation and returns the BTX byte array.
 @discussion throws SUPE2ETraceIllegalStateException if tracing has not yet started.
 @return the in-memory btx byte array
 */
-(NSData *)stopTrace;	

/*!
 @method
 @abstract  Upload the given e2e trace xml (Business Transaction Xml - BTX) to the server.
 @discussion Please note that this is a BLOCKING call and recommended to be executed in a separate thread to avoid blocking the UI thread.
 @param btx The business transaction xml to be uploaded
 @return YES/NO depending on upload success/failure
 */
- (BOOL)uploadTrace:(NSData *)btx;

@end
