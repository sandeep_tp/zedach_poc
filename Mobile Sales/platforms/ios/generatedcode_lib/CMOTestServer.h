/*
Copyright (c) Sybase, Inc. 2012 All rights reserved. 

In addition to the license terms set out in the Sybase License Agreement for 
the Sybase Unwired Platform ("Program"), the following additional or different 
rights and accompanying obligations and restrictions shall apply to the source 
code in this file ("Code"). Sybase grants you a limited, non-exclusive, 
non-transferable, revocable license to use, reproduce, and modify the Code 
solely for purposes of (i) maintaining the Code as reference material to better 
understand the operation of the Program, and (ii) development and testing of 
applications created in connection with your licensed use of the Program. 
The Code may not be transferred, sold, assigned, sublicensed or otherwise 
conveyed (whether by operation of law or otherwise) to another party without 
Sybase's prior written consent. The following provisions shall apply to any 
modifications you make to the Code: (i) Sybase will not provide any maintenance 
or support for modified Code or problems that result from use of modified Code; 
(ii) Sybase expressly disclaims any warranties and conditions, express or 
implied, relating to modified Code or any problems that result from use of the 
modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING 
TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN 
IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
to indemnify, hold harmless, and defend Sybase from and against any claims or 
lawsuits, including attorney's fees, that arise from or are related to the 
modified Code or from use of the modified Code.
*/

/******************************************************************************
* Copyright 2012 Sybase, Inc
* Source File        : Cmotestserver.h
* Date Created       : 08/10/04 08:43:54
* Description        : Automatically generated source file containing the 
*                      Cmotestserver class interface implementation.
******************************************************************************/

#ifndef CMOTESTSERVER_H
#define CMOTESTSERVER_H

#include "moClient.h"

class MOTestServerClass : public CmoObject
{

public:
   MOTestServerClass( CmoConnection* pConn );

   void CleanLogs( CmoRequestOptions* RequestOptions );


   void LogTestStart( long lTestId, 
                      CmoRequestOptions* RequestOptions );


   void LogTestEnd( long lTestId, 
                    CmoRequestOptions* RequestOptions );


   void LogFailure( long lTestId, 
                    CmoString* sFilename, 
                    long lLine, 
                    long lErrorCode, 
                    CmoString* sErrorMsg, 
                    CmoRequestOptions* RequestOptions );


   bool CheckRecordSet( CmoRecordset* MungedRS, 
                        CmoRecordset* ControlRS, 
                        CmoRequestOptions* RequestOptions );


   void MasterLog( CmoString* strTestSuite, 
                   CmoString* strTestName, 
                   short iTestNum, 
                   CmoString* strPlatform, 
                   bool bPassed, 
                   CmoString* strBuild, 
                   CmoString* StrErrorMessage, 
                   CmoRequestOptions* RequestOptions );


   void UpdateObject( CmoString* strAdminUserName, 
                      CmoString* strAdminPwd, 
                      short Version, 
                      CmoString* strErr, 
                      CmoRequestOptions* RequestOptions );


   long GetTimesCalled( long SleepVal, 
                        CmoRequestOptions* RequestOptions );


   long PassRecordset( CmoRecordset* rs, 
                       CmoString* Command, 
                       CmoRequestOptions* RequestOptions );


   long CloneRecordset( CmoRecordset* rs, 
                        CmoRequestOptions* RequestOptions );


   long CopyRecordset( CmoRecordset* rs, 
                       CmoRecordset* SourceRS, 
                       CmoRequestOptions* RequestOptions );


   CmoRecordset* GetRecordset( CmoRequestOptions* RequestOptions );


   long GetLogRS( CmoRecordset* rs, 
                  CmoRequestOptions* RequestOptions );


   void ThrowException( CmoRequestOptions* RequestOptions );


   void SendLargePacket( CmoString* sString1, 
                         long l, 
                         CmoString* sString2, 
                         long l2, 
                         CmoRequestOptions* RequestOptions );


   CmoString* ReturnString( CmoString* s, 
                            CmoRequestOptions* RequestOptions );


   CmoString* ReturnEmptyString( CmoString* s, 
                                 CmoRequestOptions* RequestOptions );


   CmoString* ReturnNullString( CmoString* s, 
                                CmoRequestOptions* RequestOptions );


   bool ReturnBoolean( bool b, 
                       CmoRequestOptions* RequestOptions );


   bool ReturnNullBoolean( bool b, 
                           CmoRequestOptions* RequestOptions );


   float ReturnSingle( float f, 
                       CmoRequestOptions* RequestOptions );


   float ReturnNullSingle( float f, 
                           CmoRequestOptions* RequestOptions );


   double ReturnDouble( double d, 
                        CmoRequestOptions* RequestOptions );


   double ReturnNullDouble( double d, 
                            CmoRequestOptions* RequestOptions );


   long ReturnLong( long l, 
                    CmoRequestOptions* RequestOptions );


   long ReturnNullLong( long l, 
                        CmoRequestOptions* RequestOptions );


   short ReturnInt( short i, 
                    CmoRequestOptions* RequestOptions );


   short ReturnNullInt( short i, 
                        CmoRequestOptions* RequestOptions );

#if !defined( MOCLIENT_IPHONE ) 
#if !defined( USE_MOBILE_OBJECTS )
   CURRENCY ReturnCurrency( CURRENCY c, 
                            CmoRequestOptions* RequestOptions );


   CURRENCY ReturnNullCurrency( CURRENCY c, 
                                CmoRequestOptions* RequestOptions );


   DATE ReturnDate( DATE d, 
                    CmoRequestOptions* RequestOptions );


   DATE ReturnNullDate( DATE d, 
                        CmoRequestOptions* RequestOptions );
#endif
#endif

   CmoRecordset* ReturnRS( CmoRecordset* rs, 
                           CmoRequestOptions* RequestOptions );


   CmoRecordset* ReturnEmptyRS( CmoRecordset* rs, 
                                CmoRequestOptions* RequestOptions );


   CmoRecordset* ReturnDeletedRS( CmoRecordset* rs, 
                                  CmoRequestOptions* RequestOptions );


   long CompareTestData( CmoRecordset* rs, 
                         long NumRecords, 
                         CmoRequestOptions* RequestOptions );

};


#endif
