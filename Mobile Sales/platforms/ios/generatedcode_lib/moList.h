/*
Copyright (c) Sybase, Inc. 2012 All rights reserved. 

In addition to the license terms set out in the Sybase License Agreement for 
the Sybase Unwired Platform ("Program"), the following additional or different 
rights and accompanying obligations and restrictions shall apply to the source 
code in this file ("Code"). Sybase grants you a limited, non-exclusive, 
non-transferable, revocable license to use, reproduce, and modify the Code 
solely for purposes of (i) maintaining the Code as reference material to better 
understand the operation of the Program, and (ii) development and testing of 
applications created in connection with your licensed use of the Program. 
The Code may not be transferred, sold, assigned, sublicensed or otherwise 
conveyed (whether by operation of law or otherwise) to another party without 
Sybase's prior written consent. The following provisions shall apply to any 
modifications you make to the Code: (i) Sybase will not provide any maintenance 
or support for modified Code or problems that result from use of modified Code; 
(ii) Sybase expressly disclaims any warranties and conditions, express or 
implied, relating to modified Code or any problems that result from use of the 
modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING 
TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN 
IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
to indemnify, hold harmless, and defend Sybase from and against any claims or 
lawsuits, including attorney's fees, that arise from or are related to the 
modified Code or from use of the modified Code.
*/

/******************************************************************************
*    Copyright 2012 Sybase, Inc
*    Source File            : moList.h
*    Platform Dependencies  :
*    Description            : Header file for list class.
*
*    Notes                  :
******************************************************************************/

#ifndef CmoList_H_INCLUDED
#define CmoList_H_INCLUDED

#include "moOS.h"
#include "moTypes.h"

#if defined( MOCLIENT_IPHONE ) || defined( USE_MOBILE_OBJECTS )
#include "tchar.h"
#endif

#ifdef _WIN32
   #include "moThreadSafe.h"
#endif

#define CmoListSUCCESS  0
#define CmoListERROR    -1


class CmoListItem;
class CmoListHead;

typedef CmoListItem* PListItem;
typedef CmoListHead* PListHead;
typedef void* PData;


typedef void ( *TOnClearEvent )( void* pData );


// CmoListItem
//***********************************************************
class CmoListItem
{
public:
   CmoListItem();
   ~CmoListItem();

   PData getData();
   void setData( PData Value );

   PListItem getNext();
   void setNext( PListItem Value );

   PListItem getPrev();
   void setPrev( PListItem Value );

private:
   PData FpData;

   PListItem FpNext;

   PListItem FpPrev;
};// CmoListItem
//***********************************************************



// CmoListHead
//***********************************************************
class CmoListHead
{
private:
   PListItem FpFirstItem;
public:
   CmoListHead();
   ~CmoListHead();
   PListItem getFirstItem();
   void setFirstItem( PListItem Value );
};



// CmoList
//***********************************************************
class CmoList
{
public:
   CmoList( TOnClearEvent OnClearEvent );
   ~CmoList();


   I32 Delete();
   I32 Delete( I32 ulPosition );
   I32 Delete( PListItem pItem );

   bool getEOL();
   bool getBOL();

   bool AtEOF(){ return getEOL();}
   bool AtBOF(){ return getBOL();}

   I32 getCount();

   TCHAR* getName();
   void setName( TCHAR* pucName );

   void Insert( I32 lPosition,
                void* pData,
                bool& bSuccess );

   void Add( void* pData );

   void Find( void* pData,
              I32& ulPos );

   PListItem ItemAtPos( I32 lPosition );

   bool GotoItem( void* pData );

   PListItem getCurrent();

   PListItem First();
   PListItem Next();
   PListItem Prior();
   PListItem Last();

   void DoOnClear( void* pData );

   void ClearList();


   void setBookMark( void* pBookMark );
   void* getBookMark();
   void setOnClearEvent( TOnClearEvent m_pfnValue ){ m_pfnOnClear = m_pfnValue;}

   void MoveFirstItem( CmoList *poFromList );

private:
#ifdef _WIN32
   CmoCriticalSection m_CS;
#endif

   bool m_bEOL;
   bool m_bBOL;
   I32 m_lCount;
   TCHAR* FpucName;
   PListHead FpListHead;
   PListItem FpListTail;
   PListItem FpCurrentItem;
   void setCurrent( PListItem pItem );
   void setCount( I32 ulCount );
   void setEOL( bool Value );
   void setBOL( bool Value );
   void InternalInit();

   TOnClearEvent m_pfnOnClear;


protected:
   PListHead getListHead();

   void setListHead( PListHead Value );

   PListItem getListTail();

   void setListTail( PListItem Value );

};// CmoList
//***********************************************************



#endif

