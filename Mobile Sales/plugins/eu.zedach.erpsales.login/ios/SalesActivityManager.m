//
//  SalesActivityManager.m
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-08-12.
//
//

#import "SalesActivityManager.h"
#import "SalesPlusSalesActivity.h"
#import "SUPAbstractLocalEntity+ERPSales.h"
#import "NSDictionary+ERPSales.h"
#import "SUPCompositeTest.h"
#import "SUPAttributeTest.h"
#import "SUPDataValueList.h"
#import "SalesPlusERPSalesPlus_SuperUserDB.h"
#import "SUPDataValue.h"
#import "SUPSortCriteria.h"
#import "SUPAttributeSort.h"
#import "UserManager.h"
#import "SalesPlusUser.h"
#import "CustomerManager.h"
#import "SalesOrderManager.h"
#import "SalesPlusSalesDocument.h"
#import "CustomizingManager.h"
#import "UserManager.h"

@implementation SalesActivityManager
@synthesize salesActivityKeys = _salesActivityKeys;

+ (id) defaultManager{
    static SalesActivityManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[SalesActivityManager alloc]initWithTableName:@"SalesActivity"];
        manager.salesActivityKeys = [NSArray arrayWithObjects:
                                     @"DOC_NUMBER",     // Document No. or VBELN
                                     @"ACTIVITY_TYPE",  // Activity type
                                     @"SALESORG",       // Sales Organization
                                     @"DISTR_CHAN",     // Distribution Channel
                                     @"DIVISION",       // Division
                                     @"SALES_OFF",
                                     @"SALES_GRP",
                                     @"FROM_DATE",      // KTABG
                                     @"FROM_TIME",
                                     @"TO_DATE",
                                     @"TO_TIME",
                                     @"ACTIVITY_COMMENT",// Note
                                     @"RESULT",
                                     @"PARTN_ID",       // partner No. or KUNNR
                                     @"CONTACT",        // Contact
                                     @"CREATED_BY",     // Creates By
                                     @"STATE",          // Status
                                     @"VBELV_SA",       // Reference SA
                                     @"VBELV_SD",       // Quotation is linked for SA
                                     nil];
    });
    return manager;
}

- (NSDictionary *)salesActivityForVBELN:(NSString *)vbeln{
    SalesPlusSalesActivity *salesActivity = [SalesPlusSalesActivity findByPrimaryKey:vbeln];
    if (salesActivity){
        NSMutableDictionary *temp = [salesActivity toDictionaryWithKeys:self.salesActivityKeys];
        [temp addEntriesFromDictionary:[self getSalesActivityAdditonalInfo:salesActivity]];
        return temp;
    }
    return [NSDictionary dictionary];
}

- (NSArray *)getSalesActivityListFrom:(int)skip To:(int)take{
    //    SUPQuery *query = [self selectAllQuery:self.salesActivityKeys];
    //    // Define the ordering:
    //    SUPSortCriteria *sort = [SUPSortCriteria getInstance];
    //    [sort add:[SUPAttributeSort descending:@"FROM_DATE"]];
    //    query.sortCriteria = sort;
    //    SUPObjectList *salesActivityList = [SalesPlusSalesActivity findWithQuery:query];
    SUPObjectList *salesActivityList = [SalesPlusSalesActivity findSortedAll:skip take:take];
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:salesActivityList.length];
    
    for (SalesPlusSalesActivity *sa in salesActivityList) {
        NSMutableDictionary *activityDict = [sa toDictionaryWithKeys:self.salesActivityKeys];
        [activityDict addEntriesFromDictionary:[self getSalesActivityAdditonalInfo:sa]];
        [result addObject:activityDict];
    }
    
    return result;
}

// Queries the database for the given value on multiple fields.
- (NSArray *)searchSalesActivityWithCriteria:(NSString *)searchString{
    // get the language the useris logged in
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString* preferredLang = [languages objectAtIndex:0];
    NSString *language = [[preferredLang capitalizedString] substringToIndex:1];
    
    NSArray *searchKeys = [NSArray arrayWithObjects:
                           @"sa.DOC_NUMBER",
                           @"sa.ACTIVITY_TYPE", //Activity type
                           @"sa.ACTIVITY_COMMENT",
                           @"sa.FROM_DATE",
                           @"c.NAME1",
                           @"cuKTAAR.VTEXT",
                           nil];
    
    SUPQuery *query = [SUPQuery getInstance];
    [query select:@"sa.DOC_NUMBER"];
    [query from:@"SalesActivity":@"sa"];
    [query join:@"Customer":@"c":@"c.KUNNR":@"sa.PARTN_ID"];
    [query join:@"CustKTAAR":@"cuKTAAR":@"sa.ACTIVITY_TYPE":@"cuKTAAR.KTAAR"];
    [query orderBy:@"sa.FROM_DATE" :[SUPSortOrder DESCENDING]];
    NSArray* searchStringTokens = [searchString componentsSeparatedByString: @" "];
    SUPCompositeTest *outerCompositeTest = [SUPCompositeTest getInstance];
    [outerCompositeTest setOperator:SUPCompositeTest_AND];
    for (int i = 0; i < searchStringTokens.count; i++){
        SUPCompositeTest *ct = [SUPCompositeTest getInstance];
        [ct setOperator:SUPCompositeTest_OR];
        for (NSString *key in searchKeys) {
            SUPAttributeTest *at = [SUPAttributeTest getInstance];
            at.attribute = key;
            //check if the searchstring is a date
            NSString *searchStringToken = [searchStringTokens objectAtIndex:i];
            NSDate *d;
            NSDateFormatter* df = [[NSDateFormatter alloc] init];
            if ([language isEqualToString:@"D"]){
                [df setDateFormat:@"MM/dd/yyyy"];
                d = [df dateFromString:searchStringToken];
                if (!d){
                    [df setDateFormat:@"MM.dd.yyyy"];
                    d = [df dateFromString:searchStringToken];
                }
            }else if ([language isEqualToString:@"E"]){
                [df setDateFormat:@"dd/MM/yyyy"];
                d = [df dateFromString:searchStringToken];
                if (!d){
                    [df setDateFormat:@"dd.MM.yyyy"];
                    d = [df dateFromString:searchStringToken];
                }
            }
            if (d){
                at.testValue = d;
                at.operator = SUPAttributeTest_EQUAL;
            }else{
                at.testValue = searchStringToken;
                at.operator = SUPAttributeTest_CONTAINS;
            }
            [ct add:at];
        }
        [outerCompositeTest add:ct];
    }

    
    [query where:outerCompositeTest];
    SUPQueryResultSet* resultList = [SalesPlusERPSalesPlus_SuperUserDB executeQuery:query];
    
    if (resultList == nil){
        return [NSArray array];
    }else{
        NSMutableArray *salesActivityList = [NSMutableArray arrayWithCapacity:resultList.length];
        for(SUPDataValueList* result in resultList){
            SalesPlusSalesActivity *sa = [SalesPlusSalesActivity findByPrimaryKey:[SUPDataValue  getNullableString:[result item:0]]];
            NSMutableDictionary *temp = [sa toDictionaryWithKeys:self.salesActivityKeys];
            [temp addEntriesFromDictionary:[self getSalesActivityAdditonalInfo:sa]];
            [salesActivityList addObject:temp];
        }
        
        return [NSArray arrayWithArray:salesActivityList];
    }
}

// Search Sales Activity by UNAME
- (NSArray *)searchSalesActivityWithUNAME:(NSString *)searchString{
    // get the language the useris logged in
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString* preferredLang = [languages objectAtIndex:0];
    NSString *language = [[preferredLang capitalizedString] substringToIndex:1];
    
    NSArray *searchKeys = [NSArray arrayWithObjects:
                           @"sa.CREATED_BY", // UNAME-Created By
                           nil];
    
    SUPQuery *query = [SUPQuery getInstance];
    [query select:@"sa.DOC_NUMBER"];
    [query from:@"SalesActivity":@"sa"];
    [query join:@"Customer":@"c":@"c.KUNNR":@"sa.PARTN_ID"];
    [query join:@"CustKTAAR":@"cuKTAAR":@"sa.ACTIVITY_TYPE":@"cuKTAAR.KTAAR"];
    [query orderBy:@"sa.FROM_DATE" :[SUPSortOrder DESCENDING]];
    NSArray* searchStringTokens = [searchString componentsSeparatedByString: @" "];
    SUPCompositeTest *outerCompositeTest = [SUPCompositeTest getInstance];
    [outerCompositeTest setOperator:SUPCompositeTest_AND];
    for (int i = 0; i < searchStringTokens.count; i++){
        SUPCompositeTest *ct = [SUPCompositeTest getInstance];
        [ct setOperator:SUPCompositeTest_OR];
        for (NSString *key in searchKeys) {
            SUPAttributeTest *at = [SUPAttributeTest getInstance];
            at.attribute = key;
            //check if the searchstring is a date
            NSString *searchStringToken = [searchStringTokens objectAtIndex:i];
            at.testValue = searchStringToken;
            at.Operator = SUPAttributeTest_EQUAL;
            
            [ct add:at];
        }
        [outerCompositeTest add:ct];
    }
    
    [query where:outerCompositeTest];
    SUPQueryResultSet* resultList = [SalesPlusERPSalesPlus_SuperUserDB executeQuery:query];
    
    if (resultList == nil){
        return [NSArray array];
    }else{
        NSMutableArray *salesActivityList = [NSMutableArray arrayWithCapacity:resultList.length];
        for(SUPDataValueList* result in resultList){
            SalesPlusSalesActivity *sa = [SalesPlusSalesActivity findByPrimaryKey:[SUPDataValue  getNullableString:[result item:0]]];
            NSMutableDictionary *temp = [sa toDictionaryWithKeys:self.salesActivityKeys];
            [temp addEntriesFromDictionary:[self getSalesActivityAdditonalInfo:sa]];
            [salesActivityList addObject:temp];
        }
        
        return [NSArray arrayWithArray:salesActivityList];
    }
}

// Queries the database for the given value on multiple fields and functionality for advanced search with ranges!
- (NSArray *)searchSalesActivityWithCriteriaAndMultipleRanges:(NSMutableDictionary *)values{
    
    // get the language the useris logged in
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString* preferredLang = [languages objectAtIndex:0];
    NSString *language = [[preferredLang capitalizedString] substringToIndex:1];
    
    NSArray *searchKeys = [NSArray arrayWithObjects:
                           @"sa.DOC_NUMBER",
                           @"sa.ACTIVITY_TYPE", //Activity type
                           @"sa.ACTIVITY_COMMENT",
                           @"sa.FROM_DATE",
                           //@"c.NAME1",
                           @"cuKTAAR.VTEXT",
                           nil];
    
    SUPQuery *query = [SUPQuery getInstance];
    [query select:@"sa.DOC_NUMBER"];
    [query from:@"SalesActivity":@"sa"];
//    [query join:@"Customer":@"c":@"c.KUNNR":@"sa.PARTN_ID"];
    [query join:@"CustKTAAR":@"cuKTAAR":@"sa.ACTIVITY_TYPE":@"cuKTAAR.KTAAR"];
    [query join:@"CustKTAER":@"cuKTAER":@"sa.RESULT":@"cuKTAER.KTAER"];
    [query join:@"CustKTAST":@"cuKTAST":@"sa.STATE":@"cuKTAST.KTAST"];
    [query orderBy:@"sa.FROM_DATE" :[SUPSortOrder DESCENDING]];
    
    NSArray* searchStringTokens = [[values objectForKey:@"SearchCriteria"] componentsSeparatedByString: @" "];
    SUPCompositeTest *outerCompositeTest = [SUPCompositeTest getInstance];
    [outerCompositeTest setOperator:SUPCompositeTest_AND];
    for (int i = 0; i < searchStringTokens.count; i++){
        SUPCompositeTest *ct = [SUPCompositeTest getInstance];
        [ct setOperator:SUPCompositeTest_OR];
        for (NSString *key in searchKeys) {
            SUPAttributeTest *at = [SUPAttributeTest getInstance];
            at.attribute = key;
            //check if the searchstring is a date
            NSString *searchStringToken = [searchStringTokens objectAtIndex:i];
            NSDate *d;
            NSDateFormatter* df = [[NSDateFormatter alloc] init];
            if ([language isEqualToString:@"D"]){
                [df setDateFormat:@"MM/dd/yyyy"];
                d = [df dateFromString:searchStringToken];
                if (!d){
                    [df setDateFormat:@"MM.dd.yyyy"];
                    d = [df dateFromString:searchStringToken];
                }
            }else if ([language isEqualToString:@"E"]){
                [df setDateFormat:@"dd/MM/yyyy"];
                d = [df dateFromString:searchStringToken];
                if (!d){
                    [df setDateFormat:@"dd.MM.yyyy"];
                    d = [df dateFromString:searchStringToken];
                }
            }
            if (d){
                at.testValue = d;
                at.operator = SUPAttributeTest_EQUAL;
            }else{
                at.testValue = searchStringToken;
                at.operator = SUPAttributeTest_CONTAINS;
            }
            [ct add:at];
        }
        [outerCompositeTest add:ct];
    }
    NSDictionary* SetOfRanges = [values objectForKey:@"SetRanges"];
    NSDictionary* RangesValues = [values objectForKey:@"Ranges"];
    NSArray* RangesKeys = [SetOfRanges allKeys];
    for (int f = 0; f < RangesKeys.count; f++){
        if([[SetOfRanges objectForKey:[RangesKeys objectAtIndex:f]] isEqualToString:@"X"]){
            NSDictionary* rangeValue = [RangesValues objectForKey:[NSString stringWithFormat:@"%@_VALUES", [RangesKeys objectAtIndex:f]]];
            NSArray* multipleSelectTokens = [rangeValue objectForKey:@"VAL"]; //componentsSeparatedByString: @","];
            SUPCompositeTest *MainANDCompositeTest = [SUPCompositeTest getInstance];
            [MainANDCompositeTest setOperator:SUPCompositeTest_AND];
            if(multipleSelectTokens.count > 1){
                SUPCompositeTest *innerCompositeForMultiple = [SUPCompositeTest getInstance];
                [innerCompositeForMultiple setOperator:SUPCompositeTest_OR];
                for (int i = 0; i < multipleSelectTokens.count; i++){
                    [innerCompositeForMultiple add:[SUPAttributeTest equal:[NSString stringWithFormat:@"%@", [RangesKeys objectAtIndex:f]] :multipleSelectTokens[i]]];
                }
                [MainANDCompositeTest add:innerCompositeForMultiple];
            }
            else{
                [MainANDCompositeTest setOperator:SUPCompositeTest_AND];
                [MainANDCompositeTest add:[SUPAttributeTest equal:[NSString stringWithFormat:@"%@", [RangesKeys objectAtIndex:f]] :multipleSelectTokens[0]]];
                //                    SUPAttributeTest *ateqt = [SUPAttributeTest getInstance];
                //                    ateqt.attribute = [NSString stringWithFormat:@"%@", [RangesKeys objectAtIndex:f]];
                //                    ateqt.testValue = [multipleSelectTokens objectAtIndex:0];
                //                    ateqt.operator = SUPAttributeTest_EQUAL;
                //                    [MainANDCompositeTest add:ateqt];
            }
            [outerCompositeTest add:MainANDCompositeTest];
        }
    }
    
    [query where:outerCompositeTest];
    SUPQueryResultSet* resultList = [SalesPlusERPSalesPlus_SuperUserDB executeQuery:query];
    
    if (resultList == nil){
        return [NSArray array];
    }else{
        NSMutableArray *salesActivityList = [NSMutableArray arrayWithCapacity:resultList.length];
        for(SUPDataValueList* result in resultList){
            SalesPlusSalesActivity *sa = [SalesPlusSalesActivity findByPrimaryKey:[SUPDataValue  getNullableString:[result item:0]]];
            NSMutableDictionary *temp = [sa toDictionaryWithKeys:self.salesActivityKeys];
            [temp addEntriesFromDictionary:[self getSalesActivityAdditonalInfo:sa]];
            [salesActivityList addObject:temp];
        }
        
        return [NSArray arrayWithArray:salesActivityList];
    }
}

- (NSArray *)salesActivityListForKunnr:(NSString *)kunnr{
    SUPObjectList *salesActivityList = [SalesPlusSalesActivity findByCustomer:kunnr];
    if(salesActivityList == nil){
        return [NSArray array];
    }else{
        NSMutableArray *result = [NSMutableArray arrayWithCapacity:salesActivityList.length];
        for (SalesPlusSalesActivity *sa in salesActivityList) {
            NSMutableDictionary *temp = [sa toDictionaryWithKeys:self.salesActivityKeys];
            [temp addEntriesFromDictionary:[self getSalesActivityAdditonalInfo:sa]];
            [result addObject:temp];
        }
        return result;
    }
}

- (NSDictionary *)getSalesActivityAdditonalInfo:(SalesPlusSalesActivity *)activity{
    NSMutableDictionary *additionalInfo = [[NSMutableDictionary alloc] initWithCapacity:4];
    NSString *orderNumbers = @"";
    // get the text for activity status
    NSArray *custKTAST = [[CustomizingManager defaultManager] ktast];
    for (NSDictionary *activityType in custKTAST){
        if ([[activityType objectForKey:@"KTAST"] isEqualToString:activity.STATE]){
            [additionalInfo setValue:[activityType objectForKey:@"VTEXT"] forKey:@"ACTIVITY_STATUS_TEXT"];
            break;
        }
    }
    NSArray *custKTAAR = [[CustomizingManager defaultManager] ktaar];
    // get the text for activity type
    for (NSDictionary *activityType in custKTAAR){
        if ([[activityType objectForKey:@"KTAAR"] isEqualToString:activity.ACTIVITY_TYPE]){
            [additionalInfo setValue:[activityType objectForKey:@"VTEXT"] forKey:@"ACTIVITY_TYPE_TEXT"];
            break;
        }
    }
    // get the customer name1
    NSDictionary *customerDict = [[CustomerManager defaultManager] customerBasicDetailsForKUNNR:activity.PARTN_ID];
    [additionalInfo setValue:[customerDict objectForKey:@"NAME1"] forKey:@"NAME1"];
    // get the indicator for order/exchange/return
    NSArray *orderList = [[SalesOrderManager defaultManager] getAllListForSalesActivity:activity.DOC_NUMBER];
    for (NSDictionary *order in orderList){
        //        if ([[order objectForKey:@"DOC_TYPE"] isEqualToString:@"ZTA"]) //salesorder
        orderNumbers = [NSString stringWithFormat:@"%@",[order objectForKey:@"DOC_NUMBER"]];
        //        else if ([[order objectForKey:@"DOC_TYPE"] isEqualToString:@"YREV"]) //return
        //            returnNumbers = [NSString stringWithFormat:@"%@",[order objectForKey:@"DOC_NUMBER"]];
        //        else if ([[order objectForKey:@"DOC_TYPE"] isEqualToString:@"YTAU"]) //exchange
        //            exchangeNumbers = [NSString stringWithFormat:@"%@",[order objectForKey:@"DOC_NUMBER"]];
    }
    [additionalInfo setValue:orderNumbers forKey:@"ORDER_NUMS"];
    //    [additionalInfo setValue:returnNumbers forKey:@"RETURN_NUMS"];
    //    [additionalInfo setValue:exchangeNumbers forKey:@"EXCHANGE_NUMS"];
    return additionalInfo;
}

- (NSString *)createSalesActivity:(NSMutableDictionary *)values{
    SalesPlusSalesActivity *activity = [[SalesPlusSalesActivity alloc]init];
    [values setValuesInMBO:activity];
    UserManager *manager = [UserManager defaultManager];
    SalesPlusUser *userMBO = [manager currentUserMBO];
    if (userMBO){
        activity.SALESORG = userMBO.VKORG;
        activity.SALES_OFF = userMBO.VKBUR;
        activity.DIVISION = userMBO.SPART;
        activity.DISTR_CHAN = userMBO.VTWEG;
        //    activity.SALES_GRP = userMBO.VKGRP;
    }
    //    //set the primary key for the sales actvity
    //    NSString * timeStampValue = [NSString stringWithFormat:@"%d", (long)[[NSDate date] timeIntervalSince1970]];
    //    NSLog(@"Time Stamp Value == %@", timeStampValue);
    //    activity.DOC_NUMBER = [NSString stringWithFormat:@"*%@",timeStampValue];
    if ([[values objectForKey:@"RELEASE"] isEqualToString:@"X"]){
        //this activity has been released clear the "*" from the number
        activity.DOC_NUMBER = [activity.DOC_NUMBER stringByReplacingOccurrencesOfString:@"*" withString:@""];
    }
    [activity create];
    if ([activity.DOC_NUMBER rangeOfString:@"*"].location == NSNotFound){
        [activity submitPending];
    }
    //    bool testing = YES;
    //    if (testing){
    //        SalesPlusSalesActivityCreateStartEndOperation *activitySE = [[SalesPlusSalesActivityCreateStartEndOperation alloc]init];
    //        activitySE.KUNNR = activity.PARTN_ID;
    //        //        activitySE.LONGITUDE = @"2222";
    //        //        activitySE.LATITUDE = @"1111";
    //        activitySE.ACTIVITY_TYPE = @"YS01";
    //        activitySE.START_END_DATE = [NSDate date];
    //        [activitySE save];
    //        [activitySE submitPending];
    //    }
    return activity.DOC_NUMBER;
}

- (bool)updateSalesActivity:(NSMutableDictionary *)values{
    SalesPlusSalesActivity *activity = [SalesPlusSalesActivity findByPrimaryKey:[values objectForKey:@"DOC_NUMBER"]];
    NSArray *orderList;
    if (activity){
        [values removeObjectForKey:@"PARTN_ID"];
        [values setValuesInMBO:activity];
        if ([[values objectForKey:@"RELEASE"] isEqualToString:@"X"]){
            //this activity has been released clear the "*" from the number
            // get all the order/returns/exchanges list to the sales activity
            orderList = [[SalesOrderManager defaultManager] getAllListForSalesActivity:activity.DOC_NUMBER];
            activity.DOC_NUMBER_TMP = [activity.DOC_NUMBER stringByReplacingOccurrencesOfString:@"*" withString:@""];
            activity.DOC_NUMBER = activity.DOC_NUMBER_TMP;
        }
        [activity update];
        if ([activity.DOC_NUMBER rangeOfString:@"*"].location == NSNotFound){
            [activity submitPending];
        }
        for (NSDictionary *values in orderList){
            SalesPlusSalesDocument *order = [SalesPlusSalesDocument findByPrimaryKey:[values objectForKey:@"DOC_NUMBER"]];
            order.VBELV_TMP = activity.DOC_NUMBER_TMP;
            order.VBELV = activity.DOC_NUMBER_TMP;
            //            order.DOC_NUMBER = [order.DOC_NUMBER stringByReplacingOccurrencesOfString:@"*" withString:@""];
            [order update];
            [order submitPending];
        }
        return YES;
    }else{
        return NO;
    }
}

-(bool)deleteSalesActivity:(NSString *)doc_number{
    SalesPlusSalesActivity *salesActivity = [SalesPlusSalesActivity findByPrimaryKey:doc_number];
    if (salesActivity){
        // delete all the order/returns/exchanges list to the sales activity
        NSArray *list = [[SalesOrderManager defaultManager] getAllListForSalesActivity:salesActivity.DOC_NUMBER];
        for (NSDictionary *values in list){
            NSString *orderNumber = [values objectForKey:@"DOC_NUMBER"];
            [[SalesOrderManager defaultManager] deleteSalesOrder:orderNumber];
        }
        [salesActivity delete];
        [salesActivity submitPending];
        return YES;
    }else{
        return NO;
    }
}
@end
