//
//  Util.m
//  ERPSales Dev
//
//  Created by Klaus Rollinger on 05.07.17.
//
//

#import <Foundation/Foundation.h>
#import "Util.h"

@implementation Util

+(NSString *) convertNSArrayToJSON:(NSArray *)array{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:0 error:&error];
    NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    return JSONString;
}

+(NSString *) convertNSDictionaryToJSON:(NSDictionary *)dic{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:&error];
    NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    return JSONString;

}

+ (id)alloc {
    [NSException raise:@"Cannot be instantiated!" format:@"Static class 'Util' cannot be instantiated!"];
    return nil;
}
@end
