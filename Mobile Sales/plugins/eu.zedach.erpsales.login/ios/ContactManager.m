//
//  ContactManager.m
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-06-26.
//
//

#import "ContactManager.h"
#import "UserManager.h"
#import "SalesPlusContact.h"
#import "SalesPlusContactAddress.h"
#import "SUPAbstractLocalEntity+ERPSales.h"
#import "NSDictionary+ERPSales.h"
#import "CustomerManager.h"

@implementation ContactManager
@synthesize contactKeys = _contactKeys;

+ (id) defaultManager{
    static ContactManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ContactManager alloc]initWithTableName:@"Contact"];
        manager.contactKeys = [NSArray arrayWithObjects:
                               @"PARNR",  // Contact Number
                               @"KUNNR",  // Customer Number
                               @"NAME1",  // Last Name
                               @"NAMEV",  // First Name
                               @"PARAU",  // Notes
                               @"PARH1",  // Hobby 1
                               @"PARH2",  // Hobby 2
                               @"GBDAT",  // Birthday
                               @"PARH3",  // Amount of children
                               @"PAFKT",
                               nil];
    });
    return manager;
}

- (NSArray *)contactListForCustomer:(NSString *)kunnr{
    SUPObjectList *contacts = [SalesPlusContact findByKUNNR:kunnr];

    NSMutableArray *contactList = [NSMutableArray arrayWithCapacity:contacts.size];
    
    for (SalesPlusContact *c in contacts) {
        if (!c.isDeleted){
            NSDictionary *contactFullDetails = [self contactDetails:c.PARNR];
            [contactList addObject:contactFullDetails];
        }
    }
//    NSMutableDictionary *values = [[NSMutableDictionary alloc] initWithCapacity:4];
//    [values setObject:kunnr forKey:@"KUNNR"];
//    [values setObject:@"SUP TEST2" forKey:@"NAME1"];
//    [values setObject:@"SUP TEST2" forKey:@"NAMEV"];
//    [values setObject:@"Mr." forKey:@"TITEL_AP"];
//    [self updateContact:values];
//    [self createContact:values];
    return contactList;
}

- (NSDictionary *)contactDetails:(NSString *)parnr{

    NSArray *addressKeys = [NSArray arrayWithObjects:
                            @"TEL1_NUMBR",          // Tel Number
                            @"FAX_NUMBER",          // Fax Number
                            @"PARTNEREMPLOYEEID",   // Contact Number
                            @"E_MAIL",              // Email
                            @"MOB_NUMBER",          // Mobile Number
                            nil];
    SalesPlusContact *contactMBO = [SalesPlusContact findByPrimaryKey:parnr];
    SalesPlusContactAddress *addressMBO = [contactMBO.contactAddresss objectAtIndex:0];

    NSMutableDictionary *contact=[NSMutableDictionary dictionaryWithDictionary:[contactMBO toDictionaryWithKeys:self.contactKeys]];
    if (contact.count > 0){
        if ([contactMBO.PARNR hasPrefix:@"9"]) {
            [contact setObject:@"X" forKey:@"NEW"];
        }else{
            [contact setObject:@"" forKey:@"NEW"];
        }
    }

    [contact addEntriesFromDictionary:[addressMBO toDictionaryWithKeys:addressKeys]];
    
    return contact;
}

- (void)createContact:(NSDictionary *)values
{
    SalesPlusContact *newContact = [[SalesPlusContact alloc] init];
    [values setValuesInMBO:newContact];
    //set the primary key for the contact and prefix it with 9 to identify if this is a new contact
    NSString *timeStampValue = [NSString stringWithFormat:@"9%d", (int)[[NSDate date] timeIntervalSince1970]];
    timeStampValue = [timeStampValue substringToIndex:10];
    newContact.PARNR = [NSString stringWithFormat:@"%@",timeStampValue];
    
    SalesPlusContactAddress *address = [[SalesPlusContactAddress alloc] init];
//    address.FIRSTNAME = newContact.NAME1;
//    address.LASTNAME = newContact.NAMEV;
//    address.CUSTOMER = newContact.KUNNR;
    [values setValuesInMBO:address];
    
    address.contact = newContact;
    [address create];
    
    [address.contact refresh];
    [newContact create];
    
    [newContact submitPending];
}

- (void)updateContact:(NSDictionary *)values
{
    SalesPlusContact *updateContact = [SalesPlusContact findByPrimaryKey:[values objectForKey:@"PARNR"]];
    if (updateContact){
        [values setValuesInMBO:updateContact];
        
        SalesPlusContactAddress *address = [updateContact.contactAddresss objectAtIndex:0];
//        address.FIRSTNAME = updateContact.NAME1;
//        address.LASTNAME = updateContact.NAMEV;
//        address.CUSTOMER = updateContact.KUNNR;
        [values setValuesInMBO:address];
        address.contact = updateContact;
        
        [address update];
        [updateContact update];
        [updateContact submitPending];
    }
}

-(void)deleteContact:(NSDictionary *)values
{
    SalesPlusContact *deleteContact = [SalesPlusContact findByPrimaryKey:[values objectForKey:@"PARNR"]];
    if (deleteContact){
        [deleteContact delete];
        [deleteContact submitPending];
    }
}

@end
