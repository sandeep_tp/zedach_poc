//
//  MaterialPictureParserJSON.h
//  MaterialPicture
//
//  Created by YohanK on 2013-10-02.
//  Copyright (c) 2013 Yohan K. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "SalesPlusProduct.h"
#import "SalesPlusProduct.h"


@interface MaterialPictureParser : NSObject <NSURLConnectionDelegate>
{	
	NSMutableArray *pictures;
    SUPObjectList *productList;
    NSMutableData *responseData;
    bool getPicsNeedingUpdate;
    bool requestSent;
    int currentIndex;
    NSString *currentMATNR;
    NSString *currentVersion;
    NSString *currentSUP;
    NSMutableArray *picsNeedingUpdate;
}

-(bool) getPictures;

@end
