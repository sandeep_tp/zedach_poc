//
//  UserManager.m
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-08-12.
//
//

#import "UserManager.h"
#import "SalesPlusUser.h"
#import "SUPAbstractLocalEntity+ERPSales.h"

@implementation UserManager

+ (id) defaultManager{
    static UserManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[UserManager alloc]initWithTableName:@"User"];
    });
    return manager;
}

- (NSDictionary *)currentUserDetails{
    SUPObjectList *userList = [SalesPlusUser findAll];
    if (userList && userList.length > 0){
        SalesPlusUser *user = [userList objectAtIndex:0];
        return [user toDictionary];
    }
    return [NSDictionary dictionary];
}

- (SalesPlusUser *)currentUserMBO{
    SUPObjectList *userList = [SalesPlusUser findAll];
    if (userList && userList.length > 0){
        SalesPlusUser *user = [userList objectAtIndex:0];
        return user;
    }
    else {
        return nil;
    }
    
}

@end
