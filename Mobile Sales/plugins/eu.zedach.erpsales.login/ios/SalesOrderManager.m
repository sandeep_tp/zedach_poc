//
//  SalesOrderHistoryManager.m
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-06-27.
//
//

#import "SalesOrderManager.h"
#import "SalesPlusSalesDocument.h"
#import "SalesPlusSalesDocumentItem.h"
#import "SalesPlusSalesDocumentSignature.h"
#import "SUPAbstractLocalEntity+ERPSales.h"
#import "SUPAttributeTest.h"
#import "SUPSortCriteria.h"
#import "SUPCompositeTest.h"
#import "SUPAttributeSort.h"
#import "NSDictionary+ERPSales.h"
#import "ProductManager.h"
#import "SalesPlusProduct.h"
#import "SalesPlusCustStatusIndicator.h"
#import "SalesPlusProductConditionItem.h"
#import "SalesPlusSalesDocumentSignature.h"
#import "UserManager.h"
#import "CustomizingManager.h"
#import "SalesPlusOpenQuotation.h"
#import "SalesPlusSalesDocumentAttachments.h"
#import "SalesPlusOpenQuotation.h"
#import "OpenQuotationManager.h"

@implementation SalesOrderManager
@synthesize salesOrderKeys = _salesOrderKeys;
+ (id) defaultManager{
    static SalesOrderManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[SalesOrderManager alloc]initWithTableName:@"SalesOrder"];
        manager.salesOrderKeys = [NSArray arrayWithObjects:
                                  @"DOC_NUMBER", // Document No. or VBELN
                                  @"DOC_TYPE",
                                  @"SALES_ORG",  // Sales Organization
                                  @"DISTR_CHAN", // Distribution Channel
                                  @"DIVISION",   // Division
                                  @"SOLD_TO",    // Customer No. or KUNNR
                                  @"BSTKD",      // Purchase Order No.
                                  @"PURCH_DATE", // Order Date
                                  @"REQ_DATE_H", // Delivery Date
                                  @"KTEXT1",     // Note 1
                                  @"KTEXT2",     // Note 2
                                  @"CREATED_BY",
                                  @"CURRENCY",
                                  @"VBELV",      // Sales Activity
                                  @"SALES_OFF",
                                  @"NET_VAL_HD",
                                  @"ORD_REASON",
                                  @"QT_VALID_F", //offer valid from
                                  @"QT_VALID_T", //offer valid until
                                  nil];
    });
    return manager;
}

- (NSArray *)getAllListForCustomer:(NSString*)kunnr{
    NSArray *itemKeys = [NSArray arrayWithObjects:
                         @"NET_VALUE",
                         nil];

    SUPObjectList *orderList = [SalesPlusSalesDocument findSortedALLByCustomer:kunnr];
    SUPObjectList *openQuotationList = [SalesPlusOpenQuotation findSortedALLByCustomer:kunnr];
    [orderList addAll:openQuotationList];
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:orderList.length];
    NSArray *csimts = [[CustomizingManager defaultManager] csimt];
    for (SalesPlusSalesDocument *so in orderList) {
        NSMutableDictionary* tmpDict = [so toDictionaryWithKeys:self.salesOrderKeys];
        SUPObjectList *salesOrderItemList = [SalesPlusSalesDocumentItem findByDOCsorted:[tmpDict objectForKey:@"DOC_NUMBER"]];
        NSMutableArray *itemResult = nil;
        itemResult = [NSMutableArray arrayWithCapacity:salesOrderItemList.length];
        for (SalesPlusSalesDocumentItem *i in salesOrderItemList) {
            NSDictionary *soi = [i toDictionaryWithKeys:itemKeys];
            [itemResult addObject:soi];
        }
        if(itemResult.count > 0){
            [tmpDict setObject:itemResult forKey:@"Items"];
        }
        for (int ii = 0; ii < csimts.count; ii++){
            NSDictionary *csimt = [csimts objectAtIndex:ii];
            if ([[csimt objectForKey:@"AUGRU"] isEqualToString:[tmpDict objectForKey:@"ORD_REASON"]]){
                [tmpDict setObject:[csimt objectForKey:@"INDIC"] forKey:@"ORD_REASON"];
            }
        }
        [result addObject:tmpDict];
    }
    
    return result;
}

- (NSArray *)getAllListForSalesActivity:(NSString*)vbelv{
//    SUPQuery *customerQuery = [SUPQuery getInstance];
//    customerQuery = [self selectAllQuery:self.salesOrderKeys];
//    SUPCompositeTest *saCompTest = [SUPCompositeTest getInstance];
//    [saCompTest setOperator:SUPCompositeTest_OR];
//    [saCompTest add:[SUPAttributeTest match:@"VBELV" :vbelv]];
//    //workaround as the newly created sales orders the VBLEV is maintained in the VBELV_TMP
//    [saCompTest add:[SUPAttributeTest match:@"VBELV_TMP" :vbelv]];
//    customerQuery.testCriteria = saCompTest;//[SUPAttributeTest match:@"VBELV" :vbelv];
//    SUPSortCriteria *sort = [SUPSortCriteria getInstance];
//    [sort add:[SUPAttributeSort descending:@"DOC_DATE"]];
//    customerQuery.sortCriteria = sort;
//    SUPObjectList *orderList = [SalesPlusSalesOrder findWithQuery:customerQuery];
    SUPObjectList *orderList = [SalesPlusSalesDocument findSortedALLByActivity:vbelv];
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:orderList.length];
    
    for (SalesPlusSalesDocument *sd in orderList) {
        [result addObject:[sd toDictionaryWithKeys:self.salesOrderKeys]];
    }
    return result;
}

-(NSArray *)getRecentSalesOrderProductsForCustomer:(NSString *)kunnr withLimit:(int)number{
    SUPQuery *customerQuery = [SUPQuery getInstance];
    customerQuery = [self selectAllQuery:self.salesOrderKeys];
    SUPCompositeTest *saCompTest = [SUPCompositeTest getInstance];
    [saCompTest setOperator:SUPCompositeTest_AND];
    [saCompTest add:[SUPAttributeTest match:@"SOLD_TO" :kunnr]];
    //workaround as the newly created sales orders the VBLEV is maintained in the VBELV_TMP
    [saCompTest add:[SUPAttributeTest match:@"DOC_TYPE" :@"ZTA"]];
    customerQuery.testCriteria = saCompTest;//[SUPAttributeTest match:@"VBELV" :vbelv];
    SUPSortCriteria *sort = [SUPSortCriteria getInstance];
    [sort add:[SUPAttributeSort descending:@"DOC_DATE"]];
    customerQuery.sortCriteria = sort;
    customerQuery.take = number;
    SUPObjectList *orderList = [SalesPlusSalesDocument findWithQuery:customerQuery];
    //SUPObjectList *orderList = [SalesPlusSalesOrder getSalesOrdersForCustomer:kunnr skip:0 take:number];
    NSMutableDictionary *result = [NSMutableDictionary dictionaryWithCapacity:orderList.length];
    for (SalesPlusSalesDocument *sd in orderList) {
        // check if this material is part of the product catalog
        for (SalesPlusSalesDocumentItem *sdi in sd.salesOrderItems){
            NSDictionary *p = [[ProductManager defaultManager] productDetailsForMATNR:sdi.MATERIAL];
            if (p.count > 0){
                [result setObject:p forKey:sdi.MATERIAL];
            }
        }
    }
    return [result allValues];
}

- (NSDictionary *)salesOrderDetails:(NSString *)docNumber{
    if ([[[OpenQuotationManager defaultManager] isOpenQuotation:docNumber] isEqualToString:@"X"]){
        return [[OpenQuotationManager defaultManager] salesQuotationDetails:docNumber];
    }
    NSArray *itemKeys = [NSArray arrayWithObjects:
                         @"DLV_PRIO",   // Delivery Order Date Fixed
                         @"ITM_NUMBER", // Position or POSNR
                         @"MATERIAL",   // Material or MATNR
                         @"REQ_QTY",    // Order Quantity or KWMENG
                         @"SALES_UNIT",
                         @"SHORT_TEXT",
                         @"COND_VALUE",
                         @"COND_P_UNT",
                         @"COND_UNIT",
                         @"NET_VALUE",
                         @"ALTERN_ITM",
                         @"DISCOUNT_VALUE",
                         nil];
    NSArray *attachmentsKeys = [NSArray arrayWithObjects:
                                @"LINK",
                                @"DESCRIPTION",
                                nil];
    
    SalesPlusSalesDocument *order = [SalesPlusSalesDocument findByPrimaryKey:docNumber];
    NSMutableDictionary *result = [[NSMutableDictionary alloc] initWithCapacity:0];
    NSMutableDictionary *headerDict = [order toDictionaryWithKeys:self.salesOrderKeys];
    NSArray *csimts = [[CustomizingManager defaultManager] csimt];
    for (int ii = 0; ii < csimts.count; ii++){
        NSDictionary *csimt = [csimts objectAtIndex:ii];
        if ([[csimt objectForKey:@"AUGRU"] isEqualToString:[headerDict objectForKey:@"ORD_REASON"]]){
            [headerDict setObject:[csimt objectForKey:@"INDIC"] forKey:@"ORD_REASON"];
        }
    }
    SalesPlusSalesDocumentSignature *soSignature = order.salesOrderSignatures;
    if (soSignature && ![soSignature.SIGNATURE isNull]){
        SUPBigBinary *fileContent = soSignature.SIGNATURE;
        NSData *imageData = [soSignature.SIGNATURE value];
        NSString *imageString = [self base64forData:imageData];
        [headerDict setObject:imageString forKey:@"SIGNATURE"];
    }else{
        [headerDict setObject:@"" forKey:@"SIGNATURE"];
    }
    [result setValue:headerDict forKey:@"Header"];
    NSMutableArray *itemResult = nil;
    if (order){
        //SUPObjectList *salesOrderItemList = order.salesOrderItems;
        SUPObjectList *salesOrderItemList = [SalesPlusSalesDocumentItem findByDOCsorted:order.DOC_NUMBER];
        itemResult = [NSMutableArray arrayWithCapacity:salesOrderItemList.length];
        for (SalesPlusSalesDocumentItem *i in salesOrderItemList) {
            NSDictionary *soi = [i toDictionaryWithKeys:itemKeys];
            [itemResult addObject:soi];
        }
        if(itemResult.count > 0){
            [result setObject:itemResult forKey:@"Items"];
        }
        // get the current logged in language
        NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
        NSArray* languages = [defs objectForKey:@"AppleLanguages"];
        NSString* preferredLang = [languages objectAtIndex:0];
        NSString *language = [[preferredLang capitalizedString] substringToIndex:1];
        
        NSMutableArray *attachmentsResult = nil;
        SUPObjectList *attachmentItems = [SalesPlusSalesDocumentAttachments findByVbelnSpras:order.DOC_NUMBER withSPRAS:language];
        attachmentsResult = [NSMutableArray arrayWithCapacity:attachmentItems.length];
        for (SalesPlusSalesDocumentAttachments *atta in attachmentItems) {
            NSDictionary *oqa = [atta toDictionaryWithKeys:attachmentsKeys];
            [attachmentsResult addObject:oqa];
        }
        if(attachmentsResult.count > 0){
            [result setObject:attachmentsResult forKey:@"ATTACHMENTS"];
        }
        
    }else{
        result = [NSMutableDictionary dictionaryWithCapacity:0];
    }
    return result;
}

- (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}


-(void)createSalesOrder:(NSDictionary *)values{
    SalesPlusSalesDocument *so = [[SalesPlusSalesDocument alloc] init];
    NSDictionary *header = [values objectForKey:@"SalesOrderHeader"];
    [header setValuesInMBO:so];
    UserManager *manager = [UserManager defaultManager];
    SalesPlusUser *userMBO = [manager currentUserMBO];
    if (userMBO){
        so.SALES_ORG = userMBO.VKORG;
        //so.SALES_OFF = userMBO.VKBUR;
        so.DIVISION = userMBO.SPART;
        //so.DISTR_CHAN = userMBO.VTWEG;
        so.SALES_GRP = userMBO.VKGRP;
    }
    if ([[header objectForKey:@"RELEASE"] isEqualToString:@"X"]){
        //this activity has been released clear the "*" from the number
        so.DOC_NUMBER = [so.DOC_NUMBER stringByReplacingOccurrencesOfString:@"*" withString:@"+"];
    }
    NSArray *salesOrderItems = [values objectForKey:@"SalesOrderItems"];
    for (int i=0; i < salesOrderItems.count; i++){
        NSDictionary *items = [salesOrderItems objectAtIndex:i];
        SalesPlusSalesDocumentItem *soi = [[SalesPlusSalesDocumentItem alloc]init];
        [items setValuesInMBO:soi];
        //get the product text
        NSDictionary *p = [[ProductManager defaultManager] productDetailsForMATNR:soi.MATERIAL];
        [soi setValue:[p objectForKey:@"MATL_DESC"] forKey:@"SHORT_TEXT"];
        soi.salesOrder = so;
        [soi create];
        [soi.salesOrder refresh];
    }
    SalesPlusSalesDocumentSignature *sos = [[SalesPlusSalesDocumentSignature alloc] init];
    sos.salesOrder = so;
    [sos.salesOrder refresh];
    [sos create];
    if ([header objectForKey:@"SIGNATURE"]){
        NSString *str = @"data:image/jpg;base64,";
        str = [str stringByAppendingString:[header objectForKey:@"SIGNATURE"]];
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:str]];
        UIImage *signatureImage = [UIImage imageWithData:imageData];
        SUPBigBinary *fileContent = sos.SIGNATURE;
        [fileContent openForWrite: 0];
        [fileContent write:imageData];
        [fileContent close];
        [sos save];
    }
    [so create];
}

- (bool)updateSalesOrder:(NSDictionary *)values{
    NSDictionary *header = [values objectForKey:@"SalesOrderHeader"];
    SalesPlusSalesDocument *so = [SalesPlusSalesDocument findByPrimaryKey:[header objectForKey:@"DOC_NUMBER"]];
    if (so){
        // delete the old sales order and create a new one
        bool status = [self deleteSalesOrder:so.DOC_NUMBER];
        if (status == YES){
            [self createSalesOrder:values];
        }
        return YES;
    }else{
        return NO;
    }
}

- (bool)updateSalesOrderValidity:(NSDictionary *)values{
    NSDictionary *header = [values objectForKey:@"SalesOrderHeader"];
    SalesPlusSalesDocument *so = [SalesPlusSalesDocument findByPrimaryKey:[header objectForKey:@"DOC_NUMBER"]];
    if (so){
        NSDate *salesDocValidDate;

        NSDateFormatter* df = [[NSDateFormatter alloc] init];

        [df setDateFormat:@"yyyyMMdd"];
        [df setTimeZone: [NSTimeZone timeZoneWithName:@"GMT"]];
        salesDocValidDate = [df dateFromString: header[@"QT_VALID_T"]];
        
        so.QT_VALID_T = salesDocValidDate;
        [so update];
        [so submitPending];
        return YES;
    }else{
        return NO;
    }
}

-(bool)deleteSalesOrder:(NSString *)doc_number{
    SalesPlusSalesDocument *so = [SalesPlusSalesDocument findByPrimaryKey:doc_number];
    if (so){
        [so delete];
        [so submitPending];
        return YES;
    }else{
        return NO;
    }
}

-(NSString *)isSalesDocument:(NSString *)VBELN {
    SalesPlusSalesDocument *sd = [SalesPlusSalesDocument findByPrimaryKey:VBELN];
    if (sd){
        return @"X";
    }
    return @"";
}

@end
