//
//  SalesActivityManager.h
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-08-12.
//
//

#import "BaseManager.h"

@interface SalesActivityManager : BaseManager

@property (nonatomic,strong) NSArray* salesActivityKeys;

+ (id) defaultManager;
- (NSArray *)getSalesActivityListFrom:(int)skip To:(int)take;
- (NSDictionary *)salesActivityForVBELN:(NSString *)vbeln;
- (NSArray *)salesActivityListForKunnr:(NSString *)kunnr;
- (NSArray *)searchSalesActivityWithCriteria:(NSString *)searchString;
- (NSArray *)searchSalesActivityWithUNAME:(NSString *)searchString;
- (NSArray *)searchSalesActivityWithCriteriaAndMultipleRanges:(NSMutableDictionary *)values;
- (NSString *)createSalesActivity:(NSMutableDictionary *)values;
- (void)saveDayStartEndLocation:(NSMutableDictionary *)values;
- (bool)updateSalesActivity:(NSMutableDictionary *)values;
- (bool)deleteSalesActivity:(NSString *)doc_number;
@end
