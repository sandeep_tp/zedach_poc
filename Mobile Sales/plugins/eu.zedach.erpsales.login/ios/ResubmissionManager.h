//
//  ResubmissionManager.h
//  ERPSales Dev
//
//  Created by Klaus Rollinger on 03.08.17.
//
//

#import "BaseManager.h"

@interface ResubmissionManager : BaseManager

@property (nonatomic,strong) NSArray* resubKeys;
+ (id) defaultManager;
- (NSArray *)getResubmissionsForVBELN:(NSString *)vbeln;
- (NSArray *)getResubmissionsForUNAME:(NSString *)uname;
- (void)createResubmission:(NSDictionary *)values;
- (bool)updateResubmission:(NSDictionary *)values;
- (bool)deleteResubmission:(NSDictionary *)doc_number;
@end
