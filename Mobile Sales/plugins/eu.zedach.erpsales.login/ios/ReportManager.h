//
//  ReportingManager.h
//  ERPSales
//
//  Created by YohanK on 2013-12-09.
//
//

#import "BaseManager.h"

@interface ReportManager : BaseManager


@property (nonatomic,strong) NSArray* reportKeys;

+ (id) defaultManager;
- (NSArray *)getUserReportList;
- (NSDictionary *)getUserReportDetails:(NSString *)reportNumber;
- (NSArray *)getProductReportList:(NSString *)productNumber;
- (NSDictionary *)getProductReportDetails:(NSString *)reportNumber;
- (NSArray *)getCustomerReportList:(NSString *)customerNumber;
- (NSDictionary *)getCustomerReportDetails:(NSString *)reportNumber;
- (NSArray *)getCustomerBOReportList:(NSString *)customerNumber;
- (NSArray *)getUserBOReportList;
@end
