//
//  NSDictionary+ERPSales.m
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-08-12.
//
//

#import "NSDictionary+ERPSales.h"
#import "SUPClassWithMetaData.h"

@implementation NSDictionary (ERPSales)

- (id)valueForAttribute:(id<SUPAttributeMetaDataProtocol>)attribute{
    SUPDataType *dataType = [attribute dataType];
    id value = [self objectForKey:[attribute name]];
    NSString *name = [attribute name];
    if (dataType.code == SUPDataType.DATE){
        if (value == nil){
            value = nil;
        }else if ([value isEqualToString:@""]){
            value = @"";
        }else{
            value = [self.dateFormatter dateFromString:value];
        }
        //value = value == nil ? nil : [self.dateFormatter dateFromString:value];
    }else if (dataType.code == SUPDataType.TIME){
        if (value == nil){
            value = nil;
        }else if ([value isEqualToString:@""]){
            value = @"";
        }else{
            value = [self.timeFormatter dateFromString:value];
        }
        //value = value == nil ? nil : [self.timeFormatter dateFromString:value];
    }else if (dataType.code == SUPDataType.LIST){
        value = nil;
    }else if (dataType.code == SUPDataType_OBJECT){ //incase when the MBO has a relationship to another object that object values are ignored
        value = nil;
    }
    return value;
}

-(void)setValuesInMBO:(SUPAbstractEntityRBS*)mbo{
    SUPClassMetaDataRBS *metaData = [((id<SUPClassWithMetaData>)mbo)getClassMetaData];
    SUPAttributeMap *attributeMap = metaData.attributeMap;
    
    SUPObjectList *attributes = metaData.attributes;
    for (id<SUPAttributeMetaDataProtocol> att in attributes) {
        id value = [self valueForAttribute:att];
        NSString *name = [att name];
        if (value && ![value isKindOfClass:[NSNull class]]){
            [mbo setValue:value forKey:[att name]];
        }
    }
}

- (NSDateFormatter *)dateFormatter{
    static NSDateFormatter *df = nil;
    if (df == nil){
        df = [[NSDateFormatter alloc]init];
        //[df setTimeZone:[NSTimeZone systemTimeZone]];
        [df setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT+0:00"]];
        df.dateFormat = @"yyyyMMdd";
    }
    return df;
}

- (NSDateFormatter *)timeFormatter{
    static NSDateFormatter *tf = nil;
    if (tf == nil){
        tf = [[NSDateFormatter alloc]init];
        //[tf setTimeZone:[NSTimeZone localTimeZone]];
        //[tf setTimeZone:[NSTimeZone systemTimeZone]];
        [tf setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT+0:00"]];
        tf.dateFormat = @"HHmmss";
    }
    return tf;
}

@end
