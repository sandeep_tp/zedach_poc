//
//  MaterialPicture.h
//  MaterialPicture
//
//  Created by Yohan K on 2013-09-23.
//  Copyright (c) 2013 Yohan K. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MaterialPicture : NSObject <NSCoding>

@property (nonatomic, strong) NSString *matnr;
@property (nonatomic, strong) NSString *version;
@property (nonatomic, strong) NSString *path;
@property (nonatomic, strong) NSString *sup;

+ (MaterialPicture *)loadMaterialPictureObjectWithKey:(NSString *)matnr;
@end
