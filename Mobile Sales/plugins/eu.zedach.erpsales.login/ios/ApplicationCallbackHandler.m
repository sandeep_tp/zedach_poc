/*
 
 Copyright (c) Sybase, Inc. 2012   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform (Program), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file (Code).  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */



#import "ApplicationCallbackHandler.h"
#import "SUPStringList.h"


@implementation ApplicationCallbackHandler

+ (ApplicationCallbackHandler*)getInstance
{
    ApplicationCallbackHandler* _me_1 = [[ApplicationCallbackHandler alloc] init];
    return _me_1;
}


- (void)notify:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

- (void)onApplicationSettingsChanged:(SUPStringList*)names
{
    MBOLog(@"================================================");
    MBOLog(@"onApplicationSettingsChanged: names = %@",[names toString]);
    MBOLog(@"================================================");
}


- (void)onConnectionStatusChanged:(SUPConnectionStatusType)connectionStatus :(int32_t)errorCode :(NSString*)errorMessage
{
    MBOLog(@"=================================================");
    MBOLog(@"onConnectionStatusChanged: status = %d, code = %d, message = %@",connectionStatus,errorCode,errorMessage);
    MBOLog(@"=================================================");
    NSString *notification = nil;
    switch(connectionStatus)
    {
        case SUPConnectionStatus_CONNECTED:
            notification = ON_CONNECT_SUCCESS;
            break;
        case SUPConnectionStatus_DISCONNECTED:
            if (errorCode > 0) {
                notification = ON_CONNECT_FAILURE;
            }
            
            break;
        default:
            // Ignore all other status changes for this example.
            break;
    }
    
    if (notification != nil)
    {
        NSNotification *n = [NSNotification notificationWithName:notification object:nil];
        [self performSelectorOnMainThread:@selector(notify:) withObject:n waitUntilDone:NO];
    }
    
}


- (void)onRegistrationStatusChanged:(SUPRegistrationStatusType)registrationStatus :(int32_t)errorCode :(NSString*)errorMessage;
{
    MBOLog(@"=================================================");
    MBOLog(@"onRegistrationStatusChanged: status = %d, code = %d, message = %@",registrationStatus,errorCode,errorMessage);
    MBOLog(@"=================================================");
    
    if (registrationStatus == SUPRegistrationStatus_REGISTRATION_ERROR)
    {
        
        NSNotification *n = [NSNotification notificationWithName:ON_CONNECT_FAILURE object:nil];
        [self performSelectorOnMainThread:@selector(notify:) withObject:n waitUntilDone:NO];
    }
}


- (void)onDeviceConditionChanged :(SUPDeviceConditionType)condition;
{
    MBOLog(@"=================================================");
    MBOLog(@"onDeviceConditionChanged: condition = %d",condition);
    MBOLog(@"=================================================");
}

- (void)onHttpCommunicationError :(int32_t)errorCode :(NSString*)errorMessage :(SUPStringProperties*)responseHeaders
{
    MBOLog(@"=================================================");
    MBOLog(@"onHttpCommunicationError: errorCode = %i",errorCode);
    MBOLog(@"=================================================");
}

@end
