//
//  OrderMBOPlugIn.h
//  ERPSales
//
//  Created by Alexander Ilg on 5/11/13.
//
//

#import <Foundation/Foundation.h>
#import <Cordova/CDV.h>
#import "SalesOrderManager.h"

@interface SalesOrderMBOPlugIn : CDVPlugin

@property (strong, nonatomic) SalesOrderManager *salesOrderManager;

-(void)getAllListForCustomer:(CDVInvokedUrlCommand*)command;
//-(void)getSalesOrderListByCustomerNr:(CDVInvokedUrlCommand*)command;
//-(void)getReturnListByCustomerNr:(CDVInvokedUrlCommand*)command;
//-(void)getExchangeListByCustomerNr:(CDVInvokedUrlCommand*)command;
-(void)getRecentSalesOrderProductsForCustomer:(CDVInvokedUrlCommand*)command;
-(void)getSalesOrderDetails:(CDVInvokedUrlCommand*)command;
-(void)updateSalesOrder:(CDVInvokedUrlCommand*)command;
-(void)updateSalesOrderValidity:(CDVInvokedUrlCommand*)command;
-(void)createSalesOrder:(CDVInvokedUrlCommand*)command;
-(void)deleteSalesOrder:(CDVInvokedUrlCommand*)command;
-(void)getAllListForSalesActivity:(CDVInvokedUrlCommand*)command;
//-(void)updateReturn:(CDVInvokedUrlCommand*)command;
//-(void)createReturn:(CDVInvokedUrlCommand*)command;
//-(void)deleteReturn:(CDVInvokedUrlCommand*)command;
//-(void)updateExchange:(CDVInvokedUrlCommand*)command;
//-(void)createExchange:(CDVInvokedUrlCommand*)command;
//-(void)deleteExchange:(CDVInvokedUrlCommand*)command;
-(void)createSalesItem:(CDVInvokedUrlCommand*)command;
-(void)updateSalesItem:(CDVInvokedUrlCommand*)command;
-(void)deleteSalesItem:(CDVInvokedUrlCommand*)command;
@end
