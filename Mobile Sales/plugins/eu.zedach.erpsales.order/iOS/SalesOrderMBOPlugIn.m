//
//  OrderMBOPlugIn.m
//  ERPSales
//
//  Created by Alexander Ilg on 5/11/13.
//
//

#import "SalesOrderMBOPlugIn.h"

#import "SalesOrderManager.h"
#import "../eu.zedach.erpsales.login/Util.h"

@implementation SalesOrderMBOPlugIn

@synthesize salesOrderManager = _salesOrderManager;

-(void)getAllListForCustomer:(CDVInvokedUrlCommand *)command{
    MBOLog(@"getAllListForCustomer called");
    
    NSString *kunnr = [command argumentAtIndex:0];
    if (!self.salesOrderManager){
        self.salesOrderManager = [SalesOrderManager defaultManager];
    }
    NSArray *orderList = kunnr ? [self.salesOrderManager getAllListForCustomer:kunnr] : [NSArray array];
    NSString *result = [Util convertNSArrayToJSON: orderList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
}

-(void)getAllListForSalesActivity:(CDVInvokedUrlCommand *)command{
    MBOLog(@"getAllListForSalesActivity called");
    
    NSString *vbelv = [command argumentAtIndex:0];
    if (!self.salesOrderManager){
        self.salesOrderManager = [SalesOrderManager defaultManager];
    }
    NSArray *orderList = vbelv ? [self.salesOrderManager getAllListForSalesActivity:vbelv] : [NSArray array];
    NSString *result = [Util convertNSArrayToJSON: orderList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
}

//- (void)getSalesOrderListByCustomerNr:(CDVInvokedUrlCommand*)command{
//    MBOLog(@"getSalesOrderListByCustomerNr called");
//
//    NSString *kunnr = [command argumentAtIndex:0];
//    if (!self.salesOrderManager){
//        self.salesOrderManager = [SalesOrderManager defaultManager];
//    }
//    NSArray *orderList = kunnr ? [self.salesOrderManager salesOrderListForCustomer:kunnr] : [NSArray array];
//    
//    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:orderList.JSONString];
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//}
//
//- (void)getReturnListByCustomerNr:(CDVInvokedUrlCommand *)command{
//    MBOLog(@"getReturnListByCustomerNr called");
//    
//    NSString *kunnr = [command argumentAtIndex:0];
//    if (!self.salesOrderManager){
//        self.salesOrderManager = [SalesOrderManager defaultManager];
//    }
//    NSArray *orderList = kunnr ? [self.salesOrderManager returnListForCustomer:kunnr] : [NSArray array];
//    
//    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:orderList.JSONString];
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//}
//
//- (void)getExchangeListByCustomerNr:(CDVInvokedUrlCommand *)command{
//    MBOLog(@"getExchangeListByCustomerNr called");
//    
//    NSString *kunnr = [command argumentAtIndex:0];
//    if (!self.salesOrderManager){
//        self.salesOrderManager = [SalesOrderManager defaultManager];
//    }
//    NSArray *orderList = kunnr ? [self.salesOrderManager exchangeListForCustomer:kunnr] : [NSArray array];
//    
//    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:orderList.JSONString];
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//}

- (void)getRecentSalesOrderProductsForCustomer:(CDVInvokedUrlCommand *)command{
    MBOLog(@"getLastSalesOrdersForCustomerNr called");
    NSString *kunnr = [command argumentAtIndex:0];
    NSString *limitString = [command argumentAtIndex:1];
    int limit = limitString ? [limitString intValue] : 3;
    if (!self.salesOrderManager){
        self.salesOrderManager = [SalesOrderManager defaultManager];
    }
    NSArray *orderList = kunnr ? [self.salesOrderManager getRecentSalesOrderProductsForCustomer:kunnr withLimit:limit] : [NSArray array];
    NSString *result = [Util convertNSArrayToJSON: orderList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getSalesOrderDetails:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getSalesOrderDetails called");
    
    NSString *docNumber = [command argumentAtIndex:0];
    if (!self.salesOrderManager){
        self.salesOrderManager = [SalesOrderManager defaultManager];
    }
    NSDictionary *order = docNumber ? [self.salesOrderManager salesOrderDetails:docNumber] : [NSDictionary dictionary];
    NSString *result = [Util convertNSDictionaryToJSON: order];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)updateSalesOrder:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"updateSalesOrder called");
    NSDictionary *values = [command argumentAtIndex:0];
    if (!self.salesOrderManager){
        self.salesOrderManager = [SalesOrderManager defaultManager];
    }
    CDVPluginResult* pluginResult;
    bool result = [self.salesOrderManager updateSalesOrder:values];
    if (result == YES){
      pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    }else{
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)updateSalesOrderValidity:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"updateSalesOrder called");
    NSDictionary *values = [command argumentAtIndex:0];
    if (!self.salesOrderManager){
        self.salesOrderManager = [SalesOrderManager defaultManager];
    }
    CDVPluginResult* pluginResult;
    bool result = [self.salesOrderManager updateSalesOrderValidity:values];
    if (result == YES){
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    }else{
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)createSalesOrder:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"createSalesOrder called");
    NSDictionary *values = [command argumentAtIndex:0];
    if (!self.salesOrderManager){
        self.salesOrderManager = [SalesOrderManager defaultManager];
    }
    [self.salesOrderManager createSalesOrder:values];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)deleteSalesOrder:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"deleteSalesOrder called");
    NSString *docNumber = [command argumentAtIndex:0];
    if (!self.salesOrderManager){
        self.salesOrderManager = [SalesOrderManager defaultManager];
    }
    CDVPluginResult* pluginResult;
    bool result = [self.salesOrderManager deleteSalesOrder:docNumber];
    if (result == YES){
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    }else{
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

//- (void)updateReturn:(CDVInvokedUrlCommand*)command
//{
//    MBOLog(@"updateReturn called");
//    NSDictionary *values = [command argumentAtIndex:0];
//    if (!self.salesOrderManager){
//        self.salesOrderManager = [SalesOrderManager defaultManager];
//    }
//    CDVPluginResult* pluginResult;
//    bool result = [self.salesOrderManager updateReturn:values];
//    if (result == YES){
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
//    }else{
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
//    }
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//}
//
//- (void)createReturn:(CDVInvokedUrlCommand*)command
//{
//    MBOLog(@"createReturn called");
//    NSDictionary *values = [command argumentAtIndex:0];
//    if (!self.salesOrderManager){
//        self.salesOrderManager = [SalesOrderManager defaultManager];
//    }
//    [self.salesOrderManager createReturn:values];
//    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//}
//
//- (void)deleteReturn:(CDVInvokedUrlCommand*)command
//{
//    MBOLog(@"deleteReturn called");
//    NSString *docNumber = [command argumentAtIndex:0];
//    if (!self.salesOrderManager){
//        self.salesOrderManager = [SalesOrderManager defaultManager];
//    }
//    CDVPluginResult* pluginResult;
//    bool result = [self.salesOrderManager deleteReturn:docNumber];
//    if (result == YES){
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
//    }else{
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
//    }
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//}
//
//- (void)updateExchange:(CDVInvokedUrlCommand*)command
//{
//    MBOLog(@"updateExchange called");
//    NSDictionary *values = [command argumentAtIndex:0];
//    if (!self.salesOrderManager){
//        self.salesOrderManager = [SalesOrderManager defaultManager];
//    }
//    CDVPluginResult* pluginResult;
//    bool result = [self.salesOrderManager updateExchange:values];
//    if (result == YES){
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
//    }else{
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
//    }
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//}
//
//- (void)createExchange:(CDVInvokedUrlCommand *)command
//{
//    MBOLog(@"createExchange called");
//    NSDictionary *values = [command argumentAtIndex:0];
//    if (!self.salesOrderManager){
//        self.salesOrderManager = [SalesOrderManager defaultManager];
//    }
//    [self.salesOrderManager createExchange:values];
//    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//}
//
//- (void)deleteExchange:(CDVInvokedUrlCommand *)command
//{
//    MBOLog(@"deleteExchange called");
//    NSString *docNumber = [command argumentAtIndex:0];
//    if (!self.salesOrderManager){
//        self.salesOrderManager = [SalesOrderManager defaultManager];
//    }
//    CDVPluginResult* pluginResult;
//    bool result = [self.salesOrderManager deleteExchange:docNumber];
//    if (result == YES){
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
//    }else{
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
//    }
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
//}
//
- (void)createSalesItem:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"createSalesItem called");
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)updateSalesItem:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"updateSalesItem called");
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)deleteSalesItem:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"deleteSalesItem called");
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}



@end
