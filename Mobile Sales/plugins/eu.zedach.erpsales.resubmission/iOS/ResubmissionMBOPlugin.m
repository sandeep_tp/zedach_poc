//
//  ResubmissionMBOPlugin.m
//  ERPSales
//
//  Created by Klaus Rollinger on 2017-08-03.
//
//

#import "ResubmissionMBOPlugin.h"
#import "ResubmissionManager.h"
#import "Util.h"

@implementation ResubmissionMBOPlugin
@synthesize manager = _manager;

-(void)pluginInitialize{
    self.manager = [ResubmissionManager defaultManager];
}


- (void)getResubDetails:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getResubDetails called");
    
    NSString *docNumber = [command argumentAtIndex:0];
    if (!self.manager){
        self.manager = [ResubmissionManager defaultManager];
    }
    NSDictionary *resub = docNumber ? [self.manager getResubmissionsForVBELN:docNumber] : [NSDictionary dictionary];
    NSString *result = [Util convertNSDictionaryToJSON: resub];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}


- (void)getResubList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getResubDetails called");
    
    NSString *userName = [command argumentAtIndex:0];
    if (!self.manager){
        self.manager = [ResubmissionManager defaultManager];
    }
    NSDictionary *resub = userName ? [self.manager getResubmissionsForUNAME:userName] : [NSDictionary dictionary];
    NSString *result = [Util convertNSDictionaryToJSON: resub];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)updateResub:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"updateResub called");
    NSDictionary *values = [command argumentAtIndex:0];
    if (!self.manager){
        self.manager = [ResubmissionManager defaultManager];
    }
    CDVPluginResult* pluginResult;
    bool result = [self.manager updateResubmission:values];
    if (result == YES){
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    }else{
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)createResub:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"createResub called");
    NSDictionary *values = [command argumentAtIndex:0];
    if (!self.manager){
        self.manager = [ResubmissionManager defaultManager];
    }
    [self.manager createResubmission:values];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)deleteResub:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"deleteResub called");
    NSString *docNumber = [command argumentAtIndex:0];
    if (!self.manager){
        self.manager = [ResubmissionManager defaultManager];
    }
    CDVPluginResult* pluginResult;
    bool result = [self.manager deleteResubmission:docNumber];
    if (result == YES){
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    }else{
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}


@end
