//
//  SyncLogPlugin.h
//  ERPSales
//
//  Created by YohanK on 2014-01-30.
//
//

#import <Cordova/CDV.h>
#import "SyncLogManager.h"

@interface SyncLogPlugin : CDVPlugin

@property (retain,nonatomic) SyncLogManager *manager;

-(void)getSyncLog:(CDVInvokedUrlCommand*)command;

-(void)deleteAllSyncLog:(CDVInvokedUrlCommand *)command;

@end
