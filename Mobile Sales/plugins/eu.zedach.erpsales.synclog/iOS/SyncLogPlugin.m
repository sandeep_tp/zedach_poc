//
//  SyncLogPlugin.m
//  ERPSales
//
//  Created by YohanK on 2014-01-30.
//
//

#import "SyncLogPlugin.h"
#import "../eu.zedach.erpsales.login/Util.h"

@implementation SyncLogPlugin

@synthesize manager = _manager;

-(void)pluginInitialize{
    self.manager = [SyncLogManager defaultManager];
}

-(void)getSyncLog:(CDVInvokedUrlCommand *)command {
    MBOLog(@"getSyncLog called");
    NSArray *reportList =  [self.manager getSyncLog];
    NSString *result = [Util convertNSArrayToJSON: reportList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)deleteAllSyncLog:(CDVInvokedUrlCommand *)command
{
    MBOLog(@"deleteAllSyncLog called");
    [self.manager deleteAllEntries];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
