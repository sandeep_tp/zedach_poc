//
//  CustomizingMBOPlugin.m
//  ERPSales
//
//  Created by Yohan Kariyawasan on 2013-07-19.
//
//

#import "CustomizingMBOPlugin.h"
#import "../eu.zedach.erpsales.login/Util.h"

@implementation CustomizingMBOPlugin

- (void)pluginInitialize{
    self.manager = [CustomizingManager defaultManager];
}

- (void)getCountryList:(CDVInvokedUrlCommand *)command{
    MBOLog(@"getCountryList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.countryList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getCurrencyList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getCurrencyList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.currencyList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getMaterialGroupList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getMaterialGroupList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.materialGroupList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getMaterialTypeList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getMaterialTypeList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.materialTypeList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getStorageLocationList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getStorageLocationList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.storageLocationList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getTitleList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getTitleList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.titleList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getUnitOfMeasureList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getUnitOfMeasureList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.unitOfMeasureList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getVSBEDList:(CDVInvokedUrlCommand *)command{
    MBOLog(@"getVSBEDList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.vsbed];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getKTAARList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getKTAARList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.ktaar];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getKTAERList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getKTAERList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.ktaer];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}


-(void)getPARLAList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getPARLAList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.parla];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getAUARTList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getAUARTList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.auart];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getGEWEIList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getGEWEIList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.gewei];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getKONDAList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getKONDAList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.konda];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getKTASTList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getKTASTList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.ktast];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getPAFKTList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getPAFKTList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.pafkt];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getPARH1List:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getPARH1List called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.parh1];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getPARH2List:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getPARH2List called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.parh2];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}


-(void)getPARH3List:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getPARH3List called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.parh3];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getVKBURList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getVKBURList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.vkbur];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getZTERMList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getZTERMList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.zterm];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getBRSCHList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getBRSCHList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.brsch];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getSalesOrgsSDdropdown:(CDVInvokedUrlCommand *)command{
    MBOLog(@"getSalesOrgsSDdropdown called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.vrkmeActive];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getUserClosePlantList:(CDVInvokedUrlCommand *)command{
    MBOLog(@"getUserClosePlantList called");
    
    NSString *result = [Util convertNSArrayToJSON: self.manager.userClosePlant];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
@end
