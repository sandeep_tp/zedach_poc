//
//  ReportingMBOPlugin.m
//  ERPSales
//
//  Created by YohanK on 2013-12-09.
//
//

#import "ReportMBOPlugin.h"
#import "Util.h"

@implementation ReportMBOPlugin
@synthesize manager = _manager;

-(void)pluginInitialize{
    self.manager = [ReportManager defaultManager];
}

-(void)getUserReportList:(CDVInvokedUrlCommand*)command {
    MBOLog(@"getUserReportList called");
    NSArray *reportList = [self.manager getUserReportList];
    NSString *result = [Util convertNSArrayToJSON: reportList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getUserReportDetails:(CDVInvokedUrlCommand*)command {
    MBOLog(@"getUserReportDetails called");
    
    NSString *reportNumber = [command argumentAtIndex:0];
    NSDictionary *details = reportNumber ? [self.manager getUserReportDetails:reportNumber] : [NSDictionary dictionary];
    NSString *result = [Util convertNSDictionaryToJSON: details];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getProductReportList:(CDVInvokedUrlCommand*)command {
    MBOLog(@"getProductReportList called");
    NSString *productNumber = [command argumentAtIndex:0];
    NSArray *reportList =  productNumber ? [self.manager getProductReportList:productNumber] : [NSArray array];
    NSString *result = [Util convertNSArrayToJSON: reportList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getProductReportDetails:(CDVInvokedUrlCommand*)command {
    MBOLog(@"getProductReportDetails called");
    
    NSString *reportNumber = [command argumentAtIndex:0];
    NSDictionary *details = reportNumber ? [self.manager getProductReportDetails:reportNumber] : [NSDictionary dictionary];
    NSString *result = [Util convertNSDictionaryToJSON: details];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getCustomerReportList:(CDVInvokedUrlCommand*)command {
    MBOLog(@"getCustomerReportList called");
    NSString *customerNumber = [command argumentAtIndex:0];
    NSArray *reportList =  customerNumber ? [self.manager getCustomerReportList:customerNumber] : [NSArray array];
    NSString *result = [Util convertNSArrayToJSON: reportList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getCustomerReportDetails:(CDVInvokedUrlCommand*)command {
    MBOLog(@"getCustomerReportDetails called");
    
    NSString *reportNumber = [command argumentAtIndex:0];
    NSDictionary *details = reportNumber ? [self.manager getCustomerReportDetails:reportNumber] : [NSDictionary dictionary];
    NSString *result = [Util convertNSDictionaryToJSON: details];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getCustomerBOReportList:(CDVInvokedUrlCommand*)command {
    MBOLog(@"getCustomerReportList called");
    NSString *customerNumber = [command argumentAtIndex:0];
    NSArray *reportList =  customerNumber ? [self.manager getCustomerBOReportList:customerNumber] : [NSArray array];
    NSString *result = [Util convertNSArrayToJSON: reportList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getuserBOReportList:(CDVInvokedUrlCommand*)command {
    MBOLog(@"getuserBOReportList called");
    NSArray *reportList =  [self.manager getUserBOReportList];
    NSString *result = [Util convertNSArrayToJSON: reportList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
@end
