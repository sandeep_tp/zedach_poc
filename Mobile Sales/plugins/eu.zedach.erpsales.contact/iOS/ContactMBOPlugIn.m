//
//  ContactMBOPlugIn.m
//  ERPSales
//
//  Created by Alexander Ilg on 5/11/13.
//
//

#import "ContactMBOPlugIn.h"

#import "SalesPlusERPSalesPlus_SuperUserDB.h"
#import "SUPQuery.h"
#import "SUPQueryResultSet.h"
#import "SUPDataValue.h"
#import "SUPAttributeTest.h"
#import "SUPCompositeTest.h"
#import "SUPAttributeTest.h"
#import "SalesPlusContact.h"
#import "SalesPlusContactAddress.h"
#import "SUPAbstractLocalEntity+ERPSales.h"
#import "../eu.zedach.erpsales.login/Util.h"

@implementation ContactMBOPlugIn
@synthesize manager = _manager;

- (void)pluginInitialize{
    self.manager = [ContactManager defaultManager];
}

- (void)getContactListByCustomerNr:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"getContactListByCustomerNr called");
    
    NSString* kunnr = [command.arguments objectAtIndex:0];    
    NSArray *contactList = [self.manager contactListForCustomer:kunnr];
    NSString *result = [Util convertNSArrayToJSON: contactList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK  messageAsString:result];
     
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getContactDetails:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getContactDetails called");
    NSString *parnr = [command argumentAtIndex:0];

    NSDictionary *contact = [self.manager contactDetails:parnr];
    NSString *result = [Util convertNSDictionaryToJSON: contact];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getContactDetailsList:(CDVInvokedUrlCommand*)command{
    MBOLog(@"getContactDetailsList called");
    //NSString *parnr = [command argumentAtIndex:0];
    NSMutableArray *contactList = [NSMutableArray arrayWithCapacity:command.arguments.count];
    
    for (NSString* parnr in command.arguments) {
        NSDictionary *contact = [self.manager contactDetails:parnr];
        NSString *result = [Util convertNSDictionaryToJSON: contact];
        [contactList addObject:result];
}
    
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:contactList];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)createContact:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"createContact called");
    NSDictionary *createContactValues = [command argumentAtIndex:0];
    [self.manager createContact:createContactValues];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)updateContact:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"updateContact called");
    NSDictionary *updateContactValues = [command argumentAtIndex:0];
    [self.manager updateContact:updateContactValues];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)deleteContact:(CDVInvokedUrlCommand*)command
{
    MBOLog(@"deleteContact called");
    NSDictionary *deleteContactValues = [command argumentAtIndex:0];
    [self.manager deleteContact:deleteContactValues];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}


@end
