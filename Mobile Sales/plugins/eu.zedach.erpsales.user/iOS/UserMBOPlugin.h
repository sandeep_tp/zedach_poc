//
//  UserMBOPlugin.h
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-08-12.
//
//

#import <Cordova/CDV.h>
#import "../eu.zedach.erpsales.login/UserManager.h"

@interface UserMBOPlugin : CDVPlugin
@property (retain, nonatomic) UserManager *manager;

-(void)getUser:(CDVInvokedUrlCommand*)command;

@end
