//
//  UserMBOPlugin.m
//  ERPSales
//
//  Created by Marc Antoine Forand on 13-08-12.
//
//

#import "UserMBOPlugin.h"
#import "../eu.zedach.erpsales.login/Util.h"

@implementation UserMBOPlugin

@synthesize manager = _manager;

- (void)pluginInitialize{
    self.manager = [UserManager defaultManager];
}

-(void)getUser:(CDVInvokedUrlCommand*)command{
    
    NSDictionary *user = [self.manager currentUserDetails];
    NSString *result = [Util convertNSDictionaryToJSON: user];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
@end
