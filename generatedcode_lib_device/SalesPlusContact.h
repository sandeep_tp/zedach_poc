
/* Copyright (c) Sybase, Inc. 2010-2013   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code;OT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES;
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */

#import "sybase_sup.h"


#import "SUPClassWithMetaData.h"
#import "SUPAbstractEntityRBS.h"
#import "SUPMobileBusinessObject.h"
#import "SUPEntityDelegate.h"


@class SalesPlusCustomer;
@class SUPObjectList;
@class SUPEntityMetaDataRBS;
@class SUPEntityDelegate;
@class SUPClassMetaDataRBS;
@class SUPQuery;

// public interface declaration, can be used by application. 
/*!
 @class SalesPlusContact
 @abstract This class is part of package "ERPSalesPlus_SuperUser:1.0"
 @discussion Generated by Sybase Unwired Platform, compiler version 3.0.13.40
*/

@interface SalesPlusContact : SUPAbstractEntityRBS<SUPMobileBusinessObject, SUPClassWithMetaData>
{
@private
    NSString* _PARNR;
    NSString* _KUNNR;
    NSString* _NAMEV;
    NSString* _NAME1;
    NSString* _PAFKT;
    NSDate* _GBDAT;
    NSString* _PARAU;
    NSString* _PARH1;
    NSString* _PARH2;
    NSString* _PARH3;
    SalesPlusCustomer* _customer;
    BOOL _customerValid;
    SUPObjectList* _contactAddresss;
    SUPBigString* _cvpOperation;
    SUPBigString* _cvpOperationLobs;
    NSNumber* _customerFK;
    int64_t _surrogateKey;
    int64_t _cvpOperationLength;
    int64_t _cvpOperationLobsLength;
}


@property(retain,nonatomic) NSString* PARNR;
@property(retain,nonatomic) NSString* KUNNR;
@property(retain,nonatomic) NSString* NAMEV;
@property(retain,nonatomic) NSString* NAME1;
@property(retain,nonatomic) NSString* PAFKT;
@property(retain,nonatomic) NSDate* GBDAT;
@property(retain,nonatomic) NSString* PARAU;
@property(retain,nonatomic) NSString* PARH1;
@property(retain,nonatomic) NSString* PARH2;
@property(retain,nonatomic) NSString* PARH3;
@property(assign,nonatomic) SalesPlusCustomer* customer;
@property(assign,nonatomic) SalesPlusCustomer* customerEntity; // internal use only
@property(retain,nonatomic) SUPObjectList* contactAddresss;
@property(retain,nonatomic) SUPBigString* cvpOperation;
@property(retain,nonatomic) SUPBigString* cvpOperationLobs;
@property(retain,nonatomic) NSNumber* customerFK;
@property(assign,nonatomic) BOOL customerValid;
@property(assign,nonatomic) int64_t surrogateKey;
@property(assign,nonatomic) int64_t cvpOperationLength;
@property(assign,nonatomic) int64_t cvpOperationLobsLength;


- (id) init;
- (void)dealloc;
/*!
  @method 
  @abstract Creates a new autoreleased instance of this class
  @discussion
 */
+ (SalesPlusContact*)getInstance;


+ (SUPEntityMetaDataRBS*)metaData;
+ (void) registerCallbackHandler:(NSObject<SUPCallbackHandler>*)handler;
+ (NSObject<SUPCallbackHandler>*)callbackHandler;
- (SUPClassMetaDataRBS*)getClassMetaData;
/*!
  @method 
  @abstract Sets relationship attributes to null to save memory (they will be retrieved from the DB on the next getter call or property reference)
  @discussion
  @throws SUPPersistenceException
 */
- (void)clearRelationshipObjects;
/*!
  @method 
  @abstract Returns the entity for the primary key value passed in, or null if the entity is not found.
  @discussion
  @throws SUPPersistenceException
 */
+ (SalesPlusContact*)find:(int64_t)id_;
/*!
  @method 
  @abstract Returns an SUPObjectList of entity rows satisfying this query
  @discussion
  @throws SUPPersistenceException
 */
+ (SUPObjectList*)findWithQuery:(SUPQuery*)query;
/*!
  @method 
  @abstract Returns the primary key for this entity.
  @discussion
 */
- (int64_t)_pk;
/*!
  @method 
  @abstract Returns the entity for the primary key value passed in; throws an exception if the entity is not found.
  @discussion
  @throws SUPPersistenceException
 */
+ (SalesPlusContact*)load:(int64_t)id;
/*!
  @method 
  @abstract Returns an SUPObjectList of log records for this entity.
  @discussion
  @throws SUPPersistenceException
 */
- (SUPObjectList*)getLogRecords;
/*!
  @method 
  @abstract Return a string description of this entity.
  @discussion
 */
- (NSString*)toString;
/*!
  @method 
  @abstract Return a string description of this entity.
  @discussion
 */
- (NSString*)description;
/*!
  @method 
  @abstract Submit all the pending operations (ready for sending to server) for objects of this type.
  @discussion
  @throws SUPPersistenceException
 */
+ (void) submitPendingOperations;
/*!
  @method 
  @abstract Cancel any pending changes for objects of this type.
  @discussion
  @throws SUPPersistenceException
 */
+ (void) cancelPendingOperations;
/*!
  @method 
  @abstract If this object has been modified on the client, returns the original state of the object.
  @discussion
  @throws SUPPersistenceException
 */
- (SalesPlusContact*)getOriginalState;
/*!
  @method 
  @abstract If this object has been modified on the client, returns the downloaded state of the object.
  @discussion
  @throws SUPPersistenceException
 */
- (SalesPlusContact*)getDownloadState;
/*!
  @method 
  @abstract Return the name of the last operation executed on this object.
  @discussion
 */
- (SUPString)getLastOperation;
/*!
  @method 
  @abstract Return a list of SalesPlusContact objects
  @discussion
  @throws SUPPersistenceException
 */
+ (SUPObjectList*)getPendingObjects;
/*!
  @method 
  @abstract Return a list of SalesPlusContact objects
  @discussion
  @throws SUPPersistenceException
 */
+ (SUPObjectList*)getPendingObjects:(int32_t)skip take:(int32_t)take;
/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @throws SUPPersistenceException
 */

+ (SUPObjectList*)findAll;

/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param skip
  @param take
  @throws SUPPersistenceException
 */

+ (SUPObjectList*)findAll:(int32_t)skip take:(int32_t)take;
/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param pARNR
  @throws SUPPersistenceException
 */

+ (SalesPlusContact*)findByPrimaryKey:(NSString*)pARNR;
/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param kUNNR
  @throws SUPPersistenceException
 */

+ (SUPObjectList*)findByKUNNR:(NSString*)kUNNR;

/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param kUNNR
  @param skip
  @param take
  @throws SUPPersistenceException
 */

+ (SUPObjectList*)findByKUNNR:(NSString*)kUNNR skip:(int32_t)skip take:(int32_t)take;
/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param surrogateKey
  @throws SUPPersistenceException
 */

+ (SUPObjectList*)getContacts_for_Customer:(NSNumber*)surrogateKey;

/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param surrogateKey
  @param skip
  @param take
  @throws SUPPersistenceException
 */

+ (SUPObjectList*)getContacts_for_Customer:(NSNumber*)surrogateKey skip:(int32_t)skip take:(int32_t)take;
/*!
  @method
  @abstract Generated class method 
  @param query
  @throws SUPPersistenceException
 */
+ (int32_t)getSize:(SUPQuery*)query;
/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param query
  @throws SUPPersistenceException
 */

- (SUPObjectList*)getContactAddresssFilterBy:(SUPQuery*)query;

/*!
  @method
  @abstract Generated from an object query defined in the Eclipse tooling project for this package
  @param query
  @param skip
  @param take
  @throws SUPPersistenceException
 */

- (SUPObjectList*)getContactAddresssFilterBy:(SUPQuery*)query skip:(int32_t)skip take:(int32_t)take;
/*!
  @method
  @abstract Generated instance method 
  @param query
  @throws SUPPersistenceException
 */
- (int32_t)getContactAddresssSize:(SUPQuery*)query;


@end
typedef SUPObjectList SalesPlusContactList;