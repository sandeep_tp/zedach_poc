/*******************************************************************************
* Source File : WorkflowErrorHandler.h
* Date Created: 1/5/2011
* Copyright   : 2000 - 2011, Sybase, Inc.
* Description : Handles workflow error handling
* Notes       :
*******************************************************************************/
#import <UIKit/UIKit.h>

#define WF_REPORT_ERROR_CALLBACK @"reportErrorFromNative"
#define WF_ON_ERR_ERRCODE @"errcode"
#define WF_ON_ERR_NATIVE_MESSAGE @"nativeerrmsg"

@interface WorkflowErrorHandler : NSObject

+ (NSString*) formatErrorResponse:(int)iErrorCode withMsg:(NSString*)nativeMsg params:(NSDictionary*)requestParams;
+ (NSString*) formatErrorResponseForRMI:(int)iErrorCode withMsg:(NSString*)nativeMsg params:(NSDictionary*)requestParams;
+ (NSString*) generateErrorCallbackParameter:(NSString*)onErrorCallback withCode:(int)iErrorCode withException:(NSString*)exceptionMessage
                              withErrMessage:(NSString*)onErrorMessage requestXmlWorkflowMessage:(NSString*)incomingWorkflowMessage;

@end
