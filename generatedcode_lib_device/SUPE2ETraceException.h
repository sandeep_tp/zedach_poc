/*
 
 Copyright (c) Sybase, Inc. 2010   All rights reserved.                                    
 
 In addition to the license terms set out in the Sybase License Agreement for 
 the Sybase Unwired Platform ("Program"), the following additional or different 
 rights and accompanying obligations and restrictions shall apply to the source 
 code in this file ("Code").  Sybase grants you a limited, non-exclusive, 
 non-transferable, revocable license to use, reproduce, and modify the Code 
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of 
 applications created in connection with your licensed use of the Program.  
 The Code may not be transferred, sold, assigned, sublicensed or otherwise 
 conveyed (whether by operation of law or otherwise) to another party without 
 Sybase's prior written consent.  The following provisions shall apply to any 
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or 
 implied, relating to modified Code or any problems that result from use of the 
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
 to indemnify, hold harmless, and defend Sybase from and against any claims or 
 lawsuits, including attorney's fees, that arise from or are related to the 
 modified Code or from use of the modified Code. 
 
 */


/*!
 @class SUPE2ETraceException
 @abstract   Base exception class for E2E Tracing
 @discussion 
 */

#import <Foundation/Foundation.h>
#import "SUPBaseException.h"

@interface SUPE2ETraceException : SUPBaseException
{
}

/*!
 @method e2ETraceExceptionWithErrorCode
 @abstract  Instantiates an SUPE2ETraceException object
 @param  errorCode The error code value
 @result The SUPE2ETraceException object.
 @discussion
 */
+ (SUPE2ETraceException*)e2ETraceExceptionWithErrorCode:(int)errorCode;

/*!
 @method e2ETraceExceptionWithErrorCode
 @abstract  Instantiates an SUPE2ETraceException object
 @param  errorCode The error code value
 @param  message the error message
 @param
 @result The SUPE2ETraceException object.
 @discussion
 */
+ (SUPE2ETraceException *)e2ETraceExceptionWithErrorCode:(int)errorCode message:(NSString *)message;


/*!
 @method e2ETraceExceptionWithErrorCode
 @abstract  Instantiates an SUPE2ETraceException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @result The SUPE2ETraceException object.
 @discussion
 */
+ (SUPE2ETraceException*)e2ETraceExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments;

/*!
 @method e2ETraceExceptionWithErrorCode
 @abstract  Instantiates an SUPE2ETraceException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @param  cause The cause of the exception
 @result The SUPE2ETraceException object.
 @discussion
 */
+ (SUPE2ETraceException*)e2ETraceExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments cause:(NSException *)cause;



/*!
 @method     
 @abstract   (Deprecated) Returns an instance of SUPE2ETraceException object.
 @discussion 
 @result The SUPE2ETraceException object.
 */
+ (SUPE2ETraceException*)getInstance DEPRECATED_ATTRIBUTE;


- (SUPE2ETraceException *)init:(NSString *)name withMessage:(NSString *)errorMessage withCause:(NSException *)cause DEPRECATED_ATTRIBUTE;
/*!
 @method     
 @abstract   (Deprecated) Returns an instance of SUPE2ETraceException object.
 @discussion 
 @param errorMessage The error message.
 @result The SUPE2ETraceException object.
 */
+ (SUPE2ETraceException*)withMessage:(NSString *)errorMessage DEPRECATED_ATTRIBUTE;


/*!
 @method     
 @abstract   (Deprecated) Returns an instance of SUPE2ETraceException object.
 @discussion 
 @param cause The exception
 @result The SUPE2ETraceException object.
 */
+ (SUPE2ETraceException*)withCause:(NSException *)cause DEPRECATED_ATTRIBUTE;


/*!
 @method     
 @abstract   (Deprecated) Returns an instance of SUPE2ETraceException object.
 @discussion 
 @param errorMessage The error message.
 @param cause The exception.
 @result The SUPE2ETraceException object.
 */
+ (SUPE2ETraceException*)getInstance:(NSString *)errorMessage withCause:(NSException *)cause DEPRECATED_ATTRIBUTE;

@end
