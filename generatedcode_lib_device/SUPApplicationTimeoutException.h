/*
 
 Copyright (c) Sybase, Inc. 2010-2012   All rights reserved.                                    
 
 In addition to the license terms set out in the Sybase License Agreement for 
 the Sybase Unwired Platform ("Program"), the following additional or different 
 rights and accompanying obligations and restrictions shall apply to the source 
 code in this file ("Code").  Sybase grants you a limited, non-exclusive, 
 non-transferable, revocable license to use, reproduce, and modify the Code 
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of 
 applications created in connection with your licensed use of the Program.  
 The Code may not be transferred, sold, assigned, sublicensed or otherwise 
 conveyed (whether by operation of law or otherwise) to another party without 
 Sybase's prior written consent.  The following provisions shall apply to any 
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or 
 implied, relating to modified Code or any problems that result from use of the 
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
 to indemnify, hold harmless, and defend Sybase from and against any claims or 
 lawsuits, including attorney's fees, that arise from or are related to the 
 modified Code or from use of the modified Code. 
 
 */

#import "sybase_sup.h"
#import "SUPBaseException.h"


@class SUPApplicationTimeoutException;

/*!
 @class SUPApplicationTimeoutException
 @abstract   An exception that is thrown when a call to startConnection, stopConnection, registerApplication or unregisterApplication times out. 
 The operation may still be active in a background thread, it is only certain that the caller that has given up waiting for completion. Use ApplicationCallback to track status changes reliably.
 @discussion 
 */

@interface SUPApplicationTimeoutException : SUPBaseException {
	
}


/*!
 @method applicationTimeoutExceptionWithErrorCode
 @abstract  Instantiates an SUPApplicationTimeoutException object
 @param  errorCode The error code value
 @result The SUPApplicationTimeoutException object.
 @discussion
 */
+ (SUPApplicationTimeoutException*)applicationTimeoutExceptionWithErrorCode:(int)errorCode;

/*!
 @method applicationTimeoutExceptionWithErrorCode
 @abstract  Instantiates an SUPApplicationTimeoutException object
 @param  errorCode The error code value
 @param  message the error message
 @result The SUPApplicationTimeoutException object.
 @discussion
 */
+ (SUPApplicationTimeoutException *)applicationTimeoutExceptionWithErrorCode:(int)errorCode message:(NSString *)message;


/*!
 @method applicationTimeoutExceptionWithErrorCode
 @abstract  Instantiates an SUPApplicationTimeoutException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @result The SUPApplicationTimeoutException object.
 @discussion
 */
+ (SUPApplicationTimeoutException*)applicationTimeoutExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments;

/*!
 @method applicationTimeoutExceptionWithErrorCode
 @abstract  Instantiates an SUPApplicationTimeoutException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @param  cause The cause of the exception
 @result The SUPApplicationTimeoutException object.
 @discussion
 */
+ (SUPApplicationTimeoutException*)applicationTimeoutExceptionWithErrorCode:(int)errorCode message:(NSString*)message arguments:(NSArray*)arguments cause:(NSException *)cause;

/*!
 @method
 @abstract   (Deprecated) Returns a new instance of SUPApplicationTimeoutException.
 @discussion 
 @result    SUPApplicationTimeoutException object.
 */
+ (SUPApplicationTimeoutException*)getInstance DEPRECATED_ATTRIBUTE;

/*!
@method     
@abstract   (Deprecated) Returns the SUPApplicationTimeoutException object with "theMessage".
@param theMessage The  message.
@result The SUPApplicationTimeoutException.
@discussion 
*/
+ (SUPApplicationTimeoutException*)withMessage:(SUPString)theMessage DEPRECATED_ATTRIBUTE;

/*!
 @method
 @abstract   Initializes an SUPApplicationTimeoutException object.
 @discussion 
 @result    The initialized SUPApplicationTimeoutException object.
 */
- (SUPApplicationTimeoutException*)init;



@end
