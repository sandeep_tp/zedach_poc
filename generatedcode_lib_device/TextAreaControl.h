//
//  TextBoxControl.h
//  Widgets
//
//  Created by Alexey Piterkin on 2/10/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ControlCell.h"
#import "InnerTextView.h"

@interface TextAreaControl : ControlCell <UITextViewDelegate> {
   InnerTextView * editor;
}

@end
