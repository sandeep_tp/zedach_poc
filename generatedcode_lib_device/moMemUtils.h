/*
Copyright (c) Sybase, Inc. 2012 All rights reserved. 

In addition to the license terms set out in the Sybase License Agreement for 
the Sybase Unwired Platform ("Program"), the following additional or different 
rights and accompanying obligations and restrictions shall apply to the source 
code in this file ("Code"). Sybase grants you a limited, non-exclusive, 
non-transferable, revocable license to use, reproduce, and modify the Code 
solely for purposes of (i) maintaining the Code as reference material to better 
understand the operation of the Program, and (ii) development and testing of 
applications created in connection with your licensed use of the Program. 
The Code may not be transferred, sold, assigned, sublicensed or otherwise 
conveyed (whether by operation of law or otherwise) to another party without 
Sybase's prior written consent. The following provisions shall apply to any 
modifications you make to the Code: (i) Sybase will not provide any maintenance 
or support for modified Code or problems that result from use of modified Code; 
(ii) Sybase expressly disclaims any warranties and conditions, express or 
implied, relating to modified Code or any problems that result from use of the 
modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING 
TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN 
IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
to indemnify, hold harmless, and defend Sybase from and against any claims or 
lawsuits, including attorney's fees, that arise from or are related to the 
modified Code or from use of the modified Code.
*/

/******************************************************************************
*    Copyright 2012 Sybase, Inc
*    Source File            : moMemUtils.h
*    Platform Dependencies  :
*    Description            : Header file for misc. memory utilities.
*
*    Notes                  :
******************************************************************************/

#ifndef moMemUtils_H_INCLUDED
#define moMemUtils_H_INCLUDED

#if !defined( XCMO_SERVER )
//#define XCMO_DEBUG
#endif

#include "moOS.h"
#include "moTypes.h"
#include "moError.h"
#include "moErrCodes.h"
#include "moUtils.h"

// macro for debuggin memory leaks
#ifdef MOCLIENT_IPHONE
#define SOURCE_LOCATION             ((strrchr(__FILE__, '/') ?: __FILE__ - 1) + 1), __LINE__
#else
#define SOURCE_LOCATION             _T(__FILE__), __LINE__
#endif

#ifdef ESI_PALM
   #include <PalmOS.h>
   #include <DataMgr.h>

   // Mask used to determine if the pointer points to memory
   // in dynamic or storage heap.
   #define XCMO_STORAGE_HEAP_MASK               0x80000000
#else
#if defined WIN32_WCE || defined _WIN32_WCE || defined WIN32
   #include <windows.h>
#endif
   #include <string.h>
   typedef void* MemHandle;
#endif


TCHAR *moNewString( UI32 ulSize );
void moMemTraceStart();
void moMemTraceStop();
void moMemTraceShowResults();

bool VerifyPtr( void* pValue,
                const TCHAR* pcFileName,
                I32 lLineNo );


//#define OBJECT_TRACKER
#if defined( OBJECT_TRACKER )

enum OBJECT_TYPE
{
   CMORECORDSET = 0,
   CMOCLIENT,
   CMOCONNECTION,
   CMOREQUESTOPTIONS,
   CMOFIELDS,
   CMOFIELD,
   CMOINDEXES,
   CMOPARAMLIST,
   CMOPARAM,
   CMOSTRING,
   CMOERROR,
   CMODATETIME,
   CMOBINARY,
   CMOLIST,
   CMOLISTITEM,
   CMOLISTHEAD,
   CMOSTREAMWRAPPER,
   CMOSYNCRECORDSET,
   CMODATALIST,
   CMODATALISTENTRY,
   CMODATACHUNK,
   CMOSIMPLEDATACHUNK,
   CMOSIMPLEDATALIST,
   CMOBYTELIST,

   // add all new types before this one
   CMONUMOBJECTS
};

void OBJECT_TRACK_START();
void OBJECT_TRACK_END();
void OBJECT_INCREMENT( OBJECT_TYPE enObjectType );
void OBJECT_DECREMENT( OBJECT_TYPE enObjectType );

#else

#define OBJECT_INCREMENT( x )
#define OBJECT_DECREMENT( x )
#define OBJECT_TRACK_START()
#define OBJECT_TRACK_END()

#endif

#define MO_CHECK_ALLOC( ptr )                                              \
   if ( ptr == NULL )                                                      \
      {                                                                    \
      MOAssertFunc( _T( "ptr != NULL" ), NULL, _T(__FILE__),__LINE__ );    \
      SetLastErr( MO_ERR_MEM_ALLOC,                                        \
                  SOURCE_LOCATION );                                       \
      }                                                                    \



#endif // moMemUtils_H_INCLUDED
