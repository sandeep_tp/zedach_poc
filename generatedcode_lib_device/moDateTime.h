/*
Copyright (c) Sybase, Inc. 2012 All rights reserved. 

In addition to the license terms set out in the Sybase License Agreement for 
the Sybase Unwired Platform ("Program"), the following additional or different 
rights and accompanying obligations and restrictions shall apply to the source 
code in this file ("Code"). Sybase grants you a limited, non-exclusive, 
non-transferable, revocable license to use, reproduce, and modify the Code 
solely for purposes of (i) maintaining the Code as reference material to better 
understand the operation of the Program, and (ii) development and testing of 
applications created in connection with your licensed use of the Program. 
The Code may not be transferred, sold, assigned, sublicensed or otherwise 
conveyed (whether by operation of law or otherwise) to another party without 
Sybase's prior written consent. The following provisions shall apply to any 
modifications you make to the Code: (i) Sybase will not provide any maintenance 
or support for modified Code or problems that result from use of modified Code; 
(ii) Sybase expressly disclaims any warranties and conditions, express or 
implied, relating to modified Code or any problems that result from use of the 
modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING 
TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN 
IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
to indemnify, hold harmless, and defend Sybase from and against any claims or 
lawsuits, including attorney's fees, that arise from or are related to the 
modified Code or from use of the modified Code.
*/

/******************************************************************************
*    Copyright 2012 Sybase, Inc
*    Source File            : moDateTime.h
*    Platform Dependencies  :
*    Description            : Header file for CmoDateTime class.
*
*    Notes                  :
******************************************************************************/


#ifndef CmoDateTime_H_INCLUDED
#define CmoDateTime_H_INCLUDED

#include "moTypes.h"
#include "moString.h"
#ifdef ESI_PALM
#elif defined ( _WIN32_WCE )
   #include <oleauto.h>
#elif defined ( _WIN32 )
#endif

using namespace mo;


typedef enum MOTimeFormat
{
   tf12Hour,
   tfMilitary
}MOTimeFormat;

// YYMDhms
#define DATETIME_COMPRESSED_LEN  7
#define DATETIME_ZERO            '!'   /* char 48 0x30 */
#define DATETIME_RADIX           93    /* 93 characters are used */

#ifdef ESI_PALM
   #include <datetime.h>
#endif



class CmoDateTime
{
public:
   CmoDateTime();
   ~CmoDateTime();

   CmoDateTime( const TCHAR* pcDateTime );

#ifdef _WIN32
   CmoDateTime( DATE* pdtResult );
#endif

   static CmoDateTime Now();

   void SetDateTime( const TCHAR* pcDateTime );


   void SetDateTime( I16 sYear,     // 1 - 65534
                     I8 cMonth,     // 1 - 12
                     I8 cDay,       // 1 - 31
                     I8 cHour,      // 0 - 23
                     I8 cMin,       // 0 - 59
                     I8 cSec );     // 0 - 59


   void GetDateTime( I16* psYear,     // 1 - 65534
                     I8* pcMonth,     // 1 - 12
                     I8* pcDay,       // 1 - 31
                     I8* pcHour,      // 0 - 23
                     I8* pcMin,       // 0 - 59
                     I8* pcSec );     // 0 - 59

   void AsString( CmoString* pstrResult,
                  MOTimeFormat eTimeFormat = tf12Hour );

   void SetToNow();

#ifdef ESI_PALM
   void GetNative( DateTimeType* pstResult );

   void GetNativeDate( DateType* pstResult );

   void GetNativeTime( TimeType* pstResult );


   void SetNative( DateTimeType* pstValue );

   void SetNativeDate( DateType* pstValue );

   void SetNativeTime( TimeType* pstValue );

#elif _WIN32
   void GetVariant( DATE* pdtResult );

   void SetVariant( DATE* pdtValue );

   void getVariant( DATE* pdtResult ){ GetVariant( pdtResult );}

   void setVariant( DATE* pdtValue ){ SetVariant( pdtValue );}
#endif



   // Operators
   bool operator!= ( CmoDateTime& RH );

   bool operator== ( CmoDateTime& RH );

   bool operator> ( CmoDateTime& RH );

   bool operator< ( CmoDateTime& RH );

   I64 GetRaw(){ return m_cyDateTime;}

   void SetRaw( I64 cyValue ){ m_cyDateTime = cyValue;}

   void AsCompressed( TCHAR *pcValue );
   void SetCompressed( const TCHAR *pcValue );

private:
   I64 m_cyDateTime;
};// CmoDateTime




#endif// CmoDateTime_H_INCLUDED
