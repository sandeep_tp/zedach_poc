/*
 Copyright (c) Sybase, Inc. 2012   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 */

#import <Foundation/Foundation.h>

@class SUPE2ETraceRequest;
@class SAP_ExtendedPassport;

/*!
 @class SUPE2ETraceMessage
 @abstract 
    <p>Encapsulates a single request-response details  like time when first byte sent, last byte was received,request string, request/response headers etc. Also holds a passport.
    <p>To use this class the application code handling request-response needs to do the following</p>
    <p>SUPE2ETraceService *traceService = [SUPE2ETraceServiceImpl getInstance];</p>
    <p>SUPE2ETraceMessage *msg = [traceService createNewTraceMessage];</p>
    <p>After this the msg instance can be used to fill the request/response details and passed to the E2ETraceService for writing into the BTX</p>
   <p>[traceService addMessage:msg];</p>
   
   <p>In creating a new E2ETraceMessage object, the createNewTraceMessage method first creates a new request which would obtain a new passport. The passport can be fetched by [msg getPassport];</p>
 */
@interface SUPE2ETraceMessage : NSObject
{
    @private
    int _returnCode;
    NSDate *_firstByteRcvd;
    NSDate *_firstByteSent;
    NSDate *_lastByteRcvd;
    NSDate *_lastByteSent;
    
    unsigned long _recvBytes; 	
    unsigned long _sentBytes;
    unsigned long _duration;
    
    NSString *_name;
    NSString *_request;
    NSString *_response;
    NSString *_requestHeaders;
    NSString *_responseHeaders;
    
    SUPE2ETraceRequest *_e2eRequest;
    
    NSMutableDictionary *_requestHeadersMap;
    NSMutableDictionary *_responseHeadersMap;
    
}

/*!
 @property firstByteRcvd
 @abstract  First byte received timestamp
 @discussion
 */
@property(readwrite, retain, nonatomic) NSDate *firstByteRcvd;
/*!
 @property firstByteSent
 @abstract  First byte sent timestamp
 @discussion
 */
@property(readwrite, retain, nonatomic) NSDate *firstByteSent;

/*!
 @property lastByteRcvd
 @abstract  Last byte received timestamp
 @discussion
 */
@property(readwrite, retain, nonatomic) NSDate *lastByteRcvd;

/*!
 @property lastByteSent
 @abstract  Last byte sent timestamp
 @discussion
 */
@property(readwrite, retain, nonatomic) NSDate *lastByteSent;

/*!
 @property recvBytes
 @abstract  bytes received size
 @discussion
 */
@property(readwrite, assign, nonatomic) unsigned long recvBytes;

/*!
 @property sentBytes
 @abstract  bytes sent size
 @discussion
 */
@property(readwrite, assign, nonatomic) unsigned long sentBytes;

/*!
 @property duration
 @abstract  duration from first byte sent to last byte received
 @discussion
 */
@property(readwrite, assign, nonatomic) unsigned long duration;


/*!
 @property returnCode
 @abstract  return code of the request
 @discussion
 */
@property(readwrite, assign, nonatomic) int returnCode;

/*!
 @property name
 @abstract  name of the message
 @discussion  There is no such thing in iOS version. 
 */
@property(readwrite, copy, nonatomic) NSString *name;

/*!
 @property request
 @abstract  requestLine attribute
 @discussion  
 */
@property(readwrite, copy, nonatomic) NSString *request;

/*!
 @property response
 @abstract  response detail to the client request
 @discussion  There is no such thing in iOS version. 
 */
@property(readwrite, copy, nonatomic) NSString *response;

/*!
 @property requestHeaders
 @abstract  client request headers
 @discussion  
 */
@property(readwrite, copy, nonatomic) NSString *requestHeaders;

/*!
 @property responseHeaders
 @abstract  response headers to the client request
 @discussion  
 */
@property(readwrite, copy, nonatomic) NSString *responseHeaders;

/*!
 @method createInstance
 @abstract Returns a new instance of SUPE2ETraceMessage
 @discussion 
 @result The initalized SUPE2ETraceMessage.
 */
+ (SUPE2ETraceMessage *)createInstance;

/*!
 @method  getPassport
 @abstract Returns the newly created passport for current request 
 @return The request's passport Http Header
 */
- (NSString *)getPassport;

/*!
 @method  addRequestHeader
 @abstract Adds a request header to a map.
 @param name
 @param value
 */
- (void)addRequestHeader:(NSString *)name withValue:(NSString *)value;

/*!
 @method addResponseHeader
 @abstract Adds a response header to a map.
 @param name
 @param value
 */
- (void)addResponseHeader:(NSString *)name withValue:(NSString *)value;

@end


@interface SUPE2ETraceMessage (internal)
- (void)setE2ETraceRequest:(SUPE2ETraceRequest *)request;
- (SUPE2ETraceRequest*)getE2ETraceRequest;

@end


