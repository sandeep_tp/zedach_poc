/*
 
 Copyright (c) Sybase, Inc. 2009-2012   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */

#import <Foundation/Foundation.h>
#import "AvailabilityMacros.h"

/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "boolean". 
 @discussion 
 */
typedef BOOL SUPBoolean;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "string".   
 @discussion 
 */
typedef NSString* SUPString;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "binary".   
 @discussion 
 */
typedef NSData* SUPBinary;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "char".   
 @discussion 
 */
typedef unichar SUPChar;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "byte".   
 @discussion 
 */
typedef signed char SUPByte;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "short".   
 @discussion 
 */
typedef short SUPShort;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "int".   
 @discussion 
 */
typedef int32_t SUPInt;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "long".   
 @discussion 
 */
typedef int64_t SUPLong;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "integer".   
 @discussion 
 */
typedef NSNumber* SUPInteger;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "decimal".   
 @discussion 
 */
typedef NSNumber* SUPDecimal;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "float".   
 @discussion 
 */
typedef float SUPFloat;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "double".   
 @discussion 
 */
typedef double SUPDouble;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "date".   
 @discussion 
 */
typedef NSDate* SUPDate;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "time".   
 @discussion 
 */
typedef NSDate* SUPTime;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "dateTime".   
 @discussion 
 */
typedef NSDate* SUPDateTime;

/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "boolean?" (nullable).   
 @discussion 
 */
typedef NSNumber* SUPNullableBoolean;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "string?" (nullable).   
 @discussion 
 */
typedef NSString* SUPNullableString;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "binary?" (nullable).   
 @discussion 
 */
typedef NSData* SUPNullableBinary;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "char?" (nullable).   
 @discussion 
 */
typedef NSString* SUPNullableChar;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "byte?" (nullable).   
 @discussion 
 */
typedef NSNumber* SUPNullableByte;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "short?" (nullable).   
 @discussion 
 */
typedef NSNumber* SUPNullableShort;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "int?" (nullable).   
 @discussion 
 */
typedef NSNumber* SUPNullableInt;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "long?" (nullable).   
 @discussion 
 */
typedef NSNumber* SUPNullableLong;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "integer?" (nullable).   
 @discussion 
 */
typedef NSNumber* SUPNullableInteger;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "decimal?" (nullable).   
 @discussion 
 */
typedef NSNumber* SUPNullableDecimal;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "float?" (nullable).   
 @discussion 
 */
typedef NSNumber* SUPNullableFloat;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "double?" (nullable).   
 @discussion 
 */
typedef NSNumber* SUPNullableDouble;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "date?" (nullable).   
 @discussion 
 */
typedef NSDate* SUPNullableDate;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "time?" (nullable).   
 @discussion 
 */
typedef NSDate* SUPNullableTime;
/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "dateTime?" (nullable).   
 @discussion 
 */
typedef NSDate* SUPNullableDateTime;

/*!
 @typedef 
 @abstract  Objective-C type of attributes with SUP type "number?" (nullable).   
 @discussion 
 */
typedef NSNumber* SUPNumber;

/*!
 @enum 
 @abstract   Enumeration of the possible current login status values returned by the database class method getOnlineLoginStatus.
 @discussion Example of usage in a callback handler to handle a login failure:
 
 <pre>
 - (void)onLoginFailure
 {
    NSString *status;
    switch([Test_TestDB getOnlineLoginStatus].status)
    {
        case SUPLoginSuccess:
            status = @"Success";
            break;
        case SUPLoginFailure:
            status = @"Failure";
            break;
        case SUPLoginPending:
            status = @"Pending";
            break;
    }
    NSLog(@"------------------------------------------------------------");
    NSLog(@"login failed.... status = %@, code = %ld, message = %@",
           status,[Test_TestDB getOnlineLoginStatus].code,[Test_TestDB getOnlineLoginStatus].message);
    NSLog(@"------------------------------------------------------------");
 
 }
 </pre>
 
 */
typedef enum
{
    SUPLoginPending = 2,
    SUPLoginSuccess = 0,
    SUPLoginFailure = 1,
	
} SUPLoginStatus;


#import "SUPAnnotations.h"
#import "SUPNull.h"

#import "SUPArrayList.h"
#import "SUPBase64Encoding.h"
#import "SUPBase64EncodingException.h"
#import "SUPBinaryUtil.h"
#import "SUPBooleanUtil.h"
#import "SUPDateTimeUtil.h"
#import "SUPDateUtil.h"
#import "SUPNullPointerException.h"
#import "SUPNumberUtil.h"
#import "SUPStringCache.h"
#import "SUPStringUtil.h"
#import "SUPTimeUtil.h"

#import "SUPAbstractOperationException.h"
#import "SUPAssertionFailedException.h"
#import "MBOLogger.h"
#import "SUPClientRTStringLiterals.h"

