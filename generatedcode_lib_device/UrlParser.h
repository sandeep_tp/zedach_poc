#import <Foundation/Foundation.h>

@interface URLParser : NSObject 
{
}

+ (NSMutableDictionary *)parseQueryString:(NSString *)query;

@end
