/*******************************************************************************
* Source File : HybridAppHelper.h
* Date Created: 
* Copyright   : 2000 - 2010, Sybase, Inc.
* Description : 
* Notes       : 
*******************************************************************************/

#import <Foundation/Foundation.h>
#import "WidgetScreenControllerPrivate.h"
#import "ListenerManager.h"
#import "SSOCertManager.h"

#define kMaxMessageSize 10000000

/*!
 @class HybridAppHelper
 @abstract HybridAppHelper class. Provides native implementation of various functions used by other classes.
 */
@interface HybridAppHelper : NSObject
/*!
 @method
 @abstract get all installed HybridApps
 @param urlDict NSDictionary of url parameters
 @return NSMutableData of installed HybridApps
 */
+ (NSMutableData*) getAllInstalledApps:(NSDictionary*)urlDict;
/*!
 @method
 @abstract get all server-initiated HybridApps
 @return NSMutableData of server-initiated HybridApps
 */
+ (NSMutableData*) getAllServerInitiatedApps;
/*!
 @method
 @abstract get HybridApp by Id
 
 @param urlDict NSDictionary of url parameters
 
 @return NSMutableData of HybridApp
 */
+ (NSMutableData*) getAppById:(NSDictionary*)urlDict;
/*!
 @method
 @abstract get Current HybridApp
 
 @param widget Current HybridApp
 
 @return NSMutableData of current hybridapp
 */
+ (NSMutableData*) getCurrentApp:(Widget*)widget;
/*!
 @method
 @abstract open hybridApp
 
 @param urlDict  NSDictionary of url parameters
 @param screenController WidgetScreenController used to hold hybridapp
 
 @return NSMutableData
 */
+ (NSMutableData*) openHybridApp:(NSDictionary*)urlDict withScreenController:(WidgetScreenControllerPrivate*)screenController;
/*!
 @method
 @abstract get filtered messages
 
 @param urlDict NSDictionary of url parameters
 
 @return NSMutableData of messages
 */
+ (NSMutableData*) getFilteredMessages:(NSDictionary*)urlDict;
/*!
 @method
 @abstract get message by Id
 
 @param urlDict NSDictionary of url parameters
 
 @return NSMutableData of Message
 */
+ (NSMutableData*) getMessageById:(NSDictionary*)urlDict;
/*!
 @method
 @abstract open message
 
 @param urlDict NSDictionary of url parameters
 @param screenController WidgetScreenControllerPrivate used to hold message
 
 @return NSMutableData
 */
+ (NSMutableData*) openMessage:(NSDictionary*)urlDict withScreenController:(WidgetScreenControllerPrivate*)screenController;
/*!
 @method
 @abstract remove message
 
 @param urlDict NSDictionary of url parameters
 */
+ (void) removeMessage:(NSDictionary*)urlDict;
/*!
 @method
 @abstract update message
 
 @param urlDict NSDictionary of url parameters
 */
+ (void) updateMessage:(NSDictionary*)urlDict;
/*!
 @method
 @abstract get hybridApp icon
 
 @param urlDict NSDictionary of url parameters
 
 @return NSMutableData of hyrbidApp icon
 */
+ (NSMutableData*) getIcon:(NSDictionary*)urlDict;
/*!
 @method
 @abstract get custom icon
 
 @param urlDict NSDictionary of url parameters
 
 @return NSMutableData of custom icon
 */
+ (NSMutableData*) getCustomIcon:(NSDictionary*)urlDict;
/*!
 @method
 @abstract get client variables
 
 @param urlDict NSDictionary of url parameters
 
 @return NSMutableData of client variables
 */
+ (NSMutableData*) getClientVariables:(NSDictionary*)urlDict;
/*!
 @method
 @abstract get Application connection id
 
 @return NSMutableData of app connection id
 */
+ (NSMutableData*) getApplicationConnectionID;
/*!
 @method
 @abstract get connection settings
 
 @return NSMutableData of connection settings
 */
+ (NSMutableData*) loadSettings;
/*!
 @method
 @abstract save connection settings
 
 @param urlDict NSDictionary of url parameters
 
 @return NSMutableData
 */
+ (NSMutableData*) saveSettings:(NSDictionary*)urlDict;
/*!
 @method
 @abstract start messaging client
 */
+ (void) startClient;
/*!
 @method
 @abstract shutdown messaging client
 */
+ (void) shutdownClient;
/*!
 @method
 @abstract make messaging client to connect to sup server
 */
+ (void) connectToServer;
/*!
 @method
 @abstract make messaging client disconnect to sup server
 */
+ (void) disconnectFromServer;
/*!
 @method
 @abstract process trace request with url parameters
 @return NSData true or false
 */
+ (NSMutableData*) processTraceRequestForUrlDict:(NSDictionary*)urlDict;

@end
/*!
 @class AddDeviceRegistrationHelper
 @abstract Device registration helper
 */
@interface AddDeviceRegistrationHelper : NSObject
{
   int retCode;
   BOOL bThreadFinished;
}
/*!
 @method
 @abstract async register with password automaticlly
 
 @param password registration password
 
 @return result code
 */
- (int) autoRegisterWithPasswordAsync:(NSString*)password;

/*!
 @method
 @abstract sync call to register with password automaticlly
 
 @param password registration password
 
 @return result code
 */
- (int) autoRegisterWithPassword:(id)thePassword;
@end

/*!
 @class AfariaCertificateHelper
 @abstract Afaria Certificate helper class
 */
@interface AfariaCertificateHelper : NSObject
{
   NSInteger retCode;
   BOOL bThreadFinished;
}
/*!
 @method
 @abstract get signed certificate from afaria
 
 @param certData CertData
 @param urlScheme url
 @param commonName common name
 @param challengeCode challenge code
 @param label certificate label
 @return registration code
 */
-(NSInteger) getSignedCertificateFromAfaria:(CertData *)certData forUrlScheme:(NSString *)urlScheme forUser:(NSString *)commonName withChallengeCode:(NSString *)challengeCode certificateLabel:(NSString*)label;
@end

/*!
 @class ExternalResourceHelper
 @abstract External resource helper class
 */
@interface ExternalResourceHelper : NSObject
{
   NSString* responseMIMEType;
   NSData* responseData;
   NSInteger responseStatusCode;
   BOOL bThreadFinished;
}
/*!
 @property
 @abstract response resource MIME type
 */
@property (nonatomic, retain) NSString* responseMIMEType;
/*!
 @property
 @abstract response data
 */
@property (nonatomic, retain) NSData* responseData;
/*!
 @property
 @abstract response status code
 */
@property (nonatomic, assign) NSInteger responseStatusCode;
/*!
 @method
 @abstract get external resource
 
 @param url url of external resource
 @param options external resource request options
 
 @return NSData of response data
 */
- (NSData*) externalResource:(NSString*)url withOptions:(NSString*)options;
/*!
 @method
 @abstract get external resource
 
 @param url         url of request external resource
 @param method      request method
 @param headers     request headers
 @param postDataStr request post data
 @param json        json format
 
 @return NSData of response data
 */
- (NSData*) externalResource:(NSString*)url withMethod:(NSString*)method withHeaders:(NSDictionary*)headers withPostData:(NSString*)postDataStr withJsonFormat:(BOOL)json;
/*!
 @method
 @abstract get external resource synchronously
 
 @param url         url of request external resource
 @param method      request method
 @param headers     request headers
 @param postDataStr request post data
 @param json        json format
 */
- (void) getDataSynchronously:(NSString*)urlStr withMethod:(NSString*)method withHeaders:(NSDictionary*)headers withPostData:(NSString*)postDataStr withJsonFormat:(BOOL)json;
@end

/*!
 @class LocalStorageHelper
 @abstract local storeage helper class
 */
@interface LocalStorageHelper: NSObject
/*!
 @method
 @abstract process storage request for a hybridapp
 
 @param widget     hybridapp
 @param urlDict    NSDictionary of url parameters
 @param bodyDict   body
 @param iErrorCode error code
 @param sErrorMsg  error message
 
 @return NSMutableData of process result code
 */
+ (NSMutableData*) processStorageRequestForWidget:(Widget*)widget forUrlDict:(NSDictionary*)urlDict forBodyDict:(NSDictionary*)bodyDict outgoingErrorCode:(int*)iErrorCode outgoingErrorMessage:(NSString**)sErrorMsg;

@end

/*!
 @class AttachmentHelper
 @abstract Attachment helper class
 */
@interface AttachmentHelper : NSObject
{
   NSMutableDictionary* attachments;
   NSMutableDictionary* attachmentsResponse;
   
   WidgetScreenControllerPrivate* controller;
}
/*!
 @property
 @abstract attachment cache
 */
@property (nonatomic, retain) NSMutableDictionary* attachments;
/*!
 @property
 @abstract attachment response cache
 */
@property (nonatomic, retain) NSMutableDictionary* attachmentsResponse;
/*!
 @method
 @abstract Initization of attachment helper
 
 @param screenController hybridapp webview controller
 
 @return instance of attachment helper
 */
- (id) initWithScreenController:(WidgetScreenControllerPrivate*)screenController;
/*!
 @method
 @abstract show attachment view
 @param uniqueKey unique key
 @param fileName file name
 @param mimeType mime type
 @return id instance of attachment helper
 */
- (void) showAttachmentForUniqueKey:(NSString*)uniqueKey fileName:(NSString*)fileName mimeType:(NSString*)mimeType;

@end
/*!
 @class ImagePickerHelper
 @abstract Image picker helper class
 */
@interface ImagePickerHelper : NSObject <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate>
{
   BOOL pictureInProgress;
   UIPopoverController* popoverController;
   NSDictionary* pictureOptions;
   
   NSString* apiVersion;
   
   WidgetScreenControllerPrivate* controller;
}
/*!
 @property
 @abstract popoverController pop over controller
 */
@property (nonatomic, retain) UIPopoverController* popoverController;
/*!
 @property
 @abstract pictureOptions options
 */
@property (nonatomic, retain) NSDictionary* pictureOptions;
/*!
 @property
 @abstract apiVersion api version
 */
@property (nonatomic, retain) NSString* apiVersion;
/*!
 @method
 @abstract Initization of ImagePickerHelper
 
 @param screenController hybridapp webview controller
 
 @return id instance of ImagePickerHelper
 */
- (id) initWithScreenController:(WidgetScreenControllerPrivate*)screenController;
/*!
 @method
 @abstract process image picker command for Url dictionary
 
 @param urlDict Url dictionary
 */
- (void) processImagePickerCommandForUrlDict:(NSDictionary*)urlDict;
/*!
 @method
 @abstract Initization of ImagePickerHelper
 
 @param screenController hybridapp webview controller
 */
- (void) imageFromImagePicker:(NSData *)imageData fileName:(NSString *)fileName imageUri:(NSString *)imageUri;
/*!
 @method
 @abstract image picker cancelled
 */
- (void) imagePickerCancelledByUser;
/*!
 @method
 @abstract report picture error
 
 @param resultCode return code
 */
- (void) reportPictureError:(int)resultCode;
/*!
 @method
 @abstract delete all temporary image files
 */
+ (void) deleteAllTemporaryImageFiles;

@end
