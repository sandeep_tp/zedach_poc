//
//  MessageValueResolver.h
//  MO
//
//  Created by Richard Lee on 9/20/11.
//  Copyright 2011 Sybase. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageValueResolver : NSObject <NSXMLParserDelegate> {
    NSXMLParser *_parser;
    NSMutableString *_currentString;
    NSMutableString *_outputMessage;
    BOOL _fileTypeFound;
    BOOL _storingCharacters;
    int _version;
}

@property (nonatomic, assign) NSXMLParser *parser;
@property (nonatomic, assign) NSMutableString *currentString;
@property (nonatomic, assign) NSMutableString *outputMessage;
@property (nonatomic, assign) int version;

- (id)initWithWorkflowMessage:(NSString *)xml;
- (NSString *)replaceMessageValueWithBase64EncodedContent;

@end
