/*
 
 Copyright (c) Sybase, Inc. 2010-2012   All rights reserved.                                    
 
 In addition to the license terms set out in the Sybase License Agreement for 
 the Sybase Unwired Platform ("Program"), the following additional or different 
 rights and accompanying obligations and restrictions shall apply to the source 
 code in this file ("Code").  Sybase grants you a limited, non-exclusive, 
 non-transferable, revocable license to use, reproduce, and modify the Code 
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of 
 applications created in connection with your licensed use of the Program.  
 The Code may not be transferred, sold, assigned, sublicensed or otherwise 
 conveyed (whether by operation of law or otherwise) to another party without 
 Sybase's prior written consent.  The following provisions shall apply to any 
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or 
 implied, relating to modified Code or any problems that result from use of the 
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE 
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF 
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree 
 to indemnify, hold harmless, and defend Sybase from and against any claims or 
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code. 
 
 */

#import "sybase_sup.h"
#import "SUPBaseException.h"

#import <Foundation/Foundation.h>


/*!
 @class SUPApplicationRuntimeException
 @abstract   An exception that is thrown when a call to startConnection, registerApplication or unregisterApplication cannot be completed due to an error.
 @discussion 
 */
@interface SUPApplicationRuntimeException : SUPBaseException {

}

/*!
 @method applicationRuntimeExceptionWithErrorCode
 @abstract  Instantiates an SUPApplicationRuntimeException object
 @param  errorCode The error code value
 @result The SUPApplicationRuntimeException object.
 @discussion
 */
+ (SUPApplicationRuntimeException *)applicationRuntimeExceptionWithErrorCode:(int)errorCode;

/*!
 @method applicationRuntimeExceptionWithErrorCode
 @abstract  Instantiates an SUPApplicationRuntimeException object
 @param  errorCode The error code value
 @param  message the error message
 @result The SUPApplicationRuntimeException object.
 @discussion
 */
+ (SUPApplicationRuntimeException *)applicationRuntimeExceptionWithErrorCode:(int)errorCode message:(NSString *)message;


/*!
 @method applicationRuntimeExceptionWithErrorCode
 @abstract  Instantiates an SUPApplicationRuntimeException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @result The SUPApplicationRuntimeException object.
 @discussion
 */
+ (SUPApplicationRuntimeException *)applicationRuntimeExceptionWithErrorCode:(int)errorCode message:(NSString *)message arguments:(NSArray *)arguments;

/*!
 @method applicationRuntimeExceptionWithErrorCode
 @abstract  Instantiates an SUPApplicationRuntimeException object
 @param  errorCode The error code value
 @param  message the error message
 @param  arguments array containing argument list
 @param  cause The cause of the exception
 @result The SUPApplicationRuntimeException object.
 @discussion
 */
+ (SUPApplicationRuntimeException *)applicationRuntimeExceptionWithErrorCode:(int)errorCode message:(NSString *)message arguments:(NSArray* )arguments cause:(NSException *)cause;


/*!
 @method     
 @abstract   (Deprecated) Returns the SUPApplicationRuntimeException object with "cause".
 @param (NSException*)ex the cause of the exception.
 @result SUPApplicationRuntimeException
 @discussion 
 */
+ (SUPApplicationRuntimeException*)withCause :(NSException*)ex DEPRECATED_ATTRIBUTE;

/*!
 @method     
 @abstract   (Deprecated) Returns the SUPApplicationRuntimeException object with "theMessage".
 @param (NSString*)msg 
 @result SUPApplicationRuntimeException
 @discussion 
 */
+ (SUPApplicationRuntimeException*)withMessage :(NSString*)msg DEPRECATED_ATTRIBUTE;

/*!
 @method     
 @abstract   (Deprecated) Returns the SUPApplicationRuntimeException object with message and cause
 @param (NSString*)msg 
 @param (NSException*)ex
 @result SUPApplicationRuntimeException
 @discussion 
 */
+ (SUPApplicationRuntimeException*)withCauseAndMessage:(NSException*)ex message:(NSString*)msg DEPRECATED_ATTRIBUTE;




@end
