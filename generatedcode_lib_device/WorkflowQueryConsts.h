/*******************************************************************************
* Source File : WorkflowQueryConsts.h
* Date Created: 1/5/2011
* Copyright   : 2000 - 2011, Sybase, Inc.
* Description : Workflow constants
* Notes       :
*******************************************************************************/

#import "platmacro.h"

#define WF_QUERY_PARAM_ON_ERROR_MSG @"onerrormsg"
#define WF_QUERY_PARAM_ON_ERROR_CALLBACK @"onerrorcallback"

#define WF_QUERY_PARAM_QUERY_TYPE @"querytype"
#define WF_QUERY_PARAM_LOG_LEVEL @"loglevel"
#define WF_QUERY_PARAM_LOG_MSG @"logmessage"
#define WF_QUERY_PARAM_WORKFLOW_MESSAGE @"xmlworkflowmessage"
#define WF_QUERY_PARAM_RMI_TIMEOUT @"rmitimeout"
#define WF_QUERY_PARAM_ON_RMI_ERROR @"onrmierror"
#define WF_QUERY_PARAM_REQUEST_EXPIRY_QUERY @"requestexpiry"
#define WF_QUERY_PARAM_SUPUSERNAME @"supusername"
#define WF_QUERY_PARAM_SUPPASSWORD @"suppassword"
#define WF_QUERY_PARAM_MENU_ITEMS @"menuitems"
#define WF_QUERY_PARAM_ATTACHMENT_KEY @"attachmentkey"
#define WF_QUERY_PARAM_ATTACHMENT_DATA @"attachmentdata"
#define WF_QUERY_PARAM_MIMETYPE @"mimetype"
#define WF_QUERY_PARAM_TITLE @"title"
#define WF_QUERY_PARAM_UNIQUE_KEY @"uniquekey"
#define WF_QUERY_PARAM_FILENAME @"filename"
#define WF_QUERY_PARAM_ON_ERROR_MSG @"onerrormsg"
#define WF_QUERY_PARAM_ON_ERROR_CALLBACK @"onerrorcallback"
#define WF_QUERY_PARAM_CACHE_KEY @"cachekey"
#define WF_QUERY_PARAM_PARSE_MESSAGE @"parse"
#define WF_QUERY_PARAM_VERSION @"version"
#define WF_QUERY_PARAM_VALUE_LOAD_TRANSFORM_DATA @"loadtransformdata"
#define WF_QUERY_PARAM_VALUE_FORM_REDIRECT @"formredirect"
#define WF_QUERY_PARAM_VALUE_RMI @"rmi"
#define WF_QUERY_PARAM_VALUE_SUBMIT @"submit"
#define WF_QUERY_PARAM_VALUE_ACTIVATE @"activate"
#define WF_QUERY_PARAM_VALUE_CREDENTIALS @"credentials"
#define WF_QUERY_PARAM_VALUE_ADD_MENU_ITEM @"addmenuitem"
#define WF_QUERY_PARAM_VALUE_ADD_ALL_MENU_ITEMS @"addallmenuitems"
#define WF_QUERY_PARAM_VALUE_REMOVE_ALL_MENU_ITEMS @"removeallmenuitems"
#define WF_QUERY_PARAM_VALUE_CLEAR_REQUEST_CACHE @"clearrequestcache"
#define WF_QUERY_PARAM_VALUE_CLOSE @"close"
#define WF_QUERY_PARAM_VALUE_SHOW_PROGRESS_DIALOG @"showprogressdialog"
#define WF_QUERY_PARAM_VALUE_HIDE_PROGRESS_DIALOG @"hideprogressdialog"
#define WF_QUERY_PARAM_VALUE_LOG_TO_WORKFLOW @"logtoworkflow"
#define WF_QUERY_PARAM_VALUE_LOG_TO_TESTLOG @"logtotestlog"
#define WF_QUERY_PARAM_VALUE_SHOW_CERT_PICKER @"showcertpicker"
#define WF_QUERY_PARAM_VALUE_SHOW_ATTACHMENT @"showattachment"
#define WF_QUERY_PARAM_VALUE_DOWNLOAD_ATTACHMENT @"downloadattachment"
#define WF_QUERY_PARAM_VALUE_SET_SCREEN_TITLE @"setscreentitle"
#define WF_QUERY_PARAM_VALUE_CLEAR_REQUEST_CACHE_ITEM @"clearrequestcacheitem"
#define WF_QUERY_PARAM_VALUE_ON_DOWNLOAD_COMPLETE @"ondownloadcomplete"
#define WF_QUERY_PARAM_VALUE_TEMP_KEY_SHOW_ATTACHMENT @"tempkeyshowattachment"
#define WF_QUERY_PARAM_VALUE_SHOW_LOCAL_ATTACHMENT @"showlocalattachment"
#define WF_QUERY_PARAM_KEY @"key"
#define WF_QUERY_PARAM_SHOW_IN_BROWSER @"showinbrowser"
#define WF_QUERY_PARAM_VALUE_URL @"url"
#define WF_QUERY_PARAM_VALUE_ALERT @"alert"
#define WF_QUERY_PARAM_VALUE_MSG @"message"
#define WF_QUERY_PARAM_VALUE_TITLE @"title"
#define WF_QUERY_PARAM_VALUE_WF_STORAGE @"workflowstorage"
#define WF_QUERY_PARAM_VALUE_STORAGE_CMD @"command"
#define WF_QUERY_PARAM_VALUE_STORAGE_LENGTH @"length"
#define WF_QUERY_PARAM_VALUE_STORAGE_KEY @"key"
#define WF_QUERY_PARAM_VALUE_STORAGE_PARAM_INDEX @"index"
#define WF_QUERY_PARAM_VALUE_STORAGE_GET_ITEM @"getItem"
#define WF_QUERY_PARAM_VALUE_STORAGE_SET_ITEM @"setItem"
#define WF_QUERY_PARAM_VALUE_STORAGE_REMOVE_ITEM @"removeItem"
#define WF_QUERY_PARAM_VALUE_STORAGE_CLEAR @"clear"
#define WF_QUERY_PARAM_VALUE_STORAGE_PARAM_VALUE @"value"
#define WF_QUERY_PARAM_VALUE_STORAGE_PARAM_STORE @"store"
#define WF_QUERY_PARAM_VALUE_FOR_SHARED_STORAGE @"shared"
#define WF_QUERY_PARAM_VALUE_STORAGE_EXIST @"exist"
#define WF_QUERY_PARAM_VALUE_STORAGE_INDEX_EXIST @"indexexist"
#define WF_QUERY_PARAM_CERTIFICATE_STORE @"certificatestore"
#define WF_QUERY_PARAM_VALUE_COMMAND @"command"
#define WF_QUERY_PARAM_VALUE_USER_NAME @"username"
#define WF_QUERY_PARAM_VALUE_SERVER_PASSWORD @"serverpassword"
#define WF_QUERY_PARAM_VALUE_CERT_PASSWORD @"certpassword"
#define WF_QUERY_PARAM_VALUE_FILE_PATH @"filepath"
#define WF_QUERY_PARAM_VALUE_PASSWORD @"password"
#define WF_QUERY_PARAM_VALUE_COMMON_NAME @"commonname"
#define WF_QUERY_PARAM_VALUE_CHALLENGE_CODE @"challengecode"
#define WF_QUERY_PARAM_VALUE_CALLBACK_SUCCESS @"onsuccess"
#define WF_QUERY_PARAM_VALUE_CALLBACK_ERROR @"onerror"
#define WF_QUERY_PARAM_VALUE_COMMAND_GET_SIGNED_CERTIFICATE_FROM_AFARIA @"getSignedCertificateFromAfaria"
#define WF_QUERY_PARAM_VALUE_COMMAND_GET_SIGNED_CERTIFICATE_FROM_SERVER @"getSignedCertificateFromServer"
#define WF_QUERY_PARAM_VALUE_COMMAND_GET_SIGNED_CERTIFICATE_FROM_FILE @"getSignedCertificateFromFile"
#define WF_QUERY_PARAM_VALUE_TIME_ZONE @"tz"
#define WF_QUERY_PARAM_SAVE_CREDENTIAL @"savecredential"
#define WF_QUERY_PARAM_CACHE_POLICY_KEY @"cachepolicy"
#define WF_QUERY_PARAM_VALUE_CACHE_POLICY_SERVER_FIRST @"ServerFirst"
#define WF_QUERY_PARAM_VALUE_CACHE_POLICY_CACHE_FIRST @"CacheFirst"
#define WF_QUERY_PARAM_ASYNCHRONOUS_KEY @"asynchronous"
#define WF_QUERY_PARAM_VALUE_ASYNCHRONOUS_TRUE @"true"
#define WF_QUERY_PARAM_VALUE_ASYNCHRONOUS_FALSE @"false"
#define WF_QUERY_PARAM_VALUE_REQUEST_EXPIRY_NEVER @"NEVER"
#define WF_QUERY_PARAM_VALUE_CACHED_DATA @"CachedData"
#define WF_QUERY_PARAM_VALUE_SERVER_DATA @"ServerData"
#define WF_QUERY_PARAM_VALUE_GET_CLIENTVARIABLES @"getclientvariables"

#define WF_QUERY_TYPE_START_PUSH_NOTIFICATION_LISTENER @"startpushnotificationlistener"
#define WF_QUERY_TYPE_STOP_PUSH_NOTIFICATION_LISTENER @"stoppushnotificationlistener"
#define WF_QUERY_TYPE_INJECT_FAKE_PUSH_NOTIFICATION @"injectfakepushnotification"

#define WF_QUERY_TYPE_E2ETrace @"e2etrace"
#define WF_QUERY_PARAM_METHOD @"method"
#define WF_QUERY_PARAM_LEVEL @"level"
#define WF_QUERY_PARAM_VALUE_IS_TRACE_ENABLED @"istraceenabled"
#define WF_QUERY_PARAM_VALUE_SET_TRACE_LEVEL @"settracelevel"
#define WF_QUERY_PARAM_VALUE_START_TRACE @"starttrace"
#define WF_QUERY_PARAM_VALUE_STOP_TRACE @"stoptrace"
#define WF_QUERY_PARAM_VALUE_UPLOAD_TRACE @"uploadtrace"

#define WF_QUERY_TYPE_PERF @"perf"
#define WF_QUERY_PARAM_VALUE_IS_ENABLED @"isenabled"
#define WF_QUERY_PARAM_VALUE_START_INTERACTION @"startinteraction"
#define WF_QUERY_PARAM_VALUE_STOP_INTERACTION @"stopinteraction"
#define WF_QUERY_PARAM_VALUE_START_INTERVAL @"startinterval"
#define WF_QUERY_PARAM_VALUE_STOP_INTERVAL @"stopinterval"
#define WF_QUERY_PARAM_INTERACTION_NAME @"interactionname"
#define WF_QUERY_PARAM_INTERVAL_NAME @"intervalname"
#define WF_QUERY_PARAM_INTERVAL_TYPE @"intervaltype"

#define WF_QUERY_PARAM_VALUE_TZ_LOCALIZE_DATE_ID @"tzdate"
#define WF_QUERY_PARAM_VALUE_TZ_LOCALIZE_TIME_ID @"tztime"
#define WF_QUERY_PARAM_VALUE_TZ_LOCALIZE_DATETIME_ID @"tzdatetime"

#define WF_QUERY_PARAM_VALUE_EXPIRE_CREDENTIALS @"expirecredentials"
#define WF_QUERY_PARAM_VALUE_REQUIRES_ACTIVATION @"requiresactivation"
#define WF_QUERY_PARAM_VALUE_MARK_AS_ACTIVATED @"markasactivated"
#define WF_QUERY_PARAM_VALUE_MARK_AS_PROCESSED @"markasprocessed"

#define WF_QUERY_PARAM_VALUE_GET_INSTALLED_APPS @"getinstalledapps"
#define WF_QUERY_PARAM_VALUE_GET_SERVER_INITIATED_APPS @"getserverinitiatedapps"
#define WF_QUERY_PARAM_VALUE_GET_MESSAGES @"getmessages"
#define WF_QUERY_PARAM_VALUE_GET_COMPLETE_LIST @"getcompletelist"
#define WF_QUERY_PARAM_VALUE_LOAD_SETTINGS @"loadsettings"
#define WF_QUERY_PARAM_VALUE_SAVE_SETTINGS @"savesettings"
#define WF_QUERY_PARAM_VALUE_OPEN_HYBRIDAPP @"openhybridapp"
#define WF_QUERY_PARAM_VALUE_OPEN_MESSAGE @"openmessage"
#define WF_QUERY_PARAM_VALUE_REMOVE_MESSAGE @"removemessage"
#define WF_QUERY_PARAM_VALUE_START_CLIENT @"startclient"
#define WF_QUERY_PARAM_VALUE_SHUTDOWN_CLIENT @"shutdownclient"
#define WF_QUERY_PARAM_VALUE_CONNECT_TO_SERVER @"connecttoserver"
#define WF_QUERY_PARAM_VALUE_DISCONNECT_FROM_SERVER @"disconnectfromserver"
#define WF_QUERY_PARAM_VALUE_START_CONNECTION_LISTENER @"startconnectionlistener"
#define WF_QUERY_PARAM_VALUE_STOP_CONNECTION_LISTENER @"stopconnectionlistener"
#define WF_QUERY_PARAM_VALUE_GET_CURRENT_APP @"getcurrentapp"
#define WF_QUERY_PARAM_VALUE_GET_APP_BY_ID @"getappbyid"
#define WF_QUERY_PARAM_VALUE_GET_APPLICATION_CONNECTION_ID @"getconnectionid"
#define WF_QUERY_PARAM_VALUE_GET_MESSAGE_BY_ID @"getmessagebyid"
#define WF_QUERY_PARAM_VALUE_UPDATE_MESSAGE @"updatemessage"
#define WF_QUERY_PARAM_VALUE_START_MESSAGE_LISTENER @"startmsglistener"
#define WF_QUERY_PARAM_VALUE_STOP_MESSAGE_LISTENER @"stopmsglistener"
#define WF_QUERY_PARAM_VALUE_START_APP_LISTENER @"startapplistener"
#define WF_QUERY_PARAM_VALUE_STOP_APP_LISTENER @"stopapplistener"
#define WF_QUERY_PARAM_VALUE_START_APP_DOWNLOAD_LISTENER @"startappinstallationlistener"
#define WF_QUERY_PARAM_VALUE_STOP_APP_DOWNLOAD_LISTENER @"stopappinstallationlistener"
#define WF_QUERY_PARAM_VALUE_GET_ICON @"clienticon"
#define WF_QUERY_PARAM_VALUE_GET_CUSTOM_ICON @"customicon"

#ifdef AUTOMATED_TEST_BUILD
// not officially supported, but these are needed for the JSunit test
#define WF_QUERY_PARAM_VALUE_WRITE_APP @"writeapp"
#define WF_QUERY_PARAM_VALUE_REMOVE_APP @"removeapp"
#define WF_QUERY_PARAM_VALUE_WRITE_MESSAGE @"writemessage"
#define WF_QUERY_PARAM_VALUE_SET_DEFAULT_APP @"setdefaultapp"
#define WF_QUERY_PARAM_VALUE_ADD_TEST_NATIVE_APP_DL_LISTENER @"addnativeappdllistener"

#define WF_QUERY_PARAM_VALUE_INSTALL_APP @"installapp"
#define WF_QUERY_PARAM_VALUE_INSTALL_APP_NAME @"appname"
#define WF_QUERY_PARAM_VALUE_UNINSTALL_APP @"uninstallapp"
#define WF_QUERY_PARAM_VALUE_ENABLE_SHARED_STORAGE_TESTING @"enablestoragetesting"
#define WF_QUERY_PARAM_VALUE_DISABLE_SHARED_STORAGE_TESTING @"disablestoragetesting"
#endif

#define WF_QUERY_PARAM_VALUE_SETTING_ENABLE_AUTO_REGISTRATION @"enableautoregistration"
#define WF_QUERY_PARAM_VALUE_SETTING_SERVER_NAME @"servername"
#define WF_QUERY_PARAM_VALUE_SETTING_PORT @"port"
#define WF_QUERY_PARAM_VALUE_SETTING_FARM_ID @"farmid"
#define WF_QUERY_PARAM_VALUE_SETTING_USER_NAME @"username"
#define WF_QUERY_PARAM_VALUE_SETTING_ACTIVATION_CODE @"activationcode"
#define WF_QUERY_PARAM_VALUE_SETTING_PROTOCOL @"protocol"
#define WF_QUERY_PARAM_VALUE_SETTING_PASSWORD @"password"
#define WF_QUERY_PARAM_VALUE_SETTING_URLSUFFIX @"urlsuffix"

#define WF_QUERY_PARAM_VALUE_FILTER_MESSAGE_SENDER @"filtermessagesender"
#define WF_QUERY_PARAM_VALUE_FILTER_MESSAGE_SUBJECT @"filtermessagesubject"
#define WF_QUERY_PARAM_VALUE_FILTER_MESSAGE_MODULE_ID @"filtermessagemoduleid"
#define WF_QUERY_PARAM_VALUE_FILTER_MESSAGE_VERSION @"filtermessageversion"
#define WF_QUERY_PARAM_VALUE_FILTER_MESSAGE_ISREAD @"filtermessageisread"
#define WF_QUERY_PARAM_VALUE_FILTER_MESSAGE_PROCESSED @"filtermessageisprocessed"

#define WF_QUERY_PARAM_VALUE_MODULE_ID @"moduleid"
#define WF_QUERY_PARAM_VALUE_MODULE_VERSION @"moduleversion"
#define WF_QUERY_PARAM_VALUE_MESSAGE_ID @"msgid"

#define WF_QUERY_PARAM_VALUE_MESSAGE_LISTENER_CALLBACK @"messagelistenercallback"
#define WF_QUERY_PARAM_VALUE_APP_LISTENER_CALLBACK @"applistenercallback"

#define WF_QUERY_PARAM_VALUE_MESSAGE_FIELD_READ @"read"
#define WF_QUERY_PARAM_VALUE_MESSAGE_FIELD_PROCESSED @"processed"
#define WF_QUERY_PARAM_VALUE_MESSAGE_FIELD @"msgfield"
#define WF_QUERY_PARAM_VALUE_MESSAGE_STATUS @"status"
#define WF_QUERY_PARAM_VALUE_MESSAGE_FROM @"messagefrom"
#define WF_QUERY_PARAM_VALUE_MESSAGE_SUBJECT @"messagesubject"
#define WF_QUERY_PARAM_VALUE_ICON_INDEX @"iconindex"
#define WF_QUERY_PARAM_VALUE_ICON_ISPROCESSED @"processed"
#define WF_QUERY_PARAM_VALUE_USER_INVOCABLE @"userinvocable"
#define WF_QUERY_PARAM_VALUE_DISPLAY_NAME @"displayname"

#define WF_QUERY_PARAM_VALUE_EVENT_ID @"eventid"

//The following constants are flag values used in the listeners
#define LOG_LISTENER_FUNCTION_NAME @"hwc._logListenerNotification"
#define LOG_LISTENER_ERROR -1
#define LOG_LISTENER_OTHER 0
#define LOG_LISTENER_CONNECTED 1
#define LOG_LISTENER_DISCONNECTED 2
#define LOG_LISTENER_RETRIEVED_ITEMS 3
#define APPLICATION_LISTENER_FUNCTION_NAME @"hwc._applicationListenerNotification"
#define APPLICATION_LISTENER_HYBRIDAPP_ADDED 2
#define APPLICATION_LISTENER_HYBRIDAPP_UPDATED 3
#define APPLICATION_LISTENER_HYBRIDAPP_REMOVED 4
#define APPLICATION_LISTENER_REFRESH_REQUIRED 1
#define MESSAGE_LISTENER_FUNCTION_NAME @"hwc._messageListenerNotification"
#define MESSAGE_LISTENER_MESSAGE_ADDED 2
#define MESSAGE_LISTENER_MESSAGE_UPDATED 3
#define MESSAGE_LISTENER_MESSAGE_REMOVED 4
#define MESSAGE_LISTENER_REFRESH_REQUIRED 1
#define APP_DOWNLOAD_FUNCTION_NAME @"hwc.appInstallationListenerNotification"
#define APP_DOWNLOAD_BEGIN 1
#define APP_DOWNLOAD_END 2
#define APP_DOWNLOAD_FAILED 3
#define CONNECTION_STATE_FUNCTION_NAME @"hwc.connectionListenerNotification"
#define CONNECTION_STATE_CONNECTED 1
#define CONNECTION_STATE_DISCONNECTED 2
#define PUSH_NOTIFICATION_LISTENER_FUNCTION_NAME @"hwc._pushnotificationListenerNotification"
#define PREPACKAGE_APP_NOT_MATCHED @"hwc.clientpackage="

// The following constants are used for return values
#define OPEN_MESSAGE_RESULT_SUCCESS 0
#define OPEN_MESSAGE_RESULT_DOES_NOT_EXIST 1
#define OPEN_MESSAGE_RESULT_OTHER 2
#define OPEN_APP_RESULT_SUCCESS 0
#define OPEN_APP_RESULT_DOES_NOT_EXIST 1
#define OPEN_APP_RESULT_OTHER 2

// Image picker callback
#define WF_JAVASCRIPT_CALLBACK_GET_PICTURE_COMPLETE @"_Picture._getPictureComplete"

// The following constants match javascript constants used for message priority
#define MSG_PRIORITY_LOW 2
#define MSG_PRIORITY_NORMAL 1
#define MSG_PRIORITY_HIGH 3

#define WF_QUERY_PARAM_VALUE_TZ_UTCOFFSET @"utcoffset"
#define WF_QUERY_PARAM_VALUE_TZ_DSTOFFSET @"dstoffset"
#define WF_QUERY_PARAM_VALUE_TZ_INDST @"indst"
#define WF_QUERY_PARAM_VALUE_TZ_DST_AWARE @"dstaware"
#define WF_QUERY_PARAM_VALUE_TZ_ID @"tzid"
#define WF_QUERY_VALUE_TZ_TIME @"time"
#define WF_QUERY_PARAM_GET_PICTURE @"getpicture"
#define WF_QUERY_PARAM_VALUE_PICTURE_OPTIONS @"pictureoptions"
#define WF_QUERY_PARAM_CALLBACK @"callback"
#define WF_QUERY_PARAM_DIALOG_ID @"dialogid"
#define WF_QUERY_PARAM_VALUE_RESOURCE @"externalresource"
#define WF_QUERY_PARAM_VALUE_URL @"url"
#define WF_QUERY_PARAM_VALUE_OPTIONS @"options"
#define WF_QUERY_PARAM_VALUE_METHOD @"method"
#define WF_QUERY_PARAM_VALUE_HEADERS @"headers"
#define WF_QUERY_PARAM_VALUE_DATA @"data"
#define WF_QUERY_PARAM_VALUE_STATUS @"status"
#define WF_QUERY_PARAM_VALUE_STATUS_TEXT @"statusText"
#define WF_QUERY_PARAM_VALUE_RESPONSE_TEXT @"responseText"
#define WF_QUERY_PARAM_MEDIA_CACHE @"mediacache"
#define WF_QUERY_PARAM_MEDIA_CACHE_POLICY @"policy"

#define GENERIC_HTTP_REQ_ERROR 500 
#define GENERIC_HTTP_REQ_ERROR_STR @"Error requesting resource"
