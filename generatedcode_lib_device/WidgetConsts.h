/*
 *  WidgetConsts.h
 *  Widgets
 *
 *  Created by Alexey Piterkin on 2/10/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#define W_LABEL_ON_THE_TOP_FONT_SIZE      13
#define W_LABEL_ON_THE_SIDE_FONT_SIZE     16
#define W_CONTENT_FONT_SIZE               16

#define W_LABEL_TEXT_COLOR                [UIColor darkGrayColor]
#define W_LABEL_TEXT_COLOR_HIGHLIGHTED    [UIColor whiteColor]
#define W_LABEL_TEXT_COLOR_ACTIVE         [UIColor blueColor]

#define W_CONTENT_TEXT_COLOR              [UIColor blackColor]
#define W_CONTENT_TEXT_COLOR_HIGHLIGHTED  [UIColor whiteColor]

#define W_CELL_H_MARGIN                   10
#define W_CELL_V_MARGIN                   5
#define W_CELL_H_SPACING                  10
#define W_CELL_V_SPACING                  2
// Used for content padding and label padding if label is on the side
#define W_CONTENT_V_PADDING               5
#define W_CONTENT_H_PADDING               5
// Only used for label at the top.
#define W_LABEL_V_PADDING                 1


// The folling is in %
#define W_DEFAULT_MIN_LABEL_WIDTH         30
#define W_DEFAULT_MIN_CONTENT_WIDTH       40
