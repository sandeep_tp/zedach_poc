/*
 
 Copyright (c) Sybase, Inc. 2009-2013   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 
 */

#import "sybase_core.h"

@class SUPJsonArray;
@class SUPJsonValue;
@class SUPJsonWriter;
@class SUPObjectList;
@class SUPStringList; 
@class SUPBigString;
@class SUPBigBinary;

@class SUPJsonObject;

/*!
 @class
 @abstract Class representing JSON objects that contain 0 or more name value pairs.
 @discussion
 */
@interface SUPJsonObject : NSObject

- (SUPJsonObject*)init;
- (void)dealloc;
+ (SUPJsonObject*)getInstance;

- (NSString*)toString;
- (NSString*)description;

#pragma mark -
#pragma mark Methods for accessing values

/*!
 @method
 @abstract Returns the JSON value for the given name as an array.
 @discussion
 @param index The name of the value to get.
 @result The array.
 */
- (SUPJsonArray*)getArray:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as an object.
 @discussion
 @param index The name of the value to get.
 @result The object.
 */
- (SUPJsonObject*)getObject:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a boolean.
 @discussion
 @param index The name of the value to get.
 @result The boolean.
 */
- (SUPBoolean)getBoolean:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a string.
 @discussion
 @param index The name of the value to get.
 @result The string.
 */
- (SUPString)getString:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a binary.
 @discussion
 @param index The name of the value to get.
 @result The binary.
 */
- (SUPBinary)getBinary:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a char.
 @discussion
 @param index The name of the value to get.
 @result The char.
 */
- (SUPChar)getChar:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a byte.
 @discussion
 @param index The name of the value to get.
 @result The byte.
 */
- (SUPByte)getByte:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a short.
 @discussion
 @param index The name of the value to get.
 @result The short.
 */
- (SUPShort)getShort:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as an int.
 @discussion
 @param index The name of the value to get.
 @result The int.
 */
- (SUPInt)getInt:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a long.
 @discussion
 @param index The name of the value to get.
 @result The long.
 */
- (SUPLong)getLong:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as an integer.
 @discussion
 @param index The name of the value to get.
 @result The integer.
 */
- (SUPInteger)getInteger:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a decimal.
 @discussion
 @param index The name of the value to get.
 @result The decimal.
 */
- (SUPDecimal)getDecimal:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a float.
 @discussion
 @param index The name of the value to get.
 @result The float.
 */
- (SUPFloat)getFloat:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a double.
 @discussion
 @param index The name of the value to get.
 @result The double.
 */
- (SUPDouble)getDouble:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a date.
 @discussion
 @param index The name of the value to get.
 @result The date.
 */
- (SUPDate)getDate:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a time.
 @discussion
 @param index The name of the value to get.
 @result The time.
 */
- (SUPTime)getTime:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a dateTime.
 @discussion
 @param index The name of the value to get.
 @result The dateTime.
 */
- (SUPDateTime)getDateTime:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a nullable boolean.
 @discussion
 @param index The name of the value to get.
 @result The nullable boolean.
 */
- (SUPNullableBoolean)getNullableBoolean:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a nullable string.
 @discussion
 @param index The name of the value to get.
 @result The nullable string.
 */
- (SUPNullableString)getNullableString:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a nullable binary.
 @discussion
 @param index The name of the value to get.
 @result The nullable binary.
 */
- (SUPNullableBinary)getNullableBinary:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a nullable char.
 @discussion
 @param index The name of the value to get.
 @result The nullable char.
 */
- (SUPNullableChar)getNullableChar:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a nullable byte.
 @discussion
 @param index The name of the value to get.
 @result The nullable byte.
 */
- (SUPNullableByte)getNullableByte:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a nullable short.
 @discussion
 @param index The name of the value to get.
 @result The nullable short.
 */
- (SUPNullableShort)getNullableShort:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a nullable int.
 @discussion
 @param index The name of the value to get.
 @result The nullable int.
 */
- (SUPNullableInt)getNullableInt:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a nullable long.
 @discussion
 @param index The name of the value to get.
 @result The nullable long.
 */
- (SUPNullableLong)getNullableLong:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a nullable integer.
 @discussion
 @param index The name of the value to get.
 @result The nullable integer.
 */
- (SUPNullableInteger)getNullableInteger:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a nullable decimal.
 @discussion
 @param index The name of the value to get.
 @result The nullable decimal.
 */
- (SUPNullableDecimal)getNullableDecimal:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a nullable float.
 @discussion
 @param index The name of the value to get.
 @result The nullable float.
 */
- (SUPNullableFloat)getNullableFloat:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a nullable double.
 @discussion
 @param index The name of the value to get.
 @result The nullable double.
 */
- (SUPNullableDouble)getNullableDouble:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a nullable date.
 @discussion
 @param index The name of the value to get.
 @result The nullable date.
 */
- (SUPNullableDate)getNullableDate:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a nullable time.
 @discussion
 @param index The name of the value to get.
 @result The nullable time.
 */
- (SUPNullableTime)getNullableTime:(NSString*)name;

/*!
 @method
 @abstract Returns the JSON value for the given name as a nullable dateTime.
 @discussion
 @param index The name of the value to get.
 @result The nullable dateTime.
 */
- (SUPNullableDateTime)getNullableDateTime:(NSString*)name;


#pragma mark -
#pragma mark Methods for setting values


/*!
 @method
 @abstract Sets the given array value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setArray:(NSString*)name : (SUPJsonArray*)value;

/*!
 @method
 @abstract Sets the given object value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setObject:(NSString*)name : (SUPJsonObject*)value;

/*!
 @method
 @abstract Sets the given boolean value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setBoolean:(NSString*)name : (SUPBoolean)value;

/*!
 @method
 @abstract Sets the given string value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setString:(NSString*)name : (SUPString)value;

/*!
 @method
 @abstract Sets the given binary value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setBinary:(NSString*)name : (SUPBinary)value;

/*!
 @method
 @abstract Sets the given char value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setChar:(NSString*)name : (SUPChar)value;

/*!
 @method
 @abstract Sets the given byte value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setByte:(NSString*)name : (SUPByte)value;

/*!
 @method
 @abstract Sets the given short value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setShort:(NSString*)name : (SUPShort)value;

/*!
 @method
 @abstract Sets the given int value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setInt:(NSString*)name : (SUPInt)value;

/*!
 @method
 @abstract Sets the given long value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setLong:(NSString*)name : (SUPLong)value;

/*!
 @method
 @abstract Sets the given integer value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setInteger:(NSString*)name : (SUPInteger)value;

/*!
 @method
 @abstract Sets the given decimal value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setDecimal:(NSString*)name : (SUPDecimal)value;

/*!
 @method
 @abstract Sets the given float value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setFloat:(NSString*)name : (SUPFloat)value;

/*!
 @method
 @abstract Sets the given double value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setDouble:(NSString*)name : (SUPDouble)value;

/*!
 @method
 @abstract Sets the given date value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setDate:(NSString*)name : (SUPDate)value;

/*!
 @method
 @abstract Sets the given time value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setTime:(NSString*)name : (SUPTime)value;

/*!
 @method
 @abstract Sets the given dateTime value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setDateTime:(NSString*)name : (SUPDateTime)value;

/*!
 @method
 @abstract Sets the given nullable boolean value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setNullableBoolean:(NSString*)name : (SUPNullableBoolean)value;

/*!
 @method
 @abstract Sets the given nullable string value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setNullableString:(NSString*)name : (SUPNullableString)value;

/*!
 @method
 @abstract Sets the given nullable binary value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setNullableBinary:(NSString*)name : (SUPNullableBinary)value;

/*!
 @method
 @abstract Sets the given nullable char value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setNullableChar:(NSString*)name : (SUPNullableChar)value;

/*!
 @method
 @abstract Sets the given nullable byte value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setNullableByte:(NSString*)name : (SUPNullableByte)value;

/*!
 @method
 @abstract Sets the given nullable short value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setNullableShort:(NSString*)name : (SUPNullableShort)value;

/*!
 @method
 @abstract Sets the given nullable int value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setNullableInt:(NSString*)name : (SUPNullableInt)value;

/*!
 @method
 @abstract Sets the given nullable long value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setNullableLong:(NSString*)name : (SUPNullableLong)value;

/*!
 @method
 @abstract Sets the given nullable integer value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setNullableInteger:(NSString*)name : (SUPNullableInteger)value;

/*!
 @method
 @abstract Sets the given nullable decimal value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setNullableDecimal:(NSString*)name : (SUPNullableDecimal)value;

/*!
 @method
 @abstract Sets the given nullable float value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setNullableFloat:(NSString*)name : (SUPNullableFloat)value;

/*!
 @method
 @abstract Sets the given nullable double value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setNullableDouble:(NSString*)name : (SUPNullableDouble)value;

/*!
 @method
 @abstract Sets the given nullable date value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setNullableDate:(NSString*)name : (SUPNullableDate)value;

/*!
 @method
 @abstract Sets the given nullable time value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setNullableTime:(NSString*)name : (SUPNullableTime)value;

/*!
 @method
 @abstract Sets the given nullable dateTime value for the given name in this JSON object.
 @discussion
 @param name The name to set.
 @param value The value to set.
 */
- (void)setNullableDateTime:(NSString*)name : (SUPNullableDateTime)value;


#pragma mark -
#pragma mark Generic JSON object methods

/*!
 @method
 @abstract Generic method to set a key value pair in this JSON object.
 @discussion
 @param key The name to set.
 @param value The value to set.
 */
- (void)add:(SUPString)key :(id)value;

/*!
 @method
 @abstract Remove a key value pair from this JSON object.
 @discussion
 @param key The name of the name value pair to remove.
 */
- (void)remove:(SUPString)key;

/*!
 @method
 @abstract Remove all key value pairs from this JSON object.
 @discussion
 */
- (void)clear;

/*!
 @method 
 @abstract Check for the existence of a name value pair in this JSON object.
 @discussion
 @param key The name to check for.
 @result True if the name appears in the JSON object, false otherwise.
 */
- (SUPBoolean)containsKey:(SUPString)key;

/*!
 @method
 @abstract Generic method to return the value for a name value pair in this JSON object.
 @discussion
 @param key The name to return the value for.
 @result The value.
 */
- (id)item:(SUPString)key;

/*!
 @method
 @abstract Returns an SUPStringList containing all the names of the name value pairs in this JSON object.
 @discussion
 */
- (SUPStringList*)keys;

/*!
 @method
 @abstract Returns an SUPObjectList containing all the values for the name value pairs in this JSON object.
 @discussion
 */
- (SUPObjectList*)values;

/*!
 @method
 @abstract Returns the internal NSMutableDictionary used by this JSON object.
 @discussion
 */
- (NSMutableDictionary*)internalMap;

@end
