//
//  WorkflowViewControllerPrivate.h
//
//  Copyright 2009 Sybase. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "WidgetScreenController.h"

@interface WorkflowViewControllerPrivate : NSObject {
   id delegate;
}

@property (nonatomic, assign) id delegate;

- (BOOL)submitResponse:(WidgetContext*)context withController:(UIViewController*)controller withData:(char*)data;
- (BOOL)markInboxItemAsProcessed:(UI32)moduleId withModuleVersion:(UI32)moduleVersion withMsgId:(I32)msgId;

@end

