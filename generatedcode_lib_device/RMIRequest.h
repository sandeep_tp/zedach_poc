//
//  RMIRequest.h
//  MO
//
//  Created by Alexey Piterkin on 4/2/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "WidgetScreenControllerPrivate.h"

@interface RMIRequest : NSObject {
   NSDictionary* rmiParams;

   NSTimeInterval cacheTimeout;
   
   // username and password are values used to send to server for making the rmi request, they may come from credential cache or from javascript with rmi request
   NSString* username;
   NSData* password;
   BOOL hasNewCredentials;
   
   NSString* data;
   
   char* cachedResponse;
   BOOL usingCachedResponse;
   BOOL responseFoundInCache;
   double storedCacheTimeout;
   NSString* cacheKey;
   NSString* cachePolicy;
   BOOL asyncReq;
   BOOL shouldTryLocalCacheIfServerCallFailed;
   
   WidgetScreenControllerPrivate* controller;
   NSString* uniqueKey;
   NSString* attachmentKey;
   NSString* onDownloadCompleteFunc;
   
   NSTimer* timer;
   NSDate* startTime;
   BOOL timedOut;
   BOOL awaitingSend; // this is used by the RMIRequestManager to avoid starting the timer if the request is not waiting in the queue
   NSTimeInterval timeout;
}

@property (nonatomic, retain) NSString* username;
@property (nonatomic, retain) NSData* password;
@property (nonatomic, retain) NSString* data;
@property (nonatomic, readwrite, assign) WidgetScreenControllerPrivate* controller;
@property (nonatomic, readonly, retain) NSString* uniqueKey;
@property (nonatomic, readonly, retain) NSString* attachmentKey;
@property (nonatomic, readonly, retain) NSString* onDownloadCompleteFunc;
@property (nonatomic, readonly, retain) NSDictionary* rmiParams;
@property (nonatomic, retain) NSTimer* timer;
@property (nonatomic, retain) NSDate* startTime;
@property (nonatomic, assign) BOOL timedOut;
@property (nonatomic, assign) BOOL awaitingSend;
@property (nonatomic, assign) NSTimeInterval timeout;

- (id) initForAction:(NSDictionary*)rmiParameters
             timeout:(NSTimeInterval)theTimeout
        cacheTimeout:(NSTimeInterval)expirySecs
            cacheKey:(NSString*)theCacheKey
      forCachePolicy:(NSString*)theCachePolicy
      isAsynchronous:(BOOL)asynchronous
            withData:(NSString*)data
          controller:(WidgetScreenControllerPrivate*)aController
             withKey:(NSString*)attKey
       withUniqueKey:(NSString*)uKey
withOnDownloadComplete:(NSString*)onDownloadComplete
        withUsername:(NSString*)newUsername
        withPassword:(NSString*)newPassword;

// SMPONP-15317 method name "processResponse" is not allowed by Apple App Store
// Change it to processResp
- (void) processResp:(NSString*)body;
- (void) processException:(NSString*)errorMsg code:(int)errCode;
- (void) send;

@end
