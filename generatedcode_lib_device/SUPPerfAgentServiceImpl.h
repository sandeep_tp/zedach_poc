/*
 Copyright (c) Sybase, Inc. 2012   All rights reserved.
 
 In addition to the license terms set out in the Sybase License Agreement for
 the Sybase Unwired Platform ("Program"), the following additional or different
 rights and accompanying obligations and restrictions shall apply to the source
 code in this file ("Code").  Sybase grants you a limited, non-exclusive,
 non-transferable, revocable license to use, reproduce, and modify the Code
 solely for purposes of (i) maintaining the Code as reference material to better
 understand the operation of the Program, and (ii) development and testing of
 applications created in connection with your licensed use of the Program.
 The Code may not be transferred, sold, assigned, sublicensed or otherwise
 conveyed (whether by operation of law or otherwise) to another party without
 Sybase's prior written consent.  The following provisions shall apply to any
 modifications you make to the Code: (i) Sybase will not provide any maintenance
 or support for modified Code or problems that result from use of modified Code;
 (ii) Sybase expressly disclaims any warranties and conditions, express or
 implied, relating to modified Code or any problems that result from use of the
 modified Code; (iii) SYBASE SHALL NOT BE LIABLE FOR ANY LOSS OR DAMAGE RELATING
 TO MODIFICATIONS MADE TO THE CODE OR FOR ANY DAMAGES RESULTING FROM USE OF THE
 MODIFIED CODE, INCLUDING, WITHOUT LIMITATION, ANY INACCURACY OF DATA, LOSS OF
 PROFITS OR DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, EVEN
 IF SYBASE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (iv) you agree
 to indemnify, hold harmless, and defend Sybase from and against any claims or
 lawsuits, including attorney's fees, that arise from or are related to the
 modified Code or from use of the modified Code.
 */

#import "SUPPerformanceAgentService.h"
#import "SUPPerfAgentServiceListener.h"

@class PerformanceAgent;

extern NSString * const SUP_PAS_PERSISTENCE_READ;
extern NSString * const SUP_PAS_PERSISTENCE_WRITE;
extern NSString * const SUP_PAS_HTTP_REQUEST;
extern NSString * const SUP_PAS_SUBMIT_PENDING;
extern NSString * const SUP_PAS_CANCEL_PENDING;
extern NSString * const SUP_PAS_TRANSACTION;

//KPIs
extern NSString * const SUP_PAS_COUNTER_TRAFFIC_SENT; //bytesSent
extern NSString * const SUP_PAS_COUNTER_TRAFFIC_RECEIVED; //bytesReceived
extern NSString * const SUP_PAS_COUNTER_NETWORK_TIME; //networkTime
extern NSString * const SUP_PAS_COUNTER_ROUND_TRIPS; //roundTrips
extern NSString * const SUP_PAS_COUNTER_TOTAL_BYTES; //totalBytes
extern NSString * const SUP_PAS_COUNTER_MEM_ALLOCATED; //memAllocated

/*!
 @class
 @abstract Class that implements the SUPPerformanceAgentService protocol.
 @discussion
 */
@interface SUPPerfAgentServiceImpl : NSObject<SUPPerformanceAgentService>
{
    @private
    PerformanceAgent *_delegate;
    id<SUPPerfAgentServiceListener> _listener;
}

@property(readwrite, retain, nonatomic) PerformanceAgent *delegate;

@property(readwrite, retain, nonatomic) id<SUPPerfAgentServiceListener> listener;

+ (SUPPerfAgentServiceImpl *)getInstance;

- (BOOL)isEnabled;

+ (BOOL)isEnabled;

/*!
 @method
 @abstract start an interval. The service will start collecting interval metrics after this is called.
 @param name The name of the interval.
 @param type The type of the interval.
 */

- (void)startInterval:(NSString *)name withType:(NSString *)type;

/*!
 @method
 @abstract stop an interval. The service will stop collecting interval metrics after this is called.
 <p>NOTE: After an interval is started it has to be stopped before another interval with the same name can be stated.
 @param name The name of the interval.
 */
- (void)stopInterval:(NSString *)name;
@end

@interface SUPPerfAgentServiceImpl (internal)
+ (NSString *)intervalNameWithType:(NSString *)name withType:(NSString *)type;

/*!
 @method
 @abstract 
 @param size max size for the log file in KB.  The default log file size is 512KB set during instance initialization
 */
- (void)resetLogFileSize:(int)size;

@end

